<?php
$string ="
<section class=\"content-header\">
    <h1>
        " . ucfirst($c_url) . "
        <small>Data Master</small>
    </h1>
    <ol class=\"breadcrumb\">
        <li><a href=\"<?php echo base_url() ?>\"><i class=\"fa fa-dashboard\"></i>Home</a></li>
        <li>Data Master</li>
        <li class=\"active\">" . ucfirst($c_url) . "</li>
    </ol>


</section>
    
<section class=\"content\">
    <div class=\"box box-default\">

        <div class=\"box-body\">
            <div id=\"notification\" ></div>
            <div class=\" headerbutton\">
\n";
if($jenis_form!="modal")
{
    $string.="                <?php echo anchor(site_url('" . $c_url . "/create_".$c_url."'), 'Create', 'class=\"btn btn-success\"'); ?>";
}
else
{
    $string.="                <?php echo anchor(\"\", 'Create', 'class=\"btn btn-success\" id=\"btt_create\"'); ?>";
    
}
$string .= "\n\t    </div>
        </div>
         <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"portlet-body form\">
        <table class=\"table table-striped table-bordered table-hover\" id=\"mytable\">";

$column_non_pk = array();
$strtype='';
foreach ($non_pk as $row) {
    if ((strpos($row['column_name'], 'create') === false) && (strpos($row['column_name'], 'update') === false)&& (strpos($row['column_name'], 'deleted') === false)) {
        if($row['column_name']=="status")
        {
            $column_non_pk[] .= "\n\t\t\t{data: \"" . $row['column_name'] . "\" , orderable:false , title :\"" . label($row['column_name']) . "\",
                        mRender: function (data, type, row) {
                            return data==1?\"Active\":\"Not Active\";
                        }}";
        }
        else
        {
        if($row['data_type']=="tinyint")
        {
        $column_non_pk[] .= "\n\t\t\t{data: \"" . $row['column_name'] . "\" , orderable:false , title :\"" . label($row['column_name']) . "\",
                        mRender: function (data, type, row) {
                            return data==1?\"yes\":\"no\";
                        }}";
        }
        else if($row['data_type']=="date"||$row['data_type']=="datetime")
        {
            $column_non_pk[] .= "\n\t\t\t{data: \"" . $row['column_name'] . "\" , orderable:false , title :\"" . label($row['column_name']) . "\",
                        mRender: function (data, type, row) {
                            return  DefaultDateFormat(data);
                        }}";
            
        }
        else if($row['data_type']=="decimal")
        {
            $column_non_pk[] .= "\n\t\t\t{data: \"" . $row['column_name'] . "\" , orderable:false , title :\"" . label($row['column_name']) . "\",
                        mRender: function (data, type, row) {
                            return Comma(data==undefined?0:data);
                        }}";
        }
        else
        {
        $column_non_pk[] .= "\n\t\t\t{data: \"" . $row['column_name'] . "\" , orderable:false , title :\"" . label($row['column_name']) . "\"}";
        }
        }
    }
}
$col_non_pk = implode(',', $column_non_pk);

$string .= "\n\t    
        </table>\n";

$string .="        </div>
                </div>

            </div>
        </div>

</section>
        <script type=\"text/javascript\">
             var table;
            function delete".$c_url."(".$pk.") {


        swal({
            title: \"Are you sure delete this data?\",
            text: \"You will not be able to recover this data!\",
            type: \"warning\",
            showCancelButton: true,
            confirmButtonColor: \"#DD6B55\",
            confirmButtonText: \"Yes, delete it!\",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/".$c_url."/".$c_url."_delete',
                dataType: 'json',
                data: {
                    ".$pk.": ".$pk."
                },
                success: function (data) {
                    if (data.st)
                    {
                        messagesuccess(data.msg);
                        table.fnDraw(false);
                    }
                    else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
       }});


    }\n";
if($jenis_form=="modal")
{
$string.="    function edit".$c_url."(".$pk.") {

        $.ajax({url: baseurl + 'index.php/".$c_url."/edit_".$c_url."/' + ".$pk.",
            success: function (data) {
                modalbootstrap(data, \"Edit ".$c."\");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });\n\n}\n";

    }
    


$string.="            $(document).ready(function() {\n";
if($jenis_form=="modal")
{
$string.="                $(\"#btt_create\").click(function () {
                $.ajax({url: baseurl + 'index.php/".$c_url."/create_".$c_url."',
                success: function (data) {
                        modalbootstrap(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });


        })\n";
}
$string.="                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        \"iStart\": oSettings._iDisplayStart,
                        \"iEnd\": oSettings.fnDisplayEnd(),
                        \"iLength\": oSettings._iDisplayLength,
                        \"iTotal\": oSettings.fnRecordsTotal(),
                        \"iFilteredTotal\": oSettings.fnRecordsDisplay(),
                        \"iPage\": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        \"iTotalPages\": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                table = $(\"#mytable\").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    oLanguage: {
                        sProcessing: \"loading...\"
                    },
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    ajax: {\"url\": \"" . $c_url . "/getdata" . $c_url . "\", \"type\": \"POST\"},
                    columns: [
                        {
                           data: \"$pk\",
                           title: \"Kode\",
                           orderable: false
                        }," . $col_non_pk . ",
                        {
                            \"data\" : \"action\",
                            \"orderable\": false,
                            \"className\" : \"text-center\"
                        }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
    ";

if(!file_exists($viewsfolder . $v_list_file))
$hasil_view_list = createFile($string, $viewsfolder . $v_list_file);
?>