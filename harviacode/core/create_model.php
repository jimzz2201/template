<?php

$string = "<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class " . ucfirst($m) . " extends CI_Model
{

    public \$table = '".str_replace($prefix, "#", $table_name)."';
    public \$id = '$pk';
    public \$order = 'DESC';
    public \$kodeincrement = '10';
    public \$incrementkey = 5;";
    $column_all = array();
    $nama="";
    $kode="";
    foreach ($all as $row) {
        if ((strpos($row['column_name'], 'create') === false)&&(strpos($row['column_name'], 'deleted') === false) && (strpos($row['column_name'], 'update') === false)) {
            $column_all[] = $row['column_name'];
        }
        if(strpos($row['column_name'], 'nama')!== false)
        {
            $nama=$row['column_name'];
        }
        if(strpos($row['column_name'], 'kode')!== false)
        {
            $kode=$row['column_name'];
        }
        
        
    }    
$string.="\n\n    function __construct()
    {
        parent::__construct();
    }";
    


$columnall = implode(',', $column_all);

$string .="\n\n    // datatables
    function GetData" . $c_url . "() {
        \$this->load->library('datatables');
        \$this->datatables->select('" . $columnall . "');
        \$this->datatables->from(\$this->table);
        \$this->datatables->where(\$this->table.'.deleted_date', null);
        //add this line for join
        //\$this->datatables->join('table2', '" . $table_name . ".field = table2.field');
        \$isedit = true;
        \$isdelete = true;
        \$straction = '';
        if (\$isedit) {\n";
        if($jenis_form=="modal")
        {
            $string.="            \$straction.=anchor(\"\", 'Update', array('class' => 'btn btn-primary btn-xs', \"onclick\" => \"edit".$c_url."($1);return false;\"));";
        }
        else {
            $string.="            \$straction.=anchor(site_url('".$c_url."/edit_".$c_url."/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));";
        }
$string.="\n        }
        if (\$isdelete) {
            \$straction.=anchor(\"\", 'Delete', array('class' => 'btn btn-danger btn-xs', \"onclick\" => \"delete".$c_url."($1);return false;\"));
        }
        \$this->datatables->add_column('action', \$straction, '".$pk."');
        return \$this->datatables->generate();
    }";


$string .="\n\n    // get all
    function GetOne".$c."(\$keyword, \$type = '".$pk."') {
        \$this->db->where(\$type, \$keyword);
        \$this->db->where(\$this->table.'.deleted_date', null);
        \$".$c_url." = \$this->db->get(\$this->table)->row();
        return $".$c_url.";
    }

    function ".$c."Manipulate(\$model) {
        try {\n";
            foreach ($non_pk as $row) {
            if ((strpos($row['column_name'], 'create') === false)&&(strpos($row['column_name'], 'deleted') === false) && (strpos($row['column_name'], 'update') === false)) {
                 $int = $row['data_type'] == 'int' || $row['data_type'] == 'double' || $row['data_type'] == 'decimal' ? '|numeric' : '';
                 
                 if($int!=''&&substr($row['column_name'], 0,2)!="id")
                 {
                     $string .= "                \$model['".$row['column_name']."'] = DefaultCurrencyDatabase(\$model['".$row['column_name']."']);\n";
                 }
                 else if(substr($row['column_name'], 0,2)=="id")
                 {
                      $string .= "                \$model['".$row['column_name']."'] = ForeignKeyFromDb(\$model['".$row['column_name']."']);\n";
                 }
                 
                 if($row['data_type']=="date"||$row['datetime'])
                 {
                     $string .= "                \$model['".$row['column_name']."'] = DefaultTanggalDatabase(\$model['".$row['column_name']."']);\n";
                 }
                 
            }
            
        }
$string.="
            if (CheckEmpty(\$model['".$pk."'])) {";
                if($kode!=""&&$generate!="")
                {
                    $string.="\n                \$model['".$kode."'] = AutoIncrement(\$this->table, '".$generate."', '".$kode."',".$num.");\n";
                }

                $string.="                \$model['created_date'] = GetDateNow();
                \$model['created_by'] = ForeignKeyFromDb(GetUserId());
                \$this->db->insert(\$this->table, \$model);";
                if($jenis_form=="form")
                {
                    $string.="\n\t\tSetMessageSession(1, '".$c." successfull added into database');";
                }
                $string.="\n\t\treturn array(\"st\" => true, \"msg\" => \"".$c." successfull added into database\");
            } else {
                \$model['updated_date'] = GetDateNow();
                \$model['updated_by'] = ForeignKeyFromDb(GetUserId());
                \$this->db->update(\$this->table, \$model, array(\"".$pk."\" => \$model['".$pk."']));";
                if($jenis_form=="form")
                {
                    $string.="\n\t\tSetMessageSession(1, '".$c." has been updated');";
                }
                $string.="\n\t\treturn array(\"st\" => true, \"msg\" => \"".$c." has been updated\");
            }
        } catch (Exception \$ex) {
            return array(\"st\" => false, \"msg\" => \$ex->getMessage());
        }
    }
    
    function GetDropDown".$c."() {
        \$list".$c_url." = GetTableData(\$this->table, '".$pk."', '".($nama==""?$kode:$nama)."', array(\$this->table.'.status' => 1,\$this->table.'.deleted_date' => null));

        return \$list".$c_url.";
    }

    function ".$c."Delete(\$".$pk.") {
        try {
            \$model['deleted_date'] = GetDateNow();
            \$model['deleted_by'] = GetUserId();
            \$this->db->update(\$this->table, \$model, array('".$pk."' => \$".$pk."));
        } catch (Exception \$ex) {
            return array(\"st\" => false, \"msg\" => \"Some Error Occured\");
        }
        return array(\"st\" => true, \"msg\" => \"".$c." has been deleted from database\");
    }


}";



if(!file_exists($modelfolder . $m_file))
$hasil_model = createFile($string, $modelfolder . $m_file);
?>