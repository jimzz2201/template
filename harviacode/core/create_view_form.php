<?php
$string="";
if ($jenis_form == 'modal') {
    $string = '<div class="modal-header">
        ' . $c . ' <?php echo @$button ?>
    </div>' . "\n";
}
else {
$string = "<section class=\"content-header\">
    <h1>
        ".$c." <?= @".'$button'." ?>
        <small>".$c."</small>
    </h1>
    <ol class=\"breadcrumb\">
        <li><a href=\"<?php echo base_url() ?>\"><i class=\"fa fa-dashboard\"></i>Home</a></li>
        <li>".$c."</li>
        <li class=\"active\">".$c." <?= @".'$button'." ?></li>
    </ol>
</section>\n"; 
$string .="<section class=\"content\">
    <div class=\"box box-default\">
        <div class=\"box-body\">
            <div id=\"notification\" ></div>
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"panel\" data-collapsed=\"0\">
                        <div class=\"panel-body\">";
}
if ($jenis_form == 'modal') {
    $string.='<div class="modal-body">' . "\n";
}
$string.='<form id="frm_' . $c_url . '" class="form-horizontal form-groups-bordered validate" method="post">' . "\n";
$string .= "\t<input type=\"hidden\" name=\"" . $pk . "\" value=\"<?php echo @$" . $pk . "; ?>\" /> ";
$isdatepicker=false;
foreach ($non_pk as $row) {
    if ((strpos($row['column_name'], 'create') === false)&&(strpos($row['column_name'], 'deleted') === false) && (strpos($row['column_name'], 'update') === false)) {
        if($row["column_name"]=="status")
        {
            $string .="\n\t<div class=\"form-group\">";
            $string .="\n\t\t<?= form_label('" . label($row["column_name"]) . "', \"txt_" . $row["column_name"] . "\", array(\"class\" => 'col-sm-4 control-label')); ?>";
            $string .="\n\t\t<div class=\"col-sm-8\">";
            $string .="\n\t\t\t<?= form_dropdown(array(\"selected\" => @$".$row["column_name"].", \"name\" => \"".$row["column_name"]."\"), array('1' => 'Active', '0' => 'Not Active'), @$".$row["column_name"].", array('class' => 'form-control', 'id' => '".$row["column_name"]."')); ?>";
            $string .="\n\t\t</div>";
            $string .="\n\t</div>";
        }
        else if((strpos($row['column_name'], 'kode') !== false))
        {
            $string .="\n\t<div class=\"form-group\">";
            $string .="\n\t\t<?= form_label('" . label($row["column_name"]) . "', \"txt_" . $row["column_name"] . "\", array(\"class\" => 'col-sm-4 control-label')); ?>";
            $string .="\n\t\t<div class=\"col-sm-8\">";
           
            $string.="\n\t\t\t<?php \$mergearray=array();
                            if(!CheckEmpty(@$".$pk."))
                            {
                                \$mergearray['disabled']=\"disabled\";
                            }
                            else
                            {
                                \$mergearray['name'] = \"".$kode."\";
                            }?>
                    <?= form_input(array_merge(\$mergearray,array('type' => 'text', 'value' => @$".$kode.", 'class' => 'form-control', 'id' => 'txt_".$kode."', 'placeholder' => '".label($row["column_name"])."'))); ?>";
            $string .="\n\t\t</div>";
            $string .="\n\t</div>";
            
        }
        else {
            
        if ($row["data_type"] == 'varchar') {
            $string .="\n\t<div class=\"form-group\">";
            $string .="\n\t\t<?= form_label('" . label($row["column_name"]) . "', \"txt_" . $row["column_name"] . "\", array(\"class\" => 'col-sm-4 control-label')); ?>";
            $string .="\n\t\t<div class=\"col-sm-8\">";
            $string .="\n\t\t\t<?= form_input(array('type' => 'text', 'name' => '" . $row["column_name"] . "', 'value' => @$" . $row["column_name"] . ", 'class' => 'form-control', 'id' => 'txt_" . $row["column_name"] . "', 'placeholder' => '" . label($row["column_name"]) . "')); ?>";
            $string .="\n\t\t</div>";
            $string .="\n\t</div>";
        } else if ($row["data_type"] == 'decimal') {
            $string .="\n\t<div class=\"form-group\">";
            $string .="\n\t\t<?= form_label('" . label($row["column_name"]) . "', \"txt_" . $row["column_name"] . "\", array(\"class\" => 'col-sm-4 control-label')); ?>";
            $string .="\n\t\t<div class=\"col-sm-8\">";
            $string .="\n\t\t\t<?= form_input(array('type' => 'text', 'name' => '" . $row["column_name"] . "', 'value' => DefaultCurrency(@$" . $row["column_name"] . "), 'class' => 'form-control', 'id' => 'txt_" . $row["column_name"] . "', 'placeholder' => '" . label($row["column_name"]) . "' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>";
            $string .="\n\t\t</div>";
            $string .="\n\t</div>";
        }
        else if ($row["data_type"] == 'tinyint') {
            $string .="\n\t<div class=\"form-group\">";
            $string .="\n\t\t<?= form_label('" . label($row["column_name"]) . "', \"txt_" . $row["column_name"] . "\", array(\"class\" => 'col-sm-4 control-label')); ?>";
            $string .="\n\t\t<div class=\"col-sm-8\">";
            $string .="\n\t\t\t<?= form_dropdown(array(\"selected\" => @$".$row["column_name"].", \"name\" => \"".$row["column_name"]."\"), array('1' => 'Yes', '0' => 'No'), @$".$row["column_name"].", array('class' => 'form-control', 'id' => '".$row["column_name"]."')); ?>";
            $string .="\n\t\t</div>";
            $string .="\n\t</div>";
        }
        else if ($row["data_type"] == 'datetime'||$row["data_type"]=="date"){
            $string .="\n\t<div class=\"form-group\">";
            $string .="\n\t\t<?= form_label('" . label($row["column_name"]) . "', \"txt_" . $row["column_name"] . "\", array(\"class\" => 'col-sm-4 control-label')); ?>";
            $string .="\n\t\t<div class=\"col-sm-8\">";
            $string .="\n\t\t\t<?= form_input(array('type' => 'text', 'name' => '" . $row["column_name"] . "', 'value' => DefaultDatePicker(@$" . $row["column_name"] . "), 'class' => 'form-control datepicker', 'id' => 'txt_" . $row["column_name"] . "', 'placeholder' => '" . label($row["column_name"]) . "')); ?>";
            $string .="\n\t\t</div>";
            $string .="\n\t</div>";
            $isdatepicker=true;
        }
        else {
            $string .="\n\t<div class=\"form-group\">";
            $string .="\n\t\t<?= form_label('" . label($row["column_name"]) . "', \"txt_" . $row["column_name"] . "\", array(\"class\" => 'col-sm-4 control-label')); ?>";
            $string .="\n\t\t<div class=\"col-sm-8\">";
            $string .="\n\t\t\t<?= form_input(array('type' => 'text', 'name' => '" . $row["column_name"] . "', 'value' => @$" . $row["column_name"] . ", 'class' => 'form-control', 'id' => 'txt_" . $row["column_name"] . "', 'placeholder' => '" . label($row["column_name"]) . "')); ?>";
            $string .="\n\t\t</div>";
            $string .="\n\t</div>";
        }
        }
    }
}
if ($jenis_form == 'modal') {
    $string.="\n\t<div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" >Cancel</button>
        <button type=\"submit\" class=\"btn btn-primary\" id=\"btt_modal_ok\" >Save</button>
    </div>\n";
}
else
{
    $string.="\n\t<div class=\"form-group\">
                                    <a href=\"<?php echo base_url().'index.php/".$c_url."' ?>\" class=\"btn btn-default\"  >Cancel</a>
                                    <button type=\"submit\" class=\"btn btn-primary\" id=\"btt_modal_ok\" >Save</button>
\t</div>";
}


$string.="\n" . '</form>' . "\n";

if ($jenis_form == 'modal') {

    $string.='</div>' . "\n";
}
 else {
    $string.="                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>";
}
$string.="<script>\n";
if($isdatepicker)
{
  $string.="$(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });
    });" ; 
}
$string.="$(\"#frm_".$c_url."\").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/".$c_url."/".$c_url."_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {\n";
if ($jenis_form != 'modal') {
$string.="                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/".$c_url."';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }\n";
}
 else {
 $string.="                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $(\"#modalbootstrap\").modal(\"hide\");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }\n";   
}
$string.="        });
        return false;

    })";
$string.="\n</script>";
if(!file_exists($viewsfolder . $v_form_file))
$hasil_view_form = createFile($string, $viewsfolder . $v_form_file);
?>











<?php

/*
  $string = "<!doctype html>
  <html>
  <head>
  <title>harviacode.com - codeigniter crud generator</title>
  <link rel=\"stylesheet\" href=\"<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>\"/>
  <style>
  body{
  padding: 15px;
  }
  </style>
  </head>
  <body>
  <h2 style=\"margin-top:0px\">".ucfirst($table_name)." <?php echo \$button ?></h2>
  <form action=\"<?php echo \$action; ?>\" method=\"post\">";
  foreach ($non_pk as $row) {
  if ($row["data_type"] == 'text')
  {
  $string .= "\n\t    <div class=\"form-group\">
  <label for=\"".$row["column_name"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
  <textarea class=\"form-control\" rows=\"3\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\"><?php echo $".$row["column_name"]."; ?></textarea>
  </div>";
  } else
  {
  $string .= "\n\t    <div class=\"form-group\">
  <label for=\"".$row["data_type"]."\">".label($row["column_name"])." <?php echo form_error('".$row["column_name"]."') ?></label>
  <input type=\"text\" class=\"form-control\" name=\"".$row["column_name"]."\" id=\"".$row["column_name"]."\" placeholder=\"".label($row["column_name"])."\" value=\"<?php echo $".$row["column_name"]."; ?>\" />
  </div>";
  }
  }
  $string .= "\n\t    <input type=\"hidden\" name=\"".$pk."\" value=\"<?php echo $".$pk."; ?>\" /> ";
  $string .= "\n\t    <button type=\"submit\" class=\"btn btn-primary\"><?php echo \$button ?></button> ";
  $string .= "\n\t    <a href=\"<?php echo site_url('".$c_url."') ?>\" class=\"btn btn-default\">Cancel</a>";
  $string .= "\n\t</form>
  </body>
  </html>";


 */
?>