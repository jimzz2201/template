<?php
$m=strtolower($m);
$string = "<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class " . $c . " extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        \$this->load->model('$m');";

        
$string .= "
    }";
$string .="\n\n    public function index()
    {
        \$javascript = array();
        \$javascript[] = \"assets/plugins/datatables/jquery.dataTables.js\";
        \$javascript[] = \"assets/plugins/datatables/dataTables.bootstrap.js\";
        \$module = \"".$modulekode."\";
        \$header = \"".$hc->get_parent($modulekode,$prefix)."\";
        \$title = \"". ucfirst(label(str_replace($prefix."_", "", $table_name))) ."\";
        \$model=array(\"title\" => \$title, \"form\" => \$header, \"formsubmenu\" => \$module);\n";
        if($modulekode!="")
        {
            $string.="\tCekModule(\$module);\n";   
        }
        
        $string.="\tLoadTemplate(\$model, \"$c_url/$v_list\", \$javascript);
        
    } 
    public function get_one_".$c_url."() {
        \$model = \$this->input->post();
        \$this->form_validation->set_rules('".$pk."', '".ucfirst(label(str_replace($prefix."_", "", $table_name)))."', 'trim|required');
        \$message = \"\";
        if (\$this->form_validation->run() === FALSE || \$message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . \$message));
        } else {
            \$data['obj'] = \$this->".$m."->GetOne".$c."(\$model[\"".$pk."\"]);
            \$data['st'] = \$data['obj'] != null;
            \$data['msg'] = !\$data['st'] ? \"Data ".ucfirst(label(str_replace($prefix."_", "", $table_name)))." tidak ditemukan dalam database\" : \"\";
            echo json_encode(\$data);
        }
    }
    public function getdata".$c_url."() {
        header('Content-Type: application/json');
        echo \$this->" . $m . "->GetData".$c_url."();
    }
    
    public function create_".$c_url."() 
    {
        \$row=['button'=>'Add'];
        \$javascript = array();\n";
        $string.="\t\$module = \"".($modulecreate==""?$modulekode:$modulecreate)."\";
        \$header = \"".$hc->get_parent($modulekode,$prefix)."\";
        \$row['title'] = \"Add ". ucfirst(label(str_replace($prefix."_", "", $table_name))) ."\";
        CekModule(\$module);
        \$row['form'] = \$header;
        \$row['formsubmenu'] = \$module;";
        if($jenis_form=="modal"){
            $string.="        \n\t    \$this->load->view('$c_url/$v_form', \$row);";
        }
        else {
            $string.="        \n\tLoadTemplate(\$row,'$c_url/$v_form', \$javascript);";
        }
$string.="       
    }
    
    public function ".$c_url."_manipulate() 
    {
        \$message='';\n".$kode;
        foreach ($non_pk as $row) {
            if ((strpos($row['column_name'], 'create') === false)&&(strpos($row['column_name'], 'deleted') === false)&&(strpos($row['column_name'], 'status') === false)&&($row['column_name']!==$kode)&&(strpos($row['column_name'], 'status')==false) && (strpos($row['column_name'], 'update') === false)) {
            $int = $row3['data_type'] == 'int' || $row['data_type'] == 'double' || $row['data_type'] == 'decimal' ? '|numeric' : '';
            $string .= "\n\t\$this->form_validation->set_rules('".$row['column_name']."', '".  ucfirst(label($row['column_name']))."', 'trim|required$int');";
            }
            
        }
        if($kode!=""&&$generate=="")
        {
            $string .= "if (CheckEmpty(@\$model['".$pk."'])) {
                \$this->form_validation->set_rules('".$kode."', '".ucfirst(label(str_replace($prefix."_", "", $table_name)))."', 'trim|required|is_unique[".$table_name.".".$kode."]');
            }";
        }
$string.="\n        \$model=\$this->input->post();
         if (\$this->form_validation->run() === FALSE || \$message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . \$message));
        } else {
            \$status=\$this->m_".$c_url."->".$c."Manipulate(\$model);
            echo json_encode(\$status);
        }
    }
    
    public function edit_".$c_url."(\$id=0) 
    {
        \$row = \$this->m_".$c_url."->GetOne".$c."(\$id);
        
        if (\$row) {
            \$row->button='Update';
            \$row = json_decode(json_encode(\$row), true);
            \$javascript = array();\n";
 $string.="\t    \$module = \"".($modulemanipulate==""?$modulekode:$modulemanipulate)."\";
            \$header = \"".$hc->get_parent($modulekode,$prefix)."\";
            \$row['title'] = \"Edit ". ucfirst(label(str_replace($prefix."_", "", $table_name))) ."\";
            CekModule(\$module);
            \$row['form'] = \$header;
            \$row['formsubmenu'] = \$module;";
if($jenis_form=="modal"){
            $string.="        \n\t    \$this->load->view('$c_url/$v_form', \$row);";
        }
        else {
            $string.="        \n\t    LoadTemplate(\$row,'$c_url/$v_form', \$javascript);";
        }
$string.="
        } else {
            SetMessageSession(0, \"".$c." cannot be found in database\");
            redirect(site_url('".$c_url."'));
        }
    }
    
    public function ".$c_url."_delete() 
    {
        \$message='';
        \$this->form_validation->set_rules('".$pk."', '".$c."', 'required');
        if (\$this->form_validation->run() == FALSE||\$message!='') {
            \$result=array();
            \$result['st']=false;
            \$result['msg']='Error :<br/>' . validation_errors() . \$message;
        }
        else {
            \$model=\$this->input->post();
            \$result=\$this->".$m."->".$c."Delete(\$model['".$pk."']);
        }
        
        echo json_encode(\$result);
        
    }";

$string .= "\n\n}\n\n/* End of file $c_file */";



if(!file_exists($controllerfolder . $c_file))
$hasil_controller = createFile($string,$controllerfolder. $c_file);

?>