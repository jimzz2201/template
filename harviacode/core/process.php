<?php

$hasil = array();

if (isset($_POST['generate']))
{
    // get form data
    $table_name = safe($_POST['table_name']);
    $jenis_tabel = safe($_POST['jenis_tabel']);
    $export_excel = safe($_POST['export_excel']);
    $export_word = safe($_POST['export_word']);
    $export_pdf = safe($_POST['export_pdf']);
    $controller = safe($_POST['controller']);
    $jenis_form = safe($_POST['jenis_form']);
    $prefix = safe($_POST['prefix']);
    $model = safe($_POST['model']);
    $modulekode = safe($_POST['modulekode']);
    $modulecreate = safe($_POST['modulecreate']);
    $modulemanipulate = safe($_POST['modulemanipulate']);
    $generate = safe($_POST['modulegenerate']);
    $num = safe($_POST['num']);
    if ($table_name <> '')
    {
        // set data
        
        $table_name = $table_name;
        $c = $controller <> '' ? ucfirst($controller) : ucfirst($table_name);
        $c_url = strtolower($c);
        $m = $model <> '' ? ucfirst($model) : ucfirst($table_name) . '_model';
        $v_list = 'v_'.$c_url . "_index";
        $v_read = $c_url . "_read";
        $v_form = 'v_'.$c_url . "_manipulate";
        $v_doc = $c_url . "_doc";
        $v_pdf = $c_url . "_pdf";

        // url
        

        // filename
        $c_file = $c . '.php';
        $m_file = $m . '.php';
        $v_list_file = $v_list . '.php';
        $v_read_file = $v_read . '.php';
        $v_form_file = $v_form . '.php';
        $v_doc_file = $v_doc . '.php';
        $v_pdf_file = $v_pdf . '.php';

        // read setting
        $get_setting = readJSON('core/settingjson.cfg');
        $target = $get_setting->target;

        $viewsfolder=$target.'modules/'.$c_url . "/views/";
        $controllerfolder=$target.'modules/'.$c_url . "/controllers/";
        $modelfolder=$target.'modules/'.$c_url . "/models/";
        if (!file_exists($viewsfolder ))
        {
            mkdir($viewsfolder, 0777, true);
        }
        if (!file_exists($controllerfolder ))
        {
            mkdir($controllerfolder, 0777, true);
        }
        if (!file_exists($modelfolder ))
        {
            mkdir($modelfolder, 0777, true);
        }
        $pk = $hc->primary_field($table_name);
        $non_pk = $hc->not_primary_field($table_name);
        $all = $hc->all_field($table_name);

        // generate
        include 'core/create_config_pagination.php';
        include 'core/create_controller.php';
        include 'core/create_model.php';
        include 'core/create_view_list_datatables.php';
        include 'core/create_view_form.php';

        $hasil[] = $hasil_controller;
        $hasil[] = $hasil_model;
        $hasil[] = $hasil_view_list;
        $hasil[] = $hasil_view_form;
        $hasil[] = $hasil_view_read;
        $hasil[] = $hasil_view_doc;
        $hasil[] = $hasil_view_pdf;
        $hasil[] = $hasil_config_pagination;
        $hasil[] = $hasil_exportexcel;
        $hasil[] = $hasil_pdf;
    } else
    {
        $hasil[] = 'No table selected.';
    }
}

?>