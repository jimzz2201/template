const tokenDivId = 'token_div';
const permissionDivId = 'permission_div';
var device_token;

var config = {
    apiKey: "AIzaSyBC9xSRns1rmk-99Vzs65PrLaQvP01ygFU",
    authDomain: "eticket-7ed99.firebaseapp.com",
    databaseURL: "https://eticket-7ed99.firebaseio.com",
    projectId: "eticket-7ed99",
    storageBucket: "eticket-7ed99.appspot.com",
    messagingSenderId: "604435549833"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.usePublicVapidKey("BCJ2VGr7X0WtQhnjVcE8KJliBnrCiYno-3T8HnlSQO779HBAZINJkufzeW1rhVBE2d86csmlqdUaY5Fprq3a9zI");

messaging.requestPermission().then(function() {
    console.log('Notification permission granted.');
    if(isTokenSentToServer()){
        console.log("Token already saved.");
    }else{
        getRegToken();
    }
}).catch(function(err) {
    console.log('Unable to get permission to notify.', err);
});

function getRegToken() {
    messaging.getToken().then(function(currentToken) {
        if (currentToken) {
            device_token = currentToken;
            sendTokenToServer();
            updateUIForPushEnabled(currentToken);
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            updateUIForPushPermissionRequired();
            setTokenSentToServer(false);
        }
    }).catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
        showToken('Error retrieving Instance ID token. ', err);
        alert(err.toString());
        setTokenSentToServer(false);
    });
}

// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function() {
    messaging.getToken().then(function(refreshedToken) {
        console.log('Token refreshed.');
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        setTokenSentToServer(false);
        // Send Instance ID token to app server.
        device_token = refreshedToken;
        sendTokenToServer();
        resetUI();
    }).catch(function(err) {
        console.log('Unable to retrieve refreshed token ', err);
        showToken('Unable to retrieve refreshed token ', err);
    });
});

messaging.onMessage(function(payload) {
    console.log('Message received. ', payload);
    // appendMessage(payload);
    if(payload.data.lat != "undefined" && payload.data.lng != "undefined"){
        createMapMarker(payload.data);
    }
});

function resetUI() {
    clearMessages();
    showToken('loading...');
    // [START get_token]
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken().then(function(currentToken) {
        if (currentToken) {
            device_token = currentToken;
            sendTokenToServer();
            updateUIForPushEnabled(currentToken);
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            updateUIForPushPermissionRequired();
            setTokenSentToServer(false);
        }
    }).catch(function(err) {
        console.log('An error occurred while retrieving token. ', err);
        showToken('Error retrieving Instance ID token. ', err);
        setTokenSentToServer(false);
    });
    // [END get_token]
}

function showToken(currentToken) {
    // Show token in console and UI.
    var tokenElement = document.querySelector('#token');
    tokenElement.textContent = currentToken;
}

function sendTokenToServer(force=false){
    if(!isTokenSentToServer() || force == true){
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/livemaps/subscribe',
            dataType: 'json',
            data: {token : device_token},
            success: function (data) {
                console.log("token sent: "+device_token);
                setTokenSentToServer(true);
            },
            error: function (xhr, status, error) {
                console.log(xhr.responseText);
            }
        });
    }
}

function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') === '1';
}
function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}

function showHideDiv(divId, show) {
    const div = document.querySelector('#' + divId);
    if (show) {
        div.style = 'display: visible';
    } else {
        div.style = 'display: none';
    }
}
function requestPermission() {
    console.log('Requesting permission...');
    // [START request_permission]
    messaging.requestPermission().then(function() {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // [START_EXCLUDE]
        // In many cases once an app has been granted notification permission, it
        // should update its UI reflecting this.
        resetUI();
        // [END_EXCLUDE]
    }).catch(function(err) {
        console.log('Unable to get permission to notify.', err);
    });
    // [END request_permission]
}

function deleteToken() {
    // Delete Instance ID token.
    // [START delete_token]
    messaging.getToken().then(function(currentToken) {
        messaging.deleteToken(currentToken).then(function() {
            console.log('Token deleted.');
            setTokenSentToServer(false);
            // [START_EXCLUDE]
            // Once token is deleted update UI.
            resetUI();
            // [END_EXCLUDE]
        }).catch(function(err) {
            console.log('Unable to delete token. ', err);
        });
        // [END delete_token]
    }).catch(function(err) {
        console.log('Error retrieving Instance ID token. ', err);
        showToken('Error retrieving Instance ID token. ', err);
    });
}

function appendMessage(payload) {
    const messagesElement = document.querySelector('#messages');
    const dataHeaderELement = document.createElement('h5');
    const dataElement = document.createElement('pre');
    dataElement.style = 'overflow-x:hidden;';
    dataHeaderELement.textContent = 'Received message:';
    dataElement.textContent = JSON.stringify(payload, null, 2);
    messagesElement.appendChild(dataHeaderELement);
    messagesElement.appendChild(dataElement);

    if(typeof payload.data.lat != "undefined" && typeof payload.data.lng != "undefined"){
        createMapMarker(payload.data);
    }

    // var notification = new Notification(payload.data.title, {body:payload.data.body});
}

// Clear the messages element of all children.
function clearMessages() {
    const messagesElement = document.querySelector('#messages');
    while (messagesElement.hasChildNodes()) {
        messagesElement.removeChild(messagesElement.lastChild);
    }
}
function updateUIForPushEnabled(currentToken) {
    showHideDiv(tokenDivId, false);
    showHideDiv(permissionDivId, false);
    showToken(currentToken);
}
function updateUIForPushPermissionRequired() {
    showHideDiv(tokenDivId, false);
    showHideDiv(permissionDivId, true);
}

resetUI();