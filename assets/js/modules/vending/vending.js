var table;
var maketReloader;
var bookingBerhasil;
var kursi = [];
var urlPemesanan = baseurl + 'index.php/vending/';
var urlAsli = baseurl + 'index.php/keberangkatan/pemesanan/';

$(document).ready(function () {
    launchIntoFullscreen(document.documentElement);
    $("#search_pangkalan_tujuan").select2({data:list_pangkalan});

    $('#search_tanggal').Zebra_DatePicker({
        format: 'd M Y',
        direction: [true, 4],
        icon_position: "right",
        days : ['Minggu', 'Sn', 'Sl', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
        show_clear_date: false,
        strict: true,
    });

    $("form#form_search").submit(function (e) {
        resetLah();
        table.api().columns.adjust();
        LoadBar.show();
        table.fnDraw(true);
        return false;
    });

    /*$("#id_pangkalan_asal, #id_pangkalan_tujuan").on("select2:select", function(e){
        setHarga();
    });*/

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    table = $("#mytable").dataTable({
        oLanguage: {
            sProcessing: "loading..."
        },
        paginate: false,
        sDom: 't',
        processing: true,
        serverSide: true,
        ajax: {"url": urlPemesanan + "grid", "type": "POST",
            "data": function (d) {
                return $.extend({}, d, {
                    "extra_search": $("form#form_search").serialize()
                });
            }},
        // deferRender:    true,
        // scrollY:        400,
        // scrollCollapse: true,
        // scroller:       true,
        columns: [
            // {data: "kode_berangkat", orderable: false, title: "Kode Berangkat"}
            {
                data: "action",
                orderable: false,
                className: "normal-padding",
                width: "50px",
            }
            , {data: "jurusan", orderable: false, title: "Jurusan", className:"normal-padding"}
            , {
                data: "tanggal", orderable: false, title: "Tgl Brkt", className:"normal-padding", width: "70px",
                mRender: function (data, type, row) {
                    return DefaultDateFormat(data);
                }
            }
            , {data: "jam", orderable: false, title: "Jam Brkt", className:"normal-padding", width: "40px",
                mRender: function (data, type, row) {
                    var jam = '';
                    if(data != "00:00:00"){
                        var value = data.split(":");
                        jam = value[0]+":"+value[1];
                    }
                    return jam;
                }
            }
            , {data: "no_polisi", orderable: false, title: "No Pol", className:"normal-padding", width: "100px"}
            , {data: "nama_tipe", orderable: false, title: "PO", className:"normal-padding", width: "80px"}
            , {data: "nama_kelas", orderable: false, title: "Kelas", className:"normal-padding text-center", width: "100px"}
            , {data: "sisa_kursi", orderable: false, title: "Sisa", className:"normal-padding text-center", width: "40px"}
            , {data: "naik", orderable: false, title: "Naik", className:"normal-padding", width: "100px"}
            , {
                data: "tanggal_naik", orderable: false, title: "Tgl Naik", className:"normal-padding", width: "70px",
                mRender: function (data, type, row) {
                    return DefaultDateFormat(data);
                }
            }
            , {data: "jam_naik", orderable: false, title: "Jam Naik", className:"normal-padding", width: "40px",
                mRender: function (data, type, row) {
                    var jam = '';
                    if(row['jam'] != "00:00:00"){
                        var value = data.split(":");
                        jam = value[0]+":"+value[1];
                    }
                    return jam;
                }
            }
        ],
        order: [[10,'asc']],
        // rowCallback: function(row, data, iDisplayIndex) {
         // var info = this.fnPagingInfo();
         // var page = info.iPage;
         // var length = info.iLength;
         // var index = page * length + (iDisplayIndex + 1);
         // index += '<input type="hidden" value="'+data.id_berangkat+'"/>';
         // $('td:eq(0)', row).html(index);


         // }
        drawCallback: function(settings ){
            $(document).ajaxComplete(function () {
                LoadBar.hide();
            });
        }

    });

    $(".btn-step-1").unbind('click').click(function () {
        $("#smartwizard").smartWizard("goToStep", 1);
    });

    $(".btn-step-2").unbind('click').click(function () {
        $("#smartwizard").smartWizard("goToStep", 2);
    });

    $(".btn-step-3").unbind('click').click(function () {
        reloadMaket();
        clearInterval(maketReloader);
        maketReloader = setInterval(reloadMaket, 10000);
        $("#smartwizard").smartWizard("goToStep", 3);
    });

    if(ticketingLayout == 1){
        if($("#layout-maket").attr("data-berangkat") == "" || $("#layout-maket").attr("data-berangkat") == null){
            $("#smartwizard").smartWizard("goToStep", 1);
        }
    }

    $("#tiketModal .btn-close").click(function(){
        reloadMaket();
        clearInterval(maketReloader);
        maketReloader = setInterval(reloadMaket, 10000);
    });

});

function ubahFormPesan() {
    if(kursi.length > 1){
        console.log($("#rombongan").is(':checked'));
        if($("#rombongan").is(':checked')){
            for(x=2;x<=kursi.length;x++){
                $("div#sub_pesan_form_"+x+" input").removeAttr('required');
                $("div#sub_pesan_form_"+x).hide();
            }
            $(".kursi_pesanan").hide();
            $(".kursi_pesanan_satuan").hide();
            $(".kursi_pesanan_semua").show();
        }else{
            for(x=2;x<=kursi.length;x++) {
                $("div#sub_pesan_form_" + x + " input").attr('required', 'required');
                $("div#sub_pesan_form_" + x + " input.frm_pesan-turun").removeAttr('required');
                $("div#sub_pesan_form_" + x).show();
            }
            $(".kursi_pesanan").show();
            $(".kursi_pesanan_satuan").show();
            $(".kursi_pesanan_semua").hide();
        }
    }
}

function selectKeberangkatan(id_berangkat) {
    LoadBar.show();
    $("#booking-detail").html('');
    kursi = [];
    $.post(
        // urlPemesanan+'get_keberangkatan',
        baseurl + 'index.php/keberangkatan/pemesanan/get_keberangkatan',
        {id_berangkat:id_berangkat},
        function(data){
            $("#id_pangkalan_asal").val($("#search_pangkalan_asal").val());
            $("#id_pangkalan_tujuan").val($("#search_pangkalan_tujuan").val());

            $("#harga_tiket").val(data.harga);
            $("#id_kelas").val(data.id_kelas);
            $("#tipe_bus").val(data.tipe_bus);
            var teks = 'Keberangkatan : '+data.kode_berangkat+'<br/>'+data.pangkalan_asal+' - '+data.pangkalan_tujuan+' - '+data.nama_kelas+' - '+data.tipe_bus+'<br/>Tanggal : '+data.tanggal;
            if(data.jam != "00:00"){
                teks += ', Jam : '+data.jam;
            }
            $("#jadwal_berangkat").html(teks);
            // if(data.status < 2 && data.available == 1){
                $("#btn-pesan").show();
            // }else{
            //     $("#btn-pesan").hide();
            // }

            $("#layout-maket").attr('data-berangkat', id_berangkat);
            $("#layout-maket").load(urlAsli + "view_maket/"+data.id_berangkat + "/" + $("#id_pangkalan_asal").val(), function(){
                $("#box-print-tiket").hide();
                setHarga(id_berangkat);
                clearInterval(maketReloader);
                maketReloader = setInterval(reloadMaket, 10000);

                if($("#smartwizard").length){
                    $('#smartwizard').smartWizard("next");
                }
                LoadBar.hide();
            });
        },"json"
    );
}

function pesanKursi(el){
    if($(el).hasClass('btn-selected')){
        var idx = kursi.indexOf($(el).data('kursi'));
        if(idx > -1){
            kursi.splice(idx, 1);
        }
        $(el).removeClass('btn-selected');
    }else{
        if(kursi.length < 2){
            $(el).addClass('btn-selected');
            kursi.push($(el).data('kursi'));
        }
    }
    updateBookingDetail();
}

function updateBookingDetail() {
    if(kursi.length > 0){
        var totalBookingan = kursi.length * $("#harga_tiket").val();
        totalBookingan = Comma(totalBookingan).replace(/,/g, ".");
        var bookingDetail = "Anda memilih kursi No : " + kursi.join(" & ") + "<br/>Total : Rp." + totalBookingan;
        $("#booking-detail").html(bookingDetail);
    }else{
        $("#booking-detail").html('');
    }
}

function pesanTiket(){
    var id_berangkat = $("#layout-maket").data("berangkat");
    var id_berangkat = $("#layout-maket").attr("data-berangkat");

    var harga = parseFloat($("#harga_tiket").val());
    $('#frm_pesan').trigger('reset');
    $('div.alert').remove();
    if(id_berangkat){
        if(kursi.length){
            if(harga > 0 && harga != "" && harga != null && !isNaN(harga)){
                // clearInterval(maketReloader);
                $.post(
                    urlAsli + 'form_pemesanan',
                    {jml:kursi.length, kursi:kursi, id_berangkat:id_berangkat},
                    function(data){
                        $("form#frm_pesan").empty().html(data);
                        if(ticketingLayout){
                            $('#smartwizard').smartWizard("next");
                        }else{
                            $("#tiketModal").modal('show');
                        }

                        if($("#nihil-kursi").length){
                            reloadMaket();
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);
                        }

                        $('html, body').animate({scrollTop: 0}, 'slow');
                    }
                );
            }else{
                $("#harga_tiket").focus().select();
                alert("Harga belum ditentukan!");
            }
        }else{
            alert("Silahkan pilih kursi!");
        }
    }else{
        alert("Silahkan pilih keberangkatan terlebih dahulu!");
    }
}

function confirmTiket(){
    var id_berangkat = $("#layout-maket").data("berangkat");
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    $('#smartwizard').smartWizard({
        disabledSteps: [1,2,3]
    });
    // var seat = kursi.join();
    var asal = $("#id_pangkalan_asal").val();
    var tujuan = $("#id_pangkalan_tujuan").val();
    var harga = $("#harga_tiket").val();
    var timestamp = new Date().getTime();
    var addition = "&asal="+asal+"&tujuan="+tujuan+"&harga="+harga+"&timestamp="+timestamp;

    if(id_berangkat){
        if(kursi.length > 0 && kursi.length <= 2){
            if(checkForm("frm_pesan")){
                $.ajax({
                    type : 'POST',
                    dataType : 'json',
                    url : urlPemesanan + 'pesan_tiket/'+id_berangkat,
                    data : $("#frm_pesan").serialize()+addition,
                    success : function(data){
                        if (data.st){
                            $("#tiketModal").modal("hide");
                            clearInterval(maketReloader);
                            messagesuccess(data.msg);

                            $("#print_tiket").empty().html(data.tiket);
                            if($("#smartwizard").length){
                                $("#smartwizard").smartWizard("next");
                            }

                            $("#booking-result").empty().load(baseurl + "index.php/vending/booking_token/"+data.kode_order+"/1");
                            printSomething(data.url, null,data.title, true, false, 400);
                            bookingBerhasil = setInterval(function () {
                                resetLah();
                                $('#smartwizard').smartWizard("reset");
                            }, 5000);
                        }else{
                            $('div.alert').remove();
                            if(ticketingLayout == 1){
                                var el = $("#frm_pesan");
                                addAlert(el, "danger", data.msg);
                            }else{
                                modaldialogerror(data.msg);
                            }
                            reloadMaket();
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);
                        }
                    },
                    error : function (xhr, status, error) {
                        $('div.alert').remove();
                        if(ticketingLayout == 1){
                            var el = $("#frm_pesan");
                            addAlert(el, "danger", xhr.responseText);
                        }else{
                            modaldialogerror(xhr.responseText);
                        }
                        reloadMaket();
                        clearInterval(maketReloader);
                        maketReloader = setInterval(reloadMaket, 10000);
                    }
                });
            }
        }else{
            alert("Silahkan pilih kursi!");
        }
    }else{
        alert("Silahkan pilih keberangkatan terlebih dahulu!");
    }
}

function reloadMaket() {
    // kursi = [];
    // $('#frm_pesan').trigger('reset');
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var link_maket = urlAsli + "view_maket/" + id_berangkat + "/" + $("#id_pangkalan_asal").val();
    $("#layout-maket").load(link_maket, {kursi: kursi}, function () {
        var terjual = $("#terjual").val();
        var status_penjualan = $("#status_penjualan").val();
        if(status_penjualan == "1"){
            $("#btn-pesan").show();
        }else{
            $("#btn-pesan").hide();
        }
        if(terjual){
            terjual = terjual.split(",");
        }
        for(var s=0;s < kursi.length;s++){
            $('.btn-kursi[data-kursi="'+ kursi[s] +'"]').addClass('btn-selected');
        }
        for(var s=0;s < terjual.length;s++){
            var idx = jQuery.inArray(parseInt(terjual[s]), kursi);
            if(idx > -1){
                $('.btn-kursi[data-kursi="'+ terjual[s] +'"]').removeClass('btn-selected');
                kursi.splice(idx, 1);

            }
        }
    });
}

function checkForm(id_form){
    var isValid = true;
    $('#'+id_form+' input').filter('[required]').each(function() {
        if ($(this).val() === '') {
            isValid = false;
            $(this).select().focus();
            console.log(this);
            return false;
        }
        console.log(isValid);
    });
    return isValid;
}

function setHarga(id_berangkat=0) {
    if(!id_berangkat){
        id_berangkat = $("#layout-maket").data('berangkat');
    }
    var dataAsal = $("#search_pangkalan_asal").select2('data')[0];
    var dataTujuan = $("#search_pangkalan_tujuan").select2('data')[0];
    $.post(
        urlAsli + 'get_tarif',
        {id_pangkalan_asal:$("#id_pangkalan_asal").val(), id_pangkalan_tujuan:$("#id_pangkalan_tujuan").val(), id_kelas:$("#id_kelas").val(), id_berangkat:id_berangkat},
        function(data){
            var harga = data.harga;
            $("#harga_tiket_teks").html("@ Rp."+Comma(harga).replace(",", "."));
            $("#harga_tiket").val(data.harga);

            if(data.tanggal_naik && data.jam_naik){
                var teks = '<p id="detail-naik" style="border-top:solid 1px #00733E; margin-top:10px; font-size: 16px;">Naik : '+dataAsal.text+'<br/>Tanggal Naik : '+data.tanggal_naik+'<br/>';

                if(data.jam_naik != "00:00"){
                    teks += 'Jam Naik : ' + data.jam_naik;
                }
                teks += '</p>';
                $("#detail-naik").remove();
                $("#jadwal_berangkat").append(teks);
            }
        }, "json"
    );
}

function resetLah() {
    $("#layout-maket").empty();
    $("#btn-pesan").hide();
    $("#btn-manifest, #btn-sp").hide();
    $("#jadwal_berangkat").html('Keberangkatan');
    $("#harga_tiket").val('');
    $("#id_pangkalan_asal, #id_pangkalan_tujuan").val('');
    clearInterval(maketReloader);
    clearInterval(bookingBerhasil);
    kursi = [];
}

function launchIntoFullscreen(element) {
    if(element.requestFullscreen) {
        element.requestFullscreen();
    } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}