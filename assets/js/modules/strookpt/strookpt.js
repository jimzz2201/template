var ajaxUrl = baseurl + 'index.php/strookpt/';

$(document).ready(function () {
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd M yyyy',
        weekStart: 1,
        language: "id",
        todayHighlight: true,
    });
    
    $("#txt_biaya_lain_lain").change(function () {
        var biaya_lain = ParseNumber($("#txt_biaya_lain_lain").val());
        if(biaya_lain > 0){
            $("#txt_keterangan_lain_lain").attr('required','required');
        }else{
            $("#txt_keterangan_lain_lain").removeAttr('required');
        }
    });

    $("#frm_strookpt").submit(function () {
        $.ajax({
            type: 'POST',
            url: ajaxUrl + "strookpt_manipulate",
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st){
                    window.location.href = ajaxUrl;
                }else{
                    messageerror(data.msg);
                }
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;
    });

    $("#txt_harga_kps_pt, #txt_harga_kps_pt_ekstra").change(function(){
        HitungSemua();
    }).change();

    $("#Cb_is_kernet_batangan").change(function () {
        CheckKernet();
        HitungSemua();
    });

    $("#Cb_is_laka").change(function(){
        if ($('#Cb_is_laka').is(':checked')) {
            $('#txt_keterangan_laka').removeAttr("readonly");
            $('#txt_keterangan_laka').attr("required", "required");

            $("#txt_tabungan").val("0");
        }else{
            $('#txt_keterangan_laka').attr("readonly", "readonly");
            $('#txt_keterangan_laka').removeAttr("required");

            $("#txt_tabungan").val($("#txt_tabungan_copy").val());
        }
    });

    $("#Cb_is_kp_selamat_timur, #Cb_is_kp_selamat_barat, #txt_kernet_batangan, #txt_tabungan, #txt_full_seat_timur, #txt_full_seat_barat, #txt_tambahan_interval, #txt_biaya_tol, #txt_biaya_lain_lain, #persen_selamat_timur").change(function(e){
        HitungSemua();
    });

    $("#Cb_is_uht").change(function () {
        RefreshKPSUHT();
    });

    $("#Cb_is_kps_ekstra_pengemudi").change(function () {
        RefreshKPSEkstra();
    });

    $("#txt_jumlah_penumpang_barat").change(function(){
        $("#Txt_Penumpang_KPS_Barat_Ekstra").val($(this).val());
        HitungSemua();
    });

    $("#Cb_is_overblass_barat").change(function () {
        if ($("#Cb_is_overblass_barat").is(':checked')) {
            overblass_barat = Number($("#Txt_Hidden_OverBlass_Barat").val().replace(/[^0-9\.]+/g, ""));
            $("#txt_overblass_barat").val(number_format(overblass_barat));
            $("#txt_overblass_barat").removeAttr("readonly");
        }else {
            $("#txt_overblass_barat").val('0');
            $("#txt_overblass_barat").attr("readonly", "readonly");
        }
        HitungSemua();
    });

    $("#Cb_is_overblass_timur").change(function () {
        if ($("#Cb_is_overblass_timur").is(':checked')) {
            overblass_timur = Number($("#Txt_Hidden_OverBlass_Timur").val().replace(/[^0-9\.]+/g, ""))
            $("#txt_overblass_timur").val(number_format(overblass_timur));
            $("#txt_overblass_timur").removeAttr("readonly");
        }
        else {
            $("#txt_overblass_timur").val(0);
            $("#txt_overblass_timur").attr("readonly", "readonly");
        }
        HitungSemua();
    });
});

function Currency2(Num){
    return Currency(parseInt(Num));
}

function HitungSemua() {
    var penumpangBarat = $("#txt_jumlah_penumpang_barat").val();
    var penumpangTimur = ParseNumber($("#TxtJumlahPenumpangTimur").val());
    var hargaKPSelamatTimur = ParseNumber($("#txt_harga_kps_pt").val());
    var persenSelamatTimur = ParseNumber($("#persen_selamat_timur").val());
    var totalTimur = penumpangTimur * hargaKPSelamatTimur;
	var kpSelamatTimur = (totalTimur / 100) * persenSelamatTimur;

    var totalBarat = penumpangBarat * hargaKPSelamatTimur;
    var kpSelamatBarat = (totalBarat / 100) * persenSelamatTimur;

    var kpSelamatBaratRound = Math.round((kpSelamatBarat - 1) / 1000) * 1000;
    var kpSelamatTimurRound = Math.round((kpSelamatTimur - 1) / 1000) * 1000;

    if ($("#Cb_is_laka").is(':checked')) {
        kpSelamatBaratRound = kpSelamatTimurRound = 0;
    }

    $("#TxtTotalKPSelamatTimur").val(Currency2(totalTimur));
    $("#TxtKPSelamatTimurPengemudiAfterPersen").val(Currency2(kpSelamatTimur));
    $("#TxtAfterPersen").val(Currency2(kpSelamatTimur));
    //
     if ($("#Cb_is_kp_selamat_barat").is(':checked')) {
         $("#txt_kp_selamat_barat").val(Currency2(kpSelamatBaratRound));
    }else {
         $("#txt_kp_selamat_barat").val(0);
     }
    
     if ($("#Cb_is_kp_selamat_timur").is(':checked')) {
         $("#txt_kp_selamat_timur").val(Currency(kpSelamatTimurRound));
     }else {
         $("#txt_kp_selamat_timur").val(0);
     }
    RefreshKPSUHT();
    RefreshKPSEkstra();
    var bppt = 0;
    bppt += ParseNumber($("#txt_honor_pengemudi_pt").val());
    bppt += ParseNumber($("#txt_honor_kernet_pt").val());
    if($("#Cb_is_kp_selamat_timur").is(':checked')) {
        bppt += ParseNumber($("#txt_kernet_batangan").val());
    }
    if($("#Cb_is_kp_selamat_barat").is(':checked')) {
        bppt += ParseNumber($("#txt_kp_selamat_barat").val());
    }
    if($("#Cb_is_kp_selamat_timur").is(':checked')) {
        bppt += ParseNumber($("#txt_kp_selamat_timur").val());
    }
    bppt += ParseNumber($("#txt_full_seat_barat").val());
    bppt += ParseNumber($("#txt_full_seat_timur").val());
    bppt += ParseNumber($("#txt_biaya_lain_lain").val());
    bppt += ParseNumber($("#txt_tambahan_interval").val());

    overblass_barat = 0;
    if ($("#Cb_is_overblass_barat").is(':checked')) {
        overblass_barat = ParseNumber($("#txt_overblass_barat").val());
    }
    overblass_timur = 0;
    if ($("#Cb_is_overblass_timur").is(':checked')) {
        overblass_timur = ParseNumber($("#txt_overblass_timur").val());
    }

    bppt = bppt * -1;

    bppt -= overblass_barat;
    bppt -= overblass_timur;

    $("#txt_biaya_pengemudi_pt").val(number_format(bppt));
    if(bppt < 0){
        $("#txt_biaya_pengemudi_pt_display").css("color","red").val("("+number_format(Math.abs(bppt))+")");
    }else{
        $("#txt_biaya_pengemudi_pt_display").css("color","#555").val(number_format(bppt));
    }

    var totalpt = bppt;
    totalpt -= ParseNumber($("#txt_biaya_tol").val());
    $("#txt_total_biaya_pengemudi_pt").val(number_format(totalpt));
    if(totalpt < 0){
        $("#txt_total_biaya_pengemudi_pt_display").css("color","red").val("("+number_format(Math.abs(totalpt))+")");
    }else{
        $("#txt_total_biaya_pengemudi_pt_display").css("color","#555").val(number_format(totalpt));
    }

    var tabungan = ParseNumber($("#txt_tabungan").val());
    if ($("#Cb_is_laka").is(':checked')) {
        tabungan = 0;
        $("#Cb_is_uht").prop('checked', false);
        $("#txt_uht").val('0');
    }
    var total = totalpt + tabungan;
    $("#txt_total").val(number_format(total));
    if(total < 0){
        $("#txt_total_display").css("color","red").val("("+number_format(Math.abs(total))+")");
    }else{
        $("#txt_total_display").css("color","#555").val(number_format(total));
    }
    
    var biayaspbu=  Number($("#txt_biaya_operasional").val().replace(/[^0-9\.]+/g, ""));
    var bototal = Number($("#txt_bo_total").val().replace(/[^0-9\.]+/g, ""));
    
    var bominimum= Number($("#txt_biaya_minimum").val().replace(/[^0-9\.]+/g, ""));
    
    var pendapatanbensin =0;
    var dendabo=0;
    if(biayaspbu>bominimum)
    {
         pendapatanbensin=bototal-biayaspbu;
    }
    else
    {
         pendapatanbensin=bototal-bominimum;
         dendabo=bominimum-biayaspbu;
    }
    
    var pendapatankernet = 0;
    var pendapatanpengemudipt = 0;
    var pendapatanpengemudipt = pendapatanbensin;
    if ($("#txt_nic_kernet_pt").val() != "")
    {
        pendapatankernet = 25 / 100 * pendapatanbensin;
        pendapatanpengemudipt = 75 / 100 * pendapatanbensin;
    }
    if(pendapatankernet<0)
    {
        pendapatankernet=0;
    }
    
    if(pendapatanpengemudipt<0)
    {
        pendapatanpengemudipt=0;
    }
    
    $("#txt_pendapatan_kernet").val(number_format(pendapatankernet));
    $("#txt_denda_bo").val(number_format(dendabo));
    $("#txt_pendapatan_pengemudi_pt").val(number_format(pendapatanpengemudipt));
    $("#txt_pendapatan_total").val(number_format( pendapatanpengemudipt + pendapatankernet));
    
    
}

function RefreshKPSUHT() {
    var penumpangBarat = ParseNumber($("#txt_jumlah_penumpang_barat").val());
    var penumpangTimur = ParseNumber($("#TxtJumlahPenumpangTimur").val());
    var perkepalaBarat = ParseNumber($("#Txt_Perkepala_Barat").val());
    var perkepalaTimur = ParseNumber($("#Txt_Perkepala_Timur").val());
    var uht = (penumpangBarat * perkepalaBarat) + (penumpangTimur * perkepalaTimur);
    if ($('#Cb_is_uht').is(':checked')) {
        $('#txt_uht').val(Currency(uht));
    }else {
        $('#txt_uht').val("0");
    }
}

function RefreshKPSEkstra() {
    var totalpnpkpsekstrabarat = ParseNumber($("#Txt_Penumpang_KPS_Barat_Ekstra").val());
    var totalpnpkpsekstratimur = ParseNumber($("#Txt_Penumpang_KPS_Timur_Ekstra").val());
    if ($('#Cb_is_kps_ekstra_pengemudi').is(':checked')) {
        var hargakpsekstrabarat = ParseNumber($("#txt_harga_kps_pt_ekstra").val());
        var hargakpsekstratimur = ParseNumber($("#txt_harga_kps_pt_ekstra").val());
        var totalkpsselamatbarat = totalpnpkpsekstrabarat * hargakpsekstrabarat;
        // totalkpsselamatbarat = Math.round((totalkpsselamatbarat - 1) / 1000) * 1000;
        var totalkpsselamattimur = totalpnpkpsekstratimur * hargakpsekstratimur;
        // totalkpsselamattimur = Math.round((totalkpsselamattimur - 1) / 1000) * 1000;
        var transitan = 0;
        var kpsExtra = number_format((totalkpsselamatbarat + totalkpsselamattimur + transitan) / 2);

        if ($("#Cb_is_laka").is(':checked')) {
            kpsExtra = 0;
        }

        $('#txt_kps_ekstra_pengemudi').val(Currency(kpsExtra));
    }else {
        $('#txt_kps_ekstra_pengemudi').val("0");
    }
}

function CheckKernet() {
    if ($('#Cb_is_kernet_batangan').is(':checked')) {
        var honor_kernet_batangan = ParseNumber($("#txt_copy_kernet_batangan").val());
        $('#txt_kernet_batangan').val(Currency(honor_kernet_batangan)).focus();
        $('#txt_kernet_batangan').removeAttr("readonly");
    }else{
        $('#txt_kernet_batangan').attr("readonly", "readonly");
        $('#txt_kernet_batangan').val(0);
    }
}

function Currency(Num) {
    var ismin = false;
    if (Num.toString().search("-") > -1)
        ismin = true;
    Num = ParseNumber(Num.toString());
    Num += '';


    if (ismin)
        Num = '-' + Num.toString().replace(/,/g, '');
    else
        Num = Num.toString().replace(/,/g, '');
    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.toString().replace(rgx, '$1' + ',' + '$2');

    return x1 + x2;
}