$(document).ready(function () {

    $(".datepicker").datepicker({
        autoclose: true,
        format: 'MM',
        minViewMod: 1,
        language: "id",
        todayHighlight: true,
    });

    $("#btn-stat").click(function(){
        getStatistik(1);
        getStatistik(2);
    });

});

function getStatistik(id){
    var bulan = $("#bulan_"+id).val();
    var tahun = $("#tahun_"+id).val();
    console.log(bulan, tahun);

    if(tahun == null || tahun == "" || tahun == "0"){
        return false;
    }

    var params = '?bulan='+bulan+'&tahun='+tahun;
    // params += '&id_kelas='+id_kelas;
    var url = baseurl + 'index.php/chart/periode/get_data/'+params;
    if(bulan > 0){
        var graphSubtitle = GetMonth(bulan) +' - '+ tahun;
    }else{
        var graphSubtitle = tahun;
    }

    $.getJSON(url, function (statistik) {
        Highcharts.chart('container_'+id, {
            chart: {
                type: 'column',
                options3d: {
                    enabled: false,
                    alpha: 10,
                    beta: 25,
                    depth: 70
                },
                backgroundColor: '#FFFFFF',
                events : {
                    drilldown: function (e) {
                        if (!e.seriesOptions) {

                            var chart = this;
                            console.log(e);
                            var params_detail = '?tahun='+e.point.tahun+'&bulan='+e.point.bulan;
                            var url_detail = baseurl + 'index.php/chart/periode/get_data_detail/'+params_detail;

                            $.getJSON(url_detail, function (detail) {
                                chart.showLoading('Mengambil data ...');

                                setTimeout(function () {
                                    chart.hideLoading();
                                    chart.setTitle({},{ text: e.point.name +' - '+tahun},false);
                                    chart.addSeriesAsDrilldown(e.point, detail);
                                }, 1000);
                            });
                        }
                    },
                    drillup : function(e){
                        var chart = this;
                        chart.setTitle({},{ text: tahun },false);
                    }
                }
            },
            title: {
                text: 'Statistik Penjualanan',
            },
            subtitle: {
                text: graphSubtitle
            },
            xAxis: {
                type: 'category',
                labels: {
                    step:1
                }
            },
            yAxis: {
                title: {
                    text: 'Total penjualan'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        // format: '{point.y:.1f}%'
                    }
                },
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
            },

            series: [{
                name: 'Penjualan '+ graphSubtitle,
                colorByPoint: true,
                data : statistik
            }],

            drilldown: {
                series: []
            }

        });
    });
}

function getChartHeight(val){
    var height = 400;
    if(val > 400)
        height = val;


    return height
}