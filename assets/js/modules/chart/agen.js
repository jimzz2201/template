$(document).ready(function () {

    $(".datepicker").datepicker({
        autoclose: true,
        endDate: '0d',
        format: 'dd M yyyy',
        weekStart: 1,
        language: "id",
        todayHighlight: true,
    });

    $("#btn-stat").click(function(){
        getStatistik(1);
        getStatistik(2);
    });

});

function getStatistik(id){
    var tgl_from = $("#tanggal_from_"+id).val();
    var tgl_to = $("#tanggal_to_"+id).val();
    var id_kelas = $("#id_kelas_"+id).val();

    if(tgl_from == null || tgl_from == ""){
        return false;
    }

    var params = '?tanggal_from='+encodeURI(tgl_from)+'&tanggal_to='+encodeURI(tgl_to)+'&id_kelas='+id_kelas;
    var url = baseurl + 'index.php/chart/agen/get_data/'+params;
    var graphTitle = 'Statistik Per Agen ' + tgl_from+' - '+tgl_to;

    $.getJSON(url, function (statistik) {
        console.log(statistik.length);
    Highcharts.chart('container_'+id, {
        chart: {
            type: 'bar',
            height: getChartHeight(statistik.length * 15),
            backgroundColor: '#FFFFFF',
            events : {
                drilldown: function (e) {
                    if (!e.seriesOptions) {

                        var chart = this;
                        console.log(e);
                        var params_detail = '?tanggal_from='+encodeURI(tgl_from)+'&tanggal_to='+encodeURI(tgl_to)+'&id_pangkalan='+e.point.id_pangkalan+'&nama_pangkalan='+e.point.name;
                        var url_detail = baseurl + 'index.php/chart/agen/get_data_detail/'+params_detail;

                        console.log();

                        $.getJSON(url_detail, function (detail) {
                            chart.showLoading('Mengambil data ...');

                            setTimeout(function () {
                                console.log(detail.length);
                                chart.hideLoading();
                                chart.setTitle({},{ text: e.point.name},false);
                                chart.addSeriesAsDrilldown(e.point, detail);
                                chart.setSize(undefined,getChartHeight(detail.length * 20));
                            }, 1000);
                        });
                    }
                },
                drillup : function(e){
                    var chart = this;
                    chart.setTitle({},{ text: '' },false);
                    chart.setSize(undefined,getChartHeight(statistik.length * 15));
                }
            }
        },
        title: {
            text: graphTitle
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category',
            labels: {
                step:1
            }
        },
        yAxis: {
            title: {
                text: 'Total penjualan'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    // format: '{point.y:.1f}%'
                }
            },
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
        },

        series: [{
            name: 'Penjualan Agen',
            colorByPoint: true,
            data : statistik
        }],

        drilldown: {
            series: []
        }

    });
    });
}

function getChartHeight(val){
    console.log(val);
    var height = 400;
    if(val > 400)
        height = val;

    console.log(height);

    return height
}