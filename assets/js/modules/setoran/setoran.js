$(document).ready(function () {
    $(".select2pangkalan").select2({data: list_pangkalan});
    $(".select2pangkalan_beli").select2({data: list_pangkalan_beli}).val(default_pangkalan).trigger('change');

    $(".datepicker").datepicker({
        autoclose: true,
        format: 'dd M yyyy',
        weekStart: 1,
        language: "id",
        todayHighlight: true,
    });

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    table = $("#mytable").dataTable({
        initComplete: function () {
            var api = this.api();
            $('#mytable_filter input')
                .off('.DT')
                .on('keyup.DT', function (e) {
                    if (e.keyCode == 13) {
                        api.search(this.value).draw();
                    }
                });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        pageLength: 100,
        // paginate: false,
        processing: true,
        // sDom:'ltrip',
        serverSide: true,
        ajax: {"url": baseurl + "index.php/setoran/getdatasetoran", "type": "POST",
            "data": function (d) {
                return $.extend({}, d, {
                    "extra_search": $("form#frm_search").serialize()
                });
            }},
        columns: [
            {data: "id_setoran", orderable: false, className:"normal-padding",
                title: "<input  id='check_box_all' type='checkbox' class='minimal-red'   />",
                mRender: function (data, type, row) {
                    return "<input name='pilihan["+data+"]' id='checkbox_pilih_" + data + "' data-status='"+row['status_tiket']+"' type='checkbox' class='minimal-red cb_detailcheck'  value='" + data + "' />";
                    // return "<input name='pilihan["+data+"]' id='checkbox_pilih_" + data + "' type='checkbox' class='minimal-red cb_detailcheck'  value='" + data + "' />";
                },
                 width:'25px'
            }
            , {data: "id_berangkat", orderable: false, title: "#", width:'35px', className:"normal-padding"}
            , {data: "nama_pangkalan", orderable: false, title: "Agen", width:'55px', className:"normal-padding",
                mRender: function (data, type, row) {
                    var pangkalan = "";
                    if(kode_pangkalan[data] !== "undefined"){
                        pangkalan = kode_pangkalan[data];
                    }
                    return pangkalan;
                }
            }
            , {data: "tanggal_penjualan", orderable: false, title: "Tgl Jual", width:'75px', className:"normal-padding",
                mRender: function (data, type, row) {
                    return DefaultDateFormat(data);
                }
            }
            ,{data: "no_spj", orderable: false, title: "No SPJ", width:'130px', className:"normal-padding"}
            ,{data: "tp_asal", orderable: false, title: "", width:'75px', className:"normal-padding",
                mRender: function (data, type, row) {
                    var trayek = "";
                    if(kode_pangkalan[data] !== "undefined" && kode_pangkalan[row['tp_tujuan']] !== "undefined"){
                        trayek = kode_pangkalan[data]+'=>'+kode_pangkalan[row['tp_tujuan']];
                    }

                    return trayek;
                }
            }
            ,{data: "nama_tipe", orderable: false, title: "Armada", width:'85px', className:"normal-padding text-center"}
            ,{data: "no_body", orderable: false, title: "No Body", width:'65px', className:"normal-padding text-center"}
            ,{data: "kode_order", orderable: false, title: "Kode Order", className:"normal-padding text-center"}
            ,{data: "no_booking", orderable: false, title: "No Booking", className:"normal-padding text-center"}
            // ,{data: "kode_berangkat", orderable: false, title: "Kode Berangkat"}
            ,{data: "tanggal", orderable: false, title: "Tgl Brgkt", width:'75px', className:"normal-padding"}
            ,{data: "jam", orderable: false, title: "Jam", width:'50px', className:"normal-padding text-center",
            mRender:function(data, type, row){
                var jam = data;
                if(data == "00:00"){
                    jam = '';
                }
                return jam;
            }}
            ,{data: "pnp", orderable: false, title: "PNP", width:'40px', className:"normal-padding text-right"}
            ,{data: "nama_shift", orderable: false, title: "Shift", width:'50px', className:"normal-padding text-right"}
            ,{data: "username", orderable: false, title: "Operator", width:'50px', className:"normal-padding text-right"}
            ,{data: "status_tiket", orderable: false, title: "Status", width:'50px', className:"normal-padding text-center",
                mRender: function (data, type, row) {
                    var status_tiket = '<span class="badge btn-info">Pesanan</span>';
                    if(data == 1){
                        status_tiket = '<span class="badge btn-danger">Tiket</span>';
                    }

                    return status_tiket;
                }
            }
            ,{data: "total_penjualan", orderable: false, title: "Total", className: "normal-padding text-right",
                mRender: function (data, type, row) {
                    var nomor_spj = row['no_spj'];
                    if(row['no_spj'] == "null" || row['no_spj'] == null){
                        nomor_spj = "";
                    }
                    var inputan1 = '<input type="hidden" value="'+parseInt(data)+'" name="nominal['+row['id_setoran']+']" id="nominal_'+row['id_setoran']+'" />';
                    var inputan2 = '<input type="hidden" value="'+row['kode']+'" name="id_pemesanan['+row['id_setoran']+']" id="pemesanan_'+row['id_setoran']+'" />';
                    var inputan3 = '<input type="hidden" value="'+nomor_spj+'" name="no_spj['+row['id_setoran']+']" id="no_spj_'+row['id_setoran']+'" />';
                    var inputan4 = '<input type="hidden" value="'+row['no_body']+'" name="no_body['+row['id_setoran']+']" id="no_body_'+row['id_setoran']+'" />';
                    var inputan5 = '<input type="hidden" value="'+row['pnp']+'" name="pnp['+row['id_setoran']+']" id="pnp_'+row['id_setoran']+'" />';
                    var inputan6 = '<input type="hidden" value="'+row['created_by']+'" name="nama_shift['+row['id_setoran']+']" id="nama_shift_'+row['id_setoran']+'" />';
                  return Comma(data) + inputan1 + inputan2 + inputan3 + inputan4 + inputan5+inputan6;
                }
            }
        ],
        order: [],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(1)', row).html(index);
        },
        drawCallback: function (settings){
            // console.log(settings.json.recordsTotal);
            var deduct_row = '';
            var id_pangkalan = $("#id_pangkalan_beli").val();
            var id_agen = $("#id_user").val();
            var date_search = $("#tanggal").val();

            var group_kode = $("#group_kode").val();
            if(group_kode == 1){
                table.api().columns([8,9]).visible(true);
                table.api().columns([13,14]).visible(false);
                console.log( 'Table\'s column visibility are set to: '+table.api().columns().visible().join(', ') );
            }else{
                table.api().columns([8,9]).visible(false);
                table.api().columns([13,14]).visible(true);
                console.log( 'Table\'s column visibility are set to: '+table.api().columns().visible().join(', ') );
            }

            $(".deduct-row").remove();
            if(id_pangkalan && pisah_komisi == 1){
                $.post(
                    baseurl + 'index.php/keberangkatan/penjualan/get_deduct',
                    {
                        id_pangkalan : id_pangkalan,
                        id_agen: id_agen
                    },
                    function(resp){
                        if(resp.length){
                            $.each(resp, function(idx, val){
                                var {id_pangkalan, kode_pangkalan, nominal, max_id, ids, jenis_deduct, tgl, jenis_deduct_text, selectable} = val;
                                // var cb_val = `${id_pangkalan}-${jenis_deduct}-${max_id}`;
                                var cb_val = jenis_deduct;
                                var def_check = nominal == 0 ? 'checked="checked"' : '';
                                if(parseInt(nominal) != 0){
                                    deduct_row += '<tr role="row" class="deduct-row">';
                                    if(selectable){
                                        deduct_row += `<td class="normal-padding"><input type="checkbox" class="cb_deductcheck" value="${cb_val}" ${def_check}/></td>`;
                                    }else{
                                        deduct_row += '<td></td>';
                                    }
                                    deduct_row += `<td></td><td class="normal-padding">${kode_pangkalan}</td>`;
                                    deduct_row += `<td class="normal-padding">${tgl}</td>`;
                                    deduct_row += `<td class="normal-padding" colspan="10">${jenis_deduct_text}`;
                                    // deduct_row += `<input type="hidden" id="deduct-max-${cb_val}" value="${max_id}" />`;
                                    deduct_row += `<input type="hidden" id="deduct-ids-${cb_val}" value="${ids}" />`;
                                    deduct_row += `<input type="hidden" id="deduct-nominal-${cb_val}" value="${nominal}" />`;
                                    deduct_row += `</td>`;
                                    deduct_row += `<td class="text-right normal-padding">${CommaMin(nominal)}</td>`;
                                    deduct_row += '</tr>';
                                }
                            });

                            if(settings.json.recordsTotal == 0){
                                $(".dataTables_empty").remove();
                            }
                        }

                        if(deduct_row != ''){
                            $("table#mytable thead").append(deduct_row);
                        }

                    }, 'json'
                );
            }

            $(document).ajaxComplete(function () {
                $('#check_box_all').prop('checked', false);

                $('#check_box_all').change(function () {
                    if ($(this).is(':checked')){
                        $('.cb_deductcheck').prop('checked', true);
                        $('.cb_detailcheck').prop('checked', true);
                    } else{
                        $('.cb_deductcheck').prop('checked', false);
                        $('.cb_detailcheck').prop('checked', false);
                    }
                });

                $("#id_pangkalan").val($("#id_pangkalan_beli").val());

            });
        }

    });

    $("form#frm_search").submit(function (e) {
        table.fnDraw(false);
        return false;
    });

    $("#btn-manifest").click(function(){
        var diCheck = [];
        var nominal = [];
        var no_spj = [];
        var pnp = [];
        var no_body = [];
        var pemesanan = "";
        var kode = 0;
        var tanggal_penjualan = null;
        var nama_shift=[];
        var status_tiket = null;
        var lanjut = true;
        var campur = false;
        var deduct_data = [];
        var deduct_nominal = [];
        LoadBar.show();
        $(".cb_detailcheck").each(function () {
            if($(this).is(':checked')){
                kode = $(this).val();

                if(diCheck[kode] === undefined && lanjut){
                    if(status_tiket == null){
                        status_tiket = $(this).attr('data-status');
                    }
                    if(status_tiket != $(this).attr('data-status')){
                        diCheck = [];
                        no_spj = [];
                        no_body = [];
                        pnp = [];
                        nama_shift = [];
                        pemesanan = "";

                        lanjut = false;
                        campur = true;
                    }else{
                        diCheck.push(kode);
                        // nominal.push($("#nominal_"+kode).val());

                        if($("#no_spj_"+kode).val()){
                            no_spj.push($("#no_spj_"+kode).val());
                        }else{
                            no_spj.push("-");
                        }
                        no_body.push($("#no_body_"+kode).val());
                        pnp.push($("#pnp_"+kode).val());
                        nama_shift.push($("#nama_shift_"+kode).val());
                        if(pemesanan == ""){
                            pemesanan = $("#pemesanan_"+kode).val();
                        }else{
                            pemesanan += ","+$("#pemesanan_"+kode).val();
                        }
                    }

                }
            }
        });

        $(".cb_deductcheck").each(function(){
            if($(this).is(":checked")){
                var jenis_deduct = $(this).val();
                // deduct_data[jenis_deduct] = parseInt($("#deduct-max-"+jenis_deduct).val());
                deduct_data[jenis_deduct] = $("#deduct-ids-"+jenis_deduct).val();
                deduct_nominal[jenis_deduct] = parseInt($("#deduct-nominal-"+jenis_deduct).val());
            }
        });

        if((diCheck.length || deduct_data.length) && lanjut){
            $.post(
                baseurl + 'index.php/setoran/form_setoran',
                {
                    pilihan: diCheck,
                    no_spj: no_spj,
                    no_body: no_body,
                    pnp: pnp,
                    nama_shift: nama_shift,
                    pemesanan: pemesanan,
                    status_tiket: status_tiket,
                    id_pangkalan: $("#id_pangkalan").val(),
                    tipe_tanggal: $("#tipe_tanggal").val(),
                    tanggal1: $("#tanggal").val(),
                    tanggal2: $("#tanggal2").val(),
                    deduct_data: deduct_data,
                    deduct_nominal: deduct_nominal,
                },
                function(resp){
                    if(resp.st){
                        $("#id_pangkalan_operasional").val($("#id_pangkalan").val());
                        $("#dipilih_operasional").val("");
                        total_operasional = 0;
                        operasional = [];
                        modalbootstrap(resp.html, "Buat Setoran");
                    }else{
                        messageerror(resp.msg);
                    }
					LoadBar.hide();
                }, 'json'
            );

        }else{
            LoadBar.hide();
            if(campur){
                alert("Setoran dari Tiket dan Pesanan tidak dapat digabung!");
            }else{
                alert("Silahkan pilih data yang akan dimasukan ke setoran!");
            }
        }
    });
    
});