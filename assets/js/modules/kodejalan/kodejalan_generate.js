var id_maket;
var alokasi = [];
var kursi = [];
var agen_alokasi;
var agen_jatah;
$(document).ready(function () {
    /*$('.datepicker').datepicker({
     autoclose: true,
     dateFormat: 'dd M yy',
     });*/
    $('.datepicker').daterangepicker({
        locale: {
            format: 'DD MMM YYYY',
            firstDay : 1
        },
        showDropdowns: true,
    });

    $(".timepicker").timepicker({
        showInputs: false,
        showMeridian: false
    });

    $(".timepickerX").timepickerExt({
        showInputs: false,
        showMeridian: false
    });

    $(".select2kelas").select2({});

    $("#available_online").change(function(){
        if($(this).val() == 1){
            $(".alokasi-el").show();
        }else{
            $(".alokasi-el").hide();
        }
    }).change();

    $('.pangkalan_alokasi').select2({
        placeholder: "Pilih Agen",
        allowClear: true,
        // dropdownParent: $("#modalbootstrap"),
        ajax: {
            url: baseurl + 'index.php/kodejalan/search_pangkalan',
            dataType: 'json',
            method: 'POST',
            minimumInputLength: 2,
            processResult: function (data) {
                return {
                    results: data.results
                }
            }
        }
    });

    $("#dd_id_rm").change(function(){
        var id_rm = $(this).val();
        var id_package = null;
        if(id_rm){
            $.post(
                baseurl + 'index.php/paket/paket_list',
                {id_rumah_makan : id_rm, id_package : id_package},
                function (data) {
                    $("#dd_id_package").empty();
                    if(data.st){
                        $("#dd_id_package").html(data.opsi);
                        console.log(data.opsi);
                    }else{
                        alert(data.msg);
                    }
                }, "json"
            );
        }else{
            $("#dd_id_package").empty();
            $("#dd_id_package").html('<option value="">Pilih Paket</option>');
        }
    }).change();

    $("#id_pangkalan_alokasi").on('select2:select', function(e){
        // reloadMaket();
        var selected = e.params.data;
        agen_alokasi = selected.text;
        if(alokasi["_"+selected.id]){
            var currentKursi = $("#alokasi_"+selected.id+"_value").val();
            kursi = currentKursi.split(",");
            kursi.forEach(function(value, idx){
                kursi[idx] = parseInt(value);
                $(".table-maket a#kursi_"+value).removeClass("btn-danger").addClass("btn-selected");
            });
        }
    });

    $("#id_pangkalan_jatah").on('select2:select', function(e){
        var selected = e.params.data;
        agen_jatah = selected.text;
    });

    $("#id_pangkalan_alokasi").on('select2:unselecting', function(e){
        kursi = [];
        $(".table-maket a.btn-kursi").removeClass("btn-selected").removeClass("btn-danger");

        Object.keys(alokasi).forEach(function(agen_) {
            alokasi[agen_].forEach(function(no_kursi, idx){
                $(".table-maket a#kursi_"+no_kursi).addClass("btn-danger");
            });
        });
    });

    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red, #cb-allday, #cb-weekday, #cb-weekend').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red',
        labelHover: false
    });

    $("input.flat-green").iCheck({
        radioClass: 'iradio_flat-green',
        labelHover: false
    });

    $("#frm_generate").submit(function(){
        var jml_hari = parseInt($(".cb-hari:checked").length);
        if(jml_hari){
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/kodejalan/generate_action',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data.st) {
                        window.location.href = baseurl + 'index.php/kodejalan';
                    }
                    else {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        }else{
            alert("Silahkan pilih hari!");
        }
//            $("#frm_generate input[type='submit']").attr('value','<?//=$button;?>//').prop('disabled', false);
        return false;
    });

    $("#cb-allday").on('ifChecked', function(event){
        changeChecked('allday');
    });
    $("#cb-allday").on('ifUnchecked', function(event){
        changeUnchecked('allday');
    });

    $("#cb-weekday").on('ifChecked', function(event){
        changeChecked('weekday');
    });
    $("#cb-weekday").on('ifUnchecked', function(event){
        changeUnchecked('weekday');
    });

    $("#cb-weekend").on('ifChecked', function(event){
        changeChecked('weekend');
    });
    $("#cb-weekend").on('ifUnchecked', function(event){
        changeUnchecked('weekend');
    });

    $("#id_maket").change(function(){
        var lanjut_ganti = true;
        if(Object.keys(alokasi).length > 0){
            swal({
                title: "Mengganti Maket akan menghilangkan kursi alokasi",
                // text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ganti Maket!",
                closeOnConfirm: true
            }).then((result) => {
                if (result.value){
                    if($(this).val()){
                        // $("#maket-alokasi").load(baseurl + 'index.php/kodejalan/maket_alokasi/' + $(this).val());
                        id_maket = $(this).val();
                    }
                    resetAlokasi();
                }else{
                    $("#id_maket").val(id_maket);
                    lanjut_ganti = false;
                }
            });
        }else{
            if($(this).val()) {
                $("#maket-alokasi").load(baseurl + 'index.php/kodejalan/maket_alokasi/' + $(this).val());
                id_maket = $(this).val();
            }
        }

        return lanjut_ganti;
    });
});

function changeChecked(mode) {
    if(mode == "allday"){
        $("#cb-weekday, #cb-weekend").iCheck('check');
    }else if(mode == "weekday"){
        if($("#cb-weekend").is(":checked")){
            $("#cb-allday").prop('checked', true).iCheck('update');
        }
        $(".cb-hari:lt(5)").iCheck('check');
    }else if(mode == "weekend"){
        if($("#cb-weekday").is(":checked")){
            $("#cb-allday").prop('checked', true).iCheck('update');
        }
        $(".cb-hari:gt(4)").iCheck('check');
    }
}
function changeUnchecked(mode) {
    if(mode == "allday"){
        $("#cb-weekday, #cb-weekend").iCheck('uncheck');
    }else if(mode == "weekday"){
        $(".cb-hari:lt(5)").iCheck('uncheck');
        $("#cb-allday").removeAttr('checked').iCheck('update');
    }else if(mode == "weekend"){
        $(".cb-hari:gt(4)").iCheck('uncheck');
        $("#cb-allday").removeAttr('checked').iCheck('update');
    }
}

function saveJatah(){
    var id_pangkalan_jatah = $("#id_pangkalan_jatah").val();
    var jatah_kursi = $("#jatah_kursi").val();
    if(id_pangkalan_jatah && jatah_kursi){
        if($("#jatah_"+id_pangkalan_jatah).length){
            $("#jatah_"+id_pangkalan_jatah).val(jatah_kursi);
            $("#barisjatah_"+id_pangkalan_jatah+" .text_jatah").html(jatah_kursi);
        }else{
            $("#form-jatah").append('<input type="hidden" name="jatah['+id_pangkalan_jatah+']" id="jatah_'+id_pangkalan_jatah+'" value="'+jatah_kursi+'">');

            var baris = '<tr id="barisjatah_'+id_pangkalan_jatah+'"><td>'+agen_jatah+'</td><td class="text_jatah">'+jatah_kursi+'</td>';
            baris += '<td><a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="hapusJatah('+id_pangkalan_jatah+')"><i class="fa fa-trash"></i></a></td></tr>';
            $("#tabel-jatah tbody").append(baris);
        }

        $("#jatah_kursi").val('');
        $("#id_pangkalan_jatah").val(null).trigger('change');
    }
}

function savePilihan(){
    var id_pangkalan_alokasi = $("#id_pangkalan_alokasi").val();
    var reset_opsi = true;
    if(id_pangkalan_alokasi && id_maket){
        if($("#alokasi_"+id_pangkalan_alokasi).length){
            if(kursi.length){
                alokasi["_"+id_pangkalan_alokasi] = kursi;
                $("#alokasi_"+id_pangkalan_alokasi+"_value").val(kursi.join(","));
                var baris = $("#baris_"+id_pangkalan_alokasi);
                baris.empty();
                baris.append('<td>'+agen_alokasi+'</td><td>'+kursi.join(", ")+'</td>');
                baris.append('<td><a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="hapusAlokasi('+id_pangkalan_alokasi+')"><i class="fa fa-trash"></i></a></td>')
            }else{
                delete alokasi["_"+id_pangkalan_alokasi];
                $("tr#baris_"+id_pangkalan_alokasi).remove();
                $("#alokasi_"+id_pangkalan_alokasi).remove();
                $("#alokasi_"+id_pangkalan_alokasi+"_value").remove();
            }
        }else{
            if(kursi.length){
                alokasi["_"+id_pangkalan_alokasi] = kursi;
                $("#form-alokasi").append('<input type="hidden" name="alokasi[]" id="alokasi_'+id_pangkalan_alokasi+'" value="'+id_pangkalan_alokasi+'">');
                $("#form-alokasi").append('<input type="hidden" name="alokasi_detail['+id_pangkalan_alokasi+']" id="alokasi_'+id_pangkalan_alokasi+'_value" value="'+kursi.join(",")+'">');

                var baris = '<tr id="baris_'+id_pangkalan_alokasi+'"><td>'+agen_alokasi+'</td><td>'+kursi.join(", ")+'</td>';
                baris += '<td><a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="hapusAlokasi('+id_pangkalan_alokasi+')"><i class="fa fa-trash"></i></a></td></tr>';

                $("#tabel-alokasi tbody").append(baris);
            }else{
                reset_opsi = false;
            }
        }
    }else{
        alert("Agen / Maket belum dipilih!");
        reset_opsi = false;
    }
    if(reset_opsi){
        $("#id_pangkalan_alokasi").val(null).trigger('change');
        $(".table-maket a.btn-selected").addClass("btn-danger").removeClass("btn-selected");
        kursi = [];
    }
}

function resetAlokasi() {
    alokasi = [];
    kursi = [];
    $("#tabel-alokasi tbody").empty();
    $("#form-alokasi").empty();
    $(".table-maket a.btn-kursi").removeClass("btn-danger").removeClass("btn-selected");
}

function hapusAlokasi(id_agen) {
    swal({
        title: "Hapus alokasi kursi ini?",
        // text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus Alokasi!",
        closeOnConfirm: true
    }).then((result) => {
        if (result.value){
            alokasi["_"+id_agen].forEach(function(no_kursi, idx){
                $(".table-maket a#kursi_"+no_kursi).removeClass("btn-danger");
            });

            $("#baris_"+id_agen).remove();
            $("#alokasi_"+id_agen+"_value").remove();
            $("#alokasi_"+id_agen).remove();

            delete alokasi["_"+id_agen];
        }
    });
}

function hapusJatah(id_agen){
    swal({
        title: "Hapus jatah kursi ini?",
        // text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus jatah!",
        closeOnConfirm: true
    }).then((result) => {
        if (result.value){
            $("#barisjatah_"+id_agen).remove();
            $("#jatah_"+id_agen).remove();
        }
    });
}