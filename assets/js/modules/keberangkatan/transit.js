var table;
var maketReloader;
var changeSeat = null;
var newSeat = null;
var kursi = [];
var urlPemesanan = baseurl + 'index.php/keberangkatan/transit/';
var urlBerangkat = baseurl + 'index.php/keberangkatan/';
var list_tujuan = [];
var pake_promo = false;
var id_promo = null;
var limitKursi = 1;
var isTambahan = false;

$(document).ready(function () {

    $("form#form_search").submit(function (e) {
        resetLah();
        $("#smartwizard ul.step-anchor li:gt(1)").removeClass("done");
        table.api().columns.adjust();
        LoadBar.show();
        table.fnDraw(true);
        return false;
    });
    
    $("#id_pangkalan_tujuan").change(function () {
        $("#kode_promo").val('');
        $("#promo-notif").empty();
        pake_promo = false;
        id_promo = null;
    });

    $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    table = $("#mytable").dataTable({
        oLanguage: {
            sProcessing: "loading..."
        },
        paginate: false,
        sDom: 't',
        processing: true,
        serverSide: true,
        ajax: {"url": urlPemesanan + "getdatakeberangkatan", "type": "POST",
            "data": function (d) {
                return $.extend({}, d, {
                    "extra_search": $("form#form_search").serialize()
                });
            }},
        // deferRender:    true,
        // scrollY:        400,
        // scrollCollapse: true,
        // scroller:       true,
        columns: [
            // {data: "kode_berangkat", orderable: false, title: "Kode Berangkat"}
            {
                data: "action",
                orderable: false,
                className: "normal-padding",
                width: "50px",
            }
            // , {data: "asal", orderable: false, title: "Asal", className:"normal-padding"}
            // , {data: "tujuan", orderable: false, title: "Tujuan"}
            , {data: "jurusan", orderable: false, title: "Jurusan", className:"normal-padding"}
            , {data: "terjadwal", orderable: false, title: " ", className:"normal-padding text-center", width: "20px",
                mRender:function (data, type, row) {
                    var terjadwal = "N";
                    if(data == "1"){
                        terjadwal = "S"
                    }
                    return terjadwal;
                }
            }
            , {
                data: "tanggal", orderable: false, title: "Tgl Brkt", className:"normal-padding", width: "70px",
                mRender: function (data, type, row) {
                    return DefaultDateFormat(data);
                }
            }
            , {data: "jam", orderable: false, title: "Jam Brkt", className:"normal-padding", width: "40px",
                mRender: function (data, type, row) {
                    var jam = '';
                    if(data.substr(0,5) != "00:00"){
                        var value = data.split(":");
                        jam = value[0]+":"+value[1];
                    }
                    return jam;
                }
            }
            , {data: "no_body", orderable: false, title: "No Body", className:"normal-padding", width: "100px"}
            , {data: "nama_tipe", orderable: false, title: "PO", className:"normal-padding", width: "80px"}
            , {data: "kode_kelas", orderable: false, title: "Kelas", className:"normal-padding text-center", width: "50px"}
        ],
        order: [[1,'asc']],
        rowCallback: function(row, data, iDisplayIndex){
            $(row).addClass("kelas_"+data.id_kelas);
            // console.log(data);
        },
        drawCallback: function(settings ){
            $(document).ajaxComplete(function () {
                LoadBar.hide();
            });
        }

    });

    $(".btn-step-1").unbind('click').click(function () {
        $("#smartwizard").smartWizard("goToStep", 1);
    });

    $(".btn-step-2").unbind('click').click(function () {
        $("#smartwizard").smartWizard("goToStep", 2);
    });

    $(".btn-step-3").unbind('click').click(function () {
        reloadMaket();
        clearInterval(maketReloader);
        maketReloader = setInterval(reloadMaket, 10000);
        $("#smartwizard").smartWizard("goToStep", 3);
    });


    //redirect ke step 1 klo di refresh
    // if(ticketingLayout == 1){
    //     if($("#layout-maket").attr("data-berangkat") == "" || $("#layout-maket").attr("data-berangkat") == null){
    //         $("#smartwizard").smartWizard("goToStep", 1);
    //     }
    // }

    $("#tiketModal .btn-close").click(function(){
        reloadMaket();
        clearInterval(maketReloader);
        maketReloader = setInterval(reloadMaket, 10000);
    });

    $("#btn-promo").click(function(){
        var id_berangkat = $("#layout-maket").attr('data-berangkat');
        if($("#promo-notif").html()=="")
        {
          setHarga(id_berangkat, true);  
        }
        else
        {
            alert("Kode Promo sudah digunakan");
        }});
    
    $("#kode_promo").click(function () {
        $(this).select();
    });

    $("#agen").change(function(){
        var username_agen = $(this).val();
        if(username_agen){
            $.post(
                baseurl + 'index.php/user/checkUser',
                {username:username_agen},
                function (resp) {
                    if(resp.ada == true){
                        $("#check-agen-notif").html('');
                    }else{
                        $("#check-agen-notif").html('<div class="warninglabel"><i class="fa fa-warning"></i> Agen tidak terdaftar di sistem!</div>');
                    }
                },'json'
            );
        }else{
            $("#check-agen-notif").html('');
        }
    });

});

function ubahFormPesan() {
    if(kursi.length > 1){
        console.log($("#rombongan").is(':checked'));
        if($("#rombongan").is(':checked')){
            for(x=2;x<=kursi.length;x++){
                $("div#sub_pesan_form_"+x+" input").removeAttr('required');
                $("div#sub_pesan_form_"+x).hide();
            }
            $(".kursi_pesanan").hide();
            $(".kursi_pesanan_satuan").hide();
            $(".kursi_pesanan_semua").show();
        }else{
            for(x=2;x<=kursi.length;x++) {
                $("div#sub_pesan_form_" + x + " input").attr('required', 'required');
                $("div#sub_pesan_form_" + x + " input.frm_pesan-turun").removeAttr('required');
                $("div#sub_pesan_form_" + x).show();
            }
            $(".kursi_pesanan").show();
            $(".kursi_pesanan_satuan").show();
            $(".kursi_pesanan_semua").hide();
        }
    }
    changeTotalPemesanan();
}


function selectKeberangkatan(id_berangkat) {
    console.log("keberangkatan: "+id_berangkat);
    LoadBar.show();
    kursi = [];
    if(id_berangkat){
            $.post(
                urlPemesanan+'get_keberangkatan',
                {id_berangkat:id_berangkat},
                function(data){
                    list_tujuan = data.list_tujuan;
                    $("#id_pangkalan_asal").empty().select2({data:data.list_asal});
                    $("#id_pangkalan_tujuan").empty().select2({data:data.list_tujuan});
                    if($("#search_pangkalan_asal").val() != ""){
                        $("#id_pangkalan_asal").val($("#search_pangkalan_asal").val()).trigger("change");
                    }else{
                        $("#id_pangkalan_asal").val(data.id_pangkalan_asal).trigger("change");
                    }
                    if($("#search_pangkalan_tujuan").val() != ""){
                        $("#id_pangkalan_tujuan").val($("#search_pangkalan_tujuan").val()).trigger("change");
                    }else{
                        $("#id_pangkalan_tujuan").val(data.id_pangkalan_tujuan).trigger("change");
                    }
                    $("#harga_tiket").val(data.harga);
                    $("#id_kelas").val(data.id_kelas);
                    $("#tipe_bus").val(data.tipe_bus);
                    var teks = '<table>';
                    var teks = '<tr><td width="125">Keberangkatan</td><td>: '+data.kode_berangkat + '</td></tr>';
                    if(data.no_body){
                        teks += '<tr><td>No Body</td><td>: <span id="lbl_no_body">'+data.no_body+'</span></td></tr>';
                    }
                    if(data.no_spj){
                        teks += '<tr><td>No SPJ</td><td>: <span id="lbl_no_spj">'+data.no_spj+'</span></td></tr>';
                    }

                    teks += '<tr><td colspan="2">'+data.pangkalan_asal+' - '+data.pangkalan_tujuan +'  [ '+data.nama_kelas+' ]</td></tr>';
                    teks += '<tr><td>'+data.tanggal + '</td>';
                    teks += '<td>';
                    if(data.jam != "00:00"){
                        teks += data.jam;
                    }
                    teks += '</td></tr>';
                    teks += '</table>';

                    $("#jadwal_berangkat").html(teks);
                    $("#btn-manifest, #btn-sp, #btn-bonggol").show();

                    $("#btn-pesan, #btn-pesan-tambahan").show();
                    $("#btn-promo").show();
                    // if(data.status < 2 && data.available == 1){
                    //     $("#btn-pesan").show();
                    //     $("#btn-promo").show();
                    // }else{
                    //     $("#btn-pesan").hide();
                    //     $("#btn-promo").hide();
                    // }

                    $("#layout-maket").attr('data-berangkat', id_berangkat);
                    $("#layout-maket").load(urlPemesanan + "view_maket/"+data.id_berangkat + "/" + $("#id_pangkalan_asal").val(), function(){
                        $("#box-print-tiket").hide();
                        setHarga(id_berangkat);
                        clearInterval(maketReloader);
                        maketReloader = setInterval(reloadMaket, 10000);

                        if($("#smartwizard").length){
                            console.log(kemana);
                            if(kemana > 0){
                                $('#smartwizard').smartWizard("next");
                            }
                        }

                        LoadBar.hide();
                    });
                },"json"
            );

    }

}

function pesanKursi(el){
    clearKursiCadangan();
    if(changeSeat == null){
        if($(el).hasClass('btn-selected')){
            var idx = kursi.indexOf($(el).data('kursi'));
            if(idx > -1){
                kursi.splice(idx, 1);
            }
            $(el).removeClass('btn-selected');
        }else{
            if(kursi.length < limitKursi){
                $(el).addClass('btn-selected');
                kursi.push($(el).data('kursi'));
            }
        }
        // clearInterval(maketReloader);
        // if(kursi.length == 0){
        //     maketReloader = setInterval(reloadMaket, 10000);
        // }
    }else{
        newSeat = $(el).data('kursi');
        var id_berangkat = $("#layout-maket").attr("data-berangkat");
        if(confirm("Konfirmasi pindah ke Kursi No. " + newSeat + "?")){
            $.post(
                urlPemesanan + "change_seat",
                {data:{id_berangkat: id_berangkat, id_pemesanan: changeSeat, no_kursi: newSeat}},
                function(data){
                    if(data.st){
                        messagesuccess(data.msg);
                        reloadMaket();
                        clearInterval(maketReloader);
                        maketReloader = setInterval(reloadMaket, 10000);
                        printSomething(data.url, null, 'Print Tiket', true, false, 400);
                    }else{
                        modaldialogerror(data.msg);
                    }
                    changeSeat = newSeat = null;
                },"json"
            );
        }else{
            changeSeat = newSeat = null;
            reloadMaket();
            maketReloader = setInterval(reloadMaket, 10000);
        }
    }
}



function pesanTiket(){
    isTambahan = false;
    clearKursiCadangan();
    var id_berangkat = $("#layout-maket").data("berangkat");
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var id_pangkalan_turun = $("#id_pangkalan_tujuan").val();
    var is_transit = $("#is_transit").is(":checked") ? 1 : 0;

    var harga = Number($("#harga_tiket").val().replace(/,/g, ''));
    $('#frm_pesan').trigger('reset');
    $('div.alert').remove();
    if(id_berangkat){
        if(kursi.length){
            if(harga != "" && harga != null && !isNaN(harga)){
                if(harga > 500000 || harga < 10000){
                    $("#harga_tiket").val(harga).focus().select();
                    alert("Harga tiket tidak valid!");
                }else{
                    clearInterval(maketReloader);
                    $.post(
                        urlPemesanan + 'form_pemesanan/normal',
                        {jml:kursi.length, kursi:kursi, id_berangkat:id_berangkat, id_pangkalan_turun:id_pangkalan_turun, is_transit: is_transit},
                        function(data){
                            if(data){
                                $("form#frm_pesan").empty().html(data);
                                if(ticketingLayout){
                                    $('#smartwizard').smartWizard("next");
                                }else{
                                    $("#tiketModal").modal('show');
                                }

                                if($("#nihil-kursi").length){
                                    reloadMaket();
                                    clearInterval(maketReloader);
                                    maketReloader = setInterval(reloadMaket, 10000);
                                }

                                $('html, body').animate({scrollTop: 0}, 'slow');
                                changeTotalPemesanan();
                            }
                        }
                    );
                }
            }else{
                $("#harga_tiket").focus().select();
                alert("Harga belum ditentukan!");
            }
        }else{
            alert("Silahkan pilih kursi!");
        }
    }else{
        alert("Silahkan pilih keberangkatan terlebih dahulu!");
    }
}

function pesanTiketTambahan() {
    isTambahan = true;
    kursi = [];
    $(".btn-kursi").removeClass('btn-selected');
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    var id_pangkalan_turun = $("#id_pangkalan_tujuan").val();
    var is_transit = $("#is_transit").is(":checked") ? 1 : 0;

    var harga = Number($("#harga_tiket").val().replace(/,/g, ''));
    kursi.push("xxx");
    $('#frm_pesan').trigger('reset');
    $('div.alert').remove();
    if(id_berangkat && kursi.length){
            if(harga != "" && harga != null && !isNaN(harga)){
                if(harga > 500000 || harga < 10000){
                    $("#harga_tiket").val(harga).focus().select();
                    alert("Harga tiket tidak valid!");
                }else{
                    clearInterval(maketReloader);
                    $.post(
                        urlPemesanan + 'form_pemesanan/tambahan',
                        {jml:1, kursi:kursi, id_berangkat:id_berangkat, id_pangkalan_turun:id_pangkalan_turun, is_transit: is_transit},
                        function(data){
                            if(data){
                                $("form#frm_pesan").empty().html(data);
                                if(ticketingLayout){
                                    $('#smartwizard').smartWizard("next");
                                }else{
                                    $("#tiketModal").modal('show');
                                }

                                if($("#nihil-kursi").length){
                                    reloadMaket();
                                    clearInterval(maketReloader);
                                    maketReloader = setInterval(reloadMaket, 10000);
                                }

                                $('html, body').animate({scrollTop: 0}, 'slow');
                                changeTotalPemesanan();
                            }
                        }
                    );
                }
            }else{
                $("#harga_tiket").focus().select();
                alert("Harga belum ditentukan!");
            }
    }else{
        alert("Silahkan pilih bus terlebih dahulu!");
    }
}

function confirmTiket(){
    var id_berangkat = $("#layout-maket").data("berangkat");
    var id_berangkat = $("#layout-maket").attr("data-berangkat");
    // var seat = kursi.join();
    var asal = $("#id_pangkalan_asal").val();
    var tujuan = $("#id_pangkalan_tujuan").val();
    var harga = $("#harga_tiket").val();
    var harga_normal = $("#harga_normal").val();
    var is_transit = $("#is_transit").is(":checked") ? 1 : 0;
    var timestamp = new Date().getTime();
    var agen = $("#agen").val();
    var addition = "&asal="+asal+"&tujuan="+tujuan+"&harga="+harga+"&harga_normal="+harga_normal+"&timestamp="+timestamp+"&is_transit="+is_transit;

    if(agen){
        addition += "&agen="+agen;
    }

    if(pake_promo && id_promo){
        addition += "&id_promo="+id_promo;
    }

    if(id_berangkat){
        if(kursi.length > 0 && kursi.length <= limitKursi){
            console.log("pesan");
            if(checkForm("frm_pesan")){
                $.ajax({
                    type : 'POST',
                    dataType : 'json',
                    url : urlPemesanan + 'pesan_tiket/'+id_berangkat,
                    data : $("#frm_pesan").serialize()+addition,
                    success : function(data){
                        if (data.st){
                            $("#tiketModal").modal("hide");
                            reloadMaket();
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);
                            messagesuccess(data.msg);

                            $("#print_tiket").empty().html(data.tiket);
                            if($("#smartwizard").length){
                                $("#smartwizard").smartWizard("next");
                                $("#smartwizard").smartWizard("disableStep", 4);
                                $("#smartwizard ul li:nth-child(4)").removeClass("done");
                            }

                            table.fnDraw(true);
                            if(data.url){
                                printSomething(data.url, null,data.title, true, false, 400);
                            }
                            if(isTambahan){
                                kursi = [];
                                isTambahan = false;
                            }
                           /* var is_printed = printSomething('',data.tiket,data.title);
                            if(!is_printed){
                                $("#print_tiket").print();
                            }*/
                        }else{
                            $('div.alert').remove();
                            if(ticketingLayout == 1){
                                var el = $("#frm_pesan");
                                addAlert(el, "danger", data.msg);
                            }else{
                                modaldialogerror(data.msg);
                            }
                            reloadMaket();
                            clearInterval(maketReloader);
                            maketReloader = setInterval(reloadMaket, 10000);
                        }
                        setHarga();
                        $("#agen").val(defUsername);
                    },
                    error : function (xhr, status, error) {
                        $('div.alert').remove();
                        if(ticketingLayout == 1){
                            var el = $("#frm_pesan");
                            addAlert(el, "danger", xhr.responseText);
                        }else{
                            modaldialogerror(xhr.responseText);
                        }
                        reloadMaket();
                        clearInterval(maketReloader);
                        maketReloader = setInterval(reloadMaket, 10000);
                    }
                });
            }
        }else{
            alert("Silahkan pilih kursi!");
        }
    }else{
        alert("Silahkan pilih keberangkatan terlebih dahulu!");
    }
}

function checkForm(id_form){
    var isValid = true;
    $('#'+id_form+' input').filter('[required]').each(function() {
        if ($(this).val() === '') {
            isValid = false;
            $(this).select().focus();
            console.log(this);
            return false;
        }
        console.log(isValid);
    });
    return isValid;
}

function resetLah() {
    
}