<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_retur_prospek extends CI_Model
{

    public $table = '#_retur_prospek';
    public $id = 'id_retur_prospek';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDataretur_prospek() {
        $this->load->library('datatables');
        $this->datatables->select('id_retur_prospek,tanggal_retur_prospek,id_do_prospek,no_retur_prospek,keterangan');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_retur_prospek.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('retur_prospek/edit_retur_prospek/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteretur_prospek($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_retur_prospek');
        return $this->datatables->generate();
    }

    // get all
    function GetOneRetur_prospek($keyword, $type = 'id_retur_prospek') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $retur_prospek = $this->db->get($this->table)->row();
        return $retur_prospek;
    }

    function Retur_prospekManipulate($model) {
        try {
                $model['tanggal_retur_prospek'] = DefaultTanggalDatabase($model['tanggal_retur_prospek']);
                $model['id_do_prospek'] = ForeignKeyFromDb($model['id_do_prospek']);

            if (CheckEmpty($model['id_retur_prospek'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		SetMessageSession(1, 'Retur_prospek successfull added into database');
		return array("st" => true, "msg" => "Retur_prospek successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_retur_prospek" => $model['id_retur_prospek']));
		SetMessageSession(1, 'Retur_prospek has been updated');
		return array("st" => true, "msg" => "Retur_prospek has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownRetur_prospek() {
        $listretur_prospek = GetTableData($this->table, 'id_retur_prospek', '', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listretur_prospek;
    }

    function Retur_prospekDelete($id_retur_prospek) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_retur_prospek' => $id_retur_prospek));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Retur_prospek has been deleted from database");
    }


}