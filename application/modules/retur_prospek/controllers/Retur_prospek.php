<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Retur_prospek extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_retur_prospek');
        $this->load->model("do_prospek/m_do_prospek");
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K107";
        $header = "K059";
        $title = "Retur Prospek";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        CekModule($module);
        LoadTemplate($model, "retur_prospek/v_retur_prospek_index", $javascript);
        
    } 
    public function get_one_retur_prospek() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_retur_prospek', 'Retur Prospek', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_retur_prospek->GetOneRetur_prospek($model["id_retur_prospek"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Retur Prospek tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdataretur_prospek() {
        header('Content-Type: application/json');
        echo $this->m_retur_prospek->GetDataretur_prospek();
    }
    
    public function create_retur_prospek() 
    {
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("kategori/m_kategori");
        $row=['button'=>'Add'];
        $javascript = array();
    	$module = "K107";
        $header = "K059";
        $row['title'] = "Add Retur Prospek";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_type_retur'] = array(array('id' => 1, 'text' => 'Pengurangan Piutang'), array('id' => 2, 'text' => 'Cash'));
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
	    LoadTemplate($row,'retur_prospek/v_retur_prospek_manipulate', $javascript);       
    }

    public function get_do_prospek_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();
       
        $list_do_prospek=$this->m_do_prospek->GetDropDownDoProspekForRetur($model);
        echo json_encode(array("list_do_prospek"=> DefaultEmptyDropdown($list_do_prospek,"json","DO Prospek"),"st"=>true));
    }
    
    public function retur_prospek_manipulate() 
    {
        $message='';

        $this->form_validation->set_rules('tanggal_retur_prospek', 'Tanggal Retur Prospek', 'trim|required');
        $this->form_validation->set_rules('id_do_prospek', 'Id Do Prospek', 'trim|required');
        $this->form_validation->set_rules('no_retur_prospek', 'No Retur Prospek', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_retur_prospek->Retur_prospekManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_retur_prospek($id=0) 
    {
        $row = $this->m_retur_prospek->GetOneRetur_prospek($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K107";
            $header = "K059";
            $row['title'] = "Edit Retur Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	        LoadTemplate($row,'retur_prospek/v_retur_prospek_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Retur_prospek cannot be found in database");
            redirect(site_url('retur_prospek'));
        }
    }
    
    public function retur_prospek_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_retur_prospek', 'Retur_prospek', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_retur_prospek->Retur_prospekDelete($model['id_retur_prospek']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Retur_prospek.php */