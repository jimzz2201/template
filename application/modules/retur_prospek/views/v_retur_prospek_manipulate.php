<section class="content-header">
    <h1>
        Retur Prospek <?= @$button ?>
        <small>Retur Prospek</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Retur Prospek</li>
        <li class="active">Retur Prospek <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                        <form id="frm_retur_prospek" class="form-horizontal form-groups-bordered validate" method="post">
                        <input type="hidden" name="id_retur_prospek" value="<?php echo @$id_retur_prospek; ?>" /> 
                        <div class="form-group">
                            <?= form_label('Tanggal Retur Prospek', "txt_tanggal_retur_prospek", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'name' => 'tanggal_retur_prospek', 'value' => DefaultDatePicker(@$tanggal_retur_prospek), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_retur_prospek', 'placeholder' => 'Tanggal Retur Prospek')); ?>
                            </div>
                            <?= form_label('Type Retur', "type_retur", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_dropdown(array("name" => "type_retur"), DefaultEmptyDropdown(@$list_type_retur, "json", "Type Retur"), @$type_retur, array('class' => 'form-control select2', 'id' => 'dd_type_retur'));?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= form_label('No Retur Prospek', "txt_no_retur_prospek", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'name' => 'no_retur_prospek', 'value' => @$no_retur_prospek, 'class' => 'form-control', 'id' => 'txt_no_retur_prospek', 'placeholder' => 'No Retur Prospek')); ?>
                            </div>
                        </div>
                        <hr />

                        <div class="form-group">
                            <?= form_label('Type Unit', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori_search', 'class' => 'form-control select2', 'id' => 'dd_id_kategori_search', 'placeholder' => 'Kategori'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$id_kategori); ?>
                            </div>
                            <?= form_label('Unit', "dd_id_unit_search", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-3">
                                <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit_search', 'class' => 'form-control select2', 'id' => 'dd_id_unit_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                            </div>
                            <?= form_label('Customer', "dd_id_customer_search", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-3">
                                <?= form_dropdown(array("name" => "id_customer"), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer, array('class' => 'form-control select2', 'id' => 'dd_id_customer_search')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= form_label('DO Prospek', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-6">
                                <?php
                                    echo form_dropdown(array('type' => 'text', 'name' => 'id_do_prospek', 'class' => 'form-control select2', 'id' => 'dd_id_do_prospek', 'placeholder' => 'DO Prospek'), DefaultEmptyDropdown(@$list_do_prospek, "json", "DO Prospek"), @$id_prospek);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= form_label('No Do Prospek', "txt_id_do_prospek", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'hidden', 'name' => 'id_do_prospek', 'value' => @$id_do_prospek, 'class' => 'form-control', 'id' => 'dd_id_do_prospek', 'placeholder' => 'Id Do Prospek')); ?>
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'id_do_prospek', 'value' => @$no_do_prospek, 'class' => 'form-control', 'id' => 'no_do_prospek', 'placeholder' => 'No Do Prospek')); ?>
                            </div>
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-primary" id="pilih_do" >Pilih DO Prospek</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= form_label('No Buka Jual', "txt_tanggal_retur_prospek", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'nomor_buka_jual', 'placeholder' => 'Nomor Buka Jual')); ?>
                            </div>
                            <?= form_label('Customer', "nama_customer", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'nama_customer', 'placeholder' => 'Nama Customer')); ?>
                            </div>
                        </div>
                        <hr />
                        <div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr style="height:30px;background-color: blue;color:#FFFFFF;">
                                    <th>Unit</th>
                                    <th>Type Unit</th>
                                    <th>Kategori Unit</th>
                                    <th>VIN Num</th>
                                    <th>VIN</th>
                                    <th>Manuvacture Code</th>
                                    <th>Engine No</th>
                                    <th>No BPKB</th>
                                    <th>No Polisi</th>
                                    <th>Posisi</th>
                                    <th>Pool</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="serial_unit_list">
                                <?php
                                    if (!CheckEmpty(@$id_retur_prospek)) {
                                        foreach(@$list_unit_serial as $dt){
                                            $checked = ($dt['date_do_out'] == null) ? '' : 'checked';
                                            $table .= "<tr onclick='selectRow(".$dt['id_unit_serial'].")'>
                                                <td>".$dt['nama_unit']."</td>
                                                <td>".$dt['nama_type_unit']."</td>
                                                <td>".$dt['nama_kategori']."</td>
                                                <td>".$dt['vin_number']."</td>
                                                <td>".$dt['vin']."</td>
                                                <td>".$dt['manufacture_code']."</td>
                                                <td>".$dt['engine_no']."</td>
                                                <td>".$dt['no_bpkb']."</td>
                                                <td>".$dt['no_polisi']."</td>
                                                <td>".($dt['type_do'] == 3 ? 'Customer' : ($dt['type_do'] == 2 ? 'Keluar' : 'Masuk'))."</td>
                                                <td>".$dt['nama_pool']."</td>
                                                <td><input type='checkbox' style='width:20px;' class='selectUnit' " . $checked . " onclick='selectUnit(".$dt['id_unit_serial'].")' id='unit".$dt['id_unit_serial']."'></td>
                                            </tr>";
                                        }
                                        echo $table;
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="areaUnit">
                        <?php
                            if (!CheckEmpty(@$id_retur_prospek)) {
                                foreach(@$list_unit_serial as $dt){
                                    if($dt['date_do_out'] != null)
                                        echo "<input type='hidden' name='id_serial_unit[]' value='" . $dt['id_unit_serial'] . "' id='unitInput" . $dt['id_unit_serial'] . "'>";
                                }
                            }
                        ?>
                        </div>
                        <hr />
                        <div class="form-group">
                            <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => "", 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <a href="<?php echo base_url().'index.php/retur_prospek' ?>" class="btn btn-default"  >Cancel</a>
                            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                        </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });
        UpdateDOProspek();
        searchListDoProspek();
    });
    $(document).ready(function () {

        <?php if (CheckEmpty(@$id_retur_prospek)) { ?>
                    $(".select2").select2();
        <?php } else { ?>
                    $(".select2").select2();
                    <?php if (!CheckEmpty(@$is_view)) { ?>
                            $("input").attr("readonly", true);
                            $("textarea").attr("readonly", true);
                    <?php } ?>
        <?php } ?>
        $('#dd_id_customer_search').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $("#dd_id_unit_search, #dd_id_kategori_search , #dd_id_customer_search").change(function () {
            var id = $(this).attr("id");
            var id_unit_search = $("#dd_id_unit_search").val();
            if (id == "dd_id_kategori_search")
            {
                id_unit_search = 0;
                RefreshUnit();

            }
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/retur_prospek/get_do_prospek_list',
                dataType: 'json',
                data: {
                    id_kategori: $("#dd_id_kategori_search").val(),
                    id_unit: id_unit_search,
                    id_customer: $("#dd_id_customer_search").val()
                },
                success: function (data) {
                    $("#dd_id_do_prospek").empty();
                    if (data.st)
                    {
                        $("#dd_id_do_prospek").select2({data: data.list_do_prospek});
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        })
    })
    function RefreshUnit()
    {
        var id_kategori = $("#dd_id_kategori_search").val();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_dropdown_unit',
            dataType: 'json',
            data: {
                id_kategori: id_kategori
            },
            success: function (data) {
                $("#dd_id_unit_search").empty();
                if (data.st)
                {
                    $("#dd_id_unit_search").select2({data: data.list_unit})
                }
                // ClearFormDetail();
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });

    }
    function searchListDoProspek () {
        LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/retur_prospek/get_do_prospek_list',
                dataType: 'json',
                data: {
                    id_kategori: $("#dd_id_kategori_search").val(),
                    id_unit: $("#dd_id_unit_search").val(),
                    id_customer: $("#dd_id_customer_search").val()
                },
                success: function (data) {
                    $("#dd_id_do_prospek").empty();
                    if (data.st)
                    {
                        $("#dd_id_do_prospek").select2({data: data.list_do_prospek});
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
    }

    function UpdateDOProspek()
    {

        $("#dd_id_do_prospek").change(function () {
            var id_do_prospek = $("#dd_id_do_prospek").val();
            var type_do = $("#dd_type_do").val();
            if (id_do_prospek != 0)
            {
                LoadBar.show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_prospek/get_one_do_prospek',
                    dataType: 'json',
                    data: {
                        id_do_prospek: id_do_prospek
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            $("#no_do_prospek").val(data.obj.nomor_do_prospek);
                            $("#nomor_buka_jual").val(data.obj.nomor_buka_jual);
                            $("#id_cust").val(data.obj.id_customer);
                            $("#areado").html("");
                            jumlahgenerate = 0;
                            showListUnit(2, data.obj.id_buka_jual, data.obj.id_customer);
                        } else
                        {
                            ClearFormDetail();
                        }
                        LoadBar.hide();
                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                        ClearFormDetail();
                        LoadBar.hide();
                    }
                });
            } else
            {
                ClearFormDetail();
                LoadBar.hide();
            }
        })
    }

    function showListUnit(type_do, id_buka_jual, id_cust=0) {
        
        $.ajax({
            type: 'GET',
            url: baseurl + 'index.php/unit/get_list_unit_serial',
            data: {
                type_do: type_do,
                id_buka_jual: id_buka_jual,
                id_cust: id_cust
            },
            success: function (data) {
                // console.log(data);
                $("#serial_unit_list").html(data);
            }
        })
    }

    function selectRow(id) {
        var chk = $("#unit" + id).prop('checked');
        if(chk) {
            $("#unit" + id).prop('checked', false);
        } else {
            $("#unit" + id).prop('checked', true);

        }
        selectUnit(id);
    }

    function selectUnit(id_unit_serial) {
        var chk = $("#unit" + id_unit_serial).prop('checked');
        if(chk == true){
            var idUnit = "<input type='hidden' name='id_serial_unit[]' value='" + id_unit_serial + "' id='unitInput" + id_unit_serial + "'>";
            $("#areaUnit").append(idUnit);
        }else{
            $("#unitInput" + id_unit_serial).remove();
        }
    }

    $("#frm_retur_prospek").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/retur_prospek/retur_prospek_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/retur_prospek';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>