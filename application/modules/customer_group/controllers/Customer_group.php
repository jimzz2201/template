<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_group extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_customer_group');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K078";
        $header = "K001";
        $title = "Customer Group";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	CekModule($module);
	LoadTemplate($model, "customer_group/v_customer_group_index", $javascript);
        
    } 
    public function get_one_customer_group() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_group_customer', 'Customer Group', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_customer_group->GetOneCustomer_group($model["id_group_customer"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Customer Group tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatacustomer_group() {
        header('Content-Type: application/json');
        echo $this->m_customer_group->GetDatacustomer_group();
    }
    
    public function create_customer_group() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	$module = "K078";
        $header = "K001";
        $row['title'] = "Add Customer Group";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	LoadTemplate($row,'customer_group/v_customer_group_manipulate', $javascript);       
    }
    
    public function customer_group_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('nama_group_customer', 'Nama Group Customer', 'trim|required');
	$this->form_validation->set_rules('jumlah_kunjungan', 'Jumlah Kunjungan', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_customer_group->Customer_groupManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_customer_group($id=0) 
    {
        $row = $this->m_customer_group->GetOneCustomer_group($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	    $module = "K078";
            $header = "K001";
            $row['title'] = "Edit Customer Group";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    LoadTemplate($row,'customer_group/v_customer_group_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Customer_group cannot be found in database");
            redirect(site_url('customer_group'));
        }
    }
    
    public function customer_group_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_group_customer', 'Customer_group', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_customer_group->Customer_groupDelete($model['id_group_customer']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Customer_group.php */