<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_customer_group extends CI_Model
{

    public $table = '#_customer_group';
    public $id = 'id_group_customer';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatacustomer_group() {
        $this->load->library('datatables');
        $this->datatables->select('id_group_customer,nama_group_customer,jumlah_kunjungan,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_customer_group.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('customer_group/edit_customer_group/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletecustomer_group($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_group_customer');
        return $this->datatables->generate();
    }

    // get all
    function GetOneCustomer_group($keyword, $type = 'id_group_customer') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $customer_group = $this->db->get($this->table)->row();
        return $customer_group;
    }

    function Customer_groupManipulate($model) {
        try {
                $model['jumlah_kunjungan'] = DefaultCurrencyDatabase($model['jumlah_kunjungan']);

            if (CheckEmpty($model['id_group_customer'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		SetMessageSession(1, 'Customer_group successfull added into database');
		return array("st" => true, "msg" => "Customer_group successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_group_customer" => $model['id_group_customer']));
		SetMessageSession(1, 'Customer_group has been updated');
		return array("st" => true, "msg" => "Customer_group has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownCustomer_group() {
        $listcustomer_group = GetTableData($this->table, 'id_group_customer', 'nama_group_customer', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listcustomer_group;
    }

    function Customer_groupDelete($id_group_customer) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_group_customer' => $id_group_customer));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Customer_group has been deleted from database");
    }


}