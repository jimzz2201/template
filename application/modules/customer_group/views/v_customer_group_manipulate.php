<section class="content-header">
    <h1>
    Customer Group <?= @$button ?>
        <small><?php echo @$title?></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><?php echo @$title?></li>
        <li class="active"><?php echo @$title?> <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_customer_group" class="form-horizontal form-groups-bordered validate" method="post">
                            <input type="hidden" name="id_group_customer" value="<?php echo @$id_group_customer; ?>" /> 
                            <div class="form-group">
                                <?= form_label('Nama Group Customer', "txt_nama_group_customer", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-8">
                                    <?= form_input(array('type' => 'text', 'name' => 'nama_group_customer', 'value' => @$nama_group_customer, 'class' => 'form-control', 'id' => 'txt_nama_group_customer', 'placeholder' => 'Nama Group Customer')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Jumlah Kunjungan', "txt_jumlah_kunjungan", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-8">
                                    <?= form_input(array('type' => 'text', 'name' => 'jumlah_kunjungan', 'value' => @$jumlah_kunjungan, 'class' => 'form-control', 'id' => 'txt_jumlah_kunjungan', 'placeholder' => 'Jumlah Kunjungan')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-8">
                                    <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="<?php echo base_url().'index.php/customer_group' ?>" class="btn btn-default"  >Cancel</a>
                                <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
$("#frm_customer_group").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/customer_group/customer_group_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/customer_group';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>