<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pegawai extends CI_Model {

    public $table = '#_pegawai';
    public $id = 'id_pegawai';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatapegawai() {
        $this->load->library('datatables');
        $this->datatables->select('#_pegawai.id_pegawai,#_pegawai.*,parent.nama_pegawai as supervisor,nama_kota,nama_jabatan');
        
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_kota', 'dgmi_pegawai.id_kota = dgmi_kota.id_kota');
        $this->datatables->join('#_pegawai parent', 'parent.id_pegawai = #_pegawai.id_pegawai_parent',"left");
        
        $this->datatables->join('#_jabatan', 'dgmi_pegawai.id_jabatan = dgmi_jabatan.id_jabatan');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('pegawai/edit_pegawai/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletepegawai($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_pegawai');
        return $this->datatables->generate();
    }

    // get all
    function GetOnePegawai($keyword, $type = 'id_pegawai') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $this->db->join("#_user","#_user.id_user=#_pegawai.id_user","left");
        $this->db->select("#_pegawai.*,#_user.username,#_user.arr_cabang,#_user.id_cabang");
        $pegawai = $this->db->get($this->table)->row();
        return $pegawai;
    }

    function PegawaiManipulate($model) {
        try {
            $id_user=null;
            if (!CheckEmpty(@$model['username'])) {
                $user = array();
                $user['created_date'] = GetDateNow();
                $user['username'] = $model['username'];
                $user['status'] = $model['status'];
                $user['nama_user'] = $model['nama_pegawai'];
                $user['id_group'] = ForeignKeyFromDb($model['id_group']);
                $user['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $user['kode_user'] = AutoIncrement("#_user", "P", "kode_user", 3);
                $user['created_by'] = ForeignKeyFromDb(GetUserId());
                $user['password_user'] = sha1($model['password_user']);
                $array_cabang = (array) @$model['arr_cabang'];
                $isinclude=false;
                foreach ($array_cabang as $cabsatuan) {
                    if ($cabsatuan == $model['id_cabang']) {
                        $isinclude = true;
                    }
                }
                if (!$isinclude) {
                    $array_cabang[] = $model['id_cabang'];
                }
                $user['arr_cabang'] =@$model['allow_all_cabang']=="1"?"all": implode(",", $array_cabang);
                $this->db->insert("#_user", $user);
                $id_user = $this->db->insert_id();
                
                if (!CheckEmpty($model['allow_all_cabang'])) {
                    $this->db->delete("#_user_cabang", array("id_user" => $id_user));
                } else {

                    foreach ($array_cabang as $cabsatuan) {
                        $this->db->from("#_user_cabang");
                        $cab = $this->db->where(array("id_cabang" => $cabsatuan, "id_user" => $id_user))->get()->row();
                        if (!$cab) {
                            $this->db->insert("#_user_cabang", array("id_user" => $id_user, "id_cabang" => $cabsatuan));
                        }
                    }
                    $this->db->from("#_user_cabang");
                    if (count($array_cabang) > 0) {
                        $this->db->where_not_in("id_cabang", $array_cabang);
                    }
                    $this->db->where(array("id_user" => $id_user));
                    $this->db->delete();
                }
            } else if (!CheckEmpty(@$model['id_user'])) {
                $id_user = @$model['id_user'];
                $user['nama_user'] = $model['nama_pegawai'];
                $user['status'] = $model['status'];
                $this->db->update("#_user", $user, array("id_user" => $model['id_user']));
            }
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $pegawai['id_kota'] = ForeignKeyFromDb($model['id_kota']);
            $pegawai['id_jabatan'] = ForeignKeyFromDb($model['id_jabatan']);
            $pegawai['id_pegawai_parent'] = ForeignKeyFromDb($model['id_pegawai_parent']);
            $pegawai['nik'] = $model['nik'];
            $pegawai['nama_pegawai'] = $model['nama_pegawai'];
            $pegawai['telp_pegawai'] = $model['telp_pegawai'];
            $pegawai['email_pegawai'] = $model['email_pegawai'];
            $pegawai['alamat'] = $model['alamat'];
            $pegawai['status'] = $model['status'];
            $pegawai['jenis_identitas'] = $model['jenis_identitas'];
            $pegawai['no_identitas'] = $model['no_identitas'];
            $pegawai['kode_pos'] = $model['kode_pos'];
            $pegawai['status'] = $model['status'];
            $pegawai['id_user']=$id_user;
            $id_pegawai = null;
            if (CheckEmpty($model['id_pegawai'])) {
                $pegawai['created_date'] = GetDateNow();
                $pegawai['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $pegawai);
                $id_pegawai = $this->db->insert_id();
            } else {
                $pegawai['updated_date'] = GetDateNow();
                $pegawai['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->query("SET FOREIGN_KEY_CHECKS = 0");
                $this->db->update($this->table, $pegawai, array("id_pegawai" => $model['id_pegawai']));
                $id_pegawai = $model['id_pegawai'];
            }
            $array_cabang = @$model['id_cabang'];
            if (CheckEmpty($array_cabang)) {
                $array_cabang = [];
            }

          

         
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data pegawai sudah ditambahkan / diupdate ke dalam database");
                return array("st" => true, "msg" => "Data sudah ditambahkan / diupdate ke dalam system");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownPegawai($type="json",$list_cabang=[]) {
        if(count($list_cabang)>0)
        {
            $this->db->join("#_user", "#_user.id_user=#_pegawai.id_user");
            $this->db->join("#_user_cabang", "#_user_cabang.id_user=#_pegawai.id_user");
            $this->db->group_start();
            $this->db->where_in("#_user_cabang.id_cabang", $list_cabang);
            $this->db->or_where("#_user.arr_cabang", "all");
            $this->db->group_end();
            $this->db->group_by("#_user.id_user");
        }
        
        $listpegawai = GetTableData($this->table, 'id_pegawai', 'nama_pegawai', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null),$type);
        return $listpegawai;
    }
    
    
    function GetDropDownSupervisor() {
        
        $this->db->from("#_pegawai");
        $this->db->where(array("id_pegawai_parent !="=>null));
        $this->db->select("id_pegawai_parent");
        $listsupervisor=$this->db->get()->result_array();
        $listsupervisor= array_column($listsupervisor, "id_pegawai_parent");
        if(count($listsupervisor)>0)
        $this->db->where_in("id_pegawai",$listsupervisor);
        $listpegawai = GetTableData($this->table, 'id_pegawai', 'nama_pegawai', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null),"json",[]);
        return $listpegawai;
    }

    function PegawaiDelete($id_pegawai) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_pegawai' => $id_pegawai));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Pegawai has been deleted from database");
    }

}
