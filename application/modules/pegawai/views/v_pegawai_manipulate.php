<section class="content-header">
    <h1>
        Pegawai <?= @$button ?>
        <small>Pegawai</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Pegawai</li>
        <li class="active">Pegawai <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_pegawai" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_pegawai" value="<?php echo @$id_pegawai; ?>" /> 
                                <input type="hidden" name="id_user" value="<?php echo @$id_user; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Nik', "txt_nik", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'nik', 'value' => @$nik, 'class' => 'form-control', 'id' => 'txt_nik', 'placeholder' => 'Nik')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Pegawai', "txt_nama_pegawai", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_pegawai', 'value' => @$nama_pegawai, 'class' => 'form-control', 'id' => 'txt_nama_pegawai', 'placeholder' => 'Nama Pegawai')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Telp Pegawai', "txt_telp_pegawai", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'telp_pegawai', 'value' => @$telp_pegawai, 'class' => 'form-control', 'id' => 'txt_telp_pegawai', 'placeholder' => 'Telp Pegawai')); ?>
                                    </div>
                                    <?= form_label('Email Pegawai', "txt_email_pegawai", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'email_pegawai', 'value' => @$email_pegawai, 'class' => 'form-control', 'id' => 'txt_email_pegawai', 'placeholder' => 'Email Pegawai')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_alamat", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat', 'rows' => '3', 'cols' => '10', 'value' => @$alamat, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Supervisor', "txt_telp_pegawai", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$id_pegawai_parent, "name" => "id_pegawai_parent"), DefaultEmptyDropdown(@$list_pegawai,"json","Pegawai"), @$id_pegawai_parent, array('class' => 'form-control', 'id' => 'dd_id_pegawai_parent')); ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <?= form_label('Kota', "txt_id_kota", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kota', 'class' => 'form-control', 'id' => 'dd_id_kota', 'placeholder' => 'kota'), DefaultEmptyDropdown($list_kota, "json", "Kota"), @$id_kota); ?>
                                    </div>
                                    <?= form_label('Kode Pos', "txt_kode_pos", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">

                                        <?= form_input(array('type' => 'text', 'name' => 'kode_pos', 'value' => @$kode_pos, 'class' => 'form-control', 'id' => 'txt_kode_pos', 'placeholder' => 'Kode Pos')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Jenis Identitas', "txt_jenis_identitas", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$jenis_identitas, "name" => "jenis_identitas"), array('1' => 'KTP', '0' => 'SIM'), @$jenis_identitas, array('class' => 'form-control', 'id' => 'jenis_identitas')); ?>
                                    </div>
                                    <?= form_label('No Identitas', "txt_no_identitas", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'no_identitas', 'value' => @$no_identitas, 'class' => 'form-control', 'id' => 'txt_no_identitas', 'placeholder' => 'No Identitas')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Jabatan', "txt_id_jabatan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_jabatan', 'class' => 'form-control', 'id' => 'dd_id_jabatan', 'placeholder' => 'Jabatan'), DefaultEmptyDropdown($list_jabatan, "json", "Jabatan"), @$id_jabatan); ?>
                                    </div>
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>
                                
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Username', "txt_username", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $merge = array();
                                        if (!CheckEmpty(@$username)) {
                                            $merge['disabled'] = true;
                                        } else {
                                            $merge['name'] = 'username';
                                        }
                                        ?>

                                        <?= form_input(array_merge($merge, array('type' => 'text','autocomplete'=>'off', 'value' => @$username, 'class' => 'form-control', 'id' => 'txt_username', 'placeholder' => 'Username'))); ?>
                                    </div>
                                    <?php if (CheckEmpty(@$username)) { ?>
                                    <?= form_label('Group', "txt_id_group", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_group', 'class' => 'form-control', 'id' => 'txt_id_group', 'placeholder' => 'Group'), DefaultEmptyDropdown(@$list_group, "obj", "Group"), @$id_group); ?>
                                    </div>
                                    <?php }?>
                                </div>
                                <?php if (CheckEmpty(@$username)) { ?>
                                    <div class="form-group">
                                        <?= form_label('Password User', "txt_password_user", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'password','autocomplete'=>'off', 'name' => 'password_user', 'value' => @$password_user, 'class' => 'form-control', 'id' => 'txt_password_user', 'placeholder' => 'Password User')); ?>
                                        </div>
                                        <?= form_label('Default Cabang', "txt_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_cabang', 'class' => 'form-control', 'id' => 'dd_id_cabang_default', 'placeholder' => 'cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$id_cabang); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <?= form_label('Akses semua Cabang', "dd_allow_all_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_dropdown(array("name" => "allow_all_cabang"), GetYaTidak(), @$allow_all_cabang, array('class' => 'form-control', 'id' => 'dd_allow_all_cabang')); ?>
                                        </div>
                                        <div id="list_cabang" style="display:none">
                                            <?= form_label('Cabang', "txt_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                            <div class="col-sm-5">
                                                <?= form_dropdown(array("name" => "arr_cabang[]"), @$list_cabang, @$arr_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang', 'multiple' => 'multiple')); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/pegawai' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
    $(document).ready(function () {
        $("select").select2();
        $("#dd_allow_all_cabang").change(function () {
            var allow_all_cabang = $(this).val();
            if (allow_all_cabang == 0) {
                $("#list_cabang").show();
            } else {
                $("#list_cabang").hide();
            }
        }).change();

    })

    $("#frm_pegawai").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pegawai/pegawai_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/pegawai';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>