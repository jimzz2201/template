<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pegawai extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_pegawai');
        $this->load->model("jabatan/m_jabatan");
        $this->load->model("kota/m_kota");
        $this->load->model("cabang/m_cabang");
        $this->load->model("user/m_user");
    }

    public function get_dropdown_pegawai() {
        header('Content-Type: application/json');
        $model = $this->input->post();
        $id_cabang = $model['id_cabang'];
        $listidcabang = [];
        if (!CheckEmpty($id_cabang)) {
            $listidcabang[] = $id_cabang;
        }
        $list_pegawai = $this->m_pegawai->GetDropDownPegawai("json", $listidcabang);

        echo json_encode(array("list_pegawai" => DefaultEmptyDropdown($list_pegawai, "json", "Pegawai"), "st" => true));
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K062";
        $header = "K001";
        $title = "Pegawai";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        LoadTemplate($model, "pegawai/v_pegawai_index", $javascript);
    }

    public function get_one_pegawai() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_pegawai', 'Pegawai', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_pegawai->GetOnePegawai($model["id_pegawai"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Pegawai tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdatapegawai() {
        header('Content-Type: application/json');
        echo $this->m_pegawai->GetDatapegawai();
    }

    public function create_pegawai() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K062";
        $header = "K001";
        $row['title'] = "Add Pegawai";
        $this->load->model("cabang/m_cabang");
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("user/m_user");
        CekModule($module);
        $row['form'] = $header;
        $row['list_group'] = GetTableData('#_group', 'id_group', 'nama_group', array(), 'obj');
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $row['formsubmenu'] = $module;
        $row['list_jabatan'] = $this->m_jabatan->GetDropDownJabatan();
        $row['list_kota'] = $this->m_kota->GetDropDownKota();
        $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
        LoadTemplate($row, 'pegawai/v_pegawai_manipulate', $javascript);
    }

    public function pegawai_manipulate() {
        $message = '';

        $this->form_validation->set_rules('nik', 'Nik', 'trim|required');
        $this->form_validation->set_rules('nama_pegawai', 'Nama Pegawai', 'trim|required');
        $this->form_validation->set_rules('telp_pegawai', 'Telp Pegawai', 'trim|required');
        $this->form_validation->set_rules('email_pegawai', 'Email Pegawai', 'trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim');
        $this->form_validation->set_rules('id_kota', 'Kota', 'trim|required');
        $this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'trim');
        $this->form_validation->set_rules('no_identitas', 'No Identitas', 'trim');
        $this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim');
        $this->form_validation->set_rules('id_jabatan', 'Id Jabatan', 'trim|required');
        $model = $this->input->post();

        if (!CheckEmpty(@$model['username'])) {
            $this->form_validation->set_rules('username', 'username', 'trim|required|is_unique[#_user.username]');
            $this->form_validation->set_rules('password_user', 'password user', 'trim|required');
            $model['password_user'] = sha1(@$model['password_user']);
        }

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $status = $this->m_pegawai->PegawaiManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_pegawai($id = 0) {
        $row = $this->m_pegawai->GetOnePegawai($id);
        $this->load->model("cabang/m_cabang");
        $this->load->model("user/m_user");
        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K062";
            $header = "K001";
            $row['title'] = "Edit Pegawai";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->model("pegawai/m_pegawai");
            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
            $row['list_group'] = GetTableData('#_group', 'id_group', 'nama_group', array(), 'obj');
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_jabatan'] = $this->m_jabatan->GetDropDownJabatan();
            $row['list_kota'] = $this->m_kota->GetDropDownKota();
            LoadTemplate($row, 'pegawai/v_pegawai_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Pegawai cannot be found in database");
            redirect(site_url('pegawai'));
        }
    }

    public function pegawai_delete() {
        $message = '';
        $this->form_validation->set_rules('id_pegawai', 'Pegawai', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_pegawai->PegawaiDelete($model['id_pegawai']);
        }

        echo json_encode($result);
    }

}

/* End of file Pegawai.php */