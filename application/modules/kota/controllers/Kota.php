<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kota extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_kota');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K045";
        $header = "K001";
        $title = "Kota";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "kota/v_kota_index", $javascript);
        
    } 
    public function get_one_kota() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_kota', 'Kota', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_kota->GetOneKota($model["id_kota"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Kota tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatakota() {
        header('Content-Type: application/json');
        echo $this->m_kota->GetDatakota();
    }
    
    public function create_kota() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	    $module = "K045";
        $header = "K001";
        $row['title'] = "Add Kota";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	    $this->load->view('kota/v_kota_manipulate', $row);       
    }
    
    public function kota_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_kota', 'Kode Kota', 'trim|required');
	$this->form_validation->set_rules('nama_kota', 'Nama Kota', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_kota->KotaManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_kota($id=0) 
    {
        $row = $this->m_kota->GetOneKota($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K045";
            $header = "K001";
            $row['title'] = "Edit Kota";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	        $this->load->view('kota/v_kota_manipulate', $row);
        } else {
            SetMessageSession(0, "Kota cannot be found in database");
            redirect(site_url('kota'));
        }
    }
    
    public function kota_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_kota', 'Kota', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_kota->KotaDelete($model['id_kota']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Kota.php */