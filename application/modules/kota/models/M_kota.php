<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kota extends CI_Model
{

    public $table = '#_kota';
    public $id = 'id_kota';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatakota() {
        $this->load->library('datatables');
        $this->datatables->select('id_kota,kode_kota,nama_kota');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_kota.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editkota($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletekota($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_kota');
        return $this->datatables->generate();
    }

    // get all
    function GetOneKota($keyword, $type = 'id_kota') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $kota = $this->db->get($this->table)->row();
        return $kota;
    }

    function KotaManipulate($model) {
        try {

            if (CheckEmpty($model['id_kota'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		return array("st" => true, "msg" => "Kota successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_kota" => $model['id_kota']));
		return array("st" => true, "msg" => "Kota has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownKota() {
        $listkota = GetTableData($this->table, 'id_kota', 'nama_kota', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listkota;
    }

    function KotaDelete($id_kota) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_kota' => $id_kota));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Kota has been deleted from database");
    }


}