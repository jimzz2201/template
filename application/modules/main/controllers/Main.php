<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function message() {
        LoadTemplate(array(), "main/message_view", array());
    }

    public function sentemail($id) {
        $this->load->model("m_email");
        $this->m_email->SendEmailActivitation($id);
    }

    public function LGIActivation() {

        $this->load->model("register/m_register");
        $this->m_register->LGIActivate();
    }

    public function new_password_action() {
        $model = $this->input->post();

        $message = '';
        $data = array();
        $data['st'] = false;

        $this->form_validation->set_rules('password', "Password", 'required');
        $this->form_validation->set_rules('confirm', "Confirm Password", 'required|matches[password]');
        $this->form_validation->set_rules('pin', "Pin", 'required|exact_length[6]|numeric');
        $this->form_validation->set_rules('confirm_pin', "Confirm", 'required|matches[pin]');
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            if ($model['user_id'] != GetUserId()) {
                $message .= 'This action cannot be completed because you need to refresh your account<br/>';
            } else {
                $this->load->model('user/m_user');
                $this->m_user->set_new_password($model['password'], $model['pin']);
                $this->session->set_userdata("ischangepassword", false);
            }
            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
        exit;
    }

    public function newpassword() {

        $session = $this->session->userdata('ischangepassword');

        if (!($session != null && $session == true)) {
            redirect(base_url() . 'index.php/profile');
        }
        LoadTemplate(array(), 'main/vaccount_new_password');
    }

    public function change_password() {


        LoadTemplate(array(), 'main/vaccount_change_password');
    }

    public function change_password_action() {
        $model = $this->input->post();
        $message = '';
        $data['st'] = false;
        $this->load->model("user/m_user");
        $this->form_validation->set_rules('password', "Password", 'required');
        $this->form_validation->set_rules('confirm', "Confirm", 'required|matches[password]');
        $this->form_validation->set_rules('lastpassword', "Last Password", 'required');

        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = validation_errors() . $message;
        } else {
            if ($model['user_id'] != GetUserId()) {
                $message .= 'This action cannot be completed because you need to refresh your account<br/>';
            } else {
                $message = $this->m_user->profile_change_password($model);
            }

            $data['st'] = $message == '';
            $data['msg'] = $message;
            if ($data['st']) {
                SetMessageSession(1, "Password Has been Updated");
            }
        }
        echo json_encode($data);
        exit;
    }

    public function activationdirectly() {
        $email = $this->input->get("email");
        $activation = $this->input->get("activation");
        $this->load->model("user/m_user");


        $message = $this->m_user->CheckActivation($email);
        if ($message == '') {
            $message = $this->m_user->CheckActivation($email);
            $user = $this->m_user->GetOneUser($email, "mlmtbl_user_master.email");

            if ($user->activation_key != $activation) {
                $message .= 'Your Activation Code is wrong<br/>';
            }
            if ($user->activation_key != $activation) {
                $message .= 'Your Activation Code is wrong<br/>';
            } else {
                $user = $this->m_user->GetOneUser($email, "mlmtbl_user_master.email");
                $this->m_user->activate($email, "email");

                if (CheckEmpty($user->password)) {
                    SetUserId($user->id_user);
                    SetUsername($user->username);
                    $this->session->set_userdata('ischangepassword', true);
                    redirect(base_url() . 'index.php/main/newpassword');
                } else {
                    SetMessageSession(1, 'Your Account has been acivated');
                    SetUserId($user->id_user);
                    SetUsername($user->username);
                    redirect(base_url() . 'index.php/main/message');
                }
            }
        }


        echo $message;
    }

    public function TestPassword() {
        $this->load->model("m_email");
        $this->m_email->SendForgotPassword(1);
    }

    public function checktokenautoload() {

        $email = $this->input->get('email');
        $code = $this->input->get('auth');
        $this->load->model('user/m_user');
        if ($this->m_user->AuthUserWithCodemd5($email, $code)) {
            $user = $this->m_user->GetOneUser($email, "email");

            if ($user->status == 2) {
                SetMessageSession(0, "Your account has been disabled by admin, Please contact admin for activate your account");
                redirect('/user/forgot');
            } else {
                SetUserId($user->id_user);
                SetUsername($user->username);
                $this->session->set_userdata('ischangepassword', true);
                redirect('/main/newpassword');
            }
        } else {
            SetMessageSession(0, 'Your Recover Code is expired , you can retry for recover your password again by click form above<br/>');

            redirect('/main/forgot');
        }
    }

    public function checktoken() {
        $model = array();
        $model['title'] = 'Edit Profile';
        $this->load->view("main/vaccount_recover");
    }

    public function sent_forgot_password() {
        $model = $this->input->post();
        $this->form_validation->set_rules('email', "Email", 'required');
        $message = '';
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $this->load->model('user/m_user');
            $user = $this->m_user->GetOneUser($model['email'], 'email');
            if ($user != null) {
                $this->load->model("m_email");
                $this->m_user->forgot_password($user->id_user);
                $this->m_email->SendForgotPassword($user->id_user);
            } else {
                $message .= 'The Email is not registered in our system';
            }
            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
    }

    public function sent_activation() {
        $model = $this->input->post();
        $this->form_validation->set_rules('email', "Email", 'required');
        $message = '';
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $this->load->model('user/m_user');
            $user = $this->m_user->GetOneUser($model['email'], 'email');
            if ($user != null) {
                if ($user->status == 0) {
                    $this->load->model("m_email");
                    $this->m_email->SendEmailActivitation($user->id_user);
                } else {
                    $message .= 'Your account has been activated before';
                }
            } else {
                $message .= 'The Email is not registered in our system';
            }
            $data['st'] = $message == '';
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }

    public function forgot() {
        $this->load->view("logon/forgot_password");
    }

    public function activate_sent() {
        $model = array();
        $model['title'] = 'Edit Profile';
        $this->load->view("main/vaccount_activate_sent");
    }

    public function activate() {
        $model = array();
        $model['title'] = 'Edit Profile';
        $this->load->view("main/vaccount_activate");
    }

    public function activate_process() {
        $message = '';
        $email = $this->input->post("email");
        $activationcode = $this->input->post("activationcode");
        $data['st'] = FALSE;
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('activationcode', "Activation Code", 'required');
        $url = '';
        $this->load->model("user/m_user");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $message = $this->m_user->CheckActivation($email);
            if ($message == '') {
                $user = $this->m_user->GetOneUser($email, "mlmtbl_user_master.email");

                if ($user->activation_key != $activationcode) {
                    $message .= 'Your Activation Code is wrong<br/>';
                } else {
                    $url .= base_url() . 'index.php/main/activationdirectly?email=' . strtolower($email) . '&&activation=' . $activationcode;
                }
            }
            $data['st'] = $message == '';
            $data['url'] = $url;
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }

    public function changepasswordaction() {
        $this->form_validation->set_rules('email', "Email", 'required');
    }

    public function forgot_process() {
        $message = '';
        $email = $this->input->post("email");
        $activationcode = $this->input->post("activationcode");
        $data['st'] = FALSE;
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('activationcode', "Recover Code", 'required');
        $url = '';
        $this->load->model("user/m_user");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $row = $this->m_user->GetOneUser($email, 'email');

            if ($row != null) {

                if ($row->forgot_password == '') {
                    $message .= 'Your Recover Code is expired , you can retry for recover your password again by click form above<br/>';
                }
                if ($message == '') {
                    if ($row->forgot_password != $activationcode) {
                        $message .= 'Your Recover Code is wrong<br/>';
                    } else {
                        $url .= base_url() . 'index.php/main/checktokenautoload?email=' . strtolower($email) . '&&auth=' . md5($activationcode);
                    }
                }
            } else {
                $message .= 'We Cannot found your profil from our database<br/>';
            }
            $data['st'] = $message == '';
            $data['url'] = $url;
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }
    
    public function GenerateHutangLink(){
        $this->db->from("#_outstanding_invoice");
        $this->db->join("#_kpu_detail","#_kpu_detail.id_kpu_detail=#_outstanding_invoice.id_kpu_detail");
        $this->db->join("#_kpu","#_kpu.id_kpu=#_kpu_detail.id_kpu");
        $this->db->group_by("#_kpu.id_kpu");
        $this->db->select("#_kpu.tanggal,sum(#_kpu_detail.subtotal/#_kpu_detail.qty) as total_bayar,#_kpu.id_kpu,#_kpu.nomor_master");
        $listpembayaran=$this->db->get()->result();
       
        
        $this->load->model("pembayaran/M_pembayaran_hutang");
       
        $dbinsert=[];
        foreach ($listpembayaran as $Pembayaran) {
            $model = [];
            $model['tanggal'] = DefaultTanggalDatabase($Pembayaran->tanggal);
            $model['pembayarandok']="BB".$Pembayaran->nomor_master;
           
            $this->db->from("#_pembayaran_utang_master");
            $this->db->where(array("tanggal_transaksi" => $model['tanggal'], "round(total_bayar)" => round($Pembayaran->total_bayar),"dokumen"=>$model['pembayarandok']));
            $pembayarandb = $this->db->get()->row();
            
            if ($pembayarandb) {
                $model['pembayaran_utang_id'] = $pembayarandb->pembayaran_utang_id;
            }
            
           
            
            $model['id_supplier'] = 1;
            $model['id_bank'] = null;
            $model['jumlah_bayar_total'] = $Pembayaran->total_bayar;
            $model['simpanan'] = 0;
            $model['potongan'] = 0;
            $model['biaya'] = 0;
            $this->db->from("#_outstanding_invoice");
            $this->db->join("#_kpu_detail","#_kpu_detail.id_kpu_detail=#_outstanding_invoice.id_kpu_detail");
            $this->db->join("#_kpu","#_kpu.id_kpu=#_kpu_detail.id_kpu");
            $this->db->where(array("tanggal" => $Pembayaran->tanggal,"#_kpu.id_kpu"=>$Pembayaran->id_kpu));
            $this->db->select("(#_kpu_detail.subtotal/#_kpu_detail.qty) as nominal,id_unit_serial");
            
            $listpembayaranunit = $this->db->get()->result();
             $dataset = [];
            $jumlah_bayar=[];
            foreach ($listpembayaranunit as $detunit) {
                $unitpemdet = [];
                $unitpemdet['pembayaran_utang_detail_id'] = null;
                if ($pembayarandb) {
                    $this->db->from("#_pembayaran_utang_detail");
                    $this->db->where(array("pembayaran_utang_id" => $pembayarandb->pembayaran_utang_id, "id_unit_serial" => $detunit->id_unit_serial));
                    $rowdet = $this->db->get()->row();
                    if ($rowdet) {
                        $unitpemdet['pembayaran_utang_detail_id'] = $rowdet->pembayaran_utang_detail_id;
                    }
                }
                

                $unitpemdet['id_unit_serial'] = $detunit->id_unit_serial;

                $unitpemdet['jumlah_bayar'] = DefaultCurrencyDatabase( $detunit->nominal);
                $jumlah_bayar[]=DefaultCurrencyDatabase( $detunit->nominal);;
                $dataset[] = $unitpemdet; 
                
            }
           
            $model['dataset'] = $dataset;
            $model['jumlah_bayar'] = $jumlah_bayar;
           
            $model['jenis_pembayaran'] = "Cash";
            $model['biaya'] = 0;
            $model['potongan'] = 0;
            $dbinsert[]=$model;
            $this->M_pembayaran_hutang->pembayaranhutang($model);
           
        }
        
        
        
        
    }
    
    public function GenerateHutangUnitLink(){
        $this->db->from("#_unit_invoice");
        $this->db->select("tgl_fp_lunas as tanggal");
        $this->db->distinct();
        $listpembayaran=$this->db->get()->result();
        
        
        $this->load->model("pembayaran/M_pembayaran_hutang");
       
        $dbinsert=[];
        foreach ($listpembayaran as $Pembayaran) {
            $model = [];
            $model['tanggal'] = DefaultTanggalDatabase($Pembayaran->tanggal);
            $this->db->from("#_unit_invoice");
            $this->db->select("*");
            $this->db->where(array("tgl_fp_lunas"=>$Pembayaran->tanggal));
            $listpembayaranunit=$this->db->get()->result();
            $totalbayar=0;
            foreach($listpembayaranunit as $det)
            {
                $totalbayar+=$det->nominal;
            }
            
            
           
            $this->db->from("#_pembayaran_utang_master");
            $this->db->where(array("tanggal_transaksi" => $model['tanggal'], "round(total_bayar)" => round($totalbayar)));
            $pembayarandb = $this->db->get()->row();
            
            if ($pembayarandb) {
                $model['pembayaran_utang_id'] = $pembayarandb->pembayaran_utang_id;
            }
            
           
            
            $model['id_supplier'] = 1;
            $model['id_bank'] = null;
            $model['jumlah_bayar_total'] = $Pembayaran->total_bayar;
            $model['simpanan'] = 0;
            $model['potongan'] = 0;
            $model['biaya'] = 0;
            $dataset = [];
            $jumlah_bayar=[];
            foreach ($listpembayaranunit as $detunit) {
                $unitpemdet = [];
                $unitpemdet['pembayaran_utang_detail_id'] = null;
                if ($pembayarandb) {
                    $this->db->from("#_pembayaran_utang_detail");
                    $this->db->where(array("pembayaran_utang_id" => $pembayarandb->pembayaran_utang_id, "id_unit_serial" => $detunit->id_unit_serial));
                    $rowdet = $this->db->get()->row();
                    if ($rowdet) {
                        $unitpemdet['pembayaran_utang_detail_id'] = $rowdet->pembayaran_utang_detail_id;
                    }
                }
                

                $unitpemdet['id_unit_serial'] = $detunit->id_unit_serial;

                $unitpemdet['jumlah_bayar'] = DefaultCurrencyDatabase( $detunit->nominal);
                $jumlah_bayar[]=DefaultCurrencyDatabase( $detunit->nominal);;
                $dataset[] = $unitpemdet; 
                
            }
           
            $model['dataset'] = $dataset;
            $model['jumlah_bayar'] = $jumlah_bayar;
           
            $model['jenis_pembayaran'] = "Cash";
            $model['biaya'] = 0;
            $model['potongan'] = 0;
            $dbinsert[]=$model;
            $this->M_pembayaran_hutang->pembayaranhutang($model);
           
        }
        
        
        
        
    }
    
    
    
    

    public function GenerateUnitLink() {
        $this->load->model("pembayaran/m_pembayaran_piutang");
        $this->db->from("#_view_pembayaran_import");
        $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_view_pembayaran_import.id_buka_jual");
        $this->db->join("#_prospek_detail", "#_prospek_detail.id_prospek=#_buka_jual.id_prospek");

        $this->db->group_by("#_buka_jual.id_prospek");
        $this->db->select("#_buka_jual.id_prospek,#_buka_jual.id_buka_jual,#_prospek_detail.id_prospek_detail");
        $listprospek = $this->db->get()->result();
        $this->load->model("prospek/m_prospek");
        foreach ($listprospek as $prospek) {
            $this->m_prospek->ChangeStatus($prospek->id_prospek);
            $this->db->from("#_unit_serial");
            $this->db->where(array("id_buka_jual" => $prospek->id_buka_jual));
            $listunit = $this->db->get()->result();

            $this->db->update("#_prospek_detail_unit", array("id_unit_serial" => null), array("id_prospek_detail" => $prospek->id_prospek_detail));

            foreach ($listunit as $unit) {
                $this->db->from("#_prospek_detail_unit");
                $this->db->where(array("id_prospek_detail" => $prospek->id_prospek_detail, "id_unit_serial" => null));
                $rowkosong = $this->db->get()->row();
                if ($rowkosong) {
                    $this->db->update("#_prospek_detail_unit", array("id_unit_serial" => $unit->id_unit_serial), array("id_prospek_detail_unit" => $rowkosong->id_prospek_detail_unit));
                }
            }
        }

        $this->db->from("#_view_pembayaran_import");
        $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_view_pembayaran_import.id_buka_jual");
        $this->db->group_by("#_view_pembayaran_import.id_customer,tanggal");
        $this->db->select("tanggal,#_view_pembayaran_import.id_customer,sum(nominal) as total_bayar,#_buka_jual.tanggal_buka_jual");
        $listpembayaran = $this->db->get()->result();
        
       
        $dbinsert=[];
        foreach ($listpembayaran as $Pembayaran) {
            $model = [];
            $model['tanggal'] = DefaultTanggalDatabase($Pembayaran->tanggal);
            if(CheckEmpty( $model['tanggal']))
            {
                $model['tanggal']=$Pembayaran->tanggal_buka_jual;
            }
            $this->db->from("#_pembayaran_piutang_master");
            $this->db->where(array("tanggal_transaksi" => $model['tanggal'], "id_customer" => $Pembayaran->id_customer, "round(total_bayar)" => round($Pembayaran->total_bayar)));
            $pembayarandb = $this->db->get()->row();
            
            if ($pembayarandb) {
                $model['pembayaran_piutang_id'] = $pembayarandb->pembayaran_piutang_id;
            }
            
            

            $model['id_customer'] = $Pembayaran->id_customer;
            $model['id_bank'] = null;
            $model['jumlah_bayar_total'] = $Pembayaran->total_bayar;
            $model['simpanan'] = 0;
            $model['potongan'] = 0;
            $model['biaya'] = 0;
            $this->db->from("#_view_pembayaran_import");
            $this->db->where(array("id_customer" => $Pembayaran->id_customer, "tanggal" => $Pembayaran->tanggal));
            $listpembayaranunit = $this->db->get()->result();
            $dataset = [];
            foreach ($listpembayaranunit as $detunit) {
                $unitpemdet = [];
                $unitpemdet['pembayaran_piutang_detail_id'] = null;
                if ($pembayarandb) {
                    $this->db->from("#_pembayaran_piutang_detail");
                    $this->db->where(array("pembayaran_piutang_id" => $pembayarandb->pembayaran_piutang_id, "id_unit_serial" => $detunit->id_unit_serial));
                    $rowdet = $this->db->get()->row();
                    if ($rowdet) {
                        $unitpemdet['pembayaran_piutang_detail_id'] = $rowdet->pembayaran_piutang_detail_id;
                    }
                }

                $unitpemdet['id_unit_serial_pembayaran'] = $detunit->id_unit_serial;

                $unitpemdet['jumlah_bayar'] = DefaultCurrencyDatabase( $detunit->nominal);
                if( DefaultCurrencyDatabase( $detunit->nominal)>0)
                {
                   $dataset[] = $unitpemdet; 
                }
                
            }
            $model['dataset'] = $dataset;
           
            
            $model['id_customer'] = $Pembayaran->id_customer;
            $model['jenis_pembayaran'] = "Cash";
            $model['biaya'] = 0;
            $model['potongan'] = 0;
            $dbinsert[]=$model;
            $this->m_pembayaran_piutang->PembayaranPiutang($model);
           
        }
       
    }

}
