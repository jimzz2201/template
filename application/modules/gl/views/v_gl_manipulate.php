<div class="modal-header">
    Gl <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_gl_account" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_gl_account" value="<?php echo @$id_gl_account; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Gl', "txt_kode_gl_account", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?php
                $mergearray = array();
                if (!CheckEmpty(@$id_gl_account))
                    $mergearray['disabled'] = "disabled";
                ?>
                <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_gl_account, 'name' => 'kode_gl_account', 'class' => 'form-control', 'id' => 'txt_kode_gl_account', 'placeholder' => 'Kode Gl'))); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Gl', "txt_nama_gl_account", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'name' => 'nama_gl_account', 'value' => @$nama_gl_account, 'class' => 'form-control', 'id' => 'txt_nama_gl_account', 'placeholder' => 'Nama Gl')); ?>
            </div>
            <?= form_label('Akun Parent', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$id_parent_gl_account, "name" => "id_parent_gl_account"), DefaultEmptyDropdown(@$list_gl_account, "json", "Parent Akun"), @$id_parent_gl_account, array('class' => 'form-control', 'id' => '$id_parent_gl_account')); ?>
            </div>
        </div>
       
        <div class="form-group">
            <?= form_label('Rugi Laba', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$id_gl_labarugi, "name" => "id_gl_labarugi"), DefaultEmptyDropdown($list_gl_labarugi,"json","Laba Rugi"), @$id_gl_labarugi, array('class' => 'form-control', 'id' => 'id_gl_labarugi')); ?>
            </div>
            <?= form_label('HPP', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$is_hpp, "name" => "is_hpp"), GetYaTidak(), @$is_hpp, array('class' => 'form-control', 'id' => 'is_hpp')); ?>
            </div>
        </div>
        
        <div class="form-group">
            <?= form_label('Type', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$is_header, "name" => "is_header"), array('1' => 'Induk', '0' => 'Akun'), @$is_header, array('class' => 'form-control', 'id' => 'is_header')); ?>
            </div>
           
        </div>
        <div class="form-group">
            <?= form_label('Type Akun', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$id_gl_header, "name" => "id_gl_header"), DefaultEmptyDropdown(@$list_gl_header, "json", "Type Akun"), @$id_gl_header, array('class' => 'form-control', 'id' => 'id_gl_header')); ?>
            </div>
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
       
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $(document).ready(function(){
        $("select").select2();
        
        
    })
    $("#frm_gl_account").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/gl/gl_account_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.reload();
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>