
<section class="content-header">
    <h1>
        Gl
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Gl</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">

                <?php echo anchor("", 'Create', 'class="btn btn-success" id="btt_create"'); ?>
            </div>
        </div>

        <br/>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">
                        <thead>
                        <th style="width:200px;">Kode Akun</th>
                        <th>Nama Akun</th>
                        <th>Type</th>
                        <th>Laba Rugi</th>
                        <!--<th>Type Umum</th>-->
                        <th>Type Akun</th>
                        <th>HPP</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                            <?php RenderViewHtml($list_gl, ""); ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script>
    function EditGl(id_gl) {

        $.ajax({url: baseurl + 'index.php/gl/edit_gl/' + id_gl,
            success: function (data) {
                modalbootstrap(data, "Edit gl", "800px");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    }
    $("#btt_create").click(function () {
        $.ajax({url: baseurl + 'index.php/gl/create_gl',
            success: function (data) {
                modalbootstrap(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });


    })
</script>
<?php

function RenderViewHtml($list_gl, $stringspace) {

    foreach ($list_gl as $gl_satuan) {
        echo "<tr>";
        echo "<td>" . $stringspace . $gl_satuan->kode_gl_account . "</td>";
        echo "<td>$gl_satuan->nama_gl_account</td>";
        echo "<td>" . (CheckEmpty($gl_satuan->is_header) ? "Akun" : "Induk") . "</td>";
        echo "<td>" . $gl_satuan->nama_header_labarugi . "</td>";
       // echo "<td>" . (CheckEmpty($gl_satuan->is_type_umum) ? "Kredit" : "Debit") . "</td>";
        echo "<td>$gl_satuan->nama_gl_header</td>";
        echo "<td>" . (CheckEmpty($gl_satuan->is_hpp) ? "Tidak" : "Ya") . "</td>";
        echo "<td><a class='btn-xs   btn-info' href='javascript:;' onclick=\"EditGl($gl_satuan->id_gl_account)\">Edit</a></td>";
        echo "</tr>";
        if (count($gl_satuan->list_gl) > 0) {
            RenderViewHtml($gl_satuan->list_gl, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $stringspace);
        }
    }
}
?>