<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_gl extends CI_Model {

    public $table = '#_gl_account';
    public $id = 'id_gl_account';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    function GetJurnalBarang()
    {
        $this->db->join("#_barang","#_gl_account.id_gl_account=#_barang.id_gl_account");
        $this->db->not_like("nama_gl_account", "Pemakaian");
        $this->db->distinct();
        $listgl = GetTableData("#_gl_account", 'id_gl_account', 'nama_gl_account', array('#_gl_account.status' => 1),"obj");
        return $listgl;
    }
    function GetSaldoGLbyDate($tanggal,$id_gl_account, $id_cabang = 0, $id_subject = 0)
    {
        $this->db->from("#_buku_besar");

        $where=array("id_gl_account"=>$id_gl_account,"tanggal_buku <" =>DefaultTanggalDatabase($tanggal));
        if(!CheckEmpty($id_subject))
        {
            $where['id_subject']=$id_subject;
        }
        
        $this->db->where($where);
        $this->db->select("ifnull(sum(debet-kredit),0)as jml");
        $row=$this->db->get()->row();
        $saldoawal=$this->GetSaldoAwalGl($id_gl_account, $id_cabang , $id_subject );
        if($row)
        {
            $saldoawal+=$row->jml;
        }
        
        return $saldoawal;
    }
    function GetSaldoAwalGl($id_gl_account, $id_cabang = 0, $id_subject = 0) {
        $this->db->from("#_saldo_awal");
      
        if (is_array($id_gl_account)) {
            $this->db->where_in("id_gl_account", $id_gl_account);
        } else {
            $this->db->where(array("id_gl_account" => $id_gl_account));
        }

        if (!CheckEmpty($id_cabang)) {
            $this->db->where(array("id_cabang" => $id_cabang));
        }
        if (!CheckEmpty($id_subject)) {
            $this->db->where(array("id_subject" => $id_subject));
        }

        $this->db->select("ifnull(sum(saldo_awal),0) as saldo_awal");
        $row = $this->db->get()->row();
        if ($row) {
            return $row->saldo_awal;
        } else {
            return 0;
        }
    }

    function CheckGl($id_gl, $is_header) {
        $message = "";
        if (CheckEmpty($is_header)) {
            $this->db->from("#_gl_account");
            $this->db->where(array("id_parent_gl_account" => $id_gl));
            $row = $this->db->get()->row();
            if ($row != null) {
                $message .= "Akun Ini sudah memiliki Sub Account";
            }
        } else {
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $id_gl));
            $row = $this->db->get()->row();
            if ($row != null) {
                $message .= "Akun Ini sudah memiliki jurnal pada buku besar";
            }
        }
        return $message;
    }

    // datatables
    function GetDatagl() {
        $this->load->library('datatables');
        $this->datatables->select('#_gl_account,id_gl_account as id');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'lian_gl.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editgl($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletegl($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id');
        return $this->datatables->generate();
    }

    // get all
    function GetOneGl($keyword, $type = 'id_gl_account', $isfull = true) {

        $this->db->where($type, $keyword);
        $gl = $this->db->get($this->table)->row();
        return $gl;
    }

    function GlManipulate($model) {
        try {
            $model['id_gl_labarugi'] = ForeignKeyFromDb($model['id_gl_labarugi']);
            $model['is_header'] = DefaultCurrencyDatabase($model['is_header']);
            $model['is_hpp'] = DefaultCurrencyDatabase($model['is_hpp']);
            $model['id_gl_header'] = ForeignKeyFromDb($model['id_gl_header']);
            $model['id_parent_gl_account'] = ForeignKeyFromDb($model['id_parent_gl_account']);
            $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_gl_account'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, "Gl successfull added into database");
                return array("st" => true, "msg" => "Gl successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_gl_account" => $model['id_gl_account']));

                SetMessageSession(1, "Gl Gl has been updated");
                return array("st" => true, "msg" => "Gl has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDataGlAccount($id_gl_account = null) {
        $this->db->from("#_gl_account");
        $this->db->where(array("id_parent_gl_account" => $id_gl_account));
        $this->db->join("#_gl_header", "#_gl_header.id_gl_header=#_gl_account.id_gl_header", "left");
        $this->db->join("#_gl_labarugi", "#_gl_labarugi.id_gl_labarugi=#_gl_account.id_gl_labarugi", "left");
        $this->db->order_by("kode_gl_account");
        $listglaccount = $this->db->get()->result();
        foreach ($listglaccount as $glsatuan) {
            if (!CheckEmpty($glsatuan->is_header)) {
                $glsatuan->list_gl = $this->GetDataGlAccount($glsatuan->id_gl_account);
            } else {
                $glsatuan->list_gl = [];
            }
        }
        return $listglaccount;
    }

    function GetGlHPPPeriod($start, $end, $id_cabang = null) {

        $this->db->from("#_gl_account");
        if (!CheckEmpty(@$id_cabang)) {
            $this->db->join("#_buku_besar", "#_buku_besar.id_gl_account=#_gl_account.id_gl_account and id_cabang=" . $id_cabang . " and tanggal_buku >= '" . DefaultTanggalDatabase($start) . "' and tanggal_buku <='" . DefaultTanggalDatabase($end) . "'", "left");
        } else {
            $this->db->join("#_buku_besar", "#_buku_besar.id_gl_account=#_gl_account.id_gl_account and tanggal_buku >= '" . DefaultTanggalDatabase($start) . "' and tanggal_buku <='" . DefaultTanggalDatabase($end) . "'", "left");
        }
        $this->db->select("ifnull(sum(#_buku_besar.debet-#_buku_besar.kredit),0) totalbiaya,#_gl_account.id_gl_account,nama_gl_account");
        $this->db->where(array("is_header" => 0, "is_hpp" => 1, "#_gl_account.status" => 1));
        $this->db->group_by("#_gl_account.id_gl_account");
        $listhpp = $this->db->get()->result();
        return $listhpp;
    }

    function GetDropDownGl($is_header = 2,$id_gl_header=0,$id_gl_parent=0,$is_hpp=0,$id_gl_laba_rugi=0,$id_gl_account=0) {
        if (@$is_header != "2") {
            $this->db->where("is_header", $is_header);
        }
        if(!CheckEmpty($id_gl_header))
        {
            $this->db->where("id_gl_header", $id_gl_header);
        }
        if(!CheckEmpty($id_gl_parent))
        {
            $this->db->where("id_gl_parent", $id_gl_parent);
        }
        if(!CheckEmpty($is_hpp))
        {
            $this->db->where("is_hpp", $is_hpp=="1"?1:0);
        }
        if(!CheckEmpty($id_gl_laba_rugi))
        {
            $this->db->where("id_gl_labarugi", $id_gl_laba_rugi);
        }
        if(!CheckEmpty($id_gl_account))
        {
            $this->db->where("id_gl_account", $id_gl_account);
        }

        $listgl = GetTableData($this->table, 'id_gl_account', 'nama_gl_account', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null), "json", array("nama_gl_account", "asc"));
        return $listgl;
    }

    function GetDropDownGlHeader() {
        $listgl = GetTableData("#_gl_header", 'id_gl_header', 'nama_gl_header', array('status' => 1));

        return $listgl;
    }
     function GetDropDownGlLabaRugi() {
        $listgl = GetTableData("#_gl_labarugi", 'id_gl_labarugi', 'nama_header_labarugi', array('status' => 1));

        return $listgl;
    }

    function GlDelete($id_gl) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_gl' => $id_gl));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Gl has been deleted from database");
    }

}
