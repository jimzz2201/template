<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gl extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_gl');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K081";
        $header = "K080";
        $title = "Gl";

        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_gl'] = $this->m_gl->GetDataGlAccount();
        //dumperror($model['list_gl']);
        //die();
        CekModule($module);
        LoadTemplate($model, "gl/v_gl_index", $javascript);
    }
    
    public function getdropdowngl()
    {
        header('Content-Type: application/json');
        $model=$this->input->post();
        $id_gl_header=CheckIfIsset($model,"id_gl_header",0);
        $id_gl_parent=CheckIfIsset($model,"id_gl_parent",0);
        $is_hpp=CheckIfIsset($model,"is_hpp",0);
        $id_gl_labarugi=CheckIfIsset($model,"id_gl_labarugi",0);
        $is_header=CheckIfIsset($model,"is_header",0);
        
        $listgl=$this->m_gl->GetDropDownGl($is_header,$id_gl_header,$id_gl_parent,$is_hpp,$id_gl_labarugi);
        echo json_encode(DefaultEmptyDropdown($listgl,"json","GL"));
    }
    

    public function get_one_gl() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_gl', 'Gl', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_gl->GetOneGl($model["id_gl"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Gl tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdatagl() {
        header('Content-Type: application/json');
        echo $this->m_gl->GetDatagl();
    }

    public function create_gl() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K081";
        $header = "K080";
        $row['title'] = "Add Gl";
        CekModule($module);
        $row['list_gl_header'] = $this->m_gl->GetDropDownGlHeader();
        $row['list_gl_account'] = $this->m_gl->GetDropDownGl(1);
        $row['list_gl_labarugi']=$this->m_gl->GetDropDownGlLabaRugi();
        $row['form'] = $header;
        $row['is_hpp'] = 0;
        $row['formsubmenu'] = $module;
        $this->load->view('gl/v_gl_manipulate', $row);
    }

    public function gl_account_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('nama_gl_account', 'Nama Gl', 'trim|required');
        $this->form_validation->set_rules('id_gl_header', 'Type Akun', 'trim|required');
        if (CheckEmpty(@$model['id_gl_account'])) {
            $this->form_validation->set_rules('kode_gl_account', 'Kode Gl', 'trim|required');
        }
        else{
            $message=$this->m_gl->CheckGl(@$model['id_gl_account'],$model['is_header']);
        }
        

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_gl->GlManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_gl($id = 0) {
        $row = $this->m_gl->GetOneGl($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K081";
            $header = "K080";
            $row['title'] = "Edit Gl";
            $row['list_gl_header'] = $this->m_gl->GetDropDownGlHeader();
            $row['list_gl_account'] = $this->m_gl->GetDropDownGl(1);
            $row['list_gl_labarugi']=$this->m_gl->GetDropDownGlLabaRugi();
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->view('gl/v_gl_manipulate', $row);
        } else {
            SetMessageSession(0, "Gl cannot be found in database");
            redirect(site_url('gl'));
        }
    }

   

}

/* End of file Gl.php */