<section class="content-header">
    <h1>
        Persediaan <?= @$button ?>
        <small>Persediaan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Persediaan</li>
        <li class="active">Persediaan <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_unit" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_unit" value="<?php echo @$id_unit; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Kode Unit', "txt_kode_unit", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $mergearray = array();
                                        if (!CheckEmpty(@$id_unit)) {
                                            $mergearray['readonly'] = "readonly";
                                            $mergearray['name'] = "kode_unit";
                                        } else {
                                            $mergearray['name'] = "kode_unit";
                                            $mergearray['readonly'] = "readonly";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_unit == '' ? 'AUTOMATIC' : @$kode_unit, 'class' => 'form-control', 'id' => 'txt_kode_unit', 'placeholder' => 'Kode Unit'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Unit', "txt_nama_unit", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_unit', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Nama Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Tipe Persediaan', "txt_id_tipe_persediaan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_tipe_persediaan', 'class' => 'form-control', 'id' => 'dd_id_tipe_persediaan', 'placeholder' => 'Tipe Persediaan'), DefaultEmptyDropdown($list_tipe_persediaan, "json", "Tipe Persediaan"), @$id_tipe_persediaan); ?>
                                    </div>
                                    <?= form_label('Kategori', "txt_id_kategori", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori', 'class' => 'form-control', 'id' => 'dd_id_kategori', 'placeholder' => 'Kategori'), DefaultEmptyDropdown($list_kategori, "json", "Kategori"), @$id_kategori); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group" id="areatipepersediaan">
                                    <?= form_label('Type Unit', "txt_id_type_unit", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_type_unit', 'class' => 'form-control', 'id' => 'dd_id_type_unit', 'placeholder' => 'Type Unit'), DefaultEmptyDropdown($list_type_unit, "json", "Type Unit"), @$id_type_unit); ?>
                                    </div>
                                    <?= form_label('Type Body', "txt_id_type_unit", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_type_body', 'class' => 'form-control', 'id' => 'dd_id_type_body', 'placeholder' => 'Type Body'), DefaultEmptyDropdown($list_type_body, "json", "Type Body"), @$id_type_body); ?>
                                    </div>
                                </div>
                               
                                
                                
                                <div class="form-group">
                                    
                                     <?= form_label('Price', "txt_price", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'price', 'value' => DefaultCurrency(@$price), 'class' => 'form-control', 'id' => 'txt_price', 'placeholder' => 'Price', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                     
                                    <?= form_label('BBN', "txt_price", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'bbn', 'value' => DefaultCurrency(@$bbn), 'class' => 'form-control', 'id' => 'txt_bbn', 'placeholder' => 'BBN', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Additional', "txt_price", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'additional_price', 'value' => DefaultCurrency(@$additional_price), 'class' => 'form-control', 'id' => 'txt_additional_price', 'placeholder' => 'Additional Price', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Qty', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text','readonly'=>'readonly', 'name' => 'qty', 'value' => @$qty, 'class' => 'form-control', 'id' => 'txt_qty', 'placeholder' => 'Qty')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    
                                     <?= form_label('Harga Beli', "txt_price", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'harga_beli', 'value' => DefaultCurrency(@$harga_beli), 'class' => 'form-control', 'id' => 'txt_price', 'placeholder' => 'Harga Beli', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>   
                                  
                                
                                
                               
                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/unit' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
    $(document).ready(function () {
       
        $("select").select2();
        $("#dd_id_tipe_persediaan").change(function(){
            RefreshArea();
         })
         RefreshArea();
    });
    function RefreshArea(){
         if($("#dd_id_tipe_persediaan").val()=="1")
            {
                $("#areatipepersediaan").css("display","block");
            }
            else
            {
                $("#areatipepersediaan").css("display","none");
            }
    }
    
    $("#frm_unit").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/unit_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/unit';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>