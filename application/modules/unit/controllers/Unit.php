<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_unit');
        $this->load->model("kategori/m_kategori");
        $this->load->model("warna/m_warna");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("tipe_persediaan/m_tipe_persediaan");
        $this->load->model("type_body/m_type_body");
    }
    
    public function get_unit_serial()
    {
        $model=$this->input->post();
        $data=[];
        $data['st']=true;
        $listserialunit=[];
        if(CheckEmpty(@$model['id_prospek']))
        {
             $listserialunit=[];
        }
        else
        {
            $this->load->model("unit/m_unit");
            $listserialunit=$this->m_unit->GetUnitSerialByProspek($model['id_prospek']);
        }
        $data['list_unit_serial']= DefaultEmptyDropdown($listserialunit,"json","Unit");
        echo json_encode($data);
    }
    
    public function get_unit_serial_full()
    {
        $model=$this->input->post();
        $data=[];
        $data['st']=true;
        $listserialunit=[];
        if(CheckEmpty(@$model['id_customer']))
        {
             $listserialunit=[];
        }
        else
        {
            $this->load->model("unit/m_unit");
            $listserialunit=$this->m_unit->GetDropDownUnitFullfromCustomer($model['id_customer'],@$model['isfull']);
        }
        $data['list_unit_serial']= DefaultEmptyDropdown($listserialunit,"json","Unit");
        echo json_encode($data);
    }
    
    
    public  function get_one_serial()
    {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_unit_serial', 'Unit', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $obj = $this->m_unit->GetOneUnitSerial($model["id_unit_serial"], "#_unit_serial.id_unit_serial", true);
            $data['obj']=$obj;
            $data['st'] = $data['obj'] != null;
            $data['html'] = $this->load->view("pembayaran/v_pembayaran_piutang_content", array("detail"=>[$data['obj']]), true);
            $data['msg'] = !$data['st'] ? "Data Unit tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    
    public  function get_one_serial_unit_pembelian()
    {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_unit_serial', 'Unit', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $obj = $this->m_unit->GetOneUnitSerialUnitPembelian($model["id_unit_serial"], "#_unit_serial.id_unit_serial", true);
            $data['obj']=$obj;
            $data['st'] = $data['obj'] != null;
            $data['html'] = $this->load->view("pembayaran/v_pembayaran_hutang_content", array("detail"=>[$data['obj']]), true);
            $data['msg'] = !$data['st'] ? "Data Unit tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    
    
    
    
    
    
    public function get_list_unit() {
        
        $model=$this->input->post();
        $data = $this->m_unit->GetListUnit($model);
        echo json_encode(array("st"=>true,"obj"=> DefaultEmptyDropdown($data,"json","Unit")));
    }

    public function get_list_unit_serial($id_do_prospek_unit=0) {
        $type_do = $this->input->get('type_do');
        $id_cust = $this->input->get('id_cust');
        $id_buka_jual = $this->input->get('id_buka_jual');
        $data = $this->m_unit->GetListUnitSerial($type_do, $id_buka_jual, $id_cust);
        $data = json_decode(json_encode($data), true);
        $table = "";
        
        
        
        foreach ($data as $dt) {
            $table .= $this->load->view("do_prospek/v_do_prospek_unit",array("detail"=>$dt),false);
        }
        echo $table;
    }

    public function get_list_unit_serial_kpu() {
        $type_do = $this->input->get('type_do');
        $id_supplier = $this->input->get('id_supplier');
        $id_do_kpu_detail = $this->input->get('id_do_kpu_detail');
        $data = $this->m_unit->GetListUnitSerialKpu($id_do_kpu_detail);
        $data = json_decode(json_encode($data), true);
        $table = "";
        foreach ($data as $dt) {
            $table .= "<tr onclick='selectRow(".$dt['id_unit_serial'].")'>
                            <td>".$dt['nama_unit']."</td>
                            <td>".$dt['nama_type_unit']."</td>
                            <td>".$dt['nama_kategori']."</td>
                            <td>".$dt['vin_number']."</td>
                            <td>".$dt['vin']."</td>
                            <td>".$dt['manufacture_code']."</td>
                            <td>".$dt['engine_no']."</td>
                            <td>".$dt['no_bpkb']."</td>
                            <td>".$dt['no_polisi']."</td>
                            <td>".$dt['nama_pool']."</td>
                            <td><input type='checkbox' style='width:20px;' class='selectUnit' onclick='selectUnit(".$dt['id_unit_serial'].")' id='unit".$dt['id_unit_serial']."'></td>
                        </tr>";
        }
        echo $table;
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K055";
        $header = "K001";
        $title = "Unit";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        LoadTemplate($model, "unit/v_unit_index", $javascript);
    }

    public function get_one_unit() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_unit', 'Unit', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_unit->GetOneUnit($model["id_unit"], "id_unit", true);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Unit tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdataunit() {
        header('Content-Type: application/json');
        echo $this->m_unit->GetDataunit();
        
    }

    public function create_unit() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K055";
        $header = "K001";
        $row['title'] = "Add Unit";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_tipe_persediaan'] = $this->m_tipe_persediaan->GetDropDownTipe_persediaan();
         $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'unit/v_unit_manipulate', $javascript, $css);
    }

    public function unit_manipulate() {
        $message = '';

        $this->form_validation->set_rules('kode_unit', 'Kode Unit', 'trim|required');
        $this->form_validation->set_rules('nama_unit', 'Nama Unit', 'trim|required');
        $this->form_validation->set_rules('id_kategori', 'Id Kategori', 'trim');
        $this->form_validation->set_rules('id_type_unit', 'Id Type Unit', 'trim');
        $this->form_validation->set_rules('manufacture_code', 'Manufacture Code', 'trim');
        $this->form_validation->set_rules('vin_no', 'Vin No', 'trim');
        $this->form_validation->set_rules('engine_no', 'Engine No', 'trim');
        $this->form_validation->set_rules('unit_vin', 'Unit Vin', 'trim');
        $this->form_validation->set_rules('id_do', 'Id Do', 'trim');
        $this->form_validation->set_rules('id_colour', 'Id Colour', 'trim');
        $this->form_validation->set_rules('free_parking', 'Free Parking', 'trim');
        $this->form_validation->set_rules('dnp', 'Dnp', 'trim|numeric');
        $this->form_validation->set_rules('unit_position', 'Unit Position', 'trim');
        $this->form_validation->set_rules('qty', 'Qty', 'trim');
        $this->form_validation->set_rules('price', 'Price', 'trim|numeric');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_unit->UnitManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_unit($id = 0) {
        $row = $this->m_unit->GetOneUnit($id, "id_unit", true);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K055";
            $header = "K001";
            $row['title'] = "Edit Unit";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
             $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_tipe_persediaan'] = $this->m_tipe_persediaan->GetDropDownTipe_Persediaan();
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'unit/v_unit_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Unit cannot be found in database");
            redirect(site_url('unit'));
        }
    }
    public function get_dropdown_unit()
    {
        header('Content-Type: application/json');
        $model=$this->input->post();
       
        $list_unit=$this->m_unit->GetDropDownUnit($model);
        echo json_encode(array("list_unit"=> DefaultEmptyDropdown($list_unit,"json","Unit"),"st"=>true));
        
    }
  
    
    public function unit_delete() {
        $message = '';
        $this->form_validation->set_rules('id_unit', 'Unit', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_unit->UnitDelete($model['id_unit']);
        }

        echo json_encode($result);
    }

}

/* End of file Unit.php */