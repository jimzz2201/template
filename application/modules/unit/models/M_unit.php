<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_unit extends CI_Model {

    public $table = '#_unit';
    public $id = 'id_unit';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    function GetUnitSerialByProspek($id_prospek,$id_buka_jual=0)
    {
        $this->load->model("prospek/m_prospek");
        $this->db->from("#_prospek_detail");
        $this->db->where(array("id_prospek"=>$id_prospek));
        $row=$this->db->get()->row();
        if($row)
        {
            $id_unit = $row->id_unit;
            $this->db->join("#_unit", "#_unit.id_unit=#_unit_serial.id_unit", "left");
            $this->db->join("#_kpu_detail", "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail", "left");
            $this->db->join("#_kpu", "#_kpu.id_kpu=#_kpu_detail.id_kpu", "left");
            $this->db->join("#_customer", "#_customer.id_customer=#_kpu.id_customer", "left");
            $or_where = array();
            if (!CheckEmpty($id_buka_jual)) {
                $this->db->group_start();
                $this->db->where(array("id_buka_jual"=>null));
                $this->db->or_where(array("id_buka_jual"=>$id_buka_jual));
                $this->db->group_end();
            }
            else
            {
                $this->db->where(array("id_buka_jual"=>null, "id_do_prospek_detail" => null));
            }

            $listunit = GetTableData("#_unit_serial", 'id_unit_serial', array('nama_customer', 'vin_number', 'nama_unit'), array("#_unit_serial.status !=" => 2, '#_unit_serial.id_unit' => $id_unit, '#_unit_serial.deleted_date' => null), 'json', array(), array(), array(), "", $or_where);
           
            return $listunit;
        }
        else {
            return [];
        }
    }
    function GetDropDownUnitFullfromCustomer($id_customer,$isfull=false) {
        $this->db->where(array("#_buka_jual.id_customer" => $id_customer));
        $this->db->group_start();
            $this->db->where(array("#_unit_serial" . '.status !=' => 2));
        $this->db->group_end();
        $this->db->join("#_buka_jual","#_unit_serial.id_buka_jual=#_buka_jual.id_buka_jual");
        $this->db->join("#_prospek","#_prospek.id_prospek=#_buka_jual.id_prospek");
        $this->db->join("#_unit","#_unit_serial.id_unit=#_unit.id_unit");
        $this->db->join("#_type_unit","#_type_unit.id_type_unit=#_unit.id_type_unit");
        $this->db->where(array("id_do_prospek_detail"=>null,"#_buka_jual.status"=>1));
        if(!$isfull)
        $this->db->where(array("#_unit_serial.id_buka_jual !="=>null));
        $listserial = GetTableData("#_unit_serial", 'id_unit_serial', array("no_prospek","nomor_buka_jual","vin_number","engine_no","nama_type_unit","nama_unit"), array( $this->table . '.deleted_date' => null),"json");
       
        return $listserial;
    }
    // datatables
    function GetDataunit() {
        $this->load->library('datatables');
        $this->datatables->select('#_unit.*,id_unit as id,nama_tipe_persediaan,nama_type_unit,nama_kategori,nama_type_body');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_kategori', 'dgmi_unit.id_kategori = #_kategori.id_kategori', 'left');

        $this->datatables->join('#_type_body', '_type_body.id_type_body = #_unit.id_type_body', 'left');
        $this->datatables->join('#_type_unit', 'dgmi_unit.id_type_unit = #_type_unit.id_type_unit', 'left');
        $this->datatables->join('#_tipe_persediaan', 'dgmi_unit.id_tipe_persediaan = #_tipe_persediaan.id_tipe_persediaan', 'left');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('unit/edit_unit/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteunit($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id');
        return $this->datatables->generate();
    }

    // get all
    function GetOneUnit($keyword, $type = '#_unit.id_unit', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);

        if ($isfull) {
            $this->db->join("#_type_unit", "#_type_unit.id_type_unit=#_unit.id_type_unit", "left");
            $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
            $this->db->join("#_type_body", "#_type_body.id_type_body=#_unit.id_type_body", "left");
        }

        $unit = $this->db->get($this->table)->row();
        return $unit;
    }

     function GetOneUnitSerial($id_unit_serial, $type = "#_unit_serial.id_unit_serial") {
        $this->db->where(array("#_unit_serial.id_unit_serial" => $id_unit_serial));
        $this->db->select("#_buka_jual.id_customer,#_unit_serial.*,#_kpu.id_kpu,#_prospek.id_prospek,#_vrf.id_vrf,#_jenis_plat.nama_jenis_plat,temp_karoseri.nama_warna as nama_warna_karoseri,#_prospek_detail.keterangan_detail,#_prospek.nama_stnk,#_prospek.alamat_stnk,#_prospek.tdp_prospek,#_prospek.no_ktp_prospek,#_buka_jual.tanggal_buka_jual,#_kpu.nomor_master as nomor_kpu,#_kpu.tanggal as tanggal_kpu,#_prospek.tanggal_prospek,vrf_date as tanggal_vrf,#_prospek.id_prospek,nama_unit,nomor_buka_jual,nama_pool,ifnull(sum(case when #_pembayaran_piutang_master.status <>2 then #_pembayaran_piutang_detail.jumlah_bayar else 0 end),0) as terbayar,nama_type_unit,nama_kategori,nama_type_body,nomor_master,no_prospek,ifnull(customerbukajual.nama_customer,customerkpu.nama_customer) as nama_customer,vrf_code,#_prospek_detail.harga_jual_unit,(#_kpu_detail.subtotal / #_kpu_detail.qty) as hpp_unit,#_warna.nama_warna,#_buka_jual.nomor_buka_jual,#_do_kpu.id_do_kpu");
        $this->db->from("#_unit_serial");
        $this->db->join("#_unit", "#_unit.id_unit=#_unit_serial.id_unit", "left");
        $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual", "left");
        $this->db->join("#_type_unit", "#_type_unit.id_type_unit=#_unit.id_type_unit", "left");
        $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
        $this->db->join("#_type_body", "#_type_body.id_type_body=#_unit.id_type_body", "left");
        $this->db->join("#_do_kpu_detail", "#_unit_serial.id_do_kpu_detail=#_do_kpu_detail.id_do_kpu_detail", "left");
        $this->db->join("#_do_kpu", "#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu", "left");
        $this->db->join("#_kpu_detail", "#_do_kpu_detail.id_kpu_detail=#_kpu_detail.id_kpu_detail", "left");
        $this->db->join("#_kpu", "#_kpu.id_kpu=#_kpu_detail.id_kpu and #_kpu.type='KPU'", "left");
        $this->db->join("#_customer customerkpu", "customerkpu.id_customer=#_kpu.id_customer", "left");
        $this->db->join("#_customer customerbukajual", "customerbukajual.id_customer=#_buka_jual.id_customer", "left");
        $this->db->join("#_vrf", "#_vrf.id_vrf=#_kpu_detail.id_vrf", "left");
        $this->db->join("#_prospek", "#_prospek.id_prospek=#_buka_jual.id_prospek", "left");
        $this->db->join("#_prospek_detail", "#_prospek_detail.id_prospek=#_prospek.id_prospek", "left");
        $this->db->join("#_jenis_plat", "#_jenis_plat.id_jenis_plat=#_prospek_detail.id_jenis_plat", "left");
        $this->db->join("#_warna", "#_warna.id_warna=#_unit_serial.id_warna", "left");
        $this->db->join("#_warna temp_karoseri", "temp_karoseri.id_warna=#_prospek_detail.id_warna_karoseri", "left");
        $this->db->join("#_pool", "#_pool.id_pool=#_unit_serial.id_pool", "left");
        $this->db->join("#_pembayaran_piutang_detail", "#_pembayaran_piutang_detail.id_unit_serial=#_unit_serial.id_unit_serial", "left");
        $this->db->join("#_pembayaran_piutang_master", "#_pembayaran_piutang_master.pembayaran_piutang_id=#_pembayaran_piutang_detail.pembayaran_piutang_id", "left");
        
        $this->db->group_by("#_unit_serial.id_unit_serial");
        $unit = $this->db->get()->row();
        if($unit)
        {
            $unit->sisa=$unit->harga_jual_unit-$unit->terbayar;
        }
        return $unit;
    }
    function GetOneUnitSerialUnitPembelian($id_unit_serial, $type = "#_unit_serial.id_unit_serial") {
        $this->db->where(array("#_unit_serial.id_unit_serial" => $id_unit_serial));
        $this->db->select("#_unit_serial.*,#_kpu.id_kpu,#_prospek.id_prospek,#_vrf.id_vrf,#_jenis_plat.nama_jenis_plat,temp_karoseri.nama_warna as nama_warna_karoseri,#_prospek_detail.keterangan_detail,#_prospek.nama_stnk,#_prospek.alamat_stnk,#_prospek.tdp_prospek,#_prospek.no_ktp_prospek,#_buka_jual.tanggal_buka_jual,#_kpu.nomor_master as nomor_kpu,#_kpu.tanggal as tanggal_kpu,#_prospek.tanggal_prospek,vrf_date as tanggal_vrf,#_prospek.id_prospek,nama_unit,nomor_buka_jual,nama_pool,ifnull(sum(case when #_pembayaran_utang_master.status <>2 then #_pembayaran_utang_detail.jumlah_bayar else 0 end),0) as terbayar,nama_type_unit,nama_kategori,nama_type_body,nomor_master,no_prospek,nama_customer,vrf_code,(#_kpu_detail.subtotal/ #_kpu_detail.qty) as harga_jual_unit ,#_warna.nama_warna,#_buka_jual.nomor_buka_jual");
        $this->db->from("#_unit_serial");
        $this->db->join("#_unit", "#_unit.id_unit=#_unit_serial.id_unit", "left");
        $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual", "left");
        $this->db->join("#_type_unit", "#_type_unit.id_type_unit=#_unit.id_type_unit", "left");
        $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
        $this->db->join("#_type_body", "#_type_body.id_type_body=#_unit.id_type_body", "left");
        $this->db->join("#_kpu_detail", "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail", "left");
        $this->db->join("#_kpu", "#_kpu.id_kpu=#_kpu_detail.id_kpu and #_kpu.type='KPU'", "left");
        $this->db->join("#_customer", "#_customer.id_customer=#_kpu.id_customer", "left");
        
        $this->db->join("#_vrf", "#_vrf.id_vrf=#_kpu_detail.id_vrf", "left");
        $this->db->join("#_prospek", "#_prospek.id_prospek=#_buka_jual.id_prospek", "left");
        $this->db->join("#_prospek_detail", "#_prospek_detail.id_prospek=#_prospek.id_prospek", "left");
        $this->db->join("#_jenis_plat", "#_jenis_plat.id_jenis_plat=#_prospek_detail.id_jenis_plat", "left");
        $this->db->join("#_warna", "#_warna.id_warna=#_unit_serial.id_warna", "left");
        $this->db->join("#_warna temp_karoseri", "temp_karoseri.id_warna=#_prospek_detail.id_warna_karoseri", "left");
        $this->db->join("#_pool", "#_pool.id_pool=#_unit_serial.id_pool", "left");
        $this->db->join("#_pembayaran_utang_detail", "#_pembayaran_utang_detail.id_unit_serial=#_unit_serial.id_unit_serial", "left");
        $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id", "left");
        
        $this->db->group_by("#_unit_serial.id_unit_serial");
        $unit = $this->db->get()->row();
        if($unit)
        {
            $unit->sisa=$unit->harga_jual_unit-$unit->terbayar;
        }
        
        return $unit;
    }
    
    

    function GetListUnitSerial($type_do, $id_buka_jual, $id_cust) {
        // $this->db->where("date_do_out", null);
        $this->db->where("#_unit_serial.id_buka_jual", $id_buka_jual);
        $this->db->from("#_unit_serial");
        $this->db->join("#_unit", "#_unit.id_unit=#_unit_serial.id_unit", "left");
        $this->db->join("#_type_unit", "#_type_unit.id_type_unit=#_unit.id_type_unit", "left");
        $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
        $this->db->join("#_type_body", "#_type_body.id_type_body=#_unit.id_type_body", "left");
        $this->db->join("#_warna", "#_warna.id_warna=#_unit_serial.id_warna", "left");
        $this->db->join("#_pool", "#_pool.id_pool=#_unit_serial.id_pool", "left");
        // $this->db->join("#_do_prospek", "#_do_prospek.id_do_prospek=#_unit_serial.id_do_so", "left");
        $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual");
        
        $unit = $this->db->get()->result_array();
        return $unit;
    }
    
    function GetListUnit($model,$is_edit=false) {
        
        $where=[];
        if(!CheckEmpty(@$model['id_buka_jual']))
        {
            $where['#_unit_serial.id_buka_jual']=$model['id_buka_jual'];
        }
        
        
        if(!CheckEmpty(@$model['id_prospek']))
        {
            $where['#_buka_jual.id_prospek']=$model['id_prospek'];
        }
        
        
        if(!CheckEmpty(@$model['id_customer']))
        {
            $where['#_buka_jual.id_customer']=$model['id_customer'];
        }
        if(!CheckEmpty(@$model['id_unit']))
        {
            $where['#_unit.id_unit']=$model['id_unit'];
        }
        if(!CheckEmpty(@$model['id_kategori']))
        {
            $where['#_unit.id_kategori']=$model['id_kategori'];
        }
        if(!CheckEmpty(@$model['id_do_kpu']))
        {
            $where['#_do_kpu.id_do_kpu']=$model['id_do_kpu'];
        }
         if(!CheckEmpty(@$model['id_kpu']))
        {
            $where['#_do_kpu.id_kpu']=$model['id_kpu'];
        }
        
        
        
        if($is_edit)
        {
            if (count($where) == 0) {
                $where['#_unit_serial.id_unit_serial']=null;
            }
        }
        $where['#_unit_serial.id_do_prospek_detail']=null;
       
        $this->db->where($where);
        
        $this->db->join("#_unit", "#_unit.id_unit=#_unit_serial.id_unit", "left");
        $this->db->join("#_do_kpu_detail", "#_do_kpu_detail.id_do_kpu_detail=#_unit_serial.id_do_kpu_detail", "left");
        $this->db->join("#_do_kpu", "#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu", "left");
        $this->db->join("#_type_unit", "#_type_unit.id_type_unit=#_unit.id_type_unit", "left");
        $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
        $this->db->join("#_type_body", "#_type_body.id_type_body=#_unit.id_type_body", "left");
        $this->db->join("#_warna", "#_warna.id_warna=#_unit_serial.id_warna", "left");
        $this->db->join("#_pool", "#_pool.id_pool=#_unit_serial.id_pool", "left");
        // $this->db->join("#_do_prospek", "#_do_prospek.id_do_prospek=#_unit_serial.id_do_so", "left");
        $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual","left");
        
        $listserial = GetTableData("#_unit_serial", 'id_unit_serial', array("no_prospek","vin_number","engine_no","nama_type_unit","nama_unit"), array( $this->table . '.deleted_date' => null),"json");
        return $listserial;
    }
    
    
    

    function GetListUnitSerialKpu($id_do_kpu_detail) {
        $this->db->where("#_unit_serial.id_do_kpu_detail", $id_do_kpu_detail);
        $this->db->from("#_unit_serial");
        $this->db->join("#_unit", "#_unit.id_unit=#_unit_serial.id_unit", "left");
        $this->db->join("#_type_unit", "#_type_unit.id_type_unit=#_unit.id_type_unit", "left");
        $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
        $this->db->join("#_type_body", "#_type_body.id_type_body=#_unit.id_type_body", "left");
        $this->db->join("#_warna", "#_warna.id_warna=#_unit_serial.id_warna", "left");
        $this->db->join("#_pool", "#_pool.id_pool=#_unit_serial.id_pool", "left");
        $unit = $this->db->get()->result_array();
        return $unit;
    }

    function UnitManipulate($model) {
        try {
            $model['id_kategori'] = ForeignKeyFromDb($model['id_kategori']);
            $model['id_type_unit'] = ForeignKeyFromDb($model['id_type_unit']);
            $model['id_type_body'] = ForeignKeyFromDb($model['id_type_body']);
            // $model['id_do'] = ForeignKeyFromDb($model['id_do']);
            $model['qty'] = DefaultCurrencyDatabase($model['qty']);
            $model['price'] = DefaultCurrencyDatabase($model['price']);
            $model['harga_beli'] = DefaultCurrencyDatabase($model['harga_beli']);
            $model['additional_price'] = DefaultCurrencyDatabase($model['additional_price']);
            $model['bbn'] = DefaultCurrencyDatabase($model['bbn']);

            if (CheckEmpty($model['id_unit'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $pattern = $model['id_kategori'] == 1 ? 'U' : 'EQP';
                $model['kode_unit'] = AutoIncrement("#_unit", $pattern, "kode_unit", 4);
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Unit successfull added into database');
                return array("st" => true, "msg" => "Unit successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_unit" => $model['id_unit']));
                SetMessageSession(1, 'Unit has been updated');
                return array("st" => true, "msg" => "Unit has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownUnit($model=[],$id_tipe_persediaan=[],$wherecombine=[]) {
        
        $where=array($this->table . '.status' => 1, $this->table . '.deleted_date' => null);
        if(!CheckEmpty(@$model['id_type_unit']))
        {
            $where['id_type_unit']=$model['id_type_unit'];
        }
         if(!CheckEmpty(@$model['id_kategori']))
        {
            $where['id_kategori']=$model['id_kategori'];
        }
        if(!CheckEmpty(@$model['id_tipe_persediaan']))
        {
            if(is_array(@$model['id_tipe_persediaan']))
            {
                $this->db->where_in("id_tipe_persediaan",@$model['id_tipe_persediaan']);
            }
            else
            {
                 $where['id_tipe_persediaan']=$model['id_tipe_persediaan'];
            }
           
        }
        else {
            $where[$this->table . '.id_tipe_persediaan']=1;
        }
        $where=array_merge($where,$wherecombine);
       
        
        
        $listunit = GetTableData($this->table, 'id_unit', 'nama_unit',$where );

        return $listunit;
    }

    function GetDropDownEquipment() {
        $listunit = GetTableData($this->table, 'id_unit', array('nama_unit'), array($this->table . '.status' => 1, $this->table . '.deleted_date' => null, $this->table . '.id_tipe_persediaan' => 3));

        return $listunit;
    }

    function GetDropDownEquipment_api() { //untuk api mobile
        // $listunit = GetTableData($this->table, 'id_unit', array('nama_unit', 'price'), array($this->table . '.status' => 1, $this->table . '.deleted_date' => null, $this->table . '.id_tipe_persediaan' => 2));
        $this->db->select('id_unit, nama_unit, price');
        $this->db->where(array($this->table . '.status' => 1, $this->table . '.deleted_date' => null, $this->table . '.id_tipe_persediaan' => 3));
        $listunit = $this->db->get($this->table)->result();
        $listOptions = array();
        if ($listunit) {
            foreach ($listunit as $k=>$data) {
                $listOptions[$k]['id'] = (int) $data->id_unit;
                $listOptions[$k]['text'] = $data->nama_unit;
                $listOptions[$k]['price'] = round($data->price, 0);
            }
        }

        return $listOptions;
    }

    function GetDropDownSerialUnit($belumkeluar="") {
        $this->db->join("#_unit", "#_unit.id_unit=#_unit_serial.id_unit");
        if(CheckEmpty(@$belumkeluar))
        {
            $this->db->where(array("id_do_prospek_detail",null));
        }
        $this->db->join("#_do_kpu_detail","#_do_kpu_detail.id_do_kpu_detail=#_unit_serial.id_do_kpu_detail","left");
        $this->db->join("#_do_kpu","#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu","left");
        $this->db->join("#_kpu","#_kpu.id_kpu=#_do_kpu_detail.id_kpu","left");
        $this->db->join("#_do_prospek_detail","#_do_prospek_detail.id_do_prospek_detail=#_unit_serial.id_do_prospek_detail","left");
        $this->db->join("#_prospek","#_do_prospek_detail.id_prospek=#_prospek.id_prospek","left");
        $this->db->join("#_buka_jual","#_buka_jual.id_prospek=#_unit_serial.id_buka_jual","left");
        $select="#_unit_serial.*,nomor_master,#_unit.nama_unit,no_prospek,nomor_buka_jual";
        $listunit = GetTableData("#_unit_serial", 'id_unit_serial', array('vin_number', 'nama_unit','nomor_master','no_prospek','nomor_buka_jual'), array("#_unit_serial.status" => 1, '#_unit_serial.deleted_date' => null),'json',[],[],[],$select,[]);

        return $listunit;
    }

    function UnitDelete($id_unit) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_unit' => $id_unit));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Unit has been deleted from database");
    }

}
