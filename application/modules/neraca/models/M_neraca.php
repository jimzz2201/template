<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_neraca extends CI_Model
{

    public $table = '#_barang';
    public $id = 'id_barang';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }
    
    
    function IsExistDokumen($no_dokumen_after, $no_dokumen_before = "") {
        if ($no_dokumen_after != $no_dokumen_before) {
            $this->db->from("#_buku_besar");
            $this->db->where("dokumen", $no_dokumen_after);
            if (!CheckEmpty($no_dokumen_before)) {
                $this->db->where("dokumen !=", $no_dokumen_before);
            }
            $row = $this->db->get()->row();
            return $row != null;
        } else {
            return false;
        }
    }
    
    function UpdateDokumen($no_dokumen_after, $no_dokumen_before = "")
    {
        if($no_dokumen_after!=$no_dokumen_before)
        {
            if (!$this->IsExistDokumen($no_dokumen_after, $no_dokumen_before)&& !CheckEmpty($no_dokumen_after)) {
                $this->db->update("#_buku_besar",array("dokumen"=>$no_dokumen_after),array("dokumen"=>$no_dokumen_before));
                return true;
            } else {
                return false;
            }
        }
        else
        {
            return true;
        }
    }
    
    

    function GetDropDownNeraca() {
        $this->datatables->where_in("#_barang.id_category",[1,9]);
        $listbarang = GetTableData($this->table, 'id_barang', 'nama_barang', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null), "json", array("nama_barang", "asc"));
       
        return $listbarang;
    }
    
    function GetDataNeraca($id_neraca,$startdate,$enddate,$cabang)
    {
        $this->db->from("#_gl_header");
        $this->db->order_by("urut");
        $listgl=$this->db->get()->result();
        
        
        if($startdate==$enddate || DefaultTanggalDatabase($startdate)<'2019-05-01')
        {
            $startdate='2019-01-01';
        }
        
        $jointext = "#_gl_account.id_gl_account=#_buku_besar.id_gl_account and #_buku_besar.tanggal_buku>='" . DefaultTanggalDatabase($startdate) . "' and tanggal_buku<='" . DefaultTanggalDatabase($enddate) . "' ";
        if (!CheckEmpty($cabang)) {
            $jointext .= " and #_buku_besar.id_cabang =" . $cabang;
        }
        $this->db->from("#_gl_header");
        $this->db->join("#_gl_account", "#_gl_account.id_gl_header=#_gl_header.id_gl_header", "left");
        $this->db->join("#_buku_besar", $jointext, "left");
        $this->db->group_by("#_gl_header.id_gl_header");
        $this->db->select("ifnull(sum(debet-kredit),0) as sum , #_gl_header.*");
        $this->db->where("#_gl_header.status", 1);
        $this->db->order_by("#_gl_header.urut", "asc");
        $listgl = $this->db->get()->result();
        $listdebet=[];
        $listkredit=[];
        foreach($listgl as $glsat)
        {
            
            if($glsat->type_umum=="A")
            {
                $listdebet[$glsat->nama_gl_header]= [];
                $listdebet[$glsat->nama_gl_header]['hitungan']= $glsat->sum;
            }
            else {
                $listkredit[$glsat->nama_gl_header]= [];
                $listkredit[$glsat->nama_gl_header]['hitungan']= $glsat->sum;
            }
        }
      
        if($startdate=='2019-01-01')
        {
            $this->db->from("#_saldo_awal");
            $this->db->join("#_gl_account","#_gl_account.id_gl_account=#_saldo_awal.id_gl_account","left");
            $this->db->join("#_gl_header","#_gl_header.id_gl_header=#_gl_account.id_gl_header","left");
            $this->db->group_by("#_gl_header.id_gl_header");
            $this->db->select("ifnull(sum(saldo_awal),0) as sum , #_gl_header.*");
            if(!CheckEmpty($cabang))
            {
                $this->db->where("#_saldo_awal.id_cabang",$cabang);
            }
            $listheader=$this->db->get()->result();
            foreach($listheader as $headersat)
            {
                if($headersat->type_umum=="A")
                {
                     $listdebet[$headersat->nama_gl_header]['hitungan']+= $headersat->sum;
                }
                else
                {
                    $listkredit[$headersat->nama_gl_header]['hitungan']+= $headersat->sum;
                }
            }
            
            
        }
      
        
        return array("listkredit"=>$listkredit,"listdebet"=>$listdebet);
    }
    
    
    function GetOneNeraca($id_barang,$isfull=true)
    {
        $this->db->from($this->table);
        $this->db->where(array("id_barang"=>$id_barang));
        if($isfull)
        {
             $this->db->join("#_satuan","#_barang.id_satuan=#_satuan.id_satuan");
             $this->db->select($this->table.".*,nama_satuan");
        }
        $neraca=$this->db->get()->row();
        
        if($isfull&&$neraca)
        {
            $this->db->from("#_barang_items");
            $this->db->select("#_barang_items.*,nama_satuan,no_part,kode_barang,_barang.nama_barang");
            $this->db->join("#_barang","#_barang.id_barang=#_barang_items.id_barang");
            $this->db->join("#_satuan","#_barang.id_satuan=#_satuan.id_satuan");
            
            $this->db->where(array("#_barang_items.id_barang_parent"=>$id_barang));
            $neraca->list_detail=$this->db->get()->result();
        }
        
        return $neraca;
    }

}