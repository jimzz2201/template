
<div class="box box-default form-element-list">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="panel" data-collapsed="0">
                    <div class="panel-body">

                        <h1 style="text-align:center">NERACA</h1>
                        <h4 style="text-align:center">PERIODE  
                            <?php if($start_date==$end_date){
                                echo DefaultTanggal($start_date);
                            }
                            else
                            {
                                echo DefaultTanggal($start_date).' - '.DefaultTanggal($end_date);
                            }?>
                        </h4>
                        <br/>
                        <br/>
                        <br/>
                        <div class="col-sm-6">
                            <table class="table table-striped table-bordered table-hover" id="mytable">
                                <thead>

                                    <tr>
                                        <th style="width:25%">Aktiva</th>
                                        <th style="width:25%">&nbsp;</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $decimaldebet = 0;
                                    foreach (@$data['listdebet'] as $key => $debet) {
                                        echo "<tr>";
                                        echo "<td>" . $key . "</td>";
                                        echo "<td style='text-align:right'>" . DefaultCurrency($debet['hitungan']) . "</td>";
                                        echo "</tr>";
                                        $decimaldebet += $debet['hitungan'] ;
                                    }
                                    ?>
                                    <tr>
                                        <td>Total</td>
                                        <td style="border-top:solid 2px #000;font-weight:bold;text-align:right;"><?= DefaultCurrency($decimaldebet) ?></td>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                        <div class="col-sm-6">
                            <table class="table table-striped table-bordered table-hover" id="mytable">
                                <thead>

                                    <tr>

                                        <th style="width:25%">Passiva</th>
                                        <th style="width:25%">&nbsp;</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $decimalkredit = 0;
                                    foreach (@$data['listkredit'] as $key => $debet) {
                                        echo "<tr>";
                                        echo "<td>" . $key . "</td>";
                                        echo "<td style='text-align:right'>" . DefaultCurrency($debet['hitungan']*-1) . "</td>";
                                        echo "</tr>";
                                        $decimalkredit += $debet['hitungan']*-1;
                                    }
                                    ?>
                                    <tr>
                                        <td>Total</td>
                                        <td style="border-top:solid 2px #000;font-weight:bold;text-align:right;"><?= DefaultCurrency($decimalkredit) ?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
