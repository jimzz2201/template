
<section class="content-header">
    <h1>
        Neraca
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Neraca</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                        <div class="col-sm-3">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'value' => @$id_cabang, 'class' => 'form-control', 'id' => 'id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'neraca', 'value' => "", 'class' => 'form-control', 'id' => 'neraca', 'placeholder' => 'Neraca'), DefaultEmptyDropdown(@$list_neraca, "json", "Neraca")); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-2">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-warning pull-right">Save Neraca</button>
                        </div>
                    </div>
                </div>


            </form>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <div id="divareadetail">
                        <?php
                        include APPPATH . 'modules/neraca/views/v_neraca_detail.php';
                        ?>
                    </div>
                </div>
            </div>

        </div>
        <div style="min-height:50px;"></div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    function RefreshGrid() {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/neraca/viewneraca',
            data:$("#frm_search").serialize(),
            success: function (data) {
                $("#divareadetail").html(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }

        });
    
    }

    $(document).ready(function () {
        $(".datepicker").daterangepicker({
            locale: {
                format: 'DD MMM YYYY'
            }
        });
        $("select").select2();


    });
</script>
