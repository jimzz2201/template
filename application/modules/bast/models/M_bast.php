<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_bast extends CI_Model {

    public $table = '#_bast';
    public $id = 'id_bast';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    

    // datatables
    function GetDataBast($params) {
        $this->load->library('datatables');
        $this->datatables->select('#_bast.*,#_bast.id_bast as id,nama_pool,nama_kategori,vin_number,engine_no,nama_type_unit,nama_unit');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_unit_serial', '#_bast.id_unit_serial = #_unit_serial.id_unit_serial', 'left');
        $this->datatables->join('#_unit', '#_unit_serial.id_unit = #_unit.id_unit', 'left');
        $this->datatables->join('#_kategori', '#_kategori.id_kategori = #_unit.id_kategori', 'left');
        $this->datatables->join('#_type_unit', '#_type_unit.id_type_unit = #_unit.id_type_unit', 'left');
        $this->datatables->join('#_pool', '#_bast.id_pool = #_pool.id_pool', 'left');
        $extra = [];
        $where = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_bast.kode_bast'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_bast.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_bast.tanggal"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_bast.tanggal"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                $where['#_bast.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
           
        }
        if (count($where)) {
            $this->db->where($where);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $strprint = '';
        $strview = anchor(site_url('bast/view_bast/$1'), 'View', array('class' => 'btn btn-info btn-xs'));
        $strprint .= anchor(site_url('bast/print_bast/$1'), 'Cetak', array('class' => 'btn btn-default btn-xs', 'target' => '_blank'));
       
        //$straction .= anchor(site_url('bast/receive_bast/$1'), 'Receive', array('class' => 'btn btn-warning btn-xs'));
        $this->datatables->add_column('view', $strview, 'id');
        $this->datatables->add_column('print', $strprint, 'id');
        //$this->datatables->add_column('action', $straction, 'id');
        return $this->datatables->generate();
    }

    function GetOneBast($id_bast) {
        $this->db->from("#_bast");
        $this->db->join("#_unit_serial","#_unit_serial.id_unit_serial=#_bast.id_unit_serial");
        $this->db->where(array("id_bast"=>$id_bast));
        $row=$this->db->get()->row();
        
        if($row)
        {
            $this->db->from("#_bast_check");
            $this->db->join("#_bast_item", "#_bast_check.id_bast_item=#_bast_item.id_bast_item","left");
            $this->db->order_by("group");
            $this->db->order_by("urut");
            $this->db->where(array("id_bast"=>$row->id_bast,"status"=>1));
            $row->list_detail=$this->db->get()->result();
        }
        
        
        return $row;
    }

    function getPrint ($id_bast) {
        $this->db->select("#_bast.*, vin_number, engine_no, nama_warna, nama_type_unit, #_pool.nama_pool, nama_customer, nomor_do_kpu, pa.nama_pool as nama_pool_asal");
        $this->db->from("#_bast");
        $this->db->join("#_unit_serial","#_unit_serial.id_unit_serial=#_bast.id_unit_serial", "left");
        $this->db->join("#_warna","#_unit_serial.id_warna=#_warna.id_warna", "left");
        $this->db->join("#_unit","#_unit_serial.id_unit=#_unit.id_unit", "left");
        $this->db->join("#_type_unit","#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
        $this->db->join("#_pool","#_unit_serial.id_pool=#_pool.id_pool", "left");
        $this->db->join("#_do_kpu_detail", "#_unit_serial.id_do_kpu_detail=#_do_kpu_detail.id_do_kpu_detail", "left");
        $this->db->join("#_do_kpu", "#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu", "left");
        $this->db->join("#_kpu_detail", "#_kpu_detail.id_kpu_detail=#_do_kpu_detail.id_kpu_detail", "left");
        $this->db->join("#_kpu", "#_kpu.id_kpu=#_kpu_detail.id_kpu", "left");
        $this->db->join("#_customer", "#_customer.id_customer=#_kpu.id_customer", "left");
        $this->db->join("#_history_stok_unit", "#_history_stok_unit.id_bast=#_bast.id_bast", "left");
        $this->db->join("#_pool pa","#_history_stok_unit.id_pool=pa.id_pool", "left");
        $this->db->where(array("#_bast.id_bast"=>$id_bast));
        $row=$this->db->get()->row();
        return array('bast'=>$row);
    }

}
