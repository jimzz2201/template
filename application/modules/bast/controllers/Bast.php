<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bast extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_bast');
    }

    public function getdatabast() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_bast->GetDataBast($params);
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K087";
        $header = "K059";
        $title = "KPU";
        $this->load->model("supplier/m_supplier");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "bast/v_bast_index", $javascript, $css);
    }

    public function create_bast() {
        $this->load->model("do_kpu/m_do_kpu");
        $this->load->model("warna/m_warna");
        $this->load->model("vrf/m_vrf");
        $this->load->model("model/m_model");
        //$row = $this->m_do_kpu->GetOneDoKpu($id_do_kpu);
        $row = [];
        $row = json_decode(json_encode($row), true);

        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $javascript = array();
        $this->load->model("unit/m_unit");

        $row['list_unit_serial'] = $this->m_unit->GetDropDownSerialUnit("All");
        $module = "K087";
        $header = "K059";
        $this->load->model("supplier/m_supplier");
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $row['free_top_proposed'] = 0;
        $row['title'] = "Add KPU";
        $row['tanggal'] = GetDateNow();
        $row['type_pembayaran'] = 2;
        $row['kelengkapan'] = $this->m_do_kpu->GetListKelengkapan();
        CekModule($module);
        $row['form'] = $header;
        $row['jumlah'] = 1;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        //$row['list_detail'] =$this->m_do_kpu->GetUnitSerialFromDo($id_do_kpu);
        $this->load->model("pool/m_pool");
        $row['list_pool'] = $this->m_pool->GetDropDownPool([], []);
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css[] = 'assets/plugins/iCheck/all.css';

        LoadTemplate($row, 'bast/v_bast_manipulate', $javascript, $css);
    }

    public function view_bast($id_bast) {
        $this->load->model("bast/m_bast");
        $row = $this->m_bast->GetOneBast($id_bast);
        $row = json_decode(json_encode($row), true);

        $javascript = array();
        $module = "K087";
        $header = "K059";
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("pool/m_pool");
        $row['title'] = "BAST";
        CekModule($module);
        $row['form'] = $header;
        $row['jumlah'] = 1;
        $row['formsubmenu'] = $module;
        $row['list_pool'] = $this->m_pool->getDropDownPool();
        $row['is_view'] = "view";
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css[] = 'assets/plugins/iCheck/all.css';

        LoadTemplate($row, 'bast/v_bast_view', $javascript, $css);
    }

    public function bast_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('id_unit_serial', 'Unit', 'trim|required');
        $this->form_validation->set_rules('id_pool', 'Pool', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('km', 'KM', 'trim|required');
        $this->form_validation->set_rules('pengecek', 'Pengecek', 'trim|required');
        $this->load->model("unit/m_unit");
        $this->load->model("do_kpu/m_do_kpu");
        $sisa = 0;
        $unit = $this->m_unit->GetOneUnitSerial(@$model['id_unit_serial']);
        if (!$unit) {
            $message .= "Unit Tidak ditemukan dalam database<br/>";
        } else if (CheckEmpty($unit->id_do_kpu_detail)) {
            $message .= "Unit Belum Masuk<br/>";
        }
        $kelengkapantemp = $this->m_do_kpu->GetListKelengkapan();
        $interior = CheckArray($model, 'penilaian_interior');
        $ket_interior = CheckArray($model, 'ket_interior');
        $eksterior = CheckArray($model, 'penilaian_eksterior');
        $ket_eksterior = CheckArray($model, 'ket_eksterior');
        $km = CheckArray($model, 'km');
        $pengecek = CheckArray($model, 'pengecek');
        $listkelengkapan = [];
        foreach ($kelengkapantemp as $item) {
            $listkelengkapan[$item->id_bast_item] = $item->nama_bast_item;
        }
        $arrbast = [];
        $itembast = [];
        $itembast['listkelengkapan'] = [];
        $itembast['id_unit_serial'] = $model['id_unit_serial'];
        $itembast['interior'] = $model['penilaian_interior'];
        $itembast['eksterior'] = $model['penilaian_eksterior'];
        $itembast['solar'] = $model['solar'];
        $itembast['ket_interior'] = $model['ket_interior'];
        $itembast['ket_eksterior'] = $model['ket_eksterior'];
        $itembast['ket_det'] = $model['ket_det'];
        $itembast['km'] = DefaultCurrencyDatabase($model['km']);
        $itembast['pengecek'] = $model['pengecek'];

        foreach ($listkelengkapan as $key => $item) {
            if (!CheckKey($model, "item_" . $key)) {
                $message .= $item . " tidak boleh kosong<br/>";
            } else {
                $det = [];
                $det['id'] = $key;
                $det['value'] = $model["item_" . $key];
                $det['ket_check'] = @$model["keterangan_" . $key];
                $itembast['listkelengkapan'][] = $det;
            }
        }
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            // dumperror($model);
            // die();
            $status = $this->m_do_kpu->ReceiveDoKPU($model, $itembast);
            echo json_encode($status);
        }
    }

    function print_bast($id_bast) {
        $CI = get_instance();
        $print = $this->input->get('print');
        $data = $this->m_bast->getPrint($id_bast);
        $data = json_decode(json_encode($data), true);
        $CI->load->view('bast/v_bast_print', $data);
    }

}
