<section class="content-header">
    <h1>
        View BAST <?= @$button ?>
        <small>BAST</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>View BAST</li>
        <li class="active">BAST <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_kpu" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_do_kpu" value="<?php echo @$id_do_kpu; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Tanggal Terima', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text','readonly'=>'readonly', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Pool', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_pool', 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Pool'), DefaultEmptyDropdown(@$list_pool, "json", "Pool"), @$id_pool); ?>
                                    </div>
                                </div>
                                <div id="areado" style="position:relative">

                                    <hr/>
                                    <div style="position:relative">
                                        <div class="form-group">

                                            <h3 style="font-weight: bold;">Berita Acara Serah Terima Unit </h3>
                                            <?= form_input(array('type' => 'hidden', 'name' => 'id_bast[]', 'value' => @$id_bast, 'id' => 'txt_id_bast' , 'placeholder' => 'Bast')); ?>
                                            <h3 style="font-weight: bold;">Engine No : <span style="color:red"><?= @$engine_no ?></span> | VIN Number : <span style="color:red"><?= @$vin_number ?></span></h3>

                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6" >
                                                <table class="table table-striped table-bordered table-hover" >
                                                    <?php foreach (@$list_detail as $item) {
                                                        ?>
                                                        <tr>
                                                            <td style="width:200px;"><?= form_label($item['nama_bast_item'], "radio" .  $item['id_bast_item']); ?></td>
                                                            <?php 
                                                               $arrayyes=[];
                                                               $arrayno=[];
                                                               if($item['value_det']=='T')
                                                               {
                                                                   $arrayyes= ['checked'=>true];
                                                               }
                                                               else
                                                               {
                                                                   $arrayno =['checked'=>true];
                                                               }
                                                            ?>
                                                            <td style="width:100px;"><label class="labelradio"><?= form_radio('item_' . $item['id_bast_item'] , 'T', false, array_merge($arrayyes,array("id" => 'radio_' . $item['id_bast_item'] . '_T'))); ?><span class="spanradio">Ya </span></label></td>
                                                            <td style="width:100px;"><label class="labelradio"><?= form_radio('item_' . $item['id_bast_item'] , 'F', false, array_merge($arrayno,array("id" => 'radio_' . $item['id_bast_item'] . '_F'))); ?><span class="spanradio">Tidak </span></label></td>
                                                            <td><?= form_input(array('type' => 'text','readonly'=>'readonly', 'name' => 'keterangan_' . $item['id_bast_item'] , 'autocomplete' => 'off', 'value' => $item['ket_check'], 'class' => 'form-control', 'id' => 'txt_' . $item['id_bast_item']  . '_ket', 'placeholder' => 'Keterangan')); ?></td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                </table>
                                            </div>
                                            <div class="col-sm-6" >
                                                <table class="table table-striped table-bordered table-hover" >
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Penerima", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td colspan="2"><?= form_input(array('type' => 'text', 'name' => 'pengecek[]', 'autocomplete' => 'off','readonly'=>'readonly', 'value' => @$pengecek['pengecek'], 'class' => 'form-control', 'placeholder' => 'Pengecek')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "KM", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td colspan="2"><?= form_input(array('type' => 'text', 'name' => 'km[]', 'autocomplete' => 'off','readonly'=>'readonly', 'value' => DefaultCurrency(@$km), 'class' => 'form-control', 'placeholder' => 'KM', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Interior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td>
                                                            <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "10", "name" => "penilaian_interior[]",'disabled'=>'disabled', 'value' => DefaultCurrency(@$interior), 'class' => 'form-control', 'id' => 'txt_range_penilaian_interior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Interior', "oninput" => "$('#txt_value_penilaian_interior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                        <td>
                                                            <?= form_input(array('type' => 'number', "name" => "penilaian_interior_value[]", 'min' => "0",'readonly'=>'readonly', 'max' => "10", 'value' => DefaultCurrency(@$interior), 'class' => 'form-control', 'id' => 'txt_value_penilaian_interior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Interior', "oninput" => "$('#txt_range_penilaian_interior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Interior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '10', 'name' => 'ket_interior[]', 'autocomplete' => 'off', 'value' => @$ket_interior,'readonly'=>'readonly', 'class' => 'form-control', 'placeholder' => 'Keterangan Interior')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Eksterior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td>
                                                            <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "10", "name" => "penilaian_eksterior[]", 'value' => DefaultCurrency(@$eksterior),'disabled'=>'disabled', 'class' => 'form-control', 'id' => 'txt_range_penilaian_eksterior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Eksterior', "oninput" => "$('#txt_value_penilaian_eksterior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                        <td>
                                                            <?= form_input(array('type' => 'number', "name" => "penilaian_eksterior_value[]", 'min' => "0", 'max' => "10", 'value' => DefaultCurrency(@$eksterior),'readonly'=>'readonly', 'class' => 'form-control', 'id' => 'txt_value_penilaian_eksterior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Eksterior', "oninput" => "$('#txt_value_penilaian_eksterior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Eksterior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Eksterior')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '10', 'name' => 'ket_eksterior[]', 'autocomplete' => 'off', 'value' => @$detail['ket_eksterior'],'readonly'=>'readonly', 'class' => 'form-control', 'placeholder' => 'Keterangan Eksterior')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Solar", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td>
                                                            <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "100", "name" => "solar[]", 'value' => DefaultCurrency(@$solar), 'class' => 'form-control', 'id' => 'txt_range_solar_' . @$detail['id_unit_serial'],'disabled'=>'disabled', 'placeholder' => 'Solar', "oninput" => "$('#txt_value_solar_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                        <td>
                                                            <?= form_input(array('type' => 'number', 'min' => "0", 'max' => "100", "name" => "solar_value[]", 'value' => DefaultCurrency(@$solar), 'class' => 'form-control', 'id' => 'txt_value_solar_' . @$detail['id_unit_serial'],'readonly'=>'readonly', 'placeholder' => 'Solar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', "oninput" => "$('#txt_value_solar_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Tambahan", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '20', 'name' => 'ket_det[]', 'autocomplete' => 'off', 'value' => @$detail['ket_det'], 'class' => 'form-control','readonly'=>'readonly', 'placeholder' => 'Description')); ?></td>
                                                    </tr>


                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>

                                    <?php if (CheckEmpty(@$is_view)) { ?>
                                        <div class="form-group" style="margin-top:50px">
                                            <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                        </div>
                                    <?php } ?>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    $(document).ready(function () {
        $("input[type=radio]").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
        $('input[type=radio]').prop('disabled', true);
        $(".select2").select2({disabled:true});
        $(".datepicker").datepicker();
    })




    $("#frm_kpu").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data receiving  berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_kpu/do_receive_manipulate',
                    dataType: 'json',
                    data: $("#frm_kpu").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/do_kpu";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })

</script>