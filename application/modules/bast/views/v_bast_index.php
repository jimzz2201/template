
<section class="content-header">
    <h1>
        BAST
        <small>Data Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">BAST</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">

                    <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                    <div class="col-sm-2">
                        <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                    </div>

                    <div class="col-sm-2">
                        <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown(array('name' => 'id_supplier', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier")); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown(array('name' => 'status', 'value' => "", 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                    </div>
                    <div class="col-sm-1">
                        <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                    </div>


                </div>



            </form>
            <hr/>
            <div class=" headerbutton">

                <?php echo anchor(site_url('bast/create_bast'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;

    function RefreshGrid()
    {
        table.fnDraw(false);
    }
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "bast/getdatabast", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_bast",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal", orderable: false, title: "Tanggal", "className": "text-center",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
                {data: "kode_bast", orderable: false, title: "Kode"},
                {data: "pengecek", orderable: false, title: "Pengecek"},
                {data: "vin_number", orderable: false, title: "VIN"},
                {data: "engine_no", orderable: false, title: "Engine No"},
                {data: "nama_kategori", orderable: false, title: "Kategori"},
                {data: "nama_unit", orderable: false, title: "Unit"},
                {data: "nama_type_unit", orderable: false, title: "Type Unit"},
                {data: "nama_pool", orderable: false, title: "Pool"},
                  {data: "doc_reference", orderable: false, title: "Reference", className: "text-center"},
                 {data: "type", orderable: false, title: "Type", className: "text-center"},
                {data: "km", orderable: false, title: "KM", "className": "text-right",
                    mRender: function (data, type, row) {
                        return Comma(data);
                    }},
                {
                    "data": "action",
                    "orderable": false,
                    "width": "100px",
                    "className": "text-center",
                    mRender: function (data, type, row) {
                        var returnhtml = row['view'];
                        returnhtml += row['print'];
                        
                        return returnhtml;
                    },
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
