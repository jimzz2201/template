<section class="content-header">
    <h1>
        Receive Delivery Order <?= @$button ?>
        <small>Delivery Order KPU</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Delivery Order KPU</li>
        <li class="active">KPU <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_kpu" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_bast" value="<?php echo @$id_bast; ?>" /> 
                               


                                <div class="form-group">
                                    <?= form_label('Tanggal BAST', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Pool', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_pool', 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Pool'), DefaultEmptyDropdown(@$list_pool, "json", "Pool"), @$id_pool); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?php
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_unit_serial', 'class' => 'form-control select2', 'id' => 'dd_id_serial_unit', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit_serial, "json", "Unit"), @$id_serial_unit);
                                        ?>
                                    </div>
                                    <div class="col-sm-1">
                                         <button type="button" class="btn btn-primary btn-block" id="btt_search" >Search</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>

                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Warna')); ?>


                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('KPU', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => @$nomor_master, 'class' => 'form-control', 'id' => 'txt_nomor_master', 'placeholder' => 'KPU')); ?>
                                        <?= form_input(array('type' => 'hidden', 'readonly' => 'readonly', 'name' => 'id_do_kpu', 'value' => @$id_do_kpu, 'class' => 'form-control', 'id' => 'txt_id_do_kpu', 'placeholder' => 'id do KPU')); ?>
                                    </div>
                                    <?= form_label('Buka Jual', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => @$nomor_buka_jual, 'class' => 'form-control', 'id' => 'txt_nomor_buka_jual', 'placeholder' => 'Buka Jual')); ?>
                                    </div>
                                    <?= form_label('Propek', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => @$no_prospek, 'class' => 'form-control', 'id' => 'txt_no_prospek', 'placeholder' => 'Prospek')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Customer', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Nama Customer')); ?>
                                    </div>
                                    <?= form_label('Current Pool', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => @$nama_pool, 'class' => 'form-control', 'id' => 'txt_nama_pool', 'placeholder' => 'Nama Pool')); ?>
                                        <?= form_input(array('type' => 'hidden', 'readonly' => 'readonly', 'name' => 'id_pool_asal', 'value' => @$id_pool, 'class' => 'form-control', 'id' => 'txt_id_pool_asal', 'placeholder' => 'Nama Pool')); ?>
                                    </div>



                                </div>
                                

                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text',  "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <hr/>
                                <div id="areado" style="position:relative">


                                    <hr/>
                                    <div style="position:relative">
                                        <div class="form-group">

                                            <h3 style="font-weight: bold;">Berita Acara Serah Terima Unit </h3>
                                            <h3 style="font-weight: bold;">Engine No : <span style="color:red" id="txt_engine_no">{Engine No}</span> | VIN Number : <span id="txt_vin_no" style="color:red">{Vin No}</span></h3>

                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6" >
                                                <table class="table table-striped table-bordered table-hover" >
                                                    <?php foreach (@$kelengkapan as $item) { ?>
                                                        <tr>
                                                            <td style="width:200px;"><?= form_label($item->nama_bast_item, "radio" ); ?></td>
                                                            <td style="width:100px;"><label class="labelradio"><?= form_radio('item_' . $item->id_bast_item , 'T', false, array("id" => 'radio_' . $item->id_bast_item  . '_T')); ?><span class="spanradio">Ya </span></label></td>
                                                            <td style="width:100px;"><label class="labelradio"><?= form_radio('item_' . $item->id_bast_item , 'F', false, array("id" => 'radio_' . $item->id_bast_item  . '_F')); ?><span class="spanradio">Tidak </span></label></td>
                                                            <td><?= form_input(array('type' => 'text', 'name' => 'keterangan_' . $item->id_bast_item , 'autocomplete' => 'off', 'value' => @$detail['keterangan'], 'class' => 'form-control', 'id' => 'txt_' . $item->id_bast_item  . '_ket', 'placeholder' => 'Keterangan')); ?></td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                </table>
                                            </div>
                                            <div class="col-sm-6" >
                                                <table class="table table-striped table-bordered table-hover" >
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Penerima", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td colspan="2"><?= form_input(array('type' => 'text', 'name' => 'pengecek', 'autocomplete' => 'off', 'value' => @$detail['pengecek'], 'class' => 'form-control', 'placeholder' => 'Pengecek')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "KM", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td colspan="2"><?= form_input(array('type' => 'text', 'name' => 'km', 'autocomplete' => 'off', 'value' => DefaultCurrency(@$detail['km']), 'class' => 'form-control', 'placeholder' => 'KM', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Interior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td>
                                                            <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "10", "name" => "penilaian_interior", 'value' => DefaultCurrency(@$penilaian_interior), 'class' => 'form-control', 'id' => 'txt_range_penilaian_interior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Interior', "oninput" => "$('#txt_value_penilaian_interior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                        <td>
                                                            <?= form_input(array('type' => 'number', "name" => "penilaian_interior_value", 'min' => "0", 'max' => "10", 'value' => DefaultCurrency(@$penilaian_interior), 'class' => 'form-control', 'id' => 'txt_value_penilaian_interior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Interior', "oninput" => "$('#txt_range_penilaian_interior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Interior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '10', 'name' => 'ket_interior', 'autocomplete' => 'off', 'value' => @$detail['ket_interior'], 'class' => 'form-control', 'placeholder' => 'Keterangan Interior')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Eksterior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td>
                                                            <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "10", "name" => "penilaian_eksterior", 'value' => DefaultCurrency(@$penilaian_eksterior), 'class' => 'form-control', 'id' => 'txt_range_penilaian_eksterior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Eksterior', "oninput" => "$('#txt_value_penilaian_eksterior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                        <td>
                                                            <?= form_input(array('type' => 'number', "name" => "penilaian_eksterior_value", 'min' => "0", 'max' => "10", 'value' => DefaultCurrency(@$penilaian_eksterior), 'class' => 'form-control', 'id' => 'txt_value_penilaian_eksterior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Eksterior', "oninput" => "$('#txt_value_penilaian_eksterior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Eksterior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Eksterior')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '10', 'name' => 'ket_eksterior', 'autocomplete' => 'off', 'value' => @$detail['ket_eksterior'], 'class' => 'form-control', 'placeholder' => 'Keterangan Eksterior')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Solar", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        <td>
                                                            <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "100", "name" => "solar", 'value' => DefaultCurrency(@$solar), 'class' => 'form-control', 'id' => 'txt_range_solar_' . @$detail['id_unit_serial'], 'placeholder' => 'Solar', "oninput" => "$('#txt_value_solar_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                        <td>
                                                            <?= form_input(array('type' => 'number', 'min' => "0", 'max' => "100", "name" => "solar_value", 'value' => DefaultCurrency(@$solar), 'class' => 'form-control', 'id' => 'txt_value_solar_' . @$detail['id_unit_serial'], 'placeholder' => 'Solar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', "oninput" => "$('#txt_value_solar_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Tambahan", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '20', 'name' => 'ket_det', 'autocomplete' => 'off', 'value' => @$detail['ket_det'], 'class' => 'form-control', 'placeholder' => 'Description')); ?></td>
                                                    </tr>


                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr/>

                                <?php if (CheckEmpty(@$is_view)) { ?>
                                    <div class="form-group" style="margin-top:50px">
                                        <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                    </div>
                                <?php } ?>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    $(document).ready(function () {
        $("input[type=radio]").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
        $(".select2").select2();
        $(".datepicker").datepicker();
    })

    $("#dd_id_serial_unit").change(function(){
        $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/unit/get_one_serial',
                    dataType: 'json',
                    data: $("#frm_kpu").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            $("#txt_nama_unit").val(data.obj.nama_unit);
                            $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                            $("#txt_nama_kategori").val(data.obj.nama_kategori);
                            $("#txt_top").val(data.obj.top);
                            $("#txt_vin").val(data.obj.vin);
                            $("#txt_nama_warna").val(data.obj.nama_warna);
                            $("#txt_nomor_master").val(data.obj.nomor_master);
                            $("#txt_no_prospek").val(data.obj.no_prospek);
                            $("#txt_nomor_buka_jual").val(data.obj.nomor_buka_jual);
                            $("#txt_nama_pool").val(data.obj.nama_pool);
                            $("#txt_nama_customer").val(data.obj.nama_customer);
                            $("#txt_engine_no").html(data.obj.engine_no);
                            $("#txt_vin_no").html(data.obj.vin_number);
                            $("#txt_id_do_kpu").val(data.obj.id_do_kpu);
                            $("#txt_id_pool_asal").val(data.obj.id_pool);
                        }
                          else
                        {
                            $("#txt_nama_unit").val("");
                            $("#txt_nama_type_unit").val("");
                            $("#txt_nama_kategori").val("");
                            $("#txt_top").val("");
                            $("#txt_vin").val("");
                            $("#txt_nama_warna").val("");
                            $("#txt_nomor_master").val("");
                            $("#txt_no_prospek").val("");
                            $("#txt_nomor_buka_jual").val("");
                            $("#txt_nama_pool").val("");
                            $("#txt_nama_customer").val("");
                            $("#txt_engine_no").val("");
                            $("#txt_vin_no").val("");
                            $("#txt_id_do_kpu").val("");
                            $("#txt_id_pool_asal").val("");
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
    })


    $("#frm_kpu").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data bast  berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/bast/bast_manipulate',
                    dataType: 'json',
                    data: $("#frm_kpu").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/bast";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })

</script>