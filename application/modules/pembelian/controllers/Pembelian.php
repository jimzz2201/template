<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembelian extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_pembelian');
    }

    public function batal_pembelian() {
        $message = '';
        $this->form_validation->set_rules('id_pembelian', 'Pembelian', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_pembelian->BatalPembelian($model['id_pembelian']);
        }

        echo json_encode($result);
    }

    public function getdokumen() {
        header('Content-Type: application/json');
        $id_supplier = $this->input->post("id_supplier");
        if (CheckEmpty($id_supplier)) {
            $id_supplier = 0;
        }
        $listdokumen = $this->m_pembelian->GetDropDownPembelian($id_supplier);
        $listdokumen = DefaultEmptyDropdown($listdokumen, "json", "Dokumen");
        echo json_encode(array("st" => true, "list" => $listdokumen));
    }

    public function GetOnePembelian() {
        $model = $this->input->post();

        $this->form_validation->set_rules('id_pembelian_master', 'nama pembelian', 'trim|required');
        $message = "";


        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $row = $this->m_pembelian->GetOnePembelian($model['id_pembelian_master'], true, true);
            echo json_encode(array("st" => $row != null, "obj" => $row));
        }
    }

    public function editpembelian($id, $nomorinvoice) {
        $row = (object) array();

        $module = "K088";
        $header = "K059";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $pembelian = $this->m_pembelian->GetOnePembelian($id, true);
        $this->m_supplier->KoreksiSaldoHutang($pembelian->id_supplier);
        if ($pembelian == null || $pembelian->nomor_master != str_replace("-", "/", $nomorinvoice)) {
            SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
            redirect(base_url() . 'index.php/main/message');
        } else {
            $row = $pembelian;
        }
        $this->load->model("satuan/m_satuan");
        $this->load->model("supplier/m_supplier");
        $this->load->model("cabang/m_cabang");
        $this->load->model("unit/m_unit");
        $this->load->model("type_unit/m_type_unit");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->tanggal = GetDateNow();
        $row->listheader = [];
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_type_unit = $this->m_type_unit->GetDropDownType_unit();
        $row->list_barang = $this->m_unit->GetDropDownUnit(array("id_tipe_persediaan"=>$row->type));
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $this->load->model("tipe_persediaan/m_tipe_persediaan");
        $row->list_tipe_persediaan = $this->m_tipe_persediaan->GetDropDownTipe_persediaan();
        $this->load->model("customer/m_customer");
        $row->list_customer = $this->m_customer->GetDropDownCustomer();
        
        $row->title = 'Create Pembelian';
        $jenis_pembayaran = array();
        $jenis_pembayaran[3] = "Tunai Kas Besar";
        $jenis_pembayaran[2] = "Kredit";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;
        $this->load->model("gl/m_gl");
        //$list_type_pembelian=$this->m_gl->GetJurnalBarang();
        //$list_type_pembelian[45] = "Obat";
        //$list_type_pembelian[46] = "Pakan";
        //$list_type_pembelian[21] = "Telur";
        
        //$row->list_type_pembelian = $list_type_pembelian;


        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row = json_decode(json_encode($row), true);
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css = array();
        $css[] = "assets/plugins/iCheck/all.css";
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        LoadTemplate($row, 'pembelian/v_pembelian_manipulate', $javascript, $css);
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K088";
        $header = "K059";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";


        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "pembelian/v_pembelian_index", $javascript, $css);
    }

    public function getdatapembelian() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_pembelian->GetDatapembelian($params);
    }

    public function getdatapenerimaan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_pembelian->GetDatapenerimaan($params);
    }

    public function getdatapembelianforpenerimaan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        $params['tgl_pengiriman'] = null;
        echo $this->m_pembelian->GetDatapembelian($params);
    }
    
    public function contentpembelian()
    {
        $model=$this->input->post();
        $arraymerge=['id_customer'=>0,'id_unit'=>0,'id_type_unit'=>0];
        $model['item']['listsubdetail'][]=$arraymerge;
        $this->load->view("pembelian/v_pembelian_content",$model);
    }
    
    public function contentpembeliandetail()
    {
        $model=$this->input->post();
        $arraymerge=['id_customer'=>0,'id_unit'=>0,'id_type_unit'=>0];
        $model['item']=array_merge($model,$arraymerge);
        $this->load->view("pembelian/v_pembelian_content_detail",$model);
    }
    
    

    public function create_pembelian() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K088";
        $header = "K059";
        CekModule($module);
        $this->load->model("satuan/m_satuan");
        $this->load->model("supplier/m_supplier");
        $this->load->model("cabang/m_cabang");
        $this->load->model("unit/m_unit");
        $this->load->model("type_unit/m_type_unit");
        $row->form = $header;
        $row->formsubmenu = $module;
        $row->tanggal = GetDateNow();
        $row->listheader = [];
        $row->listdetail = [];
        
        $row->listdetail[]['listsubdetail'][]=['id_customer'=>0,'id_unit'=>0,'id_type_unit'=>0];
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_type_unit = $this->m_type_unit->GetDropDownType_unit();
        $row->list_barang = $this->m_unit->GetDropDownEquipment();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $this->load->model("tipe_persediaan/m_tipe_persediaan");
        $row->list_tipe_persediaan = $this->m_tipe_persediaan->GetDropDownTipe_persediaan();
        
        
        $row->title = 'Create Pembelian';
        $jenis_pembayaran = array();
        $jenis_pembayaran[3] = "Tunai Kas Besar";
        $jenis_pembayaran[2] = "Kredit";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row->list_type_ppn = $list_type_ppn;
        $this->load->model("gl/m_gl");
        //$list_type_pembelian=$this->m_gl->GetJurnalBarang();
        //$list_type_pembelian[45] = "Obat";
        //$list_type_pembelian[46] = "Pakan";
        //$list_type_pembelian[21] = "Telur";
        
        //$row->list_type_pembelian = $list_type_pembelian;


        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row->list_type_pembulatan = $list_type_pembulatan;
        $row = json_decode(json_encode($row), true);
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css = array();
        $css[] = "assets/plugins/iCheck/all.css";
        LoadTemplate($row, 'pembelian/v_pembelian_manipulate', $javascript, $css);
    }

    public function mutasi_pembelian_action() {
        $model = $this->input->post();

        $this->form_validation->set_rules('id_pembelian', 'nama pembelian', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'gudang', 'trim|required');
        $this->form_validation->set_rules('tanggal_jurnal', 'tanggal jurnal', 'trim|required');
        $this->form_validation->set_rules('jenis', 'jenis aliran', 'trim|required');

        $message = "";
        if (array_column($model, 'id_history')) {
            foreach ($model['id_history'] as $key => $history) {
                if ($model['qty'][$key] > $model['stock'][$key] && $history != "0" && $model['jenis'] == "2") {
                    $message .= "Quantity tidak boleh lebih besar dari stock yang tersedia";
                }
            }
        }

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $data = $this->m_pembelian->MutasiBarang($model);
            echo json_encode($data);
        }
    }

    public function pembelian_po_manipulate() {
        $message = '';
        $model = $this->input->post();

        if (CheckEmpty($model['dataset']) || count($model['dataset']) == 0) {
            $message .= "Detail Pembelian is required<br/>";
        }
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_gudang', 'Gudang', 'trim|required');
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_pembelian->PembelianPOManipulate($model);
            echo json_encode($status);
        }
    }

    public function pembelian_manipulate() {
        $message = '';
        $model = $this->input->post();
        $listareakey= CheckArray($model, 'areakey');
        $listidserialunit=CheckArray($model, 'id_serial_unit');
        $listpembelian=[];
        $message="";
        $listtempserial=[];
        foreach($listareakey as $key=>$indexvalue)
        {
            $listincrement= CheckArray($model, "id_increment".$key);
            $listprospekdetail= CheckArray($model, "id_prospek_detail".$key);
            $listpricelist=CheckArray($model, "pricelist".$key);
            $listdisc=CheckArray($model, "disc".$key);
            $listqty=CheckArray($model, "quantity".$key);
            $listbrg= CheckArray($model, "id_barang".$key);
            $listketerangan=CheckArray($model, "keterangan".$key);
            if (!(array_search($listidserialunit[$key],$listtempserial) === false)) {
                $message .= "Tidak boleh ada unit yang sama dikeluarkan dalam buka jual<br/>";
            }
            $listtempserial[]=@$listidserialunit[$key];


            foreach($listincrement as $inc=>$incrementvalue)
            {
                $detailpembelian['id_unit_serial']=@$listidserialunit[$key];
                $detailpembelian['id_unit']=@$listbrg[$inc];
                $detailpembelian['qty']= DefaultCurrencyDatabase(@$listqty[$inc]);
                $detailpembelian['disc1']=DefaultCurrencyDatabase(@$listdisc[$inc]);
                $detailpembelian['price']=DefaultCurrencyDatabase(@$listpricelist[$inc]);
                $detailpembelian['is_persen_1']=1;
                $detailpembelian['subtotal']=@$detailpembelian['price'] * ((100 - @$detailpembelian['disc1']) / 100) * @$detailpembelian['qty'];
                $detailpembelian['keterangan_detail']=@$listketerangan[$inc];
                if(!CheckEmpty(@$detailpembelian['qty']))
                {
                    if(CheckEmpty(@$detailpembelian['id_unit']))
                    {
                       $message.="Data Barang yang kosong<br/>";
                    }
                    else
                    {
                        $listpembelian[]=$detailpembelian;
                    }
                }
                
                
            }
            
            
        }
        
        $model['listdetail']=$listpembelian;
        
       
        if ( count($listpembelian) == 0) {
            $message .= "Detail Pembelian is required<br/>";
        }
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('jth_tempo', 'Jatuh Tempo', 'trim|required');
        $this->form_validation->set_rules('type_pembayaran', 'Pembayaran', 'trim|required');
        $this->form_validation->set_rules('tipe_persediaan', 'Type Pembelian', 'trim|required');
        if (!CheckEmpty(@$model['type_pembayaran']) && $model['type_pembayaran'] == "1") {
            $this->form_validation->set_rules('jumlah_bayar', 'Jumlah Bayar', 'trim|required');
        }
       
        
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
           $status = $this->m_pembelian->PembelianManipulate($model);
            echo json_encode($status);
        }
    }

    public function get_one_pembelian() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_pembelian_master', 'nama pembelian', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_pembelian->GetOnePembelian($model["id_pembelian_master"], true);
            $data['st'] = $data['obj'] != null;
            if (CheckEmpty(@$model['typehtml'])) {
                $data['html'] = $this->load->view("retur/v_retur_beli_content", $data['obj'], true);
            } else {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_hutang_content", $data['obj'], true);
            }


            $data['msg'] = !$data['st'] ? "Data pembelian tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function penerimaan_batal() {
        $message = '';
        $this->form_validation->set_rules('id_penerimaan', 'Barang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_pembelian->BatalPenerimaan($model['id_penerimaan']);
        }

        echo json_encode($result);
    }

    public function pembelian_delete() {
        $message = '';
        $this->form_validation->set_rules('id_pembelian', 'Barang', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_pembelian->BarangDelete($model['id_pembelian']);
        }

        echo json_encode($result);
    }

}

/* End of file Barang.php */