//<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pembelian extends CI_Model {

    public $table = '#_kpu_master';
    public $id = 'id_pembelian_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDetailPembelianTelur($tanggal_from, $tanggal_to, $cabang = null, $is_telur = true) {
        $this->db->from("#_kpu_detail");
        $this->db->join("#_kpu_master", "#_kpu_detail.id_pembelian_master=#_kpu_master.id_pembelian_master");
        $this->db->select("tanggal,ifnull(sum(qty),0) as qty");
        $where = array();
        if (!CheckEmpty($cabang)) {
            $where['id_cabang'] = $cabang;
        }
        $this->db->where("#_kpu_master.tanggal between '" . DefaultTanggalDatabase($tanggal_from) . "' and '" . DefaultTanggalDatabase($tanggal_to) . "'");

        $where['#_kpu_master.status'] = 1;
        $where['#_kpu_detail.status'] = 1;
        if ($is_telur) {
            $where['#_kpu_detail.id_barang'] = 42;
        } else {
            $where['#_kpu_master.type_pembelian'] = 46;
            $where['#_kpu_detail.id_barang !='] = 42;
        }
        $this->db->where($where);
        $this->db->group_by("tanggal");
        $this->db->order_by("tanggal","asc");
        $listpembelian = $this->db->get()->result();
        $arrpembelian = [];

        foreach ($listpembelian as $row) {
            $arrpembelian[$row->tanggal] = $row;
        }
        return $arrpembelian;
    }

    function BatalPembelian($id_pembelian) {
        $message = "";
        $pembelian = $this->m_pembelian->GetOnePembelian($id_pembelian);
        $this->load->model("supplier/m_supplier");
        $this->load->model("barang/m_barang");
        if ($pembelian->status == 2) {
            $message .= "Pembelian telah dibatalkan<br/>";
        }
        $this->db->from("#_pembayaran_utang_detail");
        $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id");
        $this->db->where(array("id_pembelian_master" => $id_pembelian, "#_pembayaran_utang_master.keterangan !=" => "Pembayaran Di Muka", "#_pembayaran_utang_master.status" => 1));
        $utangpembayaran = $this->db->get()->row();
        if ($utangpembayaran) {
            $message .= "Pembelian sudah memiliki pembayaran , batalkan pembayaran terlebih dahulu<br/>";
        }


        if ($message == "") {
            try {
                $this->db->trans_rollback();
                $this->db->trans_begin();
                $this->db->update("#_kpu_master", array("status" => 2, "updated_date" => GetDateNow(), "updated_by" => GetUserId()), array("id_pembelian_master" => $id_pembelian));
                $updatebukubesar = array();
                $updatebukubesar['debet'] = 0;
                $updatebukubesar['kredit'] = 0;
                $updatebukubesar['updated_date'] = GetDateNow();
                $updatebukubesar['updated_by'] = GetUserId();
                $this->db->update("#_buku_besar", $updatebukubesar, array("dokumen" => $pembelian->nomor_master));

                $this->db->from("#_pembayaran_utang_detail");
                $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id");
                $this->db->where(array("id_pembelian_master" => $id_pembelian, "#_pembayaran_utang_master.keterangan" => "Pembayaran Di Muka", "#_pembayaran_utang_master.status" => 1));
                $utangpembayaran = $this->db->get()->row();
                if ($utangpembayaran) {
                    $this->db->update("#_pembayaran_utang_master", array("updated_date" => GetDateNow(), "updated_by" => GetUserId(), "status" => 2), array("pembayaran_utang_id" => $utangpembayaran->pembayaran_utang_id));
                }
                $this->db->update("#_buku_besar", $updatebukubesar, array("dokumen" => $pembelian->nomor_master));
                $this->db->update("#_barang_jurnal", $updatebukubesar, array("dokumen" => $pembelian->nomor_master));
                $this->m_supplier->KoreksiSaldoHutang($pembelian->id_supplier);
                $this->m_barang->KoreksiSaldoBarangByDokumen($pembelian->nomor_master);

                if ($this->db->trans_status() === FALSE || $message != '') {
                    $this->db->trans_rollback();
                    $message = "Some Error Occured<br/>" . $message;
                    return array("st" => false, "msg" => $message);
                } else {
                    $this->db->trans_commit();
                    $arrayreturn["msg"] = "Data Pembelian Sudah batalkan";
                    $arrayreturn["st"] = true;
                    return $arrayreturn;
                }
            } catch (Exception $ex) {
                $this->db->trans_rollback();
                return array("st" => false, "msg" => $ex->getMessage());
            }
        } else {
            return array("st" => $message == "", "msg" => $message);
        }
    }

    function GetDropDownPembelian($id_supplier, $islunas = false) {
        if ($islunas) {
            $this->db->where(array("tanggal_pelunasan" => null));
        }
        $this->db->where(array("id_supplier" => $id_supplier));
        $listsupplier = GetTableData($this->table, 'id_pembelian_master', array('nomor_master',"no_faktur"), array($this->table . '.status !=' => 2, $this->table . '.deleted_date' => null));
        return $listsupplier;
    }

    function InsertKartuStockPembelian($id_pembelian_master) {
        $this->load->model("barang/m_barang");
        $this->db->from("#_kpu_master");
        $this->db->where(array("id_pembelian_master" => $id_pembelian_master));
        $row = $this->db->get()->row();
        $userid = GetUserId();
        if ($row) {

            $this->db->from("#_kpu_detail");
            $this->db->where(array("id_pembelian_master" => $id_pembelian_master, "#_kpu_detail.status !=" => 5));
            $this->db->join("#_barang", "#_barang.id_barang=#_kpu_detail.id_barang");
            $this->db->join("#_satuan", "#_satuan.id_satuan=#_barang.id_satuan", "left");
            $this->db->select("#_kpu_detail.*,#_barang.id_gl_account,kode_barang,nama_satuan");
            $listpakan = $this->db->get()->result();
            $arrbukubesar = [];

            if (count($listpakan) > 0) {


                foreach ($listpakan as $pembeliandetail) {
                    $this->db->from("#_barang_jurnal");
                    $this->db->where(array("id_barang" => $pembeliandetail->id_barang, "reference_id" => $pembeliandetail->id_pembelian_detail, "dokumen" => $row->nomor_master));
                    $jurnaldetail = $this->db->get()->row();

                    if ($jurnaldetail) {
                        if ($jurnaldetail->kredit != $pembeliandetail->qty || $jurnaldetail->tanggal_jurnal != $row->tanggal) {
                            $history = [];
                            $history['tanggal_jurnal'] = $row->tanggal;
                            $history['add_info'] = $row->no_faktur;
                            $history['debet'] = $pembeliandetail->qty;
                            $history['updated_date'] = GetDateNow();
                            $history['updated_by'] = GetUserId();
                            $history['harga'] = $pembeliandetail->price_barang;
                            $this->db->update("#_barang_jurnal", $history, array("id_jurnal_barang" => $jurnaldetail->id_jurnal_barang));
                        }
                        $arrbarangjurnal[] = $jurnaldetail->id_jurnal_barang;
                    } else {
                        $history = [];
                        $history['id_barang'] = $pembeliandetail->id_barang;
                        $history['reference_id'] = $pembeliandetail->id_pembelian_detail;
                        $history['tanggal_jurnal'] = $row->tanggal;
                        $history['id_transaction'] = $id_pembelian_master;
                        $history['dokumen'] = $row->nomor_master;
                        $history['add_info'] = $row->no_faktur;
                        $history['description'] = "Pembelian Barang" . $pembeliandetail->nama_barang;
                        $history['debet'] = $pembeliandetail->qty;
                        $history['kredit'] = 0;
                        $history['harga_average'] = 0;
                        $history['harga_beli_akhir'] = 0;
                        $history['harga'] = $pembeliandetail->price_barang;
                        $history['id_cabang'] = $row->id_cabang;
                        $history['created_date'] = GetDateNow();
                        $history['created_by'] = GetUserId();
                        $this->db->insert("#_barang_jurnal", $history);
                        $arrbarangjurnal[] = $this->db->insert_id();
                    }
                }
            }

            if (count($arrbarangjurnal) > 0) {
                $this->db->where_not_in("id_jurnal_barang", $arrbarangjurnal);
                $this->db->where(array("dokumen" => $row->nomor_master));
                $this->db->update("#_barang_jurnal", array("debet" => 0, 'kredit' => 0, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("#_barang_jurnal", array("debet" => 0, 'kredit' => 0, 'updated_date' => GetDateNow()), array("dokumen" => $row->nomor_master));
            }
            $this->m_barang->KoreksiSaldoBarangByDokumen($row->nomor_master);
        }
    }

    public function UpdateStatusPembelian($id) {
        $listdetail = $this->GetDetail($id, true);
        $isfull = true;
        foreach ($listdetail as $detail) {
            if ($detail->jumlahpenerimaan < $detail->qty) {
                $isfull = false;
            }
        }
        if ($isfull) {
            $this->db->from("#_penerimaan_pembelian");
            $this->db->where(array("id_pembelian_master" => $id, "status !=" => 2));
            $this->db->order_by("tanggal_penerimaan", "desc");
            $row = $this->db->get()->row();
            if ($row != null) {
                $this->db->update("#_kpu_master", array("tgl_pengiriman" => $row->tanggal_penerimaan), array("id_pembelian_master" => $id));
            }
        } else {
            $this->db->update("#_kpu_master", array("tgl_pengiriman" => null), array("id_pembelian_master" => $id));
        }
    }

    function GetDetailPembelianSummary($id) {
        $this->db->from("#_kpu_detail");
        $this->db->where(array("id_kpu" => $id));
        $row = $this->db->get()->row();
        if ($row) {
            $row->qtyditerima = $this->GetJumlahPenerimaan($id);
        }

        return $row;
    }

    function GetDetail($id, $terima = false) {
        $this->db->select("#_kpu_detail.*");
        $this->db->from("#_kpu_detail");
        $this->db->join("#_unit", "#_unit.id_unit=#_kpu_detail.id_unit", "left");

        $this->db->where(array("id_kpu" => $id, "#_kpu_detail.status !=" => 5));

        $listdetail = $this->db->get()->result();

        return $listdetail;
    }
    
    function GetDetailObject($id, $terima = false) {
        $this->db->select("#_kpu_detail.*,#_buka_jual.id_customer,nama_customer,no_prospek,nomor_buka_jual,#_unit_serial.vin_number,engine_no");
        $this->db->join("#_unit_serial","#_unit_serial.id_unit_serial=#_kpu_detail.id_unit_serial","left");
        $this->db->from("#_kpu_detail");
        $this->db->join("#_unit", "#_unit.id_unit=#_kpu_detail.id_unit", "left");
        $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual", "left");
        $this->db->join("#_prospek", "#_prospek.id_prospek=#_buka_jual.id_prospek", "left");
        $this->db->join("#_customer", "#_buka_jual.id_customer=#_customer.nama_customer", "left");
        $this->db->where(array("id_kpu" => $id, "#_kpu_detail.status !=" => 5));

        $listdetail = $this->db->get()->result_array();
        $arrreturn=[];
        foreach($listdetail as $det)
        {
            if(!CheckKey($arrreturn, $det['id_unit_serial']))
            {
               $arrreturn[$det['id_unit_serial']] =[];
               $arrreturn[$det['id_unit_serial']]['listsubdetail'] =[];
               $arrreturn[$det['id_unit_serial']]['id_customer']=$det['id_customer'];
               $arrreturn[$det['id_unit_serial']]['nama_customer']=$det['nama_customer'];
               $arrreturn[$det['id_unit_serial']]['id_serial_unit']=$det['id_unit_serial'];
               $arrreturn[$det['id_unit_serial']]['no_prospek']=$det['no_prospek'];
               $arrreturn[$det['id_unit_serial']]['nomor_buka_jual']=$det['nomor_buka_jual'];
               $arrreturn[$det['id_unit_serial']]['vin_number']=$det['vin_number'];
               $arrreturn[$det['id_unit_serial']]['engine_no']=$det['engine_no'];
            }
            $arrreturn[$det['id_unit_serial']]['listsubdetail'][]=$det;
        }
        
        return $arrreturn;
    }
    
    

    function GetOnePembelian($id, $isfull = false, $terima = false) {
        $this->db->from("#_kpu");
        $this->db->where(array("id_kpu" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetailObject($id, $terima);
          
            $this->db->from("#_pembayaran_utang_detail");
            $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id", "left");
            $this->db->where(array("id_kpu" => $id, "#_pembayaran_utang_master.status !=" => 2));
            $listpembayaran = $this->db->get()->result();
            $jumlah = 0;
            foreach ($listpembayaran as $pembayaran) {
                $jumlah += $pembayaran->jumlah_bayar;
            }
            $row->sisa = $row->grandtotal - $jumlah;
            $row->terbayar = $jumlah;
            $row->listpembayaran = $listpembayaran;
        }

        return $row;
    }
    function SetLunasPembelian($id_pembelian_master) {
        $this->db->from("#_pembayaran_utang_detail");
        $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id", "left");
        $this->db->order_by("#_pembayaran_utang_master.tanggal_transaksi", "desc");
        $this->db->select("sum(#_pembayaran_utang_detail.jumlah_bayar) as jml,#_pembayaran_utang_master.tanggal_transaksi,#_pembayaran_utang_master.created_by,#_pembayaran_utang_master.created_date");
        $this->db->where(array("id_pembelian_master" => $id_pembelian_master, "#_pembayaran_utang_master.status" => 1));
        $pembayaran = $this->db->get()->row();
        $pembelian = $this->GetOnePembelian($id_pembelian_master);
        if ($pembelian) {
            if ($pembayaran) {
                if ($pembayaran->jml >= $pembelian->grandtotal) {
                    if ($pembelian->tanggal_pelunasan == null) {
                        $this->db->update("#_kpu_master", array("updated_by" => $pembayaran->created_by, "updated_date" => $pembayaran->created_date, "tanggal_pelunasan" => $pembayaran->tanggal_transaksi), array("id_pembelian_master" => $id_pembelian_master));
                    }
                } else {
                    if ($pembelian->tanggal_pelunasan != null) {
                        $this->db->update("#_kpu_master", array("tanggal_pelunasan" => null), array("id_pembelian_master" => $id_pembelian_master));
                    }
                }
            } else {
                if ($pembelian->tanggal_pelunasan != null&& $pembelian->grandtotal>0) {
                    $this->db->update("#_kpu_master", array("tanggal_pelunasan" => null), array("id_pembelian_master" => $id_pembelian_master));
                }
                else
                {
                    $this->db->update("#_kpu_master", array("tanggal_pelunasan" => null), array("id_pembelian_master" => $id_pembelian_master));
                }
            }
        }
    }

    function PembelianPOManipulate($model) {
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $pembelianpo = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("barang/m_barang");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
            $pembelianpo['kepada'] = $model['kepada'];
            $pembelianpo['no_fax'] = $model['no_fax'];
            $pembelianpo['pembuat'] = $model['pembuat'];

            $pembelianpo['keterangan'] = $model['keterangan'];
            $pembelianpo['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $pembelianpo['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $pembelianpo['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $pembelianpo['tgl_po'] = DefaultTanggalDatabase($model['tgl_po']);
            $pembelianpo['nominal_po'] = 0;
            $id_pembelian = 0;
            if (CheckEmpty(@$model['id_pembelian_po_master'])) {
                $pembelianpo['created_date'] = GetDateNow();
                $pembelianpo['nomor_po_master'] = AutoIncrement('#_kpu_po_master', $cabang->kode_cabang . '/PO/' . date("y") . '/', 'nomor_po_master', 5);
                $pembelianpo['created_by'] = ForeignKeyFromDb($userid);
                $pembelianpo['status'] = '1';
                $this->db->insert("#_kpu_po_master", $pembelianpo);
                $id_pembelian = $this->db->insert_id();
            } else {
                $pembelianpo['updated_date'] = GetDateNow();
                $pembelianpo['updated_by'] = ForeignKeyFromDb($userid);
                $id_pembelian = $model['id_pembelian_po_master'];
                $pembeliandb = $this->GetOnePembelianPo($id_pembelian);
                $pembelianpo['nomor_po_master'] = $pembeliandb->nomor_po_master;
                $this->db->update("#_kpu_po_master", $pembelianpo, array("id_pembelian_po_master" => $id_pembelian));
            }
            
            
            $arraydetailpembelian = array();
            foreach ($model['dataset'] as $key => $det) {
                $detailpembelian = array();
                $detailpembelian['id_barang'] = $det['id_barang'];
                $detailpembelian['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailpembelian['nama_barang'] = $det['nama_barang'];
                $detailpembelian['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailpembelian['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailpembelian['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailpembelian['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);

                if (CheckEmpty($det['id_pembelian_po_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailpembelian['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailpembelian['created_date'] = GetDateNow();
                    $detailpembelian['created_by'] = $userid;
                    $detailpembelian['status'] = 1;
                    $detailpembelian['id_pembelian_po_master'] = $id_pembelian;
                    $this->db->insert("#_kpu_po_detail", $detailpembelian);
                    $arraydetailpembelian[] = $this->db->insert_id();
                } else {

                    $detailpembelian['updated_date'] = GetDateNow();
                    $detailpembelian['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailpembelian['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("#_kpu_po_detail", $detailpembelian, array("id_pembelian_po_detail" => $det['id_pembelian_po_detail']));
                    $arraydetailpembelian[] = $det['id_pembelian_po_detail'];
                }
            }


            if (count($arraydetailpembelian) > 0) {
                $this->db->where_not_in("id_pembelian_po_detail", $arraydetailpembelian);
                $this->db->where(array("id_pembelian_po_master" => $id_pembelian));
                $this->db->update("#_kpu_po_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("#_kpu_po_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_pembelian_po_master" => $pembelianpo));
            }

            $this->InsertKartuStockPembelian($pembelianpo['nomor_po_master']);


            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();

                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_pembelian_po_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_pembelian, $pembelianpo['nomor_po_master'], 'pembelianpo');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function PembelianManipulate($model) {

       
        try {
            $message = "";
           
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $pembelian = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("supplier/m_supplier");
            $this->load->model("tipe_persediaan/m_tipe_persediaan");
            $this->load->model("unit/m_unit");
            $this->load->model("kpu/m_kpu");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
            
            $pembelian['keterangan'] = $model['keterangan'];
            $pembelian['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $pembelian['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $pembelian['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $pembelian['jth_tempo'] = DefaultTanggalDatabase($model['jth_tempo']);
            $pembelian['type_ppn'] = $model['type_ppn'];
            $pembelian['no_faktur'] = $model['no_faktur'];
            $pembelian['type'] = $model['tipe_persediaan'];
            $pembelian['type_pembayaran'] = $model['type_pembayaran'];
            $pembelian['type_pembulatan'] = $model['type_pembulatan'];
            $pembelian['disc_pembulatan'] = DefaultCurrencyDatabase($model['disc_pembulatan']);
            $pembelian['jumlah_bayar'] = DefaultCurrencyDatabase($model['jumlah_bayar']);
            $pembelian['ppn']= DefaultCurrencyDatabase(@$model['ppn']);
            $totalpembelianwithoutppn = 0;
            $totalpembelianwithppn = 0;
            $totalinvoice = 0;
            
            $listpembelian= CheckArray($model, "listdetail");
            foreach($listpembelian as $detailpem)
            {
                $totalinvoice+=$detailpem['subtotal'];
            }
            
            $totalhitungan = $totalinvoice;
            $nominalppn = 0;
            if ($pembelian['type_pembulatan'] == "1") {
                $totalhitungan = $totalhitungan - $pembelian['disc_pembulatan'];
            }

            if ($pembelian['type_ppn'] == "1") {
                $totalpembelianwithoutppn += ($totalhitungan * 100) / (100 + $pembelian['ppn']);
                $totalpembelianwithppn += $totalhitungan;
            } else {
                $totalpembelianwithoutppn += $totalhitungan;
                $totalpembelianwithppn += $totalhitungan * (100 + $pembelian['ppn']) / 100;
            }

            $nominalppn = $totalpembelianwithppn - $totalpembelianwithoutppn;

            if ($pembelian['type_pembulatan'] == "1") {
                $totalpembelianwithoutppn = $totalpembelianwithoutppn - $pembelian['disc_pembulatan'];
            } else {
                $totalpembelianwithppn = $totalpembelianwithppn - $pembelian['disc_pembulatan'];
            }

            $pembelian['nominal_ppn'] = DefaultCurrencyDatabase($nominalppn);
            $pembelian['grandtotal'] = DefaultCurrencyDatabase($totalpembelianwithppn);
            $pembelian['jumlah_bayar'] = DefaultCurrencyDatabase($model['jumlah_bayar']) > $totalpembelianwithppn ? $totalpembelianwithppn : DefaultCurrencyDatabase($model['jumlah_bayar']);
            $pembelian['total'] = DefaultCurrencyDatabase($totalpembelianwithoutppn);
            $pembelian['ppn'] = DefaultCurrencyDatabase($model['ppn']);
            $supplier = $this->m_supplier->GetOneSupplier($pembelian['id_supplier']);
            if (CheckEmpty(@$model['id_kpu'])) {
                $pembelian['created_date'] = GetDateNow();
                $pembelian['nomor_master'] = AutoIncrement('#_kpu', $cabang->kode_cabang . '/LL/' . date("y") . '/', 'nomor_master', 5);
                $pembelian['created_by'] = ForeignKeyFromDb($userid);
                $pembelian['status'] = '1';
                $this->db->insert("#_kpu", $pembelian);
                $id_kpu = $this->db->insert_id();
            } else {
                $pembelian['updated_date'] = GetDateNow();
                $pembelian['updated_by'] = ForeignKeyFromDb($userid);
                $id_kpu = $model['id_kpu'];
                $kpudb = $this->m_kpu->GetOneKPU($id_kpu);
                $pembelian['nomor_master'] = $kpudb->nomor_master;
                $this->db->update("#_kpu", $pembelian, array("id_kpu" => $id_kpu));
            }
            
            
            
            $arraydetailpembelian = array();
            foreach ($listpembelian as $key => $det) {
                $detailpembelian = array();
                $detailpembelian['id_unit'] = $det['id_unit'];
                $barang=$this->m_unit->GetOneUnit($det['id_unit']);
                $detailpembelian['price'] = DefaultCurrencyDatabase($det['price']);
                $detailpembelian['nama_barang'] = @$barang->nama_unit;
                $detailpembelian['keterangan_detail'] = $det['keterangan_detail'];
                $detailpembelian['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailpembelian['disc1'] = DefaultCurrencyDatabase($det['disc1']);
                $detailpembelian['is_persen_1'] = DefaultCurrencyDatabase($det['is_persen_1']);
                $detailpembelian['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);
               
                $detailpembelian['id_unit_serial'] = ForeignKeyFromDb($det['id_unit_serial']);
               
                if (CheckEmpty(@$det['id_kpu_detail'])) {
                    $detailpembelian['created_date'] = GetDateNow();
                    $detailpembelian['created_by'] = $userid;
                    $detailpembelian['status'] = 1;
                    $detailpembelian['id_kpu'] = $id_kpu;
                    $this->db->insert("#_kpu_detail", $detailpembelian);
                    $arraydetailpembelian[] = $this->db->insert_id();
                } else {

                    $detailpembelian['updated_date'] = GetDateNow();
                    $detailpembelian['updated_by'] = $userid;
                    $this->db->update("#_kpu_detail", $detailpembelian, array("id_kpu_detail" => $det['id_kpu_detail']));
                    $arraydetailpembelian[] = $det['id_kpu_detail'];
                }
            }
            

            if (count($arraydetailpembelian) > 0) {
                $this->db->where_not_in("id_kpu_detail", $arraydetailpembelian);
                $this->db->where(array("id_kpu" => $id_kpu));
                $this->db->update("#_kpu_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("#_kpu_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_kpu" => $pembelian));
            }
            

            $this->db->from("#_pembayaran_utang_detail");
            $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id");
            $this->db->where(array("keterangan" => "Pembayaran Di Muka", "id_kpu" => $id_kpu));
            $list_jenis_pembayaran = array();
            $list_jenis_pembayaran[1] = "Tunai Kas Kecil";
            $list_jenis_pembayaran[3] = "Tunai Kas Besar";
            $list_jenis_pembayaran[2] = "Kredit";
            $pembayaran_utang_master = $this->db->get()->row();
            if ($pembayaran_utang_master != null && $pembelian['jumlah_bayar'] == 0) {
                $this->db->delete("#_pembayaran_utang_detail", array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->delete("#_pembayaran_utang_master", array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master != null && $pembayaran_utang_master->jumlah_bayar != $pembelian['jumlah_bayar']) {
                $updatedet = array();
                $updatedet['updated_date'] = GetDateNow();
                $updatedet['updated_by'] = $userid;
                $updatedet['jumlah_bayar'] = $pembelian['jumlah_bayar'];
                $updatemas = array();
                $updatemas['updated_date'] = GetDateNow();
                $updatemas['updated_by'] = $userid;
                $updatemas['total_bayar'] = $pembelian['jumlah_bayar'];
                $this->db->update("#_pembayaran_utang_detail", $updatedet, array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->update("#_pembayaran_utang_master", $updatemas, array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master == null && $pembelian['jumlah_bayar'] > 0) {
                $dokumenpembayaran = AutoIncrement('#_pembayaran_utang_master', $cabang->kode_cabang . '/BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster = array();
                $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi'] = $pembelian['tanggal'];
                $pembayaranmaster['id_supplier'] = $pembelian['id_supplier'];
                $pembayaranmaster['jenis_pembayaran'] = $list_jenis_pembayaran[$pembelian['type_pembayaran']];
                $pembayaranmaster['total_bayar'] = $pembelian['jumlah_bayar'] > $pembelian['grandtotal'] ? $pembelian['grandtotal'] : $pembelian['jumlah_bayar'];
                $pembayaranmaster['biaya'] = 0;
                $pembayaranmaster['potongan'] = 0;
                $pembayaranmaster['status'] = 1;
                $pembayaranmaster['keterangan'] = "Pembayaran Di Muka";
                $pembayaranmaster['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $pembayaranmaster['created_date'] = GetDateNow();
                $pembayaranmaster['created_by'] = $userid;
                $this->db->insert("#_pembayaran_utang_master", $pembayaranmaster);
                $pembayaran_utang_id = $this->db->insert_id();
                $pembayarandetail = array();
                $pembayarandetail['id_pembelian_master'] = $id_pembelian;
                $pembayarandetail['pembayaran_utang_id'] = $pembayaran_utang_id;
                $pembayarandetail['jumlah_bayar'] = $pembelian['jumlah_bayar'];
                $pembayarandetail['created_date'] = GetDateNow();
                $pembayarandetail['created_by'] = $userid;
                $this->db->insert("#_pembayaran_utang_detail", $pembayarandetail);
            }

            $this->load->model("gl_config/m_gl_config");
            $akunglpembelian = $this->m_gl_config->GetIdGlConfig("pembelian", $pembelian['id_cabang']);
            $arrakun = [];
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglpembelian, "dokumen" => $pembelian['nomor_master']));
            $akun_pembelian = $this->db->get()->row();
            if (CheckEmpty($akun_pembelian)) {
                $akunpembelianinsert = array();
                $akunpembelianinsert['id_gl_account'] = $akun_pembelian;
                $akunpembelianinsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunpembelianinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunpembelianinsert['keterangan'] = "Pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunpembelianinsert['dokumen'] = $pembelian['nomor_master'];
                $akunpembelianinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpembelianinsert['id_subject'] = $pembelian['id_supplier'];
                $akunpembelianinsert['debet'] = DefaultCurrencyDatabase($model['type_pembulatan'] == "1" ? $totalpembelianwithppn * 100 / (100 + $pembelian['ppn']) : $totalpembelianwithoutppn, 0);
                $akunpembelianinsert['kredit'] = 0;
                $akunpembelianinsert['created_date'] = GetDateNow();
                $akunpembelianinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpembelianinsert);
                $arrakun[] = $this->db->insert_id();
            } else {
                $akunpembelianupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunpembelianupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunpembelianupdate['keterangan'] = "Pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunpembelianupdate['dokumen'] = $pembelian['nomor_master'];
                $akunpembelianupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpembelianupdate['id_subject'] = $pembelian['id_supplier'];
                $akunpembelianupdate['debet'] = $totalpembelianwithoutppn;
                $akunpembelianupdate['kredit'] = 0;
                $akunpembelianupdate['updated_date'] = GetDateNow();
                $akunpembelianupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpembelianupdate, array("id_buku_besar" => $akun_pembelian->id_buku_besar));
                $arrakun[] = $akun_pembelian->id_buku_besar;
            }
            $akunglppn = $this->m_gl_config->GetIdGlConfig("ppnpembelian", $pembelian['id_cabang']);
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglppn, "dokumen" => $pembelian['nomor_master']));
            $akun_ppn = $this->db->get()->row();

            if (CheckEmpty($akun_ppn) && $pembelian['nominal_ppn'] > 0) {
                $akunppninsert = array();
                $akunppninsert['id_gl_account'] = $akunglppn;
                $akunppninsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunppninsert['id_cabang'] = $pembelian['id_cabang'];
                $akunppninsert['keterangan'] = "PPN pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunppninsert['dokumen'] = $pembelian['nomor_master'];
                $akunppninsert['subject_name'] = @$supplier->kode_supplier;
                $akunppninsert['id_subject'] = $pembelian['id_supplier'];
                $akunppninsert['debet'] = $pembelian['nominal_ppn'];
                $akunppninsert['kredit'] = 0;
                $akunppninsert['created_date'] = GetDateNow();
                $akunppninsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunppninsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_ppn)) {
                $akunppnupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunppnupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunppnupdate['keterangan'] = "PPN pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunppnupdate['dokumen'] = $pembelian['nomor_master'];
                $akunppnupdate['subject_name'] = @$supplier->kode_supplier;
                $akunppnupdate['id_subject'] = $pembelian['id_supplier'];
                $akunppnupdate['debet'] = $pembelian['nominal_ppn'];
                $akunppnupdate['kredit'] = 0;
                $akunppnupdate['updated_date'] = GetDateNow();
                $akunppnupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunppnupdate, array("id_buku_besar" => $akun_ppn->id_buku_besar));
                $arrakun[] = $akun_ppn->id_buku_besar;
            }

            
            $akunglutang = $this->m_gl_config->GetIdGlConfig("utang", $pembelian['id_cabang'], array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglutang, "dokumen" => $pembelian['nomor_master']));
            $akun_utang = $this->db->get()->row();

            if (CheckEmpty($akun_utang) && $pembelian['jumlah_bayar'] < $pembelian['grandtotal']) {
                $akunutanginsert = array();
                $akunutanginsert['id_gl_account'] = $akunglutang;
                $akunutanginsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunutanginsert['id_cabang'] = $pembelian['id_cabang'];
                $akunutanginsert['keterangan'] = "Utang pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunutanginsert['dokumen'] = $pembelian['nomor_master'];
                $akunutanginsert['subject_name'] = @$supplier->kode_supplier;
                $akunutanginsert['id_subject'] = $pembelian['id_supplier'];
                $akunutanginsert['debet'] = 0;
                $akunutanginsert['kredit'] = $pembelian['grandtotal'] - $pembelian['jumlah_bayar'];
                $akunutanginsert['created_date'] = GetDateNow();
                $akunutanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunutanginsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_utang)) {
                $akunutangupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunutangupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunutangupdate['keterangan'] = "Utang pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunutangupdate['dokumen'] = $pembelian['nomor_master'];
                $akunutangupdate['subject_name'] = @$supplier->kode_supplier;
                $akunutangupdate['id_subject'] = $pembelian['id_supplier'];
                $akunutangupdate['debet'] = 0;
                $akunutangupdate['kredit'] = $pembelian['grandtotal'] - $pembelian['jumlah_bayar'];
                $akunutangupdate['updated_date'] = GetDateNow();
                $akunutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunutangupdate, array("id_buku_besar" => $akun_utang->id_buku_besar));
                $arrakun[] = $akun_utang->id_buku_besar;
            }
            $this->m_supplier->KoreksiSaldoHutang(@$supplier->id_supplier);

            $akunpotonganpembelian = $this->m_gl_config->GetIdGlConfig("potonganpembelian", $pembelian['id_cabang'], array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpotonganpembelian, "dokumen" => $pembelian['nomor_master']));
            $akun_potongan = $this->db->get()->row();

            if (CheckEmpty($akun_potongan) && $pembelian['disc_pembulatan'] > 0) {
                $akunpotonganinsert = array();
                $akunpotonganinsert['id_gl_account'] = $akunpotonganpembelian;
                $akunpotonganinsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunpotonganinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunpotonganinsert['keterangan'] = "Potongan pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunpotonganinsert['dokumen'] = $pembelian['nomor_master'];
                $akunpotonganinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganinsert['id_subject'] = $pembelian['id_supplier'];
                $akunpotonganinsert['debet'] = 0;
                $akunpotonganinsert['kredit'] = $pembelian['disc_pembulatan'];
                $akunpotonganinsert['created_date'] = GetDateNow();
                $akunpotonganinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpotonganinsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_potongan)) {
                $akunpotonganupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunpotonganupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunpotonganupdate['keterangan'] = "Potongan pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunpotonganupdate['dokumen'] = $pembelian['nomor_master'];
                $akunpotonganupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganupdate['id_subject'] = $pembelian['id_supplier'];
                $akunpotonganupdate['debet'] = 0;
                $akunpotonganupdate['kredit'] = $pembelian['disc_pembulatan'];
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
                $arrakun[] = $akun_potongan->id_buku_besar;
            }

            $akunkas = $this->m_gl_config->GetIdGlConfig("kasbesar", $pembelian['id_cabang'], array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunkas, "dokumen" => $pembelian['nomor_master']));
            $akun_kas = $this->db->get()->row();

            if (CheckEmpty($akun_kas) && $pembelian['jumlah_bayar'] > 0) {
                $akunkasinsert = array();
                $akunkasinsert['id_gl_account'] = $akunkas;
                $akunkasinsert['tanggal_buku'] = $pembelian['tanggal'];
                $akunkasinsert['id_cabang'] = $pembelian['id_cabang'];
                $akunkasinsert['keterangan'] = "Pengeluaran kas untuk pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunkasinsert['dokumen'] = $pembelian['nomor_master'];
                $akunkasinsert['subject_name'] = @$supplier->kode_supplier;
                $akunkasinsert['id_subject'] = $pembelian['id_supplier'];
                $akunkasinsert['debet'] = 0;
                $akunkasinsert['kredit'] = $pembelian['jumlah_bayar'];
                $akunkasinsert['created_date'] = GetDateNow();
                $akunkasinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunkasinsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_kas)) {
                $akunkasupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akunkasupdate['id_cabang'] = $pembelian['id_cabang'];
                $akunkasupdate['keterangan'] = "Pengeluaran kas untuk Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akunkasupdate['dokumen'] = $pembelian['nomor_master'];
                $akunkasupdate['subject_name'] = @$supplier->kode_supplier;
                $akunkasupdate['id_subject'] = $pembelian['id_supplier'];
                $akunkasupdate['debet'] = 0;
                $akunkasupdate['kredit'] = $pembelian['jumlah_bayar'];
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
                $arrakun[] = $akun_kas->id_buku_besar;
            }
            $akunpenyeimbang = $this->m_gl_config->GetIdGlConfig("penyeimbang", $pembelian['id_cabang'], array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("dokumen" => $pembelian['nomor_master']));
            if (count($arrakun) > 0)
                $this->db->where_in("id_buku_besar", $arrakun);
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpenyeimbang, "dokumen" => $pembelian['nomor_master']));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = $akunpenyeimbang;
                $akun_penyeimbanginsert['tanggal_buku'] = $pembelian['tanggal'];
                $akun_penyeimbanginsert['id_cabang'] = $pembelian['id_cabang'];
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akun_penyeimbanginsert['dokumen'] = $pembelian['nomor_master'];
                $akun_penyeimbanginsert['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbanginsert['id_subject'] = $pembelian['id_supplier'];
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $akun_penyeimbanginsert['add_info'] = $pembelian['no_faktur'];
                $this->db->insert("#_buku_besar", $akun_penyeimbanginsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $pembelian['tanggal'];
                $akun_penyeimbangupdate['id_cabang'] = $pembelian['id_cabang'];
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk Ke " . @$supplier->nama_supplier . ' ' . $pembelian['keterangan'];
                $akun_penyeimbangupdate['dokumen'] = $pembelian['nomor_master'];
                $akun_penyeimbangupdate['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbangupdate['id_subject'] = $pembelian['id_supplier'];
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $akun_penyeimbangupdate['add_info'] = $pembelian['no_faktur'];
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
                $arrakun[] = $akun_penyeimbang->id_buku_besar;
            }
            if (count($arrakun) > 0) {
                $this->db->where_not_in("id_buku_besar", $arrakun);
                $this->db->where(array("dokumen" => $pembelian['nomor_master']));
                $this->db->update("#_buku_besar", array("debet" => 0, "kredit" => "0", "updated_date" => GetDateNow(), "updated_by" => $userid));
            }


            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Pembelian  Sudah di" . (CheckEmpty(@$model['id_kpu']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_kpu, $pembelian['nomor_master'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    
    

    // datatables

    function GetDatapembelian($params) {
        $this->load->library('datatables');
        $this->datatables->select('#_kpu.id_kpu,nama_tipe_persediaan,created_user.nama_user as created_name,updated_user.nama_user as updated_name,no_faktur,nama_supplier,#_kpu.tanggal,replace(nomor_master,"/","-") as nomor_master_link,nomor_master,#_kpu.grandtotal,#_kpu.jth_tempo,"" as nomor_pengajuan,#_kpu.status');
        $this->datatables->join("#_supplier", "#_supplier.id_supplier=#_kpu.id_supplier", "left");
        $this->datatables->from('#_kpu');
        $this->datatables->join("#_tipe_persediaan", "#_tipe_persediaan.id_tipe_persediaan=#_kpu.type", "left");
        $this->datatables->join("#_user created_user", "created_user.id_user=#_kpu.created_by", "left");
        $this->datatables->join("#_user updated_user", "updated_user.id_user=#_kpu.updated_by", "left");
        $this->datatables->group_by('#_kpu.id_kpu');
        $this->datatables->where("type !=","KPU");
//add this line for join
        //$this->datatables->join('table2', '#_kpu.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $extra = array();
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['#_kpu.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['#_kpu.id_supplier'] = $params['id_supplier'];
        }
        if (isset($params['status']) && !CheckEmpty(@$params['status'])) {
            $params['#_kpu.status'] = $params['status'];
        }
        unset($params['id_supplier']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_kpu.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
        if (!CheckEmpty(@$params['nomor_master'])) {
            $this->datatables->where(array("nomor_master" => @$params['nomor_master']));
        } else if (!CheckEmpty(@$params['no_faktur'])) {
            $this->datatables->where(array("no_faktur" => @$params['no_faktur']));
        } else {
            unset($params['nomor_master']);
            unset($params['no_faktur']);

            if (count($params)) {
                $this->datatables->where($params);
            }
            if (count($extra)) {
                $this->datatables->where(implode(" AND ", $extra));
            }
        }

        if ($isedit) {
            $straction .= anchor("pembelian/editpembelian/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_kpu,nomor_master_link');
        return $this->datatables->generate();
    }

}
