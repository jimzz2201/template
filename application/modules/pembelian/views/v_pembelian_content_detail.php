<tr id="areadetail<?php echo $key.'_'.$inc?>">
    <td>
        <?= form_input(array('type' => 'hidden', 'name' => 'id_increment' . $key . '[]', 'value' => $inc,  'class' => 'form-control increment'.$key, 'id' => 'txt_increment' . @$key . '_' . @$inc, 'placeholder' => 'Price List')); ?>
        <?= form_dropdown(array("name" => "id_barang" . $key . '[]',"value"=>@$subdet['id_unit']), DefaultEmptyDropdown(@$list_barang, "", "Barang"), @$subdet['id_unit'], array('class' => 'form-control selectbarang', 'id' => 'dd_id_barang' . @$key . '_' . @$inc)); ?>
    </td>
    <td>
        <?= form_input(array('type' => 'text','value'=> DefaultCurrency(@$subdet['qty']),  'class' => 'form-control', 'id' => 'txt_quantity' . @$key . '_' . @$inc, 'placeholder' => 'Quantity', 'name' => 'quantity' . $key . '[]', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?> 
    </td>
    <td>
        <?= form_input(array('type' => 'text','value'=>DefaultCurrency(@$subdet['price']), 'name' => 'pricelist' . $key . '[]', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_price_list' . @$key . '_' . @$inc, 'placeholder' => 'Price List')); ?>
    </td>
    <td>
        <?= form_input(array('type' => 'text','value'=>DefaultCurrency(@$subdet['disc1']), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'name' => 'disc' . $key . '[]', 'class' => 'form-control', 'id' => 'txt_disc' . @$key . '_' . @$inc, 'placeholder' => 'Disc')); ?>
    </td>
    <td>
        <?= form_input(array('type' => 'text','value'=>DefaultCurrency(@$subdet['subtotal']),  'class' => 'form-control', 'id' => 'txt_subtotal' . @$key . '_' . @$inc, 'placeholder' => 'Subtotal', 'readonly' => "readonly")); ?>
    </td>
    <td style="text-align: center">
        <?php if(!CheckEmpty($inc)){?>
            <button type="button"  class="btn btn-danger btn-xs" onClick="RemoveAreaDetail<?php echo $key?>(<?php echo $key.','.$inc ?>)"  > - </button>
        <?php }?>

    </td>
</tr>
<tr id="areaketerangan<?php echo $key.'_'.$inc?>">
    <td>
        Keterangan
    </td>
    <td colspan="5">
        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan' . @$key . '_' . @$inc,'class' => 'form-control', 'id' => 'txt_keterangan'.$key, 'placeholder' => 'Keterangan')); ?>

    </td>
</tr>
<script>
    $(document).ready(function(){
        $("#dd_id_barang<?php echo   @$key . '_' . @$inc ?>").select2({data:list_barang});
        <?php if(!CheckEmpty(@$subdet['id_unit'])){
            echo  '$("#dd_id_barang'.@$key . '_' . @$inc.'").val('.@$subdet['id_unit'].');';
       }?>
    })
</script>