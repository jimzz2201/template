
<section class="content-header">
    <h1>
        Pembelian
        <small>Data Monitoring</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Monitoring</li>
        <li class="active">Pembelian</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>
                        <?= form_label('s / d', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                        <?= form_label('Cabang', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'value' => @$id_cabang, 'class' => 'form-control', 'id' => 'id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'value' => "", 'class' => 'form-control', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "", "Status")); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Supplier', "dd_id_supplier", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_supplier', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier")); ?>
                        </div>

                        <?= form_label('No Faktur', "txt_status", array("class" => 'col-sm-1 control-label')); ?>    
                        <div class="col-sm-2">
                            <?= form_input(array("autocomplete" => "off", 'name' => 'no_faktur', 'class' => 'form-control', 'id' => 'txt_no_faktur')); ?>
                        </div>
                        <?= form_label('No Invoice', "txt_status", array("class" => 'col-sm-1 control-label')); ?>    
                        <div class="col-sm-2">
                            <?= form_input(array("autocomplete" => "off", 'name' => 'nomor_master', 'class' => 'form-control', 'id' => 'txt_no_master')); ?>
                        </div>
                        <div class="col-sm-2">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                    </div>
                </div>



            </form>

            <div class="box-body">
                <div id="notification" ></div>
                <div class=" headerbutton">

                    <?php echo anchor(site_url('pembelian/create_pembelian'), 'Create', 'class="btn btn-success"'); ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        table.fnDraw(false);
        return false;
    })
    function batal(id_pembelian) {
        swal({
            title: "Apa kamu yakin ingin membatalkan data pembelian berikut ini?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            $("#dd_action_" + id_pembelian).val(0);
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembelian/batal_pembelian',
                    dataType: 'json',
                    data: {
                        id_pembelian: id_pembelian
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }

                });
            }
        });


    }
    function RefreshGrid() {


    }

    $(document).ready(function () {
        $(".datepicker").datepicker();
        $("select").select2();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "pembelian/getdatapembelian", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_kpu",
                    title: "Kode",
                    width: "50px",
                    orderable: false
                }
                , {data: "tanggal", orderable: false, title: "Tanggal", "width": "80px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data == undefined ? 0 : data);
                    }}
                , {data: "nama_supplier", orderable: false, title: "Supplier", "width": "100px"}
                , {data: "nama_supplier", orderable: false, title: "Supplier", "width": "100px"}
                , {data: "nomor_master", orderable: false, title: "NO Doc", "width": "150px"}
                , {data: "nama_tipe_persediaan", orderable: false, title: "Tipe", "width": "150px"}
                , {data: "no_faktur", orderable: false, title: "Faktur", "width": "150px"}
                , {data: "grandtotal", orderable: false, title: "Jumlah", "width": "150px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
               
                ,
                {orderable: false, title: "User",
                    mRender: function (data, type, row) {
                        return row['updated_name'] == "" || row['updated_name'] == null ? row['created_name'] : row['updated_name'];
                    }},
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center nopadding",
                    "width": "150px",
                    mRender: function (data, type, row) {
                        return row['status'] == "2" ? "Batal" : data;
                    }
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {
                RefreshGrid();
            }
        });

    });
</script>
