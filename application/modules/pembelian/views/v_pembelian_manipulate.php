<section class="content-header">
    <h1>
        Pembelian <?= @$button ?>
        <small>Pembelian</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Pembelian</li>
        <li class="active">Pembelian <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <?php if(!CheckEmpty(@$id_kpu)){
                                        echo form_input(array('type' => 'hidden',  'name' => 'tipe_persediaan', 'value' => @$type, 'id' => 'txt_id_tipe_persediaan')); 
                                }?>
                                <input type="hidden" name="id_kpu" value="<?php echo @$id_kpu; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier, array('class' => 'form-control select2dropdown', 'id' => 'dd_id_supplier')); ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$id_cabang, array('class' => 'form-control select2dropdown', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                    <?= form_label('No Faktur', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'value' => @$no_faktur, 'class' => 'form-control', 'id' => 'txt_no_faktur', 'placeholder' => 'No Faktur', 'name' => 'no_faktur')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "json", "PPN"), @$type_ppn, array('class' => 'form-control select2dropdown', 'id' => 'dd_type_ppn')); ?>
                                    </div>
                                    <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$ppn), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Pembayaran', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_pembayaran"), DefaultEmptyDropdown(@$list_type_pembayaran, "", "Pembayaran"), @$type_pembayaran, array('class' => 'form-control select2dropdown', 'id' => 'dd_type_pembayaran')); ?>
                                    </div>
                                    <?= form_label('Jth Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'jth_tempo', 'value' => DefaultDatePicker(@$jth_tempo), 'class' => 'form-control datepicker', 'id' => 'txt_jth_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div> 

                                <div class="form-group">
                                    <?= form_label('Pembelian', "dd_id_barang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "tipe_persediaan"), DefaultEmptyDropdown(@$list_tipe_persediaan, "", "Type Pembelian"), @$type, array('class' => 'form-control select2dropdown', 'id' => 'dd_id_tipe_persediaan')); ?>
                                    </div>
                                    
                                </div>
                                <hr/>
                                <div class="areainputitem">
                                    <?php
                                    $key=0;
                                    foreach (@$listdetail as $id_serial_unit => $item) {
                                        include APPPATH . 'modules/pembelian/views/v_pembelian_content.php';
                                        $key+=1;
                                    }
                                    ?>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total', "readonly" => "readonly")); ?>
                                    </div>
                                    <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'nominal_ppn', 'value' => DefaultCurrency(@$nominal_ppn), 'class' => 'form-control', 'id' => 'txt_nominal_ppn', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Type Pembulatan', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "type_pembulatan"), @$list_type_pembulatan, @$type_pembulatan, array('class' => 'form-control select2dropdown', 'id' => 'dd_type_pembulatan')); ?>
                                    </div>
                                    <?= form_label('Pembulatan', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'disc_pembulatan', 'value' => DefaultCurrency(@$disc_pembulatan), 'class' => 'form-control', 'id' => 'txt_disc_pembulatan', 'placeholder' => 'Disc Pembulatan', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Grand Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$grandtotal), 'class' => 'form-control', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
                                    </div>
                                    <?= form_label('Jumlah&nbsp;Bayar', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?php
                                        $mergepembayaran = [];
                                        if (@$type_pembayaran !== "2") {
                                            $mergepembayaran['readonly'] = "readonly";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergepembayaran, array('type' => 'text', 'name' => 'jumlah_bayar', 'value' => DefaultCurrency(@$jumlah_bayar), 'class' => 'form-control', 'id' => 'txt_jumlah_bayar', 'placeholder' => 'Jumlah Bayar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);'))); ?>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;
    $("#dd_type_pembulatan").change(function () {
        HitungSemua();
    })

    function ChangeCustomer(id_key)
    {
        var id_customer = $("#dd_id_customer" + id_key).val();
        LoadBar.show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_unit_serial_full',
            dataType: 'json',
            data: {
                id_customer: id_customer
            },
            success: function (data) {
                $("#dd_id_unit_serial" + id_key).empty();
                if (data.st)
                {
                    $("#dd_id_serial_unit" + id_key).select2({data: data.list_unit_serial});

                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }


    function RefreshGrid()
    {
        table.fnClearTable();
        if (dataset.length > 0)
        {
            table.fnAddData(dataset);
            HitungSemua();
        }

    }
    $("#dd_type_pembelian").change(function () {
        dataset = [];
        RefreshGrid();
        RefreshTypePembelian();
    })
    function RefreshPPN()
    {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").removeAttr("readonly");
        } else
        {
            $("#txt_ppn").attr("readonly", "readonly");
        }
    }
    $("#dd_type_ppn").change(function () {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").val(10);
        } else
        {
            $("#txt_ppn").val(0);
        }
        RefreshPPN();
        HitungSemua();
    })
    function HitungSemua() {
        var total = 0;
        var nominalppn = 0;
        var ppn = Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));
        $(".detaildetail").each(function (indexkey, value) {
            var totalgroup = 0;
            var key = $(value).val();
            $(".increment" + key).each(function (indexincrement, valueincrement) {
                var inc = $(valueincrement).val();
                var pricelist = Number($("#txt_price_list" + key + '_' + inc).val().replace(/[^0-9\.]+/g, ""));
                var disc = Number($("#txt_disc" + key + '_' + inc).val().replace(/[^0-9\.]+/g, ""));
                var qty = Number($("#txt_quantity" + key + '_' + inc).val().replace(/[^0-9\.]+/g, ""));
                var subtotal = pricelist * ((100 - disc) / 100) * qty;
                $("#txt_subtotal" + key + '_' + inc).val(DefaultNumber(subtotal));
                totalgroup += subtotal;
            });
            $("#txt_subtotal" + key).val(DefaultNumber(totalgroup));
            total += totalgroup;
        });
        var grandtotal = total;

        if ($("#dd_type_pembulatan").val() == "1")
        {
            total = total - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
            grandtotal = grandtotal - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
        }


        if ($("#dd_type_ppn").val() == "2")
        {
            nominalppn = ppn / 100 * grandtotal;
            grandtotal = grandtotal + nominalppn;
        } else if ($("#dd_type_ppn").val() == "1")
        {
            nominalppn = ppn / (100 + Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""))) * grandtotal;
            total = total - nominalppn;
        }
        if ($("#dd_type_pembulatan").val() == "2")
        {
            grandtotal = grandtotal - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
        }
        $("#txt_grandtotal").val(number_format(grandtotal.toFixed(4)));
        $("#txt_nominal_ppn").val(number_format(nominalppn.toFixed(4)));
        $("#txt_total").val(number_format(total.toFixed(4)));
    }

    var countitem = 1;
    var list_customer =<?php echo json_encode(DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>;
    var list_type_unit =<?php echo json_encode(DefaultEmptyDropdown(@$list_type_unit, "json", "Type Unit")); ?>;
    var list_barang =<?php echo json_encode(DefaultEmptyDropdown(@$list_barang, "json", "Barang")); ?>;
    function AddArea()
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pembelian/contentpembelian',
            data: {
                key: countitem
            },
            success: function (data) {
                countitem += 1;
                $(".areainputitem").append(data);

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    
    
    function GetOneUnit(key)
    {
        alert(key);
    }
    
    
    function RemoveArea(key)
    {
        $(".areainput" + key).remove();
    }

    $(document).ready(function () {
        $(".select2dropdown").select2();
        $("#txt_tanggal").datepicker().on('changeDate', function (e) {
            $("#txt_jth_tempo").val(DefaultDateFormat(moment(e.date).add(1, "months"))); //Where e contains date, dates and format
        });
        <?php if(!CheckEmpty(@$id_kpu)){?>
           $("#dd_id_tipe_persediaan").select2({disabled:true}); 
           $(".selectbarang").select2({data:list_barang});
        <?php }?>
        
        
        $("#dd_id_tipe_persediaan").change(function () {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/unit/get_dropdown_unit',
                data: {
                    id_tipe_persediaan: $("#dd_id_tipe_persediaan").val()
                },
                success: function (data) {
                    list_barang = data.list_unit;
                    $(".areainputitem").empty();
                    countitem = 0;
                    AddArea();

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        })
        $("#txt_jth_tempo").datepicker();
        RefreshPPN();



    })


    $("#frm_pembelian").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data pembelian berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembelian/pembelian_manipulate',
                    dataType: 'json',
                    data: $("#frm_pembelian").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/pembelian";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })

</script>