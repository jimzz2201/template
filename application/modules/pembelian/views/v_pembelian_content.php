<?php @$inc = 0 ?>
<div class="areainput<?php echo @$key ?>">
    <?= form_input(array('type' => 'hidden', 'name' => 'areakey[]', 'value' => @$key, 'class' => 'detaildetail', 'id' => 'txt_id_key' . @$key, 'placeholder' => 'Keterangan')); ?>
    <div class="form-group">
        <div class="col-sm-1">
            <?php if (@$key == 0) { ?>
                <button type="button"  onClick="AddArea()"  class="btn btn-primary btn-block" >Tambah</button>
            <?php } else { ?>
                <button type="button" onClick="RemoveArea(<?php echo @$key ?>)" id="btt_delete<?php echo @$key ?>" class="btn btn-danger btn-block" >Delete</button>
            <?php } ?>
        </div>
        <?= form_label('Customer', "txt_supplier", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-4">
            <?= form_dropdown(array("name" => "id_customer[]", 'onchange' => "ChangeCustomer(" . $key . ")"), DefaultEmptyDropdown([], "json", "Customer"), @$item['id_customer'], array('class' => 'form-control', 'id' => 'dd_id_customer' . $key)); ?>
        </div>    
    </div>

    <div class="form-group">
        <?= form_label('Unit Detail', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-7">

            <?php
            if (!CheckEmpty(@$item['id_serial_unit'])) {
                echo form_input(array('type' => 'hidden', 'value' => @$item['id_serial_unit'], 'name' => 'id_serial_unit[]', 'id' => 'txt_id_serial' . @$key));
                echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'value' => @$item['no_prospek'] . ' - ' . @$item['nomor_buka_jual'] . ' - ' . @$item['vin_number'] . ' - ' . @$item['engine_no'], 'id' => 'txt_nama_serial' . @$key));
            } else {
                echo form_dropdown(array("name" => "id_serial_unit[]"), DefaultEmptyDropdown(@$list_serial_unit, "json", "Serial Unit"), @$item['id_serial_unit'], array('class' => 'form-control ', 'id' => 'dd_id_serial_unit' . $key,"onchange"=>"GetOneUnit('".@$key."')"));
            }
            ?>


        </div>
        <div class="col-sm-3">
             <?php echo  form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'value' => "0", 'id' => 'txt_saldo_' . @$key));?>
        </div>

    </div>
    <div class="form-group">
        <table class="table table-striped table-bordered table-hover" >
            <thead>
                <tr>
                    <th><button type="button" onClick="AddAreaDetail<?php echo @$key ?>()"  class="btn btn-success btn-xs"  > + </button>Nama Barang</th>
                    <th>Qty</th>
                    <th>Harga</th>
                    <th>Diskon</th>
                    <th>Subtotal</th>
                    <th style="width:50px;"></th>
                </tr>
            </thead>
            <tbody id="tbody<?php echo @$key ?>">
<?php
foreach (@$item['listsubdetail'] as $inc => $subdet) {
    include APPPATH . 'modules/pembelian/views/v_pembelian_content_detail.php';
}
;
?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">

                    </td>
                    <td style="text-align:right;">
                        Total
                    </td>
                    <td>
                        <?= form_input(array('type' => 'text', 'value' => "0", 'class' => 'form-control', 'id' => 'txt_subtotal' . $key, 'placeholder' => 'Subtotal', 'disabled' => "disabled")); ?>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script>
    var countdetail_<?php echo @$key ?> = 1;
    $(document).ready(function () {
<?php if (!CheckEmpty(@$item['id_customer'])) { ?>
            $("#dd_id_customer<?= @$key ?>").select2({data: list_customer, disabled: true}).val('<?= @$item['id_customer'] ?>').trigger('change');
<?php } else  { ?>
            $('#dd_id_customer<?= @$key ?>').select2({
                placeholder: "Pilih Customer",
                allowClear: true,
                ajax: {
                    url: baseurl + 'index.php/customer/search_customer',
                    dataType: 'json',
                    method: 'POST',
                    minimumInputLength: 3,
                    processResult: function (data) {
                        return {
                            results: data.results
                        }
                    }
                }
            });
<?php } ?>

    })

    function RemoveAreaDetail<?php echo @$key ?>(key, inc)
    {
        $("#areadetail" + key + "_" + inc).remove();
        $("#areaketerangan" + key + "_" + inc).remove();
    }
    function AddAreaDetail<?php echo @$key ?>()
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pembelian/contentpembeliandetail',
            data: {
                key:<?php echo @$key ?>,
                inc: countdetail_<?php echo @$key ?>,

            },
            success: function (data) {
                countdetail_<?php echo @$key ?> += 1;
                $("#tbody<?php echo @$key ?>").append(data);

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
</script>