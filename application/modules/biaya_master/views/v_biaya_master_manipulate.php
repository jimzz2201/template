<div class="modal-header">
    Biaya Master <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_biaya_master" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_biaya_master" value="<?php echo @$id_biaya_master; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode ', "txt_kode_biaya_master", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?php
                $mergearray = array();
                if (!CheckEmpty(@$id_biaya_master)) {
                    $mergearray['disabled'] = "disabled";
                } else {
                    $mergearray['name'] = "kode_biaya_master";
                }
                ?>
                <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_biaya_master, 'class' => 'form-control', 'id' => 'txt_kode_biaya_master', 'placeholder' => 'Kode Biaya Master'))); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama', "txt_nama_biaya_master", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?= form_input(array('type' => 'text', 'name' => 'nama_biaya_master', 'value' => @$nama_biaya_master, 'class' => 'form-control', 'id' => 'txt_nama_biaya_master', 'placeholder' => 'Nama Biaya Master')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Group', "txt_id_group", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array('type' => 'text', 'name' => 'id_biaya_group', 'class' => 'form-control', 'id' => 'txt_id_biaya_group', 'placeholder' => 'Group'), DefaultEmptyDropdown($list_group, "json", "Group Akses"), @$id_biaya_group); ?>
            </div>
            <?= form_label('Jenis Biaya', "txt_id_group", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array('type' => 'text', 'name' => 'jenis', 'class' => 'form-control', 'id' => 'dd_jenis', 'placeholder' => 'Jenis'), $list_jenis, @$jenis); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('GL Debet', "txt_id_group", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array( 'name' => 'id_gl_debet[]'), $list_gl, @$id_gl_debet, array('class' => 'form-control', 'id' => 'dd_id_gl_debet', 'multiple' => 'multiple')); ?>
            </div>
            <?= form_label('GL Kredit', "txt_id_group", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array('type' => 'text', 'name' => 'id_gl_kredit', 'class' => 'form-control', 'id' => 'dd_id_gl_kredit', 'placeholder' => 'Kredit'), DefaultEmptyDropdown($list_gl, "json", "GL Kredit"), @$id_gl_kredit); ?>
            </div>

        </div>
        <div class="form-group">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $(document).ready(function () {
        $("select").select2();
        $("#dd_id_gl_debet").select2().val(<?=json_encode(@$id_gl_debet)?>).trigger("change");

    })
    $("#frm_biaya_master").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/biaya_master/biaya_master_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>