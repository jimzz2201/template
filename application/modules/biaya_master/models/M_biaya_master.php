<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_biaya_master extends CI_Model {

    public $table = '#_biaya_master';
    public $id = 'id_biaya_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatabiaya_master() {
        $this->load->library('datatables');
        $this->datatables->select('id_biaya_master,kode_biaya_master,nama_biaya_master,#_biaya_master.status,nama_biaya_group,#_biaya_master.is_delete,gldebet.nama_gl_account as nama_debet,glkredit.nama_gl_account as nama_kredit');
        $this->db->join("#_biaya_group", "#_biaya_group.id_biaya_group=#_biaya_master.id_biaya_group", "left");
        $this->db->join("#_gl_account gldebet", "gldebet.id_gl_account=#_biaya_master.id_gl_debet", "left");
        $this->db->join("#_gl_account glkredit", "glkredit.id_gl_account=#_biaya_master.id_gl_kredit", "left");
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'lian_biaya_master.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $stredit = '';
        if ($isedit) {
            $stredit .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editbiaya_master($1);return false;"));
        }
        $strdelete = '';
        if ($isdelete) {
            $strdelete .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletebiaya_master($1);return false;"));
        }
        $this->datatables->add_column('stredit', $stredit, 'id_biaya_master');
        $this->datatables->add_column('strdelete', $strdelete, 'id_biaya_master');
        return $this->datatables->generate();
    }

    // get all
    function GetOneBiaya_master($keyword, $type = 'id_biaya_master',$isfull=true) {
        $this->db->where($type, $keyword);
        if($isfull)
        {
            $this->db->join("#_biaya_group","#_biaya_group.id_biaya_group=#_biaya_master.id_biaya_group","left");
            $this->db->select("#_biaya_master.*,nama_biaya_group");
        }
        $this->db->where($this->table . '.deleted_date', null);
        $biaya_master = $this->db->get($this->table)->row();
        return $biaya_master;
    }

    function Biaya_masterManipulate($model) {
        try {
            $model['status'] = DefaultCurrencyDatabase($model['status']);
            $model['id_gl_kredit'] = ForeignKeyFromDb(@$model['id_gl_kredit']);
            if (CheckEmpty($model['id_biaya_master'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Biaya_master successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_biaya_master" => $model['id_biaya_master']));
                return array("st" => true, "msg" => "Biaya_master has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownBiaya_master($model = []) {
        if (count($model) > 0) {

            if (!CheckEmpty(@$model['id_jenis_kas'])) {
                $this->db->group_start();
                $this->db->where(array("jenis" => 0));
                $this->db->or_where(array("jenis" => @$model['id_jenis_kas']));
                $this->db->group_end();
            }
            if(!CheckEmpty(@$model['id_jenis_transaksi']))
            {
                $this->db->group_start();
                $this->db->where(array("FIND_IN_SET('".@$model['id_jenis_transaksi']."',id_gl_debet)>" => 0));
                $this->db->or_where(array("id_gl_kredit" => @$model['id_jenis_transaksi']));
                $this->db->group_end();
            }
            if(!CheckEmpty(@$model['id_biaya_group']))
            {
                 $this->db->where(array("id_biaya_group" => @$model['id_biaya_group']));
            }
            
            
        }
        $listbiaya_master = GetTableData($this->table, 'id_biaya_master', 'nama_biaya_master', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));
     
        return $listbiaya_master;
    }

    function GetDropDownBiaya_group() {
        $listbiaya_group = GetTableData("#_biaya_group", 'id_biaya_group', 'nama_biaya_group', array('#_biaya_group.status' => 1, '#_biaya_group.deleted_date' => null));

        return $listbiaya_group;
    }

    function Biaya_masterDelete($id_biaya_master) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_biaya_master' => $id_biaya_master));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Biaya_master has been deleted from database");
    }

}
