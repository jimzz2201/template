<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Biaya_master extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_biaya_master');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K084";
        $header = "K001";
        $title = "Biaya Master";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        CekModule($module);
        $css = [];
        LoadTemplate($model, "biaya_master/v_biaya_master_index", $javascript, $css);
    }

    public function get_one_biaya_master() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_biaya_master', 'Biaya Master', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_biaya_master->GetOneBiaya_master($model["id_biaya_master"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Biaya Master tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdatabiaya_master() {
        header('Content-Type: application/json');
        echo $this->m_biaya_master->GetDatabiaya_master();
    }

    public function create_biaya_master() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K084";
        $header = "K001";
        $this->load->model("gl/m_gl");
        $row['title'] = "Add Biaya Master";
        CekModule($module);
        $row['list_group'] = $this->m_biaya_master->GetDropDownBiaya_group();
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_jenis'] = array(array("id" => 0, "text" => "Pendapatan dan Pengeluaran"), array("id" => 1, "text" => "Pendapatan"), array("id" => 2, "text" => "Pengeluaran"));
        $row['list_gl'] = $this->m_gl->GetDropDownGl(0);

        $this->load->view('biaya_master/v_biaya_master_manipulate', $row);
    }

    public function biaya_master_manipulate() {
        $message = '';
        $model = $this->input->post();

        if (CheckEmpty(@$model['id_biaya_master'])) {
            $this->form_validation->set_rules('kode_biaya_master', 'Kode Biaya Master', 'trim|required');
        }
        $model['id_gl_debet'] = implode(',', @$model['id_gl_debet']);
        if (CheckEmpty(@$model['id_gl_debet'])) {
            $this->form_validation->set_rules('id_gl_debet', 'Gl Debet', 'trim|required');
        }
        $this->form_validation->set_rules('nama_biaya_master', 'Nama Biaya Master', 'trim|required');

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_biaya_master->Biaya_masterManipulate($model);
            echo json_encode($status);
        }
    }

    public function getdropdownbiaya() {
        $model = $this->input->post();
        $listbiaya = $this->m_biaya_master->GetDropDownBiaya_master($model);
        echo json_encode(DefaultEmptyDropdown($listbiaya, "json", "Biaya"));
    }

    public function edit_biaya_master($id = 0) {
        $row = $this->m_biaya_master->GetOneBiaya_master($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K084";
            $header = "K001";
            $row['title'] = "Edit Biaya Master";
            $row['list_group'] = $this->m_biaya_master->GetDropDownBiaya_group();
            $this->load->model("gl/m_gl");
            $row['list_jenis'] = array(array("id" => 0, "text" => "Pendapatan dan Pengeluaran"), array("id" => 1, "text" => "Pendapatan"), array("id" => 2, "text" => "Pengeluaran"));
            $row['list_gl'] = $this->m_gl->GetDropDownGl(0);
            $row['id_gl_debet'] = explode(',', $row['id_gl_debet']);
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->view('biaya_master/v_biaya_master_manipulate', $row);
        } else {
            SetMessageSession(0, "Biaya_master cannot be found in database");
            redirect(site_url('biaya_master'));
        }
    }

    public function biaya_master_delete() {
        $message = '';
        $this->form_validation->set_rules('id_biaya_master', 'Biaya_master', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_biaya_master->Biaya_masterDelete($model['id_biaya_master']);
        }

        echo json_encode($result);
    }

}

/* End of file Biaya_master.php */