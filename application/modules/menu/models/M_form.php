<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_form extends CI_Model
{

    public $table = '#_form';
    public $id = 'id_form';
    public $order = 'ASC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    function getAll(){
        $this->db->select("id_form AS id, form_name AS label, form_url AS link, IFNULL(kode_parent,0) AS parent, urut AS sort, form_icon AS icon, urlclass AS class");
        $this->db->from($this->table);
        $this->db->order_by('kode_parent', 'ASC');
        $this->db->order_by('urut', 'ASC');
        return $this->db->get()->result();
    }

}