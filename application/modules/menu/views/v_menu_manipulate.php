<div class="modal-header">
    Kota <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_kota" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_kota" value="<?php echo @$id_kota; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Kota', "txt_kode_kota", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                
                <?php $mergearray=array();
                if(!CheckEmpty(@$id_kota))
                {
                    $mergearray['disabled']="disabled";
                }
                else
                {
                    $mergearray['name'] = "kode_kota";
                }?>
                <?= form_input(array_merge($mergearray,array('type' => 'text', 'value' => @$kode_kota, 'class' => 'form-control', 'id' => 'txt_kode_kota', 'placeholder' => 'Kode Kota'))); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Kota', "txt_nama_kota", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'nama_kota', 'value' => @$nama_kota, 'class' => 'form-control', 'id' => 'txt_nama_kota', 'placeholder' => 'Nama Kota')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Wilayah', "txt_wilayah", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$wilayah, "name" => "wilayah"), array('Barat' => 'Barat', 'Timur' => 'Timur'), @$wilayah, array('class' => 'form-control', 'id' => 'wilayah')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $("#frm_kota").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/kota/kota_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>