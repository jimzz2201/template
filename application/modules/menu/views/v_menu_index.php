
<section class="content-header">
    <h1>
        Menu
        <small>Manager</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Menu Manager</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">
                <!--                --><?php //echo anchor("", 'Create Menu', 'class="btn btn-success" id="btt_create"');  ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" >
                <div class="cf nestable-lists">
                    <div class="dd" id="nestable">
                        <?= $menu ?>
                    </div>
                </div>
                <input type="hidden" id="nestable-output">
            </div>
        </div>
        <div  style="padding:20px;">
            <button type="button" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>
    </div>

</section>
<script>
    var dbmenu = [];
    var listmenu = [];

    $(document).ready(function () {


<?php
$indexheader = 0;
$indexdetail = 0;
foreach (@$list as $menusatuan) {
    echo "var objdetail={};objdetail.headerid=" . $menusatuan['id'] . ";";
    echo "var listdetail=[];";
    echo "dbmenu[" . $menusatuan['id'] . "]='" . $menusatuan['label'] . "';";
    $indexdetail = 0;
    foreach ($menusatuan['child'] as $childsatuan) {
        echo "listdetail[" . $indexdetail . "]='" . $childsatuan['id'] . "';";
        echo "dbmenu[" . $childsatuan['id'] . "]='" . $childsatuan['label'] . "';";
        $indexdetail++;
    }
    echo "objdetail.listdetail=listdetail;";
    echo "listmenu[" . $indexheader . "]=objdetail;";
    $indexheader++;
}
?>
        $("#btt_modal_ok").click(function () {

            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/menu/menu_manipulate',
                dataType: 'json',
                data: {
                    listmenu:listmenu
                },
                success: function (data) {
                    if (data.st)
                    {
                        messagesuccess(data.msg);
                    } else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
            return false;

        })
        $('#nestable').nestable({
            group: 1,
            scroll: true,
            callback: function (l, e, p) {
                SortingDiv();
            },
            beforeDragStop: function (l, e, p) {

            }
        });
    });

    function SortingDiv() {
        var hasil = $('#nestable').nestable('toArray');
        var currentheader = 0;
        var indexitem = -1;
        var tempreturn = [];
        var indexchild = [];
        var objdetail = {};
        hasil.forEach(function (item, index) {
            console.log(dbmenu[item.id]);
            if (item.parent_id == undefined)
            {
                objdetail = {};
                objdetail.headerid = item.id;
                objdetail.headername = item.id;
                objdetail.listdetail = [];
                currentheader = item.id;
                indexitem++;
                indexchild = 0;
            } else
            {
                objdetail.listdetail[indexchild] = item.id;
                tempreturn[indexitem] = objdetail;
                indexchild++;
            }
        });
        listmenu = tempreturn;
    }

</script>