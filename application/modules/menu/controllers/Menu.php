<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_form');
    }

    public function index()
    {
        $data = array(
            "openmenu"=>"menulocation"
        );
        $javascript = array(
            "assets/js/jquery.nestable.js"
        );
        $css = array(
            "assets/css/jquery.nestable.css"
        );
        $listmenu=$this->get_data_menu();
        
        $data['list']=$listmenu;
        $data['menu'] = $this->get_menu($listmenu);
        LoadTemplate($data, "v_menu_index", $javascript, $css);
    }
    
    function menu_manipulate(){
        $model=$this->input->post();
         
        foreach($model['listmenu'] as $key=>$menusatuan)
        {
            $this->db->from("#_form");
            $this->db->where(array("id_form"=>$menusatuan['headerid']));
            $row=$this->db->get()->row();
            if($row)
            {
                $row->urut=($key+1)*100;
                $this->db->update("#_form",$row,array("id_form"=>$menusatuan['headerid']));
                foreach($menusatuan['listdetail'] as $keydetail=>$detail)
                {
                    $this->db->from("#_form");
                    $this->db->where(array("id_form"=>$detail));
                    $rowdetail=$this->db->get()->row();
                    if($rowdetail)
                    {
                        $rowdetail->urut=($keydetail+1)*10;
                        $this->db->update("#_form",$rowdetail,array("id_form"=>$detail));
                    }
                }
                
            }
            
            
        }
        echo json_encode(array("st"=>true,"msg"=>"Data Berhasil Dirubah"));
    }

    function get_data_menu(){
        $ref   = [];
        $items = [];
        $form = $this->m_form->getAll();
        foreach ($form as $data){
            $thisRef = &$ref[$data->id];

            $thisRef['parent'] = $data->parent;
            $thisRef['label'] = $data->label;
            $thisRef['link'] = $data->link;
            $thisRef['icon'] = $data->icon;
            $thisRef['class'] = $data->class;
            $thisRef['id'] = $data->id;

            if($data->parent == 0) {
                $items[$data->id] = &$thisRef;
            } else {
                $ref[$data->parent]['child'][$data->id] = &$thisRef;
            }
        }

        return $items;
    }

    function get_menu($items,$class = 'dd-list') {

        $html = "<ol class=\"".$class."\" id=\"menu-id\">";

        foreach($items as $key=>$value) {
//            $tombol = '<a class="edit-button" id="'.$value['id'].'" label="'.$value['label'].'" link="'.$value['link'].'" ><i class="fa fa-pencil"></i></a>
//                            <a class="del-button" id="'.$value['id'].'"><i class="fa fa-trash"></i></a>';


//            $html.= '<li class="dd-item dd3-item" data-id="'.$value['id'].'" >
//                    <div class="dd-handle dd3-handle">&nbsp;</div>
//                    <div class="dd3-content"><span id="label_show'.$value['id'].'">'.$value['label'].'</span>
//                        <span class="span-right">/<span id="link_show'.$value['id'].'">'.$value['link'].'</span> &nbsp;&nbsp;
//                            <a id="icon_show'.$value['id'].'" class="icon_show"><i class="fa '.$value['icon'].'"></i></a> &nbsp;&nbsp;
//                            <a id="class_show'.$value['id'].'" class="class_show">'.$value['class'].'</a> &nbsp;&nbsp;
//                            </span>
//                    </div>';
            $html .= '<li class="dd-item" data-id="'.$value['id'].'" >
                    <div class="dd-handle">'.$value['label'].'</div>';
            if(array_key_exists('child',$value)) {
                $html .= $this->get_menu($value['child'],'child');
            }
            $html .= "</li>";
        }
        $html .= "</ol>";

        return $html;

    }
    
}

/* End of file Menu.php */