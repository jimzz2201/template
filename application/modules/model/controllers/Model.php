<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_model');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K058";
        $header = "K001";
        $title = "Model";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "model/v_model_index", $javascript);
        
    } 
    public function get_one_model() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_model', 'Model', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_model->GetOneModel($model["id_model"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Model tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatamodel() {
        header('Content-Type: application/json');
        echo $this->m_model->GetDatamodel();
    }
    
    public function create_model() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	    $module = "K058";
        $header = "K001";
        $row['title'] = "Add Model";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	    $this->load->view('model/v_model_manipulate', $row);       
    }
    
    public function model_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_model', 'Kode Model', 'trim|required');
	$this->form_validation->set_rules('nama_model', 'Nama Model', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_model->ModelManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_model($id=0) 
    {
        $row = $this->m_model->GetOneModel($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K058";
            $header = "K001";
            $row['title'] = "Edit Model";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    $this->load->view('model/v_model_manipulate', $row);
        } else {
            SetMessageSession(0, "Model cannot be found in database");
            redirect(site_url('model'));
        }
    }
    
    public function model_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_model', 'Model', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_model->ModelDelete($model['id_model']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Model.php */