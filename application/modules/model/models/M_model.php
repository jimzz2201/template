<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_model extends CI_Model
{

    public $table = '#_model';
    public $id = 'id_model';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatamodel() {
        $this->load->library('datatables');
        $this->datatables->select('id_model,kode_model,nama_model,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_model.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editmodel($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletemodel($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_model');
        return $this->datatables->generate();
    }

    // get all
    function GetOneModel($keyword, $type = 'id_model') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $model = $this->db->get($this->table)->row();
        return $model;
    }

    function ModelManipulate($model) {
        try {
                $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_model'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		return array("st" => true, "msg" => "Model successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_model" => $model['id_model']));
		return array("st" => true, "msg" => "Model has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownModel() {
        $listmodel = GetTableData($this->table, 'id_model', 'nama_model', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listmodel;
    }

    function ModelDelete($id_model) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_model' => $id_model));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Model has been deleted from database");
    }


}