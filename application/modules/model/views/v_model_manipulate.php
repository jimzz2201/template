<div class="modal-header">
        Model <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_model" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_model" value="<?php echo @$id_model; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Model', "txt_kode_model", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?php $mergearray=array();
                            if(!CheckEmpty(@$id_model))
                            {
                                $mergearray['disabled']="disabled";
                            }
                            else
                            {
                                $mergearray['name'] = "kode_model";
                            }?>
                    <?= form_input(array_merge($mergearray,array('type' => 'text', 'value' => @$kode_model, 'class' => 'form-control', 'id' => 'txt_kode_model', 'placeholder' => 'Kode Model'))); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Model', "txt_nama_model", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_model', 'value' => @$nama_model, 'class' => 'form-control', 'id' => 'txt_nama_model', 'placeholder' => 'Nama Model')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_model").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/model/model_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>