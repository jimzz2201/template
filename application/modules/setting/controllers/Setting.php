<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_setting');
    }

    public function index() {
        $row = $this->m_setting->GetOneSetting();
        $row->button = 'Add';
        $row = json_decode(json_encode($row), true);
        $module = "K121";
        $header = "K006";
        CekModule($module);
        $row['title']= "Global Setting";
        $row['form']= $header;
        $row['formsubmenu']= $module;
    
        $javascript = array();
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $javascript[] = "assets/plugins/select2/select2.js";
        $css = [];
        $css[] = "assets/plugins/select2/select2.css";
        $row['openmenu'] = "menusetting";
        $row['list_agen'] = [];
        LoadTemplate($row, 'setting/v_setting_manipulate', $javascript, $css);
    }

    public function report() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K112";
        $header = "K005";
        $title = "DO KPU";
        CekModule($module);
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("customer/m_customer");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['status'] = 1;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "v_report_index", $javascript, $css);
    }

    public function getdatareport() {
        header('Content-Type: application/json');
        echo $this->m_setting->GetDataSettingReport();
    }

    public function update_report($id_report) {
        $data = array(
            "openmenu" => "menulocation"
        );
        $javascript = array(
            "assets/js/jquery.nestable.js"
        );
        $css = array(
            "assets/css/jquery.nestable.css"
        );

        $data['reportlist'] = $this->m_setting->GetDataJoinReport($id_report);
        LoadTemplate($data, "v_report_manipulate", $javascript, $css);
    }
    
    public function setting_manipulate() {
        $message = '';

        $model = $this->input->post();
        
        if ($message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . $message));
        } else {
            $status = $this->m_setting->SettingManipulate($model);
            echo json_encode($status);
        }
    }

}

/* End of file Menu.php */