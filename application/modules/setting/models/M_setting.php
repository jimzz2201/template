<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_setting extends CI_Model
{

    public $table = '#_setting';
    public $id = 'id_setting';
    public $order = 'ASC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    function GetDataSettingReport() {
        $this->load->library('datatables');
        $this->datatables->select('#_report.*,id_report as id');
        $this->datatables->from("#_report");
        //add this line for join
        //$this->datatables->join('table2', '#_group.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('setting/update_report/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        
        $this->datatables->add_column('action', $straction, 'id');
        return $this->datatables->generate();
    }
    
    function GetDataJoinReport($id_report)
    {
        $this->db->from("#_report_join");
        $this->db->where(array("id_report"=>$id_report));
        $this->db->order_by("urut_join");
        $this->db->select("id_report_join as id,from_table,alias,id_group");
        $result=$this->db->get()->result();
        return $result;
        
    }
    
    
    function GetOneSetting() {

        $setting = $this->db->get($this->table)->row();
        return $setting;
    }

    function SettingManipulate($model) {
        try {
            $update['email_do'] = $model['email_do'];
            $this->db->update($this->table, $update, array("id_setting" => $model['id_setting']));
            SetMessageSession(1, 'Setting has been updated');
            return array("st" => true, "msg" => "Setting has been updated");
          
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    


}