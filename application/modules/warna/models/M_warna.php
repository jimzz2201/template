<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_warna extends CI_Model
{

    public $table = '#_warna';
    public $id = 'id_warna';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatawarna() {
        $this->load->library('datatables');
        $this->datatables->select('id_warna,kode_warna,nama_warna,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_warna.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editwarna($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletewarna($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_warna');
        return $this->datatables->generate();
    }

    // get all
    function GetOneWarna($keyword, $type = 'id_warna') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $warna = $this->db->get($this->table)->row();
        return $warna;
    }

    function WarnaManipulate($model) {
        try {
                $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_warna'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		return array("st" => true, "msg" => "Warna successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_warna" => $model['id_warna']));
		return array("st" => true, "msg" => "Warna has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownWarna() {
        $listwarna = GetTableData($this->table, 'id_warna', 'nama_warna', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listwarna;
    }

    function WarnaDelete($id_warna) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_warna' => $id_warna));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Warna has been deleted from database");
    }


}