<div class="modal-header">
        Warna <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_warna" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_warna" value="<?php echo @$id_warna; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Warna', "txt_kode_warna", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?php $mergearray=array();
                            if(!CheckEmpty(@$id_warna))
                            {
                                $mergearray['readonly']="readonly";
                                $mergearray['name'] = "kode_warna";
                            }
                            else
                            {
                                $mergearray['name'] = "kode_warna";
                            }?>
                    <?= form_input(array_merge($mergearray,array('type' => 'text', 'value' => @$kode_warna, 'class' => 'form-control', 'id' => 'txt_kode_warna', 'placeholder' => 'Kode Warna'))); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Warna', "txt_nama_warna", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_warna', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Nama Warna')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_warna").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/warna/warna_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>