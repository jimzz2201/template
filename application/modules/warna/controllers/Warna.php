<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Warna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_warna');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K047";
        $header = "K001";
        $title = "Warna";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "warna/v_warna_index", $javascript);
        
    } 
    public function get_one_warna() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_warna', 'Warna', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_warna->GetOneWarna($model["id_warna"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Warna tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatawarna() {
        header('Content-Type: application/json');
        echo $this->m_warna->GetDatawarna();
    }
    
    public function create_warna() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
        $module = "K047";
        $header = "K001";
        $row['title'] = "Add Warna";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	    $this->load->view('warna/v_warna_manipulate', $row);       
    }
    
    public function warna_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_warna', 'Kode Warna', 'trim|required');
	$this->form_validation->set_rules('nama_warna', 'Nama Warna', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_warna->WarnaManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_warna($id=0) 
    {
        $row = $this->m_warna->GetOneWarna($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K047";
            $header = "K001";
            $row['title'] = "Edit Warna";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    $this->load->view('warna/v_warna_manipulate', $row);
        } else {
            SetMessageSession(0, "Warna cannot be found in database");
            redirect(site_url('warna'));
        }
    }
    
    public function warna_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_warna', 'Warna', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_warna->WarnaDelete($model['id_warna']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Warna.php */