<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hutang extends CI_Controller {

    private $_sufix = "_hutang";

    function __construct() {
        parent::__construct();
        $this->load->model("pembayaran/m_pembayaran_hutang");
    }

    public function getdatapembayaranhutang() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_sufix, 1);
            }
            SetCookieSetting($key . $this->_sufix, $value);
        }

        echo $this->m_pembayaran_hutang->GetDataPembayaranHutang($params);
    }

    public function generatejurnalpembayaran() {
        $this->db->from("#_pembayaran_utang_master");
        $this->db->where(array("#_pembayaran_utang_master.status !=" => 2));
        $listpembayaran = $this->db->get()->result();
        foreach ($listpembayaran as $pem) {
            $this->m_pembayaran_hutang->GenerateAkunPembayaranHutang($pem->pembayaran_utang_id);
        }
    }

    public function generatepembayaran() {


        $this->db->from("#__import_pembayaran");
        $this->db->group_by("no_pembayaran");
        $this->db->select("no_pembayaran,sum(nominal) as jumlah_bayar");
        $this->db->where(array("keterangan_field" => null));
        $this->load->model("pembayaran/m_pembayaran_hutang");
        $listresult = $this->db->get()->result();
        $message = "";
        try {

            $jenis_pencarian = "created_date";
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            foreach ($listresult as $result) {
                $this->db->from("#__import_pembayaran");
                $this->db->where(array("no_pembayaran" => $result->no_pembayaran));
                $listpembayaran = $this->db->get()->result();

                $jumlahbayar = 0;
                $pembayaranmaster = array();
                $pembayaranmaster['dokumen'] = $result->no_pembayaran;
                $pembayaranmaster['jenis_pembayaran'] = "Cash";
                $pembayaranmaster['biaya'] = 0;
                $pembayaranmaster['potongan'] = 0;
                $pembayaranmaster['status'] = 1;
                $pembayaranmaster['id_bank'] = 1;
                $pembayaranmaster['keterangan'] = "";
                $pembayaranmaster['dokumen'] = $result->no_pembayaran;


                $listdetailpembayaran = [];
                $jumlah_bayartotal = 0;
                foreach ($listpembayaran as $pembayarandetail) {
                    $pembayaranmaster['tanggal_transaksi'] = DefaultTanggalDatabase($pembayarandetail->tanggal_pembayaran);
                    $pembayaranmaster['id_supplier'] = $unit->id_supplier;

                    $jumlahbayar = $pembayarandetail->nominal;
                    $this->db->from("#_view_utang_detail");
                    $this->db->where(array("invoice_number" => $pembayarandetail->faktur));
                    $listunit = $this->db->get()->result();

                    foreach ($listunit as $unit) {
                        if ($jumlahbayar > 0) {
                            $pembayarandetailinsert['id_kpu'] = $unit->id_kpu;
                            $pembayarandetailinsert['created_date'] = GetDateNow();
                            $pembayarandetailinsert['created_by'] = 1;
                            $pembayarandetailinsert['id_unit_serial'] = $unit->id_unit_serial;
                            $pembayarandetailinsert['jumlah_bayar'] = $jumlahbayar > $unit->sisa ? $unit->sisa : $jumlahbayar;
                            $jumlah_bayartotal += $jumlahbayar > $unit->sisa ? $unit->sisa : $jumlahbayar;
                            $jumlahbayar -= $jumlahbayar > $unit->sisa ? $unit->sisa : $jumlahbayar;
                            $listdetailpembayaran[] = $pembayarandetailinsert;
                        }
                    }
                    if (count($listdetailpembayaran) == 0) {
                        $this->db->update("#__import_pembayaran", array("keterangan_field" => "Data Tidak Ada"), array("no_pembayaran" => $result->no_pembayaran, "faktur" => $pembayarandetail->faktur));
                    }
                }
                if (count($listdetailpembayaran) == 0) {
                    $this->db->update("#__import_pembayaran", array("keterangan_field" => "Data Tidak Ada"), array("no_pembayaran" => $result->no_pembayaran));
                } else {
                    $pembayaranmaster['total_bayar'] = $jumlah_bayartotal;
                    $this->db->insert("#_pembayaran_utang_master", $pembayaranmaster);
                    $pembayaran_utang_id = $this->db->insert_id();

                    foreach ($listdetailpembayaran as $pemdet) {
                        $pemdet['pembayaran_utang_id'] = $pembayaran_utang_id;
                        $this->db->insert("#_pembayaran_utang_detail", $pemdet);
                    }
                    $messagedet = "Sukses";
                    if ($jumlah_bayartotal != $result->jumlah_bayar) {
                        $messagedet .= " Pembayaran tidak cocok";
                    }
                    $this->db->update("#__import_pembayaran", array("keterangan_field" => $messagedet, "jumlah_bayar" => $jumlah_bayartotal), array("no_pembayaran" => $result->no_pembayaran));
                    $this->m_pembayaran_hutang->GenerateAkunPembayaranHutang($pembayaran_utang_id);
                }
            }
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                echo $message;
            } else {
                $this->db->trans_commit();
                echo "Sukses";
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }





        dumperror($listresult);
    }

    public function generatepembayaraninvoice() {


        $this->db->from("#_view_dokumen_hutang_sisa");
        $this->db->select("id_pembelian,nomor_invoice,sisa");
        $this->db->where(array("nomor_invoice !=" => null, "nomor_invoice !=" => ""));
        $listresult = $this->db->get()->result();
        $this->load->model("invoice/m_invoice");
        foreach ($listresult as $res) {
            $this->db->from("#__import_invoice");
            $this->db->where(array("invoicef_no" => $res->nomor_invoice));
            $this->db->select("sum(hutang) as hutang");
            $rowhutang = $this->db->get()->row();

            if ($rowhutang) {
                $pembelian = $this->m_invoice->GetOnePembelian($res->id_pembelian);

                if ($rowhutang->hutang == 0 && $pembelian->tanggal_pelunasan == null) {
                    $jumlahbayar = 0;
                    $pembayaranmaster = array();
                    $pembayaranmaster['dokumen'] = "IMP-" . $pembelian->nomor_invoice;
                    $pembayaranmaster['jenis_pembayaran'] = "Cash";
                    $pembayaranmaster['biaya'] = 0;
                    $pembayaranmaster['potongan'] = 0;
                    $pembayaranmaster['status'] = 1;
                    $pembayaranmaster['id_bank'] = 1;
                    $pembayaranmaster['keterangan'] = "";
                    


                    $listdetailpembayaran = [];
                    $jumlah_bayartotal = 0;

                    foreach ($pembelian->listdetail as $pembayarandetail) {
                        $pembayaranmaster['tanggal_transaksi'] = "2020-12-31";
                        $pembayaranmaster['id_supplier'] = $pembelian->id_supplier;

                        $jumlahbayar = $pembayarandetail->hpp_unit;
                        $pembayarandetailinsert['id_kpu'] = $pembayarandetail->id_kpu;
                        $pembayarandetailinsert['id_pembelian'] = $res->id_pembelian;
                        $pembayarandetailinsert['created_date'] = GetDateNow();
                        $pembayarandetailinsert['created_by'] = 1;
                        $pembayarandetailinsert['id_unit_serial'] = $pembayarandetail->id_unit_serial;
                        $pembayarandetailinsert['jumlah_bayar'] = $jumlahbayar;
                        $jumlah_bayartotal += $jumlahbayar;
                        $listdetailpembayaran[] = $pembayarandetailinsert;
                    }
                    if (count($listdetailpembayaran) > 0) {
                        $pembayaranmaster['total_bayar'] = $jumlah_bayartotal;
                        $this->db->insert("#_pembayaran_utang_master", $pembayaranmaster);
                        $pembayaran_utang_id = $this->db->insert_id();

                        foreach ($listdetailpembayaran as $pemdet) {
                            $pemdet['pembayaran_utang_id'] = $pembayaran_utang_id;
                            $this->db->insert("#_pembayaran_utang_detail", $pemdet);
                        }
                        if ($jumlah_bayartotal != $result->jumlah_bayar) {
                            $messagedet .= " Pembayaran tidak cocok";
                        }
                        $this->m_pembayaran_hutang->GenerateAkunPembayaranHutang($pembayaran_utang_id);
                    }
                }
            }
        }



        // dumperror($listresult);
        die();
      




        dumperror($listresult);
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K043";
        $header = "K059";
        $title = "Pembayaran Hutang";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $jenis_pencariankeyword = [];
        $jenis_pencariankeyword[] = array("id" => "engine_no", "text" => "No Mesin");
        $jenis_pencariankeyword[] = array("id" => "vin_number", "text" => "No Rangka");
        $jenis_pencariankeyword[] = array("id" => "nomor_master", "text" => "No KPU");
        $jenis_pencariankeyword[] = array("id" => "nomor_buka_jual", "text" => "No Buka Jual");
        $jenis_pencariankeyword[] = array("id" => "dokumen", "text" => "No Pembayaran");
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal", "text" => "Tanggal Pembayaran");
        $model['start_date'] = GetCookieSetting("start_date" . $this->_sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $this->_sufix, GetDateNow());
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_sufix, "nomor_master");
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $this->_sufix, "created_date");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_pencarian_keyword'] = $jenis_pencariankeyword;
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        LoadTemplate($model, "pembayaran/v_pembayaran_hutang_index", $javascript, $css);
    }

    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K043";
        $header = "K059";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $this->load->model("bank/m_bank");
        $this->load->model("cabang/m_cabang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $jenis_pembayaran = array();
        $jenis_pembayaran["Cash"] = "Cash";
        $jenis_pembayaran["Transfer"] = "Transfer";
        $jenis_pembayaran["Cek & Giro"] = "Cek & Giro";
        $jenis_pembayaran["Potongan"] = "Potongan";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->tanggal_transaksi = GetDateNow();
        $row->listbank = $this->m_bank->GetDropDownBank();
        $row->detail = [];
        $row->title = 'Create Pembayaran Hutang';
        $row = json_decode(json_encode($row), true);
        $row['listdetai'] = [];
        $javascript = array();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        LoadTemplate($row, 'pembayaran/v_pembayaran_hutang_manipulate', $javascript, $css);
    }

    public function editutang($id) {
        $row = $this->m_pembayaran_hutang->GetOnePembayaranHutang($id);
        if ($row) {
            $row->hutang = $row->hutang + $row->total_bayar;
            $row->button = 'Update';
            $row->button = 'Add';

            CekModule("K043");
            $this->load->model("supplier/m_supplier");
            $this->load->model("bank/m_bank");
            $this->load->model("cabang/m_cabang");
            $row->form = $header;
            $row->formsubmenu = $module;
            $jenis_pembayaran = array();
            $jenis_pembayaran["Cash"] = "Cash";
            $jenis_pembayaran["Transfer"] = "Transfer";
            $jenis_pembayaran["Cek & Giro"] = "Cek & Giro";
            $jenis_pembayaran["Potongan"] = "Potongan";
            $jenis_pembayaran["DP"] = "DP";
            $row->list_type_pembayaran = $jenis_pembayaran;
            $row->list_supplier = $this->m_supplier->GetDropDownSupplier();
            foreach ($row->detail as $key => $det) {
                $row->total += $det['sisa'];
            }
            $row->listdokumen = $this->m_supplier->GetDocSupplier($row->id_supplier);

            $row->list_cabang = $this->m_cabang->GetDropDownCabang();
            $row->listbank = $this->m_bank->GetDropDownBank();

            $row = json_decode(json_encode($row), true);

            $javascript = array();
            $module = "K043";
            $header = "K059";
            $row['title'] = "Update Pembayaran Piutang";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";

            LoadTemplate($row, 'pembayaran/v_pembayaran_hutang_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Pembayaran tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

    public function batal() {
        $message = '';
        $this->form_validation->set_rules('pembayaran_utang_id', 'Pembayaran', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_pembayaran_hutang->HutangBatal($model['pembayaran_utang_id']);
        }

        echo json_encode($result);
    }

    public function pembayaran_hutang_manipulate() {
        $model = $this->input->post();

        $message = '';

        if (CheckEmpty($model['id_pembelian_pembayaran']) || count($model['id_pembelian_pembayaran']) == 0) {
            $message .= "Detail Pembayaran is required<br/>";
        } else {
            foreach ($model['id_pembelian_pembayaran'] as $key => $id) {
                if (DefaultCurrencyDatabase($model['jumlah_bayar'][$key]) > round(DefaultCurrencyDatabase($model['sisa_pembayaran'][$key]))) {
                    $message .= "Pembayaran lebih besar daripada piutang detail<br/>";
                }
            }
        }

        $this->form_validation->set_rules('id_supplier', 'Supplier', 'trim|required');

        if (!CheckEmpty(@$model['jenis_pembayaran'])) {
            if (@$model['jenis_pembayaran'] != 'Cash') {
                $this->form_validation->set_rules('id_bank', 'Bank', 'trim|required');
            }
        }
        if (!CheckEmpty(@$model['jumlah_bayar'])) {
            if (DefaultCurrencyDatabase(@$model['total']) < 0) {
                $message .= "Total harus lebih besar dari 0";
            }
        }
        if (DefaultCurrencyDatabase(@$model['jumlah_bayar_total']) < 0) {
            $message .= "Jumlah Bayar harus lebih besar dari 0";
        }




        $this->form_validation->set_rules('jenis_pembayaran', 'Jenis Pembayaran', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $model['dataset'] = [];
            foreach (@$model['id_pembelian_pembayaran'] as $key => $detail) {
                $model['dataset'][] = array(
                    "id_pembelian_pembayaran" => $detail,
                    "sisa_pembayaran" => $model['sisa_pembayaran'][$key],
                    "jumlah_bayar" => DefaultCurrencyDatabase($model['jumlah_bayar'][$key])
                );
            }
            $status = $this->m_pembayaran_hutang->PembayaranHutang($model);

            echo json_encode($status);
        }
    }

    public function pembayaran_hutang_view($id) {
        $row = $this->m_pembayaran_hutang->GetOnePembayaranHutang($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K043";
            $header = "K059";
            $row['title'] = "View Pembayaran Hutang";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'pembayaran/v_pembayaran_hutang_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Pembayaran tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

}

/* End of file Barang.php */