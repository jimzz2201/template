<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Piutang extends CI_Controller {

    private $_sufix = "_piutang";
    function __construct() {
        parent::__construct();
        $this->load->model("pembayaran/m_pembayaran_piutang");
    }

    public function getdatapembayaranpiutang() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
         foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_sufix, 1);
            }
            SetCookieSetting($key . $this->_sufix, $value);
        }
        
        echo $this->m_pembayaran_piutang->GetDataPembayaranPiutang($params);
    }

    function GenerateGl() {
        $this->load->model("pembayaran/m_pembayaran_piutang");
        $this->db->from("#_pembayaran_piutang_master");
        $this->db->where("status !=",2);
        $listid = $this->db->get()->result();
        foreach ($listid as $det) {
            $this->m_pembayaran_piutang->GenerateAkunPembayaranPiutang($det->pembayaran_piutang_id);
        }
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K069";
        $header = "K059";
        $title = "Pembayaran Piutang";
        CekModule($module);
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $jenis_pencariankeyword = [];
        $jenis_pencariankeyword[] = array("id" => "engine_no", "text" => "No Mesin");
        $jenis_pencariankeyword[] = array("id" => "vin_number", "text" => "No Rangka");
        $jenis_pencariankeyword[] = array("id" => "no_prospek", "text" => "No SPK");
        $jenis_pencariankeyword[] = array("id" => "nomor_buka_jual", "text" => "No Buka Jual");
        $jenis_pencariankeyword[] = array("id" => "dokumen", "text" => "No Pembayaran");
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal", "text" => "Tanggal Pembayaran");
        $model['start_date'] = GetCookieSetting("start_date" . $this->_sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $this->_sufix, GetDateNow());
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_sufix, "nomor_master");
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $this->_sufix, "created_date");
        $model['list_pencarian'] = $jenis_pencarian;
        $css = array();
       
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_pencarian_keyword'] = $jenis_pencariankeyword;
      
        $css[] = "assets/plugins/select2/select2.css";
        $this->load->model("customer/m_customer");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        LoadTemplate($model, "pembayaran/v_pembayaran_piutang_index", $javascript, $css);
    }

    public function create() {
        $row = (object) array();
        $row->button = 'Add';
        $module = "K069";
        $header = "K059";
        CekModule($module);
        $this->load->model("customer/m_customer");
        $this->load->model("bank/m_bank");
        $this->load->model("cabang/m_cabang");
        $row->form = $header;
        $row->formsubmenu = $module;
        $jenis_pembayaran = array();
        $jenis_pembayaran["Cash"] = "Cash";
        $jenis_pembayaran["Transfer"] = "Transfer";
        $jenis_pembayaran["Cek & Giro"] = "Cek & Giro";
        $jenis_pembayaran["Potongan"] = "Potongan";
        $jenis_pembayaran["DP"] = "DP";
        $row->list_type_pembayaran = $jenis_pembayaran;
        $row->listcustomer = $this->m_customer->GetDropDownCustomer();
        $row->list_cabang = $this->m_cabang->GetDropDownCabang();
        $row->tanggal_transaksi = GetDateNow();
        $row->listbank = $this->m_bank->GetDropDownBank();
        $row->detail = [];
      
        $row->title = 'Create Pembayaran Piutang';
        $row = json_decode(json_encode($row), true);
        $javascript = array();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        LoadTemplate($row, 'pembayaran/v_pembayaran_piutang_manipulate', $javascript, $css);
    }

    public function pembayaran_piutang_manipulate() {
        $model = $this->input->post();
       
        $message = '';
        $this->load->model("prospek/m_prospek");
        if (CheckEmpty($model['id_unit_serial_pembayaran']) || count($model['id_unit_serial_pembayaran']) == 0) {
            $message .= "Detail Pembayaran is required<br/>";
        } else {
            foreach ($model['id_unit_serial_pembayaran'] as $key => $id) {
                if (DefaultCurrencyDatabase($model['jumlah_bayar'][$key]) > round(DefaultCurrencyDatabase($model['sisa_pembayaran'][$key]))) {
                    $message .= "Pembayaran lebih besar daripada piutang detail<br/>";
                }
            }
        }

        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        if (CheckEmpty(@$model['potongan'])) {
            $this->form_validation->set_rules('jumlah_bayar_total', 'Jumlah Bayar', 'trim|required');
        }
        if (!CheckEmpty(@$model['jumlah_bayar'])) {
            if (DefaultCurrencyDatabase(@$model['total']) < 0) {
                $message .= "Total harus lebih besar dari 0";
            }
        }
        if (DefaultCurrencyDatabase(@$model['jumlah_bayar_total']) < 0) {
            $message .= "Jumlah Bayar harus lebih besar dari 0";
        }

        $this->form_validation->set_rules('jenis_pembayaran', 'Jenis Pembayaran', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            
              foreach (@$model['id_unit_serial_pembayaran'] as $key => $detail) {
                $model['dataset'][] = array(
                    "id_unit_serial_pembayaran" => $detail,
                    "sisa_pembayaran" => $model['sisa_pembayaran'][$key],
                    "jumlah_bayar" => DefaultCurrencyDatabase($model['jumlah_bayar'][$key]),
                    "pembayaran_piutang_detail_id"=>$model['pembayaran_piutang_detail_id'][$key],
                );
            }
             $status = $this->m_pembayaran_piutang->PembayaranPiutang($model);

            echo json_encode($status);
        }
    }

    public function pembayaran_piutang_view($id) {
        $row = $this->m_pembayaran_piutang->GetOnePembayaranPiutang($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K069";
            $header = "K059";
            $row['title'] = "View Pembayaran Piutang";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            // echo "<pre>";print_r($row);exit;
            LoadTemplate($row, 'pembayaran/v_pembayaran_piutang_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Pembayaran tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

    public function editpiutang($id) {
        $row = $this->m_pembayaran_piutang->GetOnePembayaranPiutang($id);
        if ($row) {
            $row->piutang=$row->piutang+$row->total_bayar;
            $row->button = 'Update';
            $row->button = 'Add';
            $module = "K069";
            $header = "K059";
            CekModule($module);
            $this->load->model("customer/m_customer");
            $this->load->model("bank/m_bank");
            $this->load->model("cabang/m_cabang");
            $row->form = $header;
            $row->formsubmenu = $module;
            $jenis_pembayaran = array();
            $jenis_pembayaran["Cash"] = "Cash";
            $jenis_pembayaran["Transfer"] = "Transfer";
            $jenis_pembayaran["Cek & Giro"] = "Cek & Giro";
            $jenis_pembayaran["Potongan"] = "Potongan";
            $jenis_pembayaran["DP"] = "DP";
            $row->list_type_pembayaran = $jenis_pembayaran;
            
            $row->listcustomer = $this->m_customer->GetDropDownCustomer(null,$row->id_customer);
            foreach($row->detail as $key=>$det)
            {
                $row->detail[$key]->sisa+=$det->jumlah_bayar;
                $row->detail[$key]->terbayar-=$det->jumlah_bayar;
            }
            $row->list_cabang = $this->m_cabang->GetDropDownCabang();
            $row->listbank = $this->m_bank->GetDropDownBank();
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K069";
            $header = "K059";
            $row['title'] = "Update Pembayaran Piutang";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";

            LoadTemplate($row, 'pembayaran/v_pembayaran_piutang_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Pembayaran tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

    public function batal() {
        $message = '';
        $this->form_validation->set_rules('pembayaran_piutang_id', 'Pembayaran', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_pembayaran_piutang->PiutangBatal($model['pembayaran_piutang_id']);
        }

        echo json_encode($result);
    }

}

/* End of file Barang.php */