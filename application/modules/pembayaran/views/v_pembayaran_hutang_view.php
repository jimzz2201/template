<style>
    .remodal{max-width:1024px !Important;}
</style>
<section class="content-header">
    <h1>
        Pembayaran  Hutang View
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">Pembayaran  Hutang View</li>
    </ol>


</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <div class="form-group" >

                            </div>
                            <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="withdocument" value="yes" />
                                <h1 style="font-weight: bold;"><?php
                                    echo "<span>No Pembayaran : </span><span style=color:red;>" . @$dokumen . "</span>";
                                    ?></h1>
                                <hr/>
                                <div id="notification"></div>
                               
                                <div class="form-group">

                                    <?= form_label('Type Pembayaran', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'jenis_pembayaran', 'value' => @$jenis_pembayaran, 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Jenis Pembayaran')); ?>
                                    </div>
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal_transaksi), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => @$nama_supplier, 'class' => 'form-control', 'id' => 'txt_hutang', 'placeholder' => 'Supplier')); ?>
                                    </div>    
                                    <?= form_label('Hutang', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$hutang), 'class' => 'form-control', 'id' => 'txt_hutang', 'placeholder' => 'Hutang')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?php
                                    $listcabang = DefaultEmptyDropdown(@$list_cabang, "json", "Cabang");
                                    if (!CheckEmpty(@$id_cabang)) {
                                        $textcabang = $listcabang[@$id_cabang];
                                        $listcabang = [];
                                        $listcabang[@$id_cabang] = $textcabang;
                                    }
                                    ?>
                                    
                                    <?php
                                    $listbank = DefaultEmptyDropdown(@$listbank, "json", "Bank");
                                    ?>
                                    <?= form_label('Nama Bank', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => @$nama_bank, 'class' => 'form-control', 'id' => 'txt_hutang', 'placeholder' => 'Nama Bank')); ?>
                                    </div>
                                </div>
                                <hr/>

                                <div class="form-group">


                                    <?= form_label('Nomor Cek', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => @$no_jenis_pembayaran, 'class' => 'form-control', 'id' => 'txt_no_jenis_pembayaran', 'name' => 'no_jenis_pembayaran', 'placeholder' => 'No Cek')); ?>
                                    </div>
                                    <?= form_label('Jatuh&nbsp;Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => DefaultDatePicker(@$jatuh_tempo), 'class' => 'form-control datepicker', 'id' => 'txt_jatuh_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                    </div>
                                </div>
                                <div class="form-group">


                                    <?= form_label('Debit Card', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => @$debit_card, 'class' => 'form-control', 'id' => 'txt_debit_card', 'name' => 'debit_card', 'placeholder' => 'Debit Card')); ?>
                                    </div>
                                    <?= form_label('Biaya Lain', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "name" => "biaya", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$biaya_lain), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_biaya_lain', 'placeholder' => 'Biaya Lain')); ?>
                                    </div>
                                </div>
                                <div class="form-group">


                                    <?= form_label('Credit Card', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => @$credit_card, 'class' => 'form-control', 'id' => 'txt_credit_card', 'name' => 'credit_card', 'placeholder' => 'Kredit Card')); ?>
                                    </div>
                                    <?= form_label('Potongan', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "name" => "potongan", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$potongan), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_potongan', 'placeholder' => 'Potongan')); ?>
                                    </div>
                                </div>

                                <hr/>
                                <div class="box box-default">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body form">
                                                <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>Dokumen</th>
                                                            <th>Unit</th>
                                                            <th>GrandTotal</th>
                                                            <th>Terbayar</th>
                                                            <th>Sisa</th>
                                                           
                                                        </tr>
                                                    </thead>
                                                    <tbody id="bodytable">
                                                        <?php 
                                                        foreach (@$detail as $detailsatuan) {
                                                            echo "<tr>";
                                                            echo "<td style='padding:10px;'>" . $detailsatuan['no_prospek'].' - '.$detailsatuan['nomor_kpu']." - ".$detailsatuan['invoice_number'] . "</td>";
                                                            echo "<td style='padding:10px;'>" . $detailsatuan['nama_unit'] . ' vin : ' . $detailsatuan['vin_number'] . ' engine no : ' . $detailsatuan['engine_no'] . "</td>";
                                                            echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['harga_jual_unit']) . "</td>";
                                                            echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['terbayar']) . "</td>";
                                                            echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['sisa']) . "</td>";
                                                            echo "</tr>";
                                                        }
                                                        ?> 
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group" style="margin-top:50px">
                                    <a href="<?php echo base_url() . 'index.php/pembayaran/hutang' ?>" class="btn btn-default"  >Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    $(document).ready(function () {
        $("input").attr("readonly", "readonly");
        $(".select2").select2({disabled: "readonly"});
    })
</script>