
<?php

if (count($detail) > 0) {
    $detail = json_decode(json_encode($detail), true);
    foreach ($detail as $key => $detailsatuan) {
        echo "<tr>";
        echo "<td style='padding:10px;'>" .
                form_input(array('type' => 'hidden',   'name' => 'pembayaran_utang_detail_id[]', 'value' => $detailsatuan['pembayaran_utang_detail_id'])).
                form_input(array('type' => 'hidden',   'name' => 'id_unit_serial_pembayaran[]', 'value' => $detailsatuan['id_unit_serial'])).
                form_input(array('type' => 'hidden',   'name' => 'sisa_pembayaran[]', 'value' => $detailsatuan['sisa'])).
                $detailsatuan['no_prospek'].' - '.$detailsatuan['nomor_kpu']." - ".$detailsatuan['invoice_number'] . "</td>";
        echo "<td style='padding:10px;'>" . $detailsatuan['nama_unit'] .' vin : '.$detailsatuan['vin_number'].' engine no : '.$detailsatuan['engine_no'] . "</td>";
        echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['harga_jual_unit']) . "</td>";
        echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['terbayar']) . "</td>";
        echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['sisa'])  . "</td>";
        echo "<td style='padding:10px;'>" . form_input(array('type' => 'text', 'name' => 'jumlah_bayar[]', 'value' => DefaultCurrency($detailsatuan['jumlah_bayar']), 'class' => 'form-control detailpembayaran', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua()', 'onkeypress' => 'return isNumberKey(event);', 'placeholder' => 'Jumlah&nbsp;Bayar'))  . "</td>";
        echo "</tr>";
    }
} else {
    echo '<tr><td align="center" colspan="6">No Data Display</td></tr>';
}
?>