
<?php

if (count($detail) > 0) {
    $detail = json_decode(json_encode($detail), true);
    foreach ($detail as $key => $detailsatuan) {
        echo "<tr>";
        echo "<td style='padding:10px;'>" .
                form_input(array('type' => 'hidden',   'name' => 'id_pembelian_pembayaran[]', 'value' => $detailsatuan['id_pembelian'])).
                form_input(array('type' => 'hidden',   'name' => 'sisa_pembayaran[]', 'value' => $detailsatuan['sisa'])).
                $detailsatuan['nomor_invoice'].' - '.$detailsatuan['faktur_pajak'] ." - ".$detailsatuan['jumlah_unit']." Unit " . "</td>";
        echo "<td style='padding:10px;'>" . DefaultTanggal($detailsatuan['tanggal_invoice']) . "</td>";
        echo "<td style='padding:10px;'>" . DefaultTanggal($detailsatuan['jth_tempo']) . "</td>";
        echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['grandtotal']) . "</td>";
        echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['terbayar']) . "</td>";
        echo "<td style='padding:10px;'>" . DefaultCurrency($detailsatuan['sisa'])  . "</td>";
        echo "<td style='padding:10px;'>" . form_input(array('type' => 'text', 'name' => 'jumlah_bayar[]', 'value' => DefaultCurrency($detailsatuan['jumlah_pembayaran']), 'class' => 'form-control detailpembayaran', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua()', 'onkeypress' => 'return isNumberKey(event);', 'placeholder' => 'Jumlah&nbsp;Bayar'))  . "</td>";
        echo "</tr>";
    }
} else {
    echo '<tr><td align="center" colspan="6">No Data Display</td></tr>';
}
?>