<style>
    .remodal{max-width:1024px !Important;}
</style>
<section class="content-header">
    <h1>
        Pembayaran  Piutang
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">Pembayaran  Piutang</li>
    </ol>


</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="form-group">
                    <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                    <div class="col-sm-2">
                        <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), @$list_pencarian); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                    </div>


                    <div class="col-sm-2">
                        <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown(array('name' => 'status', 'selected' => @$status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "obj", "Status")); ?>
                    </div>



                </div>
                
                
                <div class="form-group form-groups-bordered">
                   <div class="col-sm-1">
                   </div>
                    
                    
                    <div class="col-sm-2">
                        <?= form_dropdown(array('name' => 'pencarian_keyword', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_pencarian_keyword', 'placeholder' => 'Pencarian'), DefaultEmptyDropdown(@$list_pencarian_keyword, "json", "Pencarian Keyword")); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'Placeholder' => 'Keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                    </div>

                </div>
                <div class="form-group form-groups-bordered">
                    <div class="col-sm-2">
                        <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                    </div>
                </div>

            </form>
        </div>

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">
                <?php echo anchor(site_url('pembayaran/piutang/create'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>


        <div class="">



            <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body form">
                        <table class="table table-striped table-bordered table-hover" id="mytable">

                        </table>
                    </div>
                </div>

            </div>


        </div>
    </div>


</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        table.fnDraw(false);
        return false;
    })

    function viewpembayaran(id_pembayaran_pempiutangan_master)
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pembayaran/piutang/viewpembayaranpiutang',
            data: {
                id_pembayaran_pempiutangan_master: id_pembayaran_pempiutangan_master
            },
            success: function (data) {
                modaldialog(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }

        });
    }
    function batal(pembayaran_piutang_id) {
        swal({
            title: "Apakah kamu yakin membatalkan pembayaran berikut?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembayaran/piutang/batal',
                    dataType: 'json',
                    data: {
                        pembayaran_piutang_id: pembayaran_piutang_id
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }

                });
            }
        });
        return false;

    }
    function CekKeyword() {
        if (CheckEmpty($("#dd_pencarian_keyword").val()))
        {
            $("#txt_keyword").attr("disabled", "disabled");

        } else
        {
            $("#txt_keyword").removeAttr("disabled");
        }
    }


    function RefreshGrid() {


    }

    $("#dd_pencarian_keyword").change(function () {
        CekKeyword();
    })
    $(document).ready(function () {
        CekKeyword();
        $(".datepicker").datepicker();
        $("select").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl + "index.php/pembayaran/piutang/getdatapembayaranpiutang", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "pembayaran_piutang_id",
                    title: "Kode",
                    width: "50px",
                    orderable: false
                }
                , {data: "tanggal_transaksi", orderable: false, title: "Tanggal", "width": "120px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data == undefined ? 0 : data);
                    }}
                , {data: "dokumen", orderable: false, title: "Kode&nbsp;Pembayaran", "width": "100px"}
                , {data: "nama_customer", orderable: false, title: "Customer", "width": "150px"}
                , {data: "nomor_invoice", orderable: false, title: "NO Doc", "width": "100px"}
                , {data: "jenis_pembayaran", orderable: false, title: "Pembayaran", "width": "100px"}
                , {data: "total_bayar", orderable: false, title: "Jumlah", "width": "120px",
                    mRender: function (data, type, row) {
                        return Comma(data);
                    }}
                , {data: "simpanan", orderable: false, title: "Simpanan", "width": "120px",
                    mRender: function (data, type, row) {
                        return Comma(data);
                    }}
                , {orderable: false, title: "GrandTotal", "width": "120px",
                    mRender: function (data, type, row) {
                        console.log(row.total_bayar + row.simpanan);
                        return Comma(Number(row.total_bayar) + Number(row.simpanan));
                    }},

                {
                    "data": "action_view",
                    "orderable": false,
                    "width": "180px",
                    "className": "text-center nopadding",
                    mRender: function (data, type, row) {

                        var action = "";
                        if (row['status'] == "2")
                        {
                            action = row['action_view'] + ' | Batal';
                        } else
                        {
                            action = row['action_view'] + row['action_edit'] + row['action_delete'];
                        }
                        return action;
                    }
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {
                RefreshGrid();
            }
        });

    });
</script>
