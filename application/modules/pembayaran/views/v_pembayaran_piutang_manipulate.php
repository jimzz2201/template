<style>
    .remodal{max-width:1024px !Important;}
</style>
<section class="content-header">
    <h1>
        Pembayaran  Piutang
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">Pembayaran  Piutang</li>
    </ol>


</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <div class="form-group" >

                            </div>
                            <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="withdocument" value="yes" />
                                <input type="hidden" name="pembayaran_piutang_id" value="<?php echo @$pembayaran_piutang_id?>" />
                                <div id="notification"></div>

                                <div class="form-group">

                                    <?= form_label('Type Pembayaran', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "jenis_pembayaran"), DefaultEmptyDropdown(@$list_type_pembayaran, "", "Pembayaran"), @$jenis_pembayaran, array('class' => 'form-control select2', 'id' => 'dd_jenis_pembayaran')); ?>
                                    </div>
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal_transaksi), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Customer', "txt_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        if (CheckEmpty(@$id_customer)) {
                                            $listcustomer = DefaultEmptyDropdown(@$listcustomer, "json", "Customer");
                                        }
                                        ?>
                                        <?= form_dropdown(array("name" => "id_customer"), $listcustomer, @$id_customer, array('class' => 'form-control', 'id' => 'dd_id_customer')); ?>
                                    </div>    
                                    <?= form_label('Piutang', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$piutang), 'class' => 'form-control', 'id' => 'txt_piutang', 'placeholder' => 'Piutang')); ?>
                                    </div>


                                </div>
                                <div class="form-group">

                                    <?php
                                    $listbank = DefaultEmptyDropdown(@$listbank, "json", "Bank");
                                    ?>
                                    <?= form_label('Nama Bank', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array(), @$listbank, @$id_bank, array("disabled" => "disabled", 'class' => 'form-control', 'id' => 'dd_id_bank')); ?>
                                    </div>
                                    <div id="areadp" style="display:none">
                                        <?= form_label('DP', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$dp), 'class' => 'form-control', 'id' => 'txt_dp', 'placeholder' => 'DP')); ?>
                                        </div>
                                    </div>
                                </div>
                                <hr/>

                                <div class="form-group">


                                    <?= form_label('Nomor Cek', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => @$no_jenis_pembayaran, 'class' => 'form-control', 'id' => 'txt_no_jenis_pembayaran', 'name' => 'no_jenis_pembayaran', 'placeholder' => 'No Cek')); ?>
                                    </div>
                                    <?= form_label('Jatuh&nbsp;Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => DefaultDatePicker(@$jatuh_tempo), 'class' => 'form-control datepicker', 'id' => 'txt_jatuh_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                    </div>
                                </div>
                                <div class="form-group">


                                    <?= form_label('Debit Card', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => @$debit_card, 'class' => 'form-control', 'id' => 'txt_debit_card', 'name' => 'debit_card', 'placeholder' => 'Debit Card')); ?>
                                    </div>
                                    <?= form_label('Biaya Lain', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "name" => "biaya", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$biaya_lain), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_biaya_lain', 'placeholder' => 'Biaya Lain')); ?>
                                    </div>
                                </div>
                                <div class="form-group">


                                    <?= form_label('Credit Card', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", 'autocomplete' => 'off', 'value' => @$credit_card, 'class' => 'form-control', 'id' => 'txt_credit_card', 'name' => 'credit_card', 'placeholder' => 'Kredit Card')); ?>
                                    </div>
                                    <?= form_label('Potongan', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', "name" => "potongan", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$potongan), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_potongan', 'placeholder' => 'Potongan')); ?>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Dokumen', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array(), DefaultEmptyDropdown(@$listdokumen, "", "Dokumen"), @$id_pembelian, array('class' => 'form-control', 'id' => 'dd_dokumen')); ?>
                                    </div>
                                    <?= form_label('Sisa', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', "disabled" => "disabled", "name" => "sisa", 'autocomplete' => 'off', 'value' => DefaultCurrency(@$sisa), 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'class' => 'form-control', 'id' => 'txt_sisa', 'placeholder' => 'Sisa')); ?>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" class="btn btn-primary btn-block btn-oke" id="btt_save_detail" >Save</button>
                                    </div>
                                </div>
                                <div class="form-group areaprospek" style="display:none">
                                    <?= form_label('Buka Jual', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array(), DefaultEmptyDropdown(@$listbukajual, "", "Buka Jual"), @$id_buka_jual, array('class' => 'form-control', 'id' => 'dd_buka_jual', 'name' => 'id_buka_jual')); ?>
                                    </div>
                                    <?= form_label('Unit', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array(), DefaultEmptyDropdown(@$listunitserial, "", "Unit"), @$id_unit_serial, array('class' => 'form-control', 'id' => 'dd_id_unit_serial', 'name' => 'id_unit_serial')); ?>
                                    </div>

                                </div>
                                <div class="box box-default">


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="portlet-body form">
                                                <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>Dokumen</th>
                                                            <th>Unit</th>
                                                            <th>GrandTotal</th>
                                                            <th>Terbayar</th>
                                                            <th>Sisa</th>
                                                            <th>Pembayaran</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="bodytable">
                                                        <?php include APPPATH . 'modules/pembayaran/views/v_pembayaran_content.php' ?> 
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <hr/>

                                <div class="form-group">
                                    <?= form_label('Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total', "readonly" => "readonly")); ?>
                                    </div>

                                    <?= form_label('Simpanan', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'simpanan', 'value' => DefaultCurrency(@$simpanan), 'class' => 'form-control', 'id' => 'txt_simpanan', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'placeholder' => 'Jumlah&nbsp;Bayar')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Jumlah&nbsp;Bayar', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'jumlah_bayar_total', 'value' => DefaultCurrency(@$jumlah_bayar), 'class' => 'form-control', 'id' => 'txt_jumlah_bayar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', 'placeholder' => 'Jumlah&nbsp;Bayar')); ?>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>

    var table;
    var dataset = <?php echo json_encode(@$detail) ?>;

    $("#btt_save_detail").click(function () {

        if ($("#dd_id_unit_serial").val() == "0")
        {
            messageerror("Tidak ada unit yang dipilih");
        } else
        {
            var res = alasql('SELECT * FROM ? where id_unit_serial=\'' + $("#dd_id_unit_serial").val() + '\'', [dataset]);
            if (res.length > 0)
            {
                messageerror("Data Unit Sudah ada dalam list pengajuan");
            } else
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/unit/get_one_serial',
                    dataType: 'json',
                    data: {
                        id_unit_serial: $("#dd_id_unit_serial").val(),
                        typehtml: "pembayaran"
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            var pembelian = [];
                            pembelian['sisa'] = data.obj.sisa;
                            pembelian['id_unit_serial'] = data.obj.id_unit_serial;
                            dataset.push(pembelian);
                            if (dataset.length == 1)
                            {
                                $("#bodytable").html(data.html);
                            } else
                            {
                                $("#bodytable").append(data.html);
                            }

                            $("#dd_dokumen").val(0).trigger("change");
                            $("#txt_sisa").val(0);
                            HitungSemua();
                            RefreshPanelGrid();


                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        }
    })

    $(document).ready(function () {
        $("#dd_jenis_pembayaran").select2();
        $("#dd_id_bank").select2();
        CekSectionPart($("#dd_jenis_pembayaran").val());
        HitungSemua();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
<?php
if (CheckEmpty(@$pembayaran_piutang_id)) {
    echo"allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }";
}
?>

        });


        $("#dd_jenis_pembayaran").on("select2:select", function (e) {
            CekSectionPart($(this).val());
        });
        $("#dd_dokumen").select2();
        $(".datepicker").datepicker();
        $("#dd_id_customer").change(function () {
            dataset = [];
            $("#bodytable").html("<tr><td align=\"center\" colspan=\"6\">No Data Display</td></tr>");
            $("#txt_total").val(0);
            if ($("#dd_id_customer").val() != "0")
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/customer/get_one_customer',
                    dataType: 'json',
                    data: $("form#frm_pembelian").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            $("#txt_piutang").val(Comma(data.obj.piutang));
                            $("#dd_dokumen").empty().select2({data: DefaultArray(data.obj.list_doc, "Dokumen")});
                            $("#txt_dp").val(Comma(data.obj.dp));
                            $("#dd_dokumen").change(function () {
                                GetPembelian();
                            })
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            } else
            {
                $("#txt_kode_customer").val("");
                $("#txt_alamat").html("");
                $("#txt_saldo_awal").val("0");
                $("#txt_piutang").val("0");
                $("#txt_saldo_semestinya").val("0");
            }
        })

    })
    function GetPembelian()
    {
        if ($("#dd_dokumen").val() != "0")
        {
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/prospek/get_one_prospek',
                dataType: 'json',
                data: {
                    id_prospek: $("#dd_dokumen").val()
                },
                success: function (data) {
                    if (data.st)
                    {
                        if (data.obj.type == "Prospek")
                        {
                            $(".areaprospek").css("display", "block");
                            $("#dd_id_unit_serial").empty().select2({data: DefaultArray(data.obj.list_unit, "Unit")});
                            $("#dd_buka_jual").empty().select2({data: DefaultArray(data.obj.list_buka_jual, "Buka Jual")});
                        } else
                        {
                            $(".areaprospek").css("display", "none");
                        }
                        $("#txt_sisa").val(Comma(data.obj.sisa));

                    } else
                    {
                        messageerror(data.msg);
                    }
                    LoadBar.hide();

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        } else
        {
            $("#txt_sisa").val(0);
        }
    }
    function CekSectionPart(jenis_bayar)
    {
        if (jenis_bayar == "Cash" || jenis_bayar == "0" || jenis_bayar == "DP")
        {
            $("#dd_id_bank").attr("disabled", "disabled");
        } else
        {
            $("#dd_id_bank").removeAttr("disabled");
        }
        if (jenis_bayar == "DP")
        {
            $("#areadp").css("display", "block");
        } else
        {
            $("#areadp").css("display", "none");
        }

        if (jenis_bayar != "Debit Card")
        {
            $("#txt_debit_card").attr("disabled", "disabled");
        } else
        {
            $("#txt_debit_card").removeAttr("disabled");
        }
        if (jenis_bayar != "Credit Card")
        {
            $("#txt_credit_card").attr("disabled", "disabled");
        } else
        {
            $("#txt_credit_card").removeAttr("disabled");
        }
        if (jenis_bayar != "Cek & Giro")
        {
            $("#txt_jatuh_tempo").attr("disabled", "disabled");
        } else
        {
            $("#txt_jatuh_tempo").removeAttr("disabled");
        }
    }
    function HitungSemua() {
        var jumlahtotal = 0;
        $(dataset).each(function (index, element) {
            jumlahtotal += element.sisa;
        });
        var jumlah_bayar = 0;
        $(".detailpembayaran").each(function (index) {
            jumlah_bayar += Number($(this).val().replace(/[^0-9\.]+/g, ""));
        });
        var biayalain = Number($("#txt_biaya_lain").val().replace(/[^0-9\.]+/g, ""));
        var simpanan = Number($("#txt_simpanan").val().replace(/[^0-9\.]+/g, ""));
        var potongan = Number($("#txt_potongan").val().replace(/[^0-9\.]+/g, ""));
        var jumlahseluruh = jumlahtotal + biayalain - potongan;
        $("#txt_total").val(number_format(jumlahseluruh));
        $("#txt_jumlah_bayar").val(number_format(jumlah_bayar + simpanan));

    }

    function RefreshPanelGrid()
    {
        if (dataset.length > 0)
        {
            $("#frm_pembelian button[type=submit]").removeAttr("disabled");
        } else
        {
            $("#frm_pembelian button[type=submit]").attr("disabled", "disabled");
        }

    }

    $(document).ready(function () {
        $("#txt_tanggal").datepicker();
        RefreshPanelGrid();


    })


    $("#frm_pembelian").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data pembayaran berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                LoadBar.show();
                console.log(dataset);
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/pembayaran/piutang/pembayaran_piutang_manipulate',
                    dataType: 'json',
                    data: $("#frm_pembelian").serialize() ,
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/pembayaran/piutang";

                        } else
                        {
                            messageerror(data.msg);
                        }
                        LoadBar.hide();

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                        LoadBar.hide();
                    }
                });
            }
        });
        return false;


    })
</script>