<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pembayaran_hutang extends CI_Model {

    public $table = '#_pembayaran_utang_master';
    public $id = 'id_pembayaran_hutang_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDataPembayaranHutang($search) {
        $isedit = CekModule("K109", false);
        $isdelete = CekModule("K109", false);
        $this->load->library('datatables');
        $listid = [];
        if (!CheckEmpty($search['pencarian_keyword']) && !CheckEmpty($search['keyword'])) {
            $searchby = $search['pencarian_keyword'];
            $this->db->from("#_pembayaran_utang_detail");
            $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id", "left");
            $this->db->join("#_unit_serial", "#_unit_serial.id_unit_serial=#_pembayaran_utang_detail.id_unit_serial", "left");
            $this->db->join("#_kpu", "#_kpu.id_kpu=#_pembayaran_utang_detail.id_kpu", "left");
            $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual", "left");
            $this->db->where("lower(" . $searchby . ")", strtolower($search['keyword']));
            $this->db->distinct();
            $listid = $this->db->select("pembayaran_utang_detail_id")->get()->result_array();

            $listid = array_column($listid, "pembayaran_utang_detail_id");

            if (count($listid) == 0) {
                $listid[] = -1;
            }
        }

        $this->datatables->select('#_pembayaran_utang_master.tanggal_transaksi,#_pembayaran_utang_master.total_bayar,#_pembayaran_utang_master.jenis_pembayaran,#_pembayaran_utang_master.dokumen,#_pembayaran_utang_master.pembayaran_utang_id,nama_supplier,replace(#_pembayaran_utang_master.dokumen,"/","-") as nomor_master_link,group_concat(distinct #_pembelian.nomor_invoice SEPARATOR  \' , \') as nomor_invoice,#_pembayaran_utang_master.status');
        $this->datatables->join("#_supplier", "#_supplier.id_supplier=#_pembayaran_utang_master.id_supplier", "left");
        $this->datatables->join("#_pembayaran_utang_detail", "#_pembayaran_utang_detail.pembayaran_utang_id=#_pembayaran_utang_master.pembayaran_utang_id", "left");
        $this->datatables->join("#_pembelian", "#_pembelian.id_pembelian=#_pembayaran_utang_detail.id_pembelian", "left");
        $this->datatables->from('#_pembayaran_utang_master');
        $this->datatables->group_by('#_pembayaran_utang_master.pembayaran_utang_id');
        if (count($listid) > 0) {
            $this->datatables->where_in("pembayaran_utang_detail_id", $listid);
        } else {
            if (isset($search['id_supplier']) && !CheckEmpty($search['id_supplier'])) {
                $params['#_pembayaran_utang_master.id_supplier'] = $search['id_supplier'];
            }


            if (isset($search['status']) && !CheckEmpty($search['status'])) {
                $params['#_pembayaran_utang_master.status'] = $search['status'];
            }

            $tanggal = "date(#_pembayaran_utang_master.created_date)";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_pembayaran_utang_master.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_pembayaran_utang_master.updated_date)";
            } else if ($params['jenis_pencarian'] == "tanggal") {
                $tanggal = "#_pembayaran_utang_master.tanggal_transaksi";
            }
            $this->db->order_by($tanggal, "desc");


            if (isset($search['start_date'], $search['end_date']) && !empty($search['start_date']) && !empty($search['end_date'])) {
                array_push($extra, "#_pembayaran_utang_master.tanggal_transaksi BETWEEN '" . DefaultTanggalDatabase($search['start_date']) . "' AND '" . DefaultTanggalDatabase($search['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else if (isset($search['start_date']) && empty($search['end_date'])) {
                $params['#_pembayaran_utang_master.tanggal_transaksi'] = DefaultTanggalDatabase($search['start_date']);
                unset($params['start_date'], $params['end_date']);
            } else if (isset($search['end_date']) && empty($search['start_date'])) {
                $params['#_pembayaran_utang_master.tanggal_transaksi'] = DefaultTanggalDatabase($search['end_date']);
                unset($params['start_date'], $params['end_date']);
            }
        }





        if (count($params)) {
            $this->datatables->where($params);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        $stredit = '';
        $strdelete = '';
        $strview = anchor(site_url('pembayaran/hutang/pembayaran_hutang_view/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $stredit = anchor("pembayaran/hutang/editutang/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strdelete .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action_view', $strview, 'pembayaran_utang_id');
        $this->datatables->add_column('action_edit', $stredit, 'pembayaran_utang_id,nomor_master_link');
        $this->datatables->add_column('action_delete', $strdelete, 'pembayaran_utang_id,nomor_master_link');
        return $this->datatables->generate();
    }

    function GenerateAkunPembayaranHutang($id_pembayaran_hutang) {

        $userid = GetUserId();
        $this->load->model("supplier/m_supplier");
        $this->load->model("invoice/m_invoice");
        $pembayaran = $this->GetOnePembayaranHutang($id_pembayaran_hutang);
      
          $supplier = $this->m_supplier->GetOneSupplier($pembayaran->id_supplier);
        $invoice = "";
        $bank = null;
        $listidpembelian = [];

        if ($pembayaran != null) {
            $this->load->model("bank/m_bank");
            $bank = $this->m_bank->GetOneBank($pembayaran->id_bank);

            $invoice = "";
           
            foreach ($pembayaran->detail as $det) {
                $invoice .= $det['nomor_invoice'] . ' ,';
                if (!CheckKey($listidpembelian, $det['id_pembelian'])) {
                    $listidpembelian[$det['id_pembelian']] = $det['id_pembelian'];
                }
            }
    
            $this->load->model("gl_config/m_gl_config");
            $akunglkas = $this->m_gl_config->GetIdGlConfig("kasbesar", $pembayaran->id_cabang);
            $dokumenpembayaran = $pembayaran->dokumen;

            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglkas, "dokumen" => $dokumenpembayaran));
            $akun_kas = $this->db->get()->row();
            $arrglakun = [];
            if (CheckEmpty($akun_kas) && $pembayaran->jenis_pembayaran == "Cash") {
                $akunkasinsert = array();
                $akunkasinsert['id_gl_account'] = $akunglkas;
                $akunkasinsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunkasinsert['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunkasinsert['dokumen'] = $dokumenpembayaran;
                $akunkasinsert['subject_name'] = @$supplier->kode_supplier;
                $akunkasinsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akunkasinsert['debet'] = 0;
                $akunkasinsert['kredit'] = $pembayaran->total_bayar;
                $akunkasinsert['created_date'] = GetDateNow();
                $akunkasinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunkasinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_kas) && $pembayaran->jenis_pembayaran == "Cash") {
                $akunkasupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunkasupdate['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunkasupdate['dokumen'] = $dokumenpembayaran;
                $akunkasupdate['subject_name'] = @$supplier->kode_supplier;
                $akunkasupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akunkasupdate['debet'] = 0;
                $akunkasupdate['kredit'] = $pembayaran->total_bayar;
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
                $arrglakun[] = $akun_kas->id_buku_besar;
            } else if (!CheckEmpty($akun_kas) && $pembayaran->jenis_pembayaran != "Cash") {
                $akunkasupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunkasupdate['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunkasupdate['dokumen'] = $dokumenpembayaran;
                $akunkasupdate['subject_name'] = @$supplier->kode_supplier;
                $akunkasupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akunkasupdate['debet'] = 0;
                $akunkasupdate['kredit'] = 0;
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
                $arrglakun[] = $akun_kas->id_buku_besar;
            }
             $akunglbank = $this->m_gl_config->GetIdGlConfig("bank", $pembayaran->id_cabang, array("bank" => @$pembayaran->id_bank));
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglbank, "dokumen" => $dokumenpembayaran));
            $akun_bank = $this->db->get()->row();
    
            if (CheckEmpty($akun_bank) && $pembayaran->jenis_pembayaran != "Cash" && $pembayaran->jenis_pembayaran != "Potongan") {
                $akunbankinsert = array();
                $akunbankinsert['id_gl_account'] = $akunglbank;
                $akunbankinsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunbankinsert['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunbankinsert['dokumen'] = $dokumenpembayaran;
                $akunbankinsert['subject_name'] = @$bank->kode_bank;
                $akunbankinsert['id_subject'] = ForeignKeyFromDb(@$pembayaran->id_bank);
                $akunbankinsert['debet'] = 0;
                $akunbankinsert['kredit'] = $pembayaran->total_bayar;
                $akunbankinsert['created_date'] = GetDateNow();
                $akunbankinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunbankinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_bank) && $pembayaran->jenis_pembayaran != "Cash" && $pembayaran->jenis_pembayaran != "Potongan") {
                $akunbankupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunbankupdate['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunbankupdate['dokumen'] = $dokumenpembayaran;
                $akunbankupdate['subject_name'] = @$bank->kode_bank;
                $akunbankupdate['id_subject'] = ForeignKeyFromDb(@$pembayaran->id_bank);
                $akunbankupdate['debet'] = 0;
                $akunbankupdate['kredit'] = $pembayaran->total_bayar;
                $akunbankupdate['updated_date'] = GetDateNow();
                $akunbankupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunbankupdate, array("id_buku_besar" => $akun_bank->id_buku_besar));
                $arrglakun[] = $akun_bank->id_buku_besar;
            } else if (!CheckEmpty($akun_bank) && ($pembayaran->jenis_pembayaran == "Cash" || $pembayaran->jenis_pembayaran == "Potongan")) {
                $akunbankupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunbankupdate['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunbankupdate['dokumen'] = $dokumenpembayaran;
                $akunbankupdate['subject_name'] = @$bank->kode_bank;
                $akunbankupdate['id_subject'] = ForeignKeyFromDb(@$pembayaran->id_bank);
                $akunbankupdate['debet'] = 0;
                $akunbankupdate['kredit'] = 0;
                $akunbankupdate['updated_date'] = GetDateNow();
                $akunbankupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunbankupdate, array("id_buku_besar" => $akun_bank->id_buku_besar));
                $arrglakun[] = $akun_bank->id_buku_besar;
            }

            $akunpotonganpembayaran = $this->m_gl_config->GetIdGlConfig("potonganpembayaran", $pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpotonganpembayaran, "dokumen" => $dokumenpembayaran));
            $akun_potongan = $this->db->get()->row();

            if (CheckEmpty($akun_potongan) && $pembayaran->jenis_pembayaran == "Potongan") {
                $akunpotonganinsert = array();
                $akunpotonganinsert['id_gl_account'] = $akunpotonganpembayaran;
                $akunpotonganinsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunpotonganinsert['keterangan'] = "Potongan Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunpotonganinsert['dokumen'] = $dokumenpembayaran;
                $akunpotonganinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganinsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akunpotonganinsert['debet'] = 0;
                $akunpotonganinsert['kredit'] = $pembayaran->total_bayar;
                $akunpotonganinsert['created_date'] = GetDateNow();
                $akunpotonganinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpotonganinsert);
                $arrglakun[] = $$this->db->isnert_id();
            } else if (!CheckEmpty($akun_potongan) && $pembayaran->jenis_pembayaran == "Potongan") {
                $akunpotonganupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunpotonganupdate['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunpotonganupdate['dokumen'] = $dokumenpembayaran;
                $akunpotonganupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akunpotonganupdate['debet'] = 0;
                $akunpotonganupdate['kredit'] = $pembayaran->total_bayar;
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
                $arrglakun[] = $akun_potongan->id_buku_besar;
            } else if (!CheckEmpty($akun_potongan) && $pembayaran->jenis_pembayaran != "Potongan") {
                $akunpotonganupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunpotonganupdate['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunpotonganupdate['dokumen'] = $dokumenpembayaran;
                $akunpotonganupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akunpotonganupdate['debet'] = 0;
                $akunpotonganupdate['kredit'] = 0;
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
                $arrglakun[] = $akun_potongan->id_buku_besar;
            }


            $akunglhutang = $this->m_gl_config->GetIdGlConfig("utang", $$pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglhutang, "dokumen" => $dokumenpembayaran));
            $akun_utang = $this->db->get()->row();

            if (CheckEmpty($akun_utang)) {
                $akunutanginsert = array();
                $akunutanginsert['id_gl_account'] = $akunglhutang;
                $akunutanginsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunutanginsert['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunutanginsert['dokumen'] = $dokumenpembayaran;
                $akunutanginsert['subject_name'] = @$supplier->kode_supplier;
                $akunutanginsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akunutanginsert['debet'] = $pembayaran->total_bayar;
                $akunutanginsert['kredit'] = 0;
                $akunutanginsert['created_date'] = GetDateNow();
                $akunutanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunutanginsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_utang)) {
                $akunutangupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunutangupdate['keterangan'] = "Pembayaran  " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akunutangupdate['dokumen'] = $dokumenpembayaran;
                $akunutangupdate['subject_name'] = @$supplier->kode_supplier;
                $akunutangupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akunutangupdate['debet'] = $pembayaran->total_bayar;
                $akunutangupdate['kredit'] = 0;
                $akunutangupdate['updated_date'] = GetDateNow();
                $akunutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunutangupdate, array("id_buku_besar" => $akun_utang->id_buku_besar));
                $arrglakun[] = $akun_utang->id_buku_besar;
            }

            $this->db->where(array("dokumen" => $dokumenpembayaran));
            if (count($arrglakun) > 0) {
                $this->db->where_not_in("id_buku_besar", $arrglakun);
            }
            $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)));






            $akunglpenyeimbang = $this->m_gl_config->GetIdGlConfig("penyeimbang", $pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("dokumen" => $dokumenpembayaran, "id_gl_account !=" => $akunglpenyeimbang));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglpenyeimbang, "dokumen" => $dokumenpembayaran));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = $akunglpenyeimbang;
                $akun_penyeimbanginsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk pembayaran Ke " . @$supplier->nama_supplier . ' untuk invoice : ' . $invoice;
                $akun_penyeimbanginsert['dokumen'] = $dokumenpembayaran;
                $akun_penyeimbanginsert['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbanginsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akun_penyeimbanginsert);
            } else if (!CheckEmpty($akun_penyeimbang) && $row->selisih != 0) {
                $akun_penyeimbangupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk pembayaran Ke " . @$supplier->nama_supplier . ' untuk bps : ' . $bpsno;
                $akun_penyeimbangupdate['dokumen'] = $dokumenpembayaran;
                $akun_penyeimbangupdate['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbangupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk pembayaran Ke " . @$supplier->nama_supplier . ' untuk bps : ' . $bpsno;
                $akun_penyeimbangupdate['dokumen'] = $dokumenpembayaran;
                $akun_penyeimbangupdate['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbangupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_supplier);
                $akun_penyeimbangupdate['debet'] = 0;
                $akun_penyeimbangupdate['kredit'] = 0;
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            }
          
            foreach ($pembayaran->detail as $key => $det) {
                  if(!CheckEmpty($det['id_pembelian']))
                  {
                       $this->m_invoice->UpdateStatusInvoice($det['id_pembelian']); 
                  }
              
            }
            
            $this->m_supplier->KoreksiSaldoHutang($pembayaran->id_supplier);
        }
    }

    function PembayaranHutang($model) {
      
        try {
            
          
            $jenis_pencarian = "created_date";
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $bps = array();
            $this->load->model("supplier/m_supplier");
            $this->load->model("kpu/m_kpu");
            $this->load->model("invoice/m_invoice");
            $this->load->model("bank/m_bank");
            $this->load->model("unit/m_unit");
            $supplier = $this->m_supplier->GetOneSupplier($model['id_supplier'], 'id_supplier', true);
            $bank = $this->m_bank->GetOneBank(@$model['id_bank']);
            $userid = GetUserId();
            $pph = 0;
            $totalpembayaran = 0;
            $jumlah_bayar=0;
            foreach(@$model['jumlah_bayar'] as $jumlah_detail)
            {
                $jumlah_bayar+= DefaultCurrencyDatabase($jumlah_detail);
            }
           
            
            $totalbiaya = $jumlah_bayar + DefaultCurrencyDatabase(@$model['potongan']) - DefaultCurrencyDatabase(@$model['biaya']);
            $listdbpembayaran = [];
            $bpsno = "";
            $idprospek = [];
            $idbukajual = [];
            $jumlahpembayaran=$totalbiaya;
            foreach ($model['dataset']as $detail) {
                 $listdetail=$this->m_invoice->GetDetailPembayaranHutang($detail['id_pembelian_pembayaran'],@$model['pembayaran_utang_id']);
                 
                 foreach($listdetail as $pembayaranunit)
                 {
                     if($jumlahpembayaran==0)
                     {
                         break;
                     }
                     else{
                         if ($jumlahpembayaran - $pembayaranunit->sisa > 0) {
                            $jumlahpembayaran-=$pembayaranunit->sisa;
                            $pembayaranunit->jumlah_pembayaran=$pembayaranunit->sisa;
                            $listdbpembayaran[]=$pembayaranunit;
                        }
                        else
                        {
                            $pembayaranunit->jumlah_pembayaran=$jumlahpembayaran;
                            $jumlahpembayaran=0;
                            $listdbpembayaran[]=$pembayaranunit;
                        }
                    }
                 }
                 
            }
            
             if ($jumlahpembayaran > 0) {
                $jumlah_bayar -= $jumlahpembayaran;
            }
            $pembayaranmaster = array();
            $pembayaranmaster['tanggal_transaksi'] = DefaultTanggalDatabase($model['tanggal']);
            $pembayaranmaster['id_supplier'] = $model['id_supplier'];
            $pembayaranmaster['jenis_pembayaran'] = $model['jenis_pembayaran'];
            $pembayaranmaster['total_bayar'] = DefaultCurrencyDatabase($jumlah_bayar);
            $pembayaranmaster['biaya'] = DefaultCurrencyDatabase($model['biaya']);
            $pembayaranmaster['potongan'] = DefaultCurrencyDatabase($model['potongan']);
            $pembayaranmaster['status'] = 1;
            $pembayaranmaster['id_bank'] = ForeignKeyFromDb(@$model['id_bank']);
            $pembayaranmaster['keterangan'] = "";
           
           
            if (CheckEmpty(@$model['pembayaran_utang_id'])) {
                $pembayaranmaster= MergeCreated($pembayaranmaster);
                if(!CheckEmpty(@$model['pembayarandok']))
                {
                    $dokumenpembayaran=@$model['pembayarandok'];
                }
                else
                {
                     $dokumenpembayaran = AutoIncrement('#_pembayaran_utang_master', 'BB/' . date("y") . '/', 'dokumen', 5);
                }
                $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                $this->db->insert("#_pembayaran_utang_master", $pembayaranmaster);
                $pembayaran_utang_id = $this->db->insert_id();
            } else {
                $jenis_pencarian = "updated_date";
                $this->db->from("#_pembayaran_utang_master");
                $this->db->where(array("pembayaran_utang_id" => $model['pembayaran_utang_id']));
                $pembayarandb = $this->db->get()->row();
                if ($pembayarandb) {
                    $dokumenpembayaran = $pembayarandb->dokumen;
                    $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                    $pembayaran_utang_id = $pembayarandb->pembayaran_utang_id;
                    $pembayaranmaster= MergeUpdate($pembayaranmaster);
                    $this->db->update("#_pembayaran_utang_master", $pembayaranmaster, array("pembayaran_utang_id" => $pembayarandb->pembayaran_utang_id));
                }
            }

            $invoice = "";
            $listidpembayaran=[];
            
            foreach ($listdbpembayaran as $pembayaran) {
                $pembayarandetail = array();

                $pembayarandetail['jumlah_bayar'] = DefaultCurrencyDatabase($pembayaran->jumlah_pembayaran);
                $this->db->from("#_pembayaran_utang_detail");
                $this->db->where(array("pembayaran_utang_id" => $pembayaran_utang_id, "id_unit_serial" => $pembayaran->id_unit_serial));
                $dbdet = $this->db->get()->row();
            
               
                if ($dbdet) {
                    $listidpembayaran[]=$dbdet->pembayaran_utang_detail_id;
                    $this->db->update("#_pembayaran_utang_detail", $pembayarandetail, array("pembayaran_utang_detail_id"=>$dbdet->pembayaran_utang_detail_id));
                } else {
                    $pembayarandetail['id_kpu'] = $pembayaran->id_kpu;
                    $pembayarandetail['id_pembelian'] = $pembayaran->id_pembelian;
                    $pembayarandetail['pembayaran_utang_id'] = $pembayaran_utang_id;
                    $pembayarandetail['created_date'] = GetDateNow();
                    $pembayarandetail['created_by'] = $userid;
                    $pembayarandetail['id_unit_serial'] = $pembayaran->id_unit_serial;
                    $this->db->insert("#_pembayaran_utang_detail", $pembayarandetail);
                }
                    
            }
             $this->GenerateAkunPembayaranHutang($pembayaran_utang_id);


            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(1, "Data Pembayaran  Sudah di" . (CheckEmpty(@$model['pembayaran_utang_id']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($pembayaran_utang_id, $dokumenpembayaran, 'pembayaran');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function HutangBatal($pembayaran_utang_id) {
        $message = "";
        $pembayaran = $this->GetOnePembayaranHutang($pembayaran_utang_id, true);
        if (!$pembayaran) {
            $message .= "Pembayaran tidak terdapat dalam database";
        }

        $this->load->model("supplier/m_supplier");
        $this->load->model("kpu/m_kpu");

        try {
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();

            $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)), array("dokumen" => $pembayaran->dokumen));
            $this->db->update("#_pembayaran_utang_master", MergeUpdate(array("status" => 2)), array("pembayaran_utang_id" => $pembayaran->pembayaran_utang_id));
            foreach ($pembayaran->detail as $detail) {
                $this->m_kpu->UpdateStatusPembayaranKPU($detail->id_invoice);
            }
            $this->m_supplier->KoreksiSaldoHutang($pembayaran->id_supplier);

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                $arrayreturn['msg'] = "Data Pembayaran Sudah di batalkan";
                $arrayreturn["st"] = true;
                //SetPrint($id_penjualan, $penjualan['nomor_master'], 'penjualan');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    
    function GetOnePembayaranHutang($id) {
        $this->load->model("kpu/m_kpu");
        $this->db->where($this->table . '.pembayaran_utang_id', $id);
        $this->db->join('#_supplier', '#_supplier.id_supplier=' . $this->table . '.id_supplier', 'left');
        $this->db->join('#_bank', '#_bank.id_bank=' . $this->table . '.id_bank', 'left');
        $data = $this->db->get($this->table)->row();

        $this->db->from("#_pembayaran_utang_detail");
        $this->db->where('#_pembayaran_utang_detail.pembayaran_utang_id', $id);
        $this->db->join('#_pembelian','#_pembelian.id_pembelian=#_pembayaran_utang_detail.id_pembelian','left');
        $this->db->select("#_pembelian.id_pembelian,#_pembelian.grandtotal,#_pembelian.jth_tempo,#_pembelian.nomor_invoice,#_pembelian.tanggal_invoice,#_pembelian.jumlah_unit,#_pembelian.faktur_pajak,sum(#_pembayaran_utang_detail.jumlah_bayar) as jumlah_pembayaran");
        
        $this->db->group_by("id_pembelian");
        $detail = $this->db->get()->result_array();
        $idpembelian= array_column($detail, 'id_pembelian');
        
        $this->db->from("#_pembayaran_utang_master");
        $this->db->join("#_pembayaran_utang_detail","#_pembayaran_utang_detail.pembayaran_utang_id=#_pembayaran_utang_master.pembayaran_utang_id");
 
        $this->db->select("#_pembayaran_utang_detail.id_pembelian,sum(#_pembayaran_utang_detail.jumlah_bayar) as terbayar");
        if(count($idpembelian)>0)
        {
               $this->db->where_in("id_pembelian",$idpembelian);
        }
     
        $this->db->where(array("#_pembayaran_utang_master.status !="=>2,"#_pembayaran_utang_master.pembayaran_utang_id !="=>$id));
        $this->db->group_by("id_pembelian");
        $listpembayaran=$this->db->get()->result();
        $listnominal=[];
        foreach($listpembayaran as $pem)
        {
            $listnominal[$pem->id_pembelian]=$pem->terbayar;
        }
       
        foreach($detail as $key=>$det)
        {    
             $detail[$key]['terbayar']= DefaultCurrencyDatabase($listnominal[$det['id_pembelian']]);
             $detail[$key]['sisa']=$detail[$key]['grandtotal']-$detail[$key]['terbayar'];
        }
        $data->detail = $detail;

        return $data;
    }
    

    /*
    Backup 
    function GetOnePembayaranHutang($id) {
        $this->load->model("kpu/m_kpu");
        $this->db->where($this->table . '.pembayaran_utang_id', $id);
        $this->db->join('#_supplier', '#_supplier.id_supplier=' . $this->table . '.id_supplier', 'left');
        $this->db->join('#_bank', '#_bank.id_bank=' . $this->table . '.id_bank', 'left');
        $data = $this->db->get($this->table)->row();

        $this->db->where('#_pembayaran_utang_detail.pembayaran_utang_id', $id);
        $this->db->join('#_kpu', '#_kpu.id_kpu=#_pembayaran_utang_detail.id_kpu', 'left');
        $this->db->join('#_unit_serial', '#_unit_serial.id_unit_serial=#_pembayaran_utang_detail.id_unit_serial', 'left');
        $this->db->join('#_kpu_detail', '#_kpu_detail.id_kpu_detail=#_unit_serial.id_kpu_detail', 'left');
        $this->db->join('#_buka_jual', '#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual', 'left');
        $this->db->join('#_prospek', '#_prospek.id_prospek=#_buka_jual.id_prospek', 'left');
        $detail = $this->db->select('#_pembayaran_utang_detail.*,no_prospek,#_kpu_detail.*,vin_number,engine_no,invoice_number,(#_kpu_detail.subtotal/#_kpu_detail.qty) as harga_jual_unit,#_kpu.nomor_master as nomor_kpu')->get('#_pembayaran_utang_detail')->result();
        $this->load->model("unit/m_unit");
        foreach($detail as $det)
        {
            $objdb=$this->m_unit->GetOneUnitSerialUnitPembelian($det->id_unit_serial);
            $det->terbayar=$objdb->terbayar;
            $det->sisa=$objdb->sisa;
        }
        
        $data->detail = $detail;

      

        return $data;
    }
    
     */

}
