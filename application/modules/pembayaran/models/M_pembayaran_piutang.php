<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pembayaran_piutang extends CI_Model {

    public $table = '#_pembayaran_piutang_master';
    public $id = 'id_pembayaran_piutang_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;
    private $_sufix = "_piutang";
    function __construct() {
        parent::__construct();
    }

    function GetDataPembayaranPiutang($search) {
        
        $listid=[];
        if(!CheckEmpty($search['pencarian_keyword'])&&!CheckEmpty($search['keyword'])){
            $searchby=$search['pencarian_keyword'];
            $this->db->from("#_pembayaran_piutang_detail");
            $this->db->join("#_pembayaran_piutang_master","#_pembayaran_piutang_master.pembayaran_piutang_id=#_pembayaran_piutang_detail.pembayaran_piutang_id","left");
            $this->db->join("#_unit_serial","#_unit_serial.id_unit_serial=#_pembayaran_piutang_detail.id_unit_serial","left");
            $this->db->join("#_prospek","#_prospek.id_prospek=#_pembayaran_piutang_detail.id_prospek","left");
            $this->db->join("#_buka_jual","#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual","left");
            $this->db->where("lower(".$searchby.")", strtolower($search['keyword']));
            $this->db->distinct();
            $listid=$this->db->select("pembayaran_piutang_detail_id")->get()->result_array();
            
            $listid= array_column($listid, "pembayaran_piutang_detail_id");
            
            if(count($listid)==0)
            {
                $listid[]=-1;
            }
            
            
        }

        $isedit = CekModule("K109", false);
        $isdelete = CekModule("K109", false);
        $this->load->library('datatables');
        $this->datatables->select('#_pembayaran_piutang_master.tanggal_transaksi,#_pembayaran_piutang_master.total_bayar,#_pembayaran_piutang_master.jenis_pembayaran,#_pembayaran_piutang_master.dokumen,#_pembayaran_piutang_master.pembayaran_piutang_id,nama_customer,replace(#_pembayaran_piutang_master.dokumen,"/","-") as nomor_master_link,group_concat(distinct #_prospek.no_prospek) as nomor_invoice,#_pembayaran_piutang_master.status,#_pembayaran_piutang_master.simpanan');
        $this->datatables->join("#_customer", "#_customer.id_customer=#_pembayaran_piutang_master.id_customer", "left");
        $this->datatables->join("#_pembayaran_piutang_detail", "#_pembayaran_piutang_detail.pembayaran_piutang_id=#_pembayaran_piutang_master.pembayaran_piutang_id", "left");
        $this->datatables->join("#_prospek", "#_prospek.id_prospek=#_pembayaran_piutang_detail.id_prospek", "left");

        $this->datatables->from('#_pembayaran_piutang_master');
        $straction = '';
        $extra = array();
        $params = array();
        $this->datatables->group_by("#_pembayaran_piutang_master.pembayaran_piutang_id");
     
        if (count($listid) > 0) {
            $this->datatables->where_in("pembayaran_piutang_detail_id", $listid);
        } else {
            if (isset($search['id_customer']) && !CheckEmpty($search['id_customer'])) {
                $params['#_pembayaran_piutang_master.id_customer'] = $search['id_customer'];
            }
            
           
            if (isset($search['status']) && !CheckEmpty($search['status'])) {
                $params['#_pembayaran_piutang_master.status'] = $search['status'];
            }
             
            $tanggal = "date(#_prospek.tanggal_prospek)";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_pembayaran_piutang_master.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_pembayaran_piutang_master.updated_date)";
            } else if ($params['jenis_pencarian'] == "tanggal") {
                $tanggal = "#_pembayaran_piutang_master.tanggal_transaksi";
            }
            $this->db->order_by($tanggal, "desc");


            if (isset($search['start_date'], $search['end_date']) && !empty($search['start_date']) && !empty($search['end_date'])) {
                array_push($extra, "#_pembayaran_piutang_master.tanggal_transaksi BETWEEN '" . DefaultTanggalDatabase($search['start_date']) . "' AND '" . DefaultTanggalDatabase($search['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else if (isset($search['start_date']) && empty($search['end_date'])) {
                $params['#_pembayaran_piutang_master.tanggal_transaksi'] = DefaultTanggalDatabase($search['start_date']);
                unset($params['start_date'], $params['end_date']);
            } else if (isset($search['end_date']) && empty($search['start_date'])) {
                $params['#_pembayaran_piutang_master.tanggal_transaksi'] = DefaultTanggalDatabase($search['end_date']);
                unset($params['start_date'], $params['end_date']);
            }
        }





        if (count($params)) {
            $this->datatables->where($params);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        $stredit = '';
        $strdelete = '';
        $strview = anchor(site_url('pembayaran/piutang/pembayaran_piutang_view/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $stredit = anchor("pembayaran/piutang/editpiutang/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strdelete .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        
        
        
        $this->datatables->add_column('action_view', $strview, 'pembayaran_piutang_id');
        $this->datatables->add_column('action_edit', $stredit, 'pembayaran_piutang_id,nomor_master_link');
        $this->datatables->add_column('action_delete', $strdelete, 'pembayaran_piutang_id,nomor_master_link');
        return $this->datatables->generate();
    }

    function GenerateAkunPembayaranPiutang($id_pembayaranpiutang) {

        $userid = GetUserId();
        $this->load->model("gl_config/m_gl_config");
        $this->load->model("customer/m_customer");
        $this->load->model("prospek/m_prospek");
        $this->load->model("buka_jual/m_buka_jual");
        $pembayaran = $this->GetOnePembayaranPiutang($id_pembayaranpiutang);
        $invoice="";
        $bank=null;
        $listidprospek=[];
        
        if ($pembayaran != null) {
            
            $this->load->model("bank/m_bank");
            $bank=$this->m_bank->GetOneBank($pembayaran->id_bank);
         
            foreach($pembayaran->detail as $det)
            {
                $invoice.=$det->no_prospek.' ,';
                if(!CheckKey($listidprospek, $det->id_prospek))
                {
                    $listidprospek[$det->id_prospek]=$det->id_prospek;
                }
                
            }
          
            $dokumenpembayaran = $pembayaran->dokumen;
            $akunglkas = $this->m_gl_config->GetIdGlConfig("kasbesar", $pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglkas, "dokumen" => $pembayaran->dokumen));
            $akun_kas = $this->db->get()->row();
            $arrglakun = [];
           
            if (CheckEmpty($akun_kas) && $pembayaran->jenis_pembayaran == "Cash") {
                $akunkasinsert = array();
                $akunkasinsert['id_gl_account'] = $akunglkas;
                $akunkasinsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunkasinsert['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunkasinsert['dokumen'] = $dokumenpembayaran;
                $akunkasinsert['subject_name'] = $pembayaran->kode_customer;
                $akunkasinsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunkasinsert['debet'] = $pembayaran->total_bayar + $pembayaran->simpanan ;
                $akunkasinsert['kredit'] = 0;
                $akunkasinsert['created_date'] = GetDateNow();
                $akunkasinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunkasinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_kas) && $pembayaran->jenis_pembayaran == "Cash") {
                $akunkasupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunkasupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunkasupdate['dokumen'] = $dokumenpembayaran;
                $akunkasupdate['subject_name'] = $pembayaran->kode_customer;
                $akunkasupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunkasupdate['debet'] = $pembayaran->total_bayar + $pembayaran->simpanan ;
                $akunkasupdate['kredit'] = 0;
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
                $arrglakun[] = $akun_kas->id_buku_besar;
            } else if (!CheckEmpty($akun_kas) && $pembayaran->jenis_pembayaran != "Cash") {
                $akunkasupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunkasupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunkasupdate['dokumen'] = $dokumenpembayaran;
                $akunkasupdate['subject_name'] = $pembayaran->kode_customer;
                $akunkasupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunkasupdate['debet'] = 0;
                $akunkasupdate['kredit'] = 0;
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
                $arrglakun[] = $akun_kas->id_buku_besar;
            }

            $akungldp = $this->m_gl_config->GetIdGlConfig("dp", $pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akungldp, "dokumen" => $dokumenpembayaran));
            $akun_dp = $this->db->get()->row();
            if (CheckEmpty($akun_dp) && $pembayaran->jenis_pembayaran == "DP") {
                $akundpinsert = array();
                $akundpinsert['id_gl_account'] = $akungldp;
                $akundpinsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akundpinsert['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akundpinsert['dokumen'] = $dokumenpembayaran;
                $akundpinsert['subject_name'] = $pembayaran->kode_customer;
                $akundpinsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akundpinsert['debet'] = $pembayaran->total_bayar + $pembayaran->simpanan ;
                $akundpinsert['kredit'] = 0;
                $akundpinsert['created_date'] = GetDateNow();
                $akundpinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akundpinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_dp) && $pembayaran->jenis_pembayaran == "DP") {
                $akundpupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akundpupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akundpupdate['dokumen'] = $dokumenpembayaran;
                $akundpupdate['subject_name'] = $pembayaran->kode_customer;
                $akundpupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akundpupdate['debet'] = $pembayaran->total_bayar + $pembayaran->simpanan ;
                $akundpupdate['kredit'] = 0;
                $akundpupdate['updated_date'] = GetDateNow();
                $akundpupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akundpupdate, array("id_buku_besar" => $akun_dp->id_buku_besar));
                $arrglakun[] = $akun_dp->id_buku_besar;
            } else if (!CheckEmpty($akun_dp) && $pembayaran->jenis_pembayaran != "DP") {
                $akundpupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akundpupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akundpupdate['dokumen'] = $dokumenpembayaran;
                $akundpupdate['subject_name'] = $pembayaran->kode_customer;
                $akundpupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akundpupdate['debet'] = 0;
                $akundpupdate['kredit'] = 0;
                $akundpupdate['updated_date'] = GetDateNow();
                $akundpupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_dp->id_buku_besar));
                $arrglakun[] = $akun_dp->id_buku_besar;
            }





            $akunglbank = $this->m_gl_config->GetIdGlConfig("bank", $pembayaran->id_cabang, array("bank" => $pembayaran->id_bank));
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglbank, "dokumen" => $dokumenpembayaran));
            $akun_bank = $this->db->get()->row();

            if (CheckEmpty($akun_bank) && $pembayaran->jenis_pembayaran == "Transfer") {
                $akunbankinsert = array();
                $akunbankinsert['id_gl_account'] = $akunglbank;
                $akunbankinsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                // $akunbankinsert['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunbankinsert['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunbankinsert['dokumen'] = $dokumenpembayaran;
                $akunbankinsert['subject_name'] = @$bank->kode_bank;
                $akunbankinsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_bank);
                $akunbankinsert['debet'] = $pembayaran->total_bayar + $pembayaran->simpanan ;
                $akunbankinsert['kredit'] = 0;
                $akunbankinsert['created_date'] = GetDateNow();
                $akunbankinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunbankinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_bank) && $pembayaran->jenis_pembayaran == "Transfer") {
                $akunbankupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                // $akunbankupdate['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunbankupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunbankupdate['dokumen'] = $dokumenpembayaran;
                $akunbankupdate['subject_name'] = @$bank->kode_bank;
                $akunbankupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_bank);
                $akunbankupdate['debet'] = $pembayaran->total_bayar + $pembayaran->simpanan ;
                $akunbankupdate['kredit'] = 0;
                $akunbankupdate['updated_date'] = GetDateNow();
                $akunbankupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunbankupdate, array("id_buku_besar" => $akun_bank->id_buku_besar));
                $arrglakun[] = $akun_bank->id_buku_besar;
            } else if (!CheckEmpty($akun_bank) && ($pembayaran->jenis_pembayaran == "Transfer" )) {
                $akunbankupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                // $akunbankupdate['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunbankupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunbankupdate['dokumen'] = $dokumenpembayaran;
                $akunbankupdate['subject_name'] = @$bank->kode_bank;
                $akunbankupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_bank);
                $akunbankupdate['debet'] = 0;
                $akunbankupdate['kredit'] = 0;
                $akunbankupdate['updated_date'] = GetDateNow();
                $akunbankupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunbankupdate, array("id_buku_besar" => $akun_bank->id_buku_besar));
                $arrglakun[] = $akun_bank->id_buku_besar;
            }
            $akunpotonganpembayaran = $this->m_gl_config->GetIdGlConfig("potonganpembayaran", $pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpotonganpembayaran, "dokumen" => $dokumenpembayaran));
            $akun_potongan = $this->db->get()->row();

            if (CheckEmpty($akun_potongan) && $pembayaran->jenis_pembayaran == "Potongan") {
                $akunpotonganinsert = array();
                $akunpotonganinsert['id_gl_account'] = $akunpotonganpembayaran;
                $akunpotonganinsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunpotonganinsert['keterangan'] = "Potongan Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunpotonganinsert['dokumen'] = $dokumenpembayaran;
                $akunpotonganinsert['subject_name'] = $pembayaran->kode_customer;
                $akunpotonganinsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunpotonganinsert['debet'] = $pembayaran->total_bayar + $pembayaran->simpanan ;
                $akunpotonganinsert['kredit'] = 0;
                $akunpotonganinsert['created_date'] = GetDateNow();
                $akunpotonganinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpotonganinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_potongan) && $pembayaran->jenis_pembayaran == "Potongan") {
                $akunpotonganupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunpotonganupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunpotonganupdate['dokumen'] = $dokumenpembayaran;
                $akunpotonganupdate['subject_name'] = $pembayaran->kode_customer;
                $akunpotonganupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunpotonganupdate['debet'] = $pembayaran->total_bayar + $pembayaran->simpanan ;
                $akunpotonganupdate['kredit'] = 0;
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
                $arrglakun[] = $akun_potongan->id_buku_besar;
            } else if (!CheckEmpty($akun_potongan) && $pembayaran->jenis_pembayaran != "Potongan") {
                $akunpotonganupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunpotonganupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunpotonganupdate['dokumen'] = $dokumenpembayaran;
                $akunpotonganupdate['subject_name'] = $pembayaran->kode_customer;
                $akunpotonganupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunpotonganupdate['debet'] = 0;
                $akunpotonganupdate['kredit'] = 0;
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
                $arrglakun[] = $akun_potongan->id_buku_besar;
            }





            $akunpiutang = $this->m_gl_config->GetIdGlConfig("piutang", $pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpiutang, "dokumen" => $dokumenpembayaran));
            $akun_piutang = $this->db->get()->row();

            if (CheckEmpty($akun_piutang)) {
                $akunpiutanginsert = array();
                $akunpiutanginsert['id_gl_account'] = $akunpiutang;
                $akunpiutanginsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunpiutanginsert['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunpiutanginsert['dokumen'] = $dokumenpembayaran;
                $akunpiutanginsert['subject_name'] = $pembayaran->kode_customer;
                $akunpiutanginsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunpiutanginsert['debet'] = 0;
                $akunpiutanginsert['kredit'] = $pembayaran->total_bayar ;
                $akunpiutanginsert['created_date'] = GetDateNow();
                $akunpiutanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpiutanginsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_piutang)) {
                $akunpiutangupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunpiutangupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunpiutangupdate['dokumen'] = $dokumenpembayaran;
                $akunpiutangupdate['subject_name'] = $pembayaran->kode_customer;
                $akunpiutangupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunpiutangupdate['debet'] = 0;
                $akunpiutangupdate['kredit'] = $pembayaran->total_bayar ;
                $akunpiutangupdate['updated_date'] = GetDateNow();
                $akunpiutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpiutangupdate, array("id_buku_besar" => $akun_piutang->id_buku_besar));
                $arrglakun[] = $akun_piutang->id_buku_besar;
            }

            $akunglutang = $this->m_gl_config->GetIdGlConfig("hutangcustomer", $pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglutang, "dokumen" => $dokumenpembayaran));
            $akun_utang = $this->db->get()->row();

            if (CheckEmpty($akun_utang) && $pembayaran->simpanan > 0) {
                $akunutanginsert = array();
                $akunutanginsert['id_gl_account'] = $akunglutang;
                $akunutanginsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunutanginsert['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunutanginsert['dokumen'] = $dokumenpembayaran;
                $akunutanginsert['subject_name'] = $pembayaran->kode_customer;
                $akunutanginsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunutanginsert['debet'] = 0;
                $akunutanginsert['kredit'] = $pembayaran->simpanan;
                $akunutanginsert['created_date'] = GetDateNow();
                $akunutanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunutanginsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_utang)) {
                $akunutangupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akunutangupdate['keterangan'] = "Pembayaran  " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akunutangupdate['dokumen'] = $dokumenpembayaran;
                $akunutangupdate['subject_name'] = $pembayaran->kode_customer;
                $akunutangupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akunutangupdate['debet'] = 0;
                $akunutangupdate['kredit'] = $pembayaran->simpanan;
                $akunutangupdate['updated_date'] = GetDateNow();
                $akunutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunutangupdate, array("id_buku_besar" => $akun_utang->id_buku_besar));
                $arrglakun[] = $akun_utang->id_buku_besar;
            }




            $this->db->where(array("dokumen" => $dokumenpembayaran));
            if (count($arrglakun) > 0) {
                $this->db->where_not_in("id_buku_besar", $arrglakun);
            }
            $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)));



            $akunglpenyeimbang = $this->m_gl_config->GetIdGlConfig("penyeimbang", $pembayaran->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("dokumen" => $dokumenpembayaran, "id_gl_account !=" => $akunglpenyeimbang));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglpenyeimbang, "dokumen" => $dokumenpembayaran));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = $akunglpenyeimbang;
                $akun_penyeimbanginsert['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk pembayaran Ke " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akun_penyeimbanginsert['dokumen'] = $dokumenpembayaran;
                $akun_penyeimbanginsert['subject_name'] = $pembayaran->kode_customer;
                $akun_penyeimbanginsert['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akun_penyeimbanginsert);
            } else if (!CheckEmpty($akun_penyeimbang) && $row->selisih != 0) {
                $akun_penyeimbangupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk pembayaran Ke " . $pembayaran->nama_customer . ' untuk invoice : ' . $invoice;
                $akun_penyeimbangupdate['dokumen'] = $dokumenpembayaran;
                $akun_penyeimbangupdate['subject_name'] = $pembayaran->kode_customer;
                $akun_penyeimbangupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $pembayaran->tanggal_transaksi;
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk pembayaran Ke " . $pembayaran->nama_customer . ' untuk bps : ' . $invoice;
                $akun_penyeimbangupdate['dokumen'] = $dokumenpembayaran;
                $akun_penyeimbangupdate['subject_name'] = $pembayaran->kode_customer;
                $akun_penyeimbangupdate['id_subject'] = ForeignKeyFromDb($pembayaran->id_customer);
                $akun_penyeimbangupdate['debet'] = 0;
                $akun_penyeimbangupdate['kredit'] = 0;
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            }

           
            
            foreach ($pembayaran->detail as $key => $det) {
                $this->m_prospek->UpdateStatusProspek($det->id_prospek);
                $this->m_buka_jual->UpdateStatusBukaJual($det->id_buka_jual);
            }
            
            $this->m_customer->KoreksiSaldoPiutang($pembayaran->id_customer,null,true);
        }
    }

    function PembayaranPiutang($model) {

        try {
            
            $jenis_pencarian="created_date";

            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $prospek = array();
            $this->load->model("customer/m_customer");
            $this->load->model("unit/m_unit");
            $this->load->model("bank/m_bank");
            $this->load->model("prospek/m_prospek");
            $this->load->model("buka_jual/m_buka_jual");

            $customer = $this->m_customer->GetOneCustomer($model['id_customer'], 'id_customer', true);
            $bank = $this->m_bank->GetOneBank(@$model['id_bank']);
            $userid = GetUserId();
            $pph = 0;
            $totalpembayaran = 0;
            $jumlah_bayar = DefaultCurrencyDatabase(@$model['jumlah_bayar_total']);
            
            $jumlah_bayarawal = $jumlah_bayar;
            $simpanan = DefaultCurrencyDatabase(@$model['simpanan']);
            $jumlah_bayar-=$simpanan;
            $totalbiaya = $jumlah_bayar + DefaultCurrencyDatabase(@$model['potongan']) - DefaultCurrencyDatabase(@$model['biaya']);
            $listdbpembayaran = [];
            $bpsno = "";
            $idprospek = [];
            $idbukajual = [];
                
            foreach ($model['dataset']as $detail) {
                $unit = $this->m_unit->GetOneUnitSerial($detail['id_unit_serial_pembayaran']);
               
                $pembayaranpiutangdetail=null;
                if(!CheckEmpty($detail['pembayaran_piutang_detail_id']))
                {
                    $this->db->from("#_pembayaran_piutang_detail");
                    $this->db->where(array("pembayaran_piutang_detail_id"=>$detail['pembayaran_piutang_detail_id']));
                    $pembayaranpiutangdetail=$this->db->get()->row();
                }
                if($pembayaranpiutangdetail)
                {
                    $unit->sisa+=$pembayaranpiutangdetail->jumlah_bayar;
                }
                if ($unit) {
                    $totalpembayaran += $unit->sisa;
                }
                if ($detail['jumlah_bayar'] > $unit->sisa) {
                    $unit->jumlah_pembayaran = $unit->sisa;
                    $simpanan += ($detail['jumlah_bayar'] - $unit->sisa);
                } else {
                    $unit->jumlah_pembayaran = $detail['jumlah_bayar'];
                }
                $listdbpembayaran[] = $unit;
            }
             if ($totalpembayaran < $totalbiaya) {
                $jumlah_bayar = $totalpembayaran;
            }
            
           
            $pembayaranmaster = array();
            $pembayaranmaster['tanggal_transaksi'] = DefaultTanggalDatabase($model['tanggal']);
            $pembayaranmaster['id_customer'] = $model['id_customer'];
            $pembayaranmaster['jenis_pembayaran'] = $model['jenis_pembayaran'];
            $pembayaranmaster['total_bayar'] = DefaultCurrencyDatabase($jumlah_bayar);
            $pembayaranmaster['biaya'] = DefaultCurrencyDatabase($model['biaya']);
            $pembayaranmaster['potongan'] = DefaultCurrencyDatabase($model['potongan']);
            $pembayaranmaster['status'] = 1;
            $pembayaranmaster['id_bank'] = ForeignKeyFromDb(@$model['id_bank']);
            $pembayaranmaster['keterangan'] = "";
            $pembayaranmaster['created_date'] = GetDateNow();
            $pembayaranmaster['created_by'] = $userid;
            $pembayaranmaster['simpanan'] = $simpanan;
            
            $dokumenpembayaran="";
            $pembayarandb=null;
            $pembayaran_piutang_id=0;
            if(CheckEmpty(@$model['pembayaran_piutang_id']))
            {
                $dokumenpembayaran = AutoIncrement('#_pembayaran_piutang_master', 'BJ/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                $this->db->insert("#_pembayaran_piutang_master", $pembayaranmaster);
                $pembayaran_piutang_id = $this->db->insert_id();
            }
            else
            {
                $jenis_pencarian="updated_date";
                $this->db->from("#_pembayaran_piutang_master");
                $this->db->where(array("pembayaran_piutang_id"=>$model['pembayaran_piutang_id']));
                $pembayarandb=$this->db->get()->row();
                if($pembayarandb)
                {
                    $dokumenpembayaran=$pembayarandb->dokumen;
                    $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                    $pembayaran_piutang_id = $pembayarandb->pembayaran_piutang_id;
                    $this->db->update("#_pembayaran_piutang_master", $pembayaranmaster,array("pembayaran_piutang_id"=>$pembayarandb->pembayaran_piutang_id));
                }
            }
            
            
            $invoice = "";
           foreach ($listdbpembayaran as $pembayaran) {
                $pembayarandetail = array();

                $pembayarandetail['jumlah_bayar'] = $pembayaran->jumlah_pembayaran;
                $this->db->from("#_pembayaran_piutang_detail");
                $this->db->where(array("pembayaran_piutang_id" => $pembayaran_piutang_id, "id_unit_serial" => $pembayaran->id_unit_serial));
                $dbdet = $this->db->get()->row();
             
                if ($dbdet) {
                    $this->db->update("#_pembayaran_piutang_detail", $pembayarandetail, array("pembayaran_piutang_detail_id"=>$dbdet->pembayaran_piutang_detail_id));
                } else {
                    $pembayarandetail['id_prospek'] = $pembayaran->id_prospek;
                    $pembayarandetail['id_buka_jual'] = $pembayaran->id_buka_jual;
                    $pembayarandetail['pembayaran_piutang_id'] = $pembayaran_piutang_id;
                    $pembayarandetail['created_date'] = GetDateNow();
                    $pembayarandetail['created_by'] = $userid;
                    $pembayarandetail['id_unit_serial'] = $pembayaran->id_unit_serial;

                    $this->db->insert("#_pembayaran_piutang_detail", $pembayarandetail);
                }


                $jumlah_bayar -= $pembayaran->sisa;
                if (!CheckKey($idprospek, $pembayaran->id_prospek)) {
                    $invoice .= $pembayaran->no_prospek . ' , ';
                }
                $idprospek[$pembayaran->id_prospek] += $pembayarandetail['jumlah_bayar'];
                $idbukajual[$pembayaran->id_buka_jual] += $pembayarandetail['jumlah_bayar'];
            }
           
            
            $this->GenerateAkunPembayaranPiutang($pembayaran_piutang_id);
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                ClearCookiePrefix("_piutang",$jenis_pencarian);
                $this->db->trans_commit();
                SetMessageSession(1, "Data Pembayaran  Sudah di" . (CheckEmpty(@$model['pembayaran_piutang_id']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($pembayaran_piutang_id, $dokumenpembayaran, 'pembayaran');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function PiutangBatal($pembayaran_piutang_id) {
        $message = "";
        $pembayaran = $this->GetOnePembayaranPiutang($pembayaran_piutang_id, true);
        if (!$pembayaran) {
            $message .= "Pembayaran tidak terdapat dalam database";
        }

        $this->load->model("customer/m_customer");
        $this->load->model("prospek/m_prospek");
        $this->load->model("buka_jual/m_buka_jual");
        try {
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();

            $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)), array("dokumen" => $pembayaran->dokumen));
            $this->db->update("#_pembayaran_piutang_master", MergeUpdate(array("status" => 2)), array("pembayaran_piutang_id" => $pembayaran->pembayaran_piutang_id));
            foreach ($pembayaran->detail as $detail) {
                $this->m_prospek->UpdateStatusProspek($detail->id_prospek);
                $this->m_buka_jual->UpdateStatusBukaJual($detail->id_buka_jual);
            }
            $this->m_customer->KoreksiSaldoPiutang($pembayaran->id_customer);

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                $arrayreturn['msg'] = "Data Pembayaran Sudah di batalkan";
                $arrayreturn["st"] = true;
                //SetPrint($id_penjualan, $penjualan['nomor_master'], 'penjualan');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetOnePembayaranPiutang($id) {
        $this->db->where($this->table . '.pembayaran_piutang_id', $id);
        $this->db->join('#_customer', '#_customer.id_customer=' . $this->table . '.id_customer', 'left');
        $this->db->join('#_bank', '#_bank.id_bank=' . $this->table . '.id_bank', 'left');
        $data = $this->db->get($this->table)->row();

        $this->db->where('#_pembayaran_piutang_detail.pembayaran_piutang_id', $id);
        $this->db->join('#_prospek', '#_prospek.id_prospek=#_pembayaran_piutang_detail.id_prospek', 'left');
        $this->db->join('#_unit_serial', '#_unit_serial.id_unit_serial=#_pembayaran_piutang_detail.id_unit_serial', 'left');
        $this->db->join('#_buka_jual', '#_unit_serial.id_buka_jual=#_buka_jual.id_buka_jual', 'left');
        $this->db->join('#_prospek_detail', '#_prospek.id_prospek=#_prospek_detail.id_prospek', 'left');
        $detail = $this->db->select('#_pembayaran_piutang_detail.*,#_prospek_detail.*,nomor_buka_jual,vin_number,engine_no, #_prospek.no_prospek, #_prospek_detail.harga_jual_unit  as total, #_pembayaran_piutang_detail.jumlah_bayar')->get('#_pembayaran_piutang_detail')->result();
        $this->load->model("unit/m_unit");
        foreach($detail as $det)
        {
            $objdb=$this->m_unit->GetOneUnitSerial($det->id_unit_serial);
            $det->terbayar=$objdb->terbayar;
            $det->sisa=$objdb->sisa;
        }
        
        $data->detail = $detail;

        return $data;
    }

    function GetTotalPiutang($id_cabang=[],$id_pegawai=null) {
        
        if(count($id_cabang)>0)
        {
            $this->db->where_in("id_cabang",$id_cabang);
        }
        if(!CheckEmpty($id_pegawai))
        {
             $this->db->where_in("id_pegawai",$id_pegawai);
        }
        $this->db->select('SUM(sisa) as total_piutang');
        $res = $this->db->get('#_view_dokumen_sisa')->row();

        return $res->total_piutang;
    }

    function GetTotalPiutangBayar() {
        $this->db->where('tanggal_transaksi <=', date('Y-m-d'));
        $this->db->select('SUM(total_bayar) as total_bayar');
        $res = $this->db->get('dgmi_pembayaran_piutang_master')->row();

        return $res->total_bayar;
    }

}
