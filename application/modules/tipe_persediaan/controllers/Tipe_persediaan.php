<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tipe_persediaan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_tipe_persediaan');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K064";
        $header = "K001";
        $title = "Tipe Persediaan";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	CekModule($module);
	LoadTemplate($model, "tipe_persediaan/v_tipe_persediaan_index", $javascript);
        
    } 
    public function get_one_tipe_persediaan() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_tipe_persediaan', 'Tipe Persediaan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_tipe_persediaan->GetOneTipe_persediaan($model["id_tipe_persediaan"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Tipe Persediaan tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatatipe_persediaan() {
        header('Content-Type: application/json');
        echo $this->m_tipe_persediaan->GetDatatipe_persediaan();
    }
    
    public function create_tipe_persediaan() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	$module = "K064";
        $header = "K001";
        $row['title'] = "Add Tipe Persediaan";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	    $this->load->view('tipe_persediaan/v_tipe_persediaan_manipulate', $row);       
    }
    
    public function tipe_persediaan_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_tipe_persediaan', 'Kode Tipe Persediaan', 'trim|required');
	$this->form_validation->set_rules('nama_tipe_persediaan', 'Nama Tipe Persediaan', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_tipe_persediaan->Tipe_persediaanManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_tipe_persediaan($id=0) 
    {
        $row = $this->m_tipe_persediaan->GetOneTipe_persediaan($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	    $module = "K064";
            $header = "K001";
            $row['title'] = "Edit Tipe Persediaan";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    $this->load->view('tipe_persediaan/v_tipe_persediaan_manipulate', $row);
        } else {
            SetMessageSession(0, "Tipe_persediaan cannot be found in database");
            redirect(site_url('tipe_persediaan'));
        }
    }
    
    public function tipe_persediaan_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_tipe_persediaan', 'Tipe_persediaan', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_tipe_persediaan->Tipe_persediaanDelete($model['id_tipe_persediaan']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Tipe_persediaan.php */