
<section class="content-header">
    <h1>
        Tipe Persediaan
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Tipe_persediaan</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">

                <?php echo anchor("", 'Create', 'class="btn btn-success" id="btt_create"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    function deletetipe_persediaan(id_tipe_persediaan) {


        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/tipe_persediaan/tipe_persediaan_delete',
                    dataType: 'json',
                    data: {
                        id_tipe_persediaan: id_tipe_persediaan
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }
    function edittipe_persediaan(id_tipe_persediaan) {

        $.ajax({url: baseurl + 'index.php/tipe_persediaan/edit_tipe_persediaan/' + id_tipe_persediaan,
            success: function (data) {
                modalbootstrap(data, "Edit Tipe_persediaan");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    }
    $(document).ready(function () {
        $("#btt_create").click(function () {
            $.ajax({url: baseurl + 'index.php/tipe_persediaan/create_tipe_persediaan',
                success: function (data) {
                    modalbootstrap(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });


        })
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "tipe_persediaan/getdatatipe_persediaan", "type": "POST"},
            columns: [
                {
                    data: "id_tipe_persediaan",
                    title: "No",
                    orderable: false,
                    width: "10px",
                    classNmae: "text-right"
                },
                {data: "kode_tipe_persediaan", orderable: false, title: "Kode Tipe Persediaan"},
                {data: "nama_tipe_persediaan", orderable: false, title: "Nama Tipe Persediaan"},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        return data == 1 ? "Active" : "Not Active";
                    }},
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center",
                    "width": "100px"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
