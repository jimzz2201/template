<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_tipe_persediaan extends CI_Model
{

    public $table = '#_tipe_persediaan';
    public $id = 'id_tipe_persediaan';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatatipe_persediaan() {
        $this->load->library('datatables');
        $this->datatables->select('id_tipe_persediaan,kode_tipe_persediaan,nama_tipe_persediaan,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_tipe_persediaan.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "edittipe_persediaan($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletetipe_persediaan($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_tipe_persediaan');
        return $this->datatables->generate();
    }

    // get all
    function GetOneTipe_persediaan($keyword, $type = 'id_tipe_persediaan') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $tipe_persediaan = $this->db->get($this->table)->row();
        return $tipe_persediaan;
    }

    function Tipe_persediaanManipulate($model) {
        try {

            if (CheckEmpty($model['id_tipe_persediaan'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		return array("st" => true, "msg" => "Tipe_persediaan successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_tipe_persediaan" => $model['id_tipe_persediaan']));
		return array("st" => true, "msg" => "Tipe_persediaan has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownTipe_persediaan() {
        $listtipe_persediaan = GetTableData($this->table, 'id_tipe_persediaan', 'nama_tipe_persediaan', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listtipe_persediaan;
    }

    function Tipe_persediaanDelete($id_tipe_persediaan) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_tipe_persediaan' => $id_tipe_persediaan));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Tipe_persediaan has been deleted from database");
    }


}