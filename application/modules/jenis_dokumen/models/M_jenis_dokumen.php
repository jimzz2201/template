<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_jenis_dokumen extends CI_Model
{

    public $table = '#_jenis_dokumen';
    public $id = 'id_jenis_dokumen';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatajenis_dokumen() {
        $this->load->library('datatables');
        $this->datatables->select('id_jenis_dokumen,kode_jenis_dokumen,nama_jenis_dokumen,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_jenis_dokumen.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('jenis_dokumen/edit_jenis_dokumen/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletejenis_dokumen($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_jenis_dokumen');
        return $this->datatables->generate();
    }

    // get all
    function GetOneJenis_dokumen($keyword, $type = 'id_jenis_dokumen') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $jenis_dokumen = $this->db->get($this->table)->row();
        return $jenis_dokumen;
    }

    function Jenis_dokumenManipulate($model) {
        try {

            if (CheckEmpty($model['id_jenis_dokumen'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		SetMessageSession(1, 'Jenis_dokumen successfull added into database');
		return array("st" => true, "msg" => "Jenis_dokumen successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_jenis_dokumen" => $model['id_jenis_dokumen']));
		SetMessageSession(1, 'Jenis_dokumen has been updated');
		return array("st" => true, "msg" => "Jenis_dokumen has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownJenis_dokumen() {
        $listjenis_dokumen = GetTableData($this->table, 'id_jenis_dokumen', array('kode_jenis_dokumen', 'nama_jenis_dokumen'), array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listjenis_dokumen;
    }

    function Jenis_dokumenDelete($id_jenis_dokumen) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_jenis_dokumen' => $id_jenis_dokumen));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Jenis_dokumen has been deleted from database");
    }


}