<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_dokumen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_jenis_dokumen');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K073";
        $header = "K001";
        $title = "Jenis Dokumen";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	CekModule($module);
	LoadTemplate($model, "jenis_dokumen/v_jenis_dokumen_index", $javascript);
        
    } 
    public function get_one_jenis_dokumen() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_jenis_dokumen', 'Jenis Dokumen', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_jenis_dokumen->GetOneJenis_dokumen($model["id_jenis_dokumen"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Jenis Dokumen tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatajenis_dokumen() {
        header('Content-Type: application/json');
        echo $this->m_jenis_dokumen->GetDatajenis_dokumen();
    }
    
    public function create_jenis_dokumen() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	$module = "K073";
        $header = "K001";
        $row['title'] = "Add Jenis Dokumen";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	LoadTemplate($row,'jenis_dokumen/v_jenis_dokumen_manipulate', $javascript);       
    }
    
    public function jenis_dokumen_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_jenis_dokumen', 'Kode Jenis Dokumen', 'trim|required');
	$this->form_validation->set_rules('nama_jenis_dokumen', 'Nama Jenis Dokumen', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_jenis_dokumen->Jenis_dokumenManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_jenis_dokumen($id=0) 
    {
        $row = $this->m_jenis_dokumen->GetOneJenis_dokumen($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	    $module = "K073";
            $header = "K001";
            $row['title'] = "Edit Jenis Dokumen";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    LoadTemplate($row,'jenis_dokumen/v_jenis_dokumen_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Jenis_dokumen cannot be found in database");
            redirect(site_url('jenis_dokumen'));
        }
    }
    
    public function jenis_dokumen_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_jenis_dokumen', 'Jenis_dokumen', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_jenis_dokumen->Jenis_dokumenDelete($model['id_jenis_dokumen']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Jenis_dokumen.php */