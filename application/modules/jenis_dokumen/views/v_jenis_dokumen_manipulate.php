<section class="content-header">
    <h1>
        Jenis Dokumen <?= @$button ?>
        <small>Jenis Dokumen</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Jenis_dokumen</li>
        <li class="active">Jenis Dokumen <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_jenis_dokumen" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_jenis_dokumen" value="<?php echo @$id_jenis_dokumen; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Jenis Dokumen', "txt_kode_jenis_dokumen", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?php $mergearray=array();
                    if(!CheckEmpty(@$id_jenis_dokumen))
                    {
                        $mergearray['readonly']="readonly";
                        $mergearray['name'] = "kode_jenis_dokumen";
                    }
                    else
                    {
                        $mergearray['name'] = "kode_jenis_dokumen";
                    }?>
            <?= form_input(array_merge($mergearray,array('type' => 'text', 'value' => @$kode_jenis_dokumen, 'class' => 'form-control', 'id' => 'txt_kode_jenis_dokumen', 'placeholder' => 'Kode Jenis Dokumen'))); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Jenis Dokumen', "txt_nama_jenis_dokumen", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?= form_input(array('type' => 'text', 'name' => 'nama_jenis_dokumen', 'value' => @$nama_jenis_dokumen, 'class' => 'form-control', 'id' => 'txt_nama_jenis_dokumen', 'placeholder' => 'Nama Jenis Dokumen')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="form-group">
        <a href="<?php echo base_url().'index.php/jenis_dokumen' ?>" class="btn btn-default"  >Cancel</a>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
	</div>
</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
$("#frm_jenis_dokumen").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/jenis_dokumen/jenis_dokumen_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/jenis_dokumen';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>