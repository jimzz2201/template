<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo!CheckEmpty(@$title) ? @$title : "Dealer" ?></title>
        <script>
            var baseurl = "<?php echo base_url() ?>";
        </script>
        <!-- Tell 
             Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
	    <!--        <link rel="stylesheet" href="--><?php //echo base_url() ?><!--assets/bootstrap/css/bootstrap.min.css">-->
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
	    <!--<link rel="stylesheet" href="<?php /*echo base_url() */?>assets/font-awesome/css/font-awesome.min.css">-->
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
	    <script src="<?php echo base_url() ?>assets/js/helper.js?id=20"></script>
        <?php
        foreach (@$link as $item) {
            ?> 
            <link rel="<?=$item['rel']?>" href="<?php echo base_url() . $item['file'] ?>">
        <?php }
        ?>
        <?php
        foreach (@$css as $item) {
            ?>
            <link rel="stylesheet" href="<?php echo base_url() . $item ?>">
        <?php }
        ?>
        <!--<link rel="icon" type="image/ico" href="<?php echo base_url() ?>favicon.ico">-->
	    <style>
		    .spanlabel{
			    margin-top:3px;
			    margin-left:10px;
			    float:right;
			    display:block;
			
		    }
	    </style>
    </head>

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo base_url() ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">DL</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">DEALER</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                           
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url() . 'assets/images/profile/' . (@$profilpic == '' ? 'default_user.jpg' : @$profilpic) ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo @$nama_user_login ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url() . 'assets/images/profile/' . (@$profilpic == '' ? 'default_user.jpg' : @$profilpic) ?>" class="img-circle" alt="User Image">

                                       
                                    </li>
                                    <!-- Menu Body -->

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?php echo base_url() ?>index.php/main/change_password" class="btn btn-default btn-flat">Change Password</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url() ?>index.php/logon/logout" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
             <aside class="main-sidebar">
                <?php include APPPATH . 'modules/module/views/sidebar.php' ?>   
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">