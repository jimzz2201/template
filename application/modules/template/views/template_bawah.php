<div class="overlay hide">
    <div class="fillbackground" style="height:100%;width:100%;display:block">
        <img src="<?php echo base_url() ?>assets/images/loader.gif" style="width:10%;margin-top:20%;margin-left:-20%" />
    </div>
</div>
</div>
<div data-remodal-id="mymodal" role="dialog" aria-labelledby="modal2Title" aria-describedby="modal2Desc">
    <div>
        <p id="modaldesc">
        </p>
    </div>
</div>
<footer class="main-footer noprint">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2019 <a href="javascript:;">DGTI</a>.</strong> All rights
    reserved.
</footer>

</div>
<div id="modalbootstrap" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/helper.js?id=1"></script>-->
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" ></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js" ></script>
<?php
foreach (@$javascript as $item) {
    ?> 
    <script type="text/javascript" src="<?php echo base_url() . $item ?>" ></script>
<?php }
?>
<script>
    $.widget.bridge('uibutton', $.ui.button);
<?php
if (@$messagestatus != null && @$messagestatus != '5' && @$messagestatus != '' && @$message != null && @$message != '') {
    if (@$messagestatus == '2') {
        ?>
            messageerror('<?php echo $message ?>');
        <?php
    } else {
        ?>
            messagesuccess('<?php echo $message ?>');
        <?php
    }
}
if (!CheckEmpty(@$form)) {
    echo '$(".menu' . @$form . ' ul").addClass("menu-open");';
    echo '$(".menu' . @$form . '").addClass("active");';
    echo '$(".menu' . @$form . '").css("display","block");';
}
if (!CheckEmpty(@$formsubmenu)) {
    echo '$(".submenu' . @$formsubmenu . '").addClass("active");';
}

if (!CheckEmpty(@$currentmenu)) {

    echo '$(".' . @$currentmenu . '").addClass("active");';
}
?>
</script>
</body>
</html>
