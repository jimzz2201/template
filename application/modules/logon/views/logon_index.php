<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            .imglogo{
                background-color: #000;
                border-radius:50%;
                box-shadow: 0 0 0 5px #000;
                cursor:pointer;
                
            }
        </style>
        <title>Daya Guna Point Of Sales Management System</title>
        <meta charset="UTF-8">
        <script>
            var baseurl="<?php echo base_url()?>";
        </script>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/login/images/icons/favicon.ico"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/vendor/animate/animate.css">
        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/css/util.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/login/css/main.css">
        <!--===============================================================================================-->
    </head>
    <body>

        <div class="limiter">
            <div class="container-login100" style=" background: url('<?php echo base_url()?>assets/images/background.png');   background-repeat: no-repeat;
  background-attachment: fixed;
  background-size:100% 100%;">
                
                <div class="wrap-login100">
                    <div id="notification" style="width:100%;margin-bottom:100px;margin-top:-100px;"></div>
                    <div class="login100-pic js-tilt" data-tilt>
                        <img class="imglogo" src="<?php echo base_url() ?>assets/images/dgmilogo.png"  alt="IMG">
                    </div>

                    <form id="frmlogin" class="login100-form validate-form">
                        <span class="login100-form-title">
                            Member Login
                        </span>

                        <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                            <input  autocomplete="off" type="text" name="username" value="" class="input100"  placeholder="Username">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate = "Password is required">
                            <input autocomplete="off" type="password" name="password"  value="" class="input100" placeholder="Password">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="container-login100-form-btn">
                            <button type="submit" class="login100-form-btn">
                                Login
                            </button>
                        </div>

                        <div class="text-center p-t-12">

                        </div>

                        <div class="text-center p-t-136">

                        </div>
                    </form>
                </div>
            </div>
        </div>




        <!--===============================================================================================-->	
        <script src="<?php echo base_url() ?>assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="<?php echo base_url() ?>assets/login/vendor/bootstrap/js/popper.js"></script>
        <script src="<?php echo base_url() ?>assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>
        <!--===============================================================================================-->
        <script src="<?php echo base_url() ?>assets/login/vendor/select2/select2.min.js"></script>
        <!--===============================================================================================-->
        <script src="<?php echo base_url() ?>assets/login/vendor/tilt/tilt.jquery.min.js"></script>
         <script src="<?php echo base_url() ?>assets/js/helper.js"></script>
        <script >
            $('.js-tilt').tilt({
                scale: 1.1
            })
        </script>
        <!--===============================================================================================-->
        <script src="<?php echo base_url() ?>assets/login/js/main.js"></script>
        <script>
           

            $("#frmlogin").submit(function () {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/logon/login_validation',
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function (data) {
                       if (data.st)
                        {
<?php if ($this->input->get('url')): ?>
                                window.location.href = "<?= $this->input->get('url'); ?>";
<?php else: ?>
                                window.location.reload();
<?php endif; ?>
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });

                return false;
            });
        </script>
    </body>
</html>

