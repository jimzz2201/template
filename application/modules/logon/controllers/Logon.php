<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logon extends CI_Controller {

   
    public function index() {
     
        if(CheckEmpty(GetUserId()))
        {
            $model=array();
            $this->load->view("logon/logon_index",$model);
        }
        else {
            redirect('dashboard');
           
            $userid = GetUserId();
            $this->load->model("user/m_user");
            $model=$this->m_user->GetOneUser($userid,'id_user');
            $model = json_decode(json_encode($model), true);
            $model['title'] = "Dashboard";
            $model['openmenu']="dashboardmenu";
            LoadTemplate($model, "dashboard/dashboard_index", array());
        }
    }
    public function logout()
    {
        $this->session->sess_destroy();
        $cookies=$_COOKIE;
        foreach($cookies as $key=>$item)
        {
               setcookie($key, '', time()-1000,'/');
        }
      
        
        redirect(base_url());
    }
    public function logout_api()
    {
        $msg = "";
        $logout = $this->session->sess_destroy();
        if($logout) {
            $msg = "Log Out Success";
        }else{
            $msg = "Log Out failed";
        }

        echo json_encode(array('msg' => $msg));
    }
    public function login_validation() {
        $model = $this->input->post();
        $message = '';
        $data = array();
        $data['st'] = false;
       
        $this->form_validation->set_rules('username', "Username", 'required');
        $this->form_validation->set_rules('password', "Password", 'required');
        $this->load->model("user/m_user");
       
        if ($this->form_validation->run() === FALSE || $message != '') {
            
            $data['msg'] =  validation_errors() . $message;
        } else {
            $message = $this->m_user->login($model['username'], $model['password'],'username');
            
            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
        exit;
    }

    public function login_validation_api() {
        $model = $this->input->post();
        $message = '';
        $data = array();
        $data['st'] = false;
       
        $this->form_validation->set_rules('username', "Username", 'required');
        $this->form_validation->set_rules('password', "Password", 'required');
        $this->load->model("user/m_user");
       
        if ($this->form_validation->run() === FALSE || $message != '') {
            
            $data['msg'] =  validation_errors() . $message;
        } else {
            $data_login = $this->m_user->login_api($model['username'], $model['password'],'username');
            $message = $data_login['message'];
            if ($message == '') {
                $data_login = $data_login['data_login'];
                $data['api_token'] = 'd079f3c28daa9f9f47b834d03aaa9485';
                $data['data'] = array(
                                    'email' => '',
                                    'id_cabang' => $data_login->id_cabang,
                                    'id_jabatan' => '',
                                    'id_pegawai' => $data_login->id_pegawai,
                                    'id_user' => $data_login->id_user,
                                    'is_superadmin' => true,
                                    'name' => $data_login->nama_user,
                                    'username' => $data_login->username,
                );
                $data['expire_date'] = 1609906279;
                $data['notif'] = '';
                $data['permission'] = array('read-prospek', 'read-buka-jual', 'read-jadwal-kunjungan', 'create-jadwal-kunjungan', 'set-photos-pelanggans', 'set-location-pelanggans', 'create-prospek');
            }
            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
        exit;
    }

}
