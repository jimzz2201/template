<?php
$prefix = "";
$prefixlabel = "";
if ($key != 0) {
    $prefix = $key;
    $prefixlabel = "Ke " . $key;
}
?>
<div id="area<?php echo @$key ?>">
    <div class="form-group">
        <?= form_label('Contact Person ' . $prefixlabel, "txt_contact_person", array("class" => 'col-sm-2 control-label')); ?>
        <?= form_input(array('type' => 'hidden', 'name' => 'id_address[]', 'value' => $key == 0 ? 0 : @$address['id_address'])); ?>

        <div class="col-sm-1">
            <?= form_dropdown(array('type' => 'text', 'name' => 'id_jenis_customer[]', 'class' => 'form-control', 'id' => 'dd_jenis_customer'.$key, 'placeholder' => 'Jenis'), DefaultEmptyDropdown(@$list_jenis_customer, "json", "Jenis"), @$id_jenis_customer); ?>
        </div>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'name' => 'contact_person[]', 'value' => $key == 0 ? @$contact_person : @$address['contact_person'], 'class' => 'form-control', 'id' => 'txt_contact_person', 'placeholder' => 'Contact Person')); ?>
        </div>
        <?= form_label('No Telp ' . $prefixlabel, "txt_no_telp_2", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text', 'name' => 'contact_telp[]', 'value' => $key == 0 ? @$contact_telp : @$address['contact_telp'], 'class' => 'form-control', 'id' => 'txt_no_telp_2', 'placeholder' => 'No Telp 2')); ?>
        </div>
        <div class="col-sm-1">
            <?php if (@$key == 0) { ?>
                <button type="button" class="btn btn-block btn-primary" id="manageAlamat">Tambah</button>
            <?php } else { ?>
                <button type="button" class="btn btn-block btn-danger" onClick="deleteAlamat(<?php echo  @$key?>)" id="deletealamat">Delete</button>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Agama' . $prefixlabel, "txt_agama", array("class" => 'col-sm-2 control-label')); ?>

        <div class="col-sm-4">
            <?= form_dropdown(array('type' => 'text', 'name' => 'contact_agama[]', 'class' => 'form-control', 'id' => 'dd_contact_agama'.$key, 'placeholder' => 'Agama'), DefaultEmptyDropdown(@$list_agama, "json", "Agama"), $key == 0 ? @$contact_agama : @$address['contact_agama']); ?>
        </div>
        <?= form_label('Gender ' . $prefixlabel, "txt_gender", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-4">
            <?= form_dropdown(array('type' => 'text', 'name' => 'contact_gender[]', 'class' => 'form-control', 'id' => 'dd_contact_gender'.$key, 'placeholder' => 'Gender'), DefaultEmptyDropdown(@$list_gender, "json", "Gender"), $key == 0 ? @$contact_gender : @$address['contact_gender']); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('' . $prefixlabel, "txt_agama", array("class" => 'col-sm-2 control-label')); ?>

        <div class="col-sm-4"></div>
        <?= form_label('Birthdate ' . $prefixlabel, "txt_Birthday", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text', 'name' => 'contact_birthdate[]', 'value' => $key == 0 ? DefaultDatePicker(@$contact_birthdate) : DefaultDatePicker(@$address['contact_birthdate']), 'class' => 'form-control datepicker', 'id' => 'id_contact_birthdate'. $prefixlabel, 'placeholder' => 'Birthdate')); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Alamat ' . $prefixlabel, "txt_alamat", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-10">
            <?= form_textarea(array('type' => 'text', 'name' => 'contact_address[]', 'rows' => '3', 'cols' => '10', 'value' => $key == 0 ? @$contact_address : @$address['contact_address'], 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Keterangan ' . $prefixlabel, "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-10">
            <?= form_textarea(array('type' => 'text', 'name' => 'keterangan[]', 'rows' => '3', 'cols' => '10', 'value' => $key == 0 ? @$keterangan : @$address['keterangan'], 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
        </div>
    </div>
</div>
<hr/>