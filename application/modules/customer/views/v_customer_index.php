
<section class="content-header">
    <h1>
        Customer
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Customer</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'selected' => @$id_cabang, 'class' => 'form-control select2', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                    </div>
                </div>
            </form>
            <hr/>
            <div class=" headerbutton">

                <?php
                if (CekModule("K113", false)) {
                    echo anchor(site_url('customer/create_customer'), 'Create', 'class="btn btn-success"');
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    function deletecustomer(id_customer) {


        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/customer/customer_delete',
                    dataType: 'json',
                    data: {
                        id_customer: id_customer
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }
    function RefreshGrid()
    {
        table.fnDraw(false);
    }
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $("#btt_Eksport").click(function(){
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/customer/export?'+search;
         window.open(url);
    })
    $(document).ready(function () {
        $("select").select2();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            sortable: false,
            ajax: {"url": "customer/getdatacustomer", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                // {data: "id_tipe_penyewa" , orderable:false , title :"Id Tipe Penyewa"},
                {data: "kode_customer", orderable: false, title: "Kode"},
                {data: "nama_customer", orderable: false, title: "Nama"},
                {data: "nama_group_customer", orderable: false, title: "Group"},
                {data: "npwp", orderable: false, title: "Npwp"},
                {data: "no_telp", orderable: false, title: "No Telp"},
                {data: "email", orderable: false, title: "Email"},
                {data: "contact_person", orderable: false, title: "Contact&nbsp;Person"},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        return data == 1 ? "Active" : "Not Active";
                    }},
                {data: "nama_cabang", orderable: false, title: "Cabang"},
                {
                    "data": "action",
                    width: "150px",
                    title: "&nbsp;",
                    "orderable": false,
                    "className": "text-center",
                    mRender: function (data, type, row) {
                        return row['view'] + data;
                    }},
            ],
            order: [[0, 'desc']]
        });
    });
</script>
