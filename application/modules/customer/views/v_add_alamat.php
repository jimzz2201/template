<div class="modal-header">
        Alamat <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_alamat" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_alamat" value="<?php echo @$id_alamat; ?>" /> 
	<input type="hidden" name="id_customer" value="<?php echo @$id_customer; ?>" /> 
	<div class="form-group">
		<?= form_label('Nama Alamat', "txt_nama_alamat", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_alamat', 'value' => @$nama_alamat, 'class' => 'form-control', 'id' => 'txt_nama_alamat', 'placeholder' => 'Nama Alamat')); ?>
		</div>
	</div>
    <div class="form-group">
		<?= form_label('Alamat', "txt_alamat", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
            <?= form_textarea(array('type' => 'text', 'name' => 'alamat', 'rows' => '3', 'cols' => '10', 'value' => @$alamat, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
		</div>
	</div>
    <div class="form-group">
		<?= form_label('Contact Person', "txt_cp", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'contact_person', 'value' => @$contact_person, 'class' => 'form-control', 'id' => 'txt_contact_person', 'placeholder' => 'Contact Person')); ?>
		</div>
	</div>
    <div class="form-group">
		<?= form_label('Telp', "txt_telp_cp", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'telp_cp', 'value' => @$telp_cp, 'class' => 'form-control', 'id' => 'txt_telp_cp', 'placeholder' => 'Telp Contact Person')); ?>
		</div>
	</div>
    <div class="form-group">
		<?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
            <?= form_textarea(array('type' => 'text', 'name' => 'keterangan', 'rows' => '3', 'cols' => '10', 'value' => @$keterangan, 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
    $("#frm_alamat").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/customer/alamat_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    // messagesuccess(data.msg);
                    window.parent.refreshAlamat();
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;
    });
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>