<div class="modal-header" style="color:#fff">
    <?php echo @$row->contact_person.' - '.$row->nama_customer; ?>
</div>
<div class="modal-body">
    <form id="frm_jadwal_kunjungan" class="form-horizontal form-groups-bordered validate" method="post" autocomplete="off">
       
        <div class="form-group" style="<?php echo $button == 'Add' ? 'display:none' : '' ?>">
            <?= form_label('Perusahaan', "txt_start", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?= form_input(array('type' => 'text',  'class' => 'form-control', 'id' => 'txt_nama_perusahaan',"disabled"=>"disable", 'value' => @$row->nama_customer, 'placeholder' => 'Customer')); ?>
            </div>

        </div>
        <div class="form-group">
            <?= form_label('Contact', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
             <div class="col-sm-10">
                <?= form_input(array('type' => 'text',  'class' => 'form-control', 'id' => 'txt_contact_person',"disabled"=>"disable", 'value' => @$row->contact_person, 'placeholder' => 'Contact Person')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Telp', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
             <div class="col-sm-4">
                <?= form_input(array('type' => 'text',  'class' => 'form-control', 'id' => 'txt_contact_telp',"disabled"=>"disable", 'value' => @$row->contact_telp, 'placeholder' => 'Telp')); ?>
            </div>
            
        </div>
        <div class="form-group">
            <?= form_label('Agama', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
             <div class="col-sm-4">
                <?= form_input(array('type' => 'text',  'class' => 'form-control', 'id' => 'txt_agama',"disabled"=>"disable", 'value' =>CheckEmpty(@$row->contact_agama)?"":@$row->contact_agama, 'placeholder' => 'Agama')); ?>
            </div>
            <?= form_label('Gender', "txt_status", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'text',  'class' => 'form-control', 'id' => 'txt_gender',"disabled"=>"disable", 'value' => CheckEmpty(@$row->contact_gender)?"":@$row->contact_gender, 'placeholder' => 'Gender')); ?>
            </div>
        </div>
        <div class="form-group">
           
            <?= form_label('Tanggal Lahir', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text',  'class' => 'form-control', 'id' => 'txt_birthday',"disabled"=>"disable", 'value' => DefaultTanggal(@$row->contact_birthdate), 'placeholder' => 'Tanggal Lahir')); ?>
            </div>
             <?= form_label('Usia', "txt_status", array("class" => 'col-sm-1 control-label')); ?>
             <div class="col-sm-5">
                <?= form_input(array('type' => 'text',  'class' => 'form-control', 'id' => 'txt_contact_telp',"disabled"=>"disable", 'value' => GetAge(@$row->contact_birthdate), 'placeholder' => 'Umur')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Alamat', "txt_notes", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?= form_textarea(array('type' => 'text','disabled'=>"disable", 'rows' => '5', 'cols' => '10', 'class' => 'form-control', 'name' => 'contact_address', 'id' => 'txt_keterangan', 'placeholder' => 'Alamat', 'value' => @$row->contact_address)); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Catatan', "txt_notes", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?= form_textarea(array('type' => 'text','disabled'=>"disable", 'rows' => '5', 'cols' => '10', 'class' => 'form-control', 'name' => 'notes', 'id' => 'notes', 'placeholder' => 'Keterangan', 'value' => @$row->keterangan)); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
         </div>

    </form>
</div>