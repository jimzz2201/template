<section class="content-header">
    <h1>
        Customer <?= @$button ?>
        <small>Customer</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Customer</li>
        <li class="active">Customer <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_customer" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_customer" id="id_customer" value="<?php echo @$id_customer; ?>" /> 
                                <!-- <div class="form-group">
                                        <?//= form_label('Id Tipe Penyewa', "txt_id_tipe_penyewa", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-6">
                                                <?//= form_input(array('type' => 'text', 'name' => 'id_tipe_penyewa', 'value' => @$id_tipe_penyewa, 'class' => 'form-control', 'id' => 'txt_id_tipe_penyewa', 'placeholder' => 'Id Tipe Penyewa')); ?>
                                        </div>
                                </div> -->
                                <div class="form-group">
                                    <?= form_label('Kode Customer', "txt_kode_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $mergearray = array();
                                        $mergearray['readonly'] = "readonly";
                                        $mergearray['name'] = "kode_customer";
                                        if (!CheckEmpty(@$id_customer)) {
                                            $mergearray['readonly'] = "readonly";
                                        } else {
                                            $mergearray['name'] = "kode_customer";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_customer, 'class' => 'form-control', 'id' => 'txt_kode_customer', 'placeholder' => 'Kode Customer'))); ?>
                                    </div>
                                    <?= form_label('Group', "txt_group_customer", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_group_customer', 'class' => 'form-control', 'id' => 'dd_id_group_customer', 'placeholder' => 'Group'), DefaultEmptyDropdown($list_group_customer, "json", "Group"), @$id_group_customer); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Customer', "txt_nama_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_jenis_customer_master', 'class' => 'form-control', 'id' => 'dd_Jenis Customer', 'placeholder' => 'Jenis'), DefaultEmptyDropdown($list_jenis_customer, "json", "Jenis"), @$id_jenis_customer); ?>
                                    </div>
                                    <div class="col-sm-9">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_customer', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Nama Customer')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('No Telp', "txt_no_telp", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'no_telp', 'value' => @$no_telp, 'class' => 'form-control', 'id' => 'txt_no_telp', 'placeholder' => 'No Telp')); ?>
                                    </div>
                                    <?= form_label('No Telp 2', "txt_no_telp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'no_telp_2', 'value' => @$no_telp_2, 'class' => 'form-control', 'id' => 'txt_no_telp_2', 'placeholder' => 'No Telp 2')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat ', "txt_alamat", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat', 'rows' => '3', 'cols' => '10', 'value' => @$alamat, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Fax', "txt_fax", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'fax', 'value' => @$fax, 'class' => 'form-control', 'id' => 'txt_fax', 'placeholder' => 'Fax')); ?>
                                    </div>
                                    <?= form_label('Email', "txt_email", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'email', 'value' => @$email, 'class' => 'form-control', 'id' => 'txt_email', 'placeholder' => 'Email')); ?>
                                    </div>
                                </div>
                                <hr/>
                                <div id="areaaddress">
                                    <?php
                                    foreach (@$list_address as $key => $address) {
                                        include APPPATH . 'modules/customer/views/v_customer_address_book.php';
                                    }
                                    ?>
                                </div>




                                <div class="form-group">
                                    <?= form_label('TDP / NIB', "txt_tdp", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'tdp', 'value' => @$tdp, 'class' => 'form-control', 'id' => 'txt_tdp', 'placeholder' => 'TDP')); ?>
                                    </div>
                                    <?= form_label('No KTP', "txt_no_ktp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'no_ktp', 'value' => @$no_ktp, 'class' => 'form-control', 'id' => 'txt_no_ktp', 'placeholder' => 'No KTP')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Npwp', "txt_npwp", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'npwp', 'value' => @$npwp, 'class' => 'form-control', 'id' => 'txt_npwp', 'placeholder' => 'Npwp')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Npwp', "txt_nama_npwp", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_npwp', 'value' => @$nama_npwp, 'class' => 'form-control', 'id' => 'txt_nama_npwp', 'placeholder' => 'Nama Npwp')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat Npwp', "txt_npwp", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat_npwp', 'rows' => '3', 'cols' => '10', 'value' => @$alamat_npwp, 'class' => 'form-control', 'id' => 'txt_alamat_npwp', 'placeholder' => 'Alamat NPWP')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Piutang', "txt_piutang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'piutang', 'value' => DefaultCurrency(@$piutang), 'class' => 'form-control', 'id' => 'txt_piutang', 'placeholder' => 'Piutang', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Saldo Awal', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'saldo_awal', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_awal', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Plafond', "txt_plafond", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'plafond', 'value' => DefaultCurrency(@$plafond), 'class' => 'form-control', 'id' => 'txt_plafond', 'placeholder' => 'Plafond', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('DP', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$dp), 'class' => 'form-control', 'id' => 'txt_dp', 'placeholder' => 'DP', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Saldo', "txt_plafond", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'value' => DefaultCurrency(@$hutang), 'class' => 'form-control', 'id' => 'txt_saldo', 'placeholder' => 'Saldo', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                    <?= form_label('Blacklist', "txt_is_blacklist", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("selected" => @$is_blacklist, "name" => "is_blacklist"), array('1' => 'Yes', '0' => 'No'), @$is_blacklist, array('class' => 'form-control', 'id' => 'is_blacklist')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Cabang', "txt_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_cabang', 'class' => 'form-control', 'id' => 'dd_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown($list_cabang, "json", "Cabang"), @$id_cabang); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Sales', "txt_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_pegawai[]"), $list_pegawai, @$id_pegawai, array('class' => 'form-control', 'id' => 'dd_id_pegawai', 'multiple' => 'multiple')); ?>

                                    </div>
                                </div>

                                <?php include APPPATH . 'modules/module/views/upload_index.php'; ?>


                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/customer' ?>" class="btn btn-default"  >Cancel</a>
                                    <?php if (!CheckEmpty($is_edit)) { ?>
                                        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                    <?php } ?>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

    $(document).ready(function () {




        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });
        $("select").select2();

<?php if (CheckEmpty($is_edit)) { ?>
            $("select").select2('enable', false);
<?php } ?>
    });
    var arraylength =<?php echo count(@$list_address) ?>;
    var list_jenis_customer = <?php echo json_encode(DefaultEmptyDropdown($list_jenis_customer, "json", "Jenis")) ?>;
    var list_agama = <?php echo json_encode(DefaultEmptyDropdown($list_agama, "json", "Agama")) ?>;
    var list_gender = <?php echo json_encode(DefaultEmptyDropdown($list_gender, "json", "Gender")) ?>;
    $("#manageAlamat").on('click', function () {
        var idCust = $("#id_customer_tmp").val() == '' ? 0 : $("#id_customer_tmp").val();
        // alert(idCust);deleteAlamat
        $.ajax({
            url: baseurl + 'index.php/customer/add_alamat',
            type: 'post',
            data: {
                key: arraylength
            },
            success: function (data) {
                $("#areaaddress").append(data);
                $("#dd_jenis_customer" + arraylength).select2({data: list_jenis_customer});
                $("#dd_contact_agama" + arraylength).select2({data: list_agama});
                $("#dd_contact_gender" + arraylength).select2({data: list_gender});
                $("#id_contact_birthdate" + arraylength).datepicker({autoclose: true, dateFormat: 'dd M yy'});
                $(".datepicker").datepicker({autoclose: true, dateFormat: 'dd M yy'});
                $('html, body').animate({
                    scrollTop: $("#area" + arraylength).offset().top
                }, 1000);
                arraylength += 1;

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    });
    function deleteAlamat(indexalamat) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $("#area" + indexalamat).remove();
            }
        });
    }
<?php if (!CheckEmpty($is_edit)) { ?>
        $("#frm_customer").submit(function () {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/customer/customer_manipulate',
                dataType: 'json',
                data: $(this).serialize() + '&' + $.param({'listimage': listimage}),
                success: function (data) {
                    if (data.st)
                    {
                        window.location.href = baseurl + 'index.php/customer';
                    } else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
            return false;

        })
<?php } else { ?>
        $("button").attr("disable", "disable");
        $("button").css("display", "none");
        $("input").attr("readonly", "readonly");
        $("textarea").attr("readonly", "readonly");

<?php } ?>
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>