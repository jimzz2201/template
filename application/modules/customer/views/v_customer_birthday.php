
<section class="content-header">
    <h1>
        Ulang Tahun
        <small>Jadwal</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Jadwal</li>
        <li class="active">Ulang Tahun</li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-6">
                    <?= form_dropdown(array('type' => 'text', 'name' => 'cabang', 'class' => 'form-control', 'id' => 'dd_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$cabang); ?>
                </div>
                
            </div>
            <div class=" headerbutton">
                <form>

                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <div style="padding:70px;">
                        <div id='calendar' style="border:4px solid #e0dfdc;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script type="text/javascript">
    var id_cabang = $("#dd_cabang").val();
    
    $("#btt_export").click(function(){
        
        alert("ddadada");
    })
    $("#dd_cabang").on('change', function () {
        id_cabang = $(this).val();
        refresh(id_cabang);
    });

    function refresh(id_cabang) {
        $("#calendar").html('');
        var cal = setDayGridEvents(id_cabang);
        cal.render();
    }

    function setDayGridEvents(id_cabang) {
        var calendarEl = document.getElementById('calendar');
        $("#calendar").empty();
        var dataEvents = '<?php echo base_url() ?>index.php/customer/get_birthday?id_cabang=' + id_cabang ;
        var calendar = new FullCalendar.Calendar(calendarEl, {
            plugins: ['interaction', 'dayGrid', 'timeGrid'],
            selectable: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            height: 700,
            locale: 'id',
            editable: true,
            events: dataEvents,
            eventClick: function (info) {
                viewBirthday(info.event.id)
            }
        });
        calendar.setOption('locale', 'id');
        calendar.render();
        return calendar;

    }


    function viewBirthday(id_jadwal) {
        $.ajax({url: baseurl + 'index.php/customer/view_birthday/' + id_jadwal,
            success: function (data) {
                modalbootstrap(data, "Edit Warna", 800);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }

    var table;
    function deletejadwal_kunjungan(id_jadwal_kunjungan) {


        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/jadwal_kunjungan/jadwal_kunjungan_delete',
                    dataType: 'json',
                    data: {
                        id_jadwal_kunjungan: id_jadwal_kunjungan
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }
    $(document).ready(function () {
        var cal = setDayGridEvents(id_cabang);
        

        $("select").select2();
        $("#dd_cabang").change(function () {
            var id_cust = $(this).val();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/pegawai/get_dropdown_pegawai',
                data: {
                    id_cabang: $("#dd_cabang").val()
                },
                dataType: 'json',
                success: function (data) {
                    if (data.st)
                    {
                        $("#dd_sales").empty();
                        $("#dd_sales").select2({data:data.list_pegawai});
                    }
                },
                error: function (xhr, status, error) {
                    modaldialogerror(xhr.responseText);
                }
            })
        })
        $(".datepicker").datepicker();
        $('.datetimepicker').timepicker({
            template: 'dropdown'
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "jadwal_kunjungan/getdatajadwal_kunjungan", "type": "POST"},
            columns: [
                {
                    data: "id_jadwal_kunjungan",
                    title: "Kode",
                    orderable: false
                },
                {data: "tanggal", orderable: false, title: "Tanggal",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "id_customer", orderable: false, title: "Id Customer"},
                {data: "id_sales", orderable: false, title: "Id Sales"},
                {data: "notes", orderable: false, title: "Notes"},
                {data: "results", orderable: false, title: "Results"},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        return data == 1 ? "Active" : "Not Active";
                    }},
                {data: "type", orderable: false, title: "Type",
                    mRender: function (data, type, row) {
                        return data == 1 ? "yes" : "no";
                    }},
                {data: "start_time", orderable: false, title: "Start Time"},
                {data: "end_time", orderable: false, title: "End Time"},
                {data: "longitude", orderable: false, title: "Longitude"},
                {data: "latitude", orderable: false, title: "Latitude"},
                {data: "device_id", orderable: false, title: "Device Id"},
                {data: "device_name", orderable: false, title: "Device Name"},
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
<style>
    .control-label {
        text-align: left !important;
    }
    .bootstrap-timepicker-widget.dropdown-menu.open {
        display: inline-block;
        z-index: 99999 !important;
    }
</style>    