<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_customer extends CI_Model {

    public $table = '#_customer';
    public $id = 'id_customer';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    function GetOneCOntactEmail($id)
    {
        $row=null;
        if(strpos($id, "C")>=0)
        {
           $this->db->from("#_customer");
           $this->db->where(array("id_customer"=> str_replace("C", "", $id)));
           $this->db->join("#_jenis_customer","#_jenis_customer.id_jenis_customer=#_customer.id_jenis_customer","left");
           $this->db->select("contact_person,nama_customer,kode_customer,contact_address,contact_telp,contact_agama,contact_gender,contact_birthdate,#_customer.keterangan,nama_jenis_customer");
           $row=$this->db->get()->row();
        }
        else  if(strpos($id, "A")>=0)
        {
           $this->db->from("#_customer_address");
           $this->db->join("#_customer","#_customer.id_customer=#_customer_address.id_customer","left");
           $this->db->join("#_jenis_customer","#_jenis_customer.id_jenis_customer=#_customer_address.id_jenis_customer","left");

           $this->db->where(array("#_customer_address.id_address"=> str_replace("A", "", $id)));
           $this->db->select("#_customer_address.contact_person,nama_customer,kode_customer,#_customer_address.contact_address,#_customer_address.contact_telp,#_customer_address.contact_agama,#_customer_address.contact_gender,#_customer_address.contact_birthdate,#_customer_address.keterangan,nama_jenis_customer");
           $row=$this->db->get()->row();
        }
        return $row;
    }
    
    
    function GetBirhtday($model)
    {
        $yearstart=date('Y', strtotime(GetDateNow()));
        $yearend=date('Y', strtotime(GetDateNow()));
        $arrcabang= GetCabangAkses();
        $daystart=1;
        $dayend=30;
        $monthstart=1;
        $monthend=12;
        
        if(!CheckEmpty(@$model['start']))
        {
            $daystart=date('d', strtotime($model['start']));
            $monthstart=date('m', strtotime($model['start']));
            $yearstart=date('Y', strtotime($model['start']));
        }
        
        if(!CheckEmpty(@$model['end']))
        {
            $dayend=date('d', strtotime($model['end']));
            $monthend=date('m', strtotime($model['end']));
            $yearend=date('Y', strtotime($model['end']));
        }
        
        $enddate=$yearstart."-".$monthend."-".$dayend;
        $startdate=($yearstart<$yearend?$yearstart-1:$yearstart)."-".$monthstart."-".$daystart;
        
        
        if(!CheckEmpty($model['id_cabang']))
        {
            $arrcabang=array($model['id_cabang']);
        }
       
        if(count($arrcabang)==0)
        {
            $arrcabang=array("-1");
        }
        
        $this->db->where_in("id_cabang",$arrcabang);
        $this->db->from("#_view_birthday");
        $this->db->where(array("convert(concat('".$yearstart."','-',month_birth,'-',day_birth),date) >="=>$startdate));
        $this->db->where(array("convert(concat('".$yearstart."','-',month_birth,'-',day_birth),date) <="=>$enddate));
        $this->db->select("#_view_birthday.*,concat('".$yearstart."',start_view) as start");
        $listbirthday=$this->db->get()->result_array();
        
        if($yearstart!=$yearend)
        {
            
            foreach($listbirthday as $key=>$birthday)
            {
                if($birthday['month_birth']<$monthstart)
                    $listbirthday[$key]['start']=$yearend.$birthday['start_view'];
                else
                    $listbirthday[$key]['start']=$yearstart.$birthday['start_view'];  
            }
        }
        
        
        return $listbirthday;
        
    }
    // datatables
    function GetDatacustomer($params, $mode="datatables") {
        
        $isedit=false;
        $button=true;
        if(CekModule("K113", false))
        {
            $isedit=true;
        }
        $this->load->library('datatables');
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
            $button = false;
        }
        $DB->select('id_customer,kode_customer,nama_customer,nama_group_customer,npwp,alamat,no_telp,no_telp_2,fax,email,birthdate,dgmi_customer.contact_person,
                    keterangan,piutang,saldo_awal,plafond,dgmi_customer.status,is_blacklist,nama_cabang,nama_npwp,alamat_npwp,tdp,no_ktp');
        $DB->from($this->table);
        $DB->where($this->table . '.deleted_date', null);
        $DB->join('#_cabang', 'dgmi_cabang.id_cabang = #_customer.id_cabang', 'left');
        $DB->join('#_customer_group', 'dgmi_customer_group.id_group_customer = #_customer.id_group_customer', 'left');
        // $DB->join('#_jenis_customer', '#_jenis_customer.id_jenis_customer = #_customer.id_jenis_customer');
       
        if (!CheckEmpty($params['id_cabang'])) {
            $this->datatables->where('#_customer.id_cabang', $params['id_cabang']);
        }
        if($button) {
            $straction = '';
            if ($isedit) {
                $straction .= anchor(site_url('customer/edit_customer/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
                $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletecustomer($1);return false;"));
            }
            $strview .= anchor(site_url('customer/view_customer/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
            
            $DB->add_column('action', $straction, 'id_customer');
            $DB->add_column('view', $strview, 'id_customer');
        }
        
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        // print_r($this->db->last_query());
        return $result;
    }

    function KoreksiSaldoPiutang($customer_id, $saldo_awal = null, $is_all = false) {
        try {
            if ($saldo_awal == null) {
                $customer = $this->GetOneCustomer($customer_id);
                if ($customer) {
                    $saldo_awal = $customer->saldo_awal;
                }
            }
            $this->load->model("gl_config/m_gl_config");
            $akunpiutang = $this->m_gl_config->GetIdGlConfig("piutang");
           
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpiutang, "id_subject" => $customer_id));
            $this->db->select("sum(kredit)-sum(debet) as totalpiutang");
            $totalpiutang = $this->db->get()->row()->totalpiutang;
            $piutang = DefaultCurrencyDatabase($saldo_awal);
            $piutang += CheckEmpty($totalpiutang) ? 0 : $totalpiutang;
            $update = array();
            $update['updated_date'] = GetDateNow();
            $update['updated_by'] = ForeignKeyFromDb(GetUserId());
            $update['piutang'] = $piutang * -1;
            $update['saldo_awal'] = DefaultCurrencyDatabase($saldo_awal);
            if($is_all)
            {
                $akundp = $this->m_gl_config->GetIdGlConfig("dp");
                $this->db->from("#_buku_besar");
                $this->db->where(array("id_gl_account" => $akundp, "id_subject" => $customer_id));
                $this->db->select("sum(kredit)-sum(debet) as totaldp");
                $totaldp = @$this->db->get()->row()->totaldp;
                $update['dp']= CheckEmpty($totaldp)?0:$totaldp;
                
                $akunhutang = $this->m_gl_config->GetIdGlConfig("hutangcustomer");
                $this->db->from("#_buku_besar");
                $this->db->where(array("id_gl_account" => $akunhutang, "id_subject" => $customer_id));
                $this->db->select("sum(kredit)-sum(debet) as totalhutang");
                $totalhutang = @$this->db->get()->row()->totalhutang;
                $update['hutang']= CheckEmpty(@$totalhutang)?0:$totalhutang;
            }
            $message = "";
            $this->db->update("#_customer", $update, array("id_customer" => $customer_id));

            $arrayreturn["msg"] = "Piutang dan Saldo Awal Customer telah diupdate";
            $arrayreturn["st"] = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    
    function GetOneCustomer($keyword, $type = 'id_customer', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $customer = $this->db->get($this->table)->row();
        if ($customer && $isfull) {
            $this->load->model("module/m_module");
            $customer->list_doc = GetTableData("#_prospek", 'id_prospek', 'no_prospek', array('#_prospek.status' => 1, "id_customer" => $customer->id_customer, "tanggal_pelunasan" => null, '#_prospek.deleted_date' => null));
            $listpegawai = GetTableData("#_customer_pegawai", 'id_pegawai', 'nama_pegawai', array("id_customer" => $customer->id_customer), "json", [], [], array(array("table" => "#_pegawai", "condition" => "#_pegawai.id_pegawai=#_customer_pegawai.id_pegawai")));
            $customer->id_pegawai = array_column($listpegawai, 'id');
            $customer->listpegawai = DefaultEmptyDropdown($listpegawai, "json", "Salesman");
            $customer->listimage=$this->m_module->GetFile("Customer",$customer->id_customer);
        }
        return $customer;
    }

    function GetDetCustomer($keyword, $type = 'id_customer') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $customer = $this->db->get($this->table)->row_array();
        return $customer;
    }

    function CustomerManipulate($model) {
        try {
            $listimage= CheckArray($model, "listimage");
            // $model['id_tipe_penyewa'] = ForeignKeyFromDb($model['id_tipe_penyewa']);
            // $cust['birthdate'] = DefaultTanggalDatabase($model['birthdate']);
            $cust['status'] = DefaultCurrencyDatabase($model['status']);
            $cust['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $cust['is_blacklist'] = DefaultCurrencyDatabase($model['is_blacklist']);

            $cust['contact_person']=@$model['contact_person'][0];
            $cust['contact_address']=@$model['contact_address'][0];
            $cust['contact_telp']=@$model['contact_telp'][0];
            $cust['keterangan']=@$model['keterangan'][0];
            $cust['contact_agama']=@$model['contact_agama'][0];
            $cust['contact_gender']=@$model['contact_gender'][0];
           
            $cust['contact_birthdate']= DefaultTanggalDatabase(@$model['contact_birthdate'][0]);
            $cust['id_jenis_customer']=@$model['id_jenis_customer'][0];
            $cust['id_jenis_customer_master']=$model['id_jenis_customer_master'];
            $cust['nama_customer']=$model['nama_customer'];
            $cust['no_telp']=$model['no_telp'];
            $cust['no_telp_2']=$model['no_telp_2'];
            $cust['alamat']=$model['alamat'];
            $cust['fax']=$model['fax'];
            $cust['email']=$model['email'];
            $cust['npwp']=$model['npwp'];
            $cust['nama_npwp']=$model['nama_npwp'];
            $cust['alamat_npwp']=$model['alamat_npwp'];
            $cust['id_group_customer']=$model['id_group_customer'];
            $cust['tdp']=$model['tdp'];
            $cust['no_ktp']=$model['no_ktp'];
            $id_customer = $model['id_customer'];

            if (CheckEmpty($model['id_customer'])) {
                $cust['kode_customer'] = AutoIncrement("#_customer", "C", "kode_customer", 3);
                $cust['created_date'] = GetDateNow();
                $cust['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $cust);
                $id_customer = $this->db->insert_id();
                $dataret = array("st" => true, "msg" => "Customer successfull added into database");
            } else {
                $cust['updated_date'] = GetDateNow();
                $cust['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $cust, array("id_customer" => $id_customer));
                $dataret = array("st" => true, "msg" => "Customer has been updated");
            }
            $this->load->model("module/m_module");
            $this->m_module->SyncDb($listimage,"Customer",$id_customer);
            
            $arrayaddress=[];
            foreach($model['contact_person'] as $key=>$contact)
            {
                if(!CheckEmpty($contact)&&$key>0)
                {
                    $address=[];
                    $address['id_jenis_customer']=@$model['id_jenis_customer'][$key];
                    $address['id_customer']=$id_customer;
                    $address['contact_person']=@$model['contact_person'][$key];
                    $address['contact_telp']=@$model['contact_telp'][$key];
                    $address['contact_address']=@$model['contact_address'][$key];
                    $address['contact_agama']=@$model['contact_agama'][$key];
                    $address['contact_gender']=@$model['contact_gender'][$key];
                    $address['contact_birthdate']= DefaultTanggalDatabase(@$model['contact_birthdate'][$key]);
                    $address['keterangan']=@$model['keterangan'][$key];
                    if(!CheckEmpty($model['id_address'][$key]))
                    {
                        $address['updated_date']= GetDateNow();
                        $address['updated_by']= GetUserId();
                        $this->db->update("#_customer_address",$address,array("id_address"=>$model['id_address'][$key]));
                        $arrayaddress[]=$model['id_address'][$key];
                    }
                    else
                    {
                        $address['created_date']= GetDateNow();
                        $address['created_by']= GetUserId();
                        $this->db->insert("#_customer_address",$address);
                        $arrayaddress[]=$this->db->insert_id();
                    }
                }
            }

            if (count($arrayaddress) > 0) {
                $this->db->where_not_in("id_address", $arrayaddress);
            }
            $this->db->where("id_customer", $id_customer);
            $this->db->delete("#_customer_address");
            $this->db->delete("#_customer_pegawai", array("id_customer" => $id_customer));

            $listidpegawai = $model['id_pegawai'];
            if (CheckEmpty(@$model['id_pegawai'])) {
                $listidpegawai = [];
            }

            foreach ($listidpegawai as $pegawai) {
                $this->db->insert("#_customer_pegawai", array("id_pegawai" => $pegawai, "id_customer" => $id_customer));
            }
          
            SetMessageSession(1, $dataret['msg']);
            return $dataret;
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function AlamatManipulate($model) {
        try {
            $model['id_customer'] = ForeignKeyFromDb($model['id_customer']);

            if (CheckEmpty($model['id_alamat'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert("dgmi_customer_address", $model);
                SetMessageSession(1, 'Alamat Customer successfull added into database');
                return array("st" => true, "msg" => "Alamat Customer successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update("dgmi_customer_address", $model, array("id_alamat" => $model['id_alamat']));
                SetMessageSession(1, 'Alamat Customer has been updated');
                return array("st" => true, "msg" => "Alamat Customer has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetOneAlamatCustomer($keyword, $type = 'id_alamat') {
        $this->db->where($type, $keyword);
        $this->db->where('#_customer_address.deleted_date', null);
        $customer = $this->db->get('#_customer_address')->row();
        return $customer;
    }

    function GetAlamatCustomer($keyword, $type = 'id_customer') {
        $this->db->where($type, $keyword);
        $this->db->where('#_customer_address.deleted_date', null);
        $list_address = $this->db->get("#_customer_address")->result_array();

        $listreturn = [];
        $customer = $this->GetOneCustomer($keyword, $type);
        if ($customer) {
            $listreturn[] = array("id_address" => 0, "concat_person" => $customer->contact_person, "contact_address" => $customer->contact_address, "keterangan" => $customer->keterangan, "contact_telp" => $customer->contact_telp, "id_jenis_customer" => $customer->id_jenis_customer);
        }
        foreach ($list_address as $address) {
            $listreturn[] = $address;
        }
        return $listreturn;
    }

    function GetDropDownCustomer($id_pegawai = null,$id_customer=null,$list_cabang=[]) {
        if (!CheckEmpty(@$id_pegawai)||!CheckEmpty($list_cabang)) {
            $this->db->join("#_customer_pegawai", "#_customer_pegawai.id_customer=#_customer.id_customer","left");
            $this->db->group_by("#_customer.id_customer");
            $this->db->group_start();
            $this->db->where(array("#_customer_pegawai.id_pegawai" => $id_pegawai));
            
            $this->db->or_where(array("#_customer_pegawai.id_customer" => null));
            $this->db->group_end();
         }

        if (!CheckEmpty($list_cabang)) {
            $this->db->or_where_in("#_customer.id_cabang", $list_cabang);
        }
        $where=array($this->table . '.status' => 1, $this->table . '.deleted_date' => null);
        if(!CheckEmpty($id_customer))
        {
            $where['id_customer']=$id_customer;
        }
        $listcustomer = GetTableData($this->table, 'id_customer', 'nama_customer',$where ,"json",["nama_customer"],[],[],"#_customer.id_customer,nama_customer",[],10000);

        return $listcustomer;
    }

    function GetDropDownJenisCustomer() {
        $listcustomer = GetTableData("#_jenis_customer", 'id_jenis_customer', 'nama_jenis_customer', array("#_jenis_customer" . '.status' => 1, "#_jenis_customer" . '.deleted_date' => null));

        return $listcustomer;
    }

    function CustomerDelete($id_customer) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_customer' => $id_customer));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Customer has been deleted from database");
    }

    function AlamatCustomerDelete($id_alamat) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update("#_customer_address", $model, array('id_alamat' => $id_alamat));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Customer has been deleted from database");
    }

}
