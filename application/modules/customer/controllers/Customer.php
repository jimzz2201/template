<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends CI_Controller {

    private $_title = "Data Customer";
    
    function __construct() {
        parent::__construct();
        $this->load->model('m_customer');
        $this->load->model('cabang/m_cabang');
        $this->load->model('customer_group/m_customer_group');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K014";
        $header = "K001";
        $title = "Customer";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "customer/v_customer_index", $javascript, $css);
    }
    
    public function get_birthday(){
        
        
        $model=$this->input->get();
        
        
        $data_events=$this->m_customer->GetBirhtday($model);
         echo json_encode($data_events);
        
    }
    
    

    public function birthday() {
        $javascript = array();
        $css = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/plugins/select2/select2.js";
        $javascript[] = "assets/plugins/timepicker/bootstrap-timepicker.min.js";
        $javascript[] = "assets/plugins/fullcalendar/core/main.js";
        $javascript[] = "assets/plugins/fullcalendar/daygrid/main.js";
        $javascript[] = "assets/plugins/fullcalendar/interaction/main.js";
        $javascript[] = "assets/plugins/fullcalendar/core/locales/id.js";
        $javascript[] = "assets/plugins/fullcalendar/bootstrap/main.min.js";
        $javascript[] = "assets/plugins/dropzone/dropzone.min.js";
        $css[] = "assets/plugins/fullcalendar/core/main.css";
        $css[] = "assets/plugins/fullcalendar/daygrid/main.css";
        $css[] = "assets/plugins/fullcalendar/bootstrap/main.min.css";
        $css[] = "assets/plugins/select2/select2.css";
        $css[] = "assets/plugins/timepicker/bootstrap-timepicker.min.css";
        $css[] = "assets/plugins/dropzone/dropzone.min.css";
        $module = "K110";
        $header = "K074";
        $title = "Ulang Tahun";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        CekModule($module);
        LoadTemplate($model, "customer/v_customer_birthday", $javascript, $css);
    }
    
    public function view_birthday($id)
    {
       $row=$this->m_customer->GetOneCOntactEmail($id);
       
       $this->load->view("v_customer_birthday_view",array("row"=>$row));
        
    }
    

    public function get_one_customer() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_customer->GetOneCustomer($model["id_customer"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Customer tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function get_dropdown_customer() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_customer = $this->m_customer->GetDropDownCustomer(@$model['id_pegawai']);
        echo json_encode(array("list_customer" => DefaultEmptyDropdown($list_customer, "json", "Customer"), "st" => true));
    }

    public function info_customer() {
        $model = $this->input->get();
        $message = "";
        $customer = $this->m_customer->GetOneCustomer($model["id_customer"]);
        $data = array();
        $message = 'Customer Diluar Jangkauan';
        $statusCustomer = 0;
        $this->db->where('dgmi_customer_pegawai.id_customer', $customer->id_customer);
        $this->db->where('dgmi_customer_pegawai.id_pegawai', $model["id_pegawai"]);
        $cust = $this->db->get('dgmi_customer_pegawai')->row_array();
        if ($cust && $cust['id_pegawai'] == $model["id_pegawai"]) {
            $message = '';
            $statusCustomer = 1;
        }
        $customer->message = $message;
        $customer->status_customer = $statusCustomer;

        $data['data'] = $customer;
        $data['st'] = $data['data'] != null;
        $data['msg'] = !$data['st'] ? "Data Customer tidak ditemukan dalam database" : "";
        echo json_encode($data);
    }

    public function getdatacustomer() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_customer->GetDatacustomer($params);
    }

    public function create_customer() {
        $row = ['button' => 'Add'];
        $this->load->model("pegawai/m_pegawai");
        $javascript = array();
        $css = array();
        $module = "K014";
        $header = "K001";
        $row['title'] = "Add Customer";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $row['listimage'] = [];
        $row['type_upload'] = "Customer";
        $row['list_jenis_customer'] = $this->m_customer->GetDropDownJenisCustomer();
        $row['list_group_customer'] = $this->m_customer_group->GetDropDownCustomer_group();
        $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai("obj");
        $row['list_agama'] = array(array('id' => 'ISLAM', 'text' => 'ISLAM'), array('id' => 'KATOLIK', 'text' => 'KATOLIK'), array('id' => 'PROTESTAN', 'text' => 'PROTESTAN'), array('id' => 'HINDU', 'text' => 'HINDU'), array('id' => 'BUDHA', 'text' => 'BUDHA'), array('id' => 'KHONGHUCU', 'text' => 'KHONGHUCU'));
        $row['list_gender'] = array(array('id' => 'PRIA', 'text' => 'PRIA'), array('id' => 'WANITA', 'text' => 'WANITA'));
        $row['id_customer_tmp'] = rand();
        $row['list_address'] = array(array("jenis_customer" => ""));
        $row['is_blacklist'] = 0;
        $row['is_edit']="yes" ;
        LoadTemplate($row, 'customer/v_customer_manipulate', $javascript, $css);
    }

    public function customer_manipulate() {
        $message = '';

        // $this->form_validation->set_rules('id_tipe_penyewa', 'Id Tipe Penyewa', 'trim|required');
        //$this->form_validation->set_rules('kode_customer', 'Kode Customer', 'trim|required');
        $this->form_validation->set_rules('nama_customer', 'Nama Customer', 'trim|required');
        $this->form_validation->set_rules('npwp', 'Npwp', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('no_telp', 'No Telp', 'trim|required');
        $this->form_validation->set_rules('id_group_customer', 'Group Customer', 'trim|required');
        $this->form_validation->set_rules('no_telp_2', 'No Telp 2', 'trim');
        $this->form_validation->set_rules('fax', 'Fax', 'trim');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('birthdate', 'Birthdate', 'trim');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');
        // $this->form_validation->set_rules('id_cabang', 'Id Cabang', 'trim|required');
        $model = $this->input->post();

        $isvalidcontact = false;
        if (!CheckEmpty($model['contact_person'])) {
            foreach ($model['contact_person'] as $contact) {
                if (!CheckEmpty($contact)) {
                    $isvalidcontact = true;
                }
            }
        }
        if (!$isvalidcontact) {
            $message .= "Contact Person minimum <br/>";
        }

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_customer->CustomerManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_customer($id = 0) {
        $row = $this->m_customer->GetOneCustomer($id);
        
        if ($row) {
            $row->button = 'Update';
            $this->load->model("pegawai/m_pegawai");
            $row = json_decode(json_encode($row), true);
            $row['list_address'] = $this->m_customer->GetAlamatCustomer($id);
            $javascript = array();
            $module = "K113";
            $header = "K001";
            $row['title'] = "Edit Customer";
            CekModule($module);
            $row['form'] = $header;
            $row['is_edit'] = "yes";
            $row['formsubmenu'] = $module;
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_jenis_customer'] = $this->m_customer->GetDropDownJenisCustomer();

            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai("obj");
            $row['type_upload'] = "Customer";
            $row['list_group_customer'] = $this->m_customer_group->GetDropDownCustomer_group();
            $row['list_agama'] = array(array('id' => 'ISLAM', 'text' => 'ISLAM'), array('id' => 'KATOLIK', 'text' => 'KATOLIK'), array('id' => 'PROTESTAN', 'text' => 'PROTESTAN'), array('id' => 'HINDU', 'text' => 'HINDU'), array('id' => 'BUDHA', 'text' => 'BUDHA'), array('id' => 'KHONGHUCU', 'text' => 'KHONGHUCU'));
            $row['list_gender'] = array(array('id' => 'PRIA', 'text' => 'PRIA'), array('id' => 'WANITA', 'text' => 'WANITA'));
            $css = [];
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'customer/v_customer_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Customer cannot be found in database");
            redirect(site_url('customer'));
        }
    }
    public function view_customer($id = 0) {
        $row = $this->m_customer->GetOneCustomer($id);
        
        if ($row) {
            $row->button = 'Update';
            $this->load->model("pegawai/m_pegawai");
            $row = json_decode(json_encode($row), true);
            $row['list_address'] = $this->m_customer->GetAlamatCustomer($id);
            $javascript = array();
            $module = "K014";
            $header = "K001";
            $row['title'] = "View Customer";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_jenis_customer'] = $this->m_customer->GetDropDownJenisCustomer();

            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai("obj");
            $row['type_upload'] = "Customer";
            $row['list_group_customer'] = $this->m_customer_group->GetDropDownCustomer_group();
            $row['list_agama'] = array(array('id' => 'ISLAM', 'text' => 'ISLAM'), array('id' => 'KATOLIK', 'text' => 'KATOLIK'), array('id' => 'PROTESTAN', 'text' => 'PROTESTAN'), array('id' => 'HINDU', 'text' => 'HINDU'), array('id' => 'BUDHA', 'text' => 'BUDHA'), array('id' => 'KHONGHUCU', 'text' => 'KHONGHUCU'));
            $row['list_gender'] = array(array('id' => 'PRIA', 'text' => 'PRIA'), array('id' => 'WANITA', 'text' => 'WANITA'));
            $css = [];
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'customer/v_customer_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Customer cannot be found in database");
            redirect(site_url('customer'));
        }
    }

    public function customer_delete() {
        $message = '';
        $this->form_validation->set_rules('id_customer', 'Customer', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_customer->CustomerDelete($model['id_customer']);
        }

        echo json_encode($result);
    }

    public function add_alamat() {
        $key = $this->input->post('key');

        $this->load->view('customer/v_customer_address_book', array("key" => $key));
    }

    public function edit_alamat_customer($id = 0) {
        $row = $this->m_customer->GetOneAlamatCustomer($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K014";
            $header = "K001";
            $row['title'] = "Edit Alamat";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->view('customer/v_add_alamat', $row);
        } else {
            SetMessageSession(0, "alamat cannot be found in database");
            redirect(site_url('customer'));
        }
    }

    public function alamat_manipulate() {
        $message = '';
        $this->form_validation->set_rules('nama_alamat', 'Nama Alamat', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('contact_person', 'Contact Person', 'trim|required');
        $this->form_validation->set_rules('telp_cp', 'Telp Contact Person', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');
        $this->form_validation->set_rules('id_customer', 'id customer', 'trim');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_customer->AlamatManipulate($model);
            echo json_encode($status);
        }
    }

    public function get_alamat() {
        $id_customer = $this->input->post('id_customer');
        $data = $this->m_customer->GetAlamatCustomer($id_customer);
        $html = '';
        foreach ($data as $val) {
            $id = $val['id_address'];
            $html .= "<div class='form-group'>
                        <label for='txt_nama_customer' class='col-sm-2 control-label'>" . $val['nama_alamat'] . "<br>
                        <a href='javascript:;' class='btn btn-primary btn-xs' onclick='editAlamatCustomer(" . $val['id_address'] . ");return false;'>Update</a>
                        <a href='javascript:;' class='btn btn-danger btn-xs' onclick='deleteAlamat(" . $val['id_address'] . ");return false;'>Delete</a>
                        </label>
                        <div class='col-sm-10'>
                            <textarea name='alamat' disabled='disabled' cols='10' rows='3' type='text' value='' class='form-control' id='txt_alamat' placeholder='Alamat'>" . $val['alamat'] . "</textarea>
                        </div>
                      </div>";
        }
        echo $html;
    }

    public function alamat_customer_delete() {
        $message = '';
        $this->form_validation->set_rules('id_alamat', 'Alamat', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_customer->AlamatCustomerDelete($model['id_alamat']);
        }

        echo json_encode($result);
    }

    public function search_customer() {
        $term = $this->input->post('term');
        $resp = [];
        if (strlen($term) >= 3) {
            $term = strtolower($term);
            $where = "(LOWER(nama_customer) LIKE '%" . $term . "%') AND status = 1 and deleted_date is null";
            $resp['results'] = GetTableData('#_customer', 'id_customer', ['kode_customer', 'nama_customer'], $where);
        }
        echo json_encode($resp);
    }

    // -----------------------------export-----------------------------------------

    function export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_customer->GetDatacustomer($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Customers",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }

    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Kode Customer', 'valueKey' => 'kode_customer', 'colWidth' => 10, 'bold' => true),
            // array('colName' => 'Jenis Customer', 'valueKey' => 'nama_jenis_customer', 'colWidth' => 15),
            array('colName' => 'Nama Customer', 'valueKey' => 'nama_customer', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Cabang', 'valueKey' => 'nama_cabang', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Alamat', 'valueKey' => 'alamat', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'Telp', 'valueKey' => 'no_telp', 'colWidth' => 15),
            array('colName' => 'Telp2', 'valueKey' => 'no_telp_2', 'colWidth' => 15),
            array('colName' => 'Fax', 'valueKey' => 'fax', 'colWidth' => 15),
            array('colName' => 'Email', 'valueKey' => 'email', 'colWidth' => 15),
            array('colName' => 'No KTP', 'valueKey' => 'no_ktp', 'colWidth' => 15),
            array('colName' => 'Birthdate', 'valueKey' => 'birthdate', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'NPWP', 'valueKey' => 'npwp', 'colWidth' => 15),
            array('colName' => 'Nama NPWP', 'valueKey' => 'nama_npwp', 'colWidth' => 15),
            array('colName' => 'Alamat NPWP', 'valueKey' => 'alamat_npwp', 'colWidth' => 15),
            array('colName' => 'TDP', 'valueKey' => 'tdp', 'colWidth' => 15),
            array('colName' => 'CP', 'valueKey' => 'contact_person', 'colWidth' => 15),
            array('colName' => 'Alamat CP', 'valueKey' => 'contact_address', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Tlp CP', 'valueKey' => 'contact_telp', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Agama CP', 'valueKey' => 'contact_agama', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Gender CP', 'valueKey' => 'contact_gender', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Birthdate CP', 'valueKey' => 'contact_birthdate', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Keterangan', 'valueKey' => 'keterangan', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Piutang', 'valueKey' => 'piutang', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Hutang', 'valueKey' => 'hutang', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Plafond', 'valueKey' => 'plafond', 'colWidth' => 15, 'bold' => true),
        );

        return $col;
    }

    function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }

    function footInfo() {
        $info = array();
        $info[] = array();

        return $info;
    }

    function rowSet($data) {
        $result = array();
        $nomor = 1;
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
           
            $nomor++;
            $result[]=$row;
        }

        return $result;
    }

    // ---------------------------end export--------------------------------

}

/* End of file Customer.php */