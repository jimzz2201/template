<div class="modal-header">
        Adjustment Prospek <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_adjustment_prospek" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_adjustment_prospek" value="<?php echo @$id_adjustment_prospek; ?>" /> 
	<input type="hidden" name="id_prospek" value="<?php echo @$id_prospek; ?>" />
	<input type="hidden" name="status" value="1" />
    <div class="form-group">
		<?= form_label('Nomor Prospek', "txt_initial_price", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'value' => @$nomor_prospek, 'class' => 'form-control', 'id' => 'no_prospek', 'placeholder' => 'Nomor Prospek', 'readonly' => 'readonly')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Type Adjustment', "txt_type_adjustman", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
            <?= form_dropdown(array("name" => "type_adjustment"), DefaultEmptyDropdown(@$list_adjustment, "json", "Type Adjustment"), @$type_adjustment, array('class' => 'form-control select2', 'id' => 'dd_type_adjustment'));?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Initial Price', "txt_initial_price", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'initial_price', 'value' => @$initial_price, 'class' => 'form-control', 'id' => 'txt_initial_price', 'placeholder' => 'Initial Price', 'readonly' => 'readonly')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Final Price', "txt_final_price", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'final_price', 'value' => @$final_price, 'class' => 'form-control  input-currency', 'id' => 'txt_final_price', 'placeholder' => 'Final Price', 'onkeyup' => 'javascript:this.value=Comma(this.value)', 'onkeypress' => 'return isNumberKey(event)')); ?>
		</div>
	</div>
	<!-- <div class="form-group">
		<?//= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?//= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div> -->
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
    var harga = <?php echo json_encode(@$hrg)?>;
    $(document).ready(function () {
        $(".select2").select2();
    });
    $("#dd_type_adjustment").change(function () {
        selectTypeAdjustment($(this).val());
    })
    function selectTypeAdjustment (id) {
        $("#txt_initial_price").val(Comma(harga[id - 1]));
    }
    $("#frm_adjustment_prospek").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/adjustment_prospek/adjustment_prospek_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>