
<section class="content-header">
    <h1>
        Adjustment Prospek
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Adjustment Prospek</li>
    </ol>


</section>
    
<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">

                <?php echo anchor("", 'Create', 'class="btn btn-success" id="btt_create"'); ?>
	    </div>
        </div>
         <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body form">
        <table class="table table-striped table-bordered table-hover" id="mytable">
	    
        </table>
        </div>
                </div>

            </div>
        </div>

</section>
<script type="text/javascript">
    var table;
    function deleteadjustment_prospek(id_adjustment_prospek) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/adjustment_prospek/adjustment_prospek_delete',
                dataType: 'json',
                data: {
                    id_adjustment_prospek: id_adjustment_prospek
                },
                success: function (data) {
                    if (data.st)
                    {
                        messagesuccess(data.msg);
                        table.fnDraw(false);
                    }
                    else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
       }});
    }

    function editadjustment_prospek(id_adjustment_prospek) {

        $.ajax({url: baseurl + 'index.php/adjustment_prospek/edit_adjustment_prospek/' + id_adjustment_prospek,
            success: function (data) {
                modalbootstrap(data, "Edit Adjustment_prospek");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    }
    $(document).ready(function() {
        $("#btt_create").click(function () {
            $.ajax({
                url: baseurl + 'index.php/adjustment_prospek/create_adjustment_prospek',
                data: {
                    id_prospek: 20
                },
                type: 'GET',
                success: function (data) {
                        modalbootstrap(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        })
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "adjustment_prospek/getdataadjustment_prospek", "type": "POST"},
            columns: [
                {
                    data: "id_adjustment_prospek",
                    title: "Kode",
                    orderable: false
                },
                {data: "type_adjustment" , orderable:false , title :"Type Adjustment",
                    mRender: function (data, type, row) {
                        return (data == 1 ? "Chassis" : (data == 2 ? "Karoseri" : "Biaya BBN"));
                    }},
                {data: "initial_price", orderable: false, title: "Initial Price",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "final_price", orderable: false, title: "Final Price",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "status" , orderable:false , title :"Status",
                    mRender: function (data, type, row) {
                        return data==1?"Active":"Not Active";
                }},
                {
                    "data" : "action",
                    "orderable": false,
                    "className" : "text-center"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
    