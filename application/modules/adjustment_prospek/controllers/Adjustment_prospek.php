<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Adjustment_prospek extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_adjustment_prospek');
        $this->load->model('prospek/m_prospek');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K080";
        $header = "K059";
        $title = "Adjustment Prospek";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        CekModule($module);
        LoadTemplate($model, "adjustment_prospek/v_adjustment_prospek_index", $javascript);
        
    } 
    public function get_one_adjustment_prospek() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_adjustment_prospek', 'Adjustment Prospek', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_adjustment_prospek->GetOneAdjustment_prospek($model["id_adjustment_prospek"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Adjustment Prospek tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdataadjustment_prospek() {
        header('Content-Type: application/json');
        echo $this->m_adjustment_prospek->GetDataadjustment_prospek();
    }
    
    public function create_adjustment_prospek() 
    {
        $id_prospek = $this->input->get('id_prospek');
        $row=['button'=>'Add'];
        $javascript = array();
	    $module = "K080";
        $header = "K059";
        $row['title'] = "Add Adjustment Prospek";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_adjustment'] = array(array('id' => 1, 'text' => 'Chassis'), array('id' => 2, 'text' => 'Karoseri'), array('id' => 3, 'text' => 'BBN'));
        $prospek = $this->m_prospek->GetOneProspek($id_prospek);
        $prospek = json_decode(json_encode($prospek), true);//echo "<pre>";print_r($prospek);exit;
        $row['nomor_prospek'] = $prospek['no_prospek'];
        $row['hrg'] = array($prospek['harga_off_the_road'], 0, $prospek['biaya_bbn']);
        $row['id_prospek'] = $id_prospek;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
	    $this->load->view('adjustment_prospek/v_adjustment_prospek_manipulate', $row);       
    }
    
    public function adjustment_prospek_manipulate() 
    {
        $message='';

        $this->form_validation->set_rules('type_adjustment', 'Type Adjustment', 'trim|required');
        $this->form_validation->set_rules('initial_price', 'Initial Price', 'trim|required|numeric');
        $this->form_validation->set_rules('final_price', 'Final Price', 'trim|required|numeric');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_adjustment_prospek->Adjustment_prospekManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_adjustment_prospek($id=0) 
    {
        $row = $this->m_adjustment_prospek->GetOneAdjustment_prospek($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K080";
            $header = "K059";
            $row['title'] = "Edit Adjustment Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	        $this->load->view('adjustment_prospek/v_adjustment_prospek_manipulate', $row);
        } else {
            SetMessageSession(0, "Adjustment_prospek cannot be found in database");
            redirect(site_url('adjustment_prospek'));
        }
    }
    
    public function adjustment_prospek_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_adjustment_prospek', 'Adjustment_prospek', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_adjustment_prospek->Adjustment_prospekDelete($model['id_adjustment_prospek']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Adjustment_prospek.php */