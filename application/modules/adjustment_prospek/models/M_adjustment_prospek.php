<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_adjustment_prospek extends CI_Model
{

    public $table = '#_adjustment_prospek';
    public $id = 'id_adjustment_prospek';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDataadjustment_prospek() {
        $this->load->library('datatables');
        $this->datatables->select('id_adjustment_prospek,type_adjustment,initial_price,final_price,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_adjustment_prospek.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editadjustment_prospek($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteadjustment_prospek($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_adjustment_prospek');
        return $this->datatables->generate();
    }

    // get all
    function GetOneAdjustment_prospek($keyword, $type = 'id_adjustment_prospek') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $adjustment_prospek = $this->db->get($this->table)->row();
        return $adjustment_prospek;
    }

    function Adjustment_prospekManipulate($model) {
        try {
                $model['initial_price'] = DefaultCurrencyDatabase($model['initial_price']);
                $model['final_price'] = DefaultCurrencyDatabase($model['final_price']);

            if (CheckEmpty($model['id_adjustment_prospek'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		        return array("st" => true, "msg" => "Adjustment Prospek successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_adjustment_prospek" => $model['id_adjustment_prospek']));
		        return array("st" => true, "msg" => "Adjustment Prospek has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownAdjustment_prospek() {
        $listadjustment_prospek = GetTableData($this->table, 'id_adjustment_prospek', '', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listadjustment_prospek;
    }

    function Adjustment_prospekDelete($id_adjustment_prospek) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_adjustment_prospek' => $id_adjustment_prospek));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Adjustment Prospek has been deleted from database");
    }


}