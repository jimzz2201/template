<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Labarugi extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_labarugi');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/plugins/daterangepicker/daterangepicker.js";
        $css[] = "assets/plugins/daterangepicker/daterangepicker.css";
        $module = "K099";
        $header = "K005";
        $title = "Laba Rugi";
        $this->load->model("supplier/m_supplier");
        $this->load->model("cabang/m_cabang");
        $this->load->model("user/m_user");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['start_date']= GetDateNow();
        $model['end_date']= GetDateNow();
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_detail'] = array();
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $model['list_user'] = $this->m_user->GetDropDownUser();
        $model['result']=$this->m_labarugi->GetDataLabarugi(0, GetDateNow(), GetDateNow(),0);
        
        $model['start_date']= GetDateNow();
        $model['end_date']= GetDateNow();
        CekModule($module);
        LoadTemplate($model, "labarugi/v_labarugi_index", $javascript, $css);
    }
    public function createlabarugi(){
        
        
    }
    public function viewlabarugi(){
        $model=$this->input->post();
        $periode=$model['start_date'];
        $periode=explode("-", $periode);
        $startdate= GetDateNow();
        $enddate= GetDateNow();
        if(!CheckEmpty(@$periode[0]))
        {
            $startdate= DefaultTanggalDatabase(@$periode[0]);
        }
        if(!CheckEmpty(@$periode[1]))
        {
            $enddate= DefaultTanggalDatabase(@$periode[1]);
        }
        $model['start_date']=$startdate;
        $model['end_date']=$enddate;
        $model['result']=$this->m_labarugi->GetDataLabarugi(0, $startdate, $enddate,@$model['id_cabang']);
        
        $this->load->view("labarugi/v_labarugi_detail", $model);
    }
    


}

/* End of file Labarugi.php */