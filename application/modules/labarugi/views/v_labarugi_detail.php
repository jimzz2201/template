<style>
    .separatortr td{
        border-top:solid 1px #000 !Important;
        border-bottom:solid 1px #000 !Important;
    }
</style>
<div class="box box-default form-element-list">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="panel" data-collapsed="0">
                    <div class="panel-body">

                        <h1 style="text-align:center">LABA RUGI</h1>
                        <h4 style="text-align:center">PERIODE  
                            <?php
                            if ($start_date == $end_date) {
                                echo DefaultTanggal($start_date);
                            } else {
                                echo DefaultTanggal($start_date) . ' - ' . DefaultTanggal($end_date);
                            }
                            ?>
                        </h4>
                        <br/>
                        <br/>
                        <br/>
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6" style="overflow-x:scroll">

                            <table class="table table-striped table-bordered table-hover" id="mytable">

                                <tbody>

                                    <?php
                                    $totalpendapatan = 0;
                                    $totalhargapokokpenjualan = 0;
                                    $totalbiayaproduksi = 0;
                                    $totalpersediaanakhir = 0;
                                    $labakotor = 0;
                                    $biayaumumadministrasi = 0;
                                    $labarugisebelumenyusutan = 0;
                                    $totalpenyusutan = 0;
                                    $labarugisetelahpenyusutan = 0;
                                    $biayaproduksipluspersediaan = 0;
                                    $barangtersediauntukdijual = 0;
                                    foreach (@$result as $key => $row) {
                                        switch ($key) {
                                            case 1:
                                                $totalpendapatan = @$result[$key]['total'];
                                                break;
                                            case 2:
                                                $totalhargapokokpenjualan = @$result[$key]['total'];
                                                break;
                                            case 3:
                                                $totalbiayaproduksi = @$result[$key]['total'];
                                                $barangtersediauntukdijual = $totalhargapokokpenjualan - $totalbiayaproduksi;
                                                break;
                                            case 4:
                                                $biayaumumadministrasi = @$result[$key]['total'];
                                                break;
                                            case 5:
                                                $totalpersediaanakhir = @$result[$key]['total'];
                                                $biayaproduksipluspersediaan = $barangtersediauntukdijual + $totalpersediaanakhir;
                                                $labakotor = $totalpendapatan - $biayaproduksipluspersediaan;

                                                break;
                                            case 6:
                                                $totalpenyusutan = @$result[$key]['total'];
                                                break;
                                        }
                                    }
                                    GenerateGlAccount(@$result[1]);
                                    GenerateGlAccount(@$result[2]);
                                    GenerateGlAccount(@$result[3]);

                                    GenerateGlAccount(@$result[4]);
                                    echo "<tr >";
                                    echo "<td>&nbsp;</td>";
                                    echo "<td>BARANG TERSEDIA UNTUK DIJUAL</td>";
                                    echo "<td  align='right'>" . DefaultCurrencyAkuntansi($barangtersediauntukdijual) . "</td>";
                                    echo "<td>&nbsp;</td>";
                                    echo "</tr>";
                                    echo "<tr><td>&nbsp;</td><td>" . @$result[5]['name'] . "</td>";
                                    echo "<td  align='right'>" . DefaultCurrencyAkuntansi($row['total']) . "</td>";
                                    echo "<td>&nbsp;</td>";
                                    echo "</tr>";
                                    echo "<tr class='separatortr'>";
                                    echo "<td>&nbsp;</td>";
                                    echo "<td>&nbsp;</td>";
                                    echo "<td>&nbsp;</td>";
                                    echo "<td  align='right'>" . DefaultCurrencyAkuntansi($biayaproduksipluspersediaan) . "</td>";
                                    echo "</tr>";
                                    echo "<tr class='separatortr'>";
                                    echo "<td colspan='2'><b>LABA KOTOR</b></td>";
                                    echo "<td>&nbsp;</td>";
                                    echo "<td  align='right'>" . DefaultCurrencyAkuntansi($labakotor) . "</td>";
                                    echo "</tr>";
                                    $labarugisebelumenyusutan = $labakotor + $biayaumumadministrasi;
                                    echo "<tr >";
                                    echo "<td colspan=3>Laba Rugi Sebelum Penyusutan</td>";
                                    echo "<td  align='right'>" . DefaultCurrencyAkuntansi($labarugisebelumenyusutan) . "</td>";
                                    echo "</tr>";

                                    //GenerateGlAccount(@$result[6]);

                                    echo "<tr >";
                                    echo "<td colspan=3><b>LABA/RUGI SETELAH PENYUSUTAN</b></td>";
                                    echo "<td  align='right'>" . DefaultCurrencyAkuntansi($labarugisebelumenyusutan - $totalpenyusutan) . "</td>";
                                    echo "</tr>";
                                    ?>

                                </tbody>

                            </table>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

function GenerateGlAccount($row) {

    echo "<tr>";
    echo "<td colspan='4'><b>" . $row['name'] . "</b></td>";
    echo "</tr>";
    $kali = 1;
    if ($row['id_gl_labarugi'] == "3" || $row['id_gl_labarugi'] == "4") {
        $kali = -1;
    }
    foreach (@$row['listgl'] as $glsatuan) {
        echo "<tr>";
        echo "<td>&nbsp;</td>";
        echo "<td>" . $glsatuan['nama_gl_account'] . "</td>";
        echo "<td style='text-align:right'>" . DefaultCurrencyAkuntansi($kali * $glsatuan['sum']) . "</td>";
        echo "<td>&nbsp;</td>";
        echo "</tr>";
    }
    echo "<tr class='separatortr'>";
    if ($glsatuan['nama_header_labarugi'] != "BIAYA PRODUKSI" && $glsatuan['nama_header_labarugi'] != "HARGA POKOK PENJUALAN") {
        echo "<td colspan='3'>&nbsp;</td>";
        echo "<td align='right'>" . DefaultCurrencyAkuntansi($kali * $row['total']) . "</td>";
    } else {
        echo "<td colspan='2'>&nbsp;</td>";
        echo "<td align='right'>" . DefaultCurrencyAkuntansi($kali * $row['total']) . "</td>";
        echo "<td>&nbsp;</td>";
    }



    echo "</tr>";
    // return array("totaldet"=>$totaldet,"decimaldebet"=>$decimaldebet,'name'=>$name,'stringret'=>$stringret);
}
?>