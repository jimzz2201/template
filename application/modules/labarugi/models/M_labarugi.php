<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_labarugi extends CI_Model
{

    public $table = '#_barang';
    public $id = 'id_barang';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }
    
    function GetDropDownLabarugi() {
        $this->datatables->where_in("#_barang.id_category",[1,9]);
        $listbarang = GetTableData($this->table, 'id_barang', 'nama_barang', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null), "json", array("nama_barang", "asc"));
       
        return $listbarang;
    }
    
    function GetDataLabarugi($id_labarugi,$startdate,$enddate,$cabang)
    {
        $this->db->from("#_gl_header");
        $this->db->order_by("urut");
        $listgl=$this->db->get()->result();
        
        
        if($startdate==$enddate || DefaultTanggalDatabase($startdate)<'2019-05-01')
        {
            $startdate='2019-01-01';
        }
        
        $jointext = "#_gl_account.id_gl_account=#_buku_besar.id_gl_account and #_buku_besar.tanggal_buku>='" . DefaultTanggalDatabase($startdate) . "' and tanggal_buku<='" . DefaultTanggalDatabase($enddate) . "' ";
        if (!CheckEmpty($cabang)) {
            $jointext .= " and #_buku_besar.id_cabang =" . $cabang;
        }
        $this->db->from("#_gl_labarugi");
        $this->db->join("#_gl_account", "#_gl_account.id_gl_labarugi=#_gl_labarugi.id_gl_labarugi", "left");
        $this->db->join("#_buku_besar", $jointext, "left");
        $this->db->group_by("#_gl_account.id_gl_account");
        $this->db->select("ifnull(sum(kredit-debet),0) as sum , #_gl_labarugi.*,#_gl_account.id_gl_account,#_gl_account.nama_gl_account");
        $this->db->where("#_gl_labarugi.status", 1);
        $this->db->where("#_gl_account.id_gl_labarugi !=", null);
        $this->db->order_by("#_gl_labarugi.urut", "asc");
        $this->db->order_by("#_gl_account.urut_rugilaba", "asc");
        $listgl = $this->db->get()->result_array();
        
        $arrayreturn=[];
        foreach($listgl as $glsum)
        {
            if(!CheckKey($arrayreturn, $glsum['id_gl_labarugi']))
            {
                $arrayreturn[$glsum['id_gl_labarugi']]=[];
                $arrayreturn[$glsum['id_gl_labarugi']]['name']=$glsum['nama_header_labarugi'];
                $arrayreturn[$glsum['id_gl_labarugi']]['id_gl_labarugi']=$glsum['id_gl_labarugi'];
                $arrayreturn[$glsum['id_gl_labarugi']]['listgl']=[];
                $arrayreturn[$glsum['id_gl_labarugi']]['total'] =0;
            }
            $arrayreturn[$glsum['id_gl_labarugi']]['listgl'][]=$glsum;
            $arrayreturn[$glsum['id_gl_labarugi']]['total'] +=$glsum['sum'];
            
        }
       
        return $arrayreturn;
    }
    
    
    function GetOneLabarugi($id_barang,$isfull=true)
    {
        $this->db->from($this->table);
        $this->db->where(array("id_barang"=>$id_barang));
        if($isfull)
        {
             $this->db->join("#_satuan","#_barang.id_satuan=#_satuan.id_satuan");
             $this->db->select($this->table.".*,nama_satuan");
        }
        $labarugi=$this->db->get()->row();
        
        if($isfull&&$labarugi)
        {
            $this->db->from("#_barang_items");
            $this->db->select("#_barang_items.*,nama_satuan,no_part,kode_barang,_barang.nama_barang");
            $this->db->join("#_barang","#_barang.id_barang=#_barang_items.id_barang");
            $this->db->join("#_satuan","#_barang.id_satuan=#_satuan.id_satuan");
            
            $this->db->where(array("#_barang_items.id_barang_parent"=>$id_barang));
            $labarugi->list_detail=$this->db->get()->result();
        }
        
        return $labarugi;
    }

}