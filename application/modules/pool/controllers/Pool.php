<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pool extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_pool');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K079";
        $header = "K001";
        $title = "Pool";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	CekModule($module);
	LoadTemplate($model, "pool/v_pool_index", $javascript);
        
    } 
    public function get_one_pool() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_pool', 'Pool', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_pool->GetOnePool($model["id_pool"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Pool tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatapool() {
        header('Content-Type: application/json');
        echo $this->m_pool->GetDatapool();
    }
    
    public function create_pool() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	$module = "K079";
        $header = "K001";
        $row['title'] = "Add Pool";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	LoadTemplate($row,'pool/v_pool_manipulate', $javascript);       
    }
    
    public function pool_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('nama_pool', 'Nama Pool', 'trim|required');
	$this->form_validation->set_rules('alamat_pool', 'Alamat Pool', 'trim|required');
	$this->form_validation->set_rules('contact_person_pool', 'Contact Person Pool', 'trim|required');
	$this->form_validation->set_rules('telp_pool', 'Telp Pool', 'trim|required');
	$this->form_validation->set_rules('email_pool', 'Email Pool', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_pool->PoolManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_pool($id=0) 
    {
        $row = $this->m_pool->GetOnePool($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	    $module = "K079";
            $header = "K001";
            $row['title'] = "Edit Pool";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    LoadTemplate($row,'pool/v_pool_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Pool cannot be found in database");
            redirect(site_url('pool'));
        }
    }
    
    public function pool_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_pool', 'Pool', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_pool->PoolDelete($model['id_pool']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Pool.php */