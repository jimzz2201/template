<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_pool extends CI_Model
{

    public $table = '#_pool';
    public $id = 'id_pool';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatapool() {
        $this->load->library('datatables');
        $this->datatables->select('id_pool,nama_pool,alamat_pool,contact_person_pool,telp_pool,email_pool,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_pool.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('pool/edit_pool/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletepool($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_pool');
        return $this->datatables->generate();
    }

    // get all
    function GetOnePool($keyword, $type = 'id_pool') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $pool = $this->db->get($this->table)->row();
        return $pool;
    }

    function PoolManipulate($model) {
        try {

            if (CheckEmpty($model['id_pool'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		SetMessageSession(1, 'Pool successfull added into database');
		return array("st" => true, "msg" => "Pool successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_pool" => $model['id_pool']));
		SetMessageSession(1, 'Pool has been updated');
		return array("st" => true, "msg" => "Pool has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownPool($wherein=[],$wherenotin=[]) {
        if(count($wherein)>0)
        {
            $this->db->where_in("id_pool",$wherein);
        }
        if(count($wherenotin)>0)
        {
            $this->db->where_not_in("id_pool",$wherenotin);
        }
        $listpool = GetTableData($this->table, 'id_pool', 'nama_pool', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listpool;
    }

    function PoolDelete($id_pool) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_pool' => $id_pool));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Pool has been deleted from database");
    }


}