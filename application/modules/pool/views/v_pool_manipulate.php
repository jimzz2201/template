<section class="content-header">
    <h1>
        Pool <?= @$button ?>
        <small>Pool</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Pool</li>
        <li class="active">Pool <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_pool" class="form-horizontal form-groups-bordered validate" method="post">
                            <input type="hidden" name="id_pool" value="<?php echo @$id_pool; ?>" /> 
                            <div class="form-group">
                                <?= form_label('Nama Pool', "txt_nama_pool", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'nama_pool', 'value' => @$nama_pool, 'class' => 'form-control', 'id' => 'txt_nama_pool', 'placeholder' => 'Nama Pool')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Alamat Pool', "txt_alamat_pool", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                <?= form_textarea(array('type' => 'text', 'name' => 'alamat_pool', 'rows' => '3', 'cols' => '10', 'value' => @$alamat_pool, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Contact Person Pool', "txt_contact_person_pool", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'contact_person_pool', 'value' => @$contact_person_pool, 'class' => 'form-control', 'id' => 'txt_contact_person_pool', 'placeholder' => 'Contact Person Pool')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Telp Pool', "txt_telp_pool", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'telp_pool', 'value' => @$telp_pool, 'class' => 'form-control', 'id' => 'txt_telp_pool', 'placeholder' => 'Telp Pool')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Email Pool', "txt_email_pool", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'email_pool', 'value' => @$email_pool, 'class' => 'form-control', 'id' => 'txt_email_pool', 'placeholder' => 'Email Pool')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="<?php echo base_url().'index.php/pool' ?>" class="btn btn-default"  >Cancel</a>
                                <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
$("#frm_pool").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pool/pool_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/pool';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>