<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jabatan extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_jabatan');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K050";
        $header = "K001";
        $title = "Jabatan";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        LoadTemplate($model, "jabatan/v_jabatan_index", $javascript);
    }

    public function get_one_jabatan() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_jabatan', 'Jabatan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_jabatan->GetOneJabatan($model["id_jabatan"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Jabatan tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdatajabatan() {
        header('Content-Type: application/json');
        echo $this->m_jabatan->GetDatajabatan();
    }

    public function create_jabatan() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K050";
        $header = "K001";
        $row['title'] = "Add Jabatan";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $this->load->view('jabatan/v_jabatan_manipulate', $row);
    }

    public function jabatan_manipulate() {
        $message = '';

        $this->form_validation->set_rules('kode_jabatan', 'Kode Jabatan', 'trim|required');
        $this->form_validation->set_rules('nama_jabatan', 'Nama Jabatan', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_jabatan->JabatanManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_jabatan($id = 0) {
        $row = $this->m_jabatan->GetOneJabatan($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K050";
            $header = "K001";
            $row['title'] = "Edit Jabatan";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->view('jabatan/v_jabatan_manipulate', $row);
        } else {
            SetMessageSession(0, "Jabatan cannot be found in database");
            redirect(site_url('jabatan'));
        }
    }

    public function jabatan_delete() {
        $message = '';
        $this->form_validation->set_rules('id_jabatan', 'Jabatan', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_jabatan->JabatanDelete($model['id_jabatan']);
        }

        echo json_encode($result);
    }

}

/* End of file Jabatan.php */