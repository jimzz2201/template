<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_jabatan extends CI_Model
{

    public $table = '#_jabatan';
    public $id = 'id_jabatan';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatajabatan() {
        $this->load->library('datatables');
        $this->datatables->select('id_jabatan,kode_jabatan,nama_jabatan,status,level');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_jabatan.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editjabatan($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletejabatan($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_jabatan');
        return $this->datatables->generate();
    }

    // get all
    function GetOneJabatan($keyword, $type = 'id_jabatan') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $jabatan = $this->db->get($this->table)->row();
        return $jabatan;
    }

    function JabatanManipulate($model) {
        try {
                $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_jabatan'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		return array("st" => true, "msg" => "Jabatan successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_jabatan" => $model['id_jabatan']));
		return array("st" => true, "msg" => "Jabatan has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownJabatan() {
        $listjabatan = GetTableData($this->table, 'id_jabatan', 'nama_jabatan', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listjabatan;
    }

    function JabatanDelete($id_jabatan) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_jabatan' => $id_jabatan));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Jabatan has been deleted from database");
    }


}