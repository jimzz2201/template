<div class="modal-header">
    Jabatan <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_jabatan" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_jabatan" value="<?php echo @$id_jabatan; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Jabatan', "txt_kode_jabatan", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?php
                $mergearray = array();
                if (!CheckEmpty(@$id_jabatan)) {
                    $mergearray['readonly'] = "readonly";
                    $mergearray['name'] = "kode_jabatan";
                } else {
                    $mergearray['name'] = "kode_jabatan";
                }
                ?>
                <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_jabatan, 'class' => 'form-control', 'id' => 'txt_kode_jabatan', 'placeholder' => 'Kode Jabatan'))); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Jabatan', "txt_nama_jabatan", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'nama_jabatan', 'value' => @$nama_jabatan, 'class' => 'form-control', 'id' => 'txt_nama_jabatan', 'placeholder' => 'Nama Jabatan')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Level', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$level, "name" => "level"), DefaultEmptyDropdown(GetLevel(),"json","Level"), @$level, array('class' => 'form-control', 'id' => 'level')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $("#frm_jabatan").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/jabatan/jabatan_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>