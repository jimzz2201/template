<section class="content-header">
    <h1>
        DP / HUTANG <?= @$button ?>
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">DP / HUTANG <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <form id="frm_pembelian" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_dp_history" value="<?php echo @$id_dp_history; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Customer', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_customer"), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer, array('class' => 'form-control', 'id' => 'dd_id_customer')); ?>
                                    </div>    



                                </div>

                                <div class="form-group">
                                    <?= form_label('Total DP', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$dp), 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_dp', 'placeholder' => 'DP', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?= form_label('Type Pembayaran', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "jenis_pembayaran"), DefaultEmptyDropdown(@$list_type_pembayaran, "", "Pembayaran"), @$jenis_pembayaran, array('class' => 'form-control select2', 'id' => 'dd_jenis_pembayaran')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Jenis', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "jenis_dp"), DefaultEmptyDropdown(@$list_jenis_dp, "", "Jenis DP"), @$jenis_dp, array('class' => 'form-control select2', 'id' => 'dd_jenis_dp')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?php
                                    $listbank = DefaultEmptyDropdown(@$listbank, "json", "Bank");
                                    ?>
                                    <?= form_label('Nama Bank', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array(), @$listbank, @$id_bank, array("disabled" => "disabled","name"=>"id_bank", 'class' => 'form-control', 'id' => 'dd_id_bank')); ?>
                                    </div>
                                </div>
                                 <div class="form-group areahutang" style="display:none">
                                    <?= form_label('Total Hutang', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$hutang), 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_hutang', 'placeholder' => 'Hutang', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div> 




                                <div class="form-group">

                                    <?= form_label('Jumlah', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'nominal', 'value' => DefaultCurrency(@$nominal), 'class' => 'form-control', 'id' => 'txt_nominal', 'placeholder' => 'Nominal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>


                                </div>

                                <div class="form-group" style="margin-top:50px">
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>

    $(document).ready(function () {
        $("select").select2();
        $("#dd_jenis_pembayaran,#dd_jenis_dp").on("select2:select", function (e) {
            var jenis=$("#dd_jenis_pembayaran").val();
            var jenis_dp=$("#dd_jenis_dp").val();
            CekSectionPart(jenis,jenis_dp);
        });
        function CekSectionPart(jenis,jenis_pembayaran)
        {
            
            if (jenis == "Hutang" ||jenis_pembayaran.includes("Hutang"))
            {
               $(".areahutang").css("display", "block"); 
            }
            else
            {
                $(".areahutang").css("display", "none"); 
            }
        
            if (jenis == "Cash" || jenis == "0")
            {
                $("#dd_id_bank").attr("disabled", "disabled");
            } 
            else
            {
                $("#dd_id_bank").removeAttr("disabled");
            }

        }
        $("#txt_tanggal").datepicker();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $("#dd_id_customer").change(function () {
            var id_customer = $(this).val();
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/customer/get_one_customer',
                dataType: 'json',
                data: {
                    id_customer: id_customer
                },
                success: function (data) {
                    if(data.st)
                    {
                      $("#txt_dp").val(Comma(data.obj.dp));
                      $("#txt_hutang").val(Comma(data.obj.hutang));
                       
                    }
                    else
                    {
                      $("#txt_dp").val("0");
                      $("#txt_hutang").val("0");
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        })

        $("#frm_pembelian").submit(function () {

            swal({
                title: "Apakah kamu yakin ingin menginput data DP berikut?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                closeOnConfirm: true
            }).then((result) => {
                if (result.value)
                {
                    $.ajax({
                        type: 'POST',
                        url: baseurl + 'index.php/dp/dp_manipulate',
                        dataType: 'json',
                        data: $(this).serialize(),
                        success: function (data) {
                            if (data.st)
                            {
                                window.location.href = baseurl + 'index.php/dp';
                            } else
                            {
                                messageerror(data.msg);
                            }

                        },
                        error: function (xhr, status, error) {
                            messageerror(xhr.responseText);
                        }
                    });
                }
            });




            return false;

        })
    })
</script>