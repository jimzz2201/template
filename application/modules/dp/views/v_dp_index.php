
<section class="content-header">
    <h1>
        DP / HUTANG
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">DP / HUTANG</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), @$list_pencarian); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                        </div>


                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'selected' => @$id_cabang, 'class' => 'form-control', 'id' => 'id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                       <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'selected' => @$status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "obj", "Status")); ?>
                        </div>
                         
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('&nbsp;', "dd_id_supplier", array("class" => 'col-sm-1 control-label')); ?>
                       <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>


                        <div class="col-sm-2">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                    </div>
                </div>



            </form>
            <div class=" headerbutton">

                <?php echo anchor(site_url('dp/create_dp'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    function bataldp(id_dp_history) {
        swal({
            title: "Apakah kamu yakin akan membatalkan dp berikut ini?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/dp/dp_batal',
                    dataType: 'json',
                    data: {
                        id_dp_history: id_dp_history
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }
     $("form#frm_search").submit(function () {
        table.fnDraw(false);
        return false;
    })
    $(document).ready(function () {

        $(".datepicker").datepicker();
        $("select").select2();
         $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            sortable: false,
            ajax: {"url": "dp/getdatadp", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                // {data: "id_tipe_penyewa" , orderable:false , title :"Id Tipe Penyewa"},
                {data: "kode_customer", orderable: false, title: "Kode"},
                {data: "nama_customer", orderable: false, title: "Nama"},
                {data: "kode_dp", orderable: false, title: "Kode"},
                {data: "jenis_dp", orderable: false, title: "Jenis"},
                {data: "tanggal", orderable: false, title: "Tanggal", mRender: function (data, type, row) {
                        return DefaultDateFormat(data);
                    }},
                {data: "nominal", orderable: false, title: "Nominal", mRender: function (data, type, row) {
                        return Comma(data);
                    }},
                {data: "jenis_pembayaran", orderable: false, title: "Pembayaran"},
                {data: "keterangan", orderable: false, title: "Keterangan"},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        return data == 1 ? "" : "Batal";
                    }},
                {data: "nama_cabang", orderable: false, title: "Cabang"},
                {
                    "data": "action",
                    width: "100px",
                    title: "&nbsp;",
                    "orderable": false,
                    "className": "text-center",
                    mRender: function (data, type, row) {
                        return row['status'] != 2 ? data : "Batal";
                    }},
            ],
            order: [[0, 'desc']]
        });
    });
</script>
