<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dp extends CI_Controller {

    private $_sufix = "_dp";

    function __construct() {
        parent::__construct();
        $this->load->model('m_dp');
        $this->load->model('cabang/m_cabang');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K106";
        $header = "K059";
        $title = "DP";
        $this->load->model("customer/m_customer");
        $this->load->model("cabang/m_cabang");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
       
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_do", "text" => "Tanggal DP");
        $model['start_date'] = GetCookieSetting("start_date" . $this->_sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $this->_sufix, GetDateNow());
        $model['list_status'] = $status;
        $model['status'] = GetCookieSetting("status" . $this->_sufix, 0);
        $model['list_pencarian'] = $jenis_pencarian;
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $this->_sufix, "created_date");
        $model['id_cabang'] = GetCookieSetting("id_cabang" . $this->_sufix, GetIdCabang());
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        LoadTemplate($model, "dp/v_dp_index", $javascript);
    }

    public function get_one_dp() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_dp', 'Customer', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_dp->GetOneCustomer($model["id_dp"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Customer tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function get_dropdown_dp() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_dp = $this->m_dp->GetDropDownCustomer(@$model['id_pegawai']);
        echo json_encode(array("list_dp" => DefaultEmptyDropdown($list_dp, "json", "Customer"), "st" => true));
    }

    public function getdatadp() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_sufix, 1);
            }
            SetCookieSetting($key . $this->_sufix, $value);
        }
        echo $this->m_dp->GetDatadp($params);
    }

    public function create_dp() {
        $row = ['button' => 'Add'];
        $this->load->model("pegawai/m_pegawai");
        $javascript = array();
        $css = array();
        $module = "K106";
        $header = "K059";
        $row['title'] = "Add DP / HUTANG";
        CekModule($module);
        $row['form'] = $header;
        $this->load->model("bank/m_bank");
        $row['formsubmenu'] = $module;
        $row['tanggal'] = GetDateNow();
        $jenis_pembayaran = array();
        $jenis_pembayaran["Cash"] = "Cash";
        $jenis_pembayaran["Transfer"] = "Transfer";
        $jenis_pembayaran["Hutang"] = "Hutang";
        $jenis_dp = array();
        $jenis_dp["Penambahan DP"] = "Penambahan DP";
        $jenis_dp["Pengembalian DP"] = "Pengembalian DP";
        $jenis_dp["Pengurangan DP"] = "Pengurangan DP";
        $jenis_dp["Penambahan Hutang"] = "Penambahan Hutang";
        $jenis_dp["Pengurangan Hutang"] = "Pengurangan Hutang";
        $row['list_jenis_dp'] = $jenis_dp;
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $row['listbank'] = $this->m_bank->GetDropDownBank();
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        if (count($row['list_cabang']) == 1) {
            $row['id_cabang'] = $row['list_cabang'][0]['id'];
        }
        LoadTemplate($row, 'dp/v_dp_manipulate', $javascript, $css);
    }

    public function dp_manipulate() {
        $message = '';
        $model = $this->input->post();
        
        // $this->form_validation->set_rules('id_tipe_penyewa', 'Id Tipe Penyewa', 'trim|required');
        //$this->form_validation->set_rules('kode_dp', 'Kode Customer', 'trim|required');
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('nominal', 'Nominal', 'trim|required');
        $this->form_validation->set_rules('jenis_pembayaran', 'Jenis Pembayaran', 'trim|required');
        $this->form_validation->set_rules('jenis_dp', 'Jenis DP', 'trim|required');
        if ($model['jenis_pembayaran'] == "Transfer") {
            $this->form_validation->set_rules('id_bank', 'Bank', 'trim|required');
        }
        if (($model['jenis_dp'] == "Pengurangan Hutang" || $model['jenis_dp'] == "Penambahan Hutang")&&$model['jenis_pembayaran'] == "Hutang"  ) {
            $message .= "Hutang tidak dapat disalurkan kembali ke hutang<br/>";
        }

        // $this->form_validation->set_rules('id_cabang', 'Id Cabang', 'trim|required');
        if (!CheckEmpty(@$model['id_customer'])) {
            $this->load->model("customer/m_customer");
            $customer = $this->m_customer->GetOneCustomer($model['id_customer']);
            if ($model['jenis_pembayaran'] == "Hutang" || $model['jenis_pembayaran'] == "Pengurangan Hutang") {
                if ($customer->hutang < DefaultCurrencyDatabase($model['nominal'])) {
                    $message .= "Total Saldo Customer tidak mencukupi<br/>";
                }
            }
            
            if ($model['jenis_dp'] == "Pengurangan DP" || $model['jenis_dp'] == "Pengembalian DP") {
                if ($customer->dp < DefaultCurrencyDatabase($model['nominal'])) {
                    $message .= "Total DP Customer tidak mencukupi<br/>";
                }
            }
        }
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_dp->DPManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_dp($id = 0) {

        $row = $this->m_dp->GetOneDP($id);

        if ($row) {
            $row->button = 'Update';
            $this->load->model("bank/m_bank");
            $this->load->model("customer/m_customer");
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K106";
            $header = "K059";
            $row['title'] = "Edit DP / HUTANG";
            CekModule($module);
            $jenis_pembayaran["Cash"] = "Cash";
            $jenis_pembayaran["Transfer"] = "Transfer";
            $jenis_pembayaran["Hutang"] = "Hutang";
            $jenis_dp = array();
            $jenis_dp["Penambahan DP"] = "Penambahan DP";
            $jenis_dp["Pengembalian DP"] = "Pengembalian DP";
            $jenis_dp["Pengurangan DP"] = "Pengurangan DP";
            $jenis_dp["Penambahan Hutang"] = "Penambahan Hutang";
            $jenis_dp["Pengurangan Hutang"] = "Pengurangan Hutang";
            $row['list_jenis_dp'] = $jenis_dp;
            $row['list_type_pembayaran'] = $jenis_pembayaran;
            $row['listbank'] = $this->m_bank->GetDropDownBank();
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer(null, $row['id_customer']);
            if (count($row['list_cabang']) == 1) {
                $row['id_cabang'] = $row['list_cabang'][0]['id'];
            }
            $css = [];
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'dp/v_dp_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Customer cannot be found in database");
            redirect(site_url('dp'));
        }
    }

    public function dp_batal() {
        $message = '';
        $this->form_validation->set_rules('id_dp_history', 'Customer', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_dp->BatalDP($model['id_dp_history']);
        }

        echo json_encode($result);
    }

    public function add_alamat() {
        $key = $this->input->post('key');

        $this->load->view('dp/v_dp_address_book', array("key" => $key));
    }

    public function edit_alamat_dp($id = 0) {
        $row = $this->m_dp->GetOneAlamatCustomer($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K014";
            $header = "K001";
            $row['title'] = "Edit Alamat";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->view('dp/v_add_alamat', $row);
        } else {
            SetMessageSession(0, "alamat cannot be found in database");
            redirect(site_url('dp'));
        }
    }

    public function alamat_manipulate() {
        $message = '';
        $this->form_validation->set_rules('nama_alamat', 'Nama Alamat', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('contact_person', 'Contact Person', 'trim|required');
        $this->form_validation->set_rules('telp_cp', 'Telp Contact Person', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');
        $this->form_validation->set_rules('id_dp', 'id dp', 'trim');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_dp->AlamatManipulate($model);
            echo json_encode($status);
        }
    }

    public function get_alamat() {
        $id_dp = $this->input->post('id_dp');
        $data = $this->m_dp->GetAlamatCustomer($id_dp);
        $html = '';
        foreach ($data as $val) {
            $id = $val['id_address'];
            $html .= "<div class='form-group'>
                        <label for='txt_nama_dp' class='col-sm-2 control-label'>" . $val['nama_alamat'] . "<br>
                        <a href='javascript:;' class='btn btn-primary btn-xs' onclick='editAlamatCustomer(" . $val['id_address'] . ");return false;'>Update</a>
                        <a href='javascript:;' class='btn btn-danger btn-xs' onclick='deleteAlamat(" . $val['id_address'] . ");return false;'>Delete</a>
                        </label>
                        <div class='col-sm-10'>
                            <textarea name='alamat' disabled='disabled' cols='10' rows='3' type='text' value='' class='form-control' id='txt_alamat' placeholder='Alamat'>" . $val['alamat'] . "</textarea>
                        </div>
                      </div>";
        }
        echo $html;
    }

    public function alamat_dp_delete() {
        $message = '';
        $this->form_validation->set_rules('id_alamat', 'Alamat', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_dp->AlamatCustomerDelete($model['id_alamat']);
        }

        echo json_encode($result);
    }

    public function search_dp() {
        $term = $this->input->post('term');
        $resp = [];

        if (strlen($term) >= 3) {
            $term = strtolower($term);
            $where = "(LOWER(nama_dp) LIKE '%" . $term . "%') AND status = 1";
            $resp['results'] = GetTableData('#_dp', 'id_dp', ['kode_dp', 'nama_dp'], $where);
        }
        echo json_encode($resp);
    }

}

/* End of file Customer.php */