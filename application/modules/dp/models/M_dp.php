<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_dp extends CI_Model {

    public $table = '#_dp';
    public $id = 'id_dp';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatadp($params) {
        $this->load->library('datatables');
        $this->datatables->select('kode_dp,nama_customer,id_dp_history,#_customer_dp_history.*,nama_cabang,kode_customer');
        $this->datatables->from("#_customer_dp_history");
        //add this line for join
        $this->datatables->join('#_cabang', '#_cabang.id_cabang = #_customer_dp_history.id_cabang', 'left');
        $this->datatables->join('#_customer', '#_customer.id_customer = #_customer_dp_history.id_customer', 'left');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $extra = array();
        $search = [];

        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $where['#_customer_dp_history.id_customer'] = $params['id_customer'];
        }
        if (isset($params['status']) && !CheckEmpty($params['status'])) {
            $where['#_customer_dp_history.status'] = $params['status'];
        }
        if (isset($params['id_cabang']) && !CheckEmpty($params['id_cabang'])) {
            $where['#_customer_dp_history.id_cabang'] = $params['id_cabang'];
        }

        $tanggal = "#_customer_dp_history.tanggal";
        if ($params['jenis_pencarian'] == "created_date") {
            $tanggal = "date(#_customer_dp_history.created_date)";
        } else if ($params['jenis_pencarian'] == "updated_date") {
            $tanggal = "date(#_customer_dp_history.updated_date)";
        } else if ($params['jenis_pencarian'] == "tanggal") {
            $tanggal = "#_customer_dp_history.tanggal";
        }
        $this->db->order_by($tanggal, "desc");
        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        } else {

            if (isset($params['start_date']) && empty($params['end_date'])) {
                $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                unset($params['start_date']);
            } else {
                $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                unset($params['end_date']);
            }
        }


        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }

        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "bataldp($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_dp_history');
        $listresult= $this->datatables->generate();
        return $listresult;
    }

    function BatalDP($id_dp_history) {
        $message = "";
        $this->load->model("customer/m_customer");
        $dp = $this->GetOneDP($id_dp_history);

        $customer = null;
        if (CheckEmpty($dp)) {
            $message .= "DP tidak terdapat dalam database";
        } else {
            $customer = $this->m_customer->GetOneCustomer($dp->id_customer);
            if ($customer->dp < $dp->nominal) {
                $message .= "DP sudah terpakai , data tidak bisa dibatalkan";
            }
        }

        if ($message == "") {
            try {
                $this->db->trans_rollback();
                $this->db->trans_begin();
                $userid = GetUserId();

                $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)), array("dokumen" => $dp->kode_dp));
                $this->db->update("#_customer_dp_history", MergeUpdate(array("status" => 2)), array("id_dp_history" => $id_dp_history));

                $this->m_customer->KoreksiSaldoPiutang($dp->id_customer, null, true);

                if ($this->db->trans_status() === FALSE || $message != '') {
                    $this->db->trans_rollback();
                    $message = "Some Error Occured<br/>" . $message;
                    return array("st" => false, "msg" => $message);
                } else {
                    $this->db->trans_commit();
                    $arrayreturn['msg'] = "Data Pembayaran Sudah di batalkan";
                    $arrayreturn["st"] = true;
                    //SetPrint($id_penjualan, $penjualan['nomor_master'], 'penjualan');
                    return $arrayreturn;
                }
            } catch (Exception $ex) {
                $this->db->trans_rollback();
                return array("st" => false, "msg" => $ex->getMessage());
            }
        } else {
            return array("st" => false, "msg" => $message);
        }
    }

    // get all
    function GetOneDP($keyword, $type = 'id_dp_history', $isfull = true) {
        $this->db->where($type, $keyword);
        $dp = $this->db->get("#_customer_dp_history")->row();

        return $dp;
    }

    function DPManipulate($model) {
        $jenis_pencarian="created_date";
        try {
            $userid = GetUserId();
            $dp = [];
            if ($model['jenis_pembayaran'] == "Transfer") {
                $dp['id_bank'] = ForeignKeyFromDb($model['id_bank']);
            }
            $dp['jenis_pembayaran'] = $model['jenis_pembayaran'];
            $dp['jenis_dp'] = $model['jenis_dp'];
            $dp['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $dp['keterangan'] = $model['keterangan'];
            $dp['nominal'] = DefaultCurrencyDatabase($model['nominal']);
            $dp['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $dp['id_customer'] = ForeignKeyFromDb($model['id_customer']);
            $this->load->model("bank/m_bank");
            $this->load->model("gl_config/m_gl_config");
            $bank = $this->m_bank->GetOneBank(@$model['id_bank']);
            $id_dp_history = 0;
            $kode_dp = "";
            $this->load->model("customer/m_customer");
            $this->db->trans_begin();
            $customer = $this->m_customer->GetOneCustomer($model['id_customer']);
            if (CheckEmpty(@$model['id_dp_history'])) {
                $pattern = 'DP' . "/" . date("y") . "/";
                $kode_dp = AutoIncrement("#_customer_dp_history", $pattern, "kode_dp", 4);
                $dp['kode_dp'] = $kode_dp;
                $dp = MergeCreated($dp);
                $this->db->insert("#_customer_dp_history", $dp);
                $id_dp_history = $this->db->insert_id();
            } else {
                $id_dp_history = $model['id_dp_history'];
                $dp = MergeUpdate($dp);
                $jenis_pencarian="updated_date";
                $dpobj = $this->GetOneDP($id_dp_history);
                if ($dpobj) {
                    $kode_dp = $dpobj->kode_dp;
                }
                $this->db->update("#_customer_dp_history", $dp, array("id_dp_history" => $id_dp_history));
            }
            

            
            $akunglkas = $this->m_gl_config->GetIdGlConfig("kasbesar", $model['id_cabang'], array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglkas, "dokumen" => $kode_dp));
            $akun_kas = $this->db->get()->row();
            $arrglakun = [];
            $num1 = $dp['nominal'];
            $num2 = 0;

            if ($dp['jenis_dp'] == "Pengembalian DP"||$dp['jenis_dp'] == "Pengurangan DP"||$dp['jenis_dp'] == "Pengurangan Hutang") {
                $num1 = 0;
                $num2 = $dp['nominal'];
            }


            if (CheckEmpty($akun_kas)&& $model['jenis_pembayaran'] == "Cash") {
                $akunkasinsert = array();
                $akunkasinsert['id_gl_account'] = $akunglkas;
                $akunkasinsert['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akunkasinsert['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunkasinsert['dokumen'] = $kode_dp;
                $akunkasinsert['subject_name'] = @$customer->kode_customer;
                $akunkasinsert['id_subject'] = ForeignKeyFromDb($model['id_customer']);
                $akunkasinsert['debet'] = $num1;
                $akunkasinsert['kredit'] = $num2;
                $akunkasinsert['created_date'] = GetDateNow();
                $akunkasinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunkasinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_kas) && $model['jenis_pembayaran'] == "Cash") {
                $akunkasupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akunkasupdate['keterangan'] = $dp['jenis_dp']." ". @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunkasupdate['dokumen'] = $kode_dp;
                $akunkasupdate['subject_name'] = @$customer->kode_customer;
                $akunkasupdate['id_subject'] = ForeignKeyFromDb($model['id_customer']);
                $akunkasupdate['debet'] = $num1;
                $akunkasupdate['kredit'] = $num2;
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
                $arrglakun[] = $akun_kas->id_buku_besar;
            } else if (!CheckEmpty($akun_kas) && $model['jenis_pembayaran'] != "Cash") {
                $akunkasupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akunkasupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunkasupdate['dokumen'] = $kode_dp;
                $akunkasupdate['subject_name'] = @$customer->kode_customer;
                $akunkasupdate['id_subject'] = ForeignKeyFromDb($model['id_customer']);
                $akunkasupdate['debet'] = 0;
                $akunkasupdate['kredit'] = 0;
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
                $arrglakun[] = $akun_kas->id_buku_besar;
            }


            $akunglbank = $this->m_gl_config->GetIdGlConfig("bank", $model['id_cabang'], array("bank" => @$model['id_bank']));
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglbank, "dokumen" => $kode_dp));
            $akun_bank = $this->db->get()->row();

            if (CheckEmpty($akun_bank) && $model['jenis_pembayaran'] == "Transfer") {
                $akunbankinsert = array();
                $akunbankinsert['id_gl_account'] = $akunglbank;
                $akunbankinsert['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                // $akunbankinsert['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunbankinsert['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunbankinsert['dokumen'] = $kode_dp;
                $akunbankinsert['subject_name'] = @$bank->kode_bank;
                $akunbankinsert['id_subject'] = ForeignKeyFromDb(@$model['id_bank']);
                $akunbankinsert['debet'] = $num1;
                $akunbankinsert['kredit'] = $num2;
                $akunbankinsert['created_date'] = GetDateNow();
                $akunbankinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunbankinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_bank) && $model['jenis_pembayaran'] == "Transfer") {
                $akunbankupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                // $akunbankupdate['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunbankupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunbankupdate['dokumen'] = $kode_dp;
                $akunbankupdate['subject_name'] = @$bank->kode_bank;
                $akunbankupdate['id_subject'] = ForeignKeyFromDb(@$model['id_bank']);
                $akunbankupdate['debet'] = $num1;
                $akunbankupdate['kredit'] = $num2;
                $akunbankupdate['updated_date'] = GetDateNow();
                $akunbankupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunbankupdate, array("id_buku_besar" => $akun_bank->id_buku_besar));
                $arrglakun[] = $akun_bank->id_buku_besar;
            } else if (!CheckEmpty($akun_bank) && $model['jenis_pembayaran'] != "Transfer") {
                $akunbankupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                // $akunbankupdate['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunbankupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunbankupdate['dokumen'] = $kode_dp;
                $akunbankupdate['subject_name'] = @$bank->kode_bank;
                $akunbankupdate['id_subject'] = ForeignKeyFromDb(@$model['id_bank']);
                $akunbankupdate['debet'] = 0;
                $akunbankupdate['kredit'] = 0;
                $akunbankupdate['updated_date'] = GetDateNow();
                $akunbankupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunbankupdate, array("id_buku_besar" => $akun_bank->id_buku_besar));
                $arrglakun[] = $akun_bank->id_buku_besar;
            }
            

            $akunglhutang = $this->m_gl_config->GetIdGlConfig("hutangcustomer", $model['id_cabang']);
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglhutang, "dokumen" => $kode_dp));
            $akun_hutang = $this->db->get()->row();

            if (CheckEmpty($akun_hutang) && $model['jenis_pembayaran'] == "Hutang") {
                $akunhutanginsert = array();
                $akunhutanginsert['id_gl_account'] = $akunglhutang;
                $akunhutanginsert['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                // $akunbankinsert['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunhutanginsert['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunhutanginsert['dokumen'] = $kode_dp;
                $akunhutanginsert['subject_name'] = @$customer->kode_customer;
                $akunhutanginsert['id_subject'] = ForeignKeyFromDb(@$model['id_customer']);
                $akunhutanginsert['debet'] = $num1;
                $akunhutanginsert['kredit'] = $num2;
                $akunhutanginsert['created_date'] = GetDateNow();
                $akunhutanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunhutanginsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_hutang) && $model['jenis_pembayaran'] == "Hutang") {
                $akunhutangupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                // $akunbankupdate['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunhutangupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunhutangupdate['dokumen'] = $kode_dp;
                $akunhutangupdate['subject_name'] = @$bank->kode_bank;
                $akunhutangupdate['id_subject'] = ForeignKeyFromDb(@$model['id_bank']);
                $akunhutangupdate['debet'] = $num1;
                $akunhutangupdate['kredit'] = $num2;
                $akunhutangupdate['updated_date'] = GetDateNow();
                $akunhutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunhutangupdate, array("id_buku_besar" => $akun_hutang->id_buku_besar));
                $arrglakun[] = $akun_bank->id_buku_besar;
            } else if (!CheckEmpty($akun_bank) && $model['jenis_pembayaran'] != "Transfer") {
                $akunhutangupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                // $akunbankupdate['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
                $akunhutangupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunhutangupdate['dokumen'] = $kode_dp;
                $akunhutangupdate['subject_name'] = @$bank->kode_bank;
                $akunhutangupdate['id_subject'] = ForeignKeyFromDb(@$model['id_bank']);
                $akunhutangupdate['debet'] = 0;
                $akunhutangupdate['kredit'] = 0;
                $akunhutangupdate['updated_date'] = GetDateNow();
                $akunhutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunbankupdate, array("id_buku_besar" => $akun_hutang->id_buku_besar));
                $arrglakun[] = $akun_bank->id_buku_besar;
            }




            $this->load->model("gl_config/m_gl_config");
            $akungldp = $this->m_gl_config->GetIdGlConfig("dp", $model['id_cabang'], array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akungldp, "dokumen" => $kode_dp));
            $akundp = $this->db->get()->row();
            if (CheckEmpty($akundp)&& strpos($model['jenis_dp'], "DP") !== false) {
                $akundpinsert = array();
                $akundpinsert['id_gl_account'] = $akungldp;
                $akundpinsert['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akundpinsert['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akundpinsert['dokumen'] = $kode_dp;
                $akundpinsert['subject_name'] = @$customer->kode_customer;
                $akundpinsert['id_subject'] = $dp['id_customer'];
                $akundpinsert['debet'] = $num2;
                $akundpinsert['kredit'] = $num1;
                $akundpinsert['created_date'] = GetDateNow();
                $akundpinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akundpinsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akundp)&& strpos($model['jenis_dp'], "DP") !== false) {
                $akunglupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akunglupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunglupdate['dokumen'] = $kode_dp;
                $akunglupdate['subject_name'] = @$customer->kode_customer;
                $akunglupdate['id_subject'] = $dp['id_customer'];
                $akunglupdate['debet'] = $num2;
                $akunglupdate['kredit'] = $num1;
                $akunglupdate['updated_date'] = GetDateNow();
                $akunglupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunglupdate, array("id_buku_besar" => $akundp->id_buku_besar));
                $arrglakun[] = $akundp->id_buku_besar;
            }
            else if (!CheckEmpty($akundp)&& strpos($model['jenis_dp'], "DP") === false)
            {
                $akunglupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akunglupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunglupdate['dokumen'] = $kode_dp;
                $akunglupdate['subject_name'] = @$customer->kode_customer;
                $akunglupdate['id_subject'] = $dp['id_customer'];
                $akunglupdate['debet'] = 0;
                $akunglupdate['kredit'] = 0;
                $akunglupdate['updated_date'] = GetDateNow();
                $akunglupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunglupdate, array("id_buku_besar" => $akundp->id_buku_besar));
                $arrglakun[] = $akundp->id_buku_besar;
            }
            
            
            $akunglhutangcust = $this->m_gl_config->GetIdGlConfig("hutangcustomer", $model['id_cabang'], array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglhutangcust, "dokumen" => $kode_dp));
            $akunhutangcustomer = $this->db->get()->row();
            if (CheckEmpty($akunhutangcustomer)&& strpos($model['jenis_dp'], "Hutang") !== false) {
                $akunhutanginsert = array();
                $akunhutanginsert['id_gl_account'] = $akunglhutangcust;
                $akunhutanginsert['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akunhutanginsert['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunhutanginsert['dokumen'] = $kode_dp;
                $akunhutanginsert['subject_name'] = @$customer->kode_customer;
                $akunhutanginsert['id_subject'] = $dp['id_customer'];
                $akunhutanginsert['debet'] = $num2;
                $akunhutanginsert['kredit'] = $num1;
                $akunhutanginsert['created_date'] = GetDateNow();
                $akunhutanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunhutanginsert);
                $arrglakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akunhutangcustomer)&& strpos($model['jenis_dp'], "Hutang") !== false) {
                $akunhutangupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akunhutangupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunhutangupdate['dokumen'] = $kode_dp;
                $akunhutangupdate['subject_name'] = @$customer->kode_customer;
                $akunhutangupdate['id_subject'] = $dp['id_customer'];
                $akunhutangupdate['debet'] = $num2;
                $akunhutangupdate['kredit'] = $num1;
                $akunhutangupdate['updated_date'] = GetDateNow();
                $akunhutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunhutangupdate, array("id_buku_besar" => $akunhutangcustomer->id_buku_besar));
                $arrglakun[] = $akunhutangcustomer->id_buku_besar;
            }
            else if (!CheckEmpty($akunhutangcustomer)&& strpos($model['jenis_dp'], "Hutang") === false) {
                $akunhutangupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akunhutangupdate['keterangan'] = $dp['jenis_dp']." " . @$customer->nama_customer . ' untuk ' . $model['keterangan'];
                $akunhutangupdate['dokumen'] = $kode_dp;
                $akunhutangupdate['subject_name'] = @$customer->kode_customer;
                $akunhutangupdate['id_subject'] = $dp['id_customer'];
                $akunhutangupdate['debet'] = 0;
                $akunhutangupdate['kredit'] = 0;
                $akunhutangupdate['updated_date'] = GetDateNow();
                $akunhutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunhutangupdate, array("id_buku_besar" => $akunhutangcustomer->id_buku_besar));
                $arrglakun[] = $akunhutangcustomer->id_buku_besar;
            }


            $this->db->where(array("dokumen" => $kode_dp));
            if (count($arrglakun) > 0) {
                $this->db->where_not_in("id_buku_besar", $arrglakun);
            }
            $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)));



            $akunglpenyeimbang = $this->m_gl_config->GetIdGlConfig("penyeimbang", $model['id_cabang'], array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("dokumen" => $kode_dp, "id_gl_account !=" => $akunglpenyeimbang));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => "43", "dokumen" => $kode_dp));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = $akunglpenyeimbang;
                $akun_penyeimbanginsert['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk DP Ke " . @$customer->nama_customer;
                $akun_penyeimbanginsert['dokumen'] = $kode_dp;
                $akun_penyeimbanginsert['subject_name'] = @$customer->kode_customer;
                $akun_penyeimbanginsert['id_subject'] = ForeignKeyFromDb($model['id_customer']);
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akun_penyeimbanginsert);
            } else if (!CheckEmpty($akun_penyeimbang) && $row->selisih != 0) {
                $akun_penyeimbangupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk pembayaran Ke " . @$customer->nama_customer;
                $akun_penyeimbangupdate['dokumen'] = $kode_dp;
                $akun_penyeimbangupdate['subject_name'] = @$customer->kode_customer;
                $akun_penyeimbangupdate['id_subject'] = ForeignKeyFromDb($model['id_customer']);
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal']);
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk pembayaran Ke " . @$customer->nama_customer;
                $akun_penyeimbangupdate['dokumen'] = $kode_dp;
                $akun_penyeimbangupdate['subject_name'] = @$customer->kode_customer;
                $akun_penyeimbangupdate['id_subject'] = ForeignKeyFromDb($model['id_customer']);
                $akun_penyeimbangupdate['debet'] = 0;
                $akun_penyeimbangupdate['kredit'] = 0;
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
            }
            $dataret = [];
            $dataret['msg'] = "Data DP Berhasil dimasukkan ke dalam database";
            $dataret['st'] = true;
            $this->m_customer->KoreksiSaldoPiutang($model['id_customer'],null, true);
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured";
                return array("st" => false, "msg" => $message);
            } else {
                ClearCookiePrefix("_dp", $jenis_pencarian);
                $this->db->trans_commit();
                $arrayreturn["msg"] = "Piutang dan Saldo Awal Customer telah diupdate";
                $arrayreturn["st"] = true;
                return $arrayreturn;
            }
            SetMessageSession(1, $dataret['msg']);
            $this->db->trans_rollback();
            return $dataret;
        } catch (Exception $ex) {
             $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

}
