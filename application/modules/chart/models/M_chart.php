<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_chart extends CI_Model
{

    public $table = '#_pemesanan';
    public $id = 'id_pemesanan';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;
    function __construct()
    {
        parent::__construct();
    }

    function getStatistik($id_pangkalan, $from, $to, $col=array()){
        $where = " WHERE t.id_pangkalan_beli = ".$id_pangkalan;
        if($from == $to){
            $where .= " AND t.tanggal_penjualan = '".$from."'";
        }else{
            $where .= " AND t.tanggal_penjualan BETWEEN '".$from."' AND '".$to."'";
        }

        if(count($col)){
            if(array_key_exists('id_kelas', $col) && !CheckEmpty($col['id_kelas'])){
                $where .= " AND tb.id_kelas = ".$col['id_kelas'];
            }
            if(array_key_exists('id_pangkalan', $col) && !CheckEmpty($col['id_pangkalan'])){
                $where .= " AND t.id_pangkalan_beli = ".$col['id_pangkalan'];
            }
        }

        $sql = "SELECT COUNT(t.id_pemesanan) as jml FROM ".$this->table." t";
        $sql .= " JOIN #_berangkat tb ON (tb.id_berangkat = t.id_berangkat)";
        $sql .= $where;

//        return $sql;

        $row = $this->db->query($sql)->row();

        return $row->jml;
    }

    function getLowestYear(){
            $year = date('Y');
            $sql = "SELECT MIN(YEAR(created_date)) AS lowest FROM ".$this->table;
            $query = $this->db->query($sql)->row_array();

            if($query){
                $year = $query['lowest'];
            }

            return $year;
    }

    function getStatPeriode($tahun, $bulan, $tanggal=null, $filterPT=false){
       
        $jml = 0;
        $this->db->select("COUNT(*) AS jml ");
        $this->db->from($this->table);
        if($tanggal){
            $this->db->like($this->table.".created_date", $tahun."-".$bulan."-".$tanggal, "after", false);
        }else{
            $this->db->where('YEAR('.$this->table.'.created_date)', $tahun);
            $this->db->where('MONTH('.$this->table.'.created_date)', $bulan);
//	        $this->db->like($this->table.".created_date", $tahun."-".$bulan."-", "after", false);
        }
	    if($filterPT){
		    $this->db->join("#_user", "#_user.id_user = ".$this->table.".created_by", "left");
		 }

        $data = $this->db->get()->row_array();
        if($data){
            $jml = $data['jml'];
        }
        return $jml;
    }

}