<section class="content-header">
    <h1>
        Statistik Per Agen
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Statistik</li>
        <li class="active">Per Agen</li>
    </ol>


</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search" class="form-horizontal">
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label class="control-label">Tanggal</label>
                        <?= form_input(array("selected" => "", 'name' => 'tanggal_from_1', 'class' => 'form-control datepicker', 'id' => 'tanggal_from_1', 'required'=>'required'), DefaultTanggal(date('Y-m-d'))); ?>
                    </div>

                    <div class="col-sm-3">
                        <label class="control-label">Sampai Tanggal</label>
                        <?= form_input(array("selected" => "", 'name' => 'tanggal_to_1', 'class' => 'form-control datepicker', 'id' => 'tanggal_to_1', 'required'=>'required'), DefaultTanggal(date('Y-m-d'))); ?>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Kelas</label>
                        <?= form_dropdown(array('name' => 'id_kelas_1', 'class' => 'form-control', 'id' => 'id_kelas_1', 'placeholder' => 'Kelas'), array_merge(array("" => "Pilih Kelas"), $list_kelas)); ?>
                    </div>
                    <!--<div class="col-sm-1">
                        <label class="control-label">&nbsp;</label>
                        <a id="btn_search_1" class="btn btn-primary" onclick="getStatistik(1)">Tampilkan</a>
                    </div>-->
                </div>
                <hr style="margin: 3px 0px;"/>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label class="control-label">Tanggal</label>
                        <?= form_input(array("selected" => "", 'name' => 'tanggal_from_2', 'class' => 'form-control datepicker', 'id' => 'tanggal_from_2')); ?>
                    </div>

                    <div class="col-sm-3">
                        <label class="control-label">Sampai Tanggal</label>
                        <?= form_input(array("selected" => "", 'name' => 'tanggal_to_2', 'class' => 'form-control datepicker', 'id' => 'tanggal_to_2')); ?>
                    </div>
                    <div class="col-sm-3">
                        <label class="control-label">Kelas</label>
                        <?= form_dropdown(array('name' => 'id_kelas_2', 'class' => 'form-control', 'id' => 'id_kelas_2', 'placeholder' => 'Kelas'), array_merge(array("" => "Pilih Kelas"), $list_kelas)); ?>
                    </div>
                    <!--<div class="col-sm-1">
                        <label class="control-label">&nbsp;</label>
                        <a id="btn_search_2" class="btn btn-primary" onclick="getStatistik(2)">Tampilkan</a>
                    </div>-->
                </div>
                <div class="row form-group">
                    <div class="col-sm-1">
                        <a id="btn-stat" class="btn btn-primary">Tampilkan</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="box-footer box-comments">
            <div class="box-comment">
                    <div id="container_1" style="min-width: 310px; width: 85%; height:100%; margin: 0 auto"></div>
            </div>
            <br/>
            <div class="box-comment">
                    <div id="container_2" style="min-width: 310px; width: 85%; height:100%; margin: 0 auto"></div>
            </div>
        </div>
    </div>

</section>