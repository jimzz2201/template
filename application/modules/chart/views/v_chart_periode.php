<section class="content-header">
    <h1>
        Statistik Per Periode
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Statistik</li>
        <li class="active">Per Periode</li>
    </ol>


</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search" class="form-horizontal">
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label class="control-label">Bulan</label>
                        <?= form_dropdown(array("selected" => "", 'name' => 'bulan_1', 'class' => 'form-control', 'id' => 'bulan_1', 'required'=>'required'), array(""=>"--") + GetMonth()); ?>
                    </div>

                    <div class="col-sm-3">
                        <label class="control-label">Tahun</label>
                        <?= form_dropdown(array("selected" => "", 'name' => 'tahun_1', 'class' => 'form-control', 'id' => 'tahun_1', 'required'=>'required'), $list_tahun, date('Y')); ?>
                    </div>

                    <!--<div class="col-sm-3">
                        <label class="control-label">Kelas</label>
                        <?/*= form_dropdown(array('name' => 'id_kelas_1', 'class' => 'form-control', 'id' => 'id_kelas_1', 'placeholder' => 'Kelas'), array_merge(array("" => "Pilih Kelas"), $list_kelas)); */?>
                    </div>-->
                </div>
                <hr style="margin: 3px 0px;"/>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label class="control-label">Bulan</label>
                        <?= form_dropdown(array("selected" => "", 'name' => 'bulan_2', 'class' => 'form-control', 'id' => 'bulan_2'), array(""=>"--") + GetMonth()); ?>
                    </div>

                    <div class="col-sm-3">
                        <label class="control-label">Tahun</label>
                        <?= form_dropdown(array("selected" => "", 'name' => 'tahun_2', 'class' => 'form-control', 'id' => 'tahun_2'), array(""=>"--") + $list_tahun); ?>
                    </div>

                    <!--<div class="col-sm-3">
                        <label class="control-label">Kelas</label>
                        <?/*= form_dropdown(array('name' => 'id_kelas_2', 'class' => 'form-control', 'id' => 'id_kelas_2', 'placeholder' => 'Kelas'), array_merge(array("" => "Pilih Kelas"), $list_kelas)); */?>
                    </div>-->
                </div>
                <div class="row form-group">
                    <div class="col-sm-1">
                        <a id="btn-stat" class="btn btn-primary">Tampilkan</a>
                    </div>
                </div>
            </form>
        </div>
        <div class="box-footer box-comments">
            <div class="box-comment">
                <div id="container_1" style="min-width: 310px; width: 85%; margin: 0 auto"></div>
            </div>
            <br/>
            <div class="box-comment">
                <div id="container_2" style="min-width: 310px; width: 85%; margin: 0 auto"></div>
            </div>
        </div>
    </div>

</section>