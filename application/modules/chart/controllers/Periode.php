<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Periode extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_chart');
    }

    public function index() {
        $data = $javascript = $css = array();
        $javascript[] = "assets/plugins/datepicker/bootstrap-datepicker.js";
        $javascript[] = "assets/js/highcharts/highcharts.js";
        $javascript[] = "assets/js/highcharts/highcharts-3d.js";
        $javascript[] = "assets/js/highcharts/modules/data.js";
        $javascript[] = "assets/js/highcharts/modules/drilldown.js";
        $javascript[] = "assets/js/highcharts/modules/exporting.js";
        $javascript[] = "assets/js/highcharts/themes/grid-light.js";
        $javascript[] = "assets/js/modules/chart/periode.js";

        $data['openmenu'] = "menuchart";

        $data['list_kelas'] = GetTableData('#_kelas_body', 'id_kelas', array('kode_kelas','nama_kelas'), array(), 'json');

        $data['list_tahun'] = array();
        $lowest = $this->m_chart->getLowestYear();
        for($x=$lowest ; $x<=date('Y'); $x++){
            $data['list_tahun'][$x] = $x;
        }
//        var_dump('<pre>', $data['list_tahun']); die();

        LoadTemplate($data, 'v_chart_periode', $javascript, $css);
    }

    public function get_data()
    {
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');

        echo json_encode($this->getData($tahun, $bulan, false));
    }

    public function get_data_detail()
    {
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');

        echo json_encode($this->getData($tahun, $bulan, true));
    }

    function getData($tahun, $bulan=null, $drilldown=false){
        $list_waktu = array();
        $result = array();
        if($bulan){
            $y = date('t', strtotime($tahun.'-'.$bulan.'-01'));
            for($x=1;$x<=$y;$x++){
                $list_waktu[$x] = $x;
            }

            if($drilldown){
                $result['name'] = "Penjualan ". GetMonth($bulan)." - ".$tahun;
                $result['data'] = array();
            }
            foreach($list_waktu as $tanggal => $val){
                $tanggal = str_pad($tanggal, 2, '0', STR_PAD_LEFT);
                $bulan = str_pad($bulan, 2, '0', STR_PAD_LEFT);
                $data = $this->m_chart->getStatPeriode($tahun, $bulan, $tanggal);
                if($drilldown){
                    $result['data'][] = array( $tanggal, ($data > 0) ? (int)$data : null);
                }else{
                    $result[] = array(
                        'name' => $tanggal,
                        'y' => ($data > 0) ? (int)$data : null,
                        'drilldown' => true,
                    );
                }
            }
        }else{
            $list_waktu = GetMonth();

            foreach($list_waktu as $bulan => $val){
                $data = $this->m_chart->getStatPeriode($tahun, $bulan, null);
                $result[] = array(
                    'name' => GetMonth($bulan),
                    'y' => ($data > 0) ? (int)$data : null,
                    'tahun' => $tahun,
                    'bulan' => $bulan,
                    'drilldown' => true,
                );
            }
        }
        return $result;
    }

}

/* End of file Periode.php */