<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agen extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_chart');
    }

    public function index() {
        $data = $javascript = $css = array();
        $javascript[] = "assets/plugins/datepicker/bootstrap-datepicker.js";
        $javascript[] = "assets/js/highcharts/highcharts.js";
        $javascript[] = "assets/js/highcharts/modules/data.js";
        $javascript[] = "assets/js/highcharts/modules/drilldown.js";
        $javascript[] = "assets/js/highcharts/modules/exporting.js";
        $javascript[] = "assets/js/highcharts/themes/sand-signika.js";
        $javascript[] = "assets/js/modules/chart/agen.js";

        $data['openmenu'] = "menuchart";

        $data['list_kelas'] = GetTableData('#_kelas_body', 'id_kelas', array('kode_kelas','nama_kelas'), array(), 'json');

        LoadTemplate($data, 'v_chart_agen', $javascript, $css);
    }

    public function get_data()
    {
        $tanggal_from = $this->input->get('tanggal_from');
        $tanggal_to = $this->input->get('tanggal_to');
        $id_kelas = $this->input->get('id_kelas');

        echo json_encode($this->getData($tanggal_from, $tanggal_to, $id_kelas));

    }

    public function get_data_detail()
    {
        $tanggal_from = $this->input->get('tanggal_from');
        $tanggal_to = $this->input->get('tanggal_to');
        $id_pangkalan = $this->input->get('id_pangkalan');
        $nama_pangkalan = $this->input->get('nama_pangkalan');

        echo json_encode($this->getDataDetail($tanggal_from, $tanggal_to, $id_pangkalan, $nama_pangkalan));

    }

    function getData($from, $to, $id_kelas){
        $list_pangkalan = GetTableData('#_pangkalan', 'id_pangkalan', array('kode','nama_pangkalan'), array('status'=>1), 'obj', array('kode', 'asc'));
        $result = array();
        if(count($list_pangkalan)){
            foreach($list_pangkalan as $id_pangkalan => $nama_pangkalan){
                $data = $this->m_chart->getStatistik($id_pangkalan, DefaultTanggalDatabase($from), DefaultTanggalDatabase($to), array('id_kelas' => $id_kelas));
                if($data){
                    $result[] = array(
                        'name' => $nama_pangkalan,
                        'y' => (int)$data,
                        'drilldown' => true,
                        'id_pangkalan' => $id_pangkalan
                    );
                }
            }
        }

        return $result;
    }

    function getDataDetail($from, $to, $id_pangkalan, $nama_pangkalan){
        $list_kelas = GetTableData('#_kelas_body', 'id_kelas', array('nama_kelas'), array(), 'obj');
        $result = array(
            'name' => $nama_pangkalan,
            'data' => array()
        );
        if(count($list_kelas)){
            foreach($list_kelas as $id_kelas => $nama_kelas){
                $data = $this->m_chart->getStatistik($id_pangkalan, DefaultTanggalDatabase($from), DefaultTanggalDatabase($to), array('id_kelas' => $id_kelas, 'id_pangkalan'=>$id_pangkalan));
                $result['data'][] = array( $nama_kelas, (int)$data);
            }
        }

        return $result;
    }

}

/* End of file Agen.php */