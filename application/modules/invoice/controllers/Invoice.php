<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice extends CI_Controller {

    private $_sufix = "_pemb";

    function __construct() {
        parent::__construct();
        $this->load->model('m_invoice');
        $this->load->model("warna/m_warna");
        $this->load->model("vrf/m_vrf");
        $this->load->model("model/m_model");
    }
    public function generatejurnalinvoice()
    {
        $this->db->from("#_pembelian");
        $this->db->where(array("#_pembelian.status !="=>2,"#_pembelian.deleted_date"=>null));
        $listpembelian=$this->db->get()->result();
        foreach($listpembelian as $pem)
        {
           $this->m_invoice->GenerateJurnalInvoice($pem->id_pembelian ,"#_pembelian.id_pembelian",$pem->nnomor_invoice);
        }
    }
    
     public function create_invoice() {
        $row = ['button' => 'Add'];
        $jenis_pembayaran = array();
        $javascript = array();
        $this->load->model("pool/m_pool");
        $this->load->model("kategori/m_kategori");
        $module = "K122";
        $header = "K059";
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("ekspedisi/m_ekspedisi");
        $this->load->model("supplier/m_supplier");
        $this->load->model("do_kpu/m_do_kpu");
        $this->load->model("kpu/m_kpu");

        $row['title'] = "Add Pembelian";
        $row['list_top'] = array(array("id" => 0, "text" => "0"), array("id" => 30, "text" => "30"), array("id" => 45, "text" => "45"), array("id" => 60, "text" => "60"), array("id" => 90, "text" => "90"));
        $row['jth_tempo'] = AddDays(GetDateNow(), "30 days");
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $row['tanggal_do'] = GetDateNow();
        $row['type_pembayaran'] = 2;
        $row['top'] = 30;
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['listdetail'] = array();
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_do_kpu'] = $this->m_invoice->GetDropDownDoKpuFromInvoice();
        $row['list_kpu'] = $this->m_invoice->GetDropDownKPUFromInvoice();
        $row['list_unit_serial']=$this->m_invoice->GetDropDownUnitSerialForInvoice([]);
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css[] = 'assets/plugins/iCheck/all.css';
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        LoadTemplate($row, 'invoice/v_invoice_manipulate', $javascript, $css);
    }
    public function get_prospek_invoice_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();
        $list_prospek = $this->m_invoice->GetDropDownProspekForInvoice($model);
        echo json_encode(array("list_prospek" => DefaultEmptyDropdown($list_prospek, "json", "Prospek"), "st" => true));
    }
    public function get_kpu_invoice_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();
        $list_kpu = $this->m_invoice->GetDropDownKPUFromInvoice($model);
        echo json_encode(array("list_kpu" => DefaultEmptyDropdown($list_kpu, "json", "KPU"), "st" => true));
    }
    public function get_dokpu_invoice_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();
        $list_do_kpu = $this->m_invoice->GetDropDownDoKpuFromInvoice($model);
        echo json_encode(array("list_do_kpu" => DefaultEmptyDropdown($list_do_kpu, "json", "DO KPU"), "st" => true));
    }
    public function get_unitserial_invoice_list() {
        
        $model=$this->input->post();
        $data = $this->m_invoice->GetDropDownUnitSerialForInvoice($model);
        echo json_encode(array("st"=>true,"list_unit_serial"=> DefaultEmptyDropdown($data,"json","Unit")));
    }
     public function get_bukajual_invoice_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();
        $list_buka_jual = $this->m_invoice->GetDropDownBukaJualForInvoice($model);
        echo json_encode(array("list_buka_jual" => DefaultEmptyDropdown($list_buka_jual, "json", "Buka Jual"), "st" => true));
    }
    public function getdatapembelian() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_prefix, 1);
            }
            SetCookieSetting($key . $this->_prefix, $value);
        }
        echo $this->m_invoice->GetDataInvoice($params);
        //dumperror(GetLastQueryDataTable());
    }
    public function invoice_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->load->model("unit/m_unit");
      
        // $this->form_validation->set_rules('id_prospek', 'KPU', 'trim');
        $this->form_validation->set_rules('tanggal_invoice', 'Tanggal', 'trim|required');
        
        if(CheckEmpty(@$model['nomor_invoice']))
        {
            $this->form_validation->set_rules('nomor_invoice', 'Nomor Invoice', 'trim|required|is_unique[#_pembelian.nomor_invoice]');
        }
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'trim|required');
        $this->load->model("prospek/m_prospek");
        $sisa = 0;
        $dataset = CheckArray($model, "dataset");
        $qtydo = 0;
        $islanjut = false;
        $total=0;
     
        foreach ($dataset as $key => $detail) {
            if ($detail != "") {
                $total+=$detail['hpp_unit'];
                $islanjut = true;
                $serialunit = $this->m_unit->GetOneUnitSerial($detail['id_unit_serial']);
                if ($serialunit) {
                    if(CheckEmpty(@$model['id_pembelian']))
                    {
                        if (!CheckEmpty($serialunit->id_pembelian)) {
                            $message .= "Unit sudah ada dalam pembelian lain<br/>";
                        }
                    }
                    else
                    {
                        if (!CheckEmpty($serialunit->id_pembelian)&&$model['id_pembelian']!=$serialunit->id_pembelian) {
                            $message .= "Unit sudah ada dalam pembelian lain<br/>";
                        }
                    }
                   
                }
            }
        }
        $model['total']=$total;
      
        if (!$islanjut) {
            $message .= "Tidak ada unit yang diinput<br/>";
        }
        $model['dataset'] = $dataset;

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_invoice->InvoiceManipulate($model);
            echo json_encode($status);
        }
    } 
    public function edit_invoice($id = 0) {
        $row = $this->m_invoice->GetOnePembelian($id);
       
        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K122";
            $header = "K059";
            $row['title'] = "Edit Pembelian";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $this->load->model("supplier/m_supplier");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $this->load->model("customer/m_customer");
            $this->load->model("kategori/m_kategori");
            $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
            $row['form'] = $header;
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $row['list_type_ppn'] = $list_type_ppn;
            $row['formsubmenu'] = $module;
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_do_kpu'] = $this->m_invoice->GetDropDownDoKpuFromInvoice();
            $row['list_kpu'] = $this->m_invoice->GetDropDownKPUFromInvoice();
            $row['list_top'] = array(array("id" => 0, "text" => "0"), array("id" => 30, "text" => "30"), array("id" => 45, "text" => "45"), array("id" => 60, "text" => "60"), array("id" => 90, "text" => "90"));
            $row['list_unit_serial'] = $this->m_invoice->GetDropDownUnitSerialForInvoice([]);
            $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
            $css[] = 'assets/plugins/iCheck/all.css';
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
            $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
            LoadTemplate($row, 'invoice/v_invoice_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Kpu cannot be found in database");
            redirect(site_url('kpu'));
        }
    }
    
    public function view_invoice($id = 0) {
        $row = $this->m_invoice->GetOnePembelian($id);
       
        if ($row) {
            $row->button = 'Update';
            $row->is_view=1;
             $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K122";
            $header = "K059";
            $row['title'] = "Edit Pembelian";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $this->load->model("supplier/m_supplier");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $this->load->model("customer/m_customer");
            $this->load->model("kategori/m_kategori");
            $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
            $row['form'] = $header;
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $row['list_type_ppn'] = $list_type_ppn;
            $row['formsubmenu'] = $module;
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_do_kpu'] = $this->m_invoice->GetDropDownDoKpuFromInvoice();
            $row['list_kpu'] = $this->m_invoice->GetDropDownKPUFromInvoice();
            $row['list_top'] = array(array("id" => 0, "text" => "0"), array("id" => 30, "text" => "30"), array("id" => 45, "text" => "45"), array("id" => 60, "text" => "60"), array("id" => 90, "text" => "90"));
            $row['list_unit_serial'] = $this->m_invoice->GetDropDownUnitSerialForInvoice([]);
            $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
            $css[] = 'assets/plugins/iCheck/all.css';
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
            $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
            LoadTemplate($row, 'invoice/v_invoice_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Kpu cannot be found in database");
            redirect(site_url('kpu'));
        }
    }
    
    
    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K122";
        $header = "K059";
        $title = "Pembelian";
        $this->load->model("supplier/m_supplier");
        $this->load->model("kategori/m_kategori");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $jenis_pencarian = [];
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_vrf", "text" => "NO VRF");
        $jenis_keyword[] = array("id" => "nomor_master", "text" => "NO KPU");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PPROSPEK");
        $jenis_keyword[] = array("id" => "nomor_do_kpu", "text" => "NO DO KPU");
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal", "text" => "Tanggal KPU");
        $model['start_date'] = GetCookieSetting("start_date" . $this->_sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $this->_sufix, GetDateNow());
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['status'] = GetCookieSetting("status" . $this->_sufix, 0);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_sufix, "nomor_master");
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $this->_sufix, "created_date");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_keyword'] = $jenis_keyword;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "invoice/v_invoice_index", $javascript, $css);
    }
    
    public function getlistinvoicepembayaran()
    {
        $model=$this->input->post();
        $listinvoice=$this->m_inovice->GetListInvoicePembayaran($model);
        //$listunit=$this->m_supplier->GetListUnitSupplier($model);
        echo json_encode(array("st"=>true,"listinvoice"=>$listinvoice));
        
    }
    public function get_one_invoice(){
        
        $model = $this->input->post();
       
        $this->form_validation->set_rules('id_pembelian', 'Pembelian', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_invoice->GetOnePembelian($model["id_pembelian"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Pembelian tidak ditemukan dalam database" : "";
            $data['html'] = $this->load->view("pembayaran/v_pembayaran_hutang_content", array("detail"=>[$data['obj']]), true);
            echo json_encode($data);
        }
    }

}

/* End of file Kpu.php */