<section class="content-header">
    <h1>
        Pembelian <?= @$button ?>
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">Pembelian</li>
    </ol>

</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_mutasi_unit" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_pembelian" value="<?php echo @$id_pembelian; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal_invoice', 'value' => DefaultDatePicker(@$tanggal_invoice), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "top"), @$list_top, @$top, array('class' => 'form-control select2', 'id' => 'txt_top')); ?>
                                    </div> 
                                    <?= form_label('Jth Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text','readonly'=>'readonly', 'autocomplete' => 'off', 'name' => 'jth_tempo', 'value' => DefaultDatePicker(@$jth_tempo), 'class' => 'form-control', 'id' => 'txt_jth_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                    </div>
                                </div>
                               <div class="form-group">
                                   <?= form_label('Supplier', "dd_id_customer_search", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', "name" => "id_supplier", 'class' => 'form-control select2', 'id' => 'dd_id_supplier_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier); ?>
                                    </div>
                                    <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "json", "PPN"), @$type_ppn, array('class' => 'form-control select2dropdown select2', 'id' => 'dd_type_ppn')); ?>
                                    </div>
                                    <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$ppn), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                   
                                    
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nomor Invoice', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'nomor_invoice',  'value' => @$nomor_invoice, 'class' => 'form-control', 'id' => 'txt_nomor_invoice', 'placeholder' => 'No Invoice')); ?>
                                    </div>
                                     <?= form_label('Faktur Pajak', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'faktur_pajak',  'value' => @$faktur_pajak, 'class' => 'form-control', 'id' => 'txt_faktur_pajak', 'placeholder' => 'Faktur Pajak')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?php 
                                         echo form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'));
                                        ?>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Customer', "dd_id_customer_search", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', "name" => "id_customer", 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer); ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <?= form_label('Kategori', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori', 'class' => 'form-control select2', 'id' => 'dd_id_kategori_search', 'placeholder' => 'Kategori'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$id_kategori); ?>
                                    </div>
                                    <?= form_label('Unit', "dd_id_unit_search", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit', 'class' => 'form-control select2', 'id' => 'dd_id_unit_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                                    </div>

                                </div>


                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Prospek', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        // if (!CheckEmpty(@$id_prospek)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_prospek, 'name' => 'id_prospek'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_prospek), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Prospek'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_prospek', 'class' => 'form-control select2', 'id' => 'dd_id_prospek', 'placeholder' => 'Prospek'), DefaultEmptyDropdown(@$list_prospek, "json", "Prospek"), @$id_prospek);
                                        // }
                                        ?>
                                    </div>
                                    <?= form_label('Buka Jual', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?php
                                        // if (!CheckEmpty(@$id_buka_jual)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_buka_jual, 'name' => 'id_buka_jual'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_buka_jual), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Buka Jual'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_buka_jual', 'class' => 'form-control select2', 'id' => 'dd_id_buka_jual', 'placeholder' => 'Buka Jual'), DefaultEmptyDropdown(@$list_buka_jual, "json", "Buka Jual"), @$id_buka_jual);
                                        //echo form_input(array('type' => 'hidden', 'name' => 'id_customer', 'value' => @$id_customer, 'class' => 'form-control', 'id' => 'id_cust'));
                                        // }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('KPU', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        // if (!CheckEmpty(@$id_prospek)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_prospek, 'name' => 'id_prospek'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_prospek), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Prospek'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_kpu', 'class' => 'form-control select2', 'id' => 'dd_id_kpu', 'placeholder' => 'KPU'), DefaultEmptyDropdown(@$list_kpu, "json", "KPU"), @$id_kpu);
                                        // }
                                        ?>
                                    </div>
                                    <?= form_label('DO KPU', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?php
                                        // if (!CheckEmpty(@$id_buka_jual)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_buka_jual, 'name' => 'id_buka_jual'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_buka_jual), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Buka Jual'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_do_kpu', 'class' => 'form-control select2', 'id' => 'dd_id_do_kpu', 'placeholder' => 'DO KPU'), DefaultEmptyDropdown(@$list_do_kpu, "json", "DO KPU"), @$id_do_kpu);
                                        // }
                                        ?>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        // if (!CheckEmpty(@$id_buka_jual)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_buka_jual, 'name' => 'id_buka_jual'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_buka_jual), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Buka Jual'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_unit_serial', 'class' => 'form-control select2', 'id' => 'dd_id_unit_serial', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit_serial, "json", "Unit"), @$id_unit_serial);
                                        // }
                                        ?>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>

                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Warna')); ?>


                                    </div>
                                    <?= form_label('KPU', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master, 'class' => 'form-control', 'id' => 'txt_nomor_master', 'placeholder' => 'Nomor KPU')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('No Prospek', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$no_prospek, 'class' => 'form-control', 'id' => 'txt_no_prospek', 'placeholder' => 'No Prospek')); ?>
                                    </div>

                                    <?= form_label('Nomor Mesin', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_buka_jual, 'class' => 'form-control', 'id' => 'txt_nomor_buka_jual', 'placeholder' => 'Nomor Buka Jual')); ?>


                                    </div>
                                    <?= form_label('Nama  Customer', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Nama Customer')); ?>


                                    </div>
                                </div>


                                <div class="form-group">
                                    <?= form_label('Nomor Kendaraan', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$vin_number, 'class' => 'form-control', 'id' => 'txt_vin_number', 'placeholder' => 'Nomor Kendaraan')); ?>
                                    </div>

                                    <?= form_label('Nomor Mesin', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$engine_no, 'class' => 'form-control', 'id' => 'txt_engine_no', 'placeholder' => 'Nomor Mesin')); ?>

                                    </div>
                                    <?= form_label('Nomor DO', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_do, 'class' => 'form-control', 'id' => 'txt_nomor_do_supplier', 'placeholder' => 'Nomor DO')); ?>

                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <?= form_label('Price', "txt_id_colour", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$pricef, 'class' => 'form-control', 'id' => 'txt_price', 'placeholder' => 'Price')); ?>

                                    </div>
                                <div class="col-sm-1">
                                        <button type="button" class="btn btn-block btn bg-navy" id="btt_tambah_unit" >Tambah Unit</button>
                                    </div>
                                </div>  

                                <div>
                                    <table  id="mytable" class="td30 tr30 table table-striped table-bordered table-hover dataTable no-footer">

                                    </table>
                                </div>
                                <div id="areaUnit">
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total', "readonly" => "readonly")); ?>
                                    </div>
                                    <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'nominal_ppn', 'value' => DefaultCurrency(@$nominal_ppn), 'class' => 'form-control', 'id' => 'txt_nominal_ppn', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
                                    </div>


                                </div>
                              
                                <div class="form-group">
                                    <?= form_label('Grand Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$grandtotal), 'class' => 'form-control', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
                                    </div>
                                    <!--
                                    <?= form_label('Jumlah&nbsp;Bayar', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?php
                                        $mergepembayaran = [];
                                        if (@$type_pembayaran !== "2") {
                                            $mergepembayaran['readonly'] = "readonly";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergepembayaran, array('type' => 'text', 'name' => 'jumlah_bayar', 'value' => DefaultCurrency(@$jumlah_bayar), 'class' => 'form-control', 'id' => 'txt_jumlah_bayar', 'placeholder' => 'Jumlah Bayar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);'))); ?>
                                    </div>
                                    -->
                                </div>
                                <hr/>
                                <?php if (CheckEmpty(@$is_view)) { ?>
                                    <div class="form-group" style="margin-top:50px">
                                        <a style="float:right;" href="<?php echo base_url() . 'index.php/mutasi_unit' ?>" class="btn btn-default"  >Cancel</a>
                                        <div class="col-sm-2">
                                            <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                        </div>
                                    </div>
                                <?php } ?>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    var table;
    var listdetail = <?php echo json_encode(@$listdetail) ?>;
    
    function RefreshUnit()
    {
        var id_kategori = $("#dd_id_kategori_search").val();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_dropdown_unit',
            dataType: 'json',
            data: {
                id_kategori: id_kategori
            },
            success: function (data) {
                $("#dd_id_unit_search").empty();
                if (data.st)
                {
                    $("#dd_id_unit_search").select2({data: data.list_unit})
                }
                ClearFormDetail();
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }

    function RefreshProspek() {

        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/invoice/get_prospek_invoice_list',
            dataType: 'json',
            data: {
                id_kategori: $("#dd_id_kategori_search").val(),
                id_unit: $("#dd_id_unit_search").val(),
                id_customer: $("#dd_id_customer_search").val()
            },
            success: function (data) {
                $("#dd_id_prospek").empty();
                if (data.st)
                {
                    $("#dd_id_prospek").select2({data: data.list_prospek});
                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }
    function RefreshKPU() {

        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/invoice/get_kpu_invoice_list',
            dataType: 'json',
            data: {
                id_kategori: $("#dd_id_kategori_search").val(),
                id_unit: $("#dd_id_unit_search").val(),
                id_customer: $("#dd_id_customer_search").val()
            },
            success: function (data) {
                $("#dd_id_kpu").empty();
                if (data.st)
                {
                    $("#dd_id_kpu").select2({data: data.list_kpu});
                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }
    function RefreshDoKPU() {

        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/invoice/get_dokpu_invoice_list',
            dataType: 'json',
            data: {
                id_kategori: $("#dd_id_kategori_search").val(),
                id_unit: $("#dd_id_unit_search").val(),
                id_customer: $("#dd_id_customer_search").val()
            },
            success: function (data) {
                $("#dd_id_do_kpu").empty();
                if (data.st)
                {
                    $("#dd_id_do_kpu").select2({data: data.list_do_kpu});
                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }
    
    

    function RefreshSerialUnit() {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/invoice/get_unitserial_invoice_list',
            data: $("#frm_mutasi_unit").serialize(),
            dataType: "json",
            success: function (data) {
                if (data.st)
                {
                    $("#dd_id_unit_serial").empty();
                    $("#dd_id_unit_serial").select2({data: data.list_unit_serial});

                }

            }
        })
    }

    function RefreshBukaJual() {

        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/invoice/get_bukajual_invoice_list',
            dataType: 'json',
            data: $("#frm_mutasi_unit").serialize(),
            success: function (data) {
                $("#dd_id_buka_jual").empty();
                if (data.st)
                {
                    $("#dd_id_buka_jual").select2({data: data.list_buka_jual});

                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }



    function GetOneSerialUnit() {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_one_serial',
            dataType: 'json',
            data: {
                id_unit_serial: $("#dd_id_unit_serial").val()
            },
            success: function (data) {
                if (data.st)
                {
                    $("#txt_nama_unit").val(data.obj.nama_unit);
                    $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                    $("#txt_nama_warna").val(data.obj.nama_warna);
                    $("#txt_nama_kategori").val(data.obj.nama_kategori);
                    $("#txt_vin").val(data.obj.vin);
                    $("#txt_nomor_buka_jual").val(data.obj.nomor_buka_jual);
                    $("#txt_nomor_do_supplier").val(data.obj.nomor_do_supplier);
                    $("#txt_price").val(Comma(data.obj.hpp_unit));
                    $("#txt_no_prospek").val(data.obj.no_prospek);
                    $("#txt_nama_customer").val(data.obj.nama_customer);
                    $("#txt_vin_number").val(data.obj.vin_number);
                    $("#txt_engine_no").val(data.obj.engine_no);
                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }

    function ClearFormDetail()
    {
        $("#txt_nama_unit").val("");
        $("#txt_nama_type_unit").val("");
        $("#txt_nama_kategori").val("");
        $("#txt_vin").val("");
        $("#txt_nama_warna").val("");
        $("#txt_price").val("");
        $("#txt_nomor_do_supplier").val("");
        $("#txt_no_prospek").val("");
        $("#txt_nomor_buka_jual").val("");
        $("#txt_nama_customer").val("");
        $("#txt_vin_number").val("");
        $("#txt_engine_no").val("");
        jumlahgenerate = 0;
        $("#areado").html("");

    }

    function RefreshGrid()
    {   
         table.fnClearTable();
        if (listdetail.length > 0)
        {
            table.fnDraw(true);
            table.fnAddData(listdetail);
        }
        HitungSemua();

    }

    function deleterow(indrow) {
        RefreshGrid();
    }
    function RefreshTop(){
         $("#txt_jth_tempo").val(AddDays($("#txt_tanggal").val(),$("#txt_top").val()));
    }
    $(document).ready(function () {
        $(".datepicker").datepicker();
         $("#txt_tanggal , #txt_top").on('change', function (e) {
             RefreshTop();
        });
        $(".icheck").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
        $(".select2").select2();
        RefreshPPN();
        
            
    <?php if (!CheckEmpty(@$is_view)) { ?>
                $("input").attr("readonly", true);
                $("textarea").attr("readonly", true);
                $("select").attr("disabled", true);
                
    <?php } ?>

        $('#dd_id_customer_search').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            data: listdetail,
            columns: [
                {
                    data: "id_unit_serial",
                    title: "No",
                    orderable: false,

                    width: "10px",
                    className: "text-right"
                },
                {data: "nama_unit", orderable: false, title: "Nama Unit"},
                {data: "vin_number", orderable: true, title: "No Rangka"},
                {data: "engine_no", orderable: true, title: "No Mesin"},
                {data: "nomor_buka_jual", orderable: true, title: "Buka Jual"},
                {data: "nama_customer", orderable: false, title: "Nama Customer"},
                {data: "hpp_unit", orderable: false, title: "HPP" ,mRender: function (data, type, row) {
                        return Comma(data)

                    }},

                {data: "id_unit_serial",
                    mRender: function (data, type, row) {
                        return "<a onclick='hapusrow(\"" + data + "\")' style='float:right;' href='javascript:;' class='btn-xs btn-danger'  >Hapus</a>"

                    }},
            ],

            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
       

        $("#btt_tambah_unit").click(function () {

            clearmessage();
            if (!CheckEmpty($("#dd_id_unit_serial").val()))
            {

                var res = alasql('SELECT * FROM ? where id_unit_serial=\'' + $("#dd_id_unit_serial").val() + '\'', [listdetail]);
                if (res.length > 0)
                {
                    messageerror("Data Unit Sudah ada dalam list do");
                } else
                {

                    $.ajax({
                        type: 'POST',
                        url: baseurl + 'index.php/unit/get_one_serial',
                        dataType: "json",
                        data: {
                            id_unit_serial: $("#dd_id_unit_serial").val()
                        },
                        success: function (data) {
                            if (data.st)
                            {
                                listdetail.push(data.obj);
                                RefreshGrid();
                                ClearFormDetail();
                                $("#dd_id_unit_serial").val(0).trigger("change");
                            }

                        },
                        error: function (xhr, status, error) {
                            messageerror(xhr.responseText);

                        }
                    });
                }

            } else
            {
                messageerror("Pilih Data Unit yang ingin dimasukkan");
            }
        })
        $("#dd_id_supplier_search").change(function () {
         
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/supplier/get_one_supplier',
                dataType: 'json',
                data: $("form#frm_mutasi_unit").serialize(),
                success: function (data) {
                    if (data.st)
                    {
                        $("#txt_atas_nama").val(data.obj.nama_supplier);
                        $("#txt_up").val(data.obj.up);
                        $("#txt_order_ket").val(data.obj.order);
                        $("#txt_alamat_kirim").val(data.obj.alamat);

                    } else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });


        })
        $("#dd_id_unit_search,#dd_id_prospek, #dd_id_kategori_search , #dd_id_customer_search , #dd_id_kpu , #dd_id_do_kpu , #dd_id_unit_serial").change(function () {
            var id = $(this).attr("id");
            switch (id) {
                case "dd_id_kategori_search":
                    $("#dd_id_unit_search").val(0).trigger("change.select2");
                    $("#dd_id_prospek").val(0).trigger("change.select2");
                    $("#dd_id_buka_jual").val(0).trigger("change.select2");
                    RefreshUnit();
                    RefreshProspek();
                    RefreshBukaJual();
                    RefreshDoKPU();
                    RefreshKPU();
                    RefreshSerialUnit();
                    break;
                case "dd_id_customer_search":
                    $("#dd_id_kpu").val(0).trigger("change.select2");
                    $("#dd_id_prospek").val(0).trigger("change.select2");
                    $("#dd_id_buka_jual").val(0).trigger("change.select2");
                    $("#dd_id_do_kpu").val(0).trigger("change.select2");
                    RefreshProspek();
                    RefreshBukaJual();
                    RefreshSerialUnit();
                    RefreshDoKPU();
                    break;
                case "dd_id_unit_search":
                    $("#dd_id_kpu").val(0).trigger("change.select2");
                    $("#dd_id_prospek").val(0).trigger("change.select2");
                    $("#dd_id_buka_jual").val(0).trigger("change.select2");
                    $("#dd_id_do_kpu").val(0).trigger("change.select2");
                    RefreshProspek();
                    RefreshBukaJual();
                    RefreshKPU();
                    RefreshDoKPU();
                    RefreshSerialUnit();
                    break;
                case "dd_id_prospek":
                    $("#dd_id_buka_jual").val(0).trigger("change.select2");
                    RefreshBukaJual();
                    RefreshSerialUnit();
                    break;
                case "dd_id_kpu":
                    $("#dd_id_do_kpu").val(0).trigger("change.select2");
                    RefreshDoKPU();
                    RefreshSerialUnit();
                    break;
                case "dd_id_do_kpu":
                    RefreshSerialUnit();
                    break;
                case "dd_id_unit_serial":
                    GetOneSerialUnit();
                    break;


            }






        })

        $("#dd_id_prospek").change(function () {
            var id = $(this).attr("id");


        })
    })

    function hapusrow(id_unit_serial)
    {
        var index = listdetail.findIndex(function (o) {
            return o.id_unit_serial === id_unit_serial;
        })
        if (index !== -1)
            listdetail.splice(index, 1);
        RefreshGrid();
    }

    $("#frm_mutasi_unit").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data Pembelian berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                // console.log($("#frm_mutasi_unit").serialize());return false;
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/invoice/invoice_manipulate',
                    dataType: 'json',
                    data: $("#frm_mutasi_unit").serialize() + "&" + $.param({dataset: listdetail}),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/invoice";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;
    })
    $("#dd_type_ppn").change(function () {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").val(10);
        } else
        {
            $("#txt_ppn").val(0);
        }
        RefreshPPN();
        HitungSemua();
    })
    function RefreshPPN()
    {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").removeAttr("readonly");
        } else
        {
            $("#txt_ppn").attr("readonly", "readonly");
        }
    }
    function HitungSemua() {
        var total = 0;
        var nominalppn = 0;
        var ppn = Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));
        var grandtotal=0;
        $.each(listdetail, function( index, item ) {
           
            grandtotal+=ParseNumber(item.hpp_unit);
        });
        var total = grandtotal;
        var totalsemua = total;

        if ($("#dd_type_ppn").val() == "2")
        {
            nominalppn = ppn / 100 * grandtotal;
            grandtotal = grandtotal + nominalppn;
        } else if ($("#dd_type_ppn").val() == "1")
        {
            nominalppn = ppn / (100 + Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""))) * grandtotal;
            total = total - nominalppn;
        }
        $("#txt_grandtotal").val(number_format(grandtotal.toFixed(4)));
        $("#txt_nominal_ppn").val(number_format(nominalppn.toFixed(4)));
        $("#txt_total").val(number_format(totalsemua.toFixed(4)));
    }
</script>