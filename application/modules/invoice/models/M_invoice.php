<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_invoice extends CI_Model {

    public $table = '#_pembelian';
    public $id = 'id_pembelian';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    
    function UpdateStatusInvoice($id_pembelian) {
        $pembelian= $this->GetOnePembelian($id_pembelian, "#_pembelian.id_pembelian", true);
        if ($pembelian) {
           // dumperror($pembelian->listpembayaran[count($pembelian->listpembayaran) - 1]->tanggal_transaksi);
            if (CheckEmpty($pembelian->tanggal_pelunasan)) {
                if ($pembelian->sisa <= 0) {
                    if (count($pembelian->listpembayaran) > 0) {
                        $this->db->update("#_pembelian", array("tanggal_pelunasan" => $pembelian->listpembayaran[count($pembelian->listpembayaran) - 1]->tanggal_transaksi), array("id_pembelian" => $id_pembelian));
                    }
                    else
                    {
                        $this->db->update("#_pembelian", array("tanggal_pelunasan" => $pembelian->tanggal_invoice), array("id_pembelian" => $id_pembelian));
                    }
                }
            } else if (!CheckEmpty($pembelian->tanggal_pelunasan)) {
                if ($pembelian->sisa > 0) {
                    $this->db->update("#_pembelian", array("tanggal_pelunasan" => null), array("id_pembelian" => $id_pembelian));
                }
            }
        }
    }

    
    function GenerateJurnalInvoice($keyword, $type = "#_pembelian.id_pembelian", $nomor_before) {
        $this->load->model("supplier/m_supplier");
        $pembelian = $this->GetOnePembelian($keyword, $type, false);
        $supplier = $this->m_supplier->GetOneSupplier($pembelian->id_supplier);
        $userid = GetUserId();
        if ($pembelian) {
            $this->db->from("#_unit_serial");
            $this->db->where(array("id_pembelian"=>$pembelian->id_pembelian));
            $unit=$this->db->get()->row();
            $this->load->model("neraca/m_neraca");
            $this->m_neraca->UpdateDokumen($pembelian->nomor_invoice, $nomor_before);
            
            $totalpembelianwithoutppn = 0;
            $totalpembelianwithppn = 0;
            $totalinvoice = $pembelian->grandtotal;
            $totalhitungan = $totalinvoice;
            $nominalppn = 0;
          
            if ($pembelian->type_ppn == "1") {
                $totalpembelianwithoutppn += ($totalhitungan * 100) / (100 + $pembelian->ppn);
                $totalpembelianwithppn += $totalhitungan;
            } else {
                $totalpembelianwithoutppn += $totalhitungan;
                $totalpembelianwithppn += $totalhitungan * (100 + $pembelian->ppn) / 100;
            }

            $nominalppn = $totalpembelianwithppn - $totalpembelianwithoutppn;

          

            /*
            $this->db->from("#_pembayaran_utang_detail");
            $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id");
            $this->db->where(array("keterangan" => "Pembayaran Di Muka", "id_pembelian" => $pembelian->id_pembelian));

            $pembayaran_utang_master = $this->db->get()->row();
            if ($pembayaran_utang_master != null && $kpu->jumlah_bayar == 0) {
                $this->db->delete("#_pembayaran_utang_detail", array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->delete("#_pembayaran_utang_master", array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master != null && $pembayaran_utang_master->jumlah_bayar != $kpu->jumlah_bayar) {
                $updatedet = array();
                $updatedet['updated_date'] = GetDateNow();
                $updatedet['updated_by'] = $userid;
                $updatedet['jumlah_bayar'] = $kpu->jumlah_bayar;
                $updatemas = array();
                $updatemas['updated_date'] = GetDateNow();
                $updatemas['updated_by'] = $userid;
                $updatemas['total_bayar'] = $kpu->jumlah_bayar;
                $this->db->update("#_pembayaran_utang_detail", $updatedet, array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->update("#_pembayaran_utang_master", $updatemas, array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master == null && $kpu->jumlah_bayar > 0) {
                $dokumenpembayaran = AutoIncrement('#_pembayaran_utang_master', 'BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster = array();
                $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi'] = $kpu->tanggal;
                $pembayaranmaster['id_supplier'] = $kpu->id_supplier;
                $pembayaranmaster['jenis_pembayaran'] = "Cash";
                $pembayaranmaster['total_bayar'] = $kpu->jumlah_bayar > $kpu->grandtotal ? $kpu->grandtotal : $kpu->jumlah_bayar;
                $pembayaranmaster['biaya'] = 0;
                $pembayaranmaster['potongan'] = 0;
                $pembayaranmaster['status'] = 1;
                $pembayaranmaster['keterangan'] = "Pembayaran Di Muka";
                $pembayaranmaster['created_date'] = GetDateNow();
                $pembayaranmaster['created_by'] = $userid;
                $this->db->insert("#_pembayaran_utang_master", $pembayaranmaster);
                $pembayaran_utang_id = $this->db->insert_id();
                $pembayarandetail = array();
                $pembayarandetail['id_kpu'] = $kpu->id_kpu;
                $pembayarandetail['pembayaran_utang_id'] = $pembayaran_utang_id;
                $pembayarandetail['jumlah_bayar'] = $kpu->jumlah_bayar;
                $pembayarandetail['created_date'] = GetDateNow();
                $pembayarandetail['created_by'] = $userid;
                $this->db->insert("#_pembayaran_utang_detail", $pembayarandetail);
            }
            
             */
            $this->load->model("gl_config/m_gl_config");
            $akunglpembelian = $this->m_gl_config->GetIdGlConfig("pembelian", null, array("kategori" => $unit->id_kategori, "unit" => $unit->id_unit));
            $arrakun = [];
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglpembelian, "dokumen" => $pembelian->nomor_invoice));
            $akun_pembelian = $this->db->get()->row();

            if (CheckEmpty($akun_pembelian)) {
                $akunpembelianinsert = array();
                $akunpembelianinsert['id_gl_account'] = $akunglpembelian;
                $akunpembelianinsert['tanggal_buku'] = $pembelian->tanggal_invoice;
                $akunpembelianinsert['keterangan'] = "Pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian->keterangan;
                $akunpembelianinsert['dokumen'] = $pembelian->nomor_invoice;
                $akunpembelianinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpembelianinsert['id_subject'] = $pembelian->id_supplier;
                $akunpembelianinsert['debet'] = $totalpembelianwithoutppn;
                $akunpembelianinsert['kredit'] = 0;
                $akunpembelianinsert['created_date'] = GetDateNow();
                $akunpembelianinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpembelianinsert);
                $arrakun[] = $this->db->insert_id();
            } else {
                $akunpembelianupdate['tanggal_buku'] = $pembelian->tanggal_invoice;
                $akunpembelianupdate['keterangan'] = "Pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian->keterangan;
                $akunpembelianupdate['dokumen'] = $pembelian->nomor_invoice;
                $akunpembelianupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpembelianupdate['id_subject'] = $pembelian->id_supplier;
                $akunpembelianupdate['debet'] = $totalpembelianwithoutppn;
                $akunpembelianupdate['kredit'] = 0;
                $akunpembelianupdate['updated_date'] = GetDateNow();
                $akunpembelianupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpembelianupdate, array("id_buku_besar" => $akun_pembelian->id_buku_besar));
                $arrakun[] = $akun_pembelian->id_buku_besar;
            }

            $akunglppn = $this->m_gl_config->GetIdGlConfig("ppnpembelian", null, array("kategori" => $unit->id_kategori, "unit" => $unit->id_unit));
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglppn, "dokumen" => $akun_pembelian->nomor_invoice));
            $akun_ppn = $this->db->get()->row();

            if (CheckEmpty($akun_ppn) && $pembelian->ppn_nominal > 0) {
                $akunppninsert = array();
                $akunppninsert['id_gl_account'] = $akunglppn;
                $akunppninsert['tanggal_buku'] = $pembelian->tanggal_invoice;
                $akunppninsert['keterangan'] = "PPN pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian->keterangan;
                $akunppninsert['dokumen'] = $pembelian->nomor_invoice;
                $akunppninsert['subject_name'] = @$supplier->kode_supplier;
                $akunppninsert['id_subject'] = $pembelian->id_supplier;
                $akunppninsert['debet'] = $nominalppn;
                $akunppninsert['kredit'] = 0;
                $akunppninsert['created_date'] = GetDateNow();
                $akunppninsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunppninsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_ppn)) {
                $akunppnupdate['tanggal_buku'] = $pembelian->tanggal_invoice;
                $akunppnupdate['keterangan'] = "PPN pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian->keterangan;
                $akunppnupdate['dokumen'] = $pembelian->nomor_invoice;
                $akunppnupdate['subject_name'] = @$supplier->kode_supplier;
                $akunppnupdate['id_subject'] = $pembelian->id_supplier;
                $akunppnupdate['debet'] = $nominalppn;
                $akunppnupdate['kredit'] = 0;
                $akunppnupdate['updated_date'] = GetDateNow();
                $akunppnupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunppnupdate, array("id_buku_besar" => $akun_ppn->id_buku_besar));
                $arrakun[] = $akun_ppn->id_buku_besar;
            }


            $akunglutang = $this->m_gl_config->GetIdGlConfig("utang", null, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglutang, "dokumen" => $pembelian->nomor_invoice));
            $akun_utang = $this->db->get()->row();

            if (CheckEmpty($akun_utang) ) {
                $akunutanginsert = array();
                $akunutanginsert['id_gl_account'] = $akunglutang;
                $akunutanginsert['tanggal_buku'] = $pembelian->tanggal_invoice;
                $akunutanginsert['keterangan'] = "Utang pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian->keterangan;
                $akunutanginsert['dokumen'] = $pembelian->nomor_invoice;
                $akunutanginsert['subject_name'] = @$supplier->kode_supplier;
                $akunutanginsert['id_subject'] = $pembelian->id_supplier;
                $akunutanginsert['debet'] = 0;
                $akunutanginsert['kredit'] = $pembelian->grandtotal;
                $akunutanginsert['created_date'] = GetDateNow();
                $akunutanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunutanginsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_utang)) {
                $akunutangupdate['tanggal_buku'] = $pembelian->tanggal_invoice;
                $akunutangupdate['keterangan'] = "Utang pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian->keterangan;
                $akunutangupdate['dokumen'] = $pembelian->nomor_invoice;
                $akunutangupdate['subject_name'] = @$supplier->kode_supplier;
                $akunutangupdate['id_subject'] = $pembelian->id_supplier;
                $akunutangupdate['debet'] = 0;
                $akunutangupdate['kredit'] = $pembelian->grandtotal;
                $akunutangupdate['updated_date'] = GetDateNow();
                $akunutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunutangupdate, array("id_buku_besar" => $akun_utang->id_buku_besar));
                $arrakun[] = $akun_utang->id_buku_besar;
            }
            $this->m_supplier->KoreksiSaldoHutang(@$supplier->id_supplier);
            $this->UpdateStatusInvoice($pembelian->id_pembelian);
            

            $this->db->where(array("dokumen" => $pembelian->nomor_invoice));
            if (count($arrakun) > 0) {
                $this->db->where_not_in("id_buku_besar", $arrakun);
            }
            $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)));



            $akunpenyeimbang = $this->m_gl_config->GetIdGlConfig("penyeimbang",null, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("dokumen" => $pembelian->nomor_invoice, "id_gl_account !=" => $akunpenyeimbang));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpenyeimbang, "dokumen" => $pembelian->nomor_invoice));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = $akunpenyeimbang;
                $akun_penyeimbanginsert['tanggal_buku'] = $pembelian->tanggal_invoice;
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk pembelian Ke " . @$supplier->nama_supplier . ' ' . $pembelian->keterangan;
                $akun_penyeimbanginsert['dokumen'] = $pembelian->nomor_invoice;
                $akun_penyeimbanginsert['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbanginsert['id_subject'] = $pembelian->id_supplier;
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akun_penyeimbanginsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $pembelian->tanggal_invoice;
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk Ke " . @$supplier->nama_supplier . ' ' . $pembelian->keterangan;
                $akun_penyeimbangupdate['dokumen'] = $pembelian->nomor_invoice;
                $akun_penyeimbangupdate['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbangupdate['id_subject'] = $pembelian->id_supplier;
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
                $arrakun[] = $akun_penyeimbang->id_buku_besar;
            }
           
        }
    }
    function GetDataInvoice($params) {
        $isdelete = GetGroupId() == 1;
        $this->load->library('datatables');
        $this->datatables->select('#_pembelian.*,#_pembelian.id_pembelian as id,replace(#_pembelian.nomor_invoice,"/","-") as nomor,nama_supplier,group_concat(distinct #_unit_serial.nomor_do_supplier SEPARATOR  \'<br/>\') as nomor_do_supplier');
        $this->datatables->join("#_supplier","#_supplier.id_supplier=#_pembelian.id_supplier","left");
        $this->datatables->join("#_unit_serial", "#_unit_serial.id_pembelian=#_pembelian.id_pembelian");
        $this->datatables->group_by("#_pembelian.id_pembelian");
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $extra = [];
        $where = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $pencariankeyword = "nomor_invoice";
            if (@$params['jenis_keyword'] == "nomor_invoice") {
                $pencariankeyword = "#_pembelian.nomor_invoice";
            } else if (@$params['jenis_keyword'] == "vin_number" || @$params['jenis_keyword'] == "engine_no") {
                $pencariankeyword = @$params['jenis_keyword'];
                
            }
            if ($pencariankeyword == "engine_no" || $pencariankeyword == "vin_number") {
                $this->datatables->like($pencariankeyword, strtolower($params['keyword']), 'both');
            } else {
                $where["lower(" . $pencariankeyword . ")"] = trim(strtolower($params['keyword']));
            }
        } else {
            $tanggal = "#_pembelian.tanggal_invoice";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_pembelian.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_pembelian.updated_date)";
            }

            $this->db->order_by($tanggal, "desc");


            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }



            if (!CheckEmpty($params['status'])) {
                if ($params['status'] == "6") {
                    $this->db->group_start();
                    $this->datatables->where("#_pembelian.status", 0);
                    $this->datatables->or_where("#_pembelian.status", 7);
                    $this->db->group_end();
                } else {
                    $where['#_pembelian.status'] = $params['status'];
                }
            }
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        $isedit = true;
        $strview = '';
        $stredit = '';
        $strbatal = '';
        $strprint = '';

        $strview .= anchor(site_url('invoice/view_invoice/$1/$2'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $stredit .= anchor(site_url('invoice/edit_invoice/$1/$2'), 'Update', array('class' => 'btn btn-primary btn-xs'));
            $strprint .= anchor(site_url('invoice/print_invoice/$1'), 'Cetak', array('class' => 'btn btn-info btn-print btn-xs', 'target' => '_blank'));
        }
        if ($isdelete) {
            $strbatal .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalinvoice($1,5);return false;"));
        }
        $this->db->order_by("#_pembelian.tanggal_invoice", "desc");
        $this->datatables->add_column('edit', $stredit, 'id,nomor');
        $this->datatables->add_column('view', $strview, 'id,nomor');
        $this->datatables->add_column('batal', $strbatal, 'id');
        return $this->datatables->generate();
    }
    function GetListInvoicePembayaran($id_supplier)
    {
        $this->db->frrom("#_pembelian");
        $this->db->where(array("id_supplier"=>$id_supplier,"#_pembelian.status!="=>2,"tanggal_pelunasan"=>null));
        $this->db->join("#_pembayaran_utang_detail","#_pembayaran_utang_detail.id_pembelian=#_pembelian.id_pembelian","left");
        $this->db->select("id_pembelian,jumlah_unit,grandtotal,sum(jumlah_bayar) as terbayar,grandtotal-sum(jumlah_bayar) as sisa");
        $listinvoice=$this->db->get()->result_array();
        
        
        return $listinvoice;
        
    }
    
    

    function GetDropDownDoKpuFromInvoice($param = []) {

        if (!CheckEmpty(@$param['id_kpu'])) {
            $where['#_kpu.id_kpu'] = $param['id_kpu'];
        }
        if (!CheckEmpty(@$param['id_unit'])) {
            $where['#_unit.id_unit'] = $param['id_unit'];
        }
        if (!CheckEmpty(@$param['id_kategori'])) {
            $where['#_unit.id_kategori'] = $param['id_kategori'];
        }
        if (!CheckEmpty(@$param['id_customer'])) {
            $where['#_customer.id_customer'] = $param['id_customer'];
        }

        if (count($where) > 0) {
            $this->db->where($where);
        }


        $this->db->join("#_kpu", "#_kpu.id_kpu=#_do_kpu.id_kpu");
        $this->db->join("#_customer", "#_kpu.id_customer=#_customer.id_customer", "left");
        $this->db->join("#_kpu_detail", "#_kpu.id_kpu=#_kpu_detail.id_kpu", "left");
        $this->db->join("#_unit", "#_kpu_detail.id_unit=#_unit.id_unit", "left");
        $this->db->join("#_unit_serial", "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail", "left");
        $this->db->where(array("id_pembelian" => null));
        $this->db->group_by("id_do_kpu");
        $select = "id_do_kpu,nomor_do_kpu,nomor_master,nama_customer,nama_unit,concat(#_kpu_detail.qty,' unit') as qty";
        $listkpu = GetTableData("#_do_kpu", 'id_do_kpu', array('nomor_do_kpu', 'nomor_master', "nama_customer", "nama_unit", "qty"), array('#_do_kpu.status !=' => 2, '#_do_kpu.deleted_date' => null), "json", [], [], [], $select);
        return $listkpu;
    }

    function GetDropDownKPUFromInvoice($params = []) {
        $where = array('#_kpu.status !=' => 2, '#_kpu.deleted_date' => null, "id_pembelian" => null);
        if (!CheckEmpty(@$params['id_supplier'])) {
            $where['#_kpu.id_supplier'] = @$params['id_supplier'];
        }
        if (!CheckEmpty(@$params['id_kategori'])) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_unit'])) {
            $where['#_kpu_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit'])) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer'])) {
            $where['#_kpu.id_customer'] = $params['id_customer'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_kpu_detail", "condition" => "#_kpu_detail.id_kpu=#_kpu.id_kpu");
        $arrayjoin[] = array("table" => "#_unit_serial", "condition" => "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail");
        $arrayjoin[] = array("table" => "#_vrf", "condition" => "#_vrf.id_vrf=#_kpu_detail.id_vrf");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_kpu_detail.id_unit");
        $select = 'DATE_FORMAT(#_kpu.tanggal, "%d %M %Y") as kpu_date_convert,nomor_master,nama_customer,nama_unit,#_kpu.id_kpu';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_vrf.id_customer");
        $this->db->group_by("#_kpu.id_kpu");
        $listkpu = GetTableData("#_kpu", 'id_kpu', array('nomor_master', "nama_customer", "nama_unit", "kpu_date_convert"), $where, "json", array(), array(), $arrayjoin, $select, [], 1000);
        return $listkpu;
    }

    function GetDropDownKPUFromCustomer($params) {
        $where = array($this->table . '.status' => 1, $this->table . '.deleted_date' => null);
        $where['#_kpu.id_supplier'] = @$params['id_supplier'];
        if (!CheckEmpty(@$params['id_unit'])) {
            $where['#_kpu_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit'])) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer'])) {
            $where['#_kpu.id_customer'] = $params['id_customer'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_kpu_detail", "condition" => "#_kpu_detail.id_kpu=#_kpu.id_kpu");
        $arrayjoin[] = array("table" => "#_vrf", "condition" => "#_vrf.id_vrf=#_kpu_detail.id_vrf");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_kpu_detail.id_unit");
        $select = 'DATE_FORMAT(#_kpu.tanggal, "%d %M %Y") as kpu_date_convert,nomor_master,nama_customer,nama_unit,#_kpu.id_kpu';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_vrf.id_customer");
        $listkpu = GetTableData("#_kpu", 'id_kpu', array('nomor_master', "nama_customer", "nama_unit", "kpu_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listkpu;
    }

    function GetDropDownSerialInvoice($param = []) {
        if (!CheckEmpty(@$param['id_unit'])) {
            $where['#_unit.id_unit'] = $param['id_unit'];
        }
        if (!CheckEmpty(@$param['id_kategori'])) {
            $where['#_unit.id_kategori'] = $param['id_kategori'];
        }
        if (!CheckEmpty(@$param['id_prospek'])) {
            $where['#_prospek_detail.id_prospek'] = $param['id_prospek'];
        }
        if (!CheckEmpty(@$param['id_kpu'])) {
            $where['#_kpu_detail.id_kpu'] = $param['id_kpu'];
        }
        if (!CheckEmpty(@$param['id_buka_jual'])) {
            $where['#_unit_serial.id_buka_jual'] = $param['id_buka_jual'];
        }
        if (!CheckEmpty(@$param['id_do_kpu_detail'])) {
            $where['#_unit_serial.id_do_kpu_detail'] = $param['id_do_kpu_detail'];
        }
        if (count($where) > 0) {
            $this->db->where($where);
        }
        $this->db->join("#_customer", "#_kpu.id_customer=#_customer.id_customer", "left");
        $this->db->join("#_kpu_detail", "#_kpu.id_kpu=#_kpu_detail.id_kpu", "left");
        $this->db->join("#_unit", "#_kpu_detail.id_unit=#_unit.id_unit", "left");
        $this->db->join("#_unit_serial", "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail", "left");
        $this->db->where(array("#_unit_serial.id_pembelian" => null));
        $this->db->group_by("#_kpu.id_kpu");
        $select = "#_kpu.id_kpu,nomor_master,nama_customer,nama_unit,concat(#_kpu_detail.qty,' unit') as qty";
        $listkpu = GetTableData("#_kpu", 'id_kpu', array("id_kpu", 'nomor_master', "nama_customer", "nama_unit", "qty"), array($this->table . '.status !=' => 2, $this->table . '.deleted_date' => null), "json", [], [], [], $select);

        return $listkpu;
    }

    function GetDropDownProspekForInvoice($params) {
        $where = array('#_prospek.status !=' => 2, '#_prospek.deleted_date' => null, "#_unit_serial.id_pembelian" => null);

        if (!CheckEmpty(@$params['id_kategori']) || @$params['id_kategori'] > 0) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_unit']) || @$params['id_unit'] > 0) {
            $where['#_prospek_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit']) || @$params['id_type_unit'] > 0) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer']) || @$params['id_customer'] > 0) {
            $where['#_prospek.id_customer'] = $params['id_customer'];
        }
        if (!CheckEmpty(@$params['id_do_kpu']) || @$params['id_do_kpu'] > 0) {
            $where['#_do_kpu.id_do_kpu'] = $params['id_do_kpu'];
        }
        if (!CheckEmpty(@$params['id_kpu']) || @$params['id_kpu'] > 0) {
            $where['#_kpu.id_kpu'] = $params['id_kpu'];
        }

        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_prospek_detail", "condition" => "#_prospek_detail.id_prospek=#_prospek.id_prospek");
        $arrayjoin[] = array("table" => "#_unit_serial", "condition" => "#_unit_serial.id_prospek_detail=#_prospek_detail.id_prospek_detail");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_prospek_detail.id_unit");
        $select = 'DATE_FORMAT(#_prospek.tanggal_prospek, "%d %M %Y") as prospek_date_convert,no_prospek,nama_customer,nama_unit,#_prospek.id_prospek';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_prospek.id_customer");
        $this->db->group_by("#_prospek.id_prospek");
        $listprospek = GetTableData("_prospek", 'id_prospek', array('nomor_master', "nama_customer", "nama_unit", "prospek_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listprospek;
    }

    function GetDropDownBukaJualForInvoice($params) {
        $where = array('#_buka_jual.status' => 1, '#_buka_jual.deleted_date' => null);
        $where['#_buka_jual.id_prospek'] = $params['id_prospek'];
        // if (!CheckEmpty(@$params['id_kategori']) || @$params['id_kategori'] > 0) {
        //     $where['#_unit.id_kategori'] = $params['id_kategori'];
        // }
        // if (!CheckEmpty(@$params['id_unit']) || @$params['id_unit'] > 0) {
        //     $where['#_kpu_detail.id_unit'] = $params['id_unit'];
        // }
        // if (!CheckEmpty(@$params['id_type_unit']) || @$params['id_type_unit'] > 0) {
        //     $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        // }
        // if (!CheckEmpty(@$params['id_customer']) || @$params['id_customer'] > 0) {
        //     $where['#_kpu.id_customer'] = $params['id_customer'];
        // }
        $arrayjoin = [];
        $select = 'DATE_FORMAT(#_buka_jual.tanggal_buka_jual, "%d %M %Y") as buka_jual_date_convert,nomor_buka_jual,atas_nama,nama_unit,#_buka_jual.id_buka_jual';
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_buka_jual.id_unit");
        $arrayjoin[] = array("table" => "#_prospek", "condition" => "#_prospek.id_prospek=#_buka_jual.id_prospek");
        $listkpu = GetTableData("#_buka_jual", 'id_buka_jual', array('nomor_buka_jual', "atas_nama", "nama_unit", "buka_jual_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listkpu;
    }

    function GetDropDownUnitSerialForInvoice($model, $is_edit = false) {

        $where = [];
        if (!CheckEmpty(@$model['id_buka_jual'])) {
            $where['#_unit_serial.id_buka_jual'] = $model['id_buka_jual'];
        }


        if (!CheckEmpty(@$model['id_prospek'])) {
            $where['#_buka_jual.id_prospek'] = $model['id_prospek'];
        }


        if (!CheckEmpty(@$model['id_customer'])) {
            $where['#_buka_jual.id_customer'] = $model['id_customer'];
        }
        if (!CheckEmpty(@$model['id_unit'])) {
            $where['#_unit.id_unit'] = $model['id_unit'];
        }
        if (!CheckEmpty(@$model['id_kategori'])) {
            $where['#_unit.id_kategori'] = $model['id_kategori'];
        }
        if (!CheckEmpty(@$model['id_do_kpu'])) {
            $where['#_do_kpu.id_do_kpu'] = $model['id_do_kpu'];
        }
        if (!CheckEmpty(@$model['id_kpu'])) {
            $where['#_do_kpu.id_kpu'] = $model['id_kpu'];
        }



        if ($is_edit) {
            if (count($where) == 0) {
                $where['#_unit_serial.id_unit_serial'] = null;
            }
        }
        $where['#_unit_serial.id_pembelian'] = null;

        $this->db->where($where);

        $this->db->join("#_unit", "#_unit.id_unit=#_unit_serial.id_unit", "left");
        $this->db->join("#_do_kpu_detail", "#_do_kpu_detail.id_do_kpu_detail=#_unit_serial.id_do_kpu_detail", "left");
        $this->db->join("#_do_kpu", "#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu", "left");
        $this->db->join("#_type_unit", "#_type_unit.id_type_unit=#_unit.id_type_unit", "left");
        $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
        $this->db->join("#_type_body", "#_type_body.id_type_body=#_unit.id_type_body", "left");
        $this->db->join("#_warna", "#_warna.id_warna=#_unit_serial.id_warna", "left");
        $this->db->join("#_pool", "#_pool.id_pool=#_unit_serial.id_pool", "left");
        // $this->db->join("#_do_prospek", "#_do_prospek.id_do_prospek=#_unit_serial.id_do_so", "left");
        $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual", "left");

        $listserial = GetTableData("#_unit_serial", 'id_unit_serial', array("no_prospek", "vin_number", "engine_no", "nama_type_unit", "nama_unit"), array('#_unit_serial.deleted_date' => null), "json");
        return $listserial;
    }

    function GetOnePembelian($id_pembelian,$field="",$isfull=true) {
        $this->db->from("#_pembelian");
        $this->db->where(array("id_pembelian" => $id_pembelian));
        $pembeliandb = $this->db->get()->row();
        if($isfull)
        {   
            $this->db->from("#_pembayaran_utang_detail");
            $this->db->join("#_pembayaran_utang_master","#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id");
            $this->db->where(array("#_pembayaran_utang_master.status !="=>2,"#_pembayaran_utang_detail.id_pembelian"=>$id_pembelian));
            $this->db->select("sum(jumlah_bayar) as terbayar");
            $rowjumlah_bayar=$this->db->get()->row();
            $pembeliandb->terbayar=0;
            if($rowjumlah_bayar)
            {
                $pembeliandb->terbayar=$rowjumlah_bayar->terbayar;
            }
            $pembeliandb->sisa=$pembeliandb->grandtotal-$pembeliandb->terbayar;
            $this->db->from("#_unit_serial");
            $this->db->join("#_unit","#_unit.id_unit=#_unit_serial.id_unit");
            $this->db->join("#_buka_jual","#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual","left");
            $this->db->join("#_customer","#_buka_jual.id_customer=#_customer.id_customer","left");
            $this->db->join("#_kpu_detail","#_kpu_detail.id_kpu_detail=#_unit_serial.id_kpu_detail","left");
            $this->db->select("id_unit_serial,#_kpu_detail.id_kpu,nomor_buka_jual,nama_unit,vin_number,engine_no,nama_customer,(#_kpu_detail.subtotal/#_kpu_detail.qty) as hpp_unit");
            $this->db->where(array("id_pembelian"=>$id_pembelian));
            $pembeliandb->listdetail=$this->db->get()->result();
            
            
            $this->db->from("#_pembayaran_utang_detail");
            $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id", "left");
            $this->db->where(array("id_pembelian" => $id_pembelian, "#_pembayaran_utang_master.status !=" => 2));
            $this->db->order_by("#_pembayaran_utang_master.tanggal_transaksi", "asc");
            $listpembayaran = $this->db->get()->result();
            $jumlah = 0;
            foreach ($listpembayaran as $pembayaran) {
                $jumlah += $pembayaran->jumlah_bayar;
            }
            $pembeliandb->listpembayaran=$listpembayaran;
            
            
        }
        
        return $pembeliandb;
    }
    
    public function GetDetailPembayaranHutang($id_pembelian,$pembayaran_utang_id=0){
        $this->db->from("#_unit_serial");
        $this->db->join("#_kpu_detail", "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail","left");
        $this->db->join("#_pembayaran_utang_detail", "#_pembayaran_utang_detail.id_unit_serial=#_unit_serial.id_unit_serial","left");
        $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id","left");
        $this->db->where(array("#_unit_serial.id_pembelian" => $id_pembelian));
        $this->db->select("#_unit_serial.id_unit_serial,#_unit_serial.id_pembelian,#_kpu_detail.id_kpu,(#_kpu_detail.subtotal/#_kpu_detail.qty) as hpp_unit,SUM(IF( #_pembayaran_utang_master.status !=2, jumlah_bayar, 0)) as terbayar");
        $this->db->group_by("#_unit_serial.id_unit_serial");
        $listunitserial_bayar = $this->db->get()->result();
       
        $listbayarutang=[];
        if(!CheckEmpty($pembayaran_utang_id))
        {
            $this->db->from("#_pembayaran_utang_detail");
            $this->db->where(array("pembayaran_utang_id"=>$pembayaran_utang_id));
            $listdbutang=$this->db->get()->result_array();
          
            foreach($listdbutang as $pemdet)
            {
                $listbayarutang[$pemdet['id_unit_serial']]=$pemdet;
            }
        }
     
        foreach($listunitserial_bayar as $serial_unit)
        {
           
            if(CheckKey($listbayarutang, $serial_unit->id_unit_serial))
            {
                $serial_unit->pembayaran_utang_detail_id=$listbayarutang[$serial_unit->id_unit_serial]['pembayaran_utang_detail_id'];
                $serial_unit->terbayar= $serial_unit->terbayar-$listbayarutang[$serial_unit->id_unit_serial]['jumlah_bayar'];
            }
            $serial_unit->sisa=$serial_unit->hpp_unit - $serial_unit->terbayar;
        }
        
        return $listunitserial_bayar;
    }
    
    
    

    function InvoiceManipulate($model) {
        try {
            $this->load->model("do_kpu/m_do_kpu");
            $message = "";

            $this->db->trans_rollback();
            $this->db->trans_begin();

            $invoice['keterangan'] = $model['keterangan'];
            $invoice['tanggal_invoice'] = DefaultTanggalDatabase($model['tanggal_invoice']);
            $invoice['nomor_invoice'] = $model['nomor_invoice'];
            $invoice['faktur_pajak'] = $model['faktur_pajak'];
            $invoice['top'] = $model['top'];
            $invoice['jth_tempo'] = DefaultTanggalDatabase($model['jth_tempo']);
            $invoice['jumlah_unit'] = count($model['dataset']);
            $invoice['type_ppn'] = $model['type_ppn'];
            $invoice['ppn'] = $model['ppn'];
            $invoice['id_supplier'] = $model['id_supplier'];
            $userid = GetUserId();
         
            $id_pembelian = 0;
            $nominalppn = 0;
            $total = $model['total'];
            $grandtotal = $model['total'];
            if ($invoice['type_ppn'] == "2") {
                $nominalppn = $model['ppn'] / 100 * $grandtotal;
                $grandtotal = $grandtotal + $nominalppn;
            } else if ($invoice['type_ppn'] == "1") {
                $nominalppn = $model['ppn'] / (100 + $model['ppn']) * $grandtotal;
                $total = $total - $nominalppn;
            }
            $invoice['total'] = $total;
            $invoice['grandtotal'] = $grandtotal;
            $invoice['ppn_nominal'] = $nominalppn;

            if (CheckEmpty(@$model['id_pembelian'])) {
                $invoice['created_date'] = GetDateNow();
                $invoice['created_by'] = ForeignKeyFromDb(GetUserId());
                $invoice['status'] = 1;
                if (CheckEmpty($invoice['nomor_invoice'])) {
                    $invoice['nomor_invoice'] = AutoIncrement('#_pembelian', 'PO/' . date("y") . '/', 'nomor_invoice', 5);
                }

                $res = $this->db->insert("#_pembelian", $invoice);

                $id_pembelian = $this->db->insert_id();
            } else {
                $invoice['updated_date'] = GetDateNow();
                $invoice['updated_by'] = ForeignKeyFromDb(GetUserId());
                $res = $this->db->update("#_pembelian", $invoice, array("id_pembelian" => $model['id_pembelian']));
                $id_pembelian = $model['id_pembelian'];
                $pembeliandb = $this->GetOnePembelian($id_pembelian, "id_pembelian", false);
                $invoice['nomor_invoice'] = $pembeliandb->nomor_invoice;
            }

 
            $listidunit==[];
            foreach ($model['dataset'] as $detail) {
                $listidunit[]=$detail['id_unit_serial'];
                $this->db->from("#_unit_serial");
                $this->db->where(array("id_pembelian !=" => null, "id_unit_serial" => $detail['id_unit_serial']));
                $pembeliandetaildb = $this->db->get()->row();
               
                if ($pembeliandetaildb&&$pembeliandetaildb->id_pembelian!=$id_pembelian) {
                    $message .= "Unit " . $detail['vin_number'] . " sudah memiliki pembelian<br/>";
                } else {
                    $this->db->update("#_unit_serial", array("id_pembelian" => $id_pembelian), array("id_unit_serial" => $detail['id_unit_serial']));
                }
            }
            $this->db->from("#_unit_serial");
            $this->db->where_not_in("id_unit_serial",$listidunit);
            $this->db->update("#_unit_serial",array("id_pembelian"=>null),array("id_pembelian"=>$id_pembelian));
            

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Invoice Sudah di" . (CheckEmpty(@$model['id_pembelian']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_pembelian, $invoice['nomor_invoice'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

}
