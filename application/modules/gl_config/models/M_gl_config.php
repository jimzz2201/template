<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_gl_config extends CI_Model {

    public $table = '#_gl_config';
    public $id = 'id_gl_config';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    
    function GetGlModule()
    {
        $this->db->from("#_gl_module");
        $this->db->join("#_gl_account","#_gl_Account.id_gl_account=#_gl_module.id_gl_account","left");
        $this->db->where(array("#_gl_module.status"=>1));
        $listglmodule=$this->db->get()->result();
        foreach($listglmodule as $mod)
        {
            $this->db->from("#_gl_config");
            $this->db->where(array("id_gl_module"=>$mod->id_gl_module,"type"=>"config"));
            $mod->listdetail=$this->db->get()->result();
            
            $this->db->from("#_gl_config");
            $this->db->where(array("#_gl_config.id_gl_module"=>$mod->id_gl_module,"type !="=>"config"));
            $this->db->join("#_gl_module","#_gl_module.id_gl_module=#_gl_config.id_gl_module","left");
            $this->db->join("#_cabang","#_cabang.id_cabang=#_gl_config.id_cabang","left");
            $this->db->join("#_gl_account","#_gl_account.id_gl_account=#_gl_config.id_gl_account","left");
            $mod->listotherdetail=$this->db->get()->result();
            
            
        }
        return $listglmodule;
    }
    
    function GetGlConfig($id_subject,$type,$cabang=null)
    {
        $this->db->from("#_gl_config");
        $this->db->where(array("type" => $type,"id_subject"=>$id_subject));
        if(!CheckEmpty(@$cabang))
        {
            $this->db->where("id_cabang",$cabang);
        }
        $listreturn = $this->db->get()->result();
        return $listreturn;
    }
    
    function GetIdGlConfig($module,$id_cabang=0,$listgl=array())
    {
        $id_gl_account=0;
        $this->db->from("#_gl_module");
        $this->db->where(array("nama_gl_module"=>$module));
        $akunmaster=$this->db->get()->row();
        if($akunmaster)
        {
            $id_gl_account=$akunmaster->id_gl_account;
        }
        
        if(!CheckEmpty($id_cabang))
        {
            $this->db->select("#_gl_config.*");
            $this->db->from("#_gl_config");
            $this->db->join("#_gl_module","#_gl_module.id_gl_module=#_gl_config.id_gl_module");
            $this->db->where(array("nama_gl_module"=>$module,"id_cabang"=>$id_cabang,"id_subject"=>null,"type"=>"config"));
            $gl_config=$this->db->get()->row();
            if($gl_config)
            {
                $id_gl_account=$gl_config->id_gl_account;
            }
        }
        
        
        if(count($listgl)>0)
        {
            foreach($listgl as $key=>$glsat)
            {
                if(!CheckEmpty($id_cabang))
                {
                    $this->db->select("#_gl_config.*");
                    $this->db->from("#_gl_config");
                    $this->db->join("#_gl_module","#_gl_module.id_gl_module=#_gl_config.id_gl_module");
                    $this->db->where(array("nama_gl_module"=>$module,"id_cabang"=>$id_cabang,"id_subject"=>$glsat,"type"=>$key));
                    $gldef=$this->db->get()->row();
                    if($gldef)
                    {
                        $id_gl_account=$gldef->id_gl_account;
                    }
                }
                $this->db->select("#_gl_config.*");
                $this->db->from("#_gl_config");
                $this->db->join("#_gl_module", "#_gl_module.id_gl_module=#_gl_config.id_gl_module");
                $this->db->where(array("nama_gl_module" => $module,  "id_subject" => $glsat, "type" => $key));
                $gldef = $this->db->get()->row();
                if ($gldef) {
                    $id_gl_account = $gldef->id_gl_account;
                }
            }
        }
        return $id_gl_account;
    }
    
    
    
    function GetDropDownGlModule($modulekategori="")
    {
        if(!CheckEmpty($modulekategori))
        {
            $this->db->where_in("nama_gl_module",$modulekategori);
        }
        $listkategori = GetTableData("#_gl_module", 'id_gl_module', array('description_module'), array('#_gl_module.status' => 1));

        return $listkategori;
    }
    
    
    
    function GlConfigManipulate($module) {
        try {
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $arrchildid=[];
            $message="";
            foreach($module as $mod)
            {
                $this->db->update("#_gl_module",array("id_gl_account"=>$mod['id_gl_account']),array("id_gl_module"=>$mod['id_gl_module']));
                foreach($mod['listchild'] as $child)
                {
                    $childwhere=array("id_gl_module"=>$mod['id_gl_module'],"id_cabang"=>$child['id_cabang'],"type"=>"config");
                    $this->db->from("#_gl_config");
                    $this->db->where($childwhere);
                    $rowchild=$this->db->get()->row();
                    if($rowchild)
                    {
                        $arrchildid[]=$rowchild->id_gl_config;
                        $this->db->update("#_gl_config",MergeUpdate(array("id_gl_account"=>$child['id_gl_account'])),array('id_gl_config'=>$rowchild->id_gl_config));
                    }
                    else{
                        $childinsert=$childwhere;
                        $childinsert['id_gl_account']=$child['id_gl_account'];
                        $this->db->insert("#_gl_config", MergeCreated($childinsert));
                        $insert_id=$this->db->insert_id();
                        $arrchildid[]=$insert_id;
                    }
                }
            }
            $this->db->where(array("type"=>"config"));
            if(count($arrchildid)>0)
            {
                $this->db->where_not_in("id_gl_config",$arrchildid);
            }
            $this->db->delete("#_gl_config");
            
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(1, "Data Akun Sudah diupdate");
                $arrayreturn["st"] = true;
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }


}
