
<section class="content-header">
    <h1>
        Gl Config
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Gl Config</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">
        <form id="frmglconfig">
            <div class="box-body">
                <div id="notification" ></div>
                <button type="submit" class="btn btn-primary" id="btt_submit" >Save Setting</button>
                <button style="float:right" type="button" class="btn btn-danger" id="btt_restore" >Restore Default</button>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body form">
                        <table class="table table-striped table-bordered table-hover" id="mytable">
                            <thead>
                            <th style="width:500px;">Module</th>
                            <th>Cabang</th>
                            <th>GL Account</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                <?php foreach ($list_gl_module as $mod) { ?>
                                    <tr class="tr<?php echo $mod->id_gl_module ?>">
                                <input type="hidden" name="id_gl_module[]" value="<?php echo $mod->id_gl_module; ?>" /> 
                                <input type="hidden" name="description[]" value="<?php echo $mod->description_module; ?>" /> 
                                <td><?= $mod->description_module ?></td>
                                <td>&nbsp;</td>
                                <td><?= form_dropdown(array("selected" => @$mod->id_gl_account, "id" => "dd_id_gl_account" . $mod->id_gl_module, "name" => "id_gl_account[]"), DefaultEmptyDropdown(@$list_gl, "json", "Akun"), @$mod->id_gl_account, array('class' => 'form-control')); ?></td>
                                <td style="width:100px;">
                                    <a class='btn  btn-success' href='javascript:;' onclick="AddGlModule(<?= $mod->id_gl_module ?>)">Add Child</a>
                                </td>
                                </tr>
                                <?php foreach ($mod->listdetail as $detdetail) { ?>

                                    <tr class="tr<?php echo $mod->id_gl_module ?>">
                                        <td>&nbsp;</td>
                                        <td><?= form_dropdown(array("selected" => $detdetail->id_cabang, "id" => "dd_id_cabang_detail_account" . $detdetail->id_gl_config, "name" => "id_cabang" . $mod->id_gl_module . "[]"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), $detdetail->id_cabang, array('class' => 'form-control')); ?></select></td>
                                        <td><?= form_dropdown(array("selected" => $detdetail->id_gl_account, "id" => "dd_id_gl_detail_account" . $detdetail->id_gl_config, "name" => "id_gl_account" . $mod->id_gl_module . "[]"), DefaultEmptyDropdown(@$list_gl, "json", "Akun"), $detdetail->id_gl_account, array('class' => 'form-control')); ?></td>
                                        <td style="width:100px;">
                                            <a class="btn  btn-danger" href="javascript:;" onclick="DeleteGlModule(this)">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php
                                if (count($mod->listotherdetail) > 0) {
                                    echo "<tr><td colspan='4' style='padding:10px;text-align:center'><a href='javascript:;' onClick='ExpandModule(" . $mod->id_gl_module . ")'>" . $mod->description_module . " " . count($mod->listotherdetail) . " setting configure </a></td></tr>";
                                }

                                foreach ($mod->listotherdetail as $detdetail) {
                                    ?>

                                    <tr  style="display:none" class="trchild<?php echo $detdetail->id_gl_module ?>">
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $detdetail->description ?></td>
                                        <td><?php echo $detdetail->nama_cabang ?></td>
                                        <td><?php echo $detdetail->nama_gl_account ?></td>
                                        <td style="width:100px;">
                                            <a class="btn  btn-info" href="<?php echo base_url() . $detdetail->url ?>" target="_blank">View</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </form>
    </div>

</section>
<script>
    var datagl =<?php echo json_encode(DefaultEmptyDropdown(@$list_gl, "json", "Akun")) ?>;
    var datacabang =<?php echo json_encode(DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")) ?>;
    $(document).ready(function () {
        $("select").select2();
    })
    var counter = 0;
    function ExpandModule(trid)
    {

        if ($(".trchild" + trid).length)
        {
            if ($(".trchild" + trid).css('display') === 'contents')
            {
                $(".trchild" + trid).css("display", "none");
            } else
            {
                $(".trchild" + trid).css("display", "contents");
            }
        }
    }
    $("#frmglconfig").submit(function () {
        swal({
            title: "Apakah kamu yakin ingin menginput data akun berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/gl_config/gl_config_manipulate',
                    dataType: 'json',
                    data: $("#frmglconfig").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            window.location.reload();
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;

    })
    var counter = 0;
    function AddGlModule(trid)
    {
        var newRow = $("<tr class='tr" + trid + "'>");
        var cols = "";
        counter += 1;
        cols += '<td>&nbsp;</td>';
        cols += '<td><select name="id_cabang' + trid + '[]" class="form-control cabangchild' + trid + '" ></select></td>';
        cols += '<td><select name="id_gl_account' + trid + '[]" class="form-control child' + trid + '" ></select></td>';
        cols += '<td><a class="btn  btn-danger" href="javascript:;" onclick="DeleteGlModule(this)">Delete</a></td>';
        newRow.append(cols);
        newRow.insertAfter($(".tr" + trid + ":last"));
        $(".child" + trid + ":last").select2({data: datagl});
        $(".cabangchild" + trid + ":last").select2({data: datacabang});
        $(".child" + trid + ":last").val($("#dd_id_gl_account" + trid).val());
        $(".child" + trid + ":last").trigger("change");
    }
    function DeleteGlModule(module)
    {
        $(module).parent().parent().remove();
    }

</script>