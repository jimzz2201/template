<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gl_config extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_gl_config');
    }
    
    

    public function index() {
        $this->load->model("gl/m_gl");
        $this->load->model("cabang/m_cabang");
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K083";
        $header = "K080";
        $title = "Gl Config";

        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_gl_module'] = $this->m_gl_config->GetGlModule();
        $model['list_gl'] = $this->m_gl->GetDropDownGl();
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        //dumperror($model['list_gl']);
        //die();
        CekModule($module);
        LoadTemplate($model, "gl_config/v_gl_config_index", $javascript);
    }
    

    public function gl_config_manipulate() {
        $message = '';
        $model = $this->input->post();
        $module=[];
        $glmodule= CheckArray($model, "id_gl_module");
        $glaccount =  CheckArray($model, "id_gl_account");
        $description = CheckArray($model, "description");
        $message="";
        foreach($glmodule as $key=>$id_gl_module)
        {
            $det=[];
            $det['id_gl_module']=$id_gl_module;
            $det['id_gl_account']= @$glaccount[$key];
            if(CheckEmpty(@$glaccount[$key]))
            {
                $message.="GL ".$description[$key]." tidak boleh kosong<br/>";
            }
            
            $arrchildcabang= CheckArray($model, "id_cabang".$id_gl_module);
            $arrchildgl= CheckArray($model, "id_gl_account".$id_gl_module);
            $arrdet=[];
            foreach($arrchildcabang as $keydetail=>$childcabang)
            {
                if(!CheckEmpty($childcabang)&& !CheckEmpty($arrchildgl[$keydetail]))
                {
                    if(Multi_array_search($arrdet, "id_cabang", $childcabang))
                    {
                        $message.="GL ".$description[$key]." sudah memiliki config yang serupa sebelumnya<br/>";
                    }
                
                    $detailchild=[];
                    $detailchild['id_gl_account']=$arrchildgl[$keydetail];
                    $detailchild['id_cabang']=$childcabang;
                    $arrdet[]=$detailchild;
                }
                else if(!CheckEmpty($childcabang) && CheckEmpty($arrchildgl[$keydetail]))
                {
                    $message.="GL ".$description[$key]." child ke ".($keydetail+1)." tidak boleh kosong<br/>";
                }
                else if(CheckEmpty($childcabang) && !CheckEmpty($arrchildgl[$keydetail]))
                {
                    $message.="Cabang ".$description[$key]." child ke ".($keydetail+1)." tidak boleh kosong<br/>";
                }
                
                
                
                     
            }
            $det['listchild']=$arrdet;
            $module[]=$det;
        }

        if ( $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' .  $message));
        } else {
            $status = $this->m_gl_config->GlConfigManipulate($module);
            echo json_encode($status);
        }
    }

   

}

/* End of file Gl.php */