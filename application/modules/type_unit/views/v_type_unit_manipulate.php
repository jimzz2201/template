<section class="content-header">
    <h1>
        Type Unit <?= @$button ?>
        <small>Type Unit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Type Unit</li>
        <li class="active">Type Unit <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <form id="frm_type_unit" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_type_unit" id="id_type_unit" value="<?php echo @$id_type_unit; ?>" /> 
                                <input type="hidden" name="id_type_unit_tmp" id="id_type_unit_tmp" value="<?php echo @$id_type_unit_tmp; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Kode Type Unit', "txt_kode_type_unit", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $mergearray = array();
                                        if (!CheckEmpty(@$id_type_unit)) {
                                            $mergearray['readonly'] = "readonly";
                                            $mergearray['name'] = "kode_type_unit";
                                        } else {
                                            $mergearray['name'] = "kode_type_unit";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_type_unit, 'class' => 'form-control', 'id' => 'txt_kode_type_unit', 'placeholder' => 'Kode Type Unit'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Type Unit', "txt_nama_type_unit", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_type_unit', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Nama Type Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Kategori', "txt_nama_type_unit", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori', 'class' => 'form-control select2', 'id' => 'dd_id_kategori', 'placeholder' => 'Type Unit'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$id_kategori); ?>
                                       </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>
                                <hr/>
                                <div >
                                   
                                    <?php echo anchor("", 'Tambah Harga', 'class="btn btn-success" id="btt_create"'); ?>
                                </div>
                                 <br/>
                                <div class="portlet-body form" id="tbl">
                                    <!-- <table class="table table-striped table-bordered table-hover" id="mytable"></table> -->
                                </div>
                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/type_unit' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("select").select2();
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });

        $("#btt_create").click(function () {
            $.ajax({url: baseurl + 'index.php/price_list/create_price_list', data: "id_type_unit=" + $("#id_type_unit_tmp").val(), type: 'post',
                success: function (data) {
                    modalbootstrap(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        });
    });
    window.onload = function () {
        getPriceTable();
    }

    function getPriceTable() {
        var id_type_unit = $("#id_type_unit_tmp").val();
        var manage = true;
        $.ajax({
            url: baseurl + 'index.php/price_list/getdataprice_list/' + id_type_unit,
            data: 'manage=' + manage,
            method: 'post',
            success: function (data) {
                $("#tbl").html(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            },
        })
    }

    function deleteprice_list(id_price_list) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/price_list/price_list_delete',
                    dataType: 'json',
                    data: {
                        id_price_list: id_price_list
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            getPriceTable();
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
    }
    function getTop() {
        // alert('getT top');
        var idTypeUnit = $("#dd_type_unit").val();
        $.ajax({
            url: baseurl + 'index.php/price_list/get_top',
            data: 'id_type_unit=' + idTypeUnit,
            type: 'post',
            success: function (data) {
                $("#topTable").html(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            },
        })
    }
    $("#frm_type_unit").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/type_unit/type_unit_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/type_unit';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
    th {
        text-align: center !important;
    }
    .right {
        text-align: right !important;
    }
    .headTable {
        background-color: #0396B0 !important;
        color: #FFFFFF !important;
    }
</style>