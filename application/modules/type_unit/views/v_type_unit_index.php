
<section class="content-header">
    <h1>
        Type Unit
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Type Unit</li>
    </ol>


</section>
    
<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">
                <?php echo anchor(site_url('type_unit/create_type_unit'), 'Create', 'class="btn btn-success"'); ?>
	        </div>
        </div>
         <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body form">
                        <table class="table table-striped table-bordered table-hover" id="mytable"></table>
                    </div>
                </div>
            </div>
        </div>

</section>
<script type="text/javascript">
        var table;
    function deletetype_unit(id_type_unit) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/type_unit/type_unit_delete',
                dataType: 'json',
                data: {
                    id_type_unit: id_type_unit
                },
                success: function (data) {
                    if (data.st)
                    {
                        messagesuccess(data.msg);
                        table.fnDraw(false);
                    }
                    else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
       }});
    }
    function edittype_unit(id_type_unit) {

        $.ajax({url: baseurl + 'index.php/type_unit/edit_type_unit/' + id_type_unit,
            success: function (data) {
                modalbootstrap(data, "Edit Type_unit");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    $(document).ready(function() {
        $("#btt_create").click(function () {
            $.ajax({url: baseurl + 'index.php/type_unit/create_type_unit',
                success: function (data) {
                        modalbootstrap(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        })
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
        table = $("#mytable").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "type_unit/getdatatype_unit", "type": "POST"},
            columns: [
                {
                    data: "id_type_unit",
                    title: "No",
                    orderable: false,
                    width:"10px",
                    className:"text-right"
                },
            {data: "kode_type_unit" , orderable:false , title :"Kode Type Unit", width:"100px",},
            {data: "nama_type_unit" , orderable:true , title :"Nama Type Unit"},
            {data: "nama_kategori" , orderable:true , title :"Kategori"},
            {data: "status" , orderable:false , title :"Status",
                mRender: function (data, type, row) {
                    return data==1?"Active":"Not Active";
                }},
                {
                    "data" : "action",
                    "orderable": false,
                    "width":"100px",
                    "className" : "text-center"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
    