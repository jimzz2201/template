<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_type_unit extends CI_Model
{

    public $table = '#_type_unit';
    public $id = 'id_type_unit';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatatype_unit() {
        $this->load->library('datatables');
        $this->datatables->select('id_type_unit,kode_type_unit,nama_type_unit,nama_kategori,#_type_unit.status');
        $this->datatables->join('#_kategori',"#_kategori.id_kategori=#_type_unit.id_kategori","left");
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_type_unit.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('type_unit/edit_type_unit/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletetype_unit($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_type_unit');
        return $this->datatables->generate();
    }

    // get all
    function GetOneType_unit($keyword, $type = 'id_type_unit') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $type_unit = $this->db->get($this->table)->row();
        return $type_unit;
    }

    function Type_unitManipulate($model) {
        try {
                $model['kode_type_unit'] = DefaultCurrencyDatabase($model['kode_type_unit']);
                $model['status'] = DefaultCurrencyDatabase($model['status']);
                $model['id_kategori'] = ForeignKeyFromDb($model['id_kategori']);
                $idTypeUnitTmp = $model['id_type_unit_tmp'];
                unset($model['id_type_unit_tmp']);

            if (CheckEmpty($model['id_type_unit'])) {                
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());

                $this->db->trans_start();
                $this->db->insert($this->table, $model);
                $id_type_unit = $this->db->insert_id();
                $this->db->update("#_price_list", array('id_type_unit' => $id_type_unit), array('id_type_unit' => $idTypeUnitTmp));
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $status = false;
                    $msg = "failed insert into database";
                } else {
                    $status = true;
                    $msg = "Pricelist successfull added into database";
                    SetMessageSession(1, 'Pricelist successfull added into database');
                }
                return array("st" => $status, "msg" => $msg);
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_type_unit" => $model['id_type_unit']));
		        return array("st" => true, "msg" => "Type_unit has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownType_unit() {
        $listtype_unit = GetTableData($this->table, 'id_type_unit', 'nama_type_unit', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listtype_unit;
    }

    function Type_unitDelete($id_type_unit) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_type_unit' => $id_type_unit));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Type_unit has been deleted from database");
    }


}