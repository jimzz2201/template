<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Type_unit extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_type_unit');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K018";
        $header = "K001";
        $title = "Type Unit";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	    CekModule($module);
	    LoadTemplate($model, "type_unit/v_type_unit_index", $javascript);
        
    } 
    public function get_one_type_unit() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_type_unit', 'Type Unit', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_type_unit->GetOneType_unit($model["id_type_unit"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Type Unit tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatatype_unit() {
        header('Content-Type: application/json');
        echo $this->m_type_unit->GetDatatype_unit();
    }
    
    public function create_type_unit() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
	    $module = "K018";
        $header = "K001";
        $row['title'] = "Add Type Unit";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $this->load->model("kategori/m_kategori");
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['id_type_unit_tmp'] = rand();
        LoadTemplate($row,'type_unit/v_type_unit_manipulate', $javascript);
    }
    
    public function type_unit_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_type_unit', 'Kode Type Unit', 'trim|required');
	$this->form_validation->set_rules('nama_type_unit', 'Nama Type Unit', 'trim|required');
        $this->form_validation->set_rules('id_kategori', 'Kategori', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_type_unit->Type_unitManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_type_unit($id=0) 
    {
        $row = $this->m_type_unit->GetOneType_unit($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
            $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
	        $module = "K018";
            $header = "K001";
            $row['title'] = "Edit Type Unit";
            CekModule($module);
            $this->load->model("kategori/m_kategori");
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['id_type_unit_tmp'] = $id;
            LoadTemplate($row,'type_unit/v_type_unit_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Type_unit cannot be found in database");
            redirect(site_url('type_unit'));
        }
    }
    
    public function type_unit_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_type_unit', 'Type_unit', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_type_unit->Type_unitDelete($model['id_type_unit']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Type_unit.php */