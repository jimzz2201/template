<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_buka_jual extends CI_Model {

    public $table = '#_buka_jual';
    public $id = 'id_buka_jual';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function UpdateStatusBukaJual($id_buka_jual) {
        $bukajual = $this->GetOneBukaJual($id_buka_jual, "#_buka_jual.id_buka_jual", true);

        if ($bukajual) {
            if (CheckEmpty($bukajual->tanggal_pelunasan)) {
                if ($bukajual->sisa <= 0) {

                    if (count($bukajual->listpembayaran) > 0) {
                        $this->db->update("#_buka_jual", array("tanggal_pelunasan" => $bukajual->listpembayaran[count($bukajual->listpembayaran) - 1]->tanggal_transaksi), array("id_buka_jual" => $id_buka_jual));
                    }
                }
            } else if (!CheckEmpty($bukajual->tanggal_pelunasan)) {
                if ($bukajual->sisa > 0) {
                    $this->db->update("#_buka_jual", array("tanggal_pelunasan" => null), array("id_buka_jual" => $id_buka_jual));
                }
            }
        }
    }

    // datatables
    function GetDataBukaJual($params) {
        $this->load->library('datatables');
        $this->datatables->select('#_buka_jual.*,#_prospek_detail.harga_jual_unit,#_prospek_detail.jumlah_unit,#_prospek_detail.biaya_bbn,#_buka_jual.id_buka_jual as id,#_prospek.no_prospek,tanggal_prospek,nama_unit,nama_warna,nama_customer');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_prospek', '#_prospek.id_prospek = #_buka_jual.id_prospek', 'left');
        $this->datatables->join('#_prospek_detail', '#_prospek.id_prospek = #_prospek_detail.id_prospek', 'left');
        $this->datatables->join('#_warna', '#_warna.id_warna = #_prospek_detail.id_warna', 'left');
        $this->datatables->join('#_unit', '#_unit.id_unit = #_buka_jual.id_unit', 'left');
        $this->datatables->join('#_customer', '#_customer.id_customer = #_buka_jual.id_customer', 'left');
        $extra = [];
        $where = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $pencariankeyword = "nomor_buka_jual";
            if (@$params['jenis_keyword'] == "no_prospek") {
                $pencariankeyword = "#_prospek.no_prospek";
            } else if (@$params['jenis_keyword'] == "vin_number" || @$params['jenis_keyword'] == "engine_no") {
                if (@$params['jenis_keyword'] == "no_prospek") {
                    $pencariankeyword = "#_prospek.no_prospek";
                } else {
                    $pencariankeyword = @$params['jenis_keyword'];
                }

                $this->datatables->join("#_unit_serial", "#_unit_serial.id_buka_jual=#_buka_jual.id_buka_jual");
                $this->datatables->group_by("#_buka_jual.id_buka_jual");
            }
             if($pencariankeyword=="engine_no"||$pencariankeyword=="vin_number")
            {
                $this->datatables->like($pencariankeyword, strtolower($params['keyword']), 'both'); 
            }
            else
            {
                  $where["lower(".$pencariankeyword.")"] = trim(strtolower($params['keyword']));
            }
        } else {
            $tanggal = "#_buka_jual.tanggal_buka_jual";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_buka_jual.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_buka_jual.updated_date)";
            }
            else if ($params['jenis_pencarian'] == "tanggal_prospek")
            {
                $tanggal="#_prospek.tanggal_prospek";
            }
            $this->db->order_by($tanggal, "desc");


            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }
            
            
            if (!CheckEmpty($params['status'])) {
                if ($params['status'] == "6") {
                    $this->db->group_start();
                    $this->datatables->where("#_buka_jual.status", 0);
                    $this->datatables->or_where("#_buka_jual.status", 7);
                    $this->db->group_end();
                } else {
                    $where['#_buka_jual.status'] = $params['status'];
                }
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_buka_jual.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $strview = '';
        $stredit = '';
        $strapproval = '';
        $strrejected = '';
        $strbatal = '';
        if ($isedit) {
            $straction .= anchor(site_url('prospek/edit_prospek/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalprospek($1);return false;"));
        }
        $strview .= anchor(site_url('buka_jual/view_buka_jual/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $stredit .= anchor(site_url('buka_jual/edit_buka_jual/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
            $strbatal .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalbukajual($1);return false;"));
            $strapproval .= anchor("buka_jual/approval_buka_jual/$1", 'Approval', array('class' => 'btn btn-success btn-xs'));
            $strrejected .= anchor("", 'Rejecte', array('class' => 'btn btn-warning btn-xs', "onclick" => "ubahstatus($1,2);return false;"));
        }

        $this->datatables->add_column('edit', $stredit, 'id');
        $this->datatables->add_column('view', $strview, 'id');
        $this->datatables->add_column('batal', $strbatal, 'id');
        $this->datatables->add_column('rejected', $strrejected, 'id');
        $this->datatables->add_column('approval', $strapproval, 'id');
        // $this->datatables->add_column('action', $straction, 'id');
        return $this->datatables->generate();
    }

    function ApprovalBukaJual($model, $type) {
       
        $message = "";
        $st = false;
        $no_prospek = "";
		 $level = GetLevelJabatan();
		
        try {
            $id_buka_jual = @$model['id_buka_jual'];
            $bukajual = $this->GetOneBukaJual($id_buka_jual);
            if ($bukajual->status == "1" && $type != "Batal") {
                $message = "Data Buka Jual sudah diapprove<br/>";
            } else if ($bukajual->status == "2") {
                $message = "Data Buka Jual sudah direject<br/>";
            } else if ($bukajual->status == "5") {
                $message = "Data Buka Jual sudah dibatalkan<br/>";
            }
            $statusapproval = 0;
           
            if ($message == "") {
                $this->db->trans_rollback();
                $this->db->trans_begin();
               
                $id_prospek_history = 0;
                $level = GetLevelJabatan();
                if (GetGroupId() == 1) {
                    $level = '3';
                }
                if ($type == "Approve") {
                    if ($level == '3') {
                        $statusapproval = 1;
                    }
                    
                } else if ($type == "Reject") {
                    $statusapproval = 2;
                } else if ($type == "Nego") {
                    $statusapproval = 3;
                    $level = '1';
                } else if ($type == "Batal") {
                    $statusapproval = 5;
                    $level = '1';

                    $this->db->from("#_unit_serial");
                    $this->db->where(array("id_buka_jual" => $model['id_buka_jual']));
                    $arrayid = $this->db->get()->result_array();
                    $arrayid = array_column($arrayid, "id_unit_serial");
                    foreach ($arrayid as $idsat) {
                        $arraybatal = [];
                        $arraybatal['id_buka_jual'] = $model['id_buka_jual'];
                        $arraybatal['id_unit_serial'] = $idsat;
                        $this->db->insert("#_buka_jual_batal", $arraybatal);
                    }
                    $this->db->update("#_unit_serial", array("id_buka_jual" => null, "id_prospek_detail" => null), array("id_buka_jual" => $model['id_buka_jual']));
                    if (count($arrayid) > 0) {
                        $this->db->where_in("id_unit_serial", $arrayid);
                        $this->db->update("#_prospek_detail_unit", array("id_unit_serial" => null));
                    }
                    $this->db->update("#_prospek", array("close_status" => null), array("id_prospek" => $bukajual->id_prospek));
                }

                $update = [];
                $update['level'] = $level;
                $update['approved_date'] = GetDateNow();
                $update['approved_by'] = GetUserId();
                $update['status'] = $statusapproval;
                $this->db->update("#_buka_jual", MergeUpdate($update), array("id_buka_jual" => $model['id_buka_jual']));

                $approval = [];
                $approval['id_buka_jual'] = $id_buka_jual;
                $approval['status_approval'] = $type;
                $approval['ket_approve'] = $model['ket_approve'];
                $this->db->insert("#_buka_jual_approval", MergeCreated($approval));
                $this->db->update("#_buku_besar", array("debet" => 0, "kredit" => 0), array("dokumen" => $bukajual->nomor_buka_jual));
                $this->load->model("customer/m_customer");
                $this->m_customer->KoreksiSaldoPiutang($bukajual->id_customer);

				
                if ($this->db->trans_status() === FALSE || $message != '') {
                    $this->db->trans_rollback();
                    $message = "Some Error Occured<br/>" . $message;
                    return array("st" => false, "msg" => $message);
                } else {
                    ClearCookiePrefix("_buka_jual", "updated_date");
                    $this->db->trans_commit();
                    SetMessageSession(true, "Data Buka Jual Sudah di" . $type . " ke dalam database");
                    $arrayreturn["st"] = true;
                    SetPrint($id_buka_jual, $no_prospek, 'Buka Jual');
                    return $arrayreturn;
                }
            } else {
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
    }

    function GetBukaJual_api($params, $limit, $from, $count = false) {
        $this->db->select('#_buka_jual.*,#_buka_jual.id_buka_jual as id,#_prospek.no_prospek,tanggal_prospek,nama_unit,nama_warna,nama_customer');
        // $this->db->from($this->table);
        $this->db->where('#_buka_jual.deleted_date', null);
        //add this line for join
        $this->db->join('#_prospek', '#_prospek.id_prospek = #_buka_jual.id_prospek', 'left');
        $this->db->join('#_prospek_detail', '#_prospek_detail.id_prospek = #_prospek.id_prospek', 'left');
        $this->db->join('#_warna', '#_warna.id_warna = #_prospek_detail.id_warna', 'left');
        $this->db->join('#_unit', '#_unit.id_unit = #_buka_jual.id_unit', 'left');
        $this->db->join('#_customer', '#_customer.id_customer = #_buka_jual.id_customer', 'left');
        $where = [];
        $extra = [];
        $strview = '';
        $strbatal = '';
        $strapproved = '';
        $strrejected = '';
        $stredit = '';
        $strclosed = '';
        if ($params['keyword'] != '') {
            // $where['#_prospek.no_prospek'] = $params['keyword'];
            $this->db->like('#_buka_jual.nomor_buka_jual', $params['keyword']);
            $this->db->or_like('nama_customer', $params['keyword']);
        } else {
            // if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            //     array_push($extra, "#_prospek.tanggal_prospek BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            //     unset($params['start_date'], $params['end_date']);
            // } else {
            //     if (isset($params['start_date']) && empty($params['end_date'])) {
            //         $where["#_prospek.tanggal_prospek"] = DefaultTanggalDatabase($params['start_date']);
            //         unset($params['start_date']);
            //     } else {
            //         $where["#_prospek.tanggal_prospek"] = DefaultTanggalDatabase($params['end_date']);
            //         unset($params['end_date']);
            //     }
            // }


            if (!CheckEmpty($params['status'])) {
                $where['#_buka_jual.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_buka_jual.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $this->db->where($where);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }

        if ($count) {
            return $this->db->get($this->table)->result();
        } else {
            $this->db->limit($limit, $from);
            return $this->db->get($this->table)->result();
        }
    }

    function Rejurnal($id_buka_jual) {
        $message = "";
        $bukajual = $this->GetOneBukaJual($id_buka_jual);
        $bukajual = json_decode(json_encode($bukajual), true);
        $this->load->model("gl_config/m_gl_config");

        $this->load->model("customer/m_customer");
        $customer = $this->m_customer->GetOneCustomer($bukajual['id_customer']);

        $akunglpenjualan = $this->m_gl_config->GetIdGlConfig("penjualan", $bukajual['id_cabang'], array());
        $this->db->from("#_buku_besar");
        $this->db->where(array("id_gl_account" => $akunglpenjualan, "dokumen" => $bukajual['nomor_buka_jual']));
        $akun_penjualan = $this->db->get()->row();

        $userid = GetUserId();
        $totalpenjualanwithppn = $bukajual['grandtotal'];
        $totalpenjualanwithoutppn = $bukajual['total_price'];

        $nominalppn = $bukajual['ppn_nominal'];
        if (CheckEmpty($akun_penjualan)) {
            $akunpenjualaninsert = array();
            $akunpenjualaninsert['id_gl_account'] = $akunglpenjualan;
            $akunpenjualaninsert['tanggal_buku'] = $bukajual['tanggal_buka_jual'];
            $akunpenjualaninsert['keterangan'] = "Penjualan Ke " . @$customer->nama_customer . ' ' . $bukajual['keterangan'];
            $akunpenjualaninsert['dokumen'] = $bukajual['nomor_buka_jual'];
            $akunpenjualaninsert['subject_name'] = @$customer->kode_customer;
            $akunpenjualaninsert['id_subject'] = $bukajual['id_customer'];
            $akunpenjualaninsert['debet'] = 0;
            $akunpenjualaninsert['kredit'] = $totalpenjualanwithoutppn;
            $akunpenjualaninsert['created_date'] = GetDateNow();
            $akunpenjualaninsert['created_by'] = $userid;
            $this->db->insert("#_buku_besar", $akunpenjualaninsert);
        } else {
            $akunpenjualanupdate['tanggal_buku'] = $bukajual['tanggal_buka_jual'];
            $akunpenjualanupdate['keterangan'] = "Penjualan Ke " . @$customer->nama_customer . ' ' . $bukajual['keterangan'];
            $akunpenjualanupdate['dokumen'] = $bukajual['nomor_buka_jual'];
            $akunpenjualanupdate['subject_name'] = @$customer->kode_customer;
            $akunpenjualanupdate['id_subject'] = $bukajual['id_customer'];
            $akunpenjualanupdate['debet'] = 0;
            $akunpenjualanupdate['kredit'] = $totalpenjualanwithoutppn;
            $akunpenjualanupdate['updated_date'] = GetDateNow();
            $akunpenjualanupdate['updated_by'] = $userid;
            $this->db->update("#_buku_besar", $akunpenjualanupdate, array("id_buku_besar" => $akun_penjualan->id_buku_besar));
        }


        $akunglppn = $this->m_gl_config->GetIdGlConfig("ppnpenjualan", $bukajual['id_cabang'], array());
        $this->db->from("#_buku_besar");
        $this->db->where(array("id_gl_account" => $akunglppn, "dokumen" => $bukajual['nomor_buka_jual']));
        $akun_ppnpenjualan = $this->db->get()->row();

        if (CheckEmpty($akun_ppnpenjualan) && $nominalppn > 0) {
            $akunppninsert = array();
            $akunppninsert['id_gl_account'] = $akunglppn;
            $akunppninsert['tanggal_buku'] = $bukajual['tanggal_buka_jual'];
            $akunppninsert['keterangan'] = "PPN penjualan Ke " . @$customer->nama_customer . ' ' . $bukajual['keterangan'];
            $akunppninsert['dokumen'] = $bukajual['nomor_buka_jual'];
            $akunppninsert['subject_name'] = @$customer->kode_customer;
            $akunppninsert['id_subject'] = $bukajual['id_customer'];
            $akunppninsert['debet'] = 0;
            $akunppninsert['kredit'] = $nominalppn;
            $akunppninsert['created_date'] = GetDateNow();
            $akunppninsert['created_by'] = $userid;
            $this->db->insert("#_buku_besar", $akunppninsert);
        } else if (!CheckEmpty($akun_ppnpenjualan)) {
            $akunppnupdate['tanggal_buku'] = $bukajual['tanggal_buka_jual'];
            $akunppnupdate['keterangan'] = "PPN penjualan Ke " . @$customer->nama_customer . ' ' . $bukajual['keterangan'];
            $akunppnupdate['dokumen'] = $bukajual['nomor_buka_jual'];
            $akunppnupdate['subject_name'] = @$customer->kode_customer;
            $akunppnupdate['id_subject'] = $bukajual['id_customer'];
            $akunppnupdate['debet'] = 0;
            $akunppnupdate['kredit'] = $nominalppn;
            $akunppnupdate['updated_date'] = GetDateNow();
            $akunppnupdate['updated_by'] = $userid;
            $this->db->update("#_buku_besar", $akunppnupdate, array("id_buku_besar" => $akun_ppnpenjualan->id_buku_besar));
        }

        $akunglpiutang = $this->m_gl_config->GetIdGlConfig("piutang", $bukajual['id_cabang'], array());
        $this->db->from("#_buku_besar");
        $this->db->where(array("id_gl_account" => $akunglpiutang, "dokumen" => $bukajual['nomor_buka_jual']));
        $akun_piutang = $this->db->get()->row();

        if (CheckEmpty($akun_piutang)) {
            $akunpiutanginsert = array();
            $akunpiutanginsert['id_gl_account'] = $akunglpiutang;
            $akunpiutanginsert['tanggal_buku'] = $bukajual['tanggal_buka_jual'];
            $akunpiutanginsert['keterangan'] = "Piutang penjualan dari " . @$customer->nama_customer . ' ' . $bukajual['keterangan'];
            $akunpiutanginsert['dokumen'] = $bukajual['nomor_buka_jual'];
            $akunpiutanginsert['subject_name'] = @$customer->kode_customer;
            $akunpiutanginsert['id_subject'] = $bukajual['id_customer'];
            $akunpiutanginsert['debet'] = $totalpenjualanwithppn;
            $akunpiutanginsert['kredit'] = 0;
            $akunpiutanginsert['created_date'] = GetDateNow();
            $akunpiutanginsert['created_by'] = $userid;
            $this->db->insert("#_buku_besar", $akunpiutanginsert);
        } else if (!CheckEmpty($akun_piutang)) {
            $akunpiutangupdate['tanggal_buku'] = $bukajual['tanggal_buka_jual'];
            $akunpiutangupdate['keterangan'] = "Piutang penjualan Ke " . @$customer->nama_customer . ' ' . $bukajual['keterangan'];
            $akunpiutangupdate['dokumen'] = $bukajual['nomor_buka_jual'];
            $akunpiutangupdate['subject_name'] = @$customer->kode_customer;
            $akunpiutangupdate['id_subject'] = $bukajual['id_customer'];
            $akunpiutangupdate['debet'] = $totalpenjualanwithppn;
            $akunpiutangupdate['kredit'] = 0;
            $akunpiutangupdate['updated_date'] = GetDateNow();
            $akunpiutangupdate['updated_by'] = $userid;
            $this->db->update("#_buku_besar", $akunpiutangupdate, array("id_buku_besar" => $akun_piutang->id_buku_besar));
        }

        $this->m_customer->KoreksiSaldoPiutang($bukajual['id_customer']);

        $akunglpenyeimbang = $this->m_gl_config->GetIdGlConfig("penyeimbang", $bukajual['id_cabang'], array());

        $this->db->from("#_buku_besar");
        $this->db->where(array("dokumen" => $bukajual['nomor_buka_jual'], "id_gl_account !=" => $akunglpenyeimbang));
        $this->db->select("sum(debet)-sum(kredit) as selisih");
        $row = $this->db->get()->row();
        $this->db->from("#_buku_besar");
        $this->db->where(array("id_gl_account" => $akunglpenyeimbang, "dokumen" => $prospek['no_prospek']));
        $akun_penyeimbang = $this->db->get()->row();

        if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
            $akun_penyeimbanginsert = array();
            $akun_penyeimbanginsert['id_gl_account'] = $akunglpenyeimbang;
            $akun_penyeimbanginsert['tanggal_buku'] = $bukajual['tanggal_buka_jual'];
            $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk penjualan Ke " . @$customer->nama_customer . ' ' . $bukajual['keterangan'];
            $akun_penyeimbanginsert['dokumen'] = $bukajual['nomor_buka_jual'];
            $akun_penyeimbanginsert['subject_name'] = @$customer->kode_customer;
            $akun_penyeimbanginsert['id_subject'] = $bukajual['id_customer'];
            $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
            $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
            $akun_penyeimbanginsert['created_date'] = GetDateNow();
            $akun_penyeimbanginsert['created_by'] = $userid;
            $this->db->insert("#_buku_besar", $akun_penyeimbanginsert);
        } else if (!CheckEmpty($akun_penyeimbang)) {
            $akun_penyeimbangupdate['tanggal_buku'] = $bukajual['tanggal_buka_jual'];
            $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk Ke " . @$customer->nama_customer . ' ' . $bukajual['keterangan'];
            $akun_penyeimbangupdate['dokumen'] = $bukajual['nomor_buka_jual'];
            $akun_penyeimbangupdate['subject_name'] = @$customer->kode_customer;
            $akun_penyeimbangupdate['id_subject'] = $bukajual['id_customer'];
            $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
            $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
            $akun_penyeimbangupdate['updated_date'] = GetDateNow();
            $akun_penyeimbangupdate['updated_by'] = $userid;
            $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
        }
    }

    function ChangeStatus($id_buka_jual, $status) {
        try {
            $message = "";
            // $buka_jual = $this->GetOneDoProspek($id_buka_jual);
            // $buka_jual = json_decode(json_encode($buka_jual), true);
            // $this->load->model("customer/m_customer");
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $array = array("st" => true, "msg" => "Data DO Prospek Berhasil " . ($status == "1" ? "Diapprove" : ($status == "2" ? "Direject" : "Diclose")));
            $update = array();
            $update['status'] = $status;
            $update['approved_date'] = GetDateNow();
            $update['approved_by'] = GetUserId();
            $update['updated_by'] = GetUserId();
            $update['updated_date'] = GetDateNow();

            $this->db->update("#_buka_jual", $update, array("id_buka_jual" => $id_buka_jual));
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data DO Prospek Sudah di" . (CheckEmpty(@$model['id_buka_jual']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                // SetPrint($id_prospek, $buka_jual['nomor_buka_jual'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    // get all
    function GetOneDoKpu($keyword, $type = 'id_buka_jual', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        if ($isfull) {
            $this->db->select('#_buka_jual.*,nama_supplier,vrf_code,nama_unit,nama_type_unit,nama_kategori,#_vrf.qty as qty_total');
            $this->db->join('#_prospek', '#_prospek.id_prospek = #_buka_jual.id_prospek', 'left');
            $this->db->join('#_vrf', '#_vrf.id_vrf = #_prospek.id_vrf', 'left');
            $this->db->join('#_unit', '#_unit.id_unit = #_prospek.id_unit', 'left');
            $this->db->join('#_supplier', '#_prospek.id_supplier = #_vrf.id_supplier', 'left');
            $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
        }
        $buka_jual = $this->db->get($this->table)->row();
        if ($isfull && $buka_jual) {
            $this->db->from("#_unit_serial");
            $this->db->where(array("id_buka_jual" => $buka_jual->id_buka_jual, "#_unit_serial.status !=" => 2));
            $list_detail = $this->db->get()->result();
            $buka_jual->list_detail = $list_detail;
        }
        return $buka_jual;
    }

    function GetOneBukaJual($keyword, $type = 'id_buka_jual', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        if ($isfull) {
            // $this->db->select('#_buka_jual.*,nama_supplier,vrf_code,nama_unit,nama_type_unit,nama_kategori,#_vrf.qty as qty_total');
            $this->db->select('#_buka_jual.*,#_prospek.top,#_prospek_detail.tahun as vin,no_prospek,nama_unit,nama_type_unit,nama_kategori, nama_user as approved_name, nama_customer, #_customer.keterangan,
                             #_customer.contact_person, #_customer.alamat, no_prospek, #_prospek_detail.jumlah_unit as qty_total, (#_prospek_detail.jumlah_unit - qty_buka_jual) as qty_sisa,
                             tahun, nama_warna');
            $this->db->join('#_prospek', '#_prospek.id_prospek = #_buka_jual.id_prospek', 'left');
            $this->db->join('#_prospek_detail', '#_prospek.id_prospek = #_prospek_detail.id_prospek', 'left');
            $this->db->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna', 'left');
            // $this->db->join('#_vrf', '#_vrf.id_vrf = #_prospek.id_vrf','left');
            $this->db->join('#_unit', '#_unit.id_unit = #_buka_jual.id_unit', 'left');
            // $this->db->join('#_supplier', '#_prospek.id_supplier = #_vrf.id_supplier','left');
            $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
            $this->db->join("#_user", "#_user.id_user=#_buka_jual.approved_by", "left");
            $this->db->join("#_customer", "#_customer.id_customer=#_buka_jual.id_customer", "left");
        }
        $buka_jual = $this->db->get($this->table)->row();

        if ($isfull && $buka_jual) {
            $this->db->select("#_unit_serial.*,#_kpu.nomor_master as nomor_master_detail,#_vrf.vrf_code as vrf_code_detail,#_customer.nama_customer nama_customer_detail,#_unit_serial.engine_no as manufacture_code, nama_unit, nama_kategori, nama_type_unit");
            $this->db->from("#_unit_serial");
            $this->db->join("#_unit", "#_unit.id_unit = #_unit_serial.id_unit", "left");
            $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
            $this->db->join("#_do_kpu_detail", "#_unit_serial.id_do_kpu_detail=#_do_kpu_detail.id_do_kpu_detail", "left");
            $this->db->join("#_kpu_detail", "#_do_kpu_detail.id_kpu_detail=#_kpu_detail.id_kpu_detail", "left");

            $this->db->join("#_kpu", "#_kpu.id_kpu=#_kpu_detail.id_kpu and #_kpu.type='KPU'", "left");
            $this->db->join("#_customer", "#_customer.id_customer=#_kpu.id_customer", "left");
            $this->db->join("#_vrf", "#_vrf.id_vrf=#_kpu_detail.id_vrf", "left");
            $this->db->join("#_warna", "#_warna.id_warna=#_unit_serial.id_warna", "left");


            $this->db->where(array("id_buka_jual" => $buka_jual->id_buka_jual, "#_unit_serial.status !=" => 2));
            $list_detail = $this->db->get()->result();
            $this->db->from("#_buka_jual_approval");

            $this->db->select("#_buka_jual_approval.*,nama_user");

            $this->db->where(array("id_buka_jual" => $buka_jual->id_buka_jual));
            $this->db->join("#_user", "#_user.id_user=#_buka_jual_approval.created_by", "left");
            $buka_jual->history_approval = $this->db->get()->result();

            $this->db->from("#_pembayaran_piutang_detail");
            $this->db->join("#_pembayaran_piutang_master", "#_pembayaran_piutang_master.pembayaran_piutang_id=#_pembayaran_piutang_detail.pembayaran_piutang_id", "left");
            $this->db->where(array("id_buka_jual" => $buka_jual->id_buka_jual, "#_pembayaran_piutang_master.status !=" => 2));
            $this->db->order_by("#_pembayaran_piutang_master.tanggal_transaksi", "asc");
            $listpembayaran = $this->db->get()->result();
            $jumlah = 0;
            foreach ($listpembayaran as $pembayaran) {
                $jumlah += $pembayaran->jumlah_bayar;
            }
            $buka_jual->sisa = $buka_jual->grandtotal - $jumlah;
            $buka_jual->terbayar = $jumlah;
            $buka_jual->listpembayaran = $listpembayaran;

            $buka_jual->list_detail = $list_detail;
        }

        return $buka_jual;
    }

    function KoreksiBukaJual($id_buka_jual) {
        $buka_jual = $this->GetOneBukaJual($id_buka_jual);
        $this->load->model("propek/m_prospek");
        $prospek = $this->m_prospek->GetOneProspek($buka_jual->id_prospek);

        if ($buka_jual && $prospek) {
            
            $harga_off_the_road = $prospek->harga_off_the_road;
            $biaya_bbn = $prospek->biaya_bbn;
            $harga_on_the_road = $harga_off_the_road + $biaya_bbn;
            $hitunganppn = $harga_off_the_road;
            $totalequipment=$prospek->total_equipment;
            $harga_jual_unit = $harga_on_the_road + $totalequipment;
            $ppn = $prospek->ppn;
            $this->db->from("#_unit_serial");
            $this->db->where(array("id_buka_jual" => $buka_jual->id_buka_jual));
            $jumlahrow = $this->db->get()->num_rows();
            if ($jumlahrow > 0) {
                $qty = $jumlahrow;
                $nominalppn = 0;
                $hitunganppn = $harga_off_the_road;
                $harga_jual_unit = $harga_on_the_road + $totalequipment;
                $total_harga_jual = $harga_jual_unit * $jumlahrow;
                $hitunganppn = $hitunganppn * $jumlahrow;
                $ppn = $prospek->ppn;
                $grandtotal = $total_harga_jual;

                if ($prospek->type_ppn == "2") {
                    $nominalppn = $ppn / 100 * $hitunganppn;
                    $grandtotal = $grandtotal + $nominalppn;
                } else if ($prospek->type_ppn == "1") {
                    $nominalppn = $ppn / (100 + $ppn) * $hitunganppn;
                    $total_harga_jual = $total_harga_jual - $nominalppn;
                }
                $buka_jualupdate['total_bbn'] = $biaya_bbn * $jumlahrow;
                $buka_jualupdate['total_price'] = $total_harga_jual;
                $buka_jualupdate['total_equipment'] = $totalequipment;
                $buka_jualupdate['ppn_nominal'] = $nominalppn;
                $buka_jualupdate['grandtotal'] = $grandtotal;
                $buka_jualupdate['qty_buka_jual'] = $jumlahrow;
                $this->db->update("#_buka_jual", $buka_jualupdate, array("id_buka_jual" => $buka_jual->id_buka_jual));
                $this->m_buka_jual->Rejurnal($buka_jual->id_buka_jual);
                $this->UpdateStatusBukaJual($buka_jual->id_buka_jual);
            }

        }
    }

    function BukaJualManipulate($model, $status = 0) {
        
        $jenis_pencarian="created_date";
        try {
            $message = "";

            $this->db->trans_rollback();
            $this->db->trans_begin();
            $buka_jual['id_prospek'] = ForeignKeyFromDb($model['id_prospek']);
            $buka_jual['qty_buka_jual'] = @$model['qty_buka_jual'];
            $buka_jual['keterangan'] = $model['keterangan'];
            $buka_jual['alamat'] = $model['alamat'];
            $buka_jual['atas_nama'] = $model['atas_nama'];
            $buka_jual['id_customer'] = ForeignKeyFromDb($model['id_customer']);
            $buka_jual['tanggal_buka_jual'] = DefaultTanggalDatabase($model['tanggal']);
            $this->load->model("prospek/m_prospek");
            $buka_jual['free_parking'] = DefaultTanggalDatabase($model['free_parking']);
            $buka_jual['id_pool'] = ForeignKeyFromDb($model['id_pool']);
            $buka_jual['level'] = 2;
            $buka_jual['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $userid = GetUserId();
            $buka_jual['id_unit'] = ForeignKeyFromDb(@$model['id_unit']);
            $buka_jual['nomor_po_leasing'] = $model['nomor_po_leasing'];
            $prospek = $this->m_prospek->GetOneProspek($model['id_prospek']);
            $totalEquip = 0;
            $qty = $buka_jual['qty_buka_jual'];
            $nominalppn = 0;
            $hrg_on_the_road = $prospek->harga_on_the_road;
            $hrg_off_the_road = $prospek->harga_off_the_road;
            $hitunganppn = $hrg_off_the_road;


            foreach ($prospek->list_equipment as $equip) {
                if (!CheckEmpty($equip->is_ppn_detail)) {
                    $hitunganppn += $equip->harga;
                }
                $totalEquip += $equip->harga;
            }

            $harga_jual_unit = $hrg_on_the_road + $totalEquip;
            $total_harga_jual = $harga_jual_unit * $qty;
            $hitunganppn = $hitunganppn * $qty;
            $ppn = $prospek->ppn;
            $grandtotal = $total_harga_jual;

            if ($prospek->type_ppn == "2") {
                $nominalppn = $ppn / 100 * $hitunganppn;
                $grandtotal = $grandtotal + $nominalppn;
            } else if ($prospek->type_ppn == "1") {
                $nominalppn = $ppn / (100 + $ppn) * $hitunganppn;
                $total_harga_jual = $total_harga_jual - $nominalppn;
            }
            $buka_jual['total_bbn'] = $prospek->biaya_bbn * $qty;
            $buka_jual['total_price'] = $total_harga_jual;
            $buka_jual['total_equipment'] = $totalEquip;
            $buka_jual['ppn_nominal'] = $nominalppn;
            $buka_jual['grandtotal'] = $grandtotal;


            $this->load->model("customer/m_customer");
            $customer = $this->m_customer->GetOneCustomer($buka_jual['id_customer']);
            $prospek = $this->m_prospek->GetOneProspek($model['id_prospek']);
            if (!$prospek) {
                $message .= "Prospek Tidak ditemukan dalam database";
            } else {
                if (CheckEmpty(@$buka_jual['id_unit'])) {
                    $buka_jual['id_unit'] = $prospek->id_unit;
                }
            }


            if ($status == 7) {
                $buka_jual['status'] = $status;
            }


            $id_prospek = 0;
            $bukajualdb = null;
            $old_id_pool = 0;
            if (CheckEmpty(@$model['id_buka_jual'])) {
                $buka_jual['status'] = $status;
                $buka_jual['created_date'] = GetDateNow();
                $buka_jual['created_by'] = ForeignKeyFromDb(GetUserId());
                $buka_jual['nomor_buka_jual'] = AutoIncrement('#_buka_jual', 'DB/' . date("y") . '/', 'nomor_buka_jual', 5);
                $res = $this->db->insert($this->table, $buka_jual);
                $id_buka_jual = $this->db->insert_id();
            } else {
                $buka_jual['updated_date'] = GetDateNow();
                $buka_jual['updated_by'] = ForeignKeyFromDb(GetUserId());
                $res = $this->db->update($this->table, $buka_jual, array("id_buka_jual" => $model['id_buka_jual']));
                $id_buka_jual = $model['id_buka_jual'];
                $bukajualdb = $this->GetOneBukaJual($id_buka_jual, "id_buka_jual", false);
                $buka_jual['nomor_buka_jual'] = $bukajualdb->nomor_buka_jual;
                $old_id_pool = $buka_jual->id_pool;
                $jenis_pencarian="updated_date";
            }

            foreach (@$model['id_serial_unit'] as $det) {
                if (!CheckEmpty($det)) {
                    $this->db->from("#_unit_serial");
                    $this->db->where(array("id_unit_serial" => $det, "id_buka_jual !=" => null, "id_buka_jual!=" => $id_buka_jual, "status" => 1));
                    $rowserial = $this->db->get()->row();
                    if (!CheckEmpty($rowserial)) {
                        $message .= "1 / beberapa unit sudah keluar";
                        break;
                    }
                    //if($rowserial==$old_id_pool|| CheckEmpty($val))


                    $unitserial['updated_date'] = GetDateNow();
                    $unitserial['updated_by'] = GetUserId();
                    $unitserial['id_buka_jual'] = $id_buka_jual;
                    $unitserial['id_prospek_detail'] = $prospek->id_prospek_detail;
                    $this->db->update("#_unit_serial", $unitserial, array("id_unit_serial" => $det));
                }
                $this->db->from("#_prospek_detail_unit");
                $this->db->where("id_unit_serial", $det);
                $detailunit = $this->db->get()->row();
                $updatearray = [];
                if (!$detailunit) {
                    $this->db->from("#_prospek_detail_unit");
                    $this->db->where("id_unit_serial", null);
                    $this->db->where("id_prospek_detail", $prospek->id_prospek_detail);
                    $detailunit = $this->db->get()->row();
                    if ($detailunit) {
                        $updatearray['id_unit_serial'] = ForeignKeyFromDb($det);
                        $this->db->update("#_prospek_detail_unit", $updatearray, array("id_prospek_detail_unit" => $detailunit->id_prospek_detail_unit));
                    }
                }
            }

            $this->db->from("#_unit_serial");
            $this->db->where(array("id_buka_jual" => $id_buka_jual, "id_do_prospek_detail !=" => null));
            if (count(@$model['id_serial_unit']) > 0) {
                $this->db->where_not_in("id_unit_serial", $model['id_serial_unit']);
            }
            $rowserial = $this->db->get()->row();

            if ($rowserial) {
                $message .= "1 / Beberapa Unit Bus sudah memiliki do customer <br/>";
            } else {
                $this->db->from("#_unit_serial");
                $this->db->where(array("id_buka_jual" => $id_buka_jual));
                if (count(@$model['id_serial_unit']) > 0) {
                    $this->db->where_not_in("id_unit_serial", $model['id_serial_unit']);
                }
                $this->db->set(array("id_buka_jual" => null));
                $this->db->update("#_unit_serial");
            }
            $prospek = $this->m_prospek->GetOneProspek($model['id_prospek']);
            if ($prospek->jumlah_unit - $prospek->qty_buka_jual <= 0) {
                $this->db->update("#_prospek", MergeUpdate(array("close_status" => 1)), array("id_prospek" => $prospek->id_prospek));
            } else if ($prospek->status == 4) {
                $this->db->update("#_prospek", MergeUpdate(array("status" => 1)), array("id_prospek" => $prospek->id_prospek));
            } else {
                $this->db->update("#_prospek", MergeUpdate(array("close_status" => null)), array("id_prospek" => $prospek->id_prospek));
            }

            $this->Rejurnal($id_buka_jual);

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                ClearCookiePrefix("_buka_jual", $jenis_pencarian);
                $this->db->trans_commit();
                SetMessageSession(true, "Data Buka Jual Sudah di" . (CheckEmpty(@$model['id_buka_jual']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_prospek, $buka_jual['nomor_buka_jual'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownBukaJual() {
        // $listBukaJual = GetTableData($this->table, 'id_buka_jual', 'nomor_buka_jual', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));
        $this->db->where(array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));
        $this->db->select('id_buka_jual, nomor_buka_jual, DATE_FORMAT(tanggal_buka_jual, "%d %M %Y") as tanggal');
        $res = $this->db->get('dgmi_buka_jual')->result_array();
        foreach ($res as $data) {
            $listBukaJual[] = array('id' => $data['id_buka_jual'], 'text' => $data['nomor_buka_jual'] . ' | ' . $data['tanggal']);
        }

        return $listBukaJual;
    }

    function KpuDelete($id_prospek) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_prospek' => $id_prospek));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Kpu has been deleted from database");
    }

    function GetDoProspekByVrf($id_vrf) {
        $this->db->select("dgmi_buka_jual.tanggal_do, nama_customer, nomor_master, nama_unit, nama_warna, vin_number, dgmi_unit_serial.vin, manufacture_code, engine_no, no_bpkb, no_polisi, dgmi_buka_jual.alamat, pengemudi");
        $this->db->where('dgmi_vrf.id_vrf', $id_vrf);
        $this->db->where('dgmi_vrf.deleted_date', null);
        $this->db->join('dgmi_kpu', 'dgmi_kpu.id_vrf = dgmi_vrf.id_vrf', 'left');
        $this->db->join('dgmi_do_kpu', 'dgmi_do_kpu.id_kpu = dgmi_kpu.id_kpu', 'left');
        $this->db->join('dgmi_unit_serial', 'dgmi_do_kpu.id_do_kpu = dgmi_unit_serial.id_do_kpu', 'left');
        $this->db->join('dgmi_buka_jual', 'dgmi_unit_serial.id_do_so = dgmi_buka_jual.id_buka_jual');
        $this->db->join('dgmi_unit', 'dgmi_unit.id_unit = dgmi_unit_serial.id_unit');
        $this->db->join('dgmi_prospek', 'dgmi_buka_jual.id_prospek = dgmi_prospek.id_prospek');
        $this->db->join('dgmi_warna', 'dgmi_prospek.id_warna = dgmi_warna.id_warna', 'left');
        $this->db->join('dgmi_customer', 'dgmi_prospek.id_customer = dgmi_customer.id_customer', 'left');
        $grid_do_kpu = $this->db->get('dgmi_vrf')->result_array();
        return $grid_do_kpu;
    }

    function GetDropDownBukaJualFromCustomer($params) {
        $where = array($this->table . '.status' => 1, $this->table . '.deleted_date' => null);
        $where['#_buka_jual.id_prospek'] = $params['id_prospek'];
        // if (!CheckEmpty(@$params['id_kategori']) || @$params['id_kategori'] > 0) {
        //     $where['#_unit.id_kategori'] = $params['id_kategori'];
        // }
        // if (!CheckEmpty(@$params['id_unit']) || @$params['id_unit'] > 0) {
        //     $where['#_kpu_detail.id_unit'] = $params['id_unit'];
        // }
        // if (!CheckEmpty(@$params['id_type_unit']) || @$params['id_type_unit'] > 0) {
        //     $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        // }
        // if (!CheckEmpty(@$params['id_customer']) || @$params['id_customer'] > 0) {
        //     $where['#_kpu.id_customer'] = $params['id_customer'];
        // }
        $arrayjoin = [];
        $select = 'DATE_FORMAT(#_buka_jual.tanggal_buka_jual, "%d %M %Y") as buka_jual_date_convert,nomor_buka_jual,atas_nama,nama_unit,#_buka_jual.id_buka_jual';
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_buka_jual.id_unit");
        $arrayjoin[] = array("table" => "#_prospek", "condition" => "#_prospek.id_prospek=#_buka_jual.id_prospek");
        $listkpu = GetTableData($this->table, 'id_buka_jual', array('nomor_buka_jual', "atas_nama", "nama_unit", "buka_jual_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listkpu;
    }

}
