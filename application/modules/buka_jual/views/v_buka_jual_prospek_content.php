

<div class="form-group ">
    <?= form_label('Keterangan', "txt_jumlah_unit", array("class" => 'col-sm-2 control-label')); ?>
    <div class="col-md-10">
        <textarea type='text' readonly="readonly" class='form-control' id='keterangan'  name='keterangan' ><?= @$prospek['keterangan'] ?></textarea>
    </div>
</div>
<div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Approval History</span></h3></div>


<table class="table table-striped table-bordered table-hover" id="">
    <tr>
        <th style="width:100px">Status</th>
        <th style="width:200px">Pembuat</th>
        <th style="width:200px">Tanggal</th>
        <th>Notes</th>
        <th style="width:200px">Nego Value</th>
        <th style="width:100px">History</th>
    </tr>
    <?php foreach (@$prospek['history_approval'] as $approval) { ?>
        <tr>
            <td><?= $approval['status_approval'] ?></td>
            <td><?= $approval['nama_user'] ?></td>
            <td><?= DefaultTanggal($approval['created_date']) . ' ' . DefaultTimePicker($approval['created_date']) ?></td>
            <td><?= $approval['ket_approve'] ?></td>
            <td><?= DefaultCurrencyAkuntansi($approval['nego_value']) ?></td>
            <td style="text-align: center;">
                <?php if (!CheckEmpty($approval['id_prospek_history'])) { ?>
                    <a target="_blank" href="<?php echo base_url() . 'index.php/prospek/prospek_history/' . $approval['id_prospek_history'] ?>" class="btn btn-info btn-xs"  >View</a>
                <?php } ?>
            </td>
        </tr>

    <?php } ?>
</table>
<div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Pembayaran</span></h3></div>
<div class="col-sm-6">
    <table class="table table-striped table-bordered table-hover" id="">
        <tr>
            <th style="width:100px">Dokumen</th>
			<th style="width:100px">Nomor Mesin</th>
			<th style="width:100px">Nomor Rangka</th>
			 <th style="width:200px">Tanggal Transaksi</th>
            <th style="width:200px">Pembayaran</th>
            <th>Nominal</th>
        </tr>
        <?php
        if (count(@$prospek['listpembayaran']) > 0) {
            foreach (@$prospek['listpembayaran'] as $pembayaran) {
                ?>
                <tr>
                    <td><?= $pembayaran['dokumen'] ?></td>
					<td><?= $pembayaran['engine_no'] ?></td>
					<td><?= $pembayaran['vin_number'] ?></td>
                    <td><?= DefaultDatePicker($pembayaran['tanggal_transaksi']) ?></td>
                    <td><?= $pembayaran['jenis_pembayaran'] ?></td>
                    <td><?= DefaultCurrency($pembayaran['jumlah_bayar']) ?></td>

                </tr>

                <?php
            }
        } else {
            echo "<tr><td style='text-align:center' colspan=5>Tidak Ada data tersedia</td>";
        }
        ?>
    </table>
</div>
<div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">AR</span></h3></div>
<div class="col-sm-6">
    <table class="table table-striped table-bordered table-hover" id="">
        <tr>
            <th style="width:100px">Dokumen</th>
            <th style="width:200px">Tanggal Transaksi</th>
            <th style="width:200px">Pembayaran</th>
            <th>Nominal</th>
        </tr>
        <?php
        if (count(@$prospek['listar']) > 0) {
            foreach (@$prospek['listar'] as $pembayaran) {
                ?>
                <tr>
                    <td><?= $pembayaran['no_prospek'] ?></td>
                    <td><?= DefaultDatePicker($pembayaran['tanggal_prospek']) ?></td>
                    <td><?= DefaultCurrency($pembayaran['grandtotal']) ?></td>
                    <td><?= DefaultCurrency($pembayaran['terbayar']) ?></td>
                    <td><?= DefaultCurrency($pembayaran['grandtotal'] - $pembayaran['terbayar']) ?></td>
                </tr>

                <?php
            }
        } else {
            echo "<tr><td  style='text-align:center' colspan=5>Tidak Ada data tersedia</td>";
        }
        ?>
    </table>
</div>

<div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Keterangan Unit dan Estimasi Harga</span></h3></div>

<div class="areainput">
    <div class="form-group col-md-12">
        <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-2">
            <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "", "PPN"), @$prospek['type_ppn'], array('class' => 'form-control', 'id' => 'dd_type_ppn')); ?>
        </div>
        <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-1">
            <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$prospek['ppn']), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
        </div>

    </div>
    <div class="form-group col-md-12">
        <?= form_label('Jumlah Unit', "txt_jumlah_unit", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text', 'name' => 'jumlah_unit', 'value' => @$prospek['jumlah_unit'], 'class' => 'form-control', 'id' => 'txt_jumlah_unit', 'placeholder' => 'Jumlah Unit')); ?>
        </div>

    </div>
    <div class="form-group col-md-12">
        <?= form_label('Unit', "txt_id_kategori", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-4">
            <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit_bus', 'class' => 'form-control', 'id' => 'dd_id_unit_bus', 'placeholder' => 'Kategori'), [], @$prospek['id_unit']); ?>
        </div>
        <?= form_label('Tahun', "txt_tahun", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-5">
            <?= form_input(array('type' => 'text', 'name' => 'tahun', 'value' => @$prospek['tahun'], 'class' => 'form-control', 'id' => 'txt_tahun', 'placeholder' => 'Tahun')); ?>
        </div>
    </div>
    <div class="form-group col-md-12">
        <?= form_label('Kategori', "txt_id_kategori", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori'), @$prospek['nama_kategori']); ?>
        </div>
        <?= form_label('Type Unit', "txt_id_type_unit", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-5">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Type Unit'), @$prospek['nama_type_unit']); ?>
        </div>
    </div>
    <div class="form-group col-md-12">

        <?= form_label('Type Body', "txt_id_type_body", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_type_body', 'placeholder' => 'Type Body'), @$prospek['nama_type_body']); ?>
        </div>
    </div>
    <div class="form-group col-md-12">
        <?= form_label('Jenis Plat', "txt_id_jenis_plat", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-4">
            <?= form_dropdown(array('type' => 'text', 'name' => 'id_jenis_plat', 'class' => 'form-control', 'id' => 'dd_jenis_plat', 'placeholder' => 'Jenis Plat'), [], @$prospek['id_jenis_plat']); ?>
        </div>
        <?= form_label('Warna', "txt_id_warna", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-5">
            <?= form_dropdown(array('type' => 'text', 'name' => 'id_warna', 'class' => 'form-control', 'id' => 'dd_warna', 'placeholder' => 'Warna'), [], @$prospek['id_warna']); ?>
        </div>
    </div>


    <?php
    $price = array();
    $option_unit = '';
    $tempunit = [];
    foreach ($list_unit as $key => $val) {
        $sel = ($val['id'] == @$list_unit) ? ' selected' : '';
        $dt = explode('-', $val['text']);
        array_push($price, array('id' => $val['id'], 'price' => trim($dt[1])));
        $tempunit[] = array("id" => $val['id'], "text" => trim($dt[0]));
        $option_unit .= '<option value="' . $val['id'] . '" ' . $sel . '>' . $dt[0] . '</option>';
    }
    ?>

    <div class="form-group col-md-12">
        <?= form_label('Harga Off The Road', "txt_harga_off_the_road", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <div class='input-group'>
                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                <?= form_input(array('type' => 'text', 'name' => 'harga_off_the_road', 'value' => DefaultCurrency(@$prospek['harga_off_the_road']), 'class' => 'form-control calculate input-currency', 'id' => 'txt_harga_off_the_road', 'placeholder' => 'Harga Off The Road', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>
        </div>
    </div>
    <div class="form-group col-md-12">
        <?= form_label('Biaya Bbn', "txt_biaya_bbn", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <div class='input-group'>
                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                <?= form_input(array('type' => 'text', 'name' => 'biaya_bbn', 'value' => DefaultCurrency(@$prospek['biaya_bbn']), 'class' => 'form-control calculate input-currency', 'id' => 'txt_biaya_bbn', 'placeholder' => 'Biaya Bbn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>
        </div>
    </div>
    <div class="form-group col-md-12">
        <?= form_label('Harga On The Road', "txt_harga_on_the_road", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <div class='input-group'>
                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'harga_on_the_road', 'value' => DefaultCurrency(@$prospek['harga_on_the_road']), 'class' => 'form-control calculate input-currency', 'id' => 'txt_harga_on_the_road', 'placeholder' => 'Harga On The Road', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>
        </div>
    </div>

    <div id="equipments">
        <?php
        $cnt = 0;

        $opt_unit = '';
        $is_view=1;
        foreach (@$prospek['list_equipment'] as $cnt => $dataEq) {
              include APPPATH . 'modules/prospek/views/v_prospek_detail_attribute.php'; 
        }
        ?>
    </div>
    <div class="form-group col-md-12">
        <?= form_label('Harga Jual Unit', "txt_harga_jual_unit", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <div class='input-group'>
                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'harga_jual_unit', 'value' => DefaultCurrency(@$prospek['harga_jual_unit']), 'class' => 'form-control input-currency', 'id' => 'txt_harga_jual_unit', 'placeholder' => 'Harga Jual Unit', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>
        </div>
    </div>
    <div class="form-group col-md-12">
        <?= form_label('Total Harga Jual', "txt_total_harga_jual", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <div class='input-group'>
                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => DefaultCurrency(@$prospek['grandtotal']-@$prospek['nominal_ppn']), 'class' => 'form-control input-currency', 'id' => 'total_harga_jual', 'placeholder' => 'Total Harga Jual', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>
        </div>
    </div>
    <div class="form-group col-md-12">

        <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <div class='input-group'>
                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                <?= form_input(array('type' => 'text', 'name' => 'nominal_ppn', 'value' => DefaultCurrency(@$prospek['nominal_ppn']), 'class' => 'form-control  input-currency', 'id' => 'txt_nominal_ppn', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
            </div>
        </div>


    </div>
    <div class="form-group col-md-12">

        <?= form_label('Grandtotal', "txt_kepada", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-5">
            <div class='input-group'>
                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$prospek['grandtotal']), 'class' => 'form-control  input-currency', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
            </div>
        </div>


    </div>

    <div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Cara Pembayaran</span></h3></div>
    <div class="form-group col-md-12">
        <?= form_label('Payment Method', "txt_id_payment_method", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-5">
            <?= form_dropdown(array('type' => 'text', 'name' => 'id_payment_method', 'class' => 'form-control', 'id' => 'dd_payment_method', 'placeholder' => 'Payment Method'), [], @$prospek['id_payment_method']); ?>
        </div>
    </div>
    <div class="form-group col-md-12 groupleasing" <?php if (@$prospek['id_payment_method'] != "2") echo "style='display:none'"; ?>>
        <?= form_label('Leasing', "txt_contact_person", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-5">
            <?= form_dropdown(array('type' => 'text', 'name' => 'id_leasing', 'class' => 'form-control', 'id' => 'dd_id_leasing', 'placeholder' => 'Leasing'), [], @$prospek['id_leasing']); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#dd_type_ppn").select2({data: list_type_ppn}).val("<?php echo @$prospek['type_ppn'] ?>").trigger("change");
        $("#dd_payment_method").select2({data: list_payment_method}).val(<?php echo @$prospek['id_payment_method'] ?>).trigger("change");
        $("#dd_id_leasing").select2({data: list_leasing}).val(<?php echo CheckEmpty(@$prospek['id_leasing']) ? 0 : @$prospek['id_leasing'] ?>).trigger("change");
        $("#dd_warna").select2({data: list_warna}).val(<?php echo @$prospek['id_warna'] ?>).trigger("change");
        $("#dd_jenis_plat").select2({data: list_jenis_plat}).val(<?php echo @$prospek['id_jenis_plat'] ?>).trigger("change");
        $("#dd_id_unit_bus").select2({data: list_unit_bus}).val(<?php echo @$prospek['id_unit'] ?>).trigger("change");
        $(".areainput input").attr("readonly", "readonly");
        $(".areainput textarea").attr("disabled", "disabled");
        $(".areainput select").select2({disabled: "readonly"});
    })
</script>