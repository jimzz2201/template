<section class="content-header">
    <h1>
        Buka Jual <?= @$button ?>  <span style="color:red;font-weight:bold;"><?php echo @$nomor_buka_jual?></span>
        <small>Buka Jual</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Buka Jual </li>
        <li class="active">Buka Jual <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_prospek" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" id="txt_buka_jual" name="id_buka_jual" value="<?php echo @$id_buka_jual; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Customer', "txt_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        if (!CheckEmpty(@$id_customer)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_customer, 'name' => 'id_customer'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Customer'));
                                        } else {
                                            echo form_dropdown(array("name" => "id_customer"), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer, array('class' => 'form-control select2', 'id' => 'dd_id_customer'));
                                        }
                                        ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Free Parking', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'free_parking', 'value' => DefaultDatePicker(@$free_parking), 'class' => 'form-control datepicker', 'id' => 'txt_free_parking', 'placeholder' => 'Free Parking')); ?>
                                    </div>
                                    <?= form_label('Unit Position', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_pool"), DefaultEmptyDropdown(@$list_pool, "json", "Unit Position"), @$id_pool, array('class' => 'form-control', 'id' => 'dd_id_unit_position')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <div class="form-group">
                                    <?= form_label('Atas Nama', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'atas_nama', 'value' => @$atas_nama, 'class' => 'form-control ', 'id' => 'txt_atas_nama', 'placeholder' => 'Atas Nama')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat, 'name' => 'alamat', 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>

                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Nomor PO leasing', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'nomor_po_leasing', 'value' => @$nomor_po_leasing, 'class' => 'form-control ', 'id' => 'txt_nomor_po_leasing', 'placeholder' => 'Nomor PO Leasing')); ?>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Prospek', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        if (!CheckEmpty(@$id_prospek)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_prospek, 'name' => 'id_prospek'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$no_prospek, 'class' => 'form-control', 'id' => 'txt_prospek_code', 'placeholder' => 'Prospek'));
                                        } else {
                                            echo form_dropdown(array('type' => 'text', 'name' => 'id_prospek', 'class' => 'form-control select2', 'id' => 'dd_id_prospek', 'placeholder' => 'Prospek'), DefaultEmptyDropdown(@$list_prospek, "json", "Prospek"), @$id_prospek);
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>
                                    <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'top', 'value' => @$top, 'class' => 'form-control', 'id' => 'txt_top', 'placeholder' => 'Top')); ?>
                                    </div>
                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Warna')); ?>


                                    </div>
                                </div>
                                <div id="groupprospek">

                                </div>
                                <div class="form-group">
                                    <?= form_label('Qty Total', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => DefaultCurrency(@$qty_total), 'class' => 'form-control', 'id' => 'txt_qty_total', 'placeholder' => 'Qty Total', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Qty Sisa', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => DefaultCurrency(@$qty_sisa), 'class' => 'form-control', 'id' => 'txt_qty_sisa', 'placeholder' => 'Qty Sisa', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                </div>

                                <hr/>

                                <div id="areado" style="position:relative">

                                    <?php
                                    foreach (@$list_detail as $key => $detail) {
                                        include APPPATH . 'modules/buka_jual/views/v_buka_jual_content.php';
                                    }
                                    ?>

                                </div>
                                <hr/>

                                <div class="form-group" style="margin-top:50px">
                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-primary btn-block" id="btt_modal_save" >Save Only</button>
                                    </div>
                                    <?php if (@$status == "0" || @$status == "3") { ?>
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-warning btn-block" id="btt_modal_save_and_approve" onclick="Save(7)" >Save & Ask Approval</button>
                                        </div>  
                                    <?php } ?>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;
    var jumlahgenerate =<?= count(@$list_detail) ?>;
    var id_kpu = 0;
    var id_vrf = 0;
    var list_type_ppn = <?php echo json_encode(DefaultEmptyDropdown(@$list_type_ppn, "", "PPN")) ?>;
    var list_unit_bus = <?php echo json_encode(DefaultEmptyDropdown($list_unit_bus, "json", "Unit")) ?>;
    var list_warna = <?php echo json_encode(DefaultEmptyDropdown($list_warna, "json", "Warna")) ?>;
    var list_jenis_plat = <?php echo json_encode(DefaultEmptyDropdown($list_jenis_plat, "json", "Jenis Plat")) ?>;
    var list_payment_method = <?php echo json_encode(DefaultEmptyDropdown($list_payment_method, "json", "Payment Method")) ?>;
    var list_leasing = <?php echo json_encode(DefaultEmptyDropdown($list_leasing, "json", "Leasing")) ?>;
    function GenerateHtml(jumlahgenerate)
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/buka_jual/prospek_content',
            data: {
                key: jumlahgenerate
            },
            success: function (data) {
                $("#areado").append(data);
                jumlahgenerate += 1;
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);

            }
        });
    }


    function ChangeSerial(key)
    {
        var id_unit_serial = $("#dd_id_serial_unit" + key).val();

        if (id_unit_serial != 0)
        {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/unit/get_one_serial',
                dataType: 'json',
                data: {
                    id_unit_serial: id_unit_serial
                },
                success: function (data) {
                    if (data.st)
                    {
                        $("#txt_nama_unit" + key).val(data.obj.nama_unit);
                        $("#txt_nama_type_unit" + key).val(data.obj.nama_type_unit);
                        $("#txt_nama_kategori" + key).val(data.obj.nama_kategori);
                        $("#txt_top" + key).val(data.obj.top);
                        $("#txt_vin" + key).val(data.obj.tahun);
                        $("#groupprospek" + key).html(data.html);
                        $("#txt_warna" + key).val(data.obj.nama_warna);
                        $("#txt_vin_number" + key).val(data.obj.vin_number);
                        $("#txt_vin" + key).val(data.obj.vin);
                        $("#txt_engine_no" + key).val(data.obj.engine_no);
                        $("#txt_nama_customer_detail" + key).val(data.obj.nama_customer);
                        $("#txt_vrf_code_detail" + key).val(data.obj.vrf_code);
                        $("#txt_nomor_master_detail" + key).val(data.obj.nomor_master);


                    } else
                    {
                        $("#txt_nama_unit" + key).val("");
                        $("#txt_nama_type_unit" + key).val("");
                        $("#txt_nama_kategori" + key).val("");
                        $("#txt_top" + key).val("");
                        $("#txt_vin" + key).val("");
                        $("#txt_warna" + key).val("");
                        $("#txt_engine_no" + key).val("");
                        $("#txt_vin_number" + key).val("");
                        $("#txt_nama_customer_detail" + key).val("");
                        $("#txt_vrf_code_detail" + key).val("");
                        $("#txt_nomor_master_detail" + key).val("");
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    $("#txt_nama_unit" + key).val("");
                    $("#txt_nama_type_unit" + key).val("");
                    $("#txt_nama_kategori" + key).val("");
                    $("#txt_warna" + key).val("");
                    $("#txt_engine_no" + key).val("");
                    $("#txt_manufacture_code" + key).val("");
                    $("#txt_no_bpkb" + key).val("");
                    $("#txt_no_polisi" + key).val("");
                    LoadBar.hide();
                }
            });
        } else
        {
            $("#txt_nama_unit").val("");
            $("#txt_nama_type_unit").val("");
            $("#txt_nama_kategori").val("");
            $("#txt_nama_warna").val("");
        }
    }
    function GetOneProspek(id_prospek, jumlah_buka_jual, is_generate)
    {
        if (id_prospek != 0)
        {
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/prospek/get_one_prospek',
                dataType: 'json',
                data: {
                    id_prospek: id_prospek,
                    typehtml: 'propekcontent'
                },
                success: function (data) {
                    if (data.st)
                    {
                        $("#txt_nama_unit").val(data.obj.nama_unit);
                        $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                        $("#txt_nama_kategori").val(data.obj.nama_kategori);
                        $("#txt_top").val(data.obj.top);
                        $("#txt_vin").val(data.obj.tahun);
                        $("#groupprospek").html(data.html);
                        $("#txt_qty_total").val(Comma(data.obj.jumlah_unit));

                        $("#txt_qty_sisa").val(Comma(data.obj.jumlah_unit - data.obj.qty_buka_jual + jumlah_buka_jual));
                        if (jumlah_buka_jual == 0 && is_generate == true)
                        {
                            GenerateHtml(data.obj.jumlah_unit - data.obj.qty_buka_jual + jumlah_buka_jual);
                        }

                        $("#txt_nama_warna").val(data.obj.nama_warna);
                        if (is_generate)
                        {
                            $("#areado").empty();
                        }

                    } else
                    {
                        $("#txt_nama_unit").val("");
                        $("#txt_nama_type_unit").val("");
                        $("#txt_nama_kategori").val("");
                        $("#txt_top").val("");
                        $("#txt_vin").val("");
                        $("#txt_nama_warna").val("");
                        $("#areado").empty();
                    }
                    if (is_generate)
                    {
                        UpdateUnitSerial();
                    }

                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    $("#txt_nama_unit").val("");
                    $("#txt_nama_type_unit").val("");
                    $("#txt_nama_kategori").val("");
                    $("#txt_nama_warna").val("");
                    LoadBar.hide();
                }
            });
        } else
        {
            $("#txt_nama_unit").val("");
            $("#txt_nama_type_unit").val("");
            $("#txt_nama_kategori").val("");
            $("#txt_nama_warna").val("");
            LoadBar.hide();
        }
    }


    function UpdateProspek()
    {
        LoadBar.show();
        $("#dd_id_prospek").change(function () {
            var id_prospek = $("#dd_id_prospek").val();
            GetOneProspek(id_prospek, 0, true);

        })
    }

    $(document).ready(function () {
        $("select").select2();
<?php
if (!CheckEmpty(@$id_prospek)) {
    echo "GetOneProspek(" . @$id_prospek . "," . @$qty_buka_jual . ",false);";
}
?>
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });

        $("#dd_id_customer").change(function () {
            var id_customer = $(this).val();
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/prospek/get_prospek_list',
                dataType: 'json',
                data: {
                    id_customer: id_customer
                },
                success: function (data) {
                    $("#dd_id_prospek").empty();
                    $("#txt_atas_nama").val(data.obj.nama_customer);
                    $("#dd_id_customer_search").val(data.obj.id_customer).trigger("change");
                    $("#txt_alamat").val(data.obj.alamat);
                    if (data.st)
                    {
                        $("#dd_id_prospek").select2({data: data.list_prospek});
                        UpdateProspek();
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        })


    })

    var list_unit_serial = [];
    function UpdateUnitSerial()
    {
        if ($("#dd_id_prospek").val() != "0")
        {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/unit/get_unit_serial',
                dataType: 'json',
                data: {
                    id_prospek: $("#dd_id_prospek").val(),
                    id_buka_jual:$("#txt_buka_jual").val()
                },
                success: function (data) {
                    if (data.st)
                    {
                        list_unit_serial = data.list_unit_serial;
                        $(".unitserial").empty();
                        $(".unitserial").select2({data: list_unit_serial});
                    } else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        } else
        {
            $("#dd_id_unit_serial").empty();
        }

    }

    $(document).ready(function () {
        $(".datepicker").datepicker();
    })
    function Save(status) {
        swal({
            title: "Apakah kamu yakin ingin menginput data prospek berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/buka_jual/buka_jual_manipulate',
                    dataType: 'json',
                    data: $("#frm_prospek").serialize() + '&' + $.param({'status': status}),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/buka_jual";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;
    }

    $("#frm_prospek").submit(function () {
        Save(0);

        return false;


    })

</script>