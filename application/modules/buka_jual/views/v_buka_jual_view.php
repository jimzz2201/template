<section class="content-header">
    <h1>
        Buka Jual <?= @$button ?>
        <small>Buka Jual View</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Buka Jual View</li>
        <li class="active"></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_prospek" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <div class="areainput">
                                    <input type="hidden" name="id_buka_jual" value="<?php echo @$id_buka_jual; ?>" />
                                    <h1 style="font-weight: bold;"><?php
                                        echo "<span>No Buka Jual : </span><span style=color:red;>" . @$nomor_buka_jual . "</span>";
                                        ?></h1>
                                    <hr/>
                                    <div class="form-group">
                                        <?= form_label('Customer', "txt_customer", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?php
                                            if (!CheckEmpty(@$id_customer)) {
                                                echo form_input(array('type' => 'hidden', 'value' => @$id_customer, 'name' => 'id_customer'));
                                                echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Customer'));
                                            } else {
                                                echo form_dropdown(array("name" => "id_customer"), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer, array('class' => 'form-control select2', 'id' => 'dd_id_customer'));
                                            }
                                            ?>
                                        </div>    

                                        <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal_buka_jual), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Free Parking', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'free_parking', 'value' => DefaultDatePicker(@$free_parking), 'class' => 'form-control datepicker', 'id' => 'txt_free_parking', 'placeholder' => 'Free Parking')); ?>
                                        </div>
                                        <?= form_label('Unit Position', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'unit_position', 'value' => @$unit_position, 'class' => 'form-control ', 'id' => 'txt_unit_position', 'placeholder' => 'Unit Position')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-10">
                                            <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                        </div>

                                    </div>  
                                    <div class="form-group">
                                        <?= form_label('Atas Nama', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'atas_nama', 'value' => @$atas_nama, 'class' => 'form-control ', 'id' => 'txt_atas_nama', 'placeholder' => 'Atas Nama')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Alamat', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-10">
                                            <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat, 'name' => 'alamat', 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                        </div>

                                    </div>
                                    <hr/>

                                </div>
                                <div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Buka Jual History</span></h3></div>
                                <table class="table table-striped table-bordered table-hover" id="">
                                    <tr>
                                        <th style="width:100px">Status</th>
                                        <th style="width:200px">Pembuat</th>
                                        <th style="width:200px">Tanggal</th>
                                        <th>Notes</th>
                                        
                                    </tr>
                                    <?php 
                                    if(count($history_approval)>0) { 
                                    foreach ($history_approval as $approval) { ?>
                                        <tr>
                                            <td><?= $approval['status_approval'] ?></td>
                                            <td><?= $approval['nama_user'] ?></td>
                                            <td><?= DefaultTanggal($approval['created_date']) . ' ' . DefaultTimePicker($approval['created_date']) ?></td>
                                            <td><?= $approval['ket_approve'] ?></td>
                                             
                                        </tr>

                                    <?php }  } else {
                                        echo "<tr><td colspan=4>Tidak Ada Data History</td></tr>";
                                    }?>
                                </table>
                                
                                <?php echo @$approvalhtml ?>
                                <?php echo @$propsekhtml ?>


                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$tahun, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>
                                    <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'top', 'value' => @$top, 'class' => 'form-control', 'id' => 'txt_top', 'placeholder' => 'Top')); ?>
                                    </div>
                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Warna')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Qty Total', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => DefaultCurrency(@$qty_total), 'class' => 'form-control', 'id' => 'txt_qty_total', 'placeholder' => 'Qty Total', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Qty Sisa', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => DefaultCurrency(@$qty_sisa), 'class' => 'form-control', 'id' => 'txt_qty_sisa', 'placeholder' => 'Qty Sisa', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                </div>

                                <div id="groupprospek">
                                    <?php
                                    echo @$pembayaran;
                                    ?>
                                </div>
                                <div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Alokasi Unit</span></h3></div>

                                <div id="areado" class="areainput" style="position:relative">
                                    <?php
                                    foreach (@$list_detail as $key => $detail) {
                                        ?>
                                        <div style="position:relative">
                                            <h3 style="font-weight: bold;">Unit ke <?= $key + 1 ?></h3>
                                            <hr/>
                                            <div class="form-group">
                                                <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-6">
                                                    <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit' . $key, 'placeholder' => 'Unit')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <?= form_label('Warna', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-2">
                                                    <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'warna[]', 'value' => @$detail['warna'], 'class' => 'form-control', 'id' => 'txt_warna' . $key, 'placeholder' => 'Warna')); ?>
                                                </div>
                                                <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                                <div class="col-sm-3">
                                                    <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit' . $key, 'placeholder' => 'Tipe Unit')); ?>
                                                </div>
                                                <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                                <div class="col-sm-3">
                                                    <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori' . $key, 'placeholder' => 'Kategori Unit')); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
											
												<?= form_label('Vin', "txt_dnp", array("class" => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-2">
                                                    <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin[]', 'value' => @$detail['vin'], 'class' => 'form-control', 'id' => 'txt_vin' . $key, 'placeholder' => 'Vin')); ?>
                                                </div>
                                                
                                                <?= form_label('VIN Number', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
                                                <div class="col-sm-3">
                                                    <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin_number[]', 'value' => @$detail['vin_number'], 'class' => 'form-control', 'id' => 'txt_vin_number' . $key, 'placeholder' => 'VIN Number')); ?>
                                                </div>
												
												<?= form_label('Engine No', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
                                                <div class="col-sm-3">
                                                    <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'engine_no[]', 'value' => @$detail['engine_no'], 'class' => 'form-control', 'id' => 'txt_engine_no' . $key, 'placeholder' => 'Engine NO')); ?>
                                                </div>
                                                

                                            </div>
                                            <div class="form-group">
                                                
                                                <?= form_label('NO BPKB', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                                <div class="col-sm-2">
                                                    <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'no_bpkb[]', 'value' => @$detail['no_bpkb'], 'class' => 'form-control', 'id' => 'txt_no_bpkb' . $key, 'placeholder' => 'NO BPKB')); ?>
                                                </div>
                                                <?= form_label('No Polisi', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
                                                <div class="col-sm-3">
                                                    <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'no_polisi[]', 'value' => @$detail['no_polisi'], 'class' => 'form-control', 'id' => 'txt_no_polisi' . $key, 'placeholder' => 'No Polisi')); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                    ?>

                                </div>
                                <hr/>

                                <div class="form-group" style="margin-top:50px">
                                    <a href="<?php echo base_url() . 'index.php/buka_jual' ?>" class="btn btn-default"  >Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    var list_type_ppn  = <?php echo json_encode(DefaultEmptyDropdown(@$list_type_ppn, "", "PPN"))?>; 
    var list_unit_bus  = <?php echo json_encode(DefaultEmptyDropdown($list_unit_bus, "json", "Unit"))?>; 
    var list_warna  = <?php echo json_encode(DefaultEmptyDropdown($list_warna, "json", "Warna"))?>;
    var list_jenis_plat  = <?php echo json_encode(DefaultEmptyDropdown($list_jenis_plat, "json", "Jenis Plat"))?>;
    var list_payment_method = <?php echo json_encode(DefaultEmptyDropdown($list_payment_method, "json", "Payment Method"))?>;
    var list_leasing = <?php echo json_encode(DefaultEmptyDropdown($list_leasing, "json", "Leasing"))?>;
     $(document).ready(function () {
        
        calcEquip();
    })
    function calcEquip() {
        let totalEquip = 0;
        let qty = $('#txt_jumlah_unit').val();
        let hrg_on_the_road = $("#txt_harga_on_the_road").val().replace(/\,/g, '');
        $('.equip').each(function () {
            totalEquip += parseInt($(this).val().replace(/\,/g, ''));
            $(this).val(Comma($(this).val()));
        });
        let harga_jual_unit = parseInt(hrg_on_the_road) + totalEquip;
        let total_harga_jual = harga_jual_unit * qty;
        $('#txt_harga_jual_unit').val(Comma(harga_jual_unit));



        var nominalppn = 0;
        var ppn = Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));


        var grandtotal = total_harga_jual;


        if ($("#dd_type_ppn").val() == "2")
        {
            nominalppn = ppn / 100 * grandtotal;
            grandtotal = grandtotal + nominalppn;
        } else if ($("#dd_type_ppn").val() == "1")
        {
            nominalppn = ppn / (100 + Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""))) * grandtotal;
            total_harga_jual = total_harga_jual - nominalppn;
        }
        $('#total_harga_jual').val(Comma(total_harga_jual));
        $("#txt_grandtotal").val(number_format(grandtotal.toFixed(4)));
        $("#txt_nominal_ppn").val(number_format(nominalppn.toFixed(4)));



    }

</script>