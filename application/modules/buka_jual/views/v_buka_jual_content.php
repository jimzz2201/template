<div style="position:relative">
    <h3 style="font-weight: bold;">Unit ke <?= $key + 1 ?></h3>
    <hr/>
    <div class="form-group">
        <?= form_label('Select Unit', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-5">
            <?php
            echo form_dropdown(array('type' => 'text','selected'=>@$detail['id_serial_unit'], "onchange" => "ChangeSerial(" . $key . ")", 'name' => 'id_serial_unit[]', 'class' => 'form-control select2 unitserial', 'id' => 'dd_id_serial_unit' . $key, 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_serial_unit, "json", "Unit"), @$detail['id_unit_serial']);
            ?>
        </div>
        <div class="col-sm-1">
            <button type="button" class="btn btn-primary btn-block" onclick="SearchUnit('<?=$key?>')" >Search</button>
        </div>
        <?= form_label('Warna', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'warna[]', 'value' => @$detail['warna'], 'class' => 'form-control', 'id' => 'txt_warna' . $key, 'placeholder' => 'Warna')); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit'.$key, 'placeholder' => 'Unit')); ?>
        </div>
        <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit'.$key, 'placeholder' => 'Tipe Unit')); ?>
        </div>
        <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-2">
            <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori'.$key, 'placeholder' => 'Kategori Unit')); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('VIN Number', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin_number[]', 'value' => @$detail['vin_number'], 'class' => 'form-control', 'id' => 'txt_vin_number' . $key, 'placeholder' => 'VIN Number')); ?>
        </div>
		<?= form_label('Engine&nbsp;Code', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'manufacture_code[]', 'value' => @$detail['manufacture_code'], 'class' => 'form-control', 'id' => 'txt_engine_no' . $key, 'placeholder' => 'Manufacture Code')); ?>
        </div>
        <?= form_label('Vin', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-2">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin[]', 'value' => @$detail['vin'], 'class' => 'form-control', 'id' => 'txt_vin' . $key, 'placeholder' => 'Vin')); ?>
        </div>
        

    </div>
    <div class="form-group">
        <?= form_label('Customer', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'nama_customer[]', 'value' => @$detail['nama_customer_detail'], 'class' => 'form-control', 'id' => 'txt_nama_customer_detail' . $key, 'placeholder' => 'Nama Customer')); ?>
        </div>
        <?= form_label('VRF', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vrf_code[]', 'value' => @$detail['vrf_code_detail'], 'class' => 'form-control', 'id' => 'txt_vrf_code_detail' . $key, 'placeholder' => 'VRF')); ?>
        </div>
        <?= form_label('KPU', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-2">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'nomor_master[]', 'value' => @$detail['nomor_master_detail'], 'class' => 'form-control', 'id' => 'txt_nomor_master_detail' . $key, 'placeholder' => 'KPU')); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#dd_id_serial_unit<?= @$key ?>").select2({data: list_unit_serial});
    })
    
    function SearchUnit(key)
    {
        
    }

</script>