<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Buka_jual extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_buka_jual');
        $this->load->model("warna/m_warna");
        $this->load->model("vrf/m_vrf");
        $this->load->model("model/m_model");
        $this->load->model("unit/m_unit");
        $this->load->model("prospek/m_prospek");
        $this->load->model("pool/m_pool");
    }
    public function  generatejurnal()
    {
        $this->db->from("#_buka_jual");
        $this->db->where("status !=",2);
        $listbukajual=$this->db->get()->result();
        foreach($listbukajual as $bukajual)
        {
            $this->m_buka_jual->Rejurnal($bukajual->id_buka_jual);
        }
        
    }
    public function koreksi(){
        $this->db->from("#_buku_besar");
        $this->db->select("dokumen");
        $this->db->distinct();
        $listdokumen=$this->db->get()->result_array();
        $listdokumen= array_column($listdokumen, "dokumen");
        
        
        $this->db->from("#_buka_jual");
        
        $listbukajual=$this->db->get()->result();
        
        //$this->m_buka_jual->KoreksiBukaJual(18);
        
        foreach($listbukajual as $bukajual)
        {
            $this->m_buka_jual->KoreksiBukaJual($bukajual->id_buka_jual);
        }
        
        
        
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K068";
        $header = "K059";



        $title = "Buka Jual";
        $this->load->model("customer/m_customer");
        $this->load->model("cabang/m_cabang");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $status[] = array();
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal SPK");
        $jenis_pencarian[] = array("id" => "tanggal_buka_jual", "text" => "Tanggal Buka Jual");
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PROSPEK");
        $jenis_keyword[] = array("id" => "nomor_buka_jual", "text" => "NO BUKA JUAL");
        $jenis_keyword[] = array("id" => "engine_no", "text" => "NO Mesin Kendaraan");
        $jenis_keyword[] = array("id" => "vin_number", "text" => "NO Rangka Kendaraan");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", GetUserId());
        $model['list_level'] = GetLevel();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "3", "text" => "Nego");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $status[] = array("id" => "7", "text" => "Permintaan Buka Jual");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['start_date'] = GetCookieSetting("start_date_buka_jual", AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date_buka_jual", GetDateNow());
        
        $model['value_status'] = GetCookieSetting("status_buka_jual", GetCookieSetting("search_buka_jual")=="1"?0:7);
        $model['id_cabang'] = GetCookieSetting("id_cabang_buka_jual", 0);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian_buka_jual");
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_sufix, "nomor_buka_jual");
        $model['list_keyword'] = $jenis_keyword;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "buka_jual/v_buka_jual_index", $javascript, $css);
    }

    public function approval_buka_jual($id) {
        $row = ['button' => 'Update'];
        $this->load->model("buka_jual/m_buka_jual");
        $this->load->model("prospek/m_prospek");
        $row = $this->m_buka_jual->GetOneBukaJual($id);
        $prospek = $this->m_prospek->GetOneProspek($row->id_prospek);
        $row->prospek = $prospek;
        $row->qty_sisa = ($prospek->jumlah_unit - $prospek->qty_buka_jual);
        $row->list_equipment = [];
        $row = json_decode(json_encode($row), true);
        $jenis_pembayaran = array();
        $this->load->model("cabang/m_cabang");
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = ['id' => 1, 'text' => 'Include'];
        $list_type_ppn[2] = ['id' => 2, 'text' => 'Exclude'];
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $userid = GetUserId();
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", $userid);
        $javascript = array();
        $module = "K068";
        $header = "K059";
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("jenis_plat/m_jenis_plat");
        $this->load->model("warna/m_warna");
        $this->load->model("unit/m_unit");
        $this->load->model("leasing/m_leasing");
        $this->load->model("payment_method/m_payment_method");
        $row['title'] = "Tambah Buka Jual";
        $row['tanggal'] = GetDateNow();
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['approvalhtml'] = $this->load->view("v_buka_jual_approval", [], true);
        $row['list_warna'] = $this->m_warna->GetDropDownWarna();
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
        $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
        $row['list_serial_unit'] = $this->m_unit->GetUnitSerialByProspek($prospek->id_prospek, $id);
        $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
        $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
        $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
        $row['propsekhtml'] = $this->load->view("buka_jual/v_buka_jual_prospek_content", $row, true);

        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'buka_jual/v_buka_jual_view', $javascript, $css);
    }
	public function approve_all(){
		$this->db->from("#_buka_jual");
		$this->db->where(array("status"=>0,"deleted_date"=>null));
		$listdb=$this->db->get()->result_array();
	
		foreach($listdb as $bukajual)
		{
			$this->m_buka_jual->ApprovalBukaJual($bukajual, "Approve");
		}
	
		
		
	}
    public function buka_jual_approve() {
        $model = $this->input->post();

        $this->form_validation->set_rules('id_buka_jual', 'Buka Jual', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_buka_jual->ApprovalBukaJual($model, "Approve");
            echo json_encode($data);
        }
    }

    public function buka_jual_reject() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_buka_jual', 'Buka Jual', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_buka_jual->ApprovalBukaJual($model, "Reject");
            echo json_encode($data);
        }
    }

    public function buka_jual_batal() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_buka_jual', 'Buka Jual', 'trim|required');
        $this->form_validation->set_rules('ket_approve', 'Alasan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_buka_jual->ApprovalBukaJual($model, "Batal");
            echo json_encode($data);
        }
    }

    public function buka_jual_nego() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_buka_jual', 'Buka Jual', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_buka_jual->ApprovalBukaJual($model, "Nego");
            echo json_encode($data);
        }
    }

    public function get_one_prospek() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Kpu', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_prospek->GetOneProspek($model["id_prospek"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Kpu tidak ditemukan dalam database" : "";
            if (CheckEmpty(@$model['typehtml'])) {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_content", $data['obj'], true);
            } else {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_hutang_content", $data['obj'], true);
            }
            echo json_encode($data);
        }
    }

    public function get_one_buka_jual() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_buka_jual', 'Buka Jual', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_buka_jual->GetOneBukaJual($model["id_buka_jual"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Buka Jual tidak ditemukan dalam database" : "";
            // if(CheckEmpty(@$model['typehtml']))
            // {
            //     $data['html']=$this->load->view("pembayaran/v_pembayaran_content",$data['obj'],true);
            // }
            // else
            // {
            //     $data['html']=$this->load->view("pembayaran/v_pembayaran_hutang_content",$data['obj'],true);
            // }
            echo json_encode($data);
        }
    }

    public function getdatabukajual() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if($key=="status"&&$value==0)
            {
                SetCookieSetting( "search_buka_jual", 1);
            }
            SetCookieSetting($key . "_buka_jual", $value);
        }
        echo $this->m_buka_jual->GetDataBukajual($params);
    }

    public function getbukajual_api() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        // $from = 0;
        $count = true;
        $jumlah_data = $this->m_buka_jual->GetBukaJual_api($params, null, null, $count);

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/buka_jual/getbukajual_api';
        $config['total_rows'] = count($jumlah_data);
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['num_links'] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $from = $page * $config['per_page'];
        $next_page = $page + 2;
        $data['data'] = $this->m_buka_jual->GetBukaJual_api($params, $config['per_page'], $from, false);
        $data['from'] = $from;
        $data['per_page'] = $config['per_page'];
        $data['next_page_url'] = $config['base_url'] . '/' . $next_page;
        $data['path'] = $config['base_url'];
        $data['total'] = $config['total_rows'];
        $data['current_page'] = $page + 1;
        $data['last_page'] = ceil($config['total_rows'] / $config['per_page']);

        return $this->sendResponse("Success", $data, 200);
    }

    public function sendResponse($msg, $data = null, $status) {
        echo json_encode([
            "msg" => $msg,
            "data" => $data,
            'api_token' => '',
            'expired_at' => ''
                ], $status);
    }

    public function buka_jual_change_status() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_buka_jual', 'id DO Prospek', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_buka_jual->ChangeStatus($model["id_buka_jual"], $model['status']);
            echo json_encode($data);
        }
    }

    public function getDataVrf() {
        $id_Vrf = $this->input->get('id_vrf');
        // header('Content-Type: application/json');
        $res = $this->m_vrf->GetOneVrf($id_Vrf);
        echo $json = json_encode($res);
    }

    public function create_buka_jual() {
        $row = ['button' => 'Add'];
        $jenis_pembayaran = array();
        $this->load->model("cabang/m_cabang");
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $row['status'] = 0;
        $userid = GetUserId();
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", $userid);
        $row['id_cabang'] = GetIdCabang();
        $row['id_pool'] = 4;
        $javascript = array();
        $module = "K068";
        $header = "K059";
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("jenis_plat/m_jenis_plat");
        $this->load->model("leasing/m_leasing");
        $this->load->model("payment_method/m_payment_method");
        $this->load->model("unit/m_unit");
        $row['free_top_proposed'] = 0;
        $row['title'] = "Tambah Buka Jual";
        $row['tanggal'] = GetDateNow();
        $row['type_pembayaran'] = 2;
        $row['free_parking'] = AddDays(GetDateNow(), "5 days");
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_warna'] = $this->m_warna->GetDropDownWarna();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_detail'] = array();
        $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
        $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
        $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
        $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
        $row['list_pool'] = $this->m_pool->GetDropDownPool();
        $css[] = "assets/plugins/iCheck/all.css";
        $javascript[] = "assets/plugins/iCheck/icheck.js";
        //$row['listserial'] =$this->m_unit->GetDropDownSerialUnit();
        //$row['list_detail'][]=array("vin_number"=>"","vin"=>"","manufacture_code"=>"","engine_no"=>"","free_parking"=>"");
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'buka_jual/v_buka_jual_manipulate', $javascript, $css);
    }

    public function edit_buka_jual($id) {

        
        $this->load->model("buka_jual/m_buka_jual");
        $this->load->model("prospek/m_prospek");
        $row = $this->m_buka_jual->GetOneBukaJual($id);
        $prospek = $this->m_prospek->GetOneProspek($row->id_prospek);
        $row->button="Update";
        $row->qty_sisa = ($prospek->jumlah_unit - $prospek->qty_buka_jual) + $row->qty_buka_jual;
        $row = json_decode(json_encode($row), true);
        $jenis_pembayaran = array();
        $this->load->model("cabang/m_cabang");
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $userid = GetUserId();
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", $userid);
        $javascript = array();
        $module = "K068";
        $header = "K059";
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("pool/m_pool");
        $row['title'] = "Edit Buka Jual";
        if (!CheckEmpty(@$row['tanggal_buka_jual'])) {
            $row['tanggal'] = @$row['tanggal_buka_jual'];
        }
       
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_pool'] = $this->m_pool->GetDropDownPool();
        $row['list_serial_unit'] = $this->m_unit->GetUnitSerialByProspek($prospek->id_prospek, $id);
         if (count(@$row['list_detail']) < $row['qty_sisa']) {
            $selisih = $row['qty_sisa'] - count(@$row['list_detail']);
            for ($i = 0; $i < $selisih; $i++) {
                $row['list_detail'][] = ["nama_unit" => "", "nama_type_unit" => ""];
            }
        }
        $this->load->model("payment_method/m_payment_method");
        $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
        //$row['listserial'] =$this->m_unit->GetDropDownSerialUnit();
        //$row['list_detail'][]=array("vin_number"=>"","vin"=>"","manufacture_code"=>"","engine_no"=>"","free_parking"=>"");
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
         $css[] = "assets/plugins/iCheck/all.css";
        $javascript[] = "assets/plugins/iCheck/icheck.js";
        LoadTemplate($row, 'buka_jual/v_buka_jual_manipulate', $javascript, $css);
    }

    public function buka_jual_detail($id = 0) {
        $row = $this->m_buka_jual->GetOneBukaJual($id);

        if ($row) {
            $row = json_decode(json_encode($row), true);
            $module = "K068";
            $header = "K059";
            // CekModule($module);
            // $this->db->where('dgmi_prospek_equipment.id_prospek', $id);
            // $this->db->where('dgmi_prospek_equipment.deleted_date', null);
            // $this->db->join('dgmi_unit', 'dgmi_unit.id_unit=dgmi_prospek_equipment.id_unit');
            // $this->db->select('dgmi_prospek_equipment.*, nama_unit');
            // $row['equipment'] = $this->db->get('dgmi_prospek_equipment')->result_array();

            echo json_encode(array('st' => true, 'msg' => '', 'data' => $row));
        } else {
            echo json_encode(array('st' => false, 'msg' => "Prospek cannot be found in database", 'data' => null));
        }
    }

    public function prospek_content() {
        $model = $this->input->post();
        $model['detail'] = array("vin_number" => "", "vin" => "", "manufacture_code" => "", "engine_no" => "", "free_parking" => "");
        $jumlahgenerate = @$model['key'];
        for ($i = 0; $i < $jumlahgenerate; $i++) {
            $model['key'] = $i;
            $this->load->view("v_buka_jual_content", $model);
        }
    }

    public function buka_jual_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'KPU', 'trim|required');
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $this->form_validation->set_rules('id_pool', 'Pool', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('free_parking', 'Tanggal', 'trim|required');
        //$this->form_validation->set_rules('nomor_po_leasing', 'No PO Leasing', 'trim|required');
        $this->load->model("prospek/m_prospek");

        $sisa = 0;
        $prospek = $this->m_prospek->GetOneProspek(@$model['id_prospek']);
        IF ($prospek)
            $sisa = $prospek->jumlah_unit - $prospek->qty_kirim;
        $dataset = [];
        $qtydo = 0;
        $status = 0;
        if (!CheckEmpty($model['status'])) {
            $status = $model['status'];
        }
        $islanjut = false;
        $model['tempserial'] = [];
        $qty = 0;
        foreach ($model['id_serial_unit'] as $key => $detail) {
            if (!CheckEmpty($detail)) {
                $islanjut = true;
                $serialunit = $this->m_unit->GetOneUnitSerial($detail);
                if (array_search($detail, $model['tempserial']) === false) {
                    $model['tempserial'][] = $detail;
                } else {
                    $message .= "Tidak boleh ada unit yang sama dikeluarkan dalam buka jual<br/>";
                }

                if ($serialunit && $prospek) {
                    $qty += 1;
                    if ($serialunit->id_unit != $prospek->id_unit) {
                        $message .= "Unit tidak sesuai dengan prospek ini<br/>";
                    }
                }
            }
        }
        $model['qty_buka_jual'] = $qty;

        if (!$islanjut) {
             $message.="Tidak ada unit yang dikeluarkan<br/>";
        }
        if (!CheckEmpty(@$model['id_buka_jual'])) {
            $buka_jual = $this->m_buka_jual->GetOneBukaJual(@$model['id_buka_jual']);
            if ($prospek) {
                $sisa += $prospek->qty_buka_jual;
            }
        }
		
        $model['dataset'] = $dataset;

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_buka_jual->BukaJualManipulate($model, $status);
            echo json_encode($status);
        }
    }

    public function prospek_save_do() {
        $message = '';

        $tgl_do = $this->input->post('tgl_do');
        foreach ($tgl_do as $key => $value) {
            $data[] = array(
                'id_prospek' => $_POST['idprospek'],
                'tgl_do' => $_POST['tgl_do'][$key],
                'no_do' => $_POST['no_do'][$key],
                'manufacture_no' => $_POST['manufacture_no'][$key],
                'engine_no' => $_POST['engine_no'][$key],
                'created_by' => GetUserId(),
                'created_date' => GetDateNow(),
            );
        }
        $this->db->insert_batch('dgmi_do', $data);
        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function view_buka_jual($id) {
        $row = ['button' => 'Update'];
        $this->load->model("buka_jual/m_buka_jual");
        $this->load->model("prospek/m_prospek");
        $row = $this->m_buka_jual->GetOneBukaJual($id);
        $prospek = $this->m_prospek->GetOneProspek($row->id_prospek);
        $row->prospek = $prospek;
        $row->qty_sisa = ($prospek->jumlah_unit - $prospek->qty_buka_jual);
        $row->list_equipment = [];
        $row = json_decode(json_encode($row), true);
        $jenis_pembayaran = array();
        $this->load->model("cabang/m_cabang");
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = ['id' => 1, 'text' => 'Include'];
        $list_type_ppn[2] = ['id' => 2, 'text' => 'Exclude'];
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $userid = GetUserId();
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", $userid);
        $javascript = array();
        $module = "K068";
        $header = "K059";
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("jenis_plat/m_jenis_plat");
        $this->load->model("warna/m_warna");
        $this->load->model("unit/m_unit");
        $this->load->model("leasing/m_leasing");
        $this->load->model("payment_method/m_payment_method");
        $row['title'] = "Tambah Buka Jual";
        $row['tanggal'] = GetDateNow();
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_warna'] = $this->m_warna->GetDropDownWarna();
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
        $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
        $row['list_serial_unit'] = $this->m_unit->GetUnitSerialByProspek($prospek->id_prospek, $id);
        $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
        $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
        $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
        $row['propsekhtml'] = $this->load->view("buka_jual/v_buka_jual_prospek_content", $row, true);

        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'buka_jual/v_buka_jual_view', $javascript, $css);
    }

}

/* End of file Kpu.php */