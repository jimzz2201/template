<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Do_prospek extends CI_Controller {

    private  $_prefix="_surat_jalan";
    function __construct() {
        parent::__construct();
        $this->load->model('m_do_prospek');
        $this->load->model("warna/m_warna");
        $this->load->model("vrf/m_vrf");
        $this->load->model("model/m_model");
        $this->load->model("unit/m_unit");
        $this->load->model("kategori/m_kategori");
        $this->load->model("pool/m_pool");
        $this->load->model("buka_jual/m_buka_jual");
        $this->load->model("prospek/m_prospek");
    }
    
    public function pending(){
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/plugins/dropzone/dropzone.min.js";
        $css[] = "assets/plugins/dropzone/dropzone.min.css";
        $module = "K077";
        $header = "K059";
        $title = "DO Prospek";
        $this->load->model("customer/m_customer");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "tanggal_do", "text" => "Tanggal DO");
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal SPK");
        $jenis_pencarian[] = array("id" => "tanggal_buka_jual", "text" => "Tanggal Buka Jual");
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "2", "text" => "Batal");
        $status[] = array("id" => "5", "text" => "Menunggu Approval");
        $model['start_date'] = GetCookieSetting("start_date".$this->_prefix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date".$this->_prefix, GetDateNow());
        $model['list_pencarian'] = $jenis_pencarian;
        $model['value_status'] = GetCookieSetting("status".$this->_prefix, GetCookieSetting("search_buka_jual") == "1" ? 0 : 7);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian".$this->_prefix);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_prefix, "nomor_buka_jual");
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PROSPEK");
        $jenis_keyword[] = array("id" => "nomor_buka_jual", "text" => "NO BUKA JUAL");
        $jenis_keyword[] = array("id" => "engine_no", "text" => "NO Mesin Kendaraan");
        $jenis_keyword[] = array("id" => "vin_number", "text" => "NO Rangka Kendaraan");
        $model['list_keyword'] = $jenis_keyword;
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "do_prospek/v_do_prospek_pending", $javascript, $css);
    }
    
    
    
    
    
    
    
    public function GenerateNonStok(){
        $this->db->from("#_unit_serial");
        
        $this->db->join("#__import_unit","#_unit_serial.vin_number like concat('%',#__import_unit.no_rangka,'%') and #_unit_serial.engine_no like concat('%',#__import_unit.no_mesin,'%')","left");
        $this->db->join("#_buka_jual","#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual");
        $this->db->join("#_prospek","#_prospek.id_prospek=#_buka_jual.id_prospek");
         $this->db->join("#_customer","#_buka_jual.id_customer=#_customer.id_customer");
        
        $this->db->where(array("id_do_prospek_detail"=>null,"__import_unit.no_rangka"=>null));
        $this->db->select("#_buka_jual.id_buka_jual,no_prospek,tanggal_buka_jual,nomor_buka_jual,group_concat(id_unit_serial) as list_unit,#_customer.alamat,#_customer.nama_customer,#_customer.no_telp");
        $this->db->group_by("#_unit_serial.id_buka_jual");
        $listunitdb=$this->db->get()->result_array();
       
        $this->db->trans_rollback();
        $this->db->trans_begin();
        
        foreach ($listunitdb as $unit) {
            $listunit = explode(",", $unit['list_unit']);
            $unitserial = $this->m_unit->GetOneUnitSerial($listunit[0]);
            $do_prospek['id_buka_jual'] = ForeignKeyFromDb($unitserial->id_buka_jual);
            $do_prospek['id_ekspedisi'] = 3;
            $do_prospek['pengemudi'] = "import";
            $this->load->model("buka_jual/m_buka_jual");
            $do_prospek['id_customer'] = $unitserial->id_customer;
            $do_prospek['qty_do'] = count($listunit);
            $do_prospek['keterangan'] = "Import ".$unit['no_prospek'];
            $do_prospek['alamat'] = $unit['alamat'];
            $do_prospek['up'] = $unit['nama_customer'] . " " . $unit['no_telp'];
            $do_prospek['atas_nama'] = $unitserial->nama_customer;
            $tanggaldo = AddDays($unit['tanggal_buka_jual'], "1 days");
            $do_prospek['tanggal_do'] = DefaultTanggalDatabase($tanggaldo);
            $do_prospek['tanggal_proses'] = DefaultTanggalDatabase($tanggaldo);
            $this->load->model("prospek/m_prospek");
            $do_prospek['free_parking'] = $do_prospek['tanggal_do'];
            $do_prospek['status'] = 1;
            $do_prospek['order'] = $unitserial->nama_customer;
            $do_prospek['type_do'] = 3;
            $do_prospek['id_pool'] = CheckEmpty($unitserial->id_pool) ? 4 : $unitserial->id_pool;

            $this->db->from("#_do_prospek");
            $this->db->where(array("tanggal_do" => $do_prospek['tanggal_do'], "up" => $do_prospek['up'], "keterangan" => $do_prospek['keterangan']));
            $dodb = $this->db->get()->row();

            $id_do_prospek = 0;
            $prospekdb = null;
            if (CheckEmpty(@$dodb->id_do_prospek)) {
                $do_prospek['created_date'] = GetDateNow();
                $do_prospek['created_by'] = ForeignKeyFromDb(GetUserId());
                $do_prospek['nomor_do_prospek'] = AutoIncrement('#_do_prospek', 'DB/' . date("y") . '/', 'nomor_do_prospek', 5);
                $res = $this->db->insert("#_do_prospek", $do_prospek);
                $id_do_prospek = $this->db->insert_id();
            } else {
                $do_prospek['updated_date'] = GetDateNow();
                $do_prospek['updated_by'] = ForeignKeyFromDb($userid);
                $do_prospek['nomor_do_prospek'] = @$dodb->nomor_do_prospek;
                $res = $this->db->update("#_do_prospek", $do_prospek, array("id_do_prospek" => @$dodb->id_do_prospek));
                $id_do_prospek = @$dodb->id_do_prospek;
                $prospekdb = $this->m_do_prospek->GetOneDoProspek($id_do_prospek, "id_do_prospek", false);
            }



            if ($do_prospek['type_do'] == 3) {
                $unitserialup['date_do_out'] = $do_prospek['tanggal_do'];
            }
            $do_prospek_detail['id_do_prospek'] = $id_do_prospek;
            $do_prospek_detail['qty_do'] = $do_prospek['qty_do'];
            $do_prospek_detail['created_date'] = GetDateNow();
            $do_prospek_detail['created_by'] = ForeignKeyFromDb($userid);
            $this->db->from("#_do_prospek_detail");
            $this->db->where("id_do_prospek", $id_do_prospek);
            $doProspekDetail = $this->db->get()->row();
            $id_do_prospek_detail = 0;
            if ($doProspekDetail) {
                $this->db->update("#_do_prospek_detail", $do_prospek_detail, array("id_do_prospek_detail" => $doProspekDetail->id_do_prospek_detail));
                $id_do_prospek_detail = $doProspekDetail->id_do_prospek_detail;
            } else {
                $do_prospek_detail['id_prospek'] = ForeignKeyFromDb($unitserial->id_prospek);
                $do_prospek_detail['id_unit'] = ForeignKeyFromDb($unitserial->id_unit);
                $this->db->insert('#_do_prospek_detail', $do_prospek_detail);
                $id_do_prospek_detail = $this->db->insert_id();
            }

            $this->db->update("#_unit_serial", array('date_do_out' => null), array("id_do_prospek_detail" => $id_do_prospek_detail));

            $historylist = [];

            foreach ($listunit as $key => $det) {

                $unitserialup['updated_date'] = GetDateNow();
                $unitserialup['updated_by'] = GetUserId();
                $unitserialup['id_do_prospek_detail'] = $id_do_prospek_detail;
                $unitserialup['id_pool'] = 3;
                $this->db->update("#_unit_serial", $unitserialup, array("id_unit_serial" => $det));
                $this->db->from("#_history_stok_unit");
                $this->db->where(array("id_unit_serial" => $det, "module" => "do_prospek", "ref_doc" => $do_prospek['nomor_do_prospek']));
                $history_stok_db = $this->db->get()->row();
                $data['id_unit_serial'] = $det;
                $data['ref_doc'] = $do_prospek['nomor_do_prospek'];
                $data['id_pool'] = 3;
                $data['tanggal'] = $do_prospek['tanggal_do'];
                $data['keterangan'] = "DO Customer";
                $data['id_master_transaction'] = $id_do_prospek;
                $data['id_detail_transaction'] = $id_do_prospek_detail;
                $data['module'] = "do_prospek";
                $data['kolom1'] = "1. Ban Cadangan dan Velg : 1 Buah &#13;&#10;2. Pompa Pelumas : - &#13;&#10;3. Perkakas terdiri dari: &#13;&#10;- Dongkrak dan Stang &#13;&#10;- Kunci Roda dan Stang &#13;&#10;- Engkol Ban Cadangan";
                $data['kolom2'] = "4. Toolset terdiri dari : &#13;&#10;- Kunci Pas : 5 Buah &#13;&#10;- Tang : 1 Buah &#13;&#10;- Obeng : 1 Buah &#13;&#10;- Kunci Inggris : 1 (T) &#13;&#10;5. Palu : 1 Buah";
                $data['keterangan_bawah'] = "Unit Tidak Boleh Keluar Dari Karoseri Tanpa Persetujuan DGMI";

                if (CheckEmpty($history_stok_db)) {
                    $data['created_by'] = GetUserId();
                    $data['created_date'] = GetDateNow();
                    $res = $this->db->insert('#_history_stok_unit', $data);
                    $historylist[] = $this->db->insert_id();
                } else {
                    $data['updated_by'] = GetUserId();
                    $data['updated_date'] = GetDateNow();
                    $this->db->update("#_history_stok_unit", $data, array("id_history_stok_unit" => $history_stok_db->id_history_stok_unit));
                    $historylist[] = $history_stok_db->id_history_stok_unit;
                }
            }
            $this->db->from("#_history_stok_unit");
            $this->db->where(array("ref_doc" => $do_prospek['nomor_do_prospek']));
            if (count($historylist) > 0) {
                $this->db->where_not_in("id_history_stok_unit", $historylist);
            }
            $this->db->select("id_history_stok_unit,id_unit_serial");
            $resultarr = $this->db->get()->result_array();


            $listid_historydelete = array_column($resultarr, "id_history_stok_unit");
            $listunitserial = array_column($resultarr, "id_unit_serial");


            if (count($listid_historydelete)) {
                $this->db->where_in("id_history_stok_unit", $listid_historydelete);
                $this->db->delete("#_history_stok_unit");
            }

            if (count($listunitserial)) {
                $this->db->where_in("id_unit_serial", $listunitserial);
                $this->db->update("#_unit_serial", array("id_do_prospek_detail" => null));
            }
        }
        $this->db->trans_commit();
        
        
    }
    
    
    
    
    public function GenerateStokUnit()
    {
        $this->load->model("customer/m_customer");
        $this->load->model("unit/m_unit");
        $this->load->model("mutasi_unit/m_mutasi_unit");
        $this->db->from("#__import_stok");
        
        $this->db->join("#_unit_serial","#_unit_serial.vin_number like concat('%',#__import_stok.no_rangka,'%') and #_unit_serial.engine_no like concat('%',#__import_stok.no_mesin,'%')");
        $this->db->select("#_unit_serial.id_pool as poolsekarang,#_unit_serial.id_unit_serial,#_pool.id_pool as pooolsemestinya,tanggal,keterangan");
        $this->db->join("#_pool","#_pool.nama_pool=#__import_stok.pool");
        $listunit=$this->db->get()->result();
       
        foreach($listunit as $unit)
        {
            if($unit->poolsekarang<>$unit->pooolsemestinya)
            {
                $unitserial = $this->m_unit->GetOneUnitSerial($unit->id_unit_serial);
                $customer = $this->m_customer->GetOneCustomer($unitserial->id_customer);
                
                
                $mutasi_unit['id_ekspedisi'] = 3;
                $mutasi_unit['pengemudi'] = "import";
                $mutasi_unit['qty_do'] = 1;
                $mutasi_unit['alamat'] = $customer->alamat;
                $mutasi_unit['up'] = $unitserial->nama_customer . " " . $unitserial->no_telp;
                $mutasi_unit['atas_nama'] = $unitserial->nama_customer;
                $tanggaldo = $unit->tanggal;
                if(CheckEmpty($tanggaldo))
                {
                    $tanggaldo='2020-10-01';
                }
                $mutasi_unit['tanggal_do'] = $tanggaldo;
                $mutasi_unit['tanggal_proses'] = $tanggaldo;
                $mutasi_unit['status'] = 1;
                $mutasi_unit['order'] = $unitserial->nama_customer;
                $mutasi_unit['keterangan'] = $unit->keterangan;
                $mutasi_unit['type_do'] = 1;
                $mutasi_unit['pengemudi'] = "import";
                $mutasi_unit['id_pool_from'] = CheckEmpty($unitserial->id_pool) ? 4 : $unitserial->id_pool;
                $mutasi_unit['id_pool'] = $unit->pooolsemestinya;
                $userid = GetUserId();
                $id_prospek = 0;
                $this->db->from("#_mutasi");
                $this->db->where(array("tanggal_do" => $mutasi_unit['tanggal_do'], "up" => $mutasi_unit['up'], "keterangan" => $mutasi_unit['keterangan']));
                $mutasidb = $this->db->get()->row();


                if (CheckEmpty(@$mutasidb->id_mutasi)) {
                    $mutasi_unit['created_date'] = GetDateNow();
                    $mutasi_unit['created_by'] = ForeignKeyFromDb(GetUserId());
                    $mutasi_unit['nomor_mutasi'] = AutoIncrement('#_mutasi', ($mutasi_unit['type_do'] == "1" ? 'MM/' : "MK/") . date("y") . '/', 'nomor_mutasi', 5);
                    $res = $this->db->insert("#_mutasi", $mutasi_unit);
                    $id_mutasi = $this->db->insert_id();
                } else {
                    $mutasi_unit['updated_date'] = GetDateNow();
                    $mutasi_unit['updated_by'] = ForeignKeyFromDb(GetUserId());
                    $res = $this->db->update("#_mutasi", $mutasi_unit, array("id_mutasi" => @$mutasidb->id_mutasi));
                    $id_mutasi = @$mutasidb->id_mutasi;
                    $mutasidb = $this->m_mutasi_unit->GetOneMutasi($id_mutasi, "id_mutasi", false);
                    $mutasi_unit['nomor_mutasi'] = $mutasidb->nomor_mutasi;
                }

                $mutasi_unit_detail['id_mutasi'] = $id_mutasi;
                $mutasidetailid = [];
                $historylist = [];
                $this->db->from("#_mutasi_detail");
                $this->db->where(array("id_mutasi" => $id_mutasi, "id_unit_serial" => $unit->id_unit_serial));
                $mutasidetaildb = $this->db->get()->row();

                $id_mutasi_detail = 0;
                if ($mutasidetaildb) {
                    $id_mutasi_detail = $mutasidetaildb->id_mutasi_detail_unit;
                } else {
                    $mutasidetailinsert = [];
                    $mutasidetailinsert['id_unit_serial'] = $unit->id_unit_serial;
                    $mutasidetailinsert['id_mutasi'] = $id_mutasi;
                    $this->db->insert("#_mutasi_detail", $mutasidetailinsert);
                    $id_mutasi_detail = $this->db->insert_id();
                }
                $mutasidetailid[] = $id_mutasi_detail;


                $this->db->from("#_history_stok_unit");
                $this->db->where(array("ref_doc" => $mutasi_unit['nomor_mutasi'], "id_unit_serial" => $unit->id_unit_serial));
                $historydetaildb = $this->db->get()->row();


                $data['id_unit_serial'] = $detail;
                $data['ref_doc'] = $mutasi_unit['nomor_mutasi'];
                $data['id_pool'] = $mutasi_unit['id_pool'];
                $data['tanggal'] = $mutasi_unit['tanggal_do'];
                $data['keterangan'] = "Mutasi " . ($mutasi_unit['type_do'] == "1" ? 'Masuk' : "Keluar");
                $data['id_master_transaction'] = $id_mutasi;
                $data['id_detail_transaction'] = $id_mutasi_detail;
                $data['module'] = "mutasi_unit";
                if (CheckEmpty($historydetaildb)) {
                    $data['created_by'] = GetUserId();
                    $data['created_date'] = GetDateNow();
                    $res = $this->db->insert('#_history_stok_unit', $data);
                    $historylist[] = $this->db->insert_id();
                } else {
                    $data['updated_by'] = GetUserId();
                    $data['updated_date'] = GetDateNow();
                    $this->db->update("#_history_stok_unit", $data, array("id_history_stok_unit" => $historydetaildb->id_history_stok_unit));
                    $historylist[] = $historydetaildb->id_history_stok_unit;
                }
                $this->m_mutasi_unit->SetUnitSerialPool($data['id_unit_serial']);

                $this->db->where(array("ref_doc" => $mutasi_unit['nomor_mutasi']));
                if (count($historylist) > 0) {
                    $this->db->where_not_in("id_history_stok_unit", $historylist);
                }
                $this->db->delete("#_history_stok_unit");

                $this->db->where(array("id_mutasi" => $id_mutasi));
                if (count($historylist) > 0) {
                    $this->db->where_not_in("id_mutasi_detail_unit", $mutasidetailid);
                }
                $this->db->delete("#_mutasi_detail");




            }
        }
    }
    
    
    
    
    public function generate_do_prospek()
    {
        $this->db->from("#__import_pengiriman");
        $this->db->join("#_unit_serial","#_unit_serial.vin_number like concat('%',#__import_pengiriman.no_rangka,'%') and #_unit_serial.engine_no like concat('%',#__import_pengiriman.no_mesin,'%')");
        $this->db->group_by("#__import_pengiriman.tanggal_do,#__import_pengiriman.ket_1");
        $this->db->select("#__import_pengiriman.*,group_concat(#_unit_serial.id_unit_serial) as unit_id");
        $listresult=$this->db->get()->result_array();
        $this->load->model("unit/m_unit");
        $this->load->model("mutasi_unit/m_mutasi_unit");
        $this->load->model("do_prospek/m_do_prospek");
        $userid = GetUserId();
        $this->db->trans_rollback();
        $this->db->trans_begin();
        for ($urut = 1; $urut <= 3; $urut++) {
            foreach ($listresult as $result) {

                $listunit = explode(",", $result['unit_id']);
                $unitserial = $this->m_unit->GetOneUnitSerial($listunit[0]);
                if ($result['type_' . $urut] == "customer") {

                    $do_prospek['id_buka_jual'] = ForeignKeyFromDb($unitserial->id_buka_jual);
                    $do_prospek['id_ekspedisi'] = 3;
                    $do_prospek['pengemudi'] = "import";
                    $this->load->model("buka_jual/m_buka_jual");
                    $do_prospek['id_customer'] = $unitserial->id_customer;
                    $do_prospek['qty_do'] = count($listunit);
                    $do_prospek['keterangan'] = $result['ket_' . $urut];
                    $do_prospek['alamat'] = $result['alamat_pengiriman_' . $urut];
                    $do_prospek['up'] = $result['up_' . $urut] . " " . $result['telp_' . $urut];
                    $do_prospek['atas_nama'] = $unitserial->nama_customer;
                    if ($urut == 1) {
                        $tanggaldo = $result['tanggal_do'];
                        $result['tanggal_do'] = ($result['year_' . $urut] . '-' . $result['month_' . $urut] . "-" . $result['day_' . $urut]);
                    } else {
                        $tanggaldo = ($result['year_' . $urut] . '-' . $result['month_' . $urut] . "-" . $result['day_' . $urut]);
                    }
                    $do_prospek['tanggal_do'] = DefaultTanggalDatabase(CheckEmpty($tanggaldo) ? $result['tanggal_do'] : $tanggaldo);
                    $do_prospek['tanggal_proses'] = DefaultTanggalDatabase(CheckEmpty($tanggaldo) ? $result['tanggal_do'] : $tanggaldo);
                    $this->load->model("prospek/m_prospek");
                    $do_prospek['free_parking'] = $do_prospek['tanggal_do'];
                    $do_prospek['status'] = 1;
                    $do_prospek['order'] = $unitserial->nama_customer;
                    $do_prospek['keterangan'] = $result['ket_' . $urut];
                    $do_prospek['type_do'] = 3;
                    $do_prospek['id_pool'] = CheckEmpty($unitserial->id_pool) ? 4 : $unitserial->id_pool;

                    $this->db->from("#_do_prospek");
                    $this->db->where(array("tanggal_do" => $do_prospek['tanggal_do'], "up" => $do_prospek['up'], "keterangan" => $do_prospek['keterangan']));
                    $dodb = $this->db->get()->row();
                    
                    $id_do_prospek = 0;
                    $prospekdb=null;
                    if (CheckEmpty(@$dodb->id_do_prospek)) {
                        $do_prospek['created_date'] = GetDateNow();
                        $do_prospek['created_by'] = ForeignKeyFromDb(GetUserId());
                        $do_prospek['nomor_do_prospek'] = AutoIncrement('#_do_prospek', 'DB/' . date("y") . '/', 'nomor_do_prospek', 5);
                        $res = $this->db->insert("#_do_prospek", $do_prospek);
                        $id_do_prospek = $this->db->insert_id();
                    } else {
                        $do_prospek['updated_date'] = GetDateNow();
                        $do_prospek['updated_by'] = ForeignKeyFromDb($userid);
                        $do_prospek['nomor_do_prospek'] = @$dodb->nomor_do_prospek;
                        $res = $this->db->update("#_do_prospek", $do_prospek, array("id_do_prospek" => @$dodb->id_do_prospek));
                        $id_do_prospek = @$dodb->id_do_prospek;
                        $prospekdb = $this->m_do_prospek->GetOneDoProspek($id_do_prospek, "id_do_prospek", false);
                        
                    }
                    
                    
                       
                    if ($do_prospek['type_do'] == 3) {
                        $unitserialup['date_do_out'] = $do_prospek['tanggal_do'];
                    }
                    $do_prospek_detail['id_do_prospek'] = $id_do_prospek;
                    $do_prospek_detail['qty_do'] = $do_prospek['qty_do'];
                    $do_prospek_detail['created_date'] = GetDateNow();
                    $do_prospek_detail['created_by'] = ForeignKeyFromDb($userid);
                    $this->db->from("#_do_prospek_detail");
                    $this->db->where("id_do_prospek", $id_do_prospek);
                    $doProspekDetail = $this->db->get()->row();
                    $id_do_prospek_detail = 0;
                    if ($doProspekDetail) {
                        $this->db->update("#_do_prospek_detail", $do_prospek_detail, array("id_do_prospek_detail" => $doProspekDetail->id_do_prospek_detail));
                        $id_do_prospek_detail = $doProspekDetail->id_do_prospek_detail;
                    } else {
                        $do_prospek_detail['id_prospek'] = ForeignKeyFromDb($unitserial->id_prospek);
                        $do_prospek_detail['id_unit'] = ForeignKeyFromDb($unitserial->id_unit);
                        $this->db->insert('#_do_prospek_detail', $do_prospek_detail);
                        $id_do_prospek_detail = $this->db->insert_id();
                    }
                    
                    $this->db->update("#_unit_serial", array('date_do_out' => null), array("id_do_prospek_detail" => $id_do_prospek_detail));
                  
                    $historylist = [];

                    foreach ($listunit as $key => $det) {

                        $unitserialup['updated_date'] = GetDateNow();
                        $unitserialup['updated_by'] = GetUserId();
                        $unitserialup['id_do_prospek_detail'] = $id_do_prospek_detail;
                        $unitserialup['id_pool'] = 3;
                        $this->db->update("#_unit_serial", $unitserialup, array("id_unit_serial" => $det));
                        $this->db->from("#_history_stok_unit");
                        $this->db->where(array("id_unit_serial" => $det, "module" => "do_prospek", "ref_doc" => $do_prospek['nomor_do_prospek']));
                        $history_stok_db = $this->db->get()->row();
                         $data['id_unit_serial'] = $det;
                        $data['ref_doc'] = $do_prospek['nomor_do_prospek'];
                        $data['id_pool'] = 3;
                        $data['tanggal'] = $do_prospek['tanggal_do'];
                        $data['keterangan'] = "DO Customer";
                        $data['id_master_transaction'] = $id_do_prospek;
                        $data['id_detail_transaction'] = $id_do_prospek_detail;
                        $data['module'] = "do_prospek";
                        $data['kolom1'] = "1. Ban Cadangan dan Velg : 1 Buah &#13;&#10;2. Pompa Pelumas : - &#13;&#10;3. Perkakas terdiri dari: &#13;&#10;- Dongkrak dan Stang &#13;&#10;- Kunci Roda dan Stang &#13;&#10;- Engkol Ban Cadangan";
                        $data['kolom2'] = "4. Toolset terdiri dari : &#13;&#10;- Kunci Pas : 5 Buah &#13;&#10;- Tang : 1 Buah &#13;&#10;- Obeng : 1 Buah &#13;&#10;- Kunci Inggris : 1 (T) &#13;&#10;5. Palu : 1 Buah";
                        $data['keterangan_bawah'] = "Unit Tidak Boleh Keluar Dari Karoseri Tanpa Persetujuan DGMI";
                          
                        if (CheckEmpty($history_stok_db)) {
                            $data['created_by'] = GetUserId();
                            $data['created_date'] = GetDateNow();
                            $res = $this->db->insert('#_history_stok_unit', $data);
                            $historylist[] = $this->db->insert_id();
                           
                        } else {
                            $data['updated_by'] = GetUserId();
                            $data['updated_date'] = GetDateNow();
                            $this->db->update("#_history_stok_unit", $data, array("id_history_stok_unit" => $history_stok_db->id_history_stok_unit));
                            $historylist[] = $history_stok_db->id_history_stok_unit;
                        }
                    }
                    $this->db->from("#_history_stok_unit");
                    $this->db->where(array("ref_doc" => $do_prospek['nomor_do_prospek']));
                    if (count($historylist) > 0) {
                        $this->db->where_not_in("id_history_stok_unit", $historylist);
                    }
                    $this->db->select("id_history_stok_unit,id_unit_serial");
                    $resultarr = $this->db->get()->result_array();
                   
                    
                    $listid_historydelete = array_column($resultarr, "id_history_stok_unit");
                    $listunitserial = array_column($resultarr, "id_unit_serial");


                    if (count($listid_historydelete)) {
                        $this->db->where_in("id_history_stok_unit", $listid_historydelete);
                        $this->db->delete("#_history_stok_unit");
                    }

                    if (count($listunitserial)) {
                        $this->db->where_in("id_unit_serial", $listunitserial);
                        $this->db->update("#_unit_serial", array("id_do_prospek_detail" => null));
                          
                        
                    }
                
                } else if ($result['type_' . $urut] == "karoseri") {
                    $mutasi_unit['id_ekspedisi'] = 3;
                    $mutasi_unit['qty_do'] = count($listunit);
                    $mutasi_unit['keterangan'] = $result['ket_' . $urut];
                    $mutasi_unit['alamat'] = $result['alamat_pengiriman_' . $urut];
                    $mutasi_unit['up'] = $result['up_' . $urut];
                    $mutasi_unit['atas_nama'] = $unitserial->nama_customer;
                    if ($urut == 1) {
                        $tanggaldo = $result['tanggal_do'];
                        $result['tanggal_do'] = ($result['year_' . $urut] . '-' . $result['month_' . $urut] . "-" . $result['day_' . $urut]);
                    } else {
                        $tanggaldo = ($result['year_' . $urut] . '-' . $result['month_' . $urut] . "-" . $result['day_' . $urut]);
                    }
                    $mutasi_unit['tanggal_do'] = DefaultTanggalDatabase(CheckEmpty($tanggaldo) ? $result['tanggal_do'] : $tanggaldo);
                    $mutasi_unit['tanggal_proses'] = DefaultTanggalDatabase(CheckEmpty($tanggaldo) ? $result['tanggal_do'] : $tanggaldo);
                    $mutasi_unit['status'] = 1;
                    $mutasi_unit['order'] = $unitserial->nama_customer;
                    $mutasi_unit['keterangan'] = $result['ket_' . $urut];
                    $mutasi_unit['type_do'] = 2;
                    $mutasi_unit['pengemudi'] = "import";
                    $mutasi_unit['id_pool_from'] = CheckEmpty($unitserial->id_pool) ? 4 : $unitserial->id_pool;
                    $mutasi_unit['id_pool'] = 2;
                    $userid = GetUserId();
                    $id_prospek = 0;
                    $this->db->from("#_mutasi");
                    $this->db->where(array("tanggal_do" => $mutasi_unit['tanggal_do'], "up" => $mutasi_unit['up'], "keterangan" => $mutasi_unit['keterangan']));
                    $mutasidb = $this->db->get()->row();


                    if (CheckEmpty(@$mutasidb->id_mutasi)) {
                        $mutasi_unit['created_date'] = GetDateNow();
                        $mutasi_unit['created_by'] = ForeignKeyFromDb(GetUserId());
                        $mutasi_unit['nomor_mutasi'] = AutoIncrement('#_mutasi', ($mutasi_unit['type_do'] == "1" ? 'MM/' : "MK/") . date("y") . '/', 'nomor_mutasi', 5);
                        $res = $this->db->insert("#_mutasi", $mutasi_unit);
                        $id_mutasi = $this->db->insert_id();
                    } else {
                        $mutasi_unit['updated_date'] = GetDateNow();
                        $mutasi_unit['updated_by'] = ForeignKeyFromDb(GetUserId());
                        $res = $this->db->update("#_mutasi", $mutasi_unit, array("id_mutasi" => @$mutasidb->id_mutasi));
                        $id_mutasi = @$mutasidb->id_mutasi;
                        $mutasidb = $this->m_mutasi_unit->GetOneMutasi($id_mutasi, "id_mutasi", false);
                        $mutasi_unit['nomor_mutasi'] = $mutasidb->nomor_mutasi;
                    }

                    $mutasi_unit_detail['id_mutasi'] = $id_mutasi;
                    $mutasidetailid = [];
                    $historylist = [];
                    foreach ($listunit as $detail) {
                        $this->db->from("#_mutasi_detail");
                        $this->db->where(array("id_mutasi" => $id_mutasi, "id_unit_serial" => $detail));
                        $mutasidetaildb = $this->db->get()->row();

                        $id_mutasi_detail = 0;
                        if ($mutasidetaildb) {
                            $id_mutasi_detail = $mutasidetaildb->id_mutasi_detail_unit;
                        } else {
                            $mutasidetailinsert = [];
                            $mutasidetailinsert['id_unit_serial'] = $detail;
                            $mutasidetailinsert['id_mutasi'] = $id_mutasi;
                            $this->db->insert("#_mutasi_detail", $mutasidetailinsert);
                            $id_mutasi_detail = $this->db->insert_id();
                        }
                        $mutasidetailid[] = $id_mutasi_detail;


                        $this->db->from("#_history_stok_unit");
                        $this->db->where(array("ref_doc" => $mutasi_unit['nomor_mutasi'], "id_unit_serial" => $detail));
                        $historydetaildb = $this->db->get()->row();


                        $data['id_unit_serial'] = $detail;
                        $data['ref_doc'] = $mutasi_unit['nomor_mutasi'];
                        $data['id_pool'] = $mutasi_unit['id_pool'];
                        $data['tanggal'] = $mutasi_unit['tanggal_do'];
                        $data['keterangan'] = "Mutasi " . ($mutasi_unit['type_do'] == "1" ? 'Masuk' : "Keluar");
                        $data['id_master_transaction'] = $id_mutasi;
                        $data['id_detail_transaction'] = $id_mutasi_detail;
                        $data['module'] = "mutasi_unit";
                        if (CheckEmpty($historydetaildb)) {
                            $data['created_by'] = GetUserId();
                            $data['created_date'] = GetDateNow();
                            $res = $this->db->insert('#_history_stok_unit', $data);
                            $historylist[] = $this->db->insert_id();
                        } else {
                            $data['updated_by'] = GetUserId();
                            $data['updated_date'] = GetDateNow();
                            $this->db->update("#_history_stok_unit", $data, array("id_history_stok_unit" => $historydetaildb->id_history_stok_unit));
                            $historylist[] = $historydetaildb->id_history_stok_unit;
                        }
                        $this->m_mutasi_unit->SetUnitSerialPool($data['id_unit_serial']);
                    }

                    $this->db->where(array("ref_doc" => $mutasi_unit['nomor_mutasi']));
                    if (count($historylist) > 0) {
                        $this->db->where_not_in("id_history_stok_unit", $historylist);
                    }
                    $this->db->delete("#_history_stok_unit");

                    $this->db->where(array("id_mutasi" => $id_mutasi));
                    if (count($historylist) > 0) {
                        $this->db->where_not_in("id_mutasi_detail_unit", $mutasidetailid);
                    }
                    $this->db->delete("#_mutasi_detail");
                }
            }
        }


        $this->db->trans_commit();
        
    }
    
    
    
    
    
    
    public function bast_do_prospek($id_do_prospek){
        $doprospek=$this->m_do_prospek->GetOneDoProspek($id_do_prospek);
        $this->load->view("v_do_prospek_bast",$doprospek);
        
    }
    public function surat_perintah_do_prospek($id_do_prospek){
        $doprospek=$this->m_do_prospek->GetOneDoProspek($id_do_prospek);
        $this->load->model("user/m_user");
        $user = $this->m_user->GetOneUser(GetUserId(), 'id_user');

        if($user)
        {
            $doprospek->nama_user_login = $user->nama_user;
        }
        else
        {
            $doprospek->nama_user_login = "(Nama dan Cap)";
        }
        
        $this->load->view("v_do_prospek_surat_perintah",$doprospek);
        
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/plugins/dropzone/dropzone.min.js";
        $css[] = "assets/plugins/dropzone/dropzone.min.css";
        $module = "K077";
        $header = "K059";
        $title = "DO Prospek";
        $this->load->model("customer/m_customer");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "tanggal_do", "text" => "Tanggal DO");
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal SPK");
        $jenis_pencarian[] = array("id" => "tanggal_buka_jual", "text" => "Tanggal Buka Jual");
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "2", "text" => "Batal");
        $status[] = array("id" => "5", "text" => "Menunggu Approval");
        $model['start_date'] = GetCookieSetting("start_date".$this->_prefix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date".$this->_prefix, GetDateNow());
        $model['list_pencarian'] = $jenis_pencarian;
        $model['value_status'] = GetCookieSetting("status".$this->_prefix, GetCookieSetting("search_buka_jual") == "1" ? 0 : 7);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian".$this->_prefix);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_prefix, "nomor_buka_jual");
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PROSPEK");
        $jenis_keyword[] = array("id" => "nomor_buka_jual", "text" => "NO BUKA JUAL");
        $jenis_keyword[] = array("id" => "engine_no", "text" => "NO Mesin Kendaraan");
        $jenis_keyword[] = array("id" => "vin_number", "text" => "NO Rangka Kendaraan");
		$jenis_keyword[] = array("id" => "nomor_do_prospek", "text" => "Nomor DO");
        $model['list_keyword'] = $jenis_keyword;
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "do_prospek/v_do_prospek_index", $javascript, $css);
    }

    public function get_one_prospek() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Kpu', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_prospek->GetOneProspek($model["id_prospek"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Kpu tidak ditemukan dalam database" : "";
            if (CheckEmpty(@$model['typehtml'])) {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_content", $data['obj'], true);
            } else {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_hutang_content", $data['obj'], true);
            }
            echo json_encode($data);
        }
    }

    public function get_one_do_prospek() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_do_prospek', 'Do Prospek', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_do_prospek->GetOneDoProspek($model["id_do_prospek"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data DO Prospek tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdatadoprospek() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search".$this->_prefix, 1);
            }
            SetCookieSetting($key . $this->_prefix, $value);
        }
        echo $this->m_do_prospek->GetDataDoprospek($params);
    }
    
    public function getdatapendingdo() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
       
        echo $this->m_do_prospek->GetUnitBelumTerkirim(true);
    }

    public function getdoprospek_api() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        // $from = 0;
        $count = true;
        $jumlah_data = $this->m_do_prospek->GetDoProspek_api($params, null, null, $count);

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/do_prospek/getdoprospek_api';
        $config['total_rows'] = count($jumlah_data);
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['num_links'] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $from = $page * $config['per_page'];
        $next_page = $page + 2;
        $data['data'] = $this->m_do_prospek->GetDoProspek_api($params, $config['per_page'], $from, false);
        $data['from'] = $from;
        $data['per_page'] = $config['per_page'];
        $data['next_page_url'] = $config['base_url'] . '/' . $next_page;
        $data['path'] = $config['base_url'];
        $data['total'] = $config['total_rows'];
        $data['current_page'] = $page + 1;
        $data['last_page'] = ceil($config['total_rows'] / $config['per_page']);

        return $this->sendResponse("Success", $data, 200);
    }

    public function sendResponse($msg, $data = null, $status) {
        echo json_encode([
            "msg" => $msg,
            "data" => $data,
            'api_token' => '',
            'expired_at' => ''
                ], $status);
    }

    public function do_prospek_change_status() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_do_prospek', 'id DO Prospek', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_do_prospek->ChangeStatus($model["id_do_prospek"], $model['status']);
            echo json_encode($data);
        }
    }

    public function getDataVrf() {
        $id_Vrf = $this->input->get('id_vrf');
        // header('Content-Type: application/json');
        $res = $this->m_vrf->GetOneVrf($id_Vrf);
        echo $json = json_encode($res);
    }

    public function get_buka_jual_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_buka_jual = $this->m_buka_jual->GetDropDownBukaJualFromCustomer($model);
        echo json_encode(array("list_buka_jual" => DefaultEmptyDropdown($list_buka_jual, "json", "Buka Jual"), "st" => true));
    }

    public function get_prospek_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_prospek = $this->m_prospek->GetDropDownProspekForBukaJual($model);
        echo json_encode(array("list_prospek" => DefaultEmptyDropdown($list_prospek, "json", "Prospek"), "st" => true));
    }

    public function create_do_prospek() {
        $row = ['button' => 'Add'];
        $jenis_pembayaran = array();
        $javascript = array();
        $module = "K077";
        $header = "K059";
        $this->load->model("customer/m_customer");
        
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("ekspedisi/m_ekspedisi");
        $row['free_top_proposed'] = 0;
        $row['title'] = "Add DO Prospek";
        $row['tanggal_do'] = GetDateNow();
        $row['type_pembayaran'] = 2;
        $this->load->model("supplier/m_supplier");
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $row['free_parking'] = AddDays(GetDateNow(), "5 days");
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_detail'] = array();
        $row['list_ekspedisi'] = $this->m_ekspedisi->GetDropDownEkspedisi();
        $row['list_pool'] = $this->m_pool->GetDropDownPool([],[3]);
        // $row['list_buka_jual'] =$this->m_buka_jual->GetDropDownBukaJual();
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_detail'][] = array("vin_number" => "", "vin" => "", "manufacture_code" => "", "engine_no" => "", "free_parking" => "");
        $row['list_type_do'] = array( array('id' => 3, 'text' => 'DO Customer'));
        $row['type_do']=3;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css[] = 'assets/plugins/iCheck/all.css';
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/plugins/dropzone/dropzone.min.js";
        $css[] = "assets/plugins/dropzone/dropzone.min.css";
        LoadTemplate($row, 'do_prospek/v_do_prospek_manipulate', $javascript, $css);
    }

    public function do_prospek_detail($id = 0) {
        $row = $this->m_do_prospek->GetOneDoProspek($id);

        if ($row) {
            $row = json_decode(json_encode($row), true);
            $module = "K077";
            $header = "K059";
            // CekModule($module);
            // $this->db->where('dgmi_prospek_equipment.id_prospek', $id);
            // $this->db->where('dgmi_prospek_equipment.deleted_date', null);
            // $this->db->join('dgmi_unit', 'dgmi_unit.id_unit=dgmi_prospek_equipment.id_unit');
            // $this->db->select('dgmi_prospek_equipment.*, nama_unit');
            // $row['equipment'] = $this->db->get('dgmi_prospek_equipment')->result_array();

            echo json_encode(array('st' => true, 'msg' => '', 'data' => $row));
        } else {
            echo json_encode(array('st' => false, 'msg' => "Prospek cannot be found in database", 'data' => null));
        }
    }

    public function prospek_content() {
        $model = $this->input->post();
        $model['detail'] = array("vin_number" => "", "vin" => "", "manufacture_code" => "", "engine_no" => "", "free_parking" => "");
        $this->load->view("v_do_prospek_content", $model);
    }

    public function do_prospek_manipulate() {
        $message = '';
        $model = $this->input->post();

        // $this->form_validation->set_rules('id_prospek', 'KPU', 'trim');
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim');
        $this->form_validation->set_rules('id_pool', 'Pool', 'trim|required');
        $this->form_validation->set_rules('type_do', 'Type DO', 'trim|required');
        $this->form_validation->set_rules('tanggal_do', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('id_ekspedisi', 'Ekspedisi', 'trim|required');
        $this->form_validation->set_rules('free_parking', 'Tanggal', 'trim');
        $this->load->model("prospek/m_prospek");
        $sisa = 0;
       
        $prospek = $this->m_prospek->GetOneProspek(@$model['id_prospek']);
        IF ($prospek)
            $sisa = $prospek->jumlah_unit - $prospek->qty_kirim;
        $dataset = [];
        $qtydo = 0;
        $islanjut = false;
        foreach ($model['id_unit_serial'] as $key => $detail) {
            if ($detail != "") {
                $islanjut = true;
                $serialunit = $this->m_unit->GetOneUnitSerial($detail);
                if ($serialunit && $prospek) {
                    if ($serialunit->id_unit != $prospek->id_unit) {
                        $message .= "Unit tidak sesuai dengan prospek ini<br/>";
                    }
                }
            }
        }
        if (!$islanjut) {
            $message .= "Tidak ada unit yang dikeluarkan<br/>";
        }
        if (!CheckEmpty(@$model['id_do_prospek'])) {
            $do_prospek = $this->m_do_prospek->GetOneDoProspek(@$model['id_do_prospek']);
            if ($prospek) {
                $sisa += $prospek->qty_do;
            }
        }
        // if($sisa< DefaultCurrencyDatabase(count(@$model['id_serial_unit'])))
        // {
        //     $message.="QTY KPU tidak cukup untuk prospek ini<br/>";
        // }
        $model['dataset'] = $dataset;

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_do_prospek->DO_ProspekManipulate($model);
            echo json_encode($status);
        }
    }

    public function prospek_save_do() {
        $message = '';

        $tgl_do = $this->input->post('tgl_do');
        foreach ($tgl_do as $key => $value) {
            $data[] = array(
                'id_prospek' => $_POST['idprospek'],
                'tgl_do' => $_POST['tgl_do'][$key],
                'no_do' => $_POST['no_do'][$key],
                'manufacture_no' => $_POST['manufacture_no'][$key],
                'engine_no' => $_POST['engine_no'][$key],
                'created_by' => GetUserId(),
                'created_date' => GetDateNow(),
            );
        }
        $this->db->insert_batch('dgmi_do', $data);
        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function prospek_edit_do() {
        $message = '';
        $this->db->where('id_prospek', $_POST['idprospek']);
        $this->db->delete('dgmi_do');

        if ($this->input->post('tgl_do')) {
            $tgl_do = $this->input->post('tgl_do');
            foreach ($tgl_do as $key => $value) {
                $data[] = array(
                    'id_prospek' => $_POST['idprospek'],
                    'tgl_do' => $_POST['tgl_do'][$key],
                    'no_do' => $_POST['no_do'][$key],
                    'manufacture_no' => $_POST['manufacture_no'][$key],
                    'engine_no' => $_POST['engine_no'][$key],
                    'created_by' => GetUserId(),
                    'created_date' => GetDateNow(),
                );
            }
            $this->db->insert_batch('dgmi_do', $data);
        }

        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function edit_do_prospek($id = 0) {
        $row = $this->m_do_prospek->GetOneDoProspek($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K077";
            $header = "K059";
            $row['title'] = "Edit Kpu";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->model("customer/m_customer");
            $this->load->model("supplier/m_supplier");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $this->load->model("ekspedisi/m_ekspedisi");
            $row['title'] = "Update DO Prospek";
            $row['type_pembayaran'] = 2;
            $row['free_parking'] = AddDays(GetDateNow(), "5 days");
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            
            $row['list_ekspedisi'] = $this->m_ekspedisi->GetDropDownEkspedisi();
            $row['list_pool'] = $this->m_pool->GetDropDownPool();
            $params_prospek['id_kategori'] = $row['id_kategori'];
            $params_prospek['id_unit'] = $row['id_unit'];
            $params_prospek['id_type_unit'] = $row['id_type_unit'];
            $params_prospek['id_customer'] = $row['id_customer'];
            $row['list_prospek'] = $this->m_prospek->GetDropDownProspekForBukaJual($params_prospek);
            $params_buka_jual['id_prospek'] = $row['id_prospek'];
            $row['list_buka_jual'] = $this->m_buka_jual->GetDropDownBukaJualFromCustomer($params_buka_jual);
            $row['list_unit_serial'] = $this->m_unit->GetListUnitSerial($row['type_do'], $row['id_buka_jual'], $row['id_customer']);
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            
            foreach($row['list_detail'] as  $key=>$sat)
            {
                $row['list_detail'][$key]['kolom1']="";
                $row['list_detail'][$key]['kolom2']="";
            }
            $listdetail= array_column($row['list_detail'], "id_unit_serial");
            
            foreach($row['list_unit_serial'] as $det)
            {
                if(!in_array($det['id_unit_serial'], $listdetail))
                {
                    if(CheckEmpty($det['id_do_prospek_detail']))
                    $row['list_detail'][] = $det;
                }
                
            }
            
            
            
            
            
            $row['list_type_do'] = array(array('id' => 1, 'text' => 'DO Masuk'), array('id' => 2, 'text' => 'DO Keluar'), array('id' => 3, 'text' => 'DO Customer'));

            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
            $css[] = 'assets/plugins/iCheck/all.css';
            $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
            $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
            // echo "<pre>";
            // print_r($row);exit;
            LoadTemplate($row, 'do_prospek/v_do_prospek_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Kpu cannot be found in database");
            redirect(site_url('prospek'));
        }
    }

    public function edit_prospek($id = 0) {
        $row = $this->m_prospek->GetOneKpu($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K060";
            $header = "K059";
            $row['title'] = "Edit Kpu";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $jenis_pembayaran = array();
            $jenis_pembayaran[1] = "Tunai";
            $jenis_pembayaran[2] = "Kredit";
            $row['list_type_pembayaran'] = $jenis_pembayaran;
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $this->load->model("customer/m_customer");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_type_ppn'] = $list_type_ppn;
            $this->load->model("vrf/m_vrf");
            $vrf = $this->m_vrf->GetOneVrf($row['id_vrf']);
            $row['qty_sisa'] = $vrf->qty - $vrf->qty_total + $row['qty'];
            $list_type_pembulatan = array();
            $list_type_pembulatan[1] = "Sebelum PPN";
            $list_type_pembulatan[2] = "Sesudah PPN";
            $row['list_type_pembulatan'] = $list_type_pembulatan;
            $row['list_vrf'] = $this->m_vrf->GetDropDownVrf();
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_model'] = $this->m_model->GetDropDownModel();

            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'prospek/v_prospek_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Kpu cannot be found in database");
            redirect(site_url('prospek'));
        }
    }

    public function do_prospek_batal() {
        $message = '';
        $this->form_validation->set_rules('id_do_prospek', 'Prospek', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_do_prospek->DoProspekBatal($model['id_do_prospek']);
        }

        echo json_encode($result);
    }

    public function proses_do_prospek() {
        $id_do_prospek = $this->input->get('id_do_prospek');
        $row = ['button' => 'Add'];
        $javascript = array();
        $css = array();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $row['id_do_prospek'] = $id_do_prospek;
        $row['tanggal_proses'] = date('Y-m-d h:i:s');
        $this->load->view('do_prospek/v_proses_do_prospek', $row);
    }

    public function save_proses_do_prospek() {
        $model = $this->input->post();
        $result = $this->m_do_prospek->SaveProses($model);

        echo json_encode($result);
    }

    public function selesai_do_prospek() {
        $id_do_prospek = $this->input->get('id_do_prospek');
        $row = ['button' => 'Add'];
        $javascript = array();
        $css = array();
        $javascript[] = "assets/plugins/select2/select2.js";
        $javascript[] = "assets/plugins/dropzone/dropzone.min.js";
        $css[] = "assets/plugins/dropzone/dropzone.min.css";
        $css[] = "assets/plugins/select2/select2.css";
        $row['id_do_prospek'] = $id_do_prospek;
        $row['tanggal_selesai'] = date('Y-m-d h:i:s');
        $this->load->view('do_prospek/v_selesai_do_prospek', $row);
    }

    public function save_selesai_do_prospek() {
        $model = $this->input->post();
        $result = $this->m_do_prospek->SelesaiProses($model);

        echo json_encode($result);
    }

    function upload_foto() {

        $config['upload_path'] = FCPATH . '/upload/do-prospek/';
        $config['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $token = $this->input->post('token_foto');
            $id_do_prospek = $this->input->post('id_do_prospek');
            $nama = $this->upload->data('file_name');
            $file_path = FCPATH . 'upload/do-prospek/';
            // $image_path = FCPATH . 'upload/upload-foto/';
            $image_path = base_url('upload/do-prospek');
            $created_at = date('Y-m-d h:i:s');
            $this->db->insert('#_foto_do_prospek', array('id_do_prospek' => $id_do_prospek, 'filename' => $nama, 'file_path' => $file_path, 'image_path' => $image_path, 'created_at' => $created_at, 'token' => $token));
        }
    }

    function remove_foto() {
        $token = $this->input->post('token');
        $foto = $this->db->get_where('#_foto_do_prospek', array('token' => $token));

        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->filename;
            $st = false;
            if (file_exists($file = FCPATH . 'upload/do-prospek/' . $nama_foto)) {
                unlink($file);
                $del = $this->db->delete('#_foto_do_prospek', array('token' => $token));
                if ($del) {
                    $st = true;
                }
            }
        }
        // echo "{}";
        echo json_encode(array('msg' => $file, 'st' => $st));
    }

    function deleteFotoDoProspek() {
        $id = $this->input->post('id');

        $foto = $this->db->get_where('#_foto_do_prospek', array('id' => $id));
        if ($foto->num_rows() > 0) {
            $res = $foto->row();
            $nama_foto = $res->filename;
            $st = false;
            if (file_exists($file = FCPATH . 'upload/do-prospek/' . $nama_foto)) {
                unlink($file);
                $del = $this->db->query("delete from #_foto_do_prospek where id=$id");
                if ($del) {
                    $st = true;
                }
            }
        }
        echo json_encode(array('msg' => $id, 'st' => $st));
    }

    function print_do($id_do_prospek) {
        $CI = get_instance();
        $print = $this->input->get('print');
        $data = $this->m_do_prospek->getPrint($id_do_prospek);
        $data = json_decode(json_encode($data), true);
        $dt['data'] = $data['do_prospek'];
        $dt['data']['print'] = $print;
        $dt['data']['unit'] = $data['unit'];
        $CI->load->view('do_prospek/v_do_prospek_print', $dt);
    }

    public function view_do_prospek($id) {
        $row = $this->m_do_prospek->GetOneDoProspek($id);
        if ($row) {
            $row->button = 'View';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K077";
            $header = "K059";
            $row['title'] = "View DO Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $arr_type_do = array('-', 'Masuk', 'Keluar', 'Customer');
            $row['nama_type_do'] = $arr_type_do[$row['type_do']];
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            // echo "<pre>";print_r($row);exit;
            LoadTemplate($row, 'do_prospek/v_do_prospek_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Buka Jual tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

}

/* End of file Kpu.php */