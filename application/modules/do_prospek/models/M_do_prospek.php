<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_do_prospek extends CI_Model {

    public $table = '#_do_prospek';
    public $id = 'id_do_prospek';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    public function GetUnitBelumTerkirim($datatables=false)
    {
        
        if($datatables)
        {
            $this->load->library('datatables');
            $DB=$this->datatables;
        }
        else
        {
            $DB=$this->db;
        }
        
        $DB->from("#_do_kpu");
        $DB->join("#_do_kpu_detail","#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu");
        $DB->join("#_unit_serial","#_unit_serial.id_do_kpu_detail=#_do_kpu_detail.id_do_kpu_detail");
        $DB->join("#_kpu","#_kpu.id_kpu=#_do_kpu.id_kpu","left");
        $DB->join("#_customer","#_customer.id_customer=#_kpu.id_customer","left");
        $DB->join("#_pool poolfrom","poolfrom.id_pool=#_do_kpu.id_pool_from","left");
        $DB->join("#_pool poolto","poolto.id_pool=#_do_kpu.id_pool_to","left");
        $DB->group_by("#_do_kpu.id_do_kpu");
        $DB->select("#_do_kpu.id_do_kpu,nomor_do_kpu,#_do_kpu.free_parking,#_do_kpu.tanggal_do,poolfrom.nama_pool as pool_asal,poolto.nama_pool as pool_tujuan,#_customer.nama_customer,count(#_unit_serial.id_unit_serial) as jumlah_unit");
        $DB->where(" not exists(select id_mutasi from #_mutasi_detail where #_mutasi_detail.id_unit_serial=#_unit_serial.id_unit_serial)");
        $DB->where(array("#_unit_serial.id_do_prospek_detail"=>null));
        if(!$datatables)
        {
             $list=$DB->get()->result();
             return $list;
        }
        else
        {
            $strview = anchor(site_url('mutasi_unit/shortcut_do_prospek/$1'), 'Process', array('class' => 'btn btn-success btn-xs'));
            $this->datatables->add_column('action', $strview, 'id_do_kpu');
            return $this->datatables->generate();
        }
       
        
    }
    

    function DoProspekBatal($id_do_prospek) {
        $doprospek = $this->m_do_prospek->GetOneDoProspek($id_do_prospek);

        $datesekarang = GetDateNow();
        $userid = GetUserId();
        $message = "";
        try {
            $this->db->trans_rollback();
            $this->db->trans_begin();
            if ($doprospek) {

                if ($doprospek->status == "5") {
                    $message .= "Surat Jalan sudah dibatalkan sebelumnya<br/>";
                }
            } else {
                $message .= "Surat Jalan tidak ditemukan dalam database<br/>";
            }
            $this->db->update("#_do_prospek", MergeUpdate(array("status" => 5), $userid), array("id_do_prospek" => $id_do_prospek));
            $this->db->update("#_history_stok_unit", array("deleted_date" => $datesekarang, "deleted_by" => $userid), array("ref_doc" => $doprospek->nomor_do_prospek));
            $this->load->model("mutasi_unit/m_mutasi_unit");
            foreach ($doprospek->list_detail as $detail) {
                $this->db->update("#_unit_serial", array("id_do_prospek_detail" => null), array("id_unit_serial" => $detail->id_unit_serial));
                $this->m_mutasi_unit->SetUnitSerialPool($detail->id_unit_serial);
            }


            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
            } else {
                $this->db->trans_commit();
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $message = $ex->getMessage();
        }

        $st = $message == "";
        if (CheckEmpty($message)) {
            $message .= "Do Prospek Berhasil Dibatalkan";
        }


        return array("st" => $st, "msg" => $message);
    }

    // datatables
    function GetDataDoprospek($params) {
        $isdelete = GetGroupId() == 1;

        $this->load->library('datatables');
        $this->datatables->select('#_do_prospek.*,nomor_buka_jual,#_do_prospek.id_do_prospek as id,#_prospek.no_prospek,tanggal_prospek,nama_unit,nama_warna,nama_customer,type_do,#_do_prospek.pengemudi,type_do,tujuan,#_do_prospek.keterangan,nama_pool');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_do_prospek_detail', '#_do_prospek_detail.id_do_prospek = #_do_prospek.id_do_prospek', 'left');
        $this->datatables->join('#_prospek', '#_prospek.id_prospek = #_do_prospek_detail.id_prospek', 'left');
        $this->datatables->join('#_prospek_detail', '#_prospek.id_prospek = #_prospek_detail.id_prospek', 'left');
        $this->datatables->join('#_buka_jual', '#_buka_jual.id_buka_jual = #_do_prospek.id_buka_jual', 'left');

        $this->datatables->join('#_warna', '#_warna.id_warna = #_prospek_detail.id_warna', 'left');
        $this->datatables->join('#_unit', '#_unit.id_unit = #_do_prospek_detail.id_unit', 'left');
        $this->datatables->join('#_customer', '#_customer.id_customer = #_do_prospek.id_customer', 'left');
        $this->datatables->join('#_pool', '#_pool.id_pool = #_do_prospek.id_pool', 'left');
        $extra = [];
        $where = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $pencariankeyword = "nomor_do_prospek";
            if (@$params['jenis_keyword'] == "no_prospek") {
                $pencariankeyword = "#_prospek.no_prospek";
            } else if (@$params['jenis_keyword'] == "vin_number" || @$params['jenis_keyword'] == "engine_no") {
                if (@$params['jenis_keyword'] == "no_prospek") {
                    $pencariankeyword = "#_prospek.no_prospek";
                } else {
                    $pencariankeyword = @$params['jenis_keyword'];
                }
                $this->datatables->join("#_history_stok_unit", "#_history_stok_unit.id_master_transaction=#_do_prospek.id_do_prospek and #_history_stok_unit.module='do_prospek'");
                $this->datatables->join("#_unit_serial", "#_history_stok_unit.id_unit_serial=#_unit_serial.id_unit_serial");
                $this->datatables->group_by("#_do_prospek.id_do_prospek");
            }
			
			
            
            if($pencariankeyword=="engine_no"||$pencariankeyword=="vin_number")
            {
                $this->datatables->like($pencariankeyword, strtolower($params['keyword']), 'both'); 
            }
            else
            {
                  $where["lower(".$pencariankeyword.")"] = trim(strtolower($params['keyword']));
            }
       
        } else {
            $tanggal = "date(#_do_prospek.tanggal_do)";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_do_prospek.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_do_prospek.updated_date)";
            }
            else if ($params['jenis_pencarian'] == "tanggal_prospek") {
                $tanggal = "#_prospek.tanggal_prospek";
            }
            else if ($params['jenis_pencarian'] == "tanggal_buka_jual") {
                $tanggal = "#_buka_jual.tanggal_buka_jual";
            }
            $this->db->order_by($tanggal, "desc");


             if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                if ($params['status'] == "6") {
                    $this->db->group_start();
                    $this->datatables->where("#_do_prospek.status", 0);
                    $this->datatables->or_where("#_do_prospek.status", 7);
                    $this->db->group_end();
                } else {
                    $where['#_do_prospek.status'] = $params['status'];
                }
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_buka_jual.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        $isedit = true;
        $straction = '';
        $strview = '';
        $stredit = '';
        $strapproved = '';
        $strrejected = '';
        $strprocess = '';
        $strselesai = '';
        $strbatal = '';
        $strprint = '';
        if ($isedit) {
            // $straction .= anchor(site_url('prospek/edit_prospek/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalprospek($1);return false;"));
        }
        $strview .= anchor(site_url('do_prospek/view_do_prospek/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $stredit .= anchor(site_url('do_prospek/edit_do_prospek/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
            $strapproved .= anchor("", 'Approve', array('class' => 'btn btn-success btn-xs', "onclick" => "ubahstatus($1,1);return false;"));
            $strrejected .= anchor("", 'Reject', array('class' => 'btn btn-warning btn-xs', "onclick" => "ubahstatus($1,2);return false;"));
            $strprocess .= anchor("", 'Proses', array('class' => 'btn btn-warning btn-xs', "onclick" => "proses($1);return false;"));
            $strselesai .= anchor("", 'Selesai', array('class' => 'btn btn-warning btn-xs', "onclick" => "selesaiDo($1);return false;"));
            // $strprint .= anchor("", 'Cetak', array('class' => 'btn btn-print btn-xs', "onclick" => "printDo($1);return false;"));
            $strprint .= anchor(site_url('do_prospek/print_do/$1'), 'Cetak', array('class' => 'btn btn-info btn-print btn-xs', 'target' => '_blank'));
            $strprint .= anchor("", 'BAST', array('class' => 'btn bg-navy btn-print btn-xs', 'onclick' => 'cetakbast($1);return false;'));
            $strprint .= anchor("", 'Surat Perintah', array('class' => 'btn bg-blue btn-print btn-xs', 'onclick' => 'cetaksuratperintah($1);return false;'));
        }
        if ($isdelete) {
            $strbatal .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "bataldoprospek($1);return false;"));
        }
        $this->datatables->add_column('edit', $stredit, 'id');
        $this->datatables->add_column('view', $strview, 'id');
        $this->datatables->add_column('batal', $strbatal, 'id');
        $this->datatables->add_column('rejected', $strrejected, 'id');
        $this->datatables->add_column('approved', $strapproved, 'id');
        $this->datatables->add_column('process', $strprocess, 'id');
        $this->datatables->add_column('selesai', $strselesai, 'id');
        $this->datatables->add_column('cetak', $strprint, 'id');
        return $this->datatables->generate();
    }

    function GetDoProspek_api($params, $limit, $from, $count = false) {
        $this->db->select('#_do_prospek.*,#_do_prospek.id_do_prospek as id,#_prospek.no_prospek,tanggal_prospek,nama_unit,nama_warna,nama_customer');
        // $this->db->from($this->table);
        $this->db->where('#_do_prospek.deleted_date', null);
        //add this line for join
        $this->db->join('#_do_prospek_detail', '#_do_prospek_detail.id_do_prospek = #_do_prospek.id_do_prospek', 'left');
        $this->db->join('#_prospek', '#_prospek.id_prospek = #_do_prospek_detail.id_prospek', 'left');
        $this->db->join('#_prospek_detail', '#_prospek.id_prospek = #_prospek_detail.id_prospek', 'left');
        $this->db->join('#_warna', '#_warna.id_warna = #_prospek_detail.id_warna', 'left');
        $this->db->join('#_unit', '#_unit.id_unit = #_do_prospek_detail.id_unit', 'left');
        $this->db->join('#_customer', '#_customer.id_customer = #_do_prospek.id_customer', 'left');
        $where = [];
        $extra = [];
        $strview = '';
        $strbatal = '';
        $strapproved = '';
        $strrejected = '';
        $stredit = '';
        $strclosed = '';
        if ($params['keyword'] != '') {
            // $where['#_prospek.no_prospek'] = $params['keyword'];
            $this->db->like('#_do_prospek.nomor_do_prospek', $params['keyword']);
            $this->db->or_like('nama_customer', $params['keyword']);
        } else {
            // if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            //     array_push($extra, "#_prospek.tanggal_prospek BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            //     unset($params['start_date'], $params['end_date']);
            // } else {
            //     if (isset($params['start_date']) && empty($params['end_date'])) {
            //         $where["#_prospek.tanggal_prospek"] = DefaultTanggalDatabase($params['start_date']);
            //         unset($params['start_date']);
            //     } else {
            //         $where["#_prospek.tanggal_prospek"] = DefaultTanggalDatabase($params['end_date']);
            //         unset($params['end_date']);
            //     }
            // }


            if (!CheckEmpty($params['status'])) {
                $where['#_do_prospek.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_do_prospek.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $this->db->where($where);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }

        if ($count) {
            return $this->db->get($this->table)->result();
        } else {
            $this->db->limit($limit, $from);
            return $this->db->get($this->table)->result();
        }
    }

    function ChangeStatus($id_do_prospek, $status) {
        try {
            $message = "";
            // $do_prospek = $this->GetOneDoProspek($id_do_prospek);
            // $do_prospek = json_decode(json_encode($do_prospek), true);
            // $this->load->model("customer/m_customer");
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $array = array("st" => true, "msg" => "Data DO Prospek Berhasil " . ($status == "1" ? "Diapprove" : ($status == "2" ? "Direject" : "Diclose")));
            $update = array();
            $update['status'] = $status;
            $update['approved_date'] = GetDateNow();
            $update['approved_by'] = GetUserId();
            $update['updated_by'] = GetUserId();
            $update['updated_date'] = GetDateNow();

            $this->db->update("#_do_prospek", $update, array("id_do_prospek" => $id_do_prospek));
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data DO Prospek Sudah di" . (CheckEmpty(@$model['id_do_prospek']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                // SetPrint($id_prospek, $do_prospek['nomor_do_prospek'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetOneDoProspek($keyword, $type = '#_do_prospek.id_do_prospek', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        if ($isfull) {
            // $this->db->select('#_do_prospek.*,nama_supplier,vrf_code,nama_unit,nama_type_unit,nama_kategori,#_vrf.qty as qty_total');
            $this->db->select('#_do_prospek.*,nama_customer,#_do_prospek_detail.id_do_prospek_detail,nama_pool,no_prospek,nama_unit,nama_type_unit,nama_kategori, nama_user as approved_name, nama_pool, nomor_buka_jual, #_unit.id_kategori, #_buka_jual.id_prospek');
            $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_do_prospek.id_buka_jual", "left");
            $this->db->join("#_do_prospek_detail", "#_do_prospek_detail.id_do_prospek=#_do_prospek.id_do_prospek", "left");
            $this->db->join('#_prospek', '#_prospek.id_prospek = #_buka_jual.id_prospek', 'left');
            $this->db->join('#_unit', '#_unit.id_unit = #_do_prospek_detail.id_unit', 'left');
            // $this->db->join('#_supplier', '#_prospek.id_supplier = #_vrf.id_supplier','left');
            $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
            $this->db->join("#_customer", "#_customer.id_customer=#_do_prospek.id_customer", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
            $this->db->join("#_user", "#_user.id_user=#_do_prospek.approved_by", "left");
            $this->db->join("#_pool", "#_pool.id_pool=#_do_prospek.id_pool", "left");
        }
        $do_prospek = $this->db->get($this->table)->row();
                       
        if ($isfull && $do_prospek) {
            $this->db->select("#_unit_serial.*,#_history_stok_unit.kolom1,#_history_stok_unit.kolom2,#_unit.kode_unit,#_history_stok_unit.keterangan_bawah,id_history_stok_unit, nama_unit, nama_kategori, nama_type_unit,#_customer.nama_customer,#_warna.nama_warna, nama_pool");
            $this->db->from("#_do_prospek_detail");
            $this->db->join("#_history_stok_unit", "#_history_stok_unit.id_detail_transaction = #_do_prospek_detail.id_do_prospek_detail and #_history_stok_unit.module='do_prospek'", "left");
            $this->db->join("#_unit_serial", "#_unit_serial.id_unit_serial = #_history_stok_unit.id_unit_serial", "left");
            $this->db->join("#_warna", "#_warna.id_warna=#_unit_serial.id_warna", "left");
            $this->db->join("#_unit", "#_unit.id_unit = #_unit_serial.id_unit", "left");
            $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
            $this->db->join("#_do_prospek", "#_do_prospek.id_do_prospek=#_do_prospek_detail.id_do_prospek", "left");
            $this->db->join("#_pool", "#_pool.id_pool=#_unit_serial.id_pool", "left");
            $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual", "left");
            $this->db->join("#_customer", "#_customer.id_customer=#_buka_jual.id_customer", "left");
            $this->db->where(array("#_do_prospek_detail.id_do_prospek" => $do_prospek->id_do_prospek));
            $list_detail = $this->db->get()->result();
            $do_prospek->list_detail = $list_detail;
        }
        return $do_prospek;
    }

    function DO_ProspekManipulate($model) {
        try {
            $message = "";

            $this->db->trans_rollback();
            $this->db->trans_begin();
            // $do_prospek['id_prospek'] = ForeignKeyFromDb($model['id_prospek']);
            $do_prospek['id_buka_jual'] = ForeignKeyFromDb($model['id_buka_jual']);
            $do_prospek['id_ekspedisi'] = ForeignKeyFromDb($model['id_ekspedisi']);
            $this->load->model("buka_jual/m_buka_jual");
            $bukajual=$this->m_buka_jual->GetOneBukaJual($model['id_buka_jual']);
            $do_prospek['id_customer'] = ForeignKeyFromDb($model['id_customer']);
            if($bukajual)
            {
                $do_prospek['id_customer'] = $bukajual->id_customer;
            }
            $do_prospek['qty_do'] = count(@$model['id_unit_serial']);
            $do_prospek['keterangan'] = $model['keterangan'];
            $do_prospek['alamat'] = $model['alamat'];
            $do_prospek['up'] = $model['up'];
            $do_prospek['up_pengambilan'] = $model['up_pengambilan'];
            $do_prospek['order_pengambilan'] = $model['order_pengambilan'];
            $do_prospek['alamat_pengambilan'] = $model['alamat_pengambilan'];
            $do_prospek['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $do_prospek['atas_nama'] = $model['atas_nama'];
            $do_prospek['keterangan_kirim'] = $model['keterangan_kirim'];
            $do_prospek['tanggal_do'] = DefaultTanggalDatabase($model['tanggal_do']);
            $this->load->model("prospek/m_prospek");
            $do_prospek['free_parking'] = DefaultTanggalDatabase($model['free_parking']);
            $do_prospek['status'] = 0;
            $do_prospek['order'] = $model['order'];
            $do_prospek['tujuan'] = $model['tujuan'];
            $do_prospek['type_do'] = $model['type_do'];
            $do_prospek['id_pool'] = $model['id_pool'];

            $userid = GetUserId();
            $this->load->model("customer/m_customer");
            $customer = $this->m_customer->GetOneCustomer($do_prospek['id_customer']);

            $id_prospek = 0;
            if (CheckEmpty(@$model['id_do_prospek'])) {
                $do_prospek['created_date'] = GetDateNow();
                $do_prospek['created_by'] = ForeignKeyFromDb(GetUserId());
                $do_prospek['nomor_do_prospek'] = AutoIncrement('#_do_prospek', 'DB/' . date("y") . '/', 'nomor_do_prospek', 5);
                $res = $this->db->insert($this->table, $do_prospek);
                $id_do_prospek = $this->db->insert_id();
            } else {
                $do_prospek['updated_date'] = GetDateNow();
                $do_prospek['updated_by'] = ForeignKeyFromDb(GetUserId());
                $res = $this->db->update($this->table, $do_prospek, array("id_do_prospek" => $model['id_do_prospek']));
                $id_do_prospek = $model['id_do_prospek'];
                $prospekdb = $this->GetOneDoProspek($id_do_prospek, "id_do_prospek", false);
                $do_prospek['nomor_do_prospek'] = $prospekdb->nomor_do_prospek;
            }
            if ($do_prospek['type_do'] == 3) {
                $unitserial['date_do_out'] = $do_prospek['tanggal_do'];
            }
            $do_prospek_detail['id_do_prospek'] = $id_do_prospek;
            $do_prospek_detail['qty_do'] = $do_prospek['qty_do'];
            $do_prospek_detail['created_date'] = GetDateNow();
            $do_prospek_detail['created_by'] = ForeignKeyFromDb(GetUserId());
            $this->db->from("#_do_prospek_detail");
            $this->db->where("id_do_prospek", $id_do_prospek);
            $doProspekDetail = $this->db->get()->row();
            $id_do_prospek_detail = 0;
            if ($doProspekDetail) {
                $this->db->update("#_do_prospek_detail", $do_prospek_detail, array("id_do_prospek_detail" => $doProspekDetail->id_do_prospek_detail));
                $id_do_prospek_detail = $doProspekDetail->id_do_prospek_detail;
            } else {
                $do_prospek_detail['id_prospek'] = ForeignKeyFromDb($model['id_prospek']);
                $do_prospek_detail['id_unit'] = ForeignKeyFromDb(@$model['id_unit']);
                $this->db->insert('#_do_prospek_detail', $do_prospek_detail);
                $id_do_prospek_detail = $this->db->insert_id();
            }
            $in_out = $model['type_do'];
            $this->db->update("#_unit_serial", array('date_do_out' => null), array("id_do_prospek_detail" => $id_do_prospek_detail));
            $historylist = [];
            
            foreach (@$model['id_unit_serial'] as $key => $det) {

                if (!CheckEmpty(@$model['is_checked'][$key])) {
                    $unitserial['updated_date'] = GetDateNow();
                    $unitserial['updated_by'] = GetUserId();
                    $unitserial['id_do_prospek_detail'] = $id_do_prospek_detail;
                    $unitserial['id_pool'] = 3;
                    $this->db->update("#_unit_serial", $unitserial, array("id_unit_serial" => $det));
                    $this->db->from("#_history_stok_unit");
                    $this->db->where(array("id_unit_serial" => $det, "module" => "do_prospek", "ref_doc" => $do_prospek['nomor_do_prospek']));
                    $history_stok_db = $this->db->get()->row();
                    $data['id_unit_serial'] = $det;
                    $data['ref_doc'] = $do_prospek['nomor_do_prospek'];
                    $data['id_pool'] = 3;
                    $data['tanggal'] = $do_prospek['tanggal_do'];
                    $data['keterangan'] = "DO Customer";
                    $data['id_master_transaction'] = $id_do_prospek;
                    $data['id_detail_transaction'] = $id_do_prospek_detail;
                    $data['module'] = "do_prospek";
                    $data['kolom1'] = @$model['kolom1'][$key];
                    $data['kolom2'] = @$model['kolom2'][$key];
                    $data['keterangan_bawah'] = @$model['keterangan_bawah'][$key];
                    if (CheckEmpty($history_stok_db)) {
                        $data['created_by'] = GetUserId();
                        $data['created_date'] = GetDateNow();
                        $res = $this->db->insert('#_history_stok_unit', $data);
                        $historylist[] = $this->db->insert_id();
                    } else {
                        $data['updated_by'] = GetUserId();
                        $data['updated_date'] = GetDateNow();
                        $this->db->update("#_history_stok_unit", $data, array("id_history_stok_unit" => $history_stok_db->id_history_stok_unit));
                        $historylist[] = $history_stok_db->id_history_stok_unit;
                    }
                }
            }
            $this->db->from("#_history_stok_unit");
            $this->db->where(array("ref_doc" => $do_prospek['nomor_do_prospek']));
            if (count($historylist) > 0) {
                $this->db->where_not_in("id_history_stok_unit", $historylist);
            }
            $this->db->select("id_history_stok_unit,id_unit_serial");
            $resultarr = $this->db->get()->result_array();
            $listid_historydelete = array_column($resultarr, "id_history_stok_unit");
            $listunitserial = array_column($resultarr, "id_unit_serial");


            if (count($listid_historydelete)) {
                $this->db->where_in("id_history_stok_unit", $listid_historydelete);
                $this->db->delete("#_history_stok_unit");
            }

            if (count($listunitserial)) {
                $this->db->where_in("id_unit_serial", $listunitserial);
                $this->db->update("#_unit_serial", array("id_do_prospek_detail" => null));
            }



            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data DO Prospek Sudah di" . (CheckEmpty(@$model['id_do_prospek']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_prospek, $do_prospek['nomor_do_prospek'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownKpu() {
        $listprospek = GetTableData($this->table, 'id_prospek', '', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listprospek;
    }

    function GetDoProspekByVrf($id_vrf) {
        $this->db->select("#_do_prospek.tanggal_do, nama_customer, nomor_master, nama_unit, nama_warna, vin_number, #_unit_serial.vin, engine_no,  #_do_prospek.alamat, pengemudi");
        $this->db->where('#_vrf.id_vrf', $id_vrf);
        $this->db->where('#_vrf.deleted_date', null);
        $this->db->join('#_kpu_detail', '#_kpu_detail.id_vrf = #_vrf.id_vrf', 'left');
        $this->db->join('#_kpu', '#_kpu_detail.id_kpu = #_kpu.id_kpu', 'left');
        $this->db->join('#_do_kpu_detail', '#_do_kpu_detail.id_kpu_detail = #_kpu_detail.id_kpu_detail', 'left');
        $this->db->join('#_do_kpu', '#_do_kpu.id_do_kpu = #_do_kpu_detail.id_do_kpu', 'left');
        $this->db->join('#_unit_serial', '#_do_kpu_detail.id_do_kpu_detail = #_unit_serial.id_do_kpu_detail', 'left');
        $this->db->join('#_do_prospek_detail', '#_unit_serial.id_do_prospek_detail = #_do_prospek_detail.id_do_prospek_detail');
        $this->db->join('#_do_prospek', '#_do_prospek.id_do_prospek = #_do_prospek_detail.id_do_prospek');
        $this->db->join('#_unit', '#_unit.id_unit = #_unit_serial.id_unit');
        $this->db->join('#_prospek', '#_do_prospek_detail.id_prospek = #_prospek.id_prospek');
        $this->db->join('#_prospek_detail', '#_prospek_detail.id_prospek = #_prospek.id_prospek');
        $this->db->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna', 'left');
        $this->db->join('#_customer', '#_prospek.id_customer = #_customer.id_customer', 'left');
        $grid_do_kpu = $this->db->get('#_vrf')->result_array();
        return $grid_do_kpu;
    }

    function SaveProses($model) {
        try {
            $do_prospek['pengemudi'] = $model['pengemudi'];
            $do_prospek['keterangan_proses'] = $model['keterangan_proses'];
            $do_prospek['tanggal_proses'] = $model['tanggal_proses'];
            $res = $this->db->update($this->table, $do_prospek, array("id_do_prospek" => $model['id_do_prospek']));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Berhasil memproses DO Prospek");
    }

    function SelesaiProses($model) {
        try {
            $do_prospek['keterangan_selesai'] = $model['keterangan_selesai'];
            $do_prospek['tanggal_selesai'] = $model['tanggal_selesai'];
            $res = $this->db->update($this->table, $do_prospek, array("id_do_prospek" => $model['id_do_prospek']));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "DO Prospek Selesai");
    }

    function getPrint($id_do_prospek) {
        $this->db->where('#_do_prospek.id_do_prospek', $id_do_prospek);
        $this->db->join('#_customer', '#_customer.id_customer=#_do_prospek.id_customer', 'left');
        $this->db->join('#_buka_jual', '#_buka_jual.id_buka_jual=#_do_prospek.id_buka_jual', 'left');
        $this->db->join('#_prospek', '#_prospek.id_prospek=#_buka_jual.id_prospek', 'left');
        $this->db->join('#_ekspedisi', '#_do_prospek.id_ekspedisi=#_ekspedisi.id_ekspedisi', 'left');
        $this->db->select('#_do_prospek.*, #_ekspedisi.nama_ekspedisi,nama_customer, #_do_prospek.alamat, #_customer.contact_person, no_prospek, nomor_buka_jual, cp_ekspedisi');
        $result = $this->db->get('#_do_prospek')->row();

        $this->db->where(array("#_history_stok_unit.id_master_transaction" => $id_do_prospek, "module" => "do_prospek"));
        $this->db->join('#_unit_serial', '#_history_stok_unit.id_unit_serial=#_unit_serial.id_unit_serial', 'left');
        $this->db->join('#_unit', '#_unit.id_unit=#_unit_serial.id_unit', 'left');
        $this->db->join('#_warna', '#_warna.id_warna=#_unit_serial.id_warna', 'left');
        $this->db->join('#_type_body', '#_type_body.id_type_body=#_unit.id_type_body', 'left');
        $this->db->join('#_type_unit', '#_type_unit.id_type_unit=#_unit.id_type_unit', 'left');
        $this->db->join('#_bast', '#_bast.id_unit_serial=#_unit_serial.id_unit_serial', 'left');
        $this->db->join('#_pool', '#_pool.id_pool=#_unit_serial.id_pool', 'left');
        $result2 = $this->db->get('#_history_stok_unit')->result();

        return array('do_prospek' => $result, 'unit' => $result2);
    }

    function GetDropDownDoProspekForRetur($params) {
        $where = array('#_do_prospek.status' => 1, $this->table . '.deleted_date' => null);

        if (!CheckEmpty(@$params['id_kategori']) || @$params['id_kategori'] > 0) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_unit']) || @$params['id_unit'] > 0) {
            $where['#_do_prospek_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit']) || @$params['id_type_unit'] > 0) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer']) || @$params['id_customer'] > 0) {
            $where['#_do_prospek.id_customer'] = $params['id_customer'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_do_prospek_detail", "condition" => "#_do_prospek_detail.id_do_prospek=#_do_prospek.id_do_prospek");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_do_prospek_detail.id_unit");
        $select = 'DATE_FORMAT(#_do_prospek.tanggal_do, "%d %M %Y") as do_prospek_date_convert,nomor_do_prospek,nama_customer,nama_unit,#_do_prospek.id_do_prospek';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_do_prospek.id_customer");
        $listdoprospek = GetTableData($this->table, 'id_do_prospek', array('nomor_do_prospek', "nama_customer", "nama_unit", "do_prospek_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listdoprospek;
    }

}
