<div class="modal-content">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
    <style>
        .tdnoborder{
            border:none !important;
        }
        .tdnoborder td{
            border:none !important;
        }
        .tg  {border-collapse:collapse;border-spacing:0;border-color:#000;width: 100%;}
        .tg td{font-family:Arial, sans-serif;font-size:15px;padding:3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000;color:#333;background-color:#fff;}
        .tg th{font-family:Arial, sans-serif;font-size:15px;font-weight:normal;padding:3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000;color:#333;background-color:#f0f0f0;}
        .tg .tg-lqy6{text-align:right;vertical-align:top}
        .tg .tg-yw4l{vertical-align:top}
        .tg .tg-amwm{font-weight:bold;text-align:center;vertical-align:top}
        .top-ver{vertical-align:top}
        .modal-body{font-family:Arial, sans-serif;font-size:13px;padding:3px;overflow:hidden;word-break:normal;color:#333;background-color:#fff;}
        @page { margin: 0 }
        @media print{@page {size: landscape}}
        body { margin: 0 }
        .sheet {
            margin: 0;
            overflow: hidden;
            position: relative;
            page-break-after: always;
        }
        .modal-content{
            border:none !Important;
            box-shadow:none !important;
        }
        /** Paper sizes **/
        body.A3           .sheet { width: 297mm; height: 419mm }
        body.A3.landscape .sheet { width: 420mm; height: 296mm }
        body.A4           .sheet { width: 210mm; height: 296mm }
        body.A4.landscape .sheet { width: 297mm; height: 209mm }
        body.A5           .sheet { width: 148mm; height: 209mm }
        body.A5.landscape .sheet { width: 210mm; height: 147mm }

        /** Padding area **/
        .sheet.padding-10mm { padding: 5mm; padding-top:40px !important;padding-bottom:0px !Important; }
        .sheet.padding-15mm { padding: 10mm }
        .sheet.padding-20mm { padding: 15mm }
        .sheet.padding-25mm { padding: 20mm }

        /** For screen preview **/
        @media screen {
            .sheet {
                background: white;
                margin: 5mm;
            }
            .footerpage{
            margin-bottom:100px;
        }
        }

        /** Fix for Chrome issue #273306 **/
        @media print {
             .footerpage{position:fixed}
            body.A3.landscape { width: 420mm }
            body.A3, body.A4.landscape { width: 297mm }
            body.A4, body.A5.landscape { width: 210mm }
            body.A5                    { width: 148mm }
            .sheet { page-break-after: always }
        }
    </style>
    <?php
    //echo "<pre>";
// print_r(@$data);exit;
    foreach (@$data['unit'] as $dtu) {
        ?>
        <div class="sheet padding-10mm modal-body">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3 col-xs-3">
                        <div>

                        </div>
                        <div class="top-ver">

                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6" style="vertical-align:top;">
                        <div style="font-weight:bold;font-size:20px;text-align:center;">S U R A T  &nbsp;&nbsp;J A L AN</div>
                        <div style="font-weight:bold;font-size:13px;">Kepada Yth : <?php echo @$data['nama_customer'] ?></div>
                        <div><?php echo @$data['alamat'] ?></div>
                        <div>Up : <?php echo @$data['up'] ?></div>
                    </div>
                    <div class="col-sm-3 col-xs-3">
                        <div>Nomor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo @$data['nomor_do_prospek'] ?></div>
                        <div>Tanggal &nbsp;&nbsp;: <?php echo @$data['tanggal_do'] ?></div>
                        <div>Gudang &nbsp;&nbsp;: <?php echo @$dtu['nama_pool'] ?></div>
                        <div>KM Awal &nbsp;: <?php echo @$dtu['km'] ?></div>
                        <div>KM Akhir &nbsp;: </div>
                        <div>Order &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo @$data['order'] ?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="height:20px;"></div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <p>
                            Bersama ini kami serahkan 1 (satu) unit kendaraan dengan spesifikasi sebagai berikut :
                        </p>
                    </div>
                </div>
                <div class="row">
                    <section>
                        <table class="tg">
                            <tbody>
                                <tr>
                                    <td colspan="3"></td>
                                    <td colspan="2" style="font-size:12px;">SETIAP UNIT KENDARAAN DILENGKAPI DENGAN :</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        Kode Mobil : <?php echo $dtu['kode_unit'] ?><br>
                                        Nama Mobil : <?php echo $dtu['nama_unit'] ?>
                                    </td>
                                    <td rowspan="3">
                                        <?php echo preg_replace("/[\n]/", "<br/>", $dtu['kolom1']); ?>
                                    </td>
                                    <td rowspan="3">
                                        <?php echo preg_replace("/[\n]/", "<br/>", $dtu['kolom2']); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nomor Rangka</td>
                                    <td>Nomor Mesin</td>
                                    <td>Buku Service</td>
                                </tr>
                                <tr>
                                    <td><?php echo $dtu['vin_number'] ?></td>
                                    <td><?php echo $dtu['engine_no'] ?></td>
                                    <td> - </td>
                                </tr>
                                <tr>
                                <td style="border:none;">&nbsp;</td>
                                <td style="border:none;">&nbsp;</td>
                                <td style="border:none;">&nbsp;</td>
                                <td style="border:none;">&nbsp;</td>
                                <td width="20%" class="top-ver" style="border:solid 1px #000;font-family:arial;;padding:5px;font-weight:bold;font-size:12px;">Keterangan: <br><?php echo @$data['keterangan'] ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </section>
                </div>
            </div>

        </div>
        <table class="footerpage" style="  bottom: 50px;margin-top:20px;border-style:none;width:95%; border-width:0px;font-size:11px;z-index: 0">
            <tbody>
                <tr style="height:70px;">
                    <td width="20%" class="top-ver" style="text-align:center">Diterima Oleh:</td>
                    <td width="20%" class="top-ver" style="text-align:center">Diperiksa Oleh Keamanan:</td>
                    <td width="20%" class="top-ver" style="text-align:center">Disetujui Oleh Logistic:</td>
                    <td width="20%" class="top-ver" style="text-align:center">Dikirim Oleh Ekspedisi:</td>
                </tr>
                <tr style="margin-top:-20px;">
                    <td width="20%" style="text-align:center"><b>(Nama dan Cap)</b></td>
                    <td width="20%" style="text-align:center"><b>(______________)</b></td>
                    <td width="20%" style="text-align:center"><b>(______________)</b></td>
                    <td width="20%" style="text-align:center"><b><u>( <?php echo!CheckEmpty(@$data['nama_ekspedisi']) ? @$data['nama_ekspedisi'] : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" ?> )<u></u></u></b></td>
                </tr>
            </tbody>

        </table>
    <?php } ?>
</div>
<script>
    window.print();
    // window.close();
</script>