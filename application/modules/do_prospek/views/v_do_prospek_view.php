<section class="content-header">
    <h1>
        Surat Jalan Prospek <?= @$button ?>
        <small>Surat Jalan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Surat Jalan</li>
        <li class="active">Surat Jalan Prospek</li>
    </ol>
</section>
<style>
    .printtextarea{
        resize: none;
        height:170px;
        border:none;
        overflow-y: hidden; /* Hide vertical scrollbar */
    }
    .tg td{
        border:solid 1px #000 !Important;
        border-collapse: collapse;
    }
    .selectbox td{
        background-color: #00a65a;
        color:#fff;
    }
    .tg{
        border-collapse: collapse;
    }
    .tdchild td{
        border:none !Important;
    }
    input[type="checkbox"]{
        display:none !Important;
    }
</style>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_prospek" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_do_prospek" value="<?php echo @$id_do_prospek; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Type DO', "type_do", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'type_do', 'value' => @$nama_type_do, 'class' => 'form-control', 'id' => 'txt_tanggal', 'placeholder' => 'Type DO')); ?>
                                    </div>
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal_do), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nomor Buka Jual', "type_do", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'id_buka_jual', 'value' => @$nomor_buka_jual, 'class' => 'form-control', 'id' => 'txt_tanggal', 'placeholder' => 'Nomor Buka Jual')); ?>
                                    </div>
                                </div>
                                <hr/>
                                <?php
                                if (@$type_do == 3) {
                                    ?>
                                    <div class="area-cust">
                                        <div class="form-group">
                                            <?= form_label('Customer', "txt_customer", array("class" => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?php
                                                echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Customer'));
                                                ?>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <?= form_label('Free Parking', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'free_parking', 'value' => DefaultDatePicker(@$free_parking), 'class' => 'form-control datepicker', 'id' => 'txt_free_parking', 'placeholder' => 'Free Parking')); ?>
                                            </div>
                                            <?= form_label('Unit Position', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                            <div class="col-sm-5">
                                                <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'unit_position', 'value' => @$nama_pool, 'class' => 'form-control ', 'id' => 'txt_unit_position', 'placeholder' => 'Unit Position')); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?= form_label('Atas Nama', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'atas_nama', 'value' => @$atas_nama, 'class' => 'form-control ', 'id' => 'txt_atas_nama', 'placeholder' => 'Atas Nama')); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?= form_label('Alamat', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-10">
                                                <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat, 'name' => 'alamat', 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                            </div>

                                        </div>
                                    </div>
                                <?php } ?>
                                <div>
                                    <?php
                                    if (@$type_do == 1) {
                                        ?>
                                        <div class="form-group area-tujuan-in">
                                            <?= form_label('Pool', "txt_tujuan", array("class" => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tujuan', 'value' => @$nama_pool, 'class' => 'form-control ', 'id' => 'txt_tujuan', 'placeholder' => 'Tujuan')); ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    if (@$type_do == 2) {
                                        ?>
                                        <div class="form-group area-tujuan-out">
                                            <?= form_label('Tujuan', "txt_tujuan", array("class" => 'col-sm-2 control-label')); ?>
                                            <div class="col-sm-4">
                                                <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tujuan', 'value' => @$tujuan, 'class' => 'form-control ', 'id' => 'txt_tujuan', 'placeholder' => 'Tujuan')); ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-10">
                                            <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                        </div>

                                    </div>  
                                </div>
                                <hr/>
                                <div id="areado" style="position:relative">

                                    <?php
                                    foreach (@$list_detail as $key => $detail) {
                                        include APPPATH . 'modules/do_prospek/views/v_do_prospek_unit.php';
                                    }
                                    ?>
                                </div>
                                <!-- <div id="areado" style="position:relative">
                                    <div style="float:right;position:absolute;right:0;top:0;z-index: 99 !Important;">
                                <?php //echo anchor("", 'Tambah', 'class="btn btn-success" id="btt_tambah"');  ?>
                                    </div>
                                <?php
                                // foreach (@$list_detail as $key => $detail) {
                                //     include APPPATH . 'modules/buka_jual/views/v_buka_jual_content.php';
                                // }
                                ?>

                                </div>
                                <hr/> -->
                                <div id="areaUnit"></div>

                                <div class="form-group" style="margin-top:50px">
                                    <a href="<?php echo base_url() . 'index.php/do_prospek' ?>" class="btn btn-default"  >Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    function selectUnit(id_unit_serial){
        
    }
    $(document).ready(function () {
        $("input").attr("readonly", "readonly");
        $("textarea").attr("disabled", "disabled");
        $(".select2").select2({disabled: "readonly"});

    });

</script>