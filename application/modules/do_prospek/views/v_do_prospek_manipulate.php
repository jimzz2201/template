<style>
    .printtextarea{
        resize: none;
        height:170px;
        border:none;
        overflow-y: hidden; /* Hide vertical scrollbar */
    }
    .tg td{
        border:solid 1px #000 !Important;
        border-collapse: collapse;
    }
    .selectbox td{
        background-color: #00a65a;
        color:#fff;
    }
    .tg{
        border-collapse: collapse;
    }
    .tdchild td{
        border:none !Important;
    }
</style>
<section class="content-header">
    <h1>
        Surat Jalan Prospek <?= @$button ?>
        <small>Surat Jalan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Surat Jalan</li>
        <li class="active">Surat Jalan Prospek</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_do_prospek" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_do_prospek" value="<?php echo @$id_do_prospek; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Type DO', "type_do", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "type_do"), @$list_type_do, @$type_do, array('class' => 'form-control select2', 'id' => 'dd_type_do')); ?>
                                    </div>
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal_do', 'value' => DefaultDatePicker(@$tanggal_do), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Free Parking', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'free_parking', 'value' => DefaultDatePicker(@$free_parking), 'class' => 'form-control datepicker', 'id' => 'txt_free_parking', 'placeholder' => 'Free Parking')); ?>
                                    </div>
                                    <?= form_label('Unit Position', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("name" => "id_pool"), @$list_pool, @$id_pool, array('class' => 'form-control select2', 'id' => 'dd_id_pool')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?php
                                        if (@$id_do_prospek)
                                            echo form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'));
                                        else
                                            echo form_textarea(array('type' => 'text', "rows" => 4, 'value' => "Unit Tidak Boleh Keluar Dari Karoseri Tanpa Persetujuan DGMI", 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'));
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Suppplier', "dd_id_ekspedisi", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_supplier', 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier); ?>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <?= form_label('UP', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$up_pengambilan, 'name' => 'up_pengambilan', 'class' => 'form-control', 'id' => 'txt_up_pengambilan', 'placeholder' => 'UP')); ?>
                                    </div>
                                    <?= form_label('Order', "txt_alamat_kirim", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'value' => @$order_pengambilan, 'name' => 'order_pengambilan', 'class' => 'form-control', 'id' => 'txt_order_ket_pengambilan', 'placeholder' => 'Order')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-block btn-success" id="btt_search_address_pengambilan" >Pencarian  Alamat</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat Pengambilan', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat_pengambilan, 'name' => 'alamat_pengambilan', 'class' => 'form-control', 'id' => 'txt_alamat_kirim_pengambilan', 'placeholder' => 'Alamat Pengambilan')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Ekspedisi', "dd_id_ekspedisi", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_ekspedisi', 'class' => 'form-control select2', 'id' => 'dd_id_ekspedisi', 'placeholder' => 'Ekspedisi'), DefaultEmptyDropdown(@$list_ekspedisi, "json", "Ekspedisi"), @$id_ekspedisi); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('UP', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$up, 'name' => 'up', 'class' => 'form-control', 'id' => 'txt_up', 'placeholder' => 'UP')); ?>
                                    </div>
                                    <?= form_label('Order', "txt_alamat_kirim", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'value' => @$order, 'name' => 'order', 'class' => 'form-control', 'id' => 'txt_order_ket', 'placeholder' => 'Order')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-block btn-success" id="btt_search_address" >Pencarian  Alamat</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat Kirim', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat, 'name' => 'alamat', 'class' => 'form-control', 'id' => 'txt_alamat_kirim', 'placeholder' => 'Alamat Kirim')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan Kirim', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan_kirim, 'name' => 'keterangan_kirim', 'class' => 'form-control', 'id' => 'txt_keterangan_kirim', 'placeholder' => 'Keterangan Kirim')); ?>
                                    </div>
                                </div>
                               
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Type Unit', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori_search', 'class' => 'form-control select2', 'id' => 'dd_id_kategori_search', 'placeholder' => 'Kategori'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$id_kategori); ?>
                                    </div>
                                    <?= form_label('Unit', "dd_id_unit_search", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit_search', 'class' => 'form-control select2', 'id' => 'dd_id_unit_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                                    </div>
                                    <?= form_label('Customer', "dd_id_customer_search", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', "name" => "id_customer", 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer); ?>
                                    </div>
                                </div>
                                <?php if (CheckEmpty(@$id_do_prospek)) { ?>
                                    <div class="form-group">
                                        <?= form_label('Prospek', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-6">
                                            <?php
                                            // if (!CheckEmpty(@$id_prospek)) {
                                            //     echo form_input(array('type' => 'hidden', 'value' => @$id_prospek, 'name' => 'id_prospek'));
                                            //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_prospek), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Prospek'));
                                            // } else {
                                            echo form_dropdown(array('type' => 'text', 'name' => 'id_prospek', 'class' => 'form-control select2', 'id' => 'dd_id_prospek', 'placeholder' => 'Prospek'), DefaultEmptyDropdown(@$list_prospek, "json", "Prospek"), @$id_prospek);
                                            // }
                                            ?>
                                        </div>
                                    </div>

                                <?php } ?>

                                <div class="form-group">
                                    <?= form_label('Buka Jual', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        // if (!CheckEmpty(@$id_buka_jual)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_buka_jual, 'name' => 'id_buka_jual'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_buka_jual), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Buka Jual'));
                                        // } else {
                                        if (CheckEmpty(@$id_do_prospek)) {
                                            $list_buka_jual = DefaultEmptyDropdown(@$list_buka_jual, "json", "Buka Jual");
                                        }

                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_buka_jual', 'class' => 'form-control select2', 'id' => 'dd_id_buka_jual', 'placeholder' => 'Buka Jual'), @$list_buka_jual, @$id_buka_jual);
                                        echo form_input(array('type' => 'hidden', 'name' => 'id_customer', 'value' => @$id_customer, 'class' => 'form-control', 'id' => 'id_cust'));
                                        // }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?>
                                        <?= form_input(array('type' => 'hidden', 'name' => 'id_unit', 'value' => @$id_unit, 'class' => 'form-control', 'id' => 'txt_id_unit', 'placeholder' => 'id unit')); ?>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>

                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Warna')); ?>


                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Qty Total', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => DefaultCurrency(@$qty_total), 'class' => 'form-control', 'id' => 'txt_qty_total', 'placeholder' => 'Qty Total', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Qty Sisa', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => DefaultCurrency(@$qty_sisa), 'class' => 'form-control', 'id' => 'txt_qty_sisa', 'placeholder' => 'Qty Sisa', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                <div id="areado" style="position:relative">

                                    <?php
                                    foreach (@$list_detail as $key => $detail) {
                                        include APPPATH . 'modules/do_prospek/views/v_do_prospek_unit.php';
                                    }
                                    ?>
                                </div>
                                <div id="serial_unit_list">

                                </div>



                                <div id="areaUnit">

                                </div>
                                <hr/>
                                <?php if (CheckEmpty(@$is_view)) { ?>
                                    <div class="form-group" style="margin-top:50px">
                                        <a style="float:right;" href="<?php echo base_url() . 'index.php/do_prospek' ?>" class="btn btn-default"  >Cancel</a>
                                        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                    </div>
                                <?php } ?>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;
    var jumlahgenerate =<?= count(@$list_detail) ?>;
    $("#btt_search_address").click(function () {
        $.ajax({url: baseurl + 'index.php/module/address_book_search',
            success: function (data) {
                modalbootstrap(data, "Search Address Book", "1024px");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });


    })
    $("#btt_search_address_pengambilan").click(function () {
        $.ajax({url: baseurl + 'index.php/module/address_book_search/_pengambilan',
            success: function (data) {
                modalbootstrap(data, "Search Address Book", "1024px");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });


    })
    $("#btt_tambah").click(function () {
        AppendDetail(true);
    })

    function AppendDetail(isshow)
    {
        if (jumlahgenerate < $("#txt_qty_sisa").val())
        {


            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/do_kpu/kpu_content',
                data: {
                    key: jumlahgenerate,
                    jumlah: $("#txt_qty_sisa").val() - jumlahgenerate
                },
                success: function (data) {
                    jumlahgenerate = $("#txt_qty_sisa").val();
                    $("#areado").append(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);

                }
            });
        } else
        {
            if (isshow)
                alert("Smua Field DO untuk PROSPEK ini sudah terpenuhi");
        }
    }
    $("#btt_ambil_semua").click(function () {

        for (i = jumlahgenerate; i < $("#txt_qty_sisa").val(); i++) {
            AppendDetail(false);
        }


    })
    function RefreshUnit()
    {
        var id_kategori = $("#dd_id_kategori_search").val();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_dropdown_unit',
            dataType: 'json',
            data: {
                id_kategori: id_kategori
            },
            success: function (data) {
                $("#dd_id_unit_search").empty();
                if (data.st)
                {
                    $("#dd_id_unit_search").select2({data: data.list_unit})
                }
                ClearFormDetail();
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });

    }
    function UpdateBukaJual()
    {

        $("#dd_id_buka_jual").change(function () {
            var id_buka_jual = $("#dd_id_buka_jual").val();
            var type_do = $("#dd_type_do").val();
            if (id_buka_jual != 0)
            {
                LoadBar.show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/buka_jual/get_one_buka_jual',
                    dataType: 'json',
                    data: {
                        id_buka_jual: id_buka_jual
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            $("#txt_nama_unit").val(data.obj.nama_unit);
                            $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                            $("#txt_nama_kategori").val(data.obj.nama_kategori);
                            $("#txt_vin").val(data.obj.vin);
                            $("#txt_qty_total").val(Comma(data.obj.qty_total));
                            $("#txt_qty_sisa").val(Comma(data.obj.qty_sisa));
                            $("#txt_nama_warna").val(data.obj.nama_warna);
                            $("#txt_id_unit").val(data.obj.id_unit);
                            $("#id_cust").val(data.obj.id_customer);
                            $("#txt_up").val(data.obj.nama_customer);
                            $("#txt_order_ket").val(data.obj.nama_customer);
                            $("#txt_alamat_kirim").val(data.obj.alamat);
                            $("#areado").html("");
                            jumlahgenerate = 0;
                            // AppendDetail(false);
                            showListUnit(type_do, id_buka_jual, data.obj.id_customer);
                        } else
                        {
                            ClearFormDetail();
                        }
                        LoadBar.hide();
                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                        ClearFormDetail();
                        LoadBar.hide();
                    }
                });
            } else
            {
                ClearFormDetail();
                LoadBar.hide();
            }
        })
    }

    function searchListProspek() {
        LoadBar.show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/do_prospek/get_prospek_list',
            dataType: 'json',
            data: {
                id_kategori: $("#dd_id_kategori_search").val(),
                id_unit: $("#dd_id_unit_search").val(),
                id_customer: $("#dd_id_customer_search").val()
            },
            success: function (data) {
                $("#dd_id_prospek").empty();
                if (data.st)
                {
                    $("#dd_id_prospek").select2({data: data.list_prospek});
                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }

    function ClearFormDetail()
    {
        $("#txt_nama_unit").val("");
        $("#txt_nama_type_unit").val("");
        $("#txt_nama_kategori").val("");
        $("#txt_top").val("");
        $("#txt_vin").val("");
        $("#txt_nama_warna").val("");
        jumlahgenerate = 0;
        $("#areado").html("");

    }

    function RefreshGrid()
    {


    }

    function deleterow(indrow) {
        RefreshGrid();
    }
    $(document).ready(function () {
        $(".icheck").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
<?php if (CheckEmpty(@$id_do_prospek)) { ?>
            $(".select2").select2();
<?php } else { ?>
            // $(".select2").select2({disabled: true});
            $(".select2").select2();
    <?php if (!CheckEmpty(@$is_view)) { ?>
                $("input").attr("readonly", true);
                $("textarea").attr("readonly", true);
    <?php } ?>
<?php } ?>
        $('#dd_id_customer_search').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $("#dd_id_supplier").change(function(){
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/supplier/get_one_supplier',
                dataType: 'json',
                data: {
                    id_supplier: $("#dd_id_supplier").val()
                },
                success: function (data) {
                    if(data.st)
                    {
                        $("#txt_up_pengambilan").val(data.obj.up);
                        $("#txt_alamat_kirim_pengambilan").val(data.obj.alamat);
                        $("#txt_order_ket_pengambilan").val(data.obj.order);
                    }
                    
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        })
        $("#dd_id_unit_search, #dd_id_kategori_search , #dd_id_customer_search").change(function () {
            var id = $(this).attr("id");
            var id_unit_search = $("#dd_id_unit_search").val();
            if (id == "dd_id_kategori_search")
            {
                id_unit_search = 0;
                RefreshUnit();

            }
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/do_prospek/get_prospek_list',
                dataType: 'json',
                data: {
                    id_kategori: $("#dd_id_kategori_search").val(),
                    id_unit: id_unit_search,
                    id_customer: $("#dd_id_customer_search").val()
                },
                success: function (data) {
                    $("#dd_id_prospek").empty();
                    if (data.st)
                    {
                        $("#dd_id_prospek").select2({data: data.list_prospek});
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        })

        $("#dd_id_prospek").change(function () {
            var id = $(this).attr("id");
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/do_prospek/get_buka_jual_list',
                dataType: 'json',
                data: {
                    id_prospek: $(this).val(),
                    // id_kategori: $("#dd_id_kategori_search").val(),
                    // id_unit: id_unit_search,
                    // id_customer: $("#dd_id_customer_search").val()
                },
                success: function (data) {
                    $("#dd_id_buka_jual").empty();
                    if (data.st)
                    {

                        $("#dd_id_buka_jual").select2({data: data.list_buka_jual});
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        })
    })




    function selectUnit(id_unit_serial) {


        var statuscheckbox = !$("#unit" + id_unit_serial).prop('checked');


        var chk = $("#unit" + id_unit_serial).prop('checked', statuscheckbox);
        $("#is_checked" + id_unit_serial).val(statuscheckbox);
        if (statuscheckbox == true) {
            $("#unit" + id_unit_serial).parent().parent().addClass("selectbox");
        } else {
            $("#unit" + id_unit_serial).parent().parent().removeClass("selectbox");
        }
    }



    function showListUnit(type_do, id_buka_jual, id_cust = 0) {

        $.ajax({
            type: 'GET',
            url: baseurl + 'index.php/unit/get_list_unit_serial',
            data: {
                type_do: type_do,
                id_buka_jual: id_buka_jual,
                id_cust: id_cust
            },
            success: function (data) {
                // console.log(data);
                $("#serial_unit_list").html(data);
            }
        })
    }

    $(document).ready(function () {
        $(".datepicker").datepicker();

        UpdateBukaJual();
        searchListProspek();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
    })


    $("#frm_do_prospek").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data DO Prospek berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                // console.log($("#frm_do_prospek").serialize());return false;
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_prospek/do_prospek_manipulate',
                    dataType: 'json',
                    data: $("#frm_do_prospek").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/do_prospek";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })



</script>