<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
<style>
    .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%;}
    .tg td{font-family:Arial, sans-serif;font-size:11px;padding:3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
    .tg th{font-family:Arial, sans-serif;font-size:12px;font-weight:bold;padding:3px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
    .tg .tg-lqy6{text-align:right;vertical-align:top}
    .tg .tg-yw4l{vertical-align:top}
    .tg .tg-amwm{font-weight:bold;text-align:center;vertical-align:top}
    .top-ver{vertical-align:top}
    .modal-content{
        border:none !Important;
        box-shadow:none !important;
    }
    .modal-body{font-family:Arial, sans-serif;font-size:12px;padding:3px;overflow:hidden;word-break:normal;color:#333;background-color:#fff;}
    @page { margin: 0 }
    body { margin: 0 }
    .sheet {
        margin: 0;
        overflow: hidden;
        position: relative;
         page-break-after: always; 
    }

    /** Paper sizes **/
    body.A3           .sheet { width: 297mm; height: 419mm }
    body.A3.landscape .sheet { width: 420mm; height: 296mm }
    body.A4           .sheet { width: 210mm; height: 296mm }
    body.A4.landscape .sheet { width: 297mm; height: 209mm }
    body.A5           .sheet { width: 148mm; height: 209mm }
    body.A5.landscape .sheet { width: 210mm; height: 147mm }

    /** Padding area **/
    .sheet.padding-10mm { padding: 8mm }
    .sheet.padding-15mm { padding: 8mm }
    .sheet.padding-20mm { padding: 18mm }
    .sheet.padding-25mm { padding: 23mm }

    /** For screen preview **/
    @media screen {
        .sheet {
            background: white;
            margin: 5mm;
        }
    }

    /** Fix for Chrome issue #273306 **/
    @media print {
        body.A3.landscape { width: 420mm }
        body.A3, body.A4.landscape { width: 297mm }
        body.A4, body.A5.landscape { width: 210mm }
        body.A5                    { width: 148mm }
        /* .sheet { page-break-after: always } */
    }
    @page { size: A4 }
</style>

<?php foreach (@$list_detail as $detail) { ?>
    <div class="modal-content">
        <?php print_r(@$vin_number); ?>
        <div class="sheet padding-10mm modal-body">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3 col-xs-2">
                        <div>
                            <img src="<?php echo base_url() ?>assets/images/logo_bast.png" style="width:70px">
                        </div>
                    </div>
                    <div class="col-sm-9 col-xs-10" style="vertical-align:top;text-align:left">
                        <div style="font-weight:bold;font-size:15px;">PT. DAYA GUNA MOTOR INDONESIA</div>
                        <div style="font-weight:bold;font-size:10px;">Head Office : </div>
                        <div>Bukit Gading Indah Blok O / 12 Kelapa Gading - Jakarta Utara, Phone : (021) 45842793</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 col-xs-1"></div>
                    <div class="col-sm-10 col-xs-10" style="vertical-align:top;">
                        <div style="font-weight:bold;font-size:15px;text-align:center;">BUKTI PENERIMAAN / PEMERIKSAAN KENDARAAN SURAT JALAN</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" style="height:20px;"></div>
                </div>
                <div class="row">
                    <section>
                        <table class="tg">
                            <tbody><tr>
                                    <td width="15%">NAMA SOPIR</td>
                                    <td> </td>
                                    <td width="15%">MERK</td>
                                    <td><?php echo @$detail->nama_kategori ?></td>
                                </tr>
                                <tr>
                                    <td>PEMILIK D.O</td>
                                    <td> <?php echo @$detail->nama_customer ?></td>
                                    <td>TYPE</td>
                                    <td> <?php echo @$detail->nama_type_unit ?></td>
                                </tr>
                                <tr>
                                    <td>NO D.O</td>
                                    <td> <?php echo @$nomor_do_prospek?></td>
                                    <td>NO CHASIS</td>
                                    <td> <?php echo @$detail->vin_number ?></td>
                                </tr>
                                <tr>
                                    <td>DARI POOL</td>
                                    <td> <?php echo @$detail->nama_pool_asal ?></td>
                                    <td>NO ENGINE</td>
                                    <td> <?php echo @$detail->engine_no ?></td>
                                </tr>
                                <tr>
                                    <td>TUJUAN</td>
                                    <td> <?php echo @$detail->nama_pool ?></td>
                                    <td>WARNA</td>
                                    <td> <?php echo @$detail->nama_warna ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </section>
                    <section>
                        <div class="row">
                            <div class="col-sm-6 col-xs-6">
                                <table class="tg">
                                    <tbody><tr>
                                            <th>NO</th>
                                            <th>NAMA BARANG</th>
                                            <th colspan="2" style="text-align:center">KET (I)</th>
                                            <th colspan="2" style="text-align:center">KET (II)</th>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>LAMPU CABIN DALAM</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>LAMPU BESAR DEPAN L.C.R</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>LAMPU SIGN KIRI / KANAN</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>LAMPU KABUT KUNING</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>LAMPU SIGN BELAKANG</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>LAMPU PARKIR</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>LAMPU PLAT NO. POLIS</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>ALARM PARKIR</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>KARET INJAKAN KAKI</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>KLAKSON</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>WIPER</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>KACA SPION CABIN DALAM</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>KACA SPION LUAR DEPAN</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>TUTUP TANKI SOLAR / BENSIN</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>TUTUP RADIATOR</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>TUTUP OLI MESIN</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>TUTUP ACCU</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>ACCU / BATERAI</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>BAN SEREP</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>KUNCI ( ...... ) BUAH</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>RADIO TAPE</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>DOP RODA</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>AIR CONDITIONER</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-6 col-xs-6">
                                <table class="tg">
                                    <tbody><tr>
                                            <th>NO</th>
                                            <th>NAMA BARANG</th>
                                            <th colspan="2" style="text-align:center">KET (I)</th>
                                            <th colspan="2" style="text-align:center">KET (II)</th>
                                        </tr>
                                        <tr>
                                            <td>24</td>
                                            <td>KARPET DALAM</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>25</td>
                                            <td>DONGKRAK + GAGANG</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>26</td>
                                            <td>KUNCI RODA + GAGANG</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>27</td>
                                            <td>ENGKOL BAN</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>28</td>
                                            <td>SEGITIGA PENGAMAN</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>29</td>
                                            <td>BUKU MANUAL</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>30</td>
                                            <td>BUKU SERVICE</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>31</td>
                                            <td>CAT KALENG KECIL</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>32</td>
                                            <td>LIGHTER</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>33</td>
                                            <td>KUNCI BUSI ( ...... ) PS</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>34</td>
                                            <td>PLAT NO</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>35</td>
                                            <td>S.T.N.K</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>36</td>
                                            <td>TOOL KIT BOX</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>37</td>
                                            <td>PALU ( ...... ) PS</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>38</td>
                                            <td>TANG ( ...... ) PS</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>39</td>
                                            <td>KUNCI INGGRIS ( ...... ) PS</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>40</td>
                                            <td>OBENG ( ...... ) PS</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>41</td>
                                            <td>KUNCI PAS ( ...... ) PS</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>42</td>
                                            <td>POMPA GEMUK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>43</td>
                                            <td>SLEEPING BED</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>44</td>
                                            <td>ANTENA</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>45</td>
                                            <td>MUD GUARD</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                        <tr>
                                            <td>46</td>
                                            <td></td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                            <td>ADA</td>
                                            <td>TDK</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <table class="tg">
                                    <tbody><tr>
                                            <td>CATATAN</td></tr>
                                        <?php
                                        for ($i = 1; $i <= 10; $i++) {
                                            echo "<tr><td>" . $i . ". </td></tr>";
                                        }
                                        ?>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <table class="tg">
                                    <tbody><tr>
                                            <th style="border-bottom:0px;">
                                                <u>PENGIRIM</u> <br>
                                                TANGGAL KIRIM : <br><br><br><br>
                                            </th>
                                            <th style="border-bottom:0px;"><u>PENERIMA</u> <br>
                                                TANGGAL TERIMA : <br><br><br><br>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th style="text-align:center;border-top:0px;">
                                                ( ......................... ) <br>
                                                TANDA TANGAN & NAMA JELAS
                                            </th>
                                            <th style="text-align:center;border-top:0px;">
                                                ( ......................... ) <br>
                                                TANDA TANGAN & NAMA JELAS
                                            </th>
                                        </tr>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<script>
    // window.print();
    // window.close();
</script>