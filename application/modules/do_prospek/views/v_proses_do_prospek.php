<div class="modal-header">
<?php echo @$title; ?>
    </div>
<div class="modal-body">
<form id="frm_proses_do_prospek" class="form-horizontal form-groups-bordered validate" method="post" autocomplete="off">
	<input type="hidden" name="id_do_prospek" id="id_do_prospek" value="<?php echo @$id_do_prospek; ?>" />
	<div class="form-group">
		<?= form_label('Pengemudi', "txt_pengemudi", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
            <?= form_input(array('type' => 'text', 'name' => 'pengemudi', 'class' => 'form-control', 'id' => 'pengemudi', 'value' => @$pengemudi, 'placeholder' => 'Pengemudi')); ?>
			<?= form_input(array('type' => 'hidden', 'name' => 'tanggal_proses', 'class' => 'form-control', 'id' => 'tanggal_proses', 'value' => @$tanggal_proses, 'placeholder' => 'tanggal_proses')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?= form_textarea(array('type' => 'text', 'rows' => '5', 'cols' => '10', 'class' => 'form-control', 'name' => 'keterangan_proses', 'id' => 'keterangan_proses', 'placeholder' => 'Catatan', 'value' => @$keterangan_proses)); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" ><?php echo $button == 'Update' ? 'Update' : 'Simpan' ?></button>
    </div>

</form>
</div>
<script>
	
	$("#frm_proses_do_prospek").submit(function () {
		// console.log($(this).serialize());
		// return false;
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/do_prospek/save_proses_do_prospek',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                console.log(data);
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
					$("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
 
	.bootstrap-timepicker-widget.dropdown-menu.open {
		display: inline-block;
		z-index: 99999 !important;
	}
	.glyphicon {
		font-size: 20px;
		color: red;
	}
</style>