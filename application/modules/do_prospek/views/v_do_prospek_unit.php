<div class="row">

    <table class="td30 tg tr30 table table-striped table-bordered table-hover dataTable no-footer"  style="width:100%;vertical-align: top;">
        <tbody>
            <tr onclick="selectUnit('<?php echo $detail['id_unit_serial']?>')" class="<?php echo !CheckEmpty(@$detail['id_history_stok_unit'])?"selectbox":""?>">
                <td colspan="3"><input type='checkbox' <?php echo !CheckEmpty(@$detail['id_history_stok_unit'])?"checked":""?> style='width:20px;' class='selectUnit'  id='unit<?php echo $detail['id_unit_serial']?>'>
                <input type='hidden' name='id_unit_serial[]' value='<?php echo $detail['id_unit_serial']?>' id='unitInput<?php echo $detail['id_unit_serial']?>'>
                <input type='hidden' name='is_checked[]' value='<?php echo !CheckEmpty(@$detail['id_history_stok_unit'])?"true":"false"?>' id='is_checked<?php echo $detail['id_unit_serial']?>'>
                <?php echo @$detail['nama_pool']?>
                </td>
                <td colspan="2" style="font-size:12px;">SETIAP UNIT KENDARAAN DILENGKAPI DENGAN :</td>
            </tr>
            <tr>
                <td colspan="3">
                    Kode Mobil : <?php echo $detail['kode_unit'] ?><br>
                    Nama Mobil : <?php echo $detail['nama_unit'] ?>
                </td>
                <td rowspan="3">
                    <textarea class="printtextarea" style="width:100%;" name="kolom1[]" style=""><?php echo CheckEmpty(@$detail['kolom1']) ?"1. Ban Cadangan dan Velg : 1 Buah &#13;&#10;2. Pompa Pelumas : - &#13;&#10;3. Perkakas terdiri dari: &#13;&#10;- Dongkrak dan Stang &#13;&#10;- Kunci Roda dan Stang &#13;&#10;- Engkol Ban Cadangan":@$detail['kolom1']?></textarea>
                </td>
                <td rowspan="3">
                    <textarea class="printtextarea" style="width:100%;" name="kolom2[]" style=""><?php echo CheckEmpty(@$detail['kolom2']) ?"4. Toolset terdiri dari : &#13;&#10;- Kunci Pas : 5 Buah &#13;&#10;- Tang : 1 Buah &#13;&#10;- Obeng : 1 Buah &#13;&#10;- Kunci Inggris : 1 (T) &#13;&#10;5. Palu : 1 Buah":@$detail['kolom2']?> </textarea>


                </td>
            </tr>
            <tr>
                <td>Nomor Rangka</td>
                <td>Nomor Mesin</td>
                <td>Buku Service</td>
            </tr>
            <tr>
                <td><?php echo $detail['vin_number'] ?></td>
                <td><?php echo $detail['engine_no'] ?></td>
                <td> - </td>
            </tr>
            <tr>
                <td colspan="5" class="tdnoborder">
                    <table class="tdchild" style="border-style:none !Important;width:100%; border-width:0px;font-size:11px">
                        <tbody>
                            <tr style="height:70px;">
                                <td colspan="4"  class="top-ver" style="text-align:center"></td>
                                <td width="20%" class="top-ver" style="border:solid 1px #000;font-family:arial;padding-bottom: 0px !Important;margin-bottom:none;font-weight:bold;font-size:12px;">
                                    <textarea name="keterangan_bawah[]" class="" style="width:100%;height:70px;border:none" ><?php echo CheckEmpty(@$detail['keterangan_bawah'])?"Unit Tidak Boleh Keluar Dari Karoseri Tanpa Persetujuan DGMI":@$detail['keterangan_bawah']?></textarea>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</div>