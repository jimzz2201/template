
<section class="content-header noprint">
    <h1>
        Surat Jalan Prospek
        <small>Data Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">Surat Jalan Prospek</li>
    </ol>


</section>

<section class="content noprint">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>


        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;


    function RefreshGrid()
    {
        table.fnDraw(false);
    }

    $(document).ready(function () {

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "do_prospek/getdatapendingdo", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_do_kpu",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal_do", orderable: false, title: "Tanggal", "className": "text-center",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
                {data: "nomor_do_kpu", orderable: false, title: "DO KPU"},
                {data: "nama_customer", orderable: false, title: "Nama Customer"},
                {data: "pool_asal", orderable: false, title: "Asal"},
                {data: "pool_tujuan", orderable: false, title: "Tujuan"},
                {data: "jumlah_unit", orderable: false, title: "Jumlah Unit"},
                {data: "free_parking", orderable: false, title: "Free Parking",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
                {data: "action", orderable: false, title: "Aksi"},
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });


</script>
<style>
    @media print
    {
        .noprint {
            display:none;
        }

        .modal-footer, .modal-header {
            display: none;
        }

        .modal-dialog{
            border:0px;
        }
    }
</style>