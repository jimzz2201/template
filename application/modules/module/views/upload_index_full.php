<div class="form-group" style="margin-bottom:40px;">
    <button type="button" class="btn btn-danger" id="btt_upload" ><i class="fa fa-picture-o" aria-hidden="true"></i> &nbsp;Upload Dokumen</button>
    <button type="button" class="btn btn-success" id="btt_ambil_customer" ><i class="fa  fa-address-book" aria-hidden="true"></i> &nbsp;Ambil dari Customer</button>
   
</div>
<div class="form-group col-md-12">

    <div class="form-group areagallery" >
        <table class="table table-striped table-bordered table-hover" id="mytable">
            <thead>
            <th>Tgl Kirim</th>
            <th>Tgl Terima</th>      
            <th>Jenis Dokumen</th>
            <th>Keterangan</th>
            <th>Penyerah</th>
            <th>Penerima</th>
            <th style="width:140px;">File</th>
            <th>Action</th>
            </thead>
            <tbody>
                <?php
                foreach ($listimage as $key => $imagesat) {
                    echo "<tr id='tr".$imagesat['id_asset_link']."'>";
                    echo "<td>" . DefaultTanggal($imagesat['tanggal_kirim_document']) . "</td>";
                    echo "<td>" . DefaultTanggal($imagesat['tanggal_terima_do']) . "</td>";
                    echo "<td>" . $imagesat['type_dokumen'] . "</td>";
                    echo "<td><span class='clsket'>" . $imagesat['keterangan_file'] . "</span></td>";
                    echo "<td>" . $imagesat['penyerah'] . "</td>";
                    echo "<td>" . $imagesat['penerima'] . "</td>";
                    echo "<td><div class='imagesat'  data-url='" . base_url() . $imagesat['file_path'] . "/" . $imagesat['name_file'] . "' style=\"width:140px;height:100px;background:url('" . base_url() . ($imagesat['file_extension'] != "application/pdf" ? $imagesat['file_path'] . "/" . $imagesat['name_file'] : "assets/images/defaultpdf.jpg") . "')\"></div></td>";
                    echo "<td style='text-align:center'>" . "<button type='button' style='margin-right:10px;'  data-id='" . $imagesat['id_asset_link'] . "' class='btnupdateimage btn btn-info'>Update</button>". "<button type='button'  data-id='" . $imagesat['id_asset'] . "' class='btnhapusimage btn btn-danger'>Delete</button></td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>


      



    </div>
</div>

<script>
    var listimage = <?php echo json_encode($listimage) ?>;

    function RefreshImageThumbnail()
    {
        $(".areagallery .imagesat").dblclick(function () {
            var bg = $(this).css('background-image');
            bg = bg.replace('url(', '').replace(')', '').replace(/\"/gi, "");
            if (bg.indexOf("defaultpdf.jpg") == -1)
            {
                modalbootstrap("<div class='modal-body'><dic class='modalimage'><img src='" + bg + "' /></div><div class='model-desc'>" + $(this).parent().parent().find(".clsket").html() + "</div></div>");

            } else
            {
                modalbootstrap("<div class='modal-body'><dic class='modalimage'><a href='" + $(this).data("url") + "' target='_blank'><img src='" + baseurl + "assets/images/defaultpdf.jpg" + "' /></a></div><div class='model-desc'>" + $(this).find(".clsket").html() + "</div></div>");

            }
        })
        $(".areagallery .btnupdateimage").click(function () {
            var id_asset_link = $(this).data("id");
             $.ajax({
                url: baseurl + 'index.php/module/upload_edit',
                type: 'post',
                dataType: 'json',
                data: {
                    type: "<?php echo @$type_upload ?>",
                    full_upload: "<?php echo @$full_upload ?>",
                    id_asset_link:id_asset_link
                    
                },
                success: function (data) {
                    if (data.st)
                    {
                        modalbootstrap(data.html,"Uploads","1024px");

                    } else
                    {
                        messageerror(data.msg);
                    }


                },
                error: function (xhr, status, error) {
                      messageerror(xhr.responseText);
                }
            });

        })
        
        $(".areagallery .btnhapusimage").click(function () {
            var id_asset = $(this).data("id");
            listimage = listimage.filter(
                    function (value, index, arr) {
                        return value.id_asset != id_asset
                    }
            );
            $(this).parent().parent().remove();
        })
    }
    $("#btt_ambil_customer").click(function () {
        $.ajax({
            url: baseurl + 'index.php/module/getimage',
            type: 'post',
            dataType: 'json',
            data: {
                type: "Customer",
                reference_id: $("#dd_id_customer").val()
            },
            success: function (data) {
                if (data.st)
                {
                    $.each(data.listimage, function (key, value) {
                        var res = alasql('SELECT * FROM ? where id_asset=\'' + value.id_asset + '\'', [listimage]);
                        if (res.length == 0)
                        {
                            $("#mytable tbody").append(add_photo(value));
                            listimage.push(value);
                        }

                    });

                } else
                {
                    messageerror(data.msg);
                }


            },
            error: function (xhr, status, error) {
                    
            }
        });


    })
    function add_photo(data)
    {
        var addsection="";
        addsection = "<tr>";
        addsection+= "<td>" +data.tanggal_kirim_document + "</td>";
        addsection+= "<td>" +data.tanggal_terima_do + "</td>";
        addsection+= "<td>" + data.type + "</td>";
        addsection+= "<td><span class='clsket'>" + data.keterangan_file + "</span></td>";
        addsection+= "<td>" + data.penyerah + "</td>";
        addsection+= "<td>" + data.penerima + "</td>";
        addsection+= "<td><div class='imagesat'  data-url='" + baseurl + data.file_path + "/" + data.name_file + "' style=\"width:140px;height:100px;background:url('" + baseurl + (data.file_extension != "application/pdf" ? data.file_path + "/" + data.name_file : "assets/images/defaultpdf.jpg") + "')\"></div></td>";
        addsection+= "<td style='text-align:center'>" + "<button  data-id='" + data.id_asset + "' class='btnhapusimage btn btn-danger'>Delete</button></td>";
        addsection+= "</tr>";
        
       
       return addsection;
        
    }
    $(document).ready(function () {
        RefreshImageThumbnail();
        $("#btt_clear").click(function () {
            listimage = [];
            $(".areagallery").empty();


        })
        if ($("#dd_id_customer").length > 0)
        {
            $("#btt_ambil_customer").css("display", "inline-block");
        } else
        {
            $("#btt_ambil_customer").css("display", "none");
        }
        $("#btt_upload").click(function () {
            $.ajax({
                url: baseurl + 'index.php/module/upload_view',
                type: 'post',
                dataType: 'json',
                data: {
                    type: "<?php echo @$type_upload ?>",
                    full_upload: "<?php echo @$full_upload ?>"
                },
                success: function (data) {
                    if (data.st)
                    {
                        modalbootstrap(data.html,"Uploads","1024px");

                    } else
                    {
                        messageerror(data.msg);
                    }


                },
                error: function (xhr, status, error) {

                }
            });

        })


    })
</script>