<div class="form-group" style="margin-bottom:40px;">
    <button type="button" class="btn btn-danger" id="btt_upload" ><i class="fa fa-picture-o" aria-hidden="true"></i> &nbsp;Upload</button>
    <button type="button" class="btn btn-success" id="btt_ambil_customer" ><i class="fa  fa-address-book" aria-hidden="true"></i> &nbsp;Ambil dari Customer</button>
    <button type="button" class="btn btn-warning" id="btt_clear" ><i class="fa fa-trash" aria-hidden="true"></i> &nbsp;Clear</button>
</div>
<div class="form-group areagallery" >
    <?php
    foreach ($listimage as $imagesat) {
        echo "<div class=\"col-sm-1\" data-url='" . base_url() . $imagesat['file_path'] . "/" . $imagesat['name_file'] . "' style=\"background:url('" . base_url() . ($imagesat['file_extension'] != "application/pdf" ? $imagesat['file_path'] . "/" . $imagesat['name_file'] : "assets/images/defaultpdf.jpg") . "')\">" .
        ($imagesat['keterangan_file'] != "" ? "<div class='clsket'>" . $imagesat['keterangan_file'] . '</div>' : '') .
        "<i data-id='" . $imagesat['id_asset'] . "' class=\"fa fa-window-close close\" aria-hidden=\"true\"></i>" .
        "</div>";
    }
    ?>



</div>

<script>
    var listimage = <?php echo json_encode($listimage) ?>;

    function RefreshImageThumbnail()
    {
        $(".areagallery .col-sm-1").dblclick(function () {
            var bg = $(this).css('background-image');
            bg = bg.replace('url(', '').replace(')', '').replace(/\"/gi, "");
            if (bg.indexOf("defaultpdf.jpg")==-1)
            {
                modalbootstrap("<div class='modal-body'><dic class='modalimage'><img src='" + bg + "' /></div><div class='model-desc'>" + $(this).find(".clsket").html() + "</div></div>");

            } else
            {
                modalbootstrap("<div class='modal-body'><dic class='modalimage'><a href='" + $(this).data("url") + "' target='_blank'><img src='" + baseurl + "assets/images/defaultpdf.jpg" + "' /></a></div><div class='model-desc'>" + $(this).find(".clsket").html() + "</div></div>");

            }
        })
        $(".areagallery .col-sm-1 .close").click(function () {
            var id_asset = $(this).data("id");
            listimage = listimage.filter(
                    function (value, index, arr) {
                        return value.id_asset != id_asset
                    }
            );
            $(this).parent().remove();
        })
    }
    $("#btt_ambil_customer").click(function () {
        $.ajax({
            url: baseurl + 'index.php/module/getimage',
            type: 'post',
            dataType: 'json',
            data: {
                type: "Customer",
                reference_id: $("#dd_id_customer").val()
            },
            success: function (data) {
                if (data.st)
                {
                    $.each(data.listimage, function (key, value) {
                        var res = alasql('SELECT * FROM ? where id_asset=\'' + value.id_asset + '\'', [listimage]);
                        if (res.length == 0)
                        {
                            $(".areagallery").append(add_photo(value));
                            listimage.push(value);
                        }

                    });

                } else
                {
                    messageerror(data.msg);
                }


            },
            error: function (xhr, status, error) {

            }
        });


    })
    function add_photo(data)
    {
        return "<div class=\"col-sm-1\" data-url=\""+baseurl+data.file_path + "/" + data.name_file +"\" style=\"background:url('" + baseurl + (data.file_extension != "application/pdf" ? data.file_path + "/" + data.name_file : "assets/images/defaultpdf.jpg") + "')\">" +
                "<i data-id='" + data.id_asset + "' class=\"fa fa-window-close close\" aria-hidden=\"true\"></i>" +
                (data.keterangan_file != "" ? "<div class=\"clsket\">" + data.keterangan_file + "</div>" : "") +
                "</div>";
    }
    $(document).ready(function () {
        RefreshImageThumbnail();
        $("#btt_clear").click(function () {
            listimage = [];
            $(".areagallery").empty();


        })
        if ($("#dd_id_customer").length > 0)
        {
            $("#btt_ambil_customer").css("display", "inline-block");
        } else
        {
            $("#btt_ambil_customer").css("display", "none");
        }
        $("#btt_upload").click(function () {
            $.ajax({
                url: baseurl + 'index.php/module/upload_view',
                type: 'post',
                dataType: 'json',
                data: {
                    type: "<?php echo @$type_upload?>"
                },
                success: function (data) {
                    if (data.st)
                    {
                        modalbootstrap(data.html);

                    } else
                    {
                        messageerror(data.msg);
                    }


                },
                error: function (xhr, status, error) {

                }
            });

        })


    })
</script>