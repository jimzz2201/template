<style>
    .areaimage{

    }
    .areaimage img{
        max-width:100%;
    }
</style>

<div class="modal-header" style="color:#fff;font-weight:bold;">
    UPLOAD FILE
</div>
<div class="modal-body">
    <form id="frmmodalupload" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_jabatan" value="<?php echo @$id_jabatan; ?>" /> 
        <div class="form-group">
            <?= form_label('Type', "txt_kode_jabatan", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array("selected" => @$type, "name" => "type"), DefaultEmptyDropdown($listtype, "json", "Type"), @$type, array('class' => 'form-control', 'id' => 'dd_type')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('&nbsp;', "txt_nama_jabatan", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'file', 'name' => 'inputfile', 'value' => @$nama_jabatan, 'class' => 'form-control', 'id' => 'inputfile', 'placeholder' => 'Nama Jabatan')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('&nbsp;', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8 areaimage" >
                <img src="<?php echo base_url() ?>assets/images/default.jpg" id="img_thumbnail" />

            </div>

        </div>


        <div class="form-group">
            <?= form_label('Keterangan', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_textarea(array("value" => @$keterangan, "name" => "keterangan_file", 'class' => 'form-control', 'id' => 'txt_keterangan')); ?>
            </div>
        </div>

        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </form>
</div>
<script>
    $(document).ready(function () {
        $("#frmmodalupload select").select2();

    })
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                if(e.target.result.indexOf("data:application/pdf;")>-1)
                {
                    $('#img_thumbnail').attr('src', baseurl+"assets/images/defaultpdf.jpg");
                }
                else
                {
                    $('#img_thumbnail').attr('src', e.target.result);
                }
                
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputfile").change(function () {
        readURL(this);
    });
    $("#frmmodalupload").submit(function () {
        var file = document.getElementById("inputfile").files[0]; //fetch file
        var formData = new FormData(document.getElementById('frmmodalupload'));
        formData.append('file', file); //append file to formData object
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/module/uploads_manipulate',
            data: formData,
            contentType: false,
            processData: false,
            type: 'POST',
            mimeType: "multipart/form-data",
            dataType: 'Json',
            cache: false,
            success: function (data) {
                if (data.st)
                {
                    $(".areagallery").append(add_photo(data));
                    listimage.push(data);
                    RefreshImageThumbnail();
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>