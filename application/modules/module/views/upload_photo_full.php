<style>
    .areaimage{

    }
    .areaimage img{
        max-width:300px;

    }
</style>

<div class="modal-header" style="color:#fff;font-weight:bold;">
    UPLOAD Dokumen
</div>
<div class="modal-body">
    <form id="frmmodalupload" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_asset" value="<?php echo @$id_asset; ?>" /> 
        <input type="hidden" name="id_asset_link" value="<?php echo @$id_asset_link; ?>" /> 

        <div class="form-group">
            <?= form_label('Type', "txt_kode_jabatan", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$type_dokumen, "name" => "type"), DefaultEmptyDropdown($listtype, "json", "Type"), @$type_dokumen, array('class' => 'form-control', 'id' => 'dd_type')); ?>
            </div>
            <?= form_label('&nbsp;', "txt_nama_jabatan", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'file', 'name' => 'inputfile', 'value' => @$nama_jabatan, 'class' => 'form-control', 'id' => 'inputfile', 'placeholder' => 'Nama Jabatan')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Tgl Kirim', "txt_kode_jabatan", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text','autocomplete'=>'off', 'name' => 'tanggal_kirim_document', 'class' => 'form-control datepickermodal', 'id' => 'txt_tanggal_kirim_document', 'value' => DefaultDatePicker(@$tanggal_kirim_document), 'placeholder' => 'Tanggal Kirim')); ?>
            </div>
            <?= form_label('Tgl&nbsp;Terima', "txt_nama_jabatan", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'text','autocomplete'=>'off', 'name' => 'tanggal_terima_do', 'class' => 'form-control datepickermodal', 'id' => 'tanggal_terima_do', 'value' => DefaultDatePicker(@$tanggal_terima_do), 'placeholder' => 'Tanggal Terima')); ?>

            </div>
        </div>
        <div class="form-group">
            <?= form_label('Penyerah', "txt_kode_jabatan", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'name' => 'penyerah', 'class' => 'form-control', 'id' => 'txt_penyerah', 'value' => @$penyerah, 'placeholder' => 'Penyerah')); ?>
            </div>
            <?= form_label('Penerima', "txt_nama_jabatan", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'text', 'name' => 'penerima', 'class' => 'form-control', 'id' => 'txt_penerima', 'value' => @$penerima, 'placeholder' => 'Penerima')); ?>

            </div>
        </div>


        <div class="form-group">
            <?= form_label('&nbsp;', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-8 areaimage" >
                <img src="<?php echo base_url() . (CheckEmpty(@$name_file) ? "assets/images/default.jpg" : (@$file_path . "/" . @$name_file)) ?>" id="img_thumbnail" />

            </div>

        </div>


        <div class="form-group">
            <?= form_label('Keterangan', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?= form_textarea(array("value" => @$keterangan, "name" => "keterangan_file", 'class' => 'form-control', 'id' => 'txt_keterangan')); ?>
            </div>
        </div>

        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

    </form>
</div>
<script>
    $(document).ready(function () {
        $("#frmmodalupload select").select2();
        $(".datepickermodal").datepicker();
    })
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                if (e.target.result.indexOf("data:application/pdf;") > -1)
                {
                    $('#img_thumbnail').attr('src', baseurl + "assets/images/defaultpdf.jpg");
                } else
                {
                    $('#img_thumbnail').attr('src', e.target.result);
                }

            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputfile").change(function () {
        readURL(this);
    });
    $("#frmmodalupload").submit(function () {
        var file = document.getElementById("inputfile").files[0]; //fetch file
        var formData = new FormData(document.getElementById('frmmodalupload'));
        formData.append('file', file); //append file to formData object
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/module/uploads_manipulate',
            data: formData,
            contentType: false,
            processData: false,
            type: 'POST',
            mimeType: "multipart/form-data",
            dataType: 'Json',
            cache: false,
            success: function (data) {
                if (data.st)
                {
                    if (data.id_asset_link)
                    {
                        var id_asset_link = data.id_asset_link;
                        var index = listimage.findIndex(x => x.id_asset_link ===id_asset_link);
                   
                        if(index>=0)
                        {
                            listimage[index]=data;
                           
                        }
                       
                           
                        $("#tr"+data.id_asset_link+" td:eq(0)").html(data.tanggal_kirim_document);
                        $("#tr"+data.id_asset_link+" td:eq(1)").html(data.tanggal_terima_do);
                        $("#tr"+data.id_asset_link+" td:eq(2)").html(data.type);
                        $("#tr"+data.id_asset_link+" td:eq(3)").html(data.keterangan_file);
                        $("#tr"+data.id_asset_link+" td:eq(3)").html(data.penyerah);
                        $("#tr"+data.id_asset_link+" td:eq(3)").html(data.penerima);
                        $("#tr"+data.id_asset_link+" .imagesat").css("background","url('"+baseurl + (data.file_extension != "application/pdf" ? data.file_path + "/" + data.name_file : "assets/images/defaultpdf.jpg")+"')");
                    } else {
                        $("#mytable tbody").append(add_photo(data));
                        listimage.push(data);
                        RefreshImageThumbnail();
                    }

                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>