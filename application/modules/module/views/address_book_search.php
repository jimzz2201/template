<div class="modal-header">
    Bank <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_bank" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" id="txt_id_address_book_search" name="id_address_book" value="<?php echo @$id_address_book; ?>" /> 
        <div class="form-group">
            <?= form_label('Nama Address Book', "txt_kode_bank", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'name' => 'nama_address_book', 'value' => @$nama_address_book, 'class' => 'form-control', 'id' => 'txt_nama_address_book_search', 'placeholder' => 'Nama Address Book')); ?>

            </div>
            <?= form_label('Default', "txt_kode_bank", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$nama_address_book, 'class' => 'form-control', 'id' => 'txt_nama_address_book_before', 'placeholder' => 'Nama Address Book')); ?>

            </div>

        </div>

        <div class="form-group">
            <?= form_label('UP', "txt_nama_akun", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'name' => 'up', 'value' => @$up, 'class' => 'form-control', 'id' => 'txt_up_search', 'placeholder' => 'UP')); ?>
            </div>
            <?= form_label('Order', "txt_no_akun", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'text', 'name' => 'order_ket', 'value' => @$order_ket, 'class' => 'form-control', 'id' => 'txt_order_ket_search', 'placeholder' => 'Order')); ?>
            </div>
        </div>

        <div class="form-group">
            <?= form_label('Alamat Kirim', "txt_cabang_bank", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?= form_textarea(array('type' => 'text', 'name' => 'alamat_kirim', 'value' => @$alamat_kirim, 'class' => 'form-control', 'id' => 'txt_alamat_kirim_search', 'placeholder' => 'Alamat Kirim')); ?>
            </div>

        </div>


        <div class="modal-footer">
            <button type="button" class="btn btn-success" id="btt_clear_form" >Clear Form</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_form" >Tambah & Gunakan</button>
        </div>

    </form>
    <div class="form-group">
        <table  id="table_address_book" class="td30 tr30 table table-striped table-bordered table-hover dataTable no-footer">

        </table>
    </div>
</div>
<script>
    var tableaddressbook;
    $("#btt_clear_form").click(function () {
        $("#txt_alamat_kirim_search").val("");
        $("#txt_up_search").val("");
        $("#txt_order_ket_search").val("");
        $("#txt_nama_address_book_before").val("");
        $("#txt_nama_address_book_search").val("");
        $("#txt_id_address_book_search").val("");
        $("#btt_modal_form").html("Tambah & Gunakan");

    })

    function actionaddressbook(id_address_book, action)
    {
        $.ajax({
            type: 'GET',
            url: baseurl + 'index.php/module/get_address_book/' + id_address_book,
            dataType: 'json',
            success: function (data) {
                if (data.st)
                {
                    if (action == "select")
                    {
                        $("#txt_up<?php echo @$type?>").val(data.obj.up);
                        $("#txt_order_ket<?php echo @$type?>").val(data.obj.order_ket);
                        $("#txt_alamat_kirim<?php echo @$type?>").val(data.obj.alamat_kirim);
                        $("#txt_atas_nama<?php echo @$type?>").val(data.obj.nama_address_book);
                        $("#modalbootstrap").modal("hide");
                    } else
                    {
                        $("#txt_id_address_book_search").val(data.obj.id_address_book);
                        $("#txt_up_search").val(data.obj.up);
                        $("#txt_order_ket_search").val(data.obj.order_ket);
                        $("#txt_nama_address_book_before").val(data.obj.nama_address_book);
                        $("#txt_nama_address_book_search").val(data.obj.nama_address_book);
                        $("#txt_alamat_kirim_search").val(data.obj.alamat_kirim);
                        $('#modalbootstrap').animate({scrollTop: 0}, 'slow');
                        $("#btt_modal_form").html("Update & Gunakan");
                    }

                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
    }
    $(document).ready(function () {


        setTimeout(function () {
            tableaddressbook = $("#table_address_book").dataTable({
                "aoColumnDefs": [{'bSortable': true, 'aTargets': [3]}],
                "bSort": true,
                initComplete: function () {
                    var apiaddressbook = this.api();
                    $('#table_address_book_filter input')
                            .off('.DT')
                            .on('keyup.DT', function (e) {
                                if (e.keyCode == 13) {
                                    apiaddressbook.search(this.value).draw();
                                }
                            });
                },
                oLanguage: {
                    sProcessing: "loading..."
                },
                searchable: true,
                processing: true,
                searching: true,
                scrollX: true,
                ajax: {"url": baseurl + "index.php/module/getdataaddressbook", "type": "POST"},
                columns: [
                    {
                        data: "id_address_book",
                        title: "Kode",
                        width: "10px",
                        className: "text-right"
                    },
                    {data: "nama_address_book", searchables: true, api: true, title: "Nama"},
                    {data: "order_ket", title: "Order"},
                    {data: "up", title: "UP"},
                    {data: "alamat_kirim", title: "Alamat"},

                    {
                        "data": "action",
                        "orderable": false,
                        "className": "text-center",
                        "width": "100px"

                    }
                ],
                order: [[0, 'desc']],
                rowCallback: function (row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });
        }, 500);


    })
    $("#frm_bank").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/module/address_book_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    $("#txt_up<?php echo @$type?>").val($("#txt_up_search").val());
                    $("#txt_order_ket<?php echo @$type?>").val($("#txt_order_ket_search").val());
                    $("#txt_alamat_kirim<?php echo @$type?>").val($("#txt_alamat_kirim_search").val());
                    $("#txt_atas_nama<?php echo @$type?>").val($("#txt_nama_address_book_search").val());
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>