<section class="sidebar">
    <!-- Sidebar user panel -->
    
    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="dashboardmenu">
            <a href="<?php echo base_url() ?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <?php foreach ($listmenu as $menu) { 
            if(count($menu->submenu)>0){ 
            ?>
            <li class="treeview <?php echo "menu".$menu->kode_form?>">
                <a href="<?php echo CheckEmpty($menu->form_url)  ? 'javascript:;' : base_url() . $menu->form_url ?>">
                    <?php if (!CheckEmpty($menu->form_icon)) { ?>
                        <i class="fa <?php echo $menu->form_icon ?>"></i>
                    <?php } ?>
                      
                        <span><?php echo $menu->form_name ?></span>
                        <?php if (count($menu->submenu) > 0) { ?>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                       <?php  }?>
                         </a>
                          <?php if (count($menu->submenu) > 0) { ?>
                                <ul class="treeview-menu" >
                                      <?php foreach ($menu->submenu as $key=>$submenu) { ?>
                                        <li class="<?php echo ($key==0?'litreeview':''). (" submenu".$submenu->kode_form)?>"><a href="<?php echo base_url().$submenu->form_url ?>"><i class="fa fa-angle-right"></i><?php echo $submenu->form_name ?></a></li>
                                      <?php }?>
                                </ul>
                       <?php } ?>
            </li>
        <?php }} ?>
    </ul>
</section>
          