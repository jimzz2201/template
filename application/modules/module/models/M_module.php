<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_module extends CI_Model {

    public $table = '#_barang';
    public $id = 'id_barang';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function SyncDb($listimage,$type,$referenceid) {
        $idimage = [];
        foreach ($listimage as $image) {
            $this->db->from("#_asset_link");
            if(!CheckEmpty($image['id_asset_link']))
            {
                $this->db->where(array("id_asset_link" => $image['id_asset_link']));
            }
            else
            {
               $this->db->where(array("id_asset" => $image['id_asset'], 'reference_id' => $referenceid, "type_asset" => $type));  
            }
            
            $rowcheck = $this->db->get()->row();
         
            
            
            $asset_link['id_asset'] = $image['id_asset'];
            $asset_link['keterangan_file'] = $image['keterangan_file'];
            $asset_link['type_dokumen'] = $image['type'];
            $asset_link['reference_id'] = $referenceid;
            $asset_link['type_asset'] = $type;
            $asset_link['tanggal_kirim_document'] = DefaultTanggalDatabase($image['tanggal_kirim_document']);
            $asset_link['tanggal_terima_do'] = DefaultTanggalDatabase($image['tanggal_terima_do']);
            $asset_link['penyerah'] = $image['penyerah'];
            $asset_link['penerima'] = $image['penerima'];
           

            if (!$rowcheck) {
                $asset_link = MergeCreated($asset_link);
                $this->db->insert("#_asset_link", $asset_link);
            }
            else
            {
                $asset_link= MergeUpdate($asset_link);
                $this->db->update("#_asset_link",$asset_link,array("id_asset_link"=>$rowcheck->id_asset_link));
               
            }
            $idimage[] = $image['id_asset'];
        }
        if (count($idimage)) {
            $this->db->where_not_in("id_asset", $idimage);
        }
        $this->db->where(array("reference_id" => $referenceid, "type_asset" => $type, "deleted_date" => null));
        $this->db->update("#_asset_link", array("deleted_date" => GetDateNow(), "deleted_by" => GetUserId()));
    }
    
   function GetOneAddressBook($id_address_book){
       $this->db->from("#_address_book");
       $this->db->where(array("id_address_book"=>$id_address_book));
       $row=$this->db->get()->row();
       return $row;
       
   }
   
   
   function GetOneAssetLink($id_asset_link)
   {
       $this->db->from("#_asset_link");
       $this->db->join("#_asset","#_asset.id_asset=#_asset_link.id_asset","left");
       $this->db->where(array("#_asset_link.id_asset_link"=>$id_asset_link));
       $row=$this->db->get()->row();
       return $row;
   }
   
   function AddressBookManipulate($model) {
        try {
             if (CheckEmpty($model['id_address_book'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert("#_address_book", $model);
                return array("st" => true, "msg" => "Address Book successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update("_address_book", $model, array("id_address_book" => $model['id_address_book']));
                return array("st" => true, "msg" => "Adress Book has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
   
   
    function GetDataAddressBook() {
        $this->load->library('datatables');
        $this->datatables->select('id_address_book,#_address_book.*');
        $this->datatables->from("#_address_book");
        $isedit = true;
        $stredit = '';
        $stredit .= anchor("", 'Select', array('class' => 'btn btn-default btn-xs', "onclick" => "actionaddressbook($1,'select');return false;"));
        if ($isedit) {
            $stredit .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "actionaddressbook($1,'update');return false;"));
        }
        
        $this->datatables->add_column('action', $stredit, 'id_address_book');
        return $this->datatables->generate();
    }

    function GetFile($type, $reference_id) {
        $this->db->from("#_asset_link");
        $this->db->join("#_asset", "#_asset.id_asset=#_asset_link.id_asset");
        $this->db->where(array("type_asset" => $type, "reference_id" => $reference_id, "#_asset_link.deleted_date" => null));
        $this->db->select("id_asset_link,type_dokumen as type,name_file,tanggal_kirim_document,tanggal_terima_do,penyerah,penerima,file_extension,file_path,#_asset.created_date,#_asset.created_by,#_asset_link.id_asset,keterangan_file,type_dokumen");
        $listimage = $this->db->get()->result_array();

        return $listimage;
    }

    function UploadFile($data) {
        $assests = array();
        $assests['name_file'] = $data['file_name'];
        $assests['file_extension'] = $data['file_type'];
        $assests['file_path'] = $data['path'];
        $assests['created_date'] = GetDateNow();
        $assests['created_by'] = GetUserId();
        $this->db->insert("#_asset", $assests);
        $assets_id = $this->db->insert_id();
        $assests['id_asset'] = $assets_id;
        $assests['st'] = true;

        return $assests;
    }

}
