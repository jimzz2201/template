<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Module extends CI_Controller {

    function __construct() {
        parent::__construct();
    }
    
    function address_book_search($type=""){
        $model['type']=$type;
        $this->load->view("address_book_search",$model);
    }
    
    
    function address_book_manipulate(){
        
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('nama_address_book', 'Nama', 'trim|required');
        $this->form_validation->set_rules('alamat_kirim', 'Alamat Kirim', 'trim|required');
        
        

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
             $this->load->model("m_module");
            $status = $this->m_module->AddressBookManipulate($model);
            echo json_encode($status);
        }
    }
    function get_address_book($id_address_book)
    {
        $this->load->model("m_module");
        $row=$this->m_module->GetOneAddressBook($id_address_book);
        echo json_encode(array("st"=>$row!=null,"obj"=>$row));
    }
    public function getdataaddressbook() {
        header('Content-Type: application/json');
        $this->load->model("m_module");
        echo $this->m_module->GetDataAddressBook();
    }
    
    function getimage()
    {
        $model = $this->input->post();
        $message = '';
        $data = array();
        $data['st'] = false;
        
        
        $this->form_validation->set_rules('type', "Type", 'required');
        $this->form_validation->set_rules('reference_id', "Reference", 'required');
       
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] =  validation_errors() . $message;
        } else {
           
            $data['st'] = $message == '';
            $this->load->model("module/m_module");
            $data['listimage'] = $this->m_module->GetFile($model['type'],$model['reference_id']);
        }
        echo json_encode($data);
        exit;
    }
    
    function upload_view(){
        $model = $this->input->post();
        $message = '';
        $data = array();
        $data['st'] = false;
        
        
        $this->form_validation->set_rules('type', "Type", 'required');
        $listtype=[];
        
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] =  validation_errors() . $message;
        } else {
            $row = [];
            if (@$model['type'] == "Prospek") {
                $listtype['stnk'] = "STNK";
                $listtype['spk'] = "SPK";
                $listtype['notakirim'] = "Nota Pengiriman";
            }
            if (@$model['type'] == "Persediaan") {
                $listtype['bpkb'] = "BPKB";
                $listtype['stnk'] = "STNK";
            }
            
            
            $listtype['lainnya'] = "Dokumen Lainnya";
            $row['listtype'] = $listtype;
            if(CheckEmpty(@$model['full_upload']))
            {
                $viewhtml = $this->load->view("module/upload_photo", $row, true);
            }
            else
            {
                $viewhtml = $this->load->view("module/upload_photo_full", $row, true);
            }
            
            $data['st'] = $message == '';
            $data['html'] = $viewhtml;
        }
        echo json_encode($data);
        exit;
    }
    
    function upload_edit(){
        $model = $this->input->post();
        $message = '';
        $data = array();
        $data['st'] = false;
        
        $this->form_validation->set_rules('id_asset_link', "File", 'required');
        $this->form_validation->set_rules('type', "Type", 'required');
        $listtype=[];
        
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] =  validation_errors() . $message;
        } else {
            $row = [];
            $this->load->model("module/m_module");
            $row=$this->m_module->GetOneAssetLink($model['id_asset_link']);
            $row= json_decode(json_encode($row),true);
            if($row['file_extension']=="application/pdf")
            {
                $row['name_file']="defaultpdf.jpg";
                $row['file_path']="assets/images";
            }
            
            
            $listtype['npwp'] = "NPWP";
            $listtype['legal'] = "Dokumen Legal";
            if (@$model['type'] == "Prospek") {
                $listtype['stnk'] = "STNK";
                $listtype['spk'] = "SPK";
                $listtype['notakirim'] = "Nota Pengiriman";
            }
            $listtype['lainnya'] = "Dokumen Lainnya";
            $row['listtype'] = $listtype;
            if(CheckEmpty(@$model['full_upload']))
            {
                $viewhtml = $this->load->view("module/upload_photo", $row, true);
            }
            else
            {
                $viewhtml = $this->load->view("module/upload_photo_full", $row, true);
            }
            
            $data['st'] = $message == '';
            $data['html'] = $viewhtml;
        }
        echo json_encode($data);
        exit;
    }
    
    
    public function uploads_manipulate(){
        
        $model=$this->input->post();
        
        $this->load->model("module/m_module");
      
        $this->form_validation->set_rules('type', 'Type', 'required');
        $message = "";
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
            echo json_encode($result);
        } else {
            $path = "assets/uploads/".$model['type'];
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $config['upload_path'] = './' . $path;
            $config['allowed_types'] = 'gif|jpg|png|pdf';
            $returndata = array();
            $returndata['st'] = false;
            $config['max_size'] = '1000000';
            

            $message = "";
            if ($_FILES['inputfile']['name'] != "" ||  !CheckEmpty(@$model['id_asset_link'])) {
               
                $returndata = array("st" => false);
                $rowupdate=[];
                if (!CheckEmpty(@$model['id_asset_link'])) {
                    $rowupdate = $this->m_module->GetOneAssetLink(@$model['id_asset_link']);
                    $rowupdate = json_decode(json_encode($rowupdate), true);
                    $rowupdate['st'] = true;
                }
                
                if (!CheckEmpty($_FILES['inputfile']['name'])) {
                    $filename = date('Ymdhis') . '_' . preg_replace("/[^.\w]+/", "", $_FILES['inputfile']['name']);
                    $config['file_name'] = $filename;
                    $this->load->library('upload', $config);


                    if (!$this->upload->do_upload("inputfile")) {
                        $data = array('st' => false, 'msg' => $this->upload->display_errors());
                    } else {
                        $data = $this->upload->data();

                        if ($data) {
                            $data['path'] = $path;
                            $returndata = $this->m_module->UploadFile($data);
                            if (!CheckEmpty(@$model['id_asset_link'])) {
                                $rowupdate['name_file'] = $returndata['name_file'];
                                $rowupdate['file_extension'] = $returndata['file_extension'];
                                $rowupdate['file_path'] = $returndata['file_path'];
                                $rowupdate['created_date'] = $returndata['created_date'];
                                $rowupdate['created_by'] = $returndata['created_by'];
                                $rowupdate['type'] = $rowupdate['type_dokumen'];
                                $rowupdate['id_asset'] = $returndata['id_asset'];
                                $rowupdate['st'] = $returndata['st'];
                                $returndata=$rowupdate;
                                
                            }
                        }
                        
                    }
                }
                else if( !CheckEmpty(@$model['id_asset_link']))
                {
                    $returndata=$rowupdate;
                }



                $returndata['keterangan_file'] = $model['keterangan_file'];
                $returndata['tanggal_kirim_document'] = $model['tanggal_kirim_document'];
                $returndata['tanggal_terima_do'] = $model['tanggal_terima_do'];
                $returndata['penyerah'] = $model['penyerah'];
                $returndata['penerima'] = $model['penerima'];
                $returndata['type'] = $model['type'];
            } else {
                $message .= "Data Upload masih kosong";
            }
            $returndata["message"] = $message;
            echo json_encode($returndata);
        }
    }
    
    
    
   
}

/* End of file Barang.php */