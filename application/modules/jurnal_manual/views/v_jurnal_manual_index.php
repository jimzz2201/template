
<section class="content-header">
    <h1>
        Jurnal Manual
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active"> Jurnal Manual</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="form-group form-groups-bordered">
                    <div class="col-sm-2">
                        <?= form_input(array('type' => 'text', 'name' => 'start_date', 'value' => DefaultDatePicker(@$start_date), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal Awal')); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_input(array('type' => 'text', 'name' => 'end_date', 'value' => DefaultDatePicker(@$end_date), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal Tujuan')); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= form_dropdown(array("selected" => "", 'name' => 'id_cabang', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                    </div>

                    <div class="col-sm-2">
                        <?= form_input(array('type' => 'text', 'name' => 'kode_jurnal_manual', 'value' => @$no_jurnal_manual_master, 'class' => 'form-control', 'id' => 'txt_no_jurnal_manual_master', 'placeholder' => 'No Jurnal_manual Master')); ?>
                    </div>
                    <div class="col-sm-2">
                        <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                    </div>

                </div>

            </form>
        </div>
        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">

                <?php echo anchor(site_url('jurnal_manual/create_jurnal_manual'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>
</section>
<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {

        table.fnDraw(false);
        return false;
    })

    function batalDetail(id_jurnal_manual) {


        swal({
            title: "Apakah kamu yakin ingin membatalkan jurnal berikut?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/jurnal_manual/jurnal_manual_batal',
                    dataType: 'json',
                    data: {
                        id_jurnal_manual: id_jurnal_manual
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }

    $(document).ready(function () {
        $(".select2").select2();
        $(".datepicker").datepicker();
        
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "jurnal_manual/getdatajurnal_manual", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_jurnal_manual",
                    title: "Kode",
                    orderable: false,
                    width: "20px"
                },
                {data: "kode_jurnal_manual", orderable: false, title: "Kode", width: "100px"},
                {data: "payment_voucher", orderable: false, title: "Payment&nbsp;Voucher", width: "100px"},
                {data: "tanggal_jurnal", orderable: false, title: "Tanggal", width: "50px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data)
                    }},
                {data: "nama_cabang", orderable: false, title: "Cabang", width: "200px",
                    mRender: function (data, type, row) {
                        return row['nama_cabang'] ;
                    }},
                
                {data: "debetdetails", orderable: false, title: "Debet", width: "300px"},
                {data: "kreditdetails", orderable: false, title: "Kredit", width: "300px"},
                {data: "pembuat", orderable: false, title: "Pembuat", width: "100px"},
                {
                    "data": "action",
                    width: "100px",
                    "orderable": false,
                    "className": "text-center",
                    mRender: function (data, type, row) {
                        return row['status']=="2"?"Batal":data ;
                    }}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
