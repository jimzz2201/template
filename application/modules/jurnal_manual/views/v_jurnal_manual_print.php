<html>
    <head><title>Struk Kas <?= $kas[0]['kode_kas_master']; ?></title></head>
    <style>
        @media print{
            .tombol-print{
                display: none;
            }
        }
        .text-center{ text-align: center !important; }
        .text-right{ text-align: right !important; }
        .text-left{ text-align: left !important; }
        .jenis-pembayaran{
            font-size: 0.85em;
            float: right;
            padding-right: 10px;
        }

        .font10{ font-size: 10px; }
        .font12{ font-size: 12px; }
        .font14{ font-size: 14px; }
        .font16{ font-size: 16px; }

        .italic{ font-style: italic; }
        .bold{ font-weight: bold; }
        .underline{ text-decoration: underline; }

        .uppercase{ text-transform: uppercase; }
        .lowercase{ text-transform: lowercase; }
        .dashed-bottom{ border-bottom: 1px #000000 solid; }
        .dashed-top{ border-top: 1px #000000 solid; }

        table{ border-collapse: collapse; }
        table{
            line-height: 1.5em;
        }
        table th, table td{ padding: 2px 1px; }
        .no-margin{
            margin:0px !important;
        }

        body{ font-family: Verdana, Tahoma; margin: 0px; padding: 0px; }
    </style>
    <body>
        <a href="#" onclick="window.print();" class="tombol-print font10">Print</a>
        <pre>
<table border="0" cellspacing="0" cellpadding="0" style="font-size: 13px; width: 350px;">
    <tr><td colspan="2" style="text-align: center">Struk Kas <?= $kas[0]['kode_kas_master']; ?></td></tr>
    <tr><th colspan="2">TANDA UANG <?php echo @$jenis ?></th></tr>
    <tr><th colspan="2" style="text-transform:uppercase">LOKASI : <?= $lokasi ?></th></tr>
    <tr><th colspan="2" class="dashed-top"></th></tr>
    <tr>
        <td>Tanggal</td>
        <td>: <?= DefaultTanggal($kas[0]['tanggal_kas']); ?></td>
    </tr>
    <tr>
        <td class="text-left" width="45%">Jenis Pembayaran</td>
        <td>: <?= $jenis; ?></td>
    </tr>
                <?php if (!CheckEmpty(@$kas[0]['nama_karyawan'])) { ?>
            <tr>
                <td class="text-left" width="45%">Nama Karyawan</td>
                <td>: <?= @$kas[0]['nic_karyawan'] . ' - ' . @$kas[0]['nama_karyawan']; ?></td>
            </tr>
            <tr>
                <td class="text-left" width="45%">Bagian</td>
                <td>: <?= @$kas[0]['nama_departement']; ?></td>
            </tr>
                <?php } ?>
    <tr>
        <td class="text-left" width="45%">Payment Voucher</td>
        <td>: <?= @$kas[0]['payment_voucher']; ?></td>
    </tr>
    
                <?php if (!CheckEmpty($kas[0]['nama_project'])) { ?>
                        <tr>
                            <td class="text-left">Project</td>
                            <td>: <?= $kas[0]['nama_project']; ?><span class="jenis-pembayaran"></span></td>
                        </tr>
                <?php } ?>
  
    <tr>
        <td colspan="2" style="border-bottom:dashed 1px #000">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" >&nbsp;</td>
    </tr>
                <?php foreach (@$kas as $satuankas) { ?>
                    
                    <tr>
                        <td><?= $satuankas['nama_jenis_pengeluaran'] ?> - <?= $satuankas['nama_jenis_biaya'] ?></td>
                        <td align="right"><?= DefaultCurrencyAkuntansi($satuankas['id_jenis_kas'] == "1" ? $satuankas['jumlah'] : -1 * $satuankas['jumlah']) ?> </td>
                    </tr>
                    <?php if (!CheckEmpty($satuankas["keterangan"])) {
                        ?> 
                                    <tr>
                                        <td colspan="2">(<span style="font-size:10px;font-weight:bold;"><?php echo $satuankas["keterangan"] ?></span>)</td>
                                    </tr>  
                    <?php } ?>
                <?php } ?>
    
   
    
    
    <tr><td class="dashed-bottom" colspan="2"><br/></td></tr>
     <tr><td  colspan="2"><br/></td></tr>
     <tr>
        <td class="text-left">Jumlah Total</td>
        <td align="right">Rp. <?= DefaultCurrency($kas[0]['jumlah_total']); ?></td>
    </tr>
    <tr><td class="dashed-bottom" colspan="2"><br/></td></tr>
    <tr>
        <td></td>
        <td class="text-center"><?= DefaultTanggal(GetDateNow()) ?></td>
    </tr>
    
    <tr>
        <td class="text-center">Penyerah</td>
        <td class="text-center">Penerima</td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
                    <?php if ($kas[0]['jumlah_total'] > 0): ?>
                        <td class="text-center">(<?= $kas[0]['nama_karyawan'] ?>)</td>
                        <td class="text-center"><?= $pembuat ?></td>
                    <?php else : ?>
                        <td class="text-center"><?= $pembuat ?></td>
                        <td class="text-center">(<?= $kas[0]['nama_karyawan'] ?>)</td>
                    <?php endif; ?>
    </tr>
</table>
        </pre>
    </body>
</html>