<table class="tabledetail">
    <tr>
        <th>Tanggal Transaksi</th>
        <th>Kategori</th>
        <th>Kategori Detail</th>
        <th>Nominal</th>
        <th>Action</th>
    </tr>
    <?php if (count(@$listdetail) == 0) { ?>
        <tr>
            <td align="center" colspan="5">Tidak Ada Transaksi Dalam Pembayaran Ini</td>
        </tr>

    <?php
    } else {
        foreach (@$listdetail as $detailtransaction) {
            ?>
            <tr>
                <td><?= DefaultTanggalDempet($detailtransaction->tanggal_transaksi) ?></td>
                <td><?= $detailtransaction->nama_jenis_pengeluaran ?></td>
                <td><?= $detailtransaction->nama_jenis_biaya ?></td>
                <td><?php
                    $subtotal=$detailtransaction->jumlah;
                  
                    if ($detailtransaction->id_jenis_kas == "2")
                         $subtotal= $detailtransaction->jumlah * -1;
                    echo DefaultCurrencyAkuntansi($subtotal)
                    ?></td>
                <td>
                    <a href="javascript:;" class="btn btn-info btn-xs" onclick="lihatreceipt(<?= DefaultTanggalDempet($detailtransaction->id_kas) ?>);return false;"><i class="fa fa-print"></i></a>
                </td>
            </tr>
        <?php }
        ?>

<?php } ?>
</table>