<div class="modal-header" style="background-color: #367fa9; color:#fff; font-weight: bold;">
    Edit <?php echo @$kode_kas_master ?>
</div>
<form id="frm_kas" class="form-horizontal form-groups-bordered validate" method="post">
    <div class="modal-body">
        <input type="hidden" name="id_kas" value="<?php echo @$id_kas; ?>" />
        <div class="form-group">
            <?= form_label('Payment Voucher', "", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'class' => 'form-control', 'disabled' => 'disabled'), @$payment_voucher); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Jenis', "", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'class' => 'form-control', 'disabled' => 'disabled'), getJenisAliranKas($id_jenis_kas)); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Pembayaran', "", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'class' => 'form-control', 'disabled' => 'disabled'), getJenisTransaksi($id_jenis_transaksi)); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Kategori', "txt_id_jenis_pengeluaran", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array('class' => 'form-control', 'id' => 'txt_id_jenis_pengeluaran'), $list_jenis_pengeluaran, @$id_jenis_pengeluaran); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Kategori Detail', "txt_id_jenis_biaya", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array('class' => 'form-control', 'name' => 'id_jenis_biaya', 'id' => 'txt_id_jenis_biaya'), $list_jenis_biaya, @$id_jenis_biaya); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nominal', "", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'class' => 'form-control', 'disabled' => 'disabled'), DefaultCurrency($jumlah)); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Keterangan', "", array("class" => 'col-sm-4 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'textarea', 'class' => 'form-control', 'name' => 'keterangan'), @$keterangan); ?>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>
</form>
<script>
//	$("#txt_id_jenis_biaya").select2();
//	$("#txt_id_jenis_pengeluaran").select2().on('select2:select', function(e){
    $("#txt_id_jenis_pengeluaran").change(function () {
        var id_jenis_pengeluaran = $("#txt_id_jenis_pengeluaran").val();
        $("#txt_id_jenis_biaya").empty();
        $.post(
                baseurl + 'index.php/jenis_biaya/getdropdownbiaya/' + id_jenis_pengeluaran,
                {},
                function (resp) {
//				$("#txt_id_jenis_biaya").select2({data:resp});
                    $.each(resp, function (value, text) {
                        $("#txt_id_jenis_biaya").append('<option value="' + value + '">' + text + '</option>')
                    });
                }, 'json'
                );
    });
    $("#frm_kas").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/kas/update_kategori',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st) {
                    messagesuccess(data.msg);
                    table_header.fnDraw(false);
                    table_detail.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>