<section class="content-header">
    <h1>
        Create Kas
        <small>Data Entry</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Entry</li>
        <li class="active">Create Kas</li>
    </ol>


</section>
<style>
    .label-cek {
        cursor: pointer;
    }

    .datepicker-days table tbody tr td.disabled {
        color: #777777 !important;
        text-decoration: line-through;
    }
</style>
<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_jurnal_manual" class="form-horizontal form-groups-bordered validate" method="post">
                <input type="hidden" name="id_jurnal_manual" value="<?php echo @$id_jurnal_manual; ?>"/>
                <input type="hidden" name="is_checked" value="<?php echo @$is_checked; ?>"/>
                <input type="hidden" name="id_check" value="<?php echo @$id_check; ?>"/>
                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Tanggal', "txt_tanggal_jurnal", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_input(array('type' => 'text','autocomplete'=>"off", 'name' => 'tanggal_jurnal', 'value' => DefaultDatePicker(@$tanggal_jurnal), 'class' => 'form-control text-center datepicker', 'id' => 'txt_tanggal_jurnal', 'placeholder' => 'Tanggal', 'required' => 'required')); ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?= form_dropdown(array('name' => 'id_cabang', 'class' => 'form-control selectoption', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$id_cabang); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= form_label('Payment Voucher', "txt_payment_voucher", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?= form_input(array('name' => 'payment_voucher', 'class' => 'form-control', 'id' => 'txt_payment_voucher', 'placeholder' => 'Payment Voucher', 'rows' => 2, 'maxlenght' => 255), @$paymentvoucher); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                </div>


                <hr/>
                

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" id="field_biaya">
                            <?= form_label('Akun', "txt_id_gl_account", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?php
                                $field_gl_account = form_dropdown(array('name' => 'id_gl_account', 'class' => 'form-control', 'id' => 'dd_id_gl_account', 'placeholder' => 'Biaya'), DefaultEmptyDropdown(@$list_gl_account, "json", "Biaya"), @$id_gl_account);
                                if ($is_edit) {
                                    if ($edit_gl_account == 0) {
                                        $field_gl_account = form_input(array('type' => 'hidden', 'name' => 'id_gl_account'), $id_gl_account);
                                        $field_gl_account .= form_input(array('type' => 'text', 'disabled' => "disabled", 'class' => 'form-control'), getJenisBiayaKas($id_gl_account));
                                    }
                                }

                                echo $field_gl_account;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Jenis Transaksi', "txt_id_jenis_jurnal_manual", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_dropdown(array('name' => 'type', 'class' => 'form-control', 'id' => 'dd_type', 'placeholder' => 'Type'), DefaultEmptyDropdown(@$list_type, "json", "Type"), @$type); ?>
                            </div>
                        </div>
                    </div>


                </div>
                
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Jumlah', "txt_jumlah", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_input(array('type' => 'text', 'name' => 'jumlah', 'value' => DefaultCurrency(@$jumlah), 'class' => 'form-control text-right', 'id' => 'txt_jumlah', 'placeholder' => 'Jumlah', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Keperluan', "txt_keterangan", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_textarea(array('name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keperluan', 'rows' => 2, 'maxlenght' => 255), @$keterangan); ?>
                            </div>
                        </div>
                    </div>


                </div>
               
                <div class="row">
                    <div class="col-sm-2 text-center">
                    </div>
                    <div class="col-sm-8 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block" id="btt_save_detail">Save</button>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center">
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet-body form">
                            <table class="table table-striped table-bordered table-hover" id="mytable">
                                <tfoot><th></th><th></th><th class="text-right"></th><th class="text-right"></th><th class="text-right"></th><th></th><th></th></tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo base_url() ?>index.php/jurnal_manual" class="btn btn-default"
                       style="float:left;">Cancel</a>
                    <button type="button" class="btn btn-primary" id="btt_modal_ok" style="float:right">
                        Save
                    </button>
                </div>

            </form>
        </div>
    </div>
</section>
<script>
    var table;
    var listdetail = [];
   
    function RefreshGridDetail() {
        table.fnClearTable();
        if (listdetail.length > 0)
        {
            table.fnDraw(true);
            table.fnAddData(listdetail);
        }

    }
    $("#dd_id_jenis_transaksi").change(function () {
        listdetail = [];
        RefreshGridDetail();
        if($(this).val()=="33")
        {
            $(".divbank").css("display","block");
        }
        else
        {
             $(".divbank").css("display","none");
        }

    })
    $("#dd_id_project").change(function () {
        $(".summaryproject").html("");
        if ($(this).val() != "0") {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/project/GetTotalByCabang',
                data: $("#frm_jurnal_manual").serialize(),
                dataType: "json",
                success: function (data) {
                    if (data.data.length > 0) {
                        var message = "";
                        $(data.data).each(function (index, element) {
                            message += "Nama Jenis Transaksi : " + element.nama_jenis_transaksi + "<br/>";
                            message += "Debet : " + CommaMin(element.debet) + "<br/>";
                            message += "Kredit : " + CommaMin(element.kredit) + "<br/>";
                            message += "======================================<br/>";
                        });
                        $(".summaryproject").html(message);
                    }
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            })
        }
    });

    $(document).ready(function () {
        $("#txt_tanggal_jurnal").datepicker({
            autoclose: true,
            format: 'dd M yyyy',
            weekStart: 1,
            language: "id",
            todayHighlight: true,

        });
        $("select").select2();
      



        table = $("#mytable").dataTable({
            oLanguage: {
                sProcessing: "loading..."
            },
            data: listdetail,
            pageLength: 25,
            columns: [
                {data: "no", orderable: false, title: "No"}
                , {data: "nama_gl_account", orderable: true, title: "Group"}
                , {
                    data: "debet", orderable: false, title: "Debet", className: 'text-right',
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }
                }
                , {
                    data: "kredit", orderable: false, title: "Kredit", className: 'text-right',
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }
                }
                , {data: "keterangan", orderable: false, title: "Keterangan"}
                , {
                    data: "listreceipt", orderable: false, title: "Receipt",
                    mRender: function (data, type, row) {
                        var img = "";
                        $.each(data, function (key, value) {
                            img += "<img src='<?php echo base_url() ?>uploads/receipts/" + value.receiptfile + "' class='popimage'/>";
                        });
                        return img;
                    }
                }
                , {
                    data: "action", orderable: false, title: "", className: 'text-center', width: "100px",
                    mRender: function (data, type, row, meta) {
                        if (row['deleted_date'] != null)
                            return "Batal";
                        else
                            return "<a class=\"btn btn-xs btn-danger\" onclick=\"deletebiaya(" + meta.row + ")\"><i class=\"fa fa-close\"></i>&nbsp;Delete</a>";
                    }
                }
            ],
            fnFooterCallback: function (row, data, start, end, display) {
                var api = this.api();
                var preview_total = 0;
                var preview_debet = 0;
                var preview_kredit = 0;

                for (var zz = 0; zz < listdetail.length; zz++) {
                    preview_debet += parseInt(listdetail[zz].debet);
                    preview_kredit += parseInt(listdetail[zz].kredit);
                }
                preview_total = preview_debet - preview_kredit;
//				$( api.column( 4 ).footer() ).html(
//					Comma(preview_debet)
//				);
//				$( api.column( 5 ).footer() ).html(
//					Comma(preview_kredit)
//				);
//				$( api.column( 6 ).footer() ).html(
//					Comma(preview_total)
//				);
                $("#mytable tfoot th:nth-child(3)").html(Comma(preview_debet));
                $("#mytable tfoot th:nth-child(4)").html(Comma(preview_kredit));
                $("#mytable tfoot th:nth-child(5)").html('= ' + Comma(preview_total));
            }
        });

       



        
    })
  
    function deletebiaya(indexnum) {
        listdetail.splice(indexnum, 1);
        table.fnClearTable();
        if (listdetail.length > 0) {
            table.fnDraw(true);
            table.fnAddData(listdetail);
        }
    }

    $("#btt_modal_ok").click(function () {
        swal({
            title: "Apakah Kamu yakin akan menginput data berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Saya Yakin!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                var lanjut = true;

                if (lanjut) {
                    $.ajax({
                        type: 'POST',
                        url: baseurl + 'index.php/jurnal_manual/jurnal_manual_manipulate',
                        dataType: 'json',
                        data: $("#frm_jurnal_manual").serialize() + '&' + $.param({listdetail: listdetail}),
                        success: function (data) {
                            if (data.st) {
                                window.location.href = "<?php echo base_url() ?>index.php/jurnal_manual?tgl=" + $("#tanggal_jurnal").val();
                            } else {
                                messageerror(data.msg);
                            }

                        },
                        error: function (xhr, status, error) {
                            messageerror(xhr.responseText);
                        }
                    });
                }
            }
        });
        return false;
    });

    function RefreshImageList() {
        receipttable.fnClearTable();
        if (listreceipt.length > 0) {
            receipttable.fnDraw(true);
            receipttable.fnAddData(listreceipt);
        }
        RefreshImageThumbnails();
    }


    $("#frm_jurnal_manual").submit(function () {
        $.ajax({
            url: baseurl + 'index.php/jurnal_manual/save_detail',
            method: 'POST',
            dataType: "Json",
            data: $("#frm_jurnal_manual").serialize() + '&' + $.param({listdetail: listdetail}),
            success: function (data) {
//				var preview_total = 0;
//				var preview_debet = 0;
//				var preview_kredit = 0;
//				$("#mytable tfoot").remove();
                if (data.st) {
                    listdetail = data.listdetail;
//					$("#mytable").append('<tfoot><th></th><th></th><th></th><th></th><th class="text-right"></th><th class="text-right"></th><th class="text-right"></th><th></th><th></th></tfoot>');
                    table.fnClearTable();
                    table.fnDraw(true);
                    table.fnAddData(listdetail);

                    $("#dd_id_gl_account").val(0).trigger("change");
                    $("#dd_type").val(0).trigger("change");
                    $("#txt_keterangan").val("").empty();
                    $("#txt_jumlah").val(0);
                   
                    $("#btt_modal_ok").css("display", "block");
                   
                } else {
                    messageerror(data.msg);
                }
                $("#btt_save_detail").addClass("btn-primary").removeClass("btn-success");
            },
            error: function (xhr, status, error) {
                messageerror("Terjadi Kesalahan!");
                $("#btt_save_detail").addClass("btn-primary").removeClass("btn-success");
            },
            statusCode: {
                401: function () {
                    messageerror("Sesi Anda telah habis! Silahkan Login ulang");
                },
                404: function () {
                    messageerror("Halaman yang Anda akses tidak ditemukan!");
                },
                303: function () {
                    messageerror("Anda tidak memiliki Hak Akses!");
                }
            }
        });
        return false;
    })

</script>

