<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jurnal_manual extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_jurnal_manual');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';

        $css = [];
        $css[] = 'assets/plugins/iCheck/all.css';
        $data = [];
        $data['openmenu'] = "menudateentry";
        $data['currentmenu'] = "lijurnal_manual";
        //$this->load->model("jenis_transaksi/m_jenis_transaksi");
        //$data['listtransaksi'] = $this->m_jenis_transaksi->GetDropDownJenisTransaksi(GetDefaultCabang());
        $data['hak'] = GetRowData('#_group', ['id_group' => GetGroupId()]);
        $module = "K086";
        $header = "K059";
        $title = "Aliran Jurnal_manual";
        $data['title'] = "Aliran Jurnal_manual";
        CekModule($module);
        $data['form'] = $header;
        $data['formsubmenu'] = $module;
        $data['start_date'] = GetDateNow();
        $data['end_date'] = GetDateNow();
        $this->load->model("cabang/m_cabang");
        $data['list_cabang'] = $this->m_cabang->GetDropDownCabang();

        LoadTemplate($data, "jurnal_manual/v_jurnal_manual_index", $javascript, $css);
    }

    public function receipt_upload() {
        $config['upload_path'] = './uploads/receipts';
        $config['allowed_types'] = 'gif|jpg|png';
        $data = array();
        $data['st'] = false;
        $config['max_size'] = '1000000';


        if ($_FILES['userfile']['name'] != "") {
            $filename = date('Ymdhis') . '_' . preg_replace("/[^.\w]+/", "", $_FILES['userfile']['name']);

            $config['file_name'] = $filename;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $data = array('st' => false, 'error' => $this->upload->display_errors());
            } else {
                $data = array('st' => true, 'upload_data' => $this->upload->data(), "receiptfile" => array("receiptfile" => $filename));
            }
        } else {
            $data["msg"] = "Data Upload masih kosong";
        }
        echo json_encode($data);
    }

    public function getdatajurnal_manual() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $search);
        echo $this->m_jurnal_manual->GetDataJurnal_manual($search);
//        echo $this->db->last_query();
    }

    public function receipt_file() {

        $this->load->view("v_jurnal_manual_input_receipt");
    }

    public function create_jurnal_manual($tanggal = "") {
        $row = array();
        $curDate = $this->input->post('curDate');
        $row['tanggal_jurnal_manual'] = GetDateNow();

        if (!CheckEmpty($tanggal)) {
            $row['tanggal_jurnal_manual'] = DefaultDatePicker(urldecode($tanggal));
        }

        $module = "K086";
        $header = "K059";
        $title = "Aliran Jurnal_manual";
        $row['title'] = "Aliran Jurnal_manual";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;

        $this->load->model("cabang/m_cabang");
        $this->load->model("bank/m_bank");

        $paymentvoucher = "";




        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
    
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';

        $css = [];
        $this->load->model("cabang/m_cabang");

        $css[] = 'assets/plugins/iCheck/all.css';
        $this->load->model("gl/m_gl");
        $row['list_gl_account'] = $this->m_gl->GetDropDownGl();
        $row['paymentvoucher'] = $paymentvoucher;
        $row['id_jenis_transaksi'] = 7;
        $row['tgl_transfer'] = date('Y-m-d');
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $row['list_bank'] = $this->m_bank->GetDropDownBank();
        if ($curDate) {
            $row['tanggal_jurnal_manual'] = $curDate;
        }
        $row['button'] = 'Tambah';
        $row['terlihat'] = 'none';
        $row['is_edit'] = false;
        $row['list_type'] = array(array("id" => "D", "text" => "D"), array("id" => "K", "text" => "K"));


        LoadTemplate($row, 'jurnal_manual/v_jurnal_manual_manipulate', $javascript, $css);
    }

    public function jurnal_manual_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('tanggal_jurnal', 'tanggal jurnal_manual', 'trim|required');
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');

//        $this->form_validation->set_rules('biaya_bank', 'biaya bank', 'trim|numeric');
        if (CheckArray($model, 'listdetail'))
            $listdetail = $model["listdetail"];
        else {
            $listdetail = [];
        }
        if (count($listdetail) == 0) {
            $message .= "Detail Transaksi harus dimasukkan<br/>";
        }
        $totaldebet = 0;
        $totalkredit = 0;
        foreach ($listdetail as $det) {
            if ($det['type'] == "D") {
                $totaldebet += DefaultCurrencyDatabase($det['jumlah']);
            } else {
                $totalkredit += DefaultCurrencyDatabase($det['jumlah']);
            }
        }
        if ($totalkredit !== $totaldebet) {
            $message .= "Jumlah Debet dan Kredit harus sama<br/>";
        }


        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim');
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_jurnal_manual->Jurnal_manualManipulate($model);
            echo json_encode($status);
        }
    }

    public function viewdetail($id_jurnal_manual_master = 0) {
        $listdetail = $this->m_jurnal_manual->GetDataDetailJurnal_manual($id_jurnal_manual_master);
        $this->load->view("v_jurnal_manual_detail", array("listdetail" => $listdetail));
    }

    public function save_detail() {
        $model = $this->input->post();
        $message = '';
        $this->form_validation->set_rules('id_gl_account', 'Jurnal', 'trim|required');
        $this->form_validation->set_rules('type', 'Type', 'trim|required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
        if (CheckArray($model, 'listdetail'))
            $listdetail = $model["listdetail"];
        else {
            $listdetail = [];
        }

        if (@$model['id_jenis_transaksi'] != 33) {
            $model['id_bank'] = null;
        }
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $inputan = [];
            $this->load->model("gl/m_gl");
            $gl = $this->m_gl->GetOneGl($model["id_gl_account"], "id_gl_account", true);
            for ($i = 0; $i < count($listdetail); $i++) {
                $listdetail[$i]["no"] = $i + 1;
            }
            if ($gl != null) {
                $inputan["id_gl_account"] = $model["id_gl_account"];
                $inputan["jumlah"] = DefaultCurrencyDatabase($model["jumlah"]);
                $inputan["keterangan"] = $model["keterangan"];
                $inputan["no"] = count($listdetail) + 1;
                $inputan["nama_gl_account"] = $gl->nama_gl_account;
                $inputan["debet"] = 0;
                $inputan["kredit"] = 0;
                $inputan['type'] = $model['type'];
                if ($model["type"] == "D") {
                    $inputan["debet"] = $inputan["jumlah"];
                } else {
                    $inputan["kredit"] = $inputan["jumlah"];
                }
                $inputan["action"] = "<a class='btn btn-xs btn-danger' onclick='deletejurnal(" . $inputan["no"] . ")'><i class='fa fa-close'></i>&nbsp;Delete</a>";


                array_push($listdetail, $inputan);
            }
            echo json_encode(array('st' => true, 'listdetail' => $listdetail));
        }
    }

    function biaya_check($val) {
        $model = $this->input->post();
        if ($model['id_jenis_jurnal_manual'] == 2 && CheckEmpty($val)) {
            $this->form_validation->set_message('biaya_check', 'Silahkan pilih Jenis Biaya!');
            return FALSE;
        }

        return TRUE;
    }

    function getHakEdit() {
        $group = GetRowData('tic_group', ['id_group' => GetGroupId()]);
        return [
            'edit_jenis_transaksi' => $group['edit_jenis_transaksi'],
            'edit_jenis_biaya' => $group['edit_jenis_biaya'],
            'edit_tgl_transfer' => $group['edit_tgl_transfer'],
        ];
    }

    public function edit_jurnal_manual($id = 0) {
        $hakEdit = $this->getHakEdit();
        if ($hakEdit['edit_jenis_transaksi'] || $hakEdit['edit_jenis_biaya'] || $hakEdit['edit_tgl_transfer']) {
            // berarti bisa ngedit
        } else {
            echo "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><i class='icon fa fa-ban'></i> Anda Tidak mempunya hak untuk edit data!</div>";
            die();
        }

        $row = $this->m_jurnal_manual->GetOneJurnal_manual($id);

        if ($row) {
//            dumperror($row);
            $this->load->model("jenis_pengeluaran/m_jenis_pengeluaran");
            $this->load->model("jenis_transaksi/m_jenis_transaksi");
            $this->load->model("jenis_biaya/m_jenis_biaya");

            $row = (array) $row;
            $row['id_jenis_pengeluaran'] = GetScalarData('tic_jenis_biaya', 'id_jenis_biaya', $row['id_jenis_biaya'], 'id_jenis_pengeluaran');
            $row['button'] = 'Update';
            $row['listjenispengeluaran'] = $this->m_jenis_pengeluaran->GetDropDownJenisPengeluaran();
            $row['listjenistransaksi'] = $this->m_jenis_transaksi->GetDropDownJenisTransaksi();
            $row['listjenisbiaya'] = $this->m_jenis_biaya->GetDropDownJenisBiaya($row['id_jenis_pengeluaran']);
            $shift = $this->shift();
            $row['listshift'] = $shift['listshift'];
            $row['terlihat'] = 'none';
            if ($row['id_jenis_transaksi'] > 1) {
                $row['terlihat'] = 'block';
            }
            $row['is_edit'] = true;
            if ($row['id_karyawan']) {
                $karyawan = GetRowData('tic_karyawan', ['id_karyawan' => $row['id_karyawan']]);
                if ($karyawan) {
                    $row['nic_karyawan'] = $karyawan['nic_karyawan'];
                    $row['nama_karyawan'] = $karyawan['nama_karyawan'];
                }
            }
            $row = $row + $hakEdit;
            $this->load->view('jurnal_manual/v_jurnal_manual_manipulate', $row);
        } else {
            SetMessageSession(0, "Jurnal_manual cannot be found in database");
            redirect(site_url('jurnal_manual'));
        }
    }

    function shift() {
        $data = [];
        $this->load->model("shift/m_shift");
        $data['listshift'] = $this->m_shift->GetDropDownShift();
        $listshift = $this->m_shift->GetFullDataShift();
        $id_shift = 3;
        foreach ($listshift as $shift) {

            if (time() > strtotime($shift->start_shift) && time() <= strtotime($shift->end_shift)) {

                $id_shift = $shift->id_shift;
                break;
            }
        }
        $data['id_shift'] = $id_shift;

        return $data;
    }

    public function LihatReceipt($id_jurnal_manual) {
        $listreceipt = $this->m_jurnal_manual->ListReceiptJurnal_manual($id_jurnal_manual);
        echo "<div class='modal-body text-center'>";
        for ($i = 0; $i < count($listreceipt); $i++) {
            echo "<img src='" . base_url() . "uploads/receipts/" . $listreceipt[$i]->receipt_name . "'/><br/>";
        }
        if (count($listreceipt) == 0) {
            echo "<h1>No Receipt in this transaction</h1>";
        }
        echo "</div>";
    }

    public function jurnal_manual_batal() {
        $message = '';
        $this->form_validation->set_rules('id_jurnal_manual', 'Jurnal manual', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_jurnal_manual->Jurnal_ManualBatal($model['id_jurnal_manual']);
        }

        echo json_encode($result);
    }

    public function getUser() {
        $resp = ['st' => true];
        $id_pangkalan = $this->input->post('id_pangkalan');

        if (!$id_pangkalan) {
            $resp['st'] = false;
            $resp['msg'] = "Silahkan pilih pangkalan!";
        } else {
            $resp['list_agen'] = GetTableData('tic_user', 'id_user', 'nama_user', array('status' => 1, 'id_pangkalan' => $id_pangkalan), 'json');
        }

        echo json_encode($resp);
    }

    public function print_jurnal_manual($id) {

        $jurnal_manual = $this->m_jurnal_manual->GetJurnal_manualMaster($id);

        $data = [
            'jurnal_manual' => $jurnal_manual,
            'lojurnal_manuali' => $jurnal_manual[0]['nama_cabang'],
            'pembuat' => GetScalarData('tic_user', 'id_user', $jurnal_manual[0]['created_by'], 'nama_user'),
            'pengedit' => ($jurnal_manual[0]['updated_by']) ? GetScalarData('tic_user', 'id_user', $jurnal_manual[0]['updated_by'], 'nama_user') : null,
            'pencetak' => GetScalarData('tic_user', 'id_user', GetUserId(), 'nama_user'),
            'jenis' => getJenisTransaksi($jurnal_manual[0]['id_jenis_transaksi'])
        ];

        $this->load->view('v_jurnal_manual_print', $data);
    }

    public function get_nominal() {
        $resp = ['st' => false];
        $curDate = $this->input->post('curDate');
        if ($curDate) {
            if (GetDefaultCabang()) {
                try {
                    $this->load->model("jenis_transaksi/m_jenis_transaksi");
                    $listjenis_transaksi = $this->m_jenis_transaksi->GetDropDownJenisTransaksi();
                    foreach ($listjenis_transaksi as $key => $jenistran) {
                        $resp['trans_' . $key . '_debit'] = $this->m_jurnal_manual->getNominal(DefaultTanggalDatabase($curDate), 'debit', $key);
                        $resp['trans_' . $key . '_kredit'] = $this->m_jurnal_manual->getNominal(DefaultTanggalDatabase($curDate), 'kredit', $key);

                        if (isAllowViewSaldo()) {
                            $saldo_awal = $this->m_jurnal_manual->getSaldoAwal(DefaultTanggalDatabase($curDate), $key);
                            $resp['trans_' . $key . '_saldo_awal'] = $saldo_awal > 0 ? $saldo_awal : $this->m_jurnal_manual->getSaldo(DefaultTanggalDatabase($curDate), 'awal', $key);
                            $resp['trans_' . $key . '_saldo_akhir'] = $this->m_jurnal_manual->getSaldo(DefaultTanggalDatabase($curDate), 'akhir', $key);
                        }
                    }

                    $resp['st'] = true;
                } catch (Exception $ex) {
                    $resp['msg'] = $ex->getMessage();
                }
            } else {
                $resp['msg'] = "Cabang belum dipilih!";
            }
        } else {
            $resp['msg'] = "Tanggal belum dipilih!";
        }

        echo json_encode($resp);
    }

    function set_check($id_jurnal_manual) {

        $model = GetRowData('tic_jurnal_manual', ['id_jurnal_manual' => $id_jurnal_manual], [], [], [], true);

        $model['id_jurnal_manual'] = $id_jurnal_manual;
        $model['is_checked'] = 1;
        $model['id_check'] = GetUserId();

        $resp = $this->m_jurnal_manual->Jurnal_manualManipulate($model);

        echo json_encode($resp);
    }

    public function edit_kategori($id_jurnal_manual) {
        $this->load->model('jenis_biaya/m_jenis_biaya');
        $this->load->model("jenis_pengeluaran/m_jenis_pengeluaran");
        $data = $this->m_jurnal_manual->detailJurnal_manual($id_jurnal_manual);
        $data['list_jenis_pengeluaran'] = $this->m_jenis_pengeluaran->GetDropDownJenisPengeluaran($data['id_cabang']);
        $data['list_jenis_biaya'] = $this->m_jenis_biaya->GetDropDownJenisBiaya($data['id_jenis_pengeluaran'], $data['id_cabang']);
        echo $this->load->view('jurnal_manual/v_kategori_edit', $data, true);
    }

    public function update_kategori() {
        $resp = [];
        $id_jurnal_manual = $this->input->post('id_jurnal_manual');
        $id_jenis_biaya = $this->input->post('id_jenis_biaya');
        $keterangan = $this->input->post('keterangan');
        $resp['st'] = $this->m_jurnal_manual->updateJurnal_manual($id_jurnal_manual, ['id_jenis_biaya' => $id_jenis_biaya, 'keterangan' => $keterangan]);
        if ($resp['st']) {
            $resp['msg'] = 'Update berhasil';
        } else {
            $resp['msg'] = 'Update gagal!';
        }

        echo json_encode($resp);
    }

}

/* End of file Jurnal_manual.php */