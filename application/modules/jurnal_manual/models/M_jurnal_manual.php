<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_jurnal_manual extends CI_Model {

    public $table = '#_jurnal_manual';
    public $id = 'id_jurnal_manual';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;
    private $_prefix = "JM";

    function __construct() {
        parent::__construct();
    }

    function ListReceiptJurnal_Manual($id_jurnal_manual) {
        $this->db->from("#_jurnal_manual_receipt");
        $this->db->where(array("id_jurnal_manual" => $id_jurnal_manual));
        return $this->db->get()->result();
    }

    function GetDataJurnal_Manual($params = array()) {
        $this->load->library('datatables');
        $cols = '#_jurnal_manual.tanggal_jurnal ,#_jurnal_manual.status,  ,#_jurnal_manual.kode_jurnal_manual, nama_cabang,
                #_jurnal_manual.id_jurnal_manual,payment_voucher, #_jurnal_manual.deleted_date , #_user.username as pembuat,
                ifnull(group_concat(if(type=\'D\',concat(\'<li>\',\' &nbsp; \',CAST(FORMAT(#_jurnal_manual_detail.jumlah,2) AS CHAR)," ",nama_gl_account),\'\') SEPARATOR \'</li>\'),\'\') as debetdetails,
                ifnull(group_concat(if(type=\'K\',concat(\'<li>\',\' &nbsp; \',CAST(FORMAT(#_jurnal_manual_detail.jumlah,2) AS CHAR)," ",nama_gl_account),\'\') SEPARATOR \'</li>\'),\'\') as kreditdetails';
        $this->datatables->select($cols);
        $this->datatables->from('#_jurnal_manual');
        $this->datatables->join('#_jurnal_manual_detail', '#_jurnal_manual_detail.id_jurnal_manual = #_jurnal_manual.id_jurnal_manual', 'left');
        $this->datatables->join('#_gl_account', '#_gl_account.id_gl_account = #_jurnal_manual_detail.id_gl_account', 'left');
        $this->datatables->join('#_user', '#_jurnal_manual.created_by = #_user.id_user', 'left');
        $this->datatables->join('#_cabang', '#_jurnal_manual.id_cabang = #_cabang.id_cabang', 'left');

//$this->datatables->join('#_jenis_biaya', '#_jenis_biaya.id_jenis_biaya = #_jurnal_manual.id_jenis_biaya', 'left');
        //$this->datatables->join('#_jenis_pengeluaran', '#_jenis_pengeluaran.id_jenis_pengeluaran = #_jenis_biaya.id_jenis_pengeluaran', 'left');
        //$this->datatables->join('#_jurnal_manual','#_jurnal_manual.id_jurnal_manual=#_jurnal_manual.id_jurnal_manual','left');
        if (!CheckEmpty(@$params['id_cabang'])) {
            $this->datatables->where('#_jurnal_manual.id_cabang', @$params['id_cabang'], false);
            unset($params['id_cabang']);
        }

        $search = array_filter($params);
        $extra = [];
        if (CheckEmpty(@$params['kode_jurnal_manual'])) {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_jurnal_manual.tanggal_jurnal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_jurnal_manual.tanggal_jurnal"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_jurnal_manual.tanggal_jurnal"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }
            if (count($extra)) {
                foreach ($extra as $detext) {
                    $this->datatables->where_str($detext, NULL);
                }
            }
        } else {
            $this->datatables->where('#_jurnal_manual.kode_jurnal_manual', strtoupper(@$params['kode_jurnal_manual']));
        }

        $isedit = true;

        $isprint = true;
        $straction = '';

       
        $straction .= anchor("", '<i class="fa fa-pencil"></i>', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalDetail($1);return false;"));
    
        $this->datatables->group_by("#_jurnal_manual.id_jurnal_manual");
        $this->datatables->add_column('action', $straction, 'id_jurnal_manual');

        return $this->datatables->generate();
    }

    function GetDataJurnal_ManualMaster($params = array()) {
        $hak = GetRowData('#_group', ['id_group' => GetGroupId()]);
        $this->load->library('datatables');
        $cols = 'id_jurnal_manual, deleted_date, payment_voucher,
                kode_jurnal_manual, jumlah_total, is_approved, nama_jenis_transaksi,
                nama_karyawan,
                #_jurnal_manual.tanggal_jurnal_manual, nama_project, nama_user, #_jurnal_manual.created_date';
        $this->datatables->select($cols);
        $this->datatables->from('#_jurnal_manual');

        //add this line for join
        $this->datatables->join('#_user', '#_jurnal_manual.created_by = #_user.id_user', 'left');
        $this->datatables->join('#_project', '#_project.id_project = #_jurnal_manual.id_project', 'left');

        $this->datatables->join('#_karyawan', '#_karyawan.id_karyawan = #_jurnal_manual.id_karyawan', 'left');

        $this->datatables->join('#_jenis_transaksi', '#_jenis_transaksi.id_jenis_transaksi = #_jurnal_manual.id_jenis_transaksi', 'left');

        $this->datatables->where('#_jurnal_manual.id_cabang', GetDefaultCabang(), false);

        $search = array_filter($params);

        $this->datatables->where("#_jurnal_manual.tanggal_jurnal_manual", DefaultTanggalDatabase($search['tanggal']));

        $isbatal = $hak['hak_batal'];
        $isedit = false;
        if ($hak['edit_jenis_biaya'] || $hak['edit_jenis_transaksi'] || $hak['edit_tgl_transfer']) {
            $isedit = true;
        }
        $isprint = true;
        $straction = '';

        if ($isprint) {
            $straction .= anchor("", '<i class="fa fa-print"></i>', array('class' => 'btn btn-info btn-xs', "onclick" => "printJurnal_Manual($1);return false;"));
        }
        if ($isbatal) {
            $straction .= anchor("", '<i class="fa fa-trash"></i>', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletejurnal_manual($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_jurnal_manual');
        return $this->datatables->generate();
    }

    // get all
    function GetOneJurnal_Manual($keyword, $type = 'id_jurnal_manual') {
        $this->db->where($type, $keyword);
        $jurnal_manual = $this->db->get($this->table)->row();
        return $jurnal_manual;
    }

    function GetDataDetailJurnal_Manual($id_jurnal_manual = 0) {

        $this->db->from("#_jurnal_manual");
        $this->db->where(array("id_jurnal_manual" => $id_jurnal_manual));
        $this->db->select("id_jurnal_manual,#_jurnal_manual.tanggal_jurnal_manual as tanggal_transaksi,nama_jenis_biaya,nama_jenis_pengeluaran,jumlah,id_jenis_jurnal_manual");
        $this->db->join("#_jenis_biaya", "#_jenis_biaya.id_jenis_biaya=#_jurnal_manual.id_jenis_biaya");
        $this->db->join("#_jenis_pengeluaran", "#_jenis_pengeluaran.id_jenis_pengeluaran=#_jenis_biaya.id_jenis_pengeluaran");
        $listresult = $this->db->get()->result();
        return $listresult;
    }

    function Jurnal_ManualManipulate($model) {
        try {

            $this->db->trans_start();
            $listdetail = [];
            if (CheckArray($model, 'listdetail'))
                $listdetail = $model["listdetail"];

            $jurnal_manualmaster = array();
            $jurnal_manualmaster["kode_jurnal_manual"] = $this->getKodeJurnal_ManualMaster(@$model['id_cabang']);
            $jurnal_manualmaster['tanggal_jurnal'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
            $jurnal_manualmaster['id_cabang'] = ForeignKeyFromDb(@$model['id_cabang']);

            $jurnal_manualmaster['created_date'] = GetDateNow();
            $jurnal_manualmaster['created_by'] = ForeignKeyFromDb(GetUserId());
            $jurnal_manualmaster['payment_voucher'] = $model["payment_voucher"];
            $jurnal_manualmaster['deleted_date'] = null;
            $jurnal_manualmaster['status'] = 1;
            $this->db->insert("#_jurnal_manual", $jurnal_manualmaster);
            $id_jurnal_manual = $this->db->insert_id();
            $arrbukubesar=[];
            for ($i = 0; $i < count($listdetail); $i++) {

                $jurnal_manual['id_jurnal_manual'] = $id_jurnal_manual;
                $jurnal_manual['type'] = ForeignKeyFromDb($listdetail[$i]['type']);
                $jurnal_manual['id_gl_account'] = ForeignKeyFromDb($listdetail[$i]['id_gl_account']);
                $jurnal_manual['jumlah'] = DefaultCurrencyDatabase($listdetail[$i]['jumlah']);
                $jurnal_manual['created_date'] = GetDateNow();
                $jurnal_manual['created_by'] = ForeignKeyFromDb(GetUserId());
                $jurnal_manual['status'] = 1;
                $jurnal_manual['keterangan'] = $listdetail[$i]['keterangan'];
                $jurnal_manual['deleted_date'] = null;
//            $model['biaya_bank'] = DefaultCurrencyDatabase($model['biaya_bank']);
                $this->db->insert("#_jurnal_manual_detail", $jurnal_manual);
                $id_jurnal_detail = $this->db->insert_id();
                $this->db->from("#_buku_besar");
                $this->db->where(array("id_gl_account" => $jurnal_manual['id_gl_account'], "dokumen" => $jurnal_manualmaster["kode_jurnal_manual"]));
                $akundetail = $this->db->get()->row();
                if (CheckEmpty($akundetail)) {
                    $akundetailinsert = array();
                    $akundetailinsert['id_gl_account'] = $jurnal_manual['id_gl_account'];
                    $akundetailinsert['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
                    $akundetailinsert['id_cabang'] = ForeignKeyFromDb(@$model['id_cabang']);
                    $akundetailinsert['keterangan'] = "Jurnal Manual ". $listdetail[$i]['keterangan'];
                    $akundetailinsert['dokumen'] =  $jurnal_manualmaster["kode_jurnal_manual"];
                    $akundetailinsert['subject_name'] = "";
                    $akundetailinsert['id_subject'] = "";
                    $akundetailinsert['id_detail']=$id_jurnal_detail;
                    $akundetailinsert['debet'] = ForeignKeyFromDb($listdetail[$i]['type'])=='D'?DefaultCurrencyDatabase($listdetail[$i]['jumlah']):0;
                    $akundetailinsert['kredit'] = ForeignKeyFromDb($listdetail[$i]['type'])=='K'?DefaultCurrencyDatabase($listdetail[$i]['jumlah']):0;
                    $akundetailinsert['created_date'] = GetDateNow();
                    $akundetailinsert['created_by'] = GetUserId();
                    $this->db->insert("#_buku_besar", $akundetailinsert);
                    $arrbukubesar[] = $this->db->insert_id();
                } else {
                    $akundetailupdate['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal_jurnal']);
                    $akundetailupdate['id_cabang'] = ForeignKeyFromDb(@$model['id_cabang']);
                    $akundetailupdate['keterangan'] = "Jurnal Manual ". $listdetail[$i]['keterangan'];
                    $akundetailupdate['dokumen'] = $jurnal_manualmaster["kode_jurnal_manual"];
                    $akundetailupdate['subject_name'] ="";
                    $akundetailupdate['id_subject'] = "";
                    $akundetailupdate['id_detail']=$id_jurnal_detail;
                    $akundetailupdate['debet'] = ForeignKeyFromDb($listdetail[$i]['type'])=='D'?DefaultCurrencyDatabase($listdetail[$i]['jumlah']):0;
                    $akundetailupdate['kredit'] = ForeignKeyFromDb($listdetail[$i]['type'])=='K'?DefaultCurrencyDatabase($listdetail[$i]['jumlah']):0;
                    $akundetailupdate['updated_date'] = GetDateNow();
                    $akundetailupdate['updated_by'] = GetUserId();
                    $this->db->update("#_buku_besar", $akundetailupdate, array("id_buku_besar" => $akundetail->id_buku_besar));
                    $arrbukubesar[] = $akundetail->id_buku_besar;
                }
                if (count($arrbukubesar) > 0) {
                    $this->db->where_not_in("id_buku_besar", $arrbukubesar);
                    $this->db->where(array("dokumen" => $jurnal_manualmaster["kode_jurnal_manual"]));
                    $this->db->update("#_buku_besar", array("debet" => 0, "kredit" => "0", "updated_date" => GetDateNow(), "updated_by" => GetUserId()));
                }
            }




            $message = "";
            $st = false;

            if (CheckEmpty(@$model['id_jurnal_manual'])) {
                SetMessageSession($st, "Jurnal_Manual dengan code " . $jurnal_manualmaster["kode_jurnal_manual"] . " berhasil dimasukkan ke dalam database");
                $message = "Transaksi berhasil diinput";
                $this->session->set_userdata(array("id_jurnal_manual" => $id_jurnal_manual));
                $st = true;
                $id = $id_jurnal_manual;
            } else {
                $jurnal_manual['is_checked'] = ForeignKeyFromDb($model['is_checked']);
                $jurnal_manual['id_check'] = ForeignKeyFromDb($model['id_check']);
                $jurnal_manual['updated_date'] = GetDateNow();
                $jurnal_manual['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $jurnal_manual, array("id_jurnal_manual" => $model['id_jurnal_manual']));

                $message = "Transaksi berhasil diupdate";
                $st = true;
                $id = $id_jurnal_manual;
            }
            $this->session->set_userdata("id_jurnal_manual", $id);
            $this->session->set_userdata("kode_jurnal_manual", $jurnal_manualmaster["kode_jurnal_manual"]);
            $this->db->trans_commit();
            return array("st" => $st, "msg" => $message, "id" => $id);
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function Jurnal_ManualBatal($id_jurnal_manual) {
        $resp = ['st' => true];
        try {
            $row=$this->GetOneJurnal_Manual($id_jurnal_manual);
            
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = ForeignKeyFromDb(GetUserId());
            $model['status'] =2;
            $this->db->update("#_jurnal_manual", $model, array('id_jurnal_manual' => $id_jurnal_manual));
            
            $this->db->update("#_buku_besar",array("debet"=>0,"kredit"=>0,"updated_date"=> GetDateNow(),"updated_by"=> GetUserId()),array("dokumen"=>$row->kode_jurnal_manual));
            
            $resp['msg'] = "Transaksi Berhasil dibatalkan";
        } catch (Exception $ex) {
            $resp['st'] = false;
            $resp['msg'] = $ex;
        }

        return $resp;
    }

    function getKodeJurnal_Manual($jurnal_manualmaster) {


        $this->db->select("(CAST(SUBSTR(kode_jurnal_manual, " . (int) -4 . ", 4) AS UNSIGNED) + 1) AS next", FALSE);
        $this->db->from("#_jurnal_manual_detail");
        $this->db->where("kode_jurnal_manual LIKE '" . $jurnal_manualmaster . "%'");
        $this->db->order_by($this->id, 'DESC');
        $this->db->limit(1);

        $kode = $this->db->get()->row();
        $next = "0001";
        if ($kode) {
            $next = str_pad($kode->next, 4, '0', STR_PAD_LEFT);
        }

        return $jurnal_manualmaster . $next;
    }

    function getKodeJurnal_ManualMaster($id_cabang) {
        $date = date('ym');
        $kodeCabang = GetScalarData('#_cabang', 'id_cabang', $id_cabang, "kode_cabang");
        $kode_jurnal_manual = $this->_prefix . str_pad($id_cabang, 4, '0', STR_PAD_LEFT) . $date;
        if ($kodeCabang) {
            $kode_jurnal_manual = $this->_prefix . $kodeCabang . $date;
        }

        $this->db->select("(CAST(SUBSTR(kode_jurnal_manual, " . (int) -4 . ", 4) AS UNSIGNED) + 1) AS next", FALSE);
        $this->db->from("#_jurnal_manual");
        $this->db->where("kode_jurnal_manual LIKE '" . $kode_jurnal_manual . "%'");
        $this->db->order_by("id_jurnal_manual", 'DESC');
        $this->db->limit(1);

        $kode = $this->db->get()->row();
        $next = "0001";

        if ($kode) {
            $next = str_pad($kode->next, 4, '0', STR_PAD_LEFT);
        }


        return $kode_jurnal_manual . $next;
    }

    function getNominal($tgl, $tipe, $id_jenis_transaksi) {
        $this->db->select("SUM(jumlah) AS nominal");
        $this->db->from($this->table);
        $this->db->join("#_jurnal_manual", "#_jurnal_manual.id_jurnal_manual=#_jurnal_manual.id_jurnal_manual", "left");
        $this->db->where('#_jurnal_manual.id_jenis_transaksi', $id_jenis_transaksi, false);
        $this->db->where('#_jurnal_manual.id_cabang', GetDefaultCabang(), false);
        $this->db->where('#_jurnal_manual.tanggal_jurnal_manual', $tgl);
        switch ($tipe) {
            case 'debit' :
                $this->db->where('id_jenis_jurnal_manual', 1, false);
                $this->db->where('id_jenis_biaya <>', 203, false);
                break;
            case 'kredit' : $this->db->where('id_jenis_jurnal_manual', 2, false);
                break;
        }
        $this->db->where('#_jurnal_manual.deleted_date IS NULL');

        $result = $this->db->get()->row_array();

        return (int) $result['nominal'];
    }

    function getSaldo($tgl, $tipe, $id_jenis_transaksi) {
        $this->db->select("(SUM(IF(id_jenis_jurnal_manual = 1, jumlah, 0)) - SUM(IF(id_jenis_jurnal_manual = 2, jumlah, 0))) AS nominal");
        $this->db->from($this->table);
        $this->db->join("#_jurnal_manual", "#_jurnal_manual.id_jurnal_manual=#_jurnal_manual.id_jurnal_manual", "left");
        $this->db->where('#_jurnal_manual.id_jenis_transaksi', $id_jenis_transaksi, false);
        $this->db->where('#_jurnal_manual.id_cabang', GetDefaultCabang(), false);
        switch ($tipe) {
            case 'awal' :
                $this->db->where('#_jurnal_manual.tanggal_jurnal_manual <', $tgl);
//                $this->db->where('#_jurnal_manual.id_jenis_biaya', 203, false);
                break;
            case 'akhir' : $this->db->where('#_jurnal_manual.tanggal_jurnal_manual <=', $tgl);
                break;
        }
        $this->db->where('#_jurnal_manual.deleted_date IS NULL');

        $result = $this->db->get()->row_array();

        return (int) $result['nominal'];
    }

    function getSaldoAwal($tgl, $id_jenis_transaksi) {
        $this->db->select("(SUM(IF(id_jenis_jurnal_manual = 1, jumlah, 0)) - SUM(IF(id_jenis_jurnal_manual = 2, jumlah, 0))) AS nominal");
        $this->db->from($this->table);
        $this->db->join("#_jurnal_manual", "#_jurnal_manual.id_jurnal_manual=#_jurnal_manual.id_jurnal_manual", "left");
        $this->db->where('#_jurnal_manual.id_jenis_transaksi', $id_jenis_transaksi, false);
        $this->db->where('#_jurnal_manual.id_cabang', GetDefaultCabang(), false);
        $this->db->where('#_jurnal_manual.tanggal_jurnal_manual =', $tgl);
        $this->db->where('#_jurnal_manual.id_jenis_biaya', 203, false);
        $this->db->where('#_jurnal_manual.deleted_date IS NULL');

        $result = $this->db->get()->row_array();

        return (int) $result['nominal'];
    }

    function detailJurnal_Manual($id_jurnal_manual) {
        $select = '#_jurnal_manual.*, kode_jurnal_manual, payment_voucher, id_jenis_transaksi, id_jenis_pengeluaran, #_jurnal_manual.id_cabang';
        $this->db->select($select);
        $this->db->from($this->table);
        $this->db->join('#_jurnal_manual', '#_jurnal_manual.id_jurnal_manual = #_jurnal_manual.id_jurnal_manual');
        $this->db->join('#_jenis_biaya', '#_jenis_biaya.id_jenis_biaya = #_jurnal_manual.id_jenis_biaya');
        $this->db->where('id_jurnal_manual', (int) $id_jurnal_manual, false);

        return $this->db->get()->row_array();
    }

    function updateJurnal_Manual($id, $model) {
        $this->db->where('id_jurnal_manual', $id, false);
        return $this->db->update('#_jurnal_manual', $model);
    }

}
