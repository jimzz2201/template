<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_method extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_payment_method');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K053";
        $header = "K001";
        $title = "Payment Method";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "payment_method/v_payment_method_index", $javascript);
        
    } 
    public function get_one_payment_method() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_payment_method', 'Payment Method', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_payment_method->GetOnePayment_method($model["id_payment_method"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Payment Method tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatapayment_method() {
        header('Content-Type: application/json');
        echo $this->m_payment_method->GetDatapayment_method();
    }
    
    public function create_payment_method() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	    $module = "K053";
        $header = "K001";
        $row['title'] = "Add Payment Method";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
        $this->load->model("bank/m_bank");
        $row['list_bank'] = $this->m_bank->GetDropDownBank();
	    $this->load->view('payment_method/v_payment_method_manipulate', $row);       
    }
    
    public function payment_method_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('nama_payment_method', 'Nama Payment Method', 'trim|required');
	$this->form_validation->set_rules('id_bank', 'Id Bank', 'trim');
	$this->form_validation->set_rules('chargeable', 'Chargeable', 'trim');
	$this->form_validation->set_rules('charge_value', 'Charge Value', 'trim|numeric');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_payment_method->Payment_methodManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_payment_method($id=0) 
    {
        $row = $this->m_payment_method->GetOnePayment_method($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K053";
            $header = "K001";
            $row['title'] = "Edit Payment Method";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
            $this->load->model("bank/m_bank");
            $row['list_bank'] = $this->m_bank->GetDropDownBank();
	    $this->load->view('payment_method/v_payment_method_manipulate', $row);
        } else {
            SetMessageSession(0, "Payment_method cannot be found in database");
            redirect(site_url('payment_method'));
        }
    }
    
    public function payment_method_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_payment_method', 'Payment_method', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_payment_method->Payment_methodDelete($model['id_payment_method']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Payment_method.php */