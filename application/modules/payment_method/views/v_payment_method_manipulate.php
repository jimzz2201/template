<div class="modal-header">
        Payment_method <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_payment_method" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_payment_method" value="<?php echo @$id_payment_method; ?>" /> 
	<div class="form-group">
		<?= form_label('Nama Payment Method', "txt_nama_payment_method", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-6">
			<?= form_input(array('type' => 'text', 'name' => 'nama_payment_method', 'value' => @$nama_payment_method, 'class' => 'form-control', 'id' => 'txt_nama_payment_method', 'placeholder' => 'Nama Payment Method')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Bank', "txt_id_bank", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-6">
		<?= form_dropdown(array('type' => 'text', 'name' => 'id_bank', 'class' => 'form-control', 'id' => 'dd_id_bank', 'placeholder' => 'bank'), DefaultEmptyDropdown($list_bank,"json","Bank"), @$id_bank); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Chargeable', "txt_chargeable", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-6">
			<?= form_dropdown(array("selected" => @$chargeable, "name" => "chargeable"), array('1' => 'Yes', '0' => 'No'), @$chargeable, array('class' => 'form-control', 'id' => 'chargeable')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Charge Value', "txt_charge_value", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-6">
			<?= form_input(array('type' => 'text', 'name' => 'charge_value', 'value' => DefaultCurrency(@$charge_value), 'class' => 'form-control', 'id' => 'txt_charge_value', 'placeholder' => 'Charge Value' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-6">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_payment_method").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/payment_method/payment_method_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>