<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_payment_method extends CI_Model {

    public $table = '#_payment_method';
    public $id = 'id_payment_method';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatapayment_method() {
        $this->load->library('datatables');
        $this->datatables->select('id_payment_method,nama_payment_method,nama_bank,chargeable,charge_value,dgmi_payment_method.status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_bank', 'dgmi_bank.id_bank = dgmi_payment_method.id_bank', "left");
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editpayment_method($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletepayment_method($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_payment_method');
        return $this->datatables->generate();
    }

    // get all
    function GetOnePayment_method($keyword, $type = 'id_payment_method') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $payment_method = $this->db->get($this->table)->row();
        return $payment_method;
    }

    function Payment_methodManipulate($model) {
        try {
            $model['id_bank'] = ForeignKeyFromDb($model['id_bank']);
            $model['charge_value'] = DefaultCurrencyDatabase($model['charge_value']);

            if (CheckEmpty($model['id_payment_method'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                return array("st" => true, "msg" => "Payment_method successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_payment_method" => $model['id_payment_method']));
                return array("st" => true, "msg" => "Payment_method has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownPayment_method() {
        $listpayment_method = GetTableData($this->table, 'id_payment_method', 'nama_payment_method', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listpayment_method;
    }

    function Payment_methodDelete($id_payment_method) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_payment_method' => $id_payment_method));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Payment_method has been deleted from database");
    }

}
