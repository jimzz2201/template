<div class="modal-header">
        Jenis_plat <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_jenis_plat" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_jenis_plat" value="<?php echo @$id_jenis_plat; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Jenis Plat', "txt_kode_jenis_plat", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?php $mergearray=array();
                            if(!CheckEmpty(@$id_jenis_plat))
                            {
                                $mergearray['readonly']="readonly";
                                $mergearray['name'] = "kode_jenis_plat";
                            }
                            else
                            {
                                $mergearray['name'] = "kode_jenis_plat";
                            }?>
                    <?= form_input(array_merge($mergearray,array('type' => 'text', 'value' => @$kode_jenis_plat, 'class' => 'form-control', 'id' => 'txt_kode_jenis_plat', 'placeholder' => 'Kode Jenis Plat'))); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Jenis Plat', "txt_nama_jenis_plat", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_jenis_plat', 'value' => @$nama_jenis_plat, 'class' => 'form-control', 'id' => 'txt_nama_jenis_plat', 'placeholder' => 'Nama Jenis Plat')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_jenis_plat").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/jenis_plat/jenis_plat_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>