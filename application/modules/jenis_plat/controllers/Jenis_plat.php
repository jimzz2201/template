<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jenis_plat extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_jenis_plat');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K051";
        $header = "K001";
        $title = "Jenis Plat";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "jenis_plat/v_jenis_plat_index", $javascript);
        
    } 
    public function get_one_jenis_plat() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_jenis_plat', 'Jenis Plat', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_jenis_plat->GetOneJenis_plat($model["id_jenis_plat"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Jenis Plat tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatajenis_plat() {
        header('Content-Type: application/json');
        echo $this->m_jenis_plat->GetDatajenis_plat();
    }
    
    public function create_jenis_plat() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	    $module = "K051";
        $header = "K001";
        $row['title'] = "Add Jenis Plat";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	    $this->load->view('jenis_plat/v_jenis_plat_manipulate', $row);       
    }
    
    public function jenis_plat_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_jenis_plat', 'Kode Jenis Plat', 'trim|required');
	$this->form_validation->set_rules('nama_jenis_plat', 'Nama Jenis Plat', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_jenis_plat->Jenis_platManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_jenis_plat($id=0) 
    {
        $row = $this->m_jenis_plat->GetOneJenis_plat($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K051";
            $header = "K001";
            $row['title'] = "Edit Jenis Plat";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    $this->load->view('jenis_plat/v_jenis_plat_manipulate', $row);
        } else {
            SetMessageSession(0, "Jenis_plat cannot be found in database");
            redirect(site_url('jenis_plat'));
        }
    }
    
    public function jenis_plat_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_jenis_plat', 'Jenis_plat', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_jenis_plat->Jenis_platDelete($model['id_jenis_plat']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Jenis_plat.php */