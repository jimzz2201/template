<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_jenis_plat extends CI_Model
{

    public $table = '#_jenis_plat';
    public $id = 'id_jenis_plat';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatajenis_plat() {
        $this->load->library('datatables');
        $this->datatables->select('id_jenis_plat,kode_jenis_plat,nama_jenis_plat,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_jenis_plat.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editjenis_plat($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletejenis_plat($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_jenis_plat');
        return $this->datatables->generate();
    }

    // get all
    function GetOneJenis_plat($keyword, $type = 'id_jenis_plat') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $jenis_plat = $this->db->get($this->table)->row();
        return $jenis_plat;
    }

    function Jenis_platManipulate($model) {
        try {
                $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_jenis_plat'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		return array("st" => true, "msg" => "Jenis_plat successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_jenis_plat" => $model['id_jenis_plat']));
		return array("st" => true, "msg" => "Jenis_plat has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownJenis_plat() {
        $listjenis_plat = GetTableData($this->table, 'id_jenis_plat', 'nama_jenis_plat', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listjenis_plat;
    }

    function Jenis_platDelete($id_jenis_plat) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_jenis_plat' => $id_jenis_plat));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Jenis_plat has been deleted from database");
    }


}