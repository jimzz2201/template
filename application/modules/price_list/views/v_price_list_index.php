
<section class="content-header">
    <h1>
        Price_list
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Price_list</li>
    </ol>


</section>
    
<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">

                <?php echo anchor(site_url('price_list/create_price_list'), 'Create', 'class="btn btn-success"'); ?>
	    </div>
        </div>
         <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body form">
        <table class="table table-striped table-bordered table-hover" id="mytable">
	    
        </table>
        </div>
                </div>

            </div>
        </div>

</section>
        <script type="text/javascript">
             var table;
            function deleteprice_list(id_price_list) {


        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/price_list/price_list_delete',
                dataType: 'json',
                data: {
                    id_price_list: id_price_list
                },
                success: function (data) {
                    if (data.st)
                    {
                        messagesuccess(data.msg);
                        table.fnDraw(false);
                    }
                    else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
       }});


    }
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                table = $("#mytable").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    ajax: {"url": "price_list/getdataprice_list", "type": "POST"},
                    columns: [
                        {
                           data: "id_price_list",
                           title: "Kode",
                           orderable: false
                        },
			{data: "id_type_unit" , orderable:false , title :"Id Type Unit"},
			{data: "berlaku_dari" , orderable:false , title :"Berlaku Dari",
                        mRender: function (data, type, row) {
                            return  DefaultDateFormat(data);
                        }},
			{data: "berlaku_sampai" , orderable:false , title :"Berlaku Sampai",
                        mRender: function (data, type, row) {
                            return  DefaultDateFormat(data);
                        }},
			{data: "vrf_default" , orderable:false , title :"Vrf Default",
                        mRender: function (data, type, row) {
                            return Comma(data==undefined?0:data);
                        }},
			{data: "finance_program" , orderable:false , title :"Finance Program",
                        mRender: function (data, type, row) {
                            return Comma(data==undefined?0:data);
                        }},
			{data: "discount_option" , orderable:false , title :"Discount Option",
                        mRender: function (data, type, row) {
                            return Comma(data==undefined?0:data);
                        }},
			{data: "status" , orderable:false , title :"Status",
                        mRender: function (data, type, row) {
                            return data==1?"Active":"Not Active";
                        }},
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center"
                        }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
    