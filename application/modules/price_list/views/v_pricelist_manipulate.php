<div class="modal-header">
        Price List <?php echo @$id_type_unit ?>
    </div>
<div class="modal-body">
    <form id="frm_pricelist" class="form-horizontal form-groups-bordered validate" method="post" autocomplete="off">
    <input type="hidden" name="id_price_list" value="<?php echo @$id_price_list; ?>" /> 
    <input type="hidden" name="id_type_unit" id="id_type_unit" value="<?php echo @$id_type_unit; ?>" />
    <div class="form-group">
        <?= form_label('Berlaku Dari', "txt_berlaku_dari", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text','readonly'=>'readonly', 'name' => 'berlaku_dari', 'value' => DefaultDatePicker(@$berlaku_dari), 'class' => 'form-control', 'id' => 'txt_berlaku_dari', 'placeholder' => 'Berlaku Dari')); ?>
        </div>
    </div>
    <div id="topTable"></div>
    <div class="form-group">
        <?= form_label('Vrf Default', "txt_vrf_default", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text', 'name' => 'vrf_default', 'value' => DefaultCurrency(@$vrf_default), 'class' => 'form-control', 'id' => 'txt_vrf_default', 'placeholder' => 'Vrf Default' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Finance Program', "txt_finance_program", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text', 'name' => 'finance_program', 'value' => DefaultCurrency(@$finance_program), 'class' => 'form-control', 'id' => 'txt_finance_program', 'placeholder' => 'Finance Program' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Discount Option', "txt_discount_option", array("class" => 'col-sm-3 control-label')); ?>
        <div class="col-sm-4">
            <?= form_input(array('type' => 'text', 'name' => 'discount_option', 'value' => DefaultCurrency(@$discount_option), 'class' => 'form-control', 'id' => 'txt_discount_option', 'placeholder' => 'Discount Option' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>
    </form>
</div>
<script>
    
    $('#modalbootstrap').on('show.bs.modal', function() {
        getTop();
    })
    function getTop () {
        // alert('getT top');
        var id_type_unit = $("#id_type_unit").val();
        $.ajax({
            url: baseurl + 'index.php/price_list/get_top',
            data: 'id_type_unit=' + id_type_unit,
            type: 'post',
            success: function (data) {
                $("#topTable").html(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            },
        })
    }
    $("#frm_pricelist").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/price_list/price_list_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    getPriceTable();
                    // table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>