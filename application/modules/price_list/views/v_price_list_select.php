<div class="modal-header">
  Price List
</div>
<div class="modal-body">
  <section class="content">
    <div class="box box-default">
        <div class="box-body">
          <div id="notification" ></div>
          <div class=" headerbutton"><input type="hidden" name="id_type_unit" id="id_type_unit" value="<?php echo @$id_type_unit; ?>" /> </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="portlet-body form"  id="tbl">
              <!-- <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;"></table> -->
            </div>
          </div>
        </div>
     </div>
  </section>
  <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal" >Tutup</button>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#modalbootstrap').on('shown.bs.modal', function () {
      getPriceTable();
    });
  });
  window.onload = function(){
      getPriceTable();
  }

  function getPriceTable () {
      var id_type_unit = $("#id_type_unit").val();
      $.ajax({
          url: baseurl + 'index.php/price_list/getdataprice_list/' + id_type_unit,
          success: function (data) {
              $("#tbl").html(data);
          },
          error: function (xhr, status, error) {
              messageerror(xhr.responseText);
          },
      })
  }
</script>
<style>
  th {
        text-align: center !important;
    }
    .right {
        text-align: right !important;
    }
    .headTable {
        background-color: #0396B0 !important;
        color: #FFFFFF !important;
    }
</style>