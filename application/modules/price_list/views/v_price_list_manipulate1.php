<section class="content-header">
    <h1>
        Price List <?= @$button ?>
        <small>Price List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Price List</li>
        <li class="active">Price List <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <form id="frm_price_list" class="form-horizontal form-groups-bordered validate" method="post">
                            <input type="hidden" name="id_price_list" value="<?php echo @$id_price_list; ?>" />
                            <div class="form-group">
                                <?= form_label('Berlaku Dari', "txt_berlaku_dari", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'berlaku_dari', 'value' => DefaultDatePicker(@$berlaku_dari), 'class' => 'form-control datepicker', 'id' => 'txt_berlaku_dari', 'placeholder' => 'Berlaku Dari')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Berlaku Sampai', "txt_berlaku_sampai", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'berlaku_sampai', 'value' => DefaultDatePicker(@$berlaku_sampai), 'class' => 'form-control datepicker', 'id' => 'txt_berlaku_sampai', 'placeholder' => 'Berlaku Sampai')); ?>
                                </div>
                            </div>
                            <div id="topTable"></div>
                            <div class="form-group">
                                <?= form_label('Vrf Default', "txt_vrf_default", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'vrf_default', 'value' => DefaultCurrency(@$vrf_default), 'class' => 'form-control', 'id' => 'txt_vrf_default', 'placeholder' => 'Vrf Default' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Finance Program', "txt_finance_program", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'finance_program', 'value' => DefaultCurrency(@$finance_program), 'class' => 'form-control', 'id' => 'txt_finance_program', 'placeholder' => 'Finance Program' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Discount Option', "txt_discount_option", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_input(array('type' => 'text', 'name' => 'discount_option', 'value' => DefaultCurrency(@$discount_option), 'class' => 'form-control', 'id' => 'txt_discount_option', 'placeholder' => 'Discount Option' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                <div class="col-sm-4">
                                    <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <a href="<?php echo base_url().'index.php/price_list' ?>" class="btn btn-default"  >Cancel</a>
                                <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                <button type="button" class="btn btn-primary" id="settingTop">Setting TOP</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });
        $(".select2").select2();
    });
    window.onload = function () {
        getTop();
    }
    function getTop () {
        alert('getT top');
        var idTypeUnit = $("#dd_type_unit").val();
        $.ajax({
            url: baseurl + 'index.php/price_list/get_top',
            data: 'id_type_unit=' + idTypeUnit,
            type: 'post',
            success: function (data) {
                $("#topTable").html(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            },
        })
    }
    $("#frm_price_list").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/price_list/price_list_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/price_list';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>