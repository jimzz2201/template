<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Price_list extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_price_list');
        $this->load->model('type_unit/m_type_unit');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K072";
        $header = "K001";
        $title = "Price List";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        CekModule($module);
        LoadTemplate($model, "price_list/v_price_list_index", $javascript);
        
    } 
    public function get_one_price_list() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_price_list', 'Price List', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_price_list->GetOnePrice_list($model["id_price_list"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Price List tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function get_top() {
        $idTypeUnit = $this->input->post('id_type_unit');
        $html = '';
        $top = $this->m_price_list->GetTop($idTypeUnit);
        asort($top);
        foreach($top as $val){
            $html .= "<div class='form-group'>
                        <div class='col-sm-3'>
                            <label for='txt_berlaku_dari' class='col-sm-3 control-label'>TOP</label>
                            <div class='col-sm-9'><input type='text' name='top[]' value='".$val['top']."' class='form-control' placeholder='Top' onkeypress='return isNumberKey(event);'></div>
                        </div>
                        <div class='col-sm-4'>
                            <input type='text' name='price[]' value='0' class='form-control' id='txt' placeholder='Price' onkeyup='javascript:this.value=Comma(this.value);' onkeypress='return isNumberKey(event);'>
                        </div>
                    </div>";
        }
        echo $html;
    }
    public function getdataprice_list_json($idTypeUnit) {
        // $idTypeUnit = $this->input->post('id_type_unit');
        // header('Content-Type: application/json');
        // echo $this->m_price_list->GetDataprice_list($idTypeUnit);
        
        $priceList = $this->m_price_list->GetPriceList($idTypeUnit);
        foreach($priceList as $key=>$val) {
            $priceList[$key]['detail'] = $this->m_price_list->GetPriceListDetail($idTypeUnit, $val['id_price_list']);
        }
        echo json_encode($priceList);
    }

    public function getdataprice_list($idTypeUnit, $manage = true) {
        $manage = $this->input->post('manage');
        $priceList = array();
        $table = '<table class="table table-striped table-bordered table-hover">';
        $thead = '<tr class="headTable"><th>No</th><th>Berlaku</th><th>Sampai</th>';
        $tbody = '';
        $button = '';
        $num = 1;
        $returnarray = $this->m_price_list->GetValuePrice($idTypeUnit);
        $top=$returnarray['top'];
        $priceList=$returnarray['list'];
        foreach($top as $val){
            $thead .= "<th>".$val."</th>";
        }
        $thead .= "<th>VRF Default</th><th>Finance Program</th><th>Discount Option</th><th></th></tr>";
        
        if($priceList == null) {
            $tbody .= "<tr><td colspan='13'>Tidak Ada Data</td></tr>";
        }
      
        foreach($priceList as $key=>$obj) {
            $tbody .= "<tr><td>".$num."</td><td>".DefaultTanggal($obj['data']->berlaku_dari)."</td>";
            if(!CheckEmpty($obj['data']->berlaku_sampai))
            {
                $tbody .="<td>".DefaultTanggal($obj['data']->berlaku_sampai)."</td>";
            }
            else
            {
                $tbody .="<td>&nbsp;</td>";
            }
            foreach ($top as $tp) {
                $price = $obj['top'][$tp]->price;
                $tbody .= "<td class='right'>".DefaultCurrency($price)."</td>";
            }
            $tbody .= "<td class='right'>".DefaultCurrency($obj['data']->vrf_default)."</td><td class='right'>".DefaultCurrency($obj['data']->finance_program)."</td><td class='right'>".DefaultCurrency($obj['data']->discount_option)."</td>";
            if($obj['data']->status=="2")
            {
                $button="Batal";
            }
            else
            {
                $button = $manage ? anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteprice_list(".$obj['data']->id_price_list.");return false;")) : "";
            }
            $tbody .= "<td>".$button."</td></tr>";
            $num++;
        }
        $table .= $thead . $tbody;
        $table .= "</table>";
        echo $table;
    }

    public function getTopDropdown ($id_type_unit = 0) {
        $data = $this->m_price_list->getTopDropdown($id_type_unit);
        echo json_encode($data);
    }

    public function getVrfDefault ($id_type_unit = 0) {
        $data = $this->m_price_list->getVrfDefault($id_type_unit);
        echo json_encode($data);
    }

    public function getTopPrice () {
        $params = $this->input->post();
        $top = $params['top'];
        $id_unit = $params['id_unit'];

        if($top != '' && $id_unit != '')
            $data = $this->m_price_list->getTopPrice(0, $top,$id_unit);
        else
            $data = array('vrf' => 0, 'price' => 0, 'top' => 0);
        echo json_encode($data);
    }
    
    public function create_price_list() 
    {
        $idTypeUnit = $this->input->post('id_type_unit');
        $row=['button'=>'Add'];
        $javascript = array();
	    $module = "K072";
        $header = "K001";
        $row['title'] = "Add Price List";
        // CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;    
        $row['id_type_unit'] = $idTypeUnit;
        $row['berlaku_dari'] = GetDateNow();
        $this->load->view('price_list/v_pricelist_manipulate', $row);
    }
    
    public function price_list_manipulate() 
    {
        $message='';

        $this->form_validation->set_rules('id_type_unit', 'Id Type Unit', 'trim');
        $this->form_validation->set_rules('berlaku_dari', 'Berlaku Dari', 'trim|required');
        $this->form_validation->set_rules('berlaku_sampai', 'Berlaku Sampai', 'trim');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_price_list->Price_listManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_price_list($id=0) 
    {
        $row = $this->m_price_list->GetOnePrice_list($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K072";
            $header = "K001";
            $row['title'] = "Edit Price List";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	        // LoadTemplate($row,'price_list/v_price_list_manipulate', $javascript);
            $this->load->view('price_list/v_price_list_manipulate', $row);
        } else {
            SetMessageSession(0, "Price_list cannot be found in database");
            redirect(site_url('price_list'));
        }
    }

    public function selectPricelist($idTypeUnit) {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K063";
        $header = "K059";
        $title = "Pricelist";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module, 'id_type_unit' => $idTypeUnit);
        $this->load->view("price_list/v_price_list_select", $model);
    }
    
    public function price_list_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_price_list', 'Price_list', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_price_list->Price_listDelete($model['id_price_list']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Price_list.php */