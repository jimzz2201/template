<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_price_list extends CI_Model
{

    public $table = '#_price_list';
    public $id = 'id_price_list';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }
    
    function GetValuePrice($id_type_unit)
    {
        $this->db->from("#_price_list_detail");
        $this->db->join("#_price_list","#_price_list.id_price_list=#_price_list_detail.id_price_list");
        $this->db->where(array("#_price_list.id_type_unit"=>$id_type_unit));
        $this->db->order_by("#_price_list.id_price_list,top");
        $listtop=$this->db->get()->result();
        $listreturn=[];
        $top=[];
        foreach($listtop as $topsatuan)
        {
            if(!in_array($topsatuan->top,$top))
            {
                $top[]=$topsatuan->top;
            }
            if(!CheckKey($listreturn, $topsatuan->id_price_list))
            {
                $listreturn[$topsatuan->id_price_list]=[];
                $listreturn[$topsatuan->id_price_list]["data"]=$topsatuan;
                $listreturn[$topsatuan->id_price_list]["top"]=[];
            }
            $listreturn[$topsatuan->id_price_list]["top"][$topsatuan->top]=$topsatuan;
        }
        return array("top"=>$top,"list"=>$listreturn);
        
        
    }

    // datatables
    function GetDataprice_list($id_type_unit) {
        $this->load->library('datatables');
        $this->datatables->select('id_price_list,id_type_unit,berlaku_dari,berlaku_sampai,vrf_default,finance_program,discount_option,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        $this->datatables->where($this->table.'.id_type_unit', $id_type_unit);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_price_list.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('price_list/edit_price_list/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteprice_list($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_price_list');
        return $this->datatables->generate();
    }

    function GetPriceList($id_type_unit) {
        $this->db->select('id_price_list,id_type_unit,berlaku_dari,berlaku_sampai,vrf_default,finance_program,discount_option,status');
        $this->db->where($this->table.'.deleted_date', null);
        $this->db->where($this->table.'.id_type_unit', $id_type_unit);
        $this->db->order_by('berlaku_dari', 'DESC');
        $pricelist = $this->db->get($this->table)->result_array();
        return $pricelist;
    }

    function GetPriceListDetail($id_type_unit, $id_price_list) {
        $this->db->select('top, price');
        $this->db->where($this->table.'.deleted_date', null);
        $this->db->where($this->table.'.id_type_unit', $id_type_unit);
        $this->db->where('#_price_list_detail.id_price_list', $id_price_list);
        $this->db->join($this->table, '#_price_list_detail.id_price_list = #_price_list.id_price_list');
        $pricelistDetail = $this->db->get("#_price_list_detail")->result();
        return $pricelistDetail;
    }

    function GetTopValue($id_type_unit, $id_price_list, $top) {
        $this->db->select('top, price');
        $this->db->where($this->table.'.deleted_date', null);
        $this->db->where($this->table.'.id_type_unit', $id_type_unit);
        $this->db->where('#_price_list_detail.id_price_list', $id_price_list);
        $this->db->where('#_price_list_detail.top', $top);
        $this->db->join($this->table, '#_price_list_detail.id_price_list = #_price_list.id_price_list');
        $topVal = $this->db->get("#_price_list_detail")->row();
        if($topVal)
        {
            return $topVal->price;
        }
        else
        {
            return 0;
        }
    }

    function GetTop($idTypeUnit) {
        $this->db->where('id_type_unit', $idTypeUnit);
        $this->db->where('deleted_date', null);
        $this->db->select('top');
        $this->db->join('#_price_list_detail', '#_price_list_detail.id_price_list = #_price_list.id_price_list');
        $this->db->group_by('top');
        $this->db->order_by('top', 'ASC');
        $res = $this->db->get("#_price_list")->result_array();
        $top =  $res != null ? $res : array(array('top'=>30), array('top'=>45), array('top'=>60), array('top'=>120));
        return $top;
    }

    // get all
    function GetOnePrice_list($keyword, $type = 'id_price_list') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $price_list = $this->db->get($this->table)->row();
        return $price_list;
    }

    function Price_listManipulate($model) {
        try {
            $model['id_type_unit'] = ForeignKeyFromDb($model['id_type_unit']);
            $model['berlaku_dari'] = DefaultTanggalDatabase($model['berlaku_dari']);
            // $model['berlaku_sampai'] = DefaultTanggalDatabase($model['berlaku_sampai']);
            $model['vrf_default'] = DefaultCurrencyDatabase($model['vrf_default']);
            $model['finance_program'] = DefaultCurrencyDatabase($model['finance_program']);
            $model['discount_option'] = DefaultCurrencyDatabase($model['discount_option']);
            $model['status'] = 1;

            $top = $model['top'];
            foreach($top as $key => $value) {
                $priceDetail[]  = array(
                    'top' => $_POST['top'][$key],
                    'price' => DefaultCurrencyDatabase($_POST['price'][$key]),
                );
            }
            unset($model['top']);
            unset($model['price']);

            if (CheckEmpty($model['id_price_list'])) {                
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());

                $this->db->trans_start();
                $this->db->insert($this->table, $model);
                $id_price_list = $this->db->insert_id();
                foreach ($priceDetail as &$val) $val['id_price_list'] = $id_price_list;
                $this->db->insert_batch('#_price_list_detail',$priceDetail);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $status = false;
                    $msg = "failed insert into database";
                } else {
                    $status = true;
                    $msg = "Pricelist successfull added into database";
                    SetMessageSession(1, 'Pricelist successfull added into database');
                }

                return array("st" => $status, "msg" => $msg);
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_price_list" => $model['id_price_list']));
                SetMessageSession(1, 'Price_list has been updated');
                return array("st" => true, "msg" => "Price_list has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    // getTopDropdown
    function GetTopDropdown($id_type_unit = 0) {
        $this->db->where(array('id_type_unit' => $id_type_unit, 'status' => 1, 'deleted_date' => null));
        $this->db->order_by('berlaku_dari', 'DESC');
        $idPricelist = $this->db->get('#_price_list')->row();
        $this->db->where("#_price_list.id_price_list", $idPricelist->id_price_list);
        $this->db->select("top, FORMAT(price,0) harga");
        $this->db->order_by('top', 'ASC');
        $data = $this->db->get('#_price_list_detail')->result_array();
        asort($data);
        $listprice_list = array();
        foreach($data as $row) {
            $listprice_list[] = array('id' => (int) $row['top'], 'text' => $row['top'] . ' - ' . $row['harga']);
        }
       
        return $listprice_list;
    }

    public function getVrfDefault($id_type_unit) {
        $this->db->select('FORMAT(vrf_default,0) vrf');
        $this->db->where('id_type_unit', $id_type_unit);
        $this->db->where('deleted_date', null);
        $this->db->order_by('berlaku_dari', 'DESC');
        $res = $this->db->get($this->table)->row_array();
        return $res;
    }

    public function getTopPrice($id_type_unit, $top,$id_unit=0) {
        $hargabeli=0;
        $discount=0;
        if(!CheckEmpty($id_unit))
        {
            $this->load->model("unit/m_unit");
            $unit=$this->m_unit->GetOneUnit($id_unit);
            $hargabeli=$unit->harga_beli;
            $id_type_unit =$unit->id_type_unit;
        }
        
        $this->db->where(array('id_type_unit' => $id_type_unit, 'status' => 1, 'deleted_date' => null));
        $this->db->order_by('berlaku_dari', 'DESC');
        $this->db->join("#_price_list_detail","#_price_list_detail.id_price_list=#_price_list.id_price_list and #_price_list_detail.top=".$top,"left");
        $this->db->select("#_price_list_detail.*,#_price_list.vrf_default,#_price_list.finance_program");
        $this->db->from("#_price_list");
        $pricelist = $this->db->get()->row_array();
        
        if(!$pricelist)
        {
            $this->db->where(array('id_type_unit' => $id_type_unit));
            $this->db->order_by('berlaku_dari', 'DESC');
            $this->db->join("#_price_list_detail","#_price_list_detail.id_price_list=#_price_list.id_price_list and #_price_list_detail.top=".$top,"left");
            $this->db->select("#_price_list_detail.*,#_price_list.vrf_default,#_price_list.finance_program");
            $this->db->from("#_price_list");
            $pricelist = $this->db->get()->row_array();

        }
        if($pricelist)
        {
            if(CheckEmpty(@$pricelist['price']))
            {
                $hargabeli=$pricelist['vrf_default'];
            }
            else{
                $hargabeli=$pricelist['price'];
            }
        }
        
        
        $listprice_list = array();
        if($pricelist){
            $listprice_list = array('vrf' => $pricelist['finance_program'], 'price' => $hargabeli, 'top' => $top);
        }else{
            $listprice_list = array('vrf' => 0, 'price' => $hargabeli, 'top' => 0);
        }

        return $listprice_list;
    }

    function GetDropDownPrice_list() {
        $listprice_list = GetTableData($this->table, 'id_price_list', '', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listprice_list;
    }

    function Price_listDelete($id_price_list) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $model['status']=2;
            $this->db->update($this->table, $model, array('id_price_list' => $id_price_list));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Price_list has been deleted from database");
    }


}