<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Home</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-sm-12">
            <div class="well text-center">
                <h4 style="margin-top:0px"><strong><?php echo date('l, jS F Y', strtotime('today')); ?></strong></h4>
                <p>Selamat datang <strong><?php echo @$nama_user?></strong></p>
            </div>
        </div>
    </div>
    <?php if($content): ?>
        <div class="row">
        <?php
        foreach($content as $item){
            echo $item['html'];
        }
        ?>
        </div>
    <?php endif; ?>
   

</section>
<!-- /.content -->
<script>
    $(document).ready(function(){
        <?php
        foreach($content as $item){
        ?>
            updateValue("<?=$item['tipe']?>", "<?=$item['ajax']?>");
        <?php
        }
        ?>
    });

    function updateValue(tipe, ajax) {
        $("#counter-"+tipe).html('<i class="fa fa-refresh fa-spin"></i>');
        $.post(
            baseurl + 'index.php/'+ajax,
            {},
            function(value){
                $("#counter-"+tipe).html(value);
            }
        );
    }

    function activateGraph(gId) {
        if($("#graphbox-"+gId).hasClass("collapsed-box") && $("#graphbox-"+gId).hasClass("closed")){
            $("#graphbox-"+gId).append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i><p style="color: black;">Mengambil Data...</p></div>');
            $.post(
                baseurl + 'index.php/dashboard/get'+gId+'_data',
                {},
                function(resp){
                    if(resp.st){
                        buildGraph(gId+'-chart', gId, resp.data, resp.title);
                        $("#graphbox-"+gId+" .overlay").remove();
                        $("#graphbox-"+gId).removeClass("closed");
                    }
                },"json"
            );
        }
    }

    
</script>