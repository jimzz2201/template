<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_dashboard');
        $this->load->model('chart/m_chart');
        $this->load->model('pembayaran/m_pembayaran_piutang');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $userid = GetUserId();
        $this->load->model("user/m_user");
        $this->load->library('user_agent');
        $model = $this->m_user->GetOneUser($userid, 'id_user');
        $model = json_decode(json_encode($model), true);
        $model['title'] = "Dashboard";
        $model['openmenu'] = "dashboardmenu";
        $model['is_mobile'] = $this->agent->is_mobile();

        $js = $css = array();
        $js[] = "assets/plugins/morris/raphael-min.js";
        $js[] = "assets/plugins/morris/morris.js";
        $js[] = "assets/js/modules/dashboard/dashboard.js";

        $css[] = "assets/plugins/morris/morris.css";

        $model['content'] = $this->getContent();
        $model['graph'] = array();
        if(CekModule('K068', 0)) {
            array_push($model['graph'], ['id'=>'penjualan', 'title'=>'Penjualan']);
        }
        LoadTemplate($model, "dashboard/dashboard_index", $js, $css);
    }

    function getContent(){
        $content = array();

        if(CekModule('K069', 0)){
            $content['piutang'] = [
                'html' => $this->prepareContent('piutang', 'Piutang Customer','money','pembayaran/piutang','red'),
                'ajax' => 'dashboard/getPiutang',
                'tipe' => 'piutang'
            ];
            $content['birthday'] = [
                'html' => $this->prepareContent('birthday', 'Birthday','users','customer/birthday','blue'),
                'ajax' => 'dashboard/getBirthday',
                'tipe' => 'birthday'
            ];
        }
        
        
        IF(CekModule("K121",0))
        {
             $content['pendingdo'] = [
                'html' => $this->prepareContent('pendingdo', 'Pending DO KPU','money','do_prospek/pending','green'),
                'ajax' => 'dashboard/getPendingDo',
                'tipe' => 'pendingdo'
            ];
        }
        
        

        return $content;
    }
    
    public function getPendingDo(){
        $this->load->model("do_prospek/m_do_prospek");
        $listpendingdo=$this->m_do_prospek->GetUnitBelumTerkirim();
        $jumlahunit=0;
        $jumlahdo=0;
        foreach ($listpendingdo as $dopending) {
            $jumlahunit += $dopending->jumlah_unit;
            $jumlahdo += 1;
        }
        
      
        echo $jumlahdo." DO (". DefaultCurrency($jumlahunit)." Unit)";
    }
    
    

    public function counter($tipe){
        $count = 0;
        
        switch($tipe){
            case 'berangkat' : $count = (CekModule('K067', 0)) ? $this->m_dashboard->getCountWhere('#_berangkat', "tanggal = '" . date('Y-m-d') . "' AND #_berangkat.status < 2", true, '#_user','#_berangkat.created_by = #_user.id_user', 'id_pt') : 0;
                break;
            case 'spj' : $count = (CekModule('K066', 0)) ? $this->m_dashboard->getCountWhere('#_spj', array('#_spj.tanggal', date('Y-m-d')), true, '#_user','#_spj.created_by = #_user.id_user', 'id_pt') : 0;
                break;
            case 'tiket' : $count = (CekModule('K065', 0)) ? $this->m_dashboard->getCountLike('#_pemesanan', '#_pemesanan.created_date', date('Y-m-d'), 'after', ['#_pemesanan.is_batal'=>0], true, '#_user','#_pemesanan.created_by = #_user.id_user', 'id_pt') : 0;
                break;
            case 'penjualan' :
                $join = ['#_deduct', '#_deduct.id_pemesanan = #_pemesanan.id_pemesanan AND #_deduct.jenis_deduct = 2 AND #_deduct.is_batal = 0', 'left'];
                $col = "#_pemesanan.harga_tiket) + IFNULL(SUM(#_deduct.nominal), 0";
                $count = (CekModule('K064', 0)) ? DefaultCurrency($this->m_dashboard->getSumLike('#_pemesanan', $col, '#_pemesanan.created_date', date('Y-m-d'),'after', $join, null, true, '#_user','#_pemesanan.created_by = #_user.id_user', 'id_pt')) : 0;
                break;
            default : $count = 0;
        }
        
        echo $count;
    }

    public function getPiutang(){
        $piutang = $this->m_pembayaran_piutang->GetTotalPiutang(GetCabangAkses());
      
        echo number_format($piutang, 0, '', ',');
    }
    public function getBirthday(){
        $this->load->model("customer/m_customer");
        $model['start']= date("Y-m-1 H:i:s");
        $model['end']= AddDays(AddDays($model['start'], "1 month"), "-1 days")  ;
        $piutang = count($this->m_customer->GetBirhtday($model));
      
        echo number_format($piutang, 0, '', ',')." Customer";
    }
    
    
    

    function prepareContent($tipe, $title,$icon,$url,$color, $val="--"){
        $contentHtml = '<div class="col-lg-3 col-xs-6">';
        $contentHtml .= '<div class="small-box bg-'.$color.'">';
                $contentHtml .= '<div class="inner">';
                    $contentHtml .= '<h3 id="counter-'.$tipe.'">'.$val.'</h3>';
                    $contentHtml .= '<p>'.$title.'</p>';
                $contentHtml .= '</div>';
        $contentHtml .= '<div class="icon">';
        $contentHtml .= '<i class="fa fa-'.$icon.'"></i>';
        $contentHtml .= '</div>';
        $contentHtml .= '<div style="position: absolute; top: 0px; right: 3px; opacity: 0.5; cursor: pointer" onclick="updateValue(\''.$tipe.'\',\'dashboard/counter/'.$tipe.'\')"><i class="fa fa-refresh"></i></div>';
        $contentHtml .= anchor(site_url($url),'Lebih Lengkap <i class="fa fa-arrow-circle-right"></i>', array('class'=>'small-box-footer'));
        $contentHtml .= '</div>';
        $contentHtml .= '</div>';

        return $contentHtml;
    }

    function getGraphPenjualan($json=true){
        $result = array();
        $tahun = date('Y');
        $bulan = date('n');
        for($x = 1; $x <= 12; $x++){
            if($x > $bulan){
                array_push($result, array(
                    'y' => $tahun.'-'.str_pad($x,2,'0',STR_PAD_LEFT),
                    'penjualan' => (int)0,
                ));
            }else{
                array_push($result, array(
                    'y' => $tahun.'-'.str_pad($x,2,'0',STR_PAD_LEFT),
                    'penjualan' => (int)$this->m_chart->getStatPeriode($tahun, $x, null, true),
                ));
            }
        }

//        12 bulan terakhir, tapi lama
//        for($m = 1; $m <= 12 ; $m++){
//            $b = ($m + $bulan) - 12;
//            $t = $tahun;
//            if($b <= 0){
//                $b += 12;
//                $t = $tahun - 1;
//            }
//
//            array_push($result, array(
//                'y' => $t.'-'.str_pad($b,2,'0',STR_PAD_LEFT),
//                'penjualan' => (int)$this->m_chart->getStatPeriode($t, $b),
//            ));
//        }

        $data = array(
            'title' => 'Penjualan',
            'data' => $json ? json_encode($result) : $result,
        );

        return $data;
    }

    public function getpenjualan_data(){
        $data = $this->getGraphPenjualan(false);
        if($data['data']){
            $data['st'] = true;
        }else{
            $data['st'] = false;
        }

        echo json_encode($data);
    }

}
