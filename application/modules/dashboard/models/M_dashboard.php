<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_dashboard extends CI_Model
{

    function __construct()
    {
        parent::__construct();
      
    }

    function getCountWhere($table, $where, $filterPT=false, $filterTable='', $filterJoin='', $filterCol=''){
        $jml = 0;
        $this->db->select("COUNT(*) AS jml ");
        $this->db->from($table);
	    if($filterPT){
		    $this->db->join($filterTable, $filterJoin, 'left');
	    }
        if(is_array($where)){
            $this->db->where($where[0], $where[1]);
        }else{
            $this->db->where($where);
        }

        $data = $this->db->get()->row_array();
       
        if($data){
            $jml = $data['jml'];
        }
        return $jml;
    }

    function getCountLike($table, $col, $match, $pos, $where=null, $filterPT=false, $filterTable='', $filterJoin='', $filterCol=''){
        $jml = 0;
        if(CheckEmpty($pos)){
            $post = "both";
        }
        $this->db->select("COUNT(".$col.") AS jml ");
        $this->db->from($table);
	    if($filterPT){
		    $this->db->join($filterTable, $filterJoin, 'left');
		  }
        $this->db->like($col, $match, $pos);
        if($where){
            $this->db->where($where);
        }

        $data = $this->db->get()->row_array();
        if($data){
            $jml = $data['jml'];
        }
        return $jml;
    }

    function getSumLike($table, $colSum, $col, $match, $pos, $join=[], $where=null, $filterPT=false, $filterTable='', $filterJoin='', $filterCol=''){
        $jml = 0;
        if(CheckEmpty($pos)){
            $pos = "both";
        }
        $this->db->select("SUM(".$colSum.") AS jml ");
        $this->db->from($table);
	    if($filterPT){
		    $this->db->join($filterTable, $filterJoin, 'left');
		 }
	    if($join){
		    $this->db->join($join[0], $join[1], $join[2]);
	    }
        $this->db->like($col, $match, $pos);
	    if($where){
		    $this->db->where($where);
	    }

        $data = $this->db->get()->row_array();
        if($data){
            $jml = $data['jml'];
        }
        return (int)$jml;
    }

}