<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kategori extends CI_Model
{

    public $table = '#_kategori';
    public $id = 'id_kategori';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatakategori() {
        $this->load->library('datatables');
        $this->datatables->select('id_kategori,kode_kategori,nama_kategori,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_kategori.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("/kategori/edit_kategori/$1", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletekategori($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_kategori');
        return $this->datatables->generate();
    }

    // get all
    function GetOneKategori($keyword, $type = 'id_kategori') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $kategori = $this->db->get($this->table)->row();
        return $kategori;
    }

    function KategoriManipulate($model) {
        try {
            $kat['kode_kategori']=$model['kode_kategori'];
            $kat['nama_kategori']=$model['nama_kategori'];
            $kat['status'] = DefaultCurrencyDatabase($model['status']);
            
            $messagereturn="";
            $messagestatus=false;
            $id_cat=0;
            $description="Setting dari Kategori ".$kat['nama_kategori'];
            $url="index.php/kategori/edit_kategori/";
            if (CheckEmpty($model['id_kategori'])) {                
                $kat['created_date'] = GetDateNow();
                $kat['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $kat);
                $id_cat=$this->db->insert_id();
                $msgreturn="Kategori successfull added into database";
                $messagestatus=true;
            } else {
                $id_cat=$model['id_kategori'];
                $kat['updated_date'] = GetDateNow();
                $kat['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $kat, array("id_kategori" => $model['id_kategori']));
                $messagereturn="Kategori has been updated";
                $messagestatus=true;
		
            }
            $url="index.php/kategori/edit_kategori/".$id_cat;
            $arrchildid=[];
            foreach ($model['listconfig'] as $child) {
                $childwhere = array("id_gl_module" => $child['id_gl_module'], "id_cabang" => ForeignKeyFromDb($child['id_cabang']), "type" => "kategori","id_subject"=>$id_cat);
                $this->db->from("#_gl_config");
                $this->db->where($childwhere);
                $rowchild = $this->db->get()->row();
                if ($rowchild) {
                    $arrchildid[] = $rowchild->id_gl_config;
                    $this->db->update("#_gl_config", MergeUpdate(array("id_gl_account" => $child['id_gl_account'],"description"=>$description,"url"=>$url)), array('id_gl_config' => $rowchild->id_gl_config));
                } else {
                    $childinsert = $childwhere;
                    $childinsert['id_gl_account'] = $child['id_gl_account'];
                    $childinsert['description'] = $description;
                    $childinsert['url'] = $url;
                    $this->db->insert("#_gl_config", MergeCreated($childinsert));
                    $insert_id = $this->db->insert_id();
                    $arrchildid[] = $insert_id;
                }
            }
            $this->db->where(array("type" => "kategori","id_subject"=>$id_cat));
            if (count($arrchildid) > 0) {
                $this->db->where_not_in("id_gl_config", $arrchildid);
            }
            $this->db->delete("#_gl_config");
            if($messagestatus)
            {
                SetMessageSession(1, $messagereturn);
            }
            return array("st" => $messagestatus, "msg" => $messagereturn);
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownKategori() {
        $listkategori = GetTableData($this->table, 'id_kategori', 'nama_kategori', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listkategori;
    }

    function KategoriDelete($id_kategori) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_kategori' => $id_kategori));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Kategori has been deleted from database");
    }


}