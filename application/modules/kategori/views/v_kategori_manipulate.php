<section class="content-header">
    <h1>
        Kategori <?= @$button ?>
        <small>Customer</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Kategori</li>
        <li class="active">Kategori <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <form id="frm_kategori" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_kategori" value="<?php echo @$id_kategori; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Kode Kategori', "txt_kode_kategori", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        $mergearray = array();
                                        if (!CheckEmpty(@$id_kategori)) {
                                            $mergearray['readonly'] = "readonly";
                                            $mergearray['name'] = "kode_kategori";
                                        } else {
                                            $mergearray['name'] = "kode_kategori";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_kategori, 'class' => 'form-control', 'id' => 'txt_kode_kategori', 'placeholder' => 'Kode Kategori'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Kategori', "txt_nama_kategori", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_kategori', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Nama Kategori')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <table class="table table-striped table-bordered table-hover" id="mytable">
                                        <thead>
                                        <th>Module</th>
                                        <th>Cabang</th>
                                        <th>GL Account</th>
                                        <th style="width:150px;">Action <a style="float:right" class='btn-xs  btn-success' href='javascript:;' onclick="AddGlModule()">Add Child</a></th>
                                        </thead>
                                        <tbody>
                                            <?php if (count(@$list_gl_config) > 0) {
                                                foreach (@$list_gl_config as $config) {
                                                    ?>
                                                <tr class="tr<?php echo $config->id_gl_config ?>">
                                                <input type="hidden" name="id_gl_config[]" value="<?php echo $config->id_gl_config; ?>" /> 
                                                <td><?= form_dropdown(array("selected" => @$config->id_gl_module, "id" => "dd_id_gl_module" . $config->id_gl_module, "name" => "id_gl_module[]"), DefaultEmptyDropdown(@$list_gl_module, "json", "Module"), @$config->id_gl_module, array('class' => 'form-control')); ?></td>

                                                <td><?= form_dropdown(array("selected" => @$config->id_cabang, "id" => "dd_id_cabang" . $config->id_cabang, "name" => "id_cabang[]"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$config->id_cabang, array('class' => 'form-control')); ?></td>
                                                <td><?= form_dropdown(array("selected" => @$config->id_gl_account, "id" => "dd_id_gl_account" . $config->id_gl_module, "name" => "id_gl_account[]"), DefaultEmptyDropdown(@$list_gl_account, "json", "Akun"), @$config->id_gl_account, array('class' => 'form-control')); ?></td>
                                                <td style="width:200px;">
                                                    <a class="btn  btn-danger" href="javascript:;" onclick="DeleteGlModule(this)">Delete</a>
                                                </td>
                                                </tr>

                                            <?php
                                            }
                                        } else {
                                            echo "<tr class='trkosong'><td colspan='4' style='padding:10px;text-align:center'>Tidak ada setting akun</td></tr>";
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>


                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("select").select2();
    });
    var datagl =<?php echo json_encode(DefaultEmptyDropdown(@$list_gl_account, "json", "Akun")) ?>;
    var datacabang =<?php echo json_encode(DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")) ?>;
    var datamodule =<?php echo json_encode(DefaultEmptyDropdown(@$list_gl_module, "json", "Module")) ?>;

    function AddGlModule()
    {
        $(".trkosong").remove();
        var newRow = $("<tr class='trchild'>");
        var cols = "";
        cols += '<td><select name="id_gl_module[]" class="form-control modulechild" ></select></td>';
        cols += '<td><select name="id_cabang[]" class="form-control cabangchild" ></select></td>';
        cols += '<td><select name="id_gl_account[]" class="form-control child" ></select></td>';
        cols += '<td><a class="btn  btn-danger" href="javascript:;" onclick="DeleteGlModule(this)">Delete</a></td>';
        newRow.append(cols);
        $("tbody").append(newRow);
        $(".child:last").select2({data: datagl});
        $(".modulechild:last").select2({data: datamodule});
        $(".cabangchild:last").select2({data: datacabang});
        $(".child:last").val(0);
        $(".child:last").trigger("change");
    }
    function DeleteGlModule(module)
    {
        $(module).parent().parent().remove();
        if ($(".child").length == 0)
        {
            $("tbody").append("<tr class='trkosong'><td colspan='4' style='padding:10px;text-align:center'>Tidak ada setting akun</td></tr>");
        }
    }

    $("#frm_kategori").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/kategori/kategori_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/kategori';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>