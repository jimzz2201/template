<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kategori extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_kategori');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K001";
        $title = "Kategori";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "kategori/v_kategori_index", $javascript);
        
    } 
    public function get_one_kategori() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_kategori', 'Kategori', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_kategori->GetOneKategori($model["id_kategori"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Kategori tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatakategori() {
        header('Content-Type: application/json');
        echo $this->m_kategori->GetDatakategori();
    }
    
    public function create_kategori() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $row['title'] = "Add Kategori";
        $module = "K054";

        $header = "K001";
        $row['title'] = "Edit Kategori";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $this->load->model("gl_config/m_gl_config");
        $this->load->model("gl/m_gl");
        $this->load->model("cabang/m_cabang");
        $row['list_gl_account'] = $this->m_gl->GetDropDownGl(0);
        $row['list_gl_module'] = $this->m_gl_config->GetDropDownGlModule(array("penjualan", "pembelian", "returpenjualan", "returpembelian", "mutasimasuk", "mutasikeluar"));
        $row['list_gl_config'] = [];
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $javascript = [];
        LoadTemplate($row, "kategori/v_kategori_manipulate", $javascript);
    }

    public function kategori_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_kategori', 'Kode Kategori', 'trim|required');
	$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'trim|required');
        $model=$this->input->post();
        $listmodule= CheckArray($model, 'id_gl_module');
        $listcabang= CheckArray($model, 'id_cabang');
        $listid_gl_account= CheckArray($model, 'id_gl_account');
        $arrconfig=[];
        foreach($listmodule as $key=>$mod)
        {
            
            if(CheckEmpty($listid_gl_account[$key])&& !CheckEmpty($listmodule[$key]))
            {
                $message .= "Akun GL Tidak boleh kosong<br/>";
            }
            else if(!CheckEmpty($listid_gl_account[$key])&& CheckEmpty($listmodule[$key]))
            {
                $message .= "Module Tidak boleh kosong<br/>";
            } 
            else if (!CheckEmpty($listid_gl_account[$key]) && !CheckEmpty($listmodule[$key])) {
                $temparray = Multi_array_search($arrconfig, "id_gl_module", ForeignKeyFromDb($listmodule[$key]));
                $temparray = Multi_array_search($temparray, "id_cabang", ForeignKeyFromDb($listcabang[$key]));

                if ($temparray) {
                    $message .= "GL  sudah memiliki config yang serupa sebelumnya<br/>";
                }

                if ($message == "" ) {
                    $configdetail = [];
                    $configdetail['id_cabang'] = ForeignKeyFromDb($listcabang[$key]);
                    $configdetail['id_gl_module'] = ForeignKeyFromDb($listmodule[$key]);
                    $configdetail['id_gl_account'] = ForeignKeyFromDb($listid_gl_account[$key]);
                    $arrconfig[] = $configdetail;
                }
            }
        }
        $model['listconfig']=$arrconfig;
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_kategori->KategoriManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_kategori($id=0) 
    {
        $row = $this->m_kategori->GetOneKategori($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K054";
            
            $header = "K001";
            $row['title'] = "Edit Kategori";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;   
            $javascript=[];
            $this->load->model("gl_config/m_gl_config");
            $this->load->model("gl/m_gl");
            $this->load->model("cabang/m_cabang");
            $row['list_gl_account']=$this->m_gl->GetDropDownGl(0);
            $row['list_gl_module']=$this->m_gl_config->GetDropDownGlModule(array("penjualan","pembelian","returpenjualan","returpembelian","mutasimasuk","mutasikeluar"));
            $row['list_gl_config']=$this->m_gl_config->GetGlConfig($id,"kategori");
            $row['list_cabang']=$this->m_cabang->GetDropDownCabang();
            LoadTemplate($row, "kategori/v_kategori_manipulate", $javascript);
	} else {
            SetMessageSession(0, "Kategori cannot be found in database");
            redirect(site_url('kategori'));
        }
    }
    
    public function kategori_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_kategori', 'Kategori', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_kategori->KategoriDelete($model['id_kategori']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Kategori.php */