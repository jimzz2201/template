<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_surat_jalan extends CI_Model
{

    public $table = '#_surat_jalan';
    public $id = 'id_surat_jalan';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatasurat_jalan() {
        $this->load->library('datatables');
        $this->datatables->select('id_surat_jalan,no_surat_jalan,tanggal_surat_jalan,id_prospek,id_do,id_unit,diterima_oleh,diperiksa_oleh,disetujui_oleh,disiapkan_oleh,keterangan');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_surat_jalan.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('surat_jalan/edit_surat_jalan/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletesurat_jalan($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_surat_jalan');
        return $this->datatables->generate();
    }

    // get all
    function GetOneSurat_jalan($keyword, $type = 'id_surat_jalan') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $surat_jalan = $this->db->get($this->table)->row();
        return $surat_jalan;
    }

    function Surat_jalanManipulate($model) {
        try {
                $model['tanggal_surat_jalan'] = DefaultTanggalDatabase($model['tanggal_surat_jalan']);
                $model['id_prospek'] = ForeignKeyFromDb($model['id_prospek']);
                $model['id_do'] = ForeignKeyFromDb($model['id_do']);
                $model['id_unit'] = ForeignKeyFromDb($model['id_unit']);

            if (CheckEmpty($model['id_surat_jalan'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		SetMessageSession(1, 'Surat_jalan successfull added into database');
		return array("st" => true, "msg" => "Surat_jalan successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_surat_jalan" => $model['id_surat_jalan']));
		SetMessageSession(1, 'Surat_jalan has been updated');
		return array("st" => true, "msg" => "Surat_jalan has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownSurat_jalan() {
        $listsurat_jalan = GetTableData($this->table, 'id_surat_jalan', '', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listsurat_jalan;
    }

    function Surat_jalanDelete($id_surat_jalan) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_surat_jalan' => $id_surat_jalan));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Surat_jalan has been deleted from database");
    }


}