
<section class="content-header">
    <h1>
        Surat_jalan
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Surat_jalan</li>
    </ol>


</section>
    
<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">

                <?php echo anchor(site_url('surat_jalan/create_surat_jalan'), 'Create', 'class="btn btn-success"'); ?>
	    </div>
        </div>
         <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body form">
        <table class="table table-striped table-bordered table-hover" id="mytable">
	    
        </table>
        </div>
                </div>

            </div>
        </div>

</section>
        <script type="text/javascript">
             var table;
            function deletesurat_jalan(id_surat_jalan) {


        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/surat_jalan/surat_jalan_delete',
                dataType: 'json',
                data: {
                    id_surat_jalan: id_surat_jalan
                },
                success: function (data) {
                    if (data.st)
                    {
                        messagesuccess(data.msg);
                        table.fnDraw(false);
                    }
                    else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
       }});


    }
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                table = $("#mytable").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    ajax: {"url": "surat_jalan/getdatasurat_jalan", "type": "POST"},
                    columns: [
                        {
                           data: "id_surat_jalan",
                           title: "Kode",
                           orderable: false
                        },
			{data: "no_surat_jalan" , orderable:false , title :"No Surat Jalan"},
			{data: "tanggal_surat_jalan" , orderable:false , title :"Tanggal Surat Jalan",
                        mRender: function (data, type, row) {
                            return  DefaultDateFormat(data);
                        }},
			{data: "id_prospek" , orderable:false , title :"Id Prospek"},
			{data: "id_do" , orderable:false , title :"Id Do"},
			{data: "id_unit" , orderable:false , title :"Id Unit"},
			{data: "diterima_oleh" , orderable:false , title :"Diterima Oleh"},
			{data: "diperiksa_oleh" , orderable:false , title :"Diperiksa Oleh"},
			{data: "disetujui_oleh" , orderable:false , title :"Disetujui Oleh"},
			{data: "disiapkan_oleh" , orderable:false , title :"Disiapkan Oleh"},
			{data: "keterangan" , orderable:false , title :"Keterangan"},
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center"
                        }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
    