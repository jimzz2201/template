<section class="content-header">
    <h1>
        Surat Jalan <?= @$button ?>
        <small>Surat Jalan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Surat Jalan</li>
        <li class="active">Surat Jalan <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary" data-collapsed="0">
												<div class="panel-heading"></div>
                        <div class="panel-body">
												<form id="frm_surat_jalan" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_surat_jalan" value="<?php echo @$id_surat_jalan; ?>" /> 
	<div class="form-group col-sm-6">
		<?= form_label('No Surat Jalan', "txt_no_surat_jalan", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'no_surat_jalan', 'value' => @$no_surat_jalan == '' ? 'automatic' : '@$no_surat_jalan', 'class' => 'form-control', 'id' => 'txt_no_surat_jalan', 'placeholder' => 'No Surat Jalan')); ?>
		</div>
	</div>
	<div class="form-group col-sm-6">
		<?= form_label('Tanggal Surat Jalan', "txt_tanggal_surat_jalan", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'tanggal_surat_jalan', 'value' => DefaultDatePicker(@$tanggal_surat_jalan), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_surat_jalan', 'placeholder' => 'Tanggal Surat Jalan')); ?>
		</div>
	</div>

	<div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Prospek</span></h3></div>

	<!-- pilih prospek -->

	<div class="form-group">
		<?= form_label('No Prospek', "txt_id_prospek", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_no_customer', 'placeholder' => 'Nama Customer')); ?>
			<span class="badge badge-light" id="selectProspek" style="cursor: pointer;">Pilih Prospek</span>
		</div>
	</div>

	<!-- Detail customer -->

	<div class="form-group">
		<?= form_label('Nama Customer', "txt_nama_cust", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Nama Customer')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Alamat Customer', "txt_alamat_cust", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?= form_textarea(array('type' => 'text', 'rows' => '3', 'cols' => '10', 'class' => 'form-control', 'id' => 'alamat_customer', 'placeholder' => 'Alamat', 'readonly' => 'readonly')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('No Telp', "txt_telp_cust", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_telp_customer', 'placeholder' => 'Telp Customer')); ?>
		</div>
	</div>

	<!-- end detail customer -->

	<div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">DO</span></h3></div>

	<!--Select DO -->

	<div class="form-group col-md-6">
		<?= form_label('No Do', "txt_id_do", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array('type' => 'text', 'name' => 'id_do', 'class' => 'form-control', 'id' => 'dd_id_do', 'placeholder' => 'DO'), DefaultEmptyDropdown($list_do,"json","do"), @$id_do); ?>
		</div>
	</div>

	<!-- End Select DO -->

	<div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Unit</span></h3></div>

	<!-- Select Unit -->

	<div class="form-group col-sm-6">
		<?= form_label('Unit', "txt_id_unit", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array('type' => 'text', 'name' => 'id_unit', 'class' => 'form-control', 'id' => 'dd_id_unit', 'placeholder' => 'Unit'), DefaultEmptyDropdown($list_unit,"json","unit"), @$id_unit); ?>
		</div>
	</div>

	<!-- End Unit -->

	<div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Keterangan</span></h3></div>

	<!-- Keterangan -->

	<div class="form-group col-sm-6">
		<?= form_label('Diterima Oleh', "txt_diterima_oleh", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'diterima_oleh', 'value' => @$diterima_oleh, 'class' => 'form-control', 'id' => 'txt_diterima_oleh', 'placeholder' => 'Diterima Oleh')); ?>
		</div>
	</div>
	<div class="form-group col-sm-6">
		<?= form_label('Diperiksa Oleh', "txt_diperiksa_oleh", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'diperiksa_oleh', 'value' => @$diperiksa_oleh, 'class' => 'form-control', 'id' => 'txt_diperiksa_oleh', 'placeholder' => 'Diperiksa Oleh')); ?>
		</div>
	</div>
	<div class="form-group col-sm-6">
		<?= form_label('Disetujui Oleh', "txt_disetujui_oleh", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'disetujui_oleh', 'value' => @$disetujui_oleh, 'class' => 'form-control', 'id' => 'txt_disetujui_oleh', 'placeholder' => 'Disetujui Oleh')); ?>
		</div>
	</div>
	<div class="form-group col-sm-6">
		<?= form_label('Disiapkan Oleh', "txt_disiapkan_oleh", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'disiapkan_oleh', 'value' => @$disiapkan_oleh, 'class' => 'form-control', 'id' => 'txt_disiapkan_oleh', 'placeholder' => 'Disiapkan Oleh')); ?>
		</div>
	</div>
	<div class="form-group col-sm-6">
		<?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_textarea(array('type' => 'text', 'rows' => '3', 'cols' => '10', 'class' => 'form-control', 'value' => @$keterangan, 'name' => 'keterangan', 'id' => 'keterangan', 'placeholder' => 'Keterangan')); ?>
		</div>
	</div>
	<div class="form-group col-sm-12">
		<a href="<?php echo base_url().'index.php/surat_jalan' ?>" class="btn btn-default"  >Cancel</a>
		<button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
	</div>
</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
		$(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });
				$("#selectProspek").click(function () {
					var url_select = baseurl + 'index.php/prospek/selectProspek';
					$.ajax({
							type: 'get',
							url: url_select,
							success: function (data) {
									modalbootstrap(data, "", "60%");
							},
							error: function (xhr, status, error) {
									messageerror(xhr.responseText);
							}
					});
				});
		});
		
		function pilihProspek (id) {
			$.ajax({
				type: 'post',
				data: 'id_prospek=' + id,
				url: baseurl + 'index.php/prospek/getSelProspek',
				dataType: 'json',
				success: function (data) {
					// alert(data.obj.no_prospek);return false;
					$('#txt_no_customer').val(data.obj.no_prospek);
					$('#txt_nama_customer').val(data.obj.nama_customer);
					$('#alamat_customer').val(data.obj.alamat);
					$('#txt_telp_customer').val(data.obj.no_telp);
					$("#modalbootstrap");
					closemodalboostrap();
				},
				error: function (xhr, status, error) {
					messageerror(xhr.responseText);
				}
			})
		}
		
		$("#frm_surat_jalan").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/surat_jalan/surat_jalan_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/surat_jalan';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>