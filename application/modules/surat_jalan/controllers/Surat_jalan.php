<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Surat_jalan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_surat_jalan');
        $this->load->model('prospek/m_prospek');
       
        $this->load->model('unit/m_unit');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K068";
        $header = "K059";
        $title = "Surat Jalan";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        CekModule($module);
        LoadTemplate($model, "surat_jalan/v_surat_jalan_index", $javascript);
        
    } 
    public function get_one_surat_jalan() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_surat_jalan', 'Surat Jalan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_surat_jalan->GetOneSurat_jalan($model["id_surat_jalan"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Surat Jalan tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatasurat_jalan() {
        header('Content-Type: application/json');
        echo $this->m_surat_jalan->GetDatasurat_jalan();
    }
    
    public function create_surat_jalan() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
	    $module = "K068";
        $header = "K059";
        $row['title'] = "Add Surat Jalan";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
        $row['list_prospek'] = $this->m_prospek->GetDropDownProspek();
        //$row['list_do'] = $this->m_do->GetDropDownDo();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
	    LoadTemplate($row,'surat_jalan/v_surat_jalan_manipulate', $javascript);       
    }
    
    public function surat_jalan_manipulate() 
    {
        $message='';

        $this->form_validation->set_rules('no_surat_jalan', 'No Surat Jalan', 'trim|required');
        $this->form_validation->set_rules('tanggal_surat_jalan', 'Tanggal Surat Jalan', 'trim|required');
        $this->form_validation->set_rules('id_prospek', 'Id Prospek', 'trim|required');
        $this->form_validation->set_rules('id_do', 'Id Do', 'trim|required');
        $this->form_validation->set_rules('id_unit', 'Id Unit', 'trim|required');
        $this->form_validation->set_rules('diterima_oleh', 'Diterima Oleh', 'trim|required');
        $this->form_validation->set_rules('diperiksa_oleh', 'Diperiksa Oleh', 'trim|required');
        $this->form_validation->set_rules('disetujui_oleh', 'Disetujui Oleh', 'trim|required');
        $this->form_validation->set_rules('disiapkan_oleh', 'Disiapkan Oleh', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        $model=$this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_surat_jalan->Surat_jalanManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_surat_jalan($id=0) 
    {
        $row = $this->m_surat_jalan->GetOneSurat_jalan($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K068";
            $header = "K059";
            $row['title'] = "Edit Surat Jalan";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	        LoadTemplate($row,'surat_jalan/v_surat_jalan_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Surat_jalan cannot be found in database");
            redirect(site_url('surat_jalan'));
        }
    }
    
    public function surat_jalan_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_surat_jalan', 'Surat_jalan', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_surat_jalan->Surat_jalanDelete($model['id_surat_jalan']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Surat_jalan.php */