<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Persediaan_unit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_persediaan_unit');
    }
    
    public function create_perlengkapan($id_unit_serial){
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $this->load->model("unit/m_unit");
        $row=$this->m_unit->GetOneUnitSerial($id_unit_serial);
        if($row)
        {
            $row = json_decode(json_encode($row), true);
            $module = "K076";
            $header = "K059";
            CekModule($module);
            $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
            $css = array();
            $row['title'] = "Form Unit";
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->model("jenis_dokumen/m_jenis_dokumen");
            $row['list_jenis_dokumen'] = $this->m_jenis_dokumen->GetDropDownJenis_dokumen();
            $row['id_unit_serial'] = $id_unit_serial;
            $javascript[] = "assets/plugins/select2/select2.js";
            $row['type_upload'] = "persediaan_unit";
            $css[] = "assets/plugins/select2/select2.css";
            $row['list_equipment'] = [];
            $row['type_upload'] = "Persediaan";
            $row['full_upload'] = true;
            $row['listimage'] = [];
            $this->load->model("module/m_module");
            $row['listimage'] = $this->m_module->GetFile("Persediaan", $row['id_unit_serial']);
            LoadTemplate($row, "persediaan_unit/v_persediaan_unit_manipulate", $javascript, $css);
        }
        else
        {
              SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
              redirect(base_url() . 'index.php/main/message');
        }
       
    }
    
    public function persediaan_unit_manipulate(){
        $message = '';

        $this->form_validation->set_rules('id_unit_serial', 'Unit', 'trim|required');
        $model = $this->input->post();
      
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
           $status = $this->m_persediaan_unit->PersediaanUnitManipulate($model);

            //$email = $this->send_email($status['id_prospek']);
            echo json_encode($status);
        }
        
    }
    
    
    
    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K071";
        $header = "K005";
        $title = "Unit";
        CekModule($module);
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("kategori/m_kategori");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("type_body/m_type_body");
        $this->load->model('unit/m_unit');
        $this->load->model('pool/m_pool');
        $vin= date("Y");
        $listvin=[];
        for($i=0;$i<=10;$i++)
        {
            $listvin[]=array("id"=>$vin-$i,"text"=>$vin-$i);
        }
        
        
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal Buat SPK");
        $jenis_pencarian[] = array("id" => "tanggal_buka_jual", "text" => "Tanggal Buka Jual");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_vin'] = $listvin;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $jenis_pencariankeyword = [];
        $jenis_pencariankeyword[] = array("id" => "engine_no", "text" => "No Mesin");
        $jenis_pencariankeyword[] = array("id" => "vin_number", "text" => "No Rangka");
        $jenis_pencariankeyword[] = array("id" => "no_prospek", "text" => "No SPK");
        $jenis_pencariankeyword[] = array("id" => "nomor_buka_jual", "text" => "No Buka Jual");
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $model['list_pool'] = $this->m_pool->GetDropDownPool();
        $model['list_pencarian_keyword'] = $jenis_pencariankeyword;
        $model['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $model['list_type_body'] = $this->m_type_body->GetDropDownType_body();
        $model['list_unit'] = $this->m_unit->GetDropDownUnit();
        LoadTemplate($model, "persediaan_unit/v_persediaan_unit_index", $javascript, $css);
    }

    public function add_perlengkapan($id_unit_sereal) {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K076";
        $header = "K059";
        $title = "Perlengkapan";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("jenis_dokumen/m_jenis_dokumen");
        $model['list_jenis_dokumen'] = $this->m_jenis_dokumen->GetDropDownJenis_dokumen();
        $model['id_unit_sereal'] = $id_unit_sereal;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "perlengkapan/v_perlengkapan_manipulate", $javascript, $css);
    }

    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        LoadTemplate($model, "persediaan_unit/v_persediaan_unit_detail", $javascript, $css);
    }

    public function per_group() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K055";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        $this->load->model("jenis/m_jenis");
        $model['list_jenis'] = $this->m_jenis->GetDropDownJenis();
        $this->load->model("merek/m_merek");
        $model['list_merek'] = $this->m_merek->GetDropDownMerek();
        LoadTemplate($model, "persediaan_unit/v_persediaan_unit_per_group", $javascript, $css);
    }

    public function pajak_masukan() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K056";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/js/icheck/icheck.min.js";
        $javascript[] = "assets/js/icheck/icheck-active.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        LoadTemplate($model, "persediaan_unit/v_persediaan_unit_pajak_masukan", $javascript, $css);
    }

    public function getdataunit() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_persediaan_unit->GetDataReportunit($params);
    }

    public function getdataunitdetail() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdataunitpergroup() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdataunitpajakmasukan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

}

/* End of file Barang.php */