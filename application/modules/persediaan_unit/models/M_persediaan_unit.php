<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_persediaan_unit extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function GetDataReportunit($params) {
        $this->load->library('datatables');
        $this->datatables->select('#_unit_serial.id_unit_serial,#_prospek.no_prospek,nama_customer,#_kpu.nomor_master as nomor_kpu, #_unit_serial.*,nama_warna,nama_kategori,nama_unit,nama_pool');
        $this->datatables->from("#_unit_serial");
        $this->datatables->join('#_warna', '#_warna.id_warna = #_unit_serial.id_warna', 'left');
        $this->datatables->join('#_kpu_detail', '#_kpu_detail.id_kpu_detail = #_unit_serial.id_kpu_detail', 'left');
        $this->datatables->join('#_kpu', '#_kpu.id_kpu = #_kpu_detail.id_kpu', 'left');
        $this->datatables->join('#_prospek_detail', '#_prospek_detail.id_prospek_detail = #_unit_serial.id_prospek_detail', 'left');
        $this->datatables->join('#_prospek', '#_prospek.id_prospek = #_prospek_detail.id_prospek', 'left');
        $this->datatables->join('#_customer', '#_customer.id_customer = #_prospek.id_customer', 'left');
        $this->datatables->join('#_pool', '#_unit_serial.id_pool = #_pool.id_pool', 'left');
        
        $this->datatables->join('#_unit', '#_unit.id_unit = #_unit_serial.id_unit', 'left');
        $this->datatables->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori', 'left');
        $extra=[];
        $where=[];
        if(!CheckEmpty($params['pencarian_keyword'])&&!CheckEmpty($params['keyword'])){
            $searchby=$params['pencarian_keyword'];
			
            
            if($params['pencarian_keyword']=="engine_no"||$params['pencarian_keyword']=="vin_number")
            {
                $this->datatables->like($searchby, strtolower($params['keyword']), 'both'); 
            }
            else
            {
                  $where["lower(".$searchby.")"] = strtolower($params['keyword']);
            }
               
            
        }
        else {

            if (!CheckEmpty($params['jenis_pencarian'])) {
                $tanggal = "date(#_prospek.tanggal_prospek)";
                if ($params['jenis_pencarian'] == "tanggal_buka_jual") {
                    $tanggal = "date(#_buka_jual.tanggal_buka_jual)";
                }
                else if ($params['jenis_pencarian'] == "tanggal_do") {
                    $tanggal = "date(#_do_prospek.tanggal_do)";
                }
                else if ($params['jenis_pencarian'] == "tanggal_do_kpu") {
                    $tanggal = "date(#_do_kpu.tanggal_do)";
                }
                $this->db->order_by($tanggal, "desc");


                if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                    array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                    unset($params['start_date'], $params['end_date']);
                } else {

                    if (isset($params['start_date']) && empty($params['end_date'])) {
                        $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                        unset($params['start_date']);
                    } else {
                        $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                        unset($params['end_date']);
                    }
                }
            }
		}
        if (!CheckEmpty(@$params['id_kategori'])) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_type_unit'])) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_type_body'])) {
            $where['#_unit.id_type_body'] = $params['id_type_body'];
        }
        if (!CheckEmpty(@$params['id_unit'])) {
            $where['#_unit.id_unit'] = $params['id_unit'];
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        // $straction = anchor(site_url('persediaan_unit/add_perlengkapan/$1'), 'Perlengkapan', array('class' => 'btn btn-primary btn-xs'));
        $straction = anchor(site_url('persediaan_unit/create_perlengkapan/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        $this->datatables->add_column('action', $straction, 'id_unit_serial');
        return $this->datatables->generate();
    }
    
    function PersediaanUnitManipulate($model)
    {
        $message="";
        if(!CheckEmpty($model['id_unit_serial']))
        {
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $update = [];
            $update['invoice_number'] = $model['invoice_number'];
			$update['nomor_do_supplier'] = $model['nomor_do_supplier'];
			$update['vin_number'] = $model['vin_number'];
			$update['engine_no'] = $model['engine_no'];
            $update['no_bpkb'] = $model['no_bpkb'];
            $update['nopol'] = $model['nopol'];
            $this->db->update("#_unit_serial", MergeUpdate($update), array("id_unit_serial" => $model['id_unit_serial']));
            $this->load->model("module/m_module");
            $listimage = CheckArray($model, "listimage");
            $this->m_module->SyncDb($listimage, "Persediaan", $model['id_unit_serial']);
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                $arrayreturn["st"] = true;
                return $arrayreturn;
            }
        }
        else
        {
            $arrayreturn["st"] = false;
        }
       
        
        
        
    }

    function GetUnitSerialByVrf ($id_vrf) {
        $this->db->select("nomor_master, nama_unit, date_do_in, #_do_prospek.tanggal_do as date_out, nomor_do_kpu, vin_number, #_unit_serial.vin, engine_no, nama_warna, #_unit_serial.free_parking");
        $this->db->where('#_vrf.id_vrf', $id_vrf);
        $this->db->where('#_unit_serial.status !=', 2);
        $this->db->join('#_kpu_detail', '#_kpu_detail.id_vrf = #_vrf.id_vrf', 'left');
        $this->db->join('#_kpu', '#_kpu.id_kpu = #_kpu_detail.id_kpu', 'left');
        $this->db->join('#_do_kpu_detail', '#_do_kpu_detail.id_kpu_detail = #_kpu_detail.id_kpu_detail', 'left');
        $this->db->join('#_do_kpu', '#_do_kpu.id_do_kpu = #_do_kpu_detail.id_do_kpu', 'left');
        $this->db->join('#_unit_serial', '#_do_kpu_detail.id_do_kpu_detail = #_unit_serial.id_do_kpu_detail', 'left');
        $this->db->join('#_do_prospek_detail', '#_unit_serial.id_do_prospek_detail = #_do_prospek_detail.id_do_prospek_detail', 'left');
        $this->db->join('#_do_prospek', '#_do_prospek.id_do_prospek = #_do_prospek_detail.id_do_prospek', 'left');
        $this->db->join('#_warna', '#_warna.id_warna = #_unit_serial.id_warna', 'left');
        $this->db->join('#_unit', '#_unit.id_unit = #_unit_serial.id_unit', 'left');
        $grid_unit_serial = $this->db->get('#_vrf')->result_array();
        return $grid_unit_serial;
    }

}
