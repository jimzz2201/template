<section class="content-header">
    <h1>
        Perlengkapan <?= @$button ?>
        <small>Perlengkapan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Perlengkapan</li>
        <li class="active">Perlengkapan <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>

            <div class="row">
                <div class="col-md-12">
                    <h3><span class="label label-default">Keterangan Unit</span></h3><br/>
                    <form id="frm_prospek" class="form-horizontal form-groups-bordered validate" method="post">
                        <div class="form-group col-md-12">
                            <?= form_input(array('value' => @$id_unit_serial, 'name' => 'id_unit_serial', 'type' => 'hidden', 'id' => 'txt_id_unit_serial')); ?>

                            <?= form_label('VRF', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$vrf_code, 'name' => 'vrf_code', 'class' => 'form-control', 'id' => 'txt_vrf_code', 'placeholder' => 'No VRF')); ?>
                            </div>
                            <div class="col-sm-1">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => DefaultTanggal(@$tanggal_vrf), 'name' => 'tanggal_vrf', 'class' => 'form-control', 'id' => 'txt_tanggal_vrf', 'placeholder' => 'Tanggal VRF')); ?>
                            </div>
                            <div class="col-sm-1">
                                <a type="button" target="_blank" class="btn btn-warning btn-block" href="<?php echo base_url() . "index.php/vrf/view_vrf/" . $id_vrf ?>" >Lihat VRF</a>
                            </div>
                            <?= form_label('KPU', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$nomor_kpu, 'name' => 'nomor_kpu', 'class' => 'form-control', 'id' => 'txt_nomor_kpu', 'placeholder' => 'No KPU')); ?>
                            </div>
                            <div class="col-sm-1">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => DefaultTanggal(@$tanggal_kpu), 'name' => 'tanggal_kpu', 'class' => 'form-control', 'id' => 'txt_tanggal_kpu', 'placeholder' => 'Tanggal KPU')); ?>
                            </div>
                            <div class="col-sm-1">
                                <a type="button" target="_blank" class="btn bg-light-blue-active btn-block" href="<?php echo base_url() . "index.php/kpu/view_kpu/" . $id_kpu ?>" >Lihat KPU</a>
                            </div>
                        </div>


                        <div class="form-group col-md-12">
                            <?= form_label('No Prospek', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$no_prospek, 'name' => 'no_prospek', 'class' => 'form-control', 'id' => 'txt_no_prospek', 'placeholder' => 'No SPK')); ?>
                            </div>
                            <div class="col-sm-1">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => DefaultTanggal(@$tanggal_prospek), 'name' => 'tanggal_prospek', 'class' => 'form-control', 'id' => 'txt_tanggal_prospek', 'placeholder' => 'Tanggal Prospek')); ?>
                            </div>
                            <div class="col-sm-1">
                                <?php if (!CheckEmpty(@$no_prospek)) { ?>
                                    <a type="button" target="_blank" class="btn bg-maroon-active btn-block" href="<?php echo base_url() . "index.php/prospek/view_prospek/" . $id_prospek ?>" >Lihat Prospek</a>
                                <?php } ?>
                            </div>
                            <?= form_label('No Buka Jual', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$nomor_buka_jual, 'name' => 'nomor_buka_jual', 'class' => 'form-control', 'id' => 'ttx_nomor_buka_jual', 'placeholder' => 'No Buka Jual')); ?>
                            </div>
                            <div class="col-sm-1">

                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => DefaultTanggal(@$tanggal_buka_jual), 'name' => 'tanggal_buka_jual', 'class' => 'form-control', 'id' => 'txt_tanggal_buka_jual', 'placeholder' => 'Tanggal Buka Jual')); ?>

                            </div>
                            <div class="col-sm-1">
                                <?php if (!CheckEmpty(@$nomor_buka_jual)) { ?>
                                    <a type="button" target="_blank" class="btn bg-teal-active btn-block" href="<?php echo base_url() . "index.php/buka_jual/view_buka_jual/" . $id_buka_jual ?>" >Lihat Buka Jual</a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <?= form_label('Nama Customer', "txt_nama_stnk", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'nama_customer', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Nama Customer')); ?>
                            </div>
                            <?= form_label('Posisi', "txt_nama_stnk", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'nama_pool', 'value' => @$nama_pool, 'class' => 'form-control', 'id' => 'txt_nama_pool', 'placeholder' => 'Nama Pool')); ?>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn bg-navy-active color-palette btn-block" >Lihat History</button>
                            </div>
                        </div>
						<div class="form-group col-md-12">
                            <?= form_label('Nomor DO', "txt_nama_stnk", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'name' => 'nomor_do_supplier',  'value' => @$nomor_do_supplier, 'class' => 'form-control', 'id' => 'nomor_do_supplier', 'placeholder' => 'NO DO')); ?>
                            </div>
                            <?= form_label('Invoice Number', "txt_nama_stnk", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?= form_input(array('type' => 'text', 'name' => 'invoice_number', 'value' => @$invoice_number, 'class' => 'form-control', 'id' => 'txt_invoice_number', 'placeholder' => 'Invoice Number')); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <?= form_label('Nama STNK', "txt_nama_stnk", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'name' => 'nama_stnk', 'readonly' => 'readonly', 'value' => @$nama_stnk, 'class' => 'form-control', 'id' => 'nama_stnk', 'placeholder' => 'Nama STNK')); ?>
                            </div>
                           
                        </div>
                        <div class="form-group col-md-12">
                            <?= form_label('Alamat STNK', "txt_biaya_bbn", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <?= form_textarea(array('type' => 'text', 'name' => 'alamat_stnk', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'alamat_stnk', 'placeholder' => 'Alamat STNK'), @$alamat_stnk); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <?= form_label('TDP / NIB', "txt_id_tdp", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'tdp_prospek', 'class' => 'form-control', 'id' => 'tdp_prospek', 'placeholder' => 'TDP'), @$tdp_prospek); ?>
                            </div>
                            <?= form_label('No KTP', "txt_id_no_ktp", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'no_ktp_prospek', 'class' => 'form-control', 'id' => 'no_ktp_prospek', 'placeholder' => 'No KTP'), @$no_ktp_prospek); ?>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <?= form_label('Unit', "txt_id_kategori", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'nama_unit', 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Nama_Unit'), @$nama_unit); ?>
                            </div>
                            <?= form_label('Tahun', "txt_tahun", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'tahun', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_tahun', 'placeholder' => 'Tahun')); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <?= form_label('Kategori', "txt_id_kategori", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori'), @$nama_kategori); ?>
                            </div>
                            <?= form_label('Type Unit', "txt_id_type_unit", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Type Unit'), @$nama_type_unit); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">

                            <?= form_label('Type Body', "txt_id_type_body", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => 'Type Body'), @$nama_type_body); ?>
                            </div>
                            <?= form_label('Jenis Plat', "txt_id_jenis_plat", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => 'Jenis Plat'), @$nama_jenis_plat); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">

                            <?= form_label('Warna Kabin', "txt_id_warna", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => 'Warna Kabin'), @$nama_warna); ?>
                            </div>
                            <?= form_label('Warna Karoseri', "txt_id_warna", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => 'Warna Karoseri'), @$nama_warna_karoseri); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">

                            <?= form_label('Engine Number', "txt_id_warna", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'name' => 'engine_no', 'class' => 'form-control', 'placeholder' => 'Engine Number'), @$engine_no); ?>
                            </div>
                            <?= form_label('Vin Number', "txt_id_warna", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?= form_input(array('type' => 'text', 'name' => 'vin_number', 'class' => 'form-control', 'placeholder' => 'Vin Number'), @$vin_number); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">

                            <?= form_label('No BPKB', "txt_id_warna", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text','name'=>'no_bpkb', 'class' => 'form-control', 'placeholder' => 'BPKB'), @$no_bpkb); ?>
                            </div>
                            <?= form_label('NO Polisi', "txt_id_warna", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-5">
                                <?= form_input(array('type' => 'text','name'=>'nopol',  'class' => 'form-control', 'placeholder' => 'NO Polisi'), @$nopol); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <?= form_label('Keterangan', "txt_biaya_bbn", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-10">
                                <?= form_textarea(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'placeholder' => 'Keterangan'), @$keterangan_detail); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <hr/>
                        </div>
                        <div style="clear:both"></div>
                        <div class="form-group" style="margin-left:10px;">
                            <?php include APPPATH . 'modules/module/views/upload_index_full.php'; ?>
                        </div>
                        
                        
                        <div class="form-group col-md-12">
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("select").select2();
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });

        
    });

    function deleteperlengkapan(id_perlengkapan) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/perlengkapan/perlengkapan_delete',
                    dataType: 'json',
                    data: {
                        id_perlengkapan: id_perlengkapan
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
    }
    $("#frm_prospek").submit(function () {
         
        swal({
            title: "Apakah kamu yakin ingin menginput data Persediaan berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/persediaan_unit/persediaan_unit_manipulate',
                    dataType: 'json',
                    data: $(this).serialize()+ '&' + $.param({listimage:listimage}),
                    success: function (data) {
                        if (data.st)
                        {
                            window.location.href = baseurl + 'index.php/prospek';
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });




        return false;

    })
    // $('#btt_modal_ok').click(function () {
    // 		console.log('test');
    // 		var file_data = $('#frm_perlengkapan').prop('files')[0];
    // 		var form_data = new FormData();
    // 		form_data.append('file', file_data);
    // 		console.log(dataToSend);
    // 		console.log('form_data');return false;
    // })

    // $("#frm_perlengkapan").submit(function () {
    // 		// var dataToSend = $(this).serialize() + '&file='+new FormData(this);
    // 		// var attach = new FormData($("#frm_perlengkapan")[0]);
    // var file_data = $(this).prop('files')[0];return false;
    // 		var form_data = new FormData();
    // 		// form_data.append('file', file_data);
    // console.log(dataToSend);
    // 		console.log('form_data');return false;
    //     $.ajax({
    //         type: 'POST',
    //         url: baseurl + 'index.php/perlengkapan/perlengkapan_manipulate',
    //         dataType: 'json',
    //         data: dataToSend,
    //         success: function (data) {
    //             if (data.st)
    //             {
    //                 // window.location.href=baseurl + 'index.php/perlengkapan';
    // 								messagesuccess(data.msg);
    //             }
    //             else
    //             {
    //                 messageerror(data.msg);
    //             }

    //         },
    //         error: function (xhr, status, error) {
    //             messageerror(xhr.responseText);
    //         }
    //     });
    //     return false;

    // })
</script>
