<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_bank extends CI_Model {

    public $table = '#_bank';
    public $id = 'id_bank';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatabank() {
        $this->load->library('datatables');
        $this->datatables->select('id_bank,kode_bank,nama_bank,nama_akun,no_akun,cabang_bank,#_bank.status,nama_gl_account');
        $this->datatables->from($this->table);
        $this->datatables->join("#_gl_account","#_bank.id_gl_account=#_gl_account.id_gl_account","left");
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_bank.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editbank($1);return false;"));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletebank($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_bank');
        return $this->datatables->generate();
    }

    // get all
    function GetOneBank($keyword, $type = 'id_bank') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $bank = $this->db->get($this->table)->row();
        return $bank;
    }

    function BankManipulate($model) {
        try {
           
            $bank['status'] = DefaultCurrencyDatabase($model['status']);
            $bank['nama_bank']=$model['nama_bank'];
            $bank['nama_akun']=$model['nama_akun'];
            $bank['no_akun']=$model['no_akun'];
            $bank['cabang_bank']=$model['cabang_bank'];
            $bank['id_gl_account']= ForeignKeyFromDb($model['id_gl_account']);
            $msg="";
            $id_bank="";
            if (CheckEmpty($model['id_bank'])) {
                $bank['kode_bank'] = AutoIncrement($this->table, 'B', 'kode_bank', 3);
                $bank= MergeCreated($bank);
                $bank['saldo_awal'] = 0;
                $bank['saldo_akhir'] = 0;
                $this->db->insert($this->table, $bank);
                $id_bank=$this->db->insert_id();
                $msg="Bank successfull added into database";
            } else {
                $bank= MergeUpdate($bank);
                $id_bank=$model['id_bank'];
                $this->db->update($this->table, $bank, array("id_bank" => $model['id_bank']));
                $msg = "Bank has been updated";
            }
            $url = "index.php/bank";
            $arrchildid = [];
            $childwhere = array("nama_gl_module" => "bank", "type" => "bank", "id_subject" => $id_bank);
            $this->db->from("#_gl_config");
            $this->db->join("#_gl_module", "#_gl_module.id_gl_module=#_gl_config.id_gl_module");

            $this->db->where($childwhere);
            $rowchild = $this->db->get()->row();

            if(!CheckEmpty(@$bank['id_gl_account']))
            {
                
                if ($rowchild) {
                    $arrchildid[] = $rowchild->id_gl_config;
                    $this->db->update("#_gl_config", MergeUpdate(array("id_gl_account" => $bank['id_gl_account'], "description" => "Setting dari Bank", "url" => $url)), array('id_gl_config' => $rowchild->id_gl_config));
                } else {
                    $childinsert = $childwhere;
                    unset($childinsert['nama_gl_module']);
                    $childinsert['id_gl_account'] = $bank['id_gl_account'];
                    $childinsert['description'] = "Setting dari Bank";
                    $childinsert['id_gl_module'] = 10;
                    $childinsert['url'] = $url;
                    $this->db->insert("#_gl_config", MergeCreated($childinsert));
                    $insert_id = $this->db->insert_id();
                    $arrchildid[] = $insert_id;
                }
            }
            else if(!CheckEmpty($rowchild))
            {
                $this->db->delete("#_gl_config",array("id_gl_config"=>$rowchild->id_gl_config));
            }
            
            return array("st" => true, "msg" => $msg);
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownBank() {
        $listbank = GetTableData($this->table, 'id_bank', 'nama_akun', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listbank;
    }

    function BankDelete($id_bank) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_bank' => $id_bank));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Bank has been deleted from database");
    }

}
