<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bank extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_bank');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K048";
        $header = "K001";
        $title = "Bank";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	CekModule($module);
	LoadTemplate($model, "bank/v_bank_index", $javascript);
        
    } 
    public function get_one_bank() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_bank', 'Bank', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_bank->GetOneBank($model["id_bank"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Bank tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatabank() {
        header('Content-Type: application/json');
        echo $this->m_bank->GetDatabank();
    }
    
    public function create_bank() 
    {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K048";
        $header = "K001";
        $row['title'] = "Add Bank";
        CekModule($module);
        $this->load->model("gl/m_gl");
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_gl_account']=$this->m_gl->GetDropDownGl(0);
        $this->load->view('bank/v_bank_manipulate', $row);
    }
    
    public function bank_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('nama_bank', 'Nama Bank', 'trim|required');
	$this->form_validation->set_rules('nama_akun', 'Nama Akun', 'trim|required');
	$this->form_validation->set_rules('no_akun', 'No Akun', 'trim|required');
	$model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_bank->BankManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_bank($id=0) 
    {
        $row = $this->m_bank->GetOneBank($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	    $module = "K048";
            $header = "K001";
            $row['title'] = "Edit Bank";
            CekModule($module);
            $this->load->model("gl/m_gl");
            $row['form'] = $header;
            $row['formsubmenu'] = $module;   
            $row['list_gl_account']=$this->m_gl->GetDropDownGl(0);
	    $this->load->view('bank/v_bank_manipulate', $row);
        } else {
            SetMessageSession(0, "Bank cannot be found in database");
            redirect(site_url('bank'));
        }
    }
    
    public function bank_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_bank', 'Bank', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_bank->BankDelete($model['id_bank']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Bank.php */