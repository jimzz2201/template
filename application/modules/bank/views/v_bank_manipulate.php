<div class="modal-header">
    Bank <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_bank" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_bank" value="<?php echo @$id_bank; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Bank', "txt_kode_bank", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?php
                $mergearray = array();
                if (!CheckEmpty(@$id_bank)) {
                    $mergearray['disabled'] = "disabled";
                } else {
                    $mergearray['name'] = "kode_bank";
                }
                ?>
                <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_bank, 'class' => 'form-control', 'id' => 'txt_kode_bank', 'placeholder' => 'Kode Bank'))); ?>
            </div>
            <?= form_label('Nama&nbsp;Bank', "txt_nama_bank", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'text', 'name' => 'nama_bank', 'value' => @$nama_bank, 'class' => 'form-control', 'id' => 'txt_nama_bank', 'placeholder' => 'Nama Bank')); ?>
            </div>
        </div>
        <div class="form-group">

        </div>
        <div class="form-group">
            <?= form_label('Nama Akun', "txt_nama_akun", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'name' => 'nama_akun', 'value' => @$nama_akun, 'class' => 'form-control', 'id' => 'txt_nama_akun', 'placeholder' => 'Nama Akun')); ?>
            </div>
            <?= form_label('No Akun', "txt_no_akun", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'text', 'name' => 'no_akun', 'value' => @$no_akun, 'class' => 'form-control', 'id' => 'txt_no_akun', 'placeholder' => 'No Akun')); ?>
            </div>
        </div>

        <div class="form-group">
            <?= form_label('Cabang Bank', "txt_cabang_bank", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'name' => 'cabang_bank', 'value' => @$cabang_bank, 'class' => 'form-control', 'id' => 'txt_cabang_bank', 'placeholder' => 'Cabang Bank')); ?>
            </div>

            <?= form_label('Gl', "txt_cabang_bank", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_dropdown(array("selected" => @$id_gl_account, "name" => "id_gl_account"), DefaultEmptyDropdown(@$list_gl_account, "json", "Akun"), @$id_gl_account, array('class' => 'form-control', 'id' => 'dd_id_gl_account')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Saldo&nbsp;Awal', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'saldo_awal', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_awal', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>
            <?= form_label('Saldo&nbsp;Akhir', "txt_saldo_akhir", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-5">
                <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'saldo_akhir', 'value' => DefaultCurrency(@$saldo_akhir), 'class' => 'form-control', 'id' => 'txt_saldo_akhir', 'placeholder' => 'Saldo Akhir', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>
        </div>

        <div class="form-group">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $(document).ready(function(){
        $("select").select2();
    })
    $("#frm_bank").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/bank/bank_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>