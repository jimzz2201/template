<div class="modal-header">
        Type_body <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_type_body" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_type_body" value="<?php echo @$id_type_body; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Type Body', "txt_kode_type_body", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?php $mergearray=array();
                            if(!CheckEmpty(@$id_type_body))
                            {
                                $mergearray['readonly']="readonly";
                                $mergearray['name'] = "kode_type_body";
                            }
                            else
                            {
                                $mergearray['name'] = "kode_type_body";
                            }?>
                    <?= form_input(array_merge($mergearray,array('type' => 'text', 'value' => @$kode_type_body, 'class' => 'form-control', 'id' => 'txt_kode_type_body', 'placeholder' => 'Kode Type Body'))); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Type Body', "txt_nama_type_body", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_type_body', 'value' => @$nama_type_body, 'class' => 'form-control', 'id' => 'txt_nama_type_body', 'placeholder' => 'Nama Type Body')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_type_body").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/type_body/type_body_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>