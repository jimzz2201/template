<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_type_body extends CI_Model
{

    public $table = '#_type_body';
    public $id = 'id_type_body';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatatype_body() {
        $this->load->library('datatables');
        $this->datatables->select('id_type_body,kode_type_body,nama_type_body,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_type_body.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "edittype_body($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletetype_body($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_type_body');
        return $this->datatables->generate();
    }

    // get all
    function GetOneType_body($keyword, $type = 'id_type_body') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $type_body = $this->db->get($this->table)->row();
        return $type_body;
    }

    function Type_bodyManipulate($model) {
        try {
                $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_type_body'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		return array("st" => true, "msg" => "Type_body successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_type_body" => $model['id_type_body']));
		return array("st" => true, "msg" => "Type_body has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownType_body() {
        $listtype_body = GetTableData($this->table, 'id_type_body', 'nama_type_body', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listtype_body;
    }

    function Type_bodyDelete($id_type_body) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_type_body' => $id_type_body));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Type_body has been deleted from database");
    }


}