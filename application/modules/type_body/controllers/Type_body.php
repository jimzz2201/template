<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Type_body extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_type_body');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K056";
        $header = "K001";
        $title = "Type Body";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "type_body/v_type_body_index", $javascript);
        
    } 
    public function get_one_type_body() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_type_body', 'Type Body', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_type_body->GetOneType_body($model["id_type_body"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Type Body tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatatype_body() {
        header('Content-Type: application/json');
        echo $this->m_type_body->GetDatatype_body();
    }
    
    public function create_type_body() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
        $module = "K056";
        $header = "K001";
        $row['title'] = "Add Type Body";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	    $this->load->view('type_body/v_type_body_manipulate', $row);       
    }
    
    public function type_body_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_type_body', 'Kode Type Body', 'trim|required');
	$this->form_validation->set_rules('nama_type_body', 'Nama Type Body', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_type_body->Type_bodyManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_type_body($id=0) 
    {
        $row = $this->m_type_body->GetOneType_body($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K056";
            $header = "K001";
            $row['title'] = "Edit Type Body";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    $this->load->view('type_body/v_type_body_manipulate', $row);
        } else {
            SetMessageSession(0, "Type_body cannot be found in database");
            redirect(site_url('type_body'));
        }
    }
    
    public function type_body_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_type_body', 'Type_body', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_type_body->Type_bodyDelete($model['id_type_body']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Type_body.php */