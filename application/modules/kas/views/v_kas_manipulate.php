<section class="content-header">
    <h1>
        Create Kas
        <small>Data Entry</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Entry</li>
        <li class="active">Create Kas</li>
    </ol>


</section>
<style>
    .label-cek {
        cursor: pointer;
    }

    .datepicker-days table tbody tr td.disabled {
        color: #777777 !important;
        text-decoration: line-through;
    }
</style>
<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_kas" class="form-horizontal form-groups-bordered validate" method="post">
                <input type="hidden" name="id_kas" value="<?php echo @$id_kas; ?>"/>
                <input type="hidden" name="is_checked" value="<?php echo @$is_checked; ?>"/>
                <input type="hidden" name="id_check" value="<?php echo @$id_check; ?>"/>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Tanggal', "txt_tanggal_kas", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_input(array('type' => 'text', 'autocomplete' => "off", 'name' => 'tanggal_kas', 'value' => DefaultDatePicker(@$tanggal_kas), 'class' => 'form-control text-center datepicker', 'id' => 'txt_tanggal_kas', 'placeholder' => 'Tanggal', 'required' => 'required')); ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?= form_dropdown(array('name' => 'id_cabang', 'class' => 'form-control selectoption', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$id_cabang); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= form_label('Pembayaran', "dd_id_jenis_transaksi", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php
                                        $field_jenis_transaksi = form_dropdown(array('name' => 'id_jenis_transaksi', 'class' => 'form-control', 'id' => 'dd_id_jenis_transaksi', 'placeholder' => 'Jenis Pembayaran', 'required' => 'required'), DefaultEmptyDropdown(@$list_jenis_transaksi, "json", "Jenis "), @$id_jenis_transaksi);
                                        if ($is_edit) {
                                            if ($edit_jenis_transaksi == 0) {
                                                $field_jenis_transaksi = form_input(array('type' => 'hidden', 'name' => 'id_jenis_transaksi'), $id_jenis_transaksi);
                                                $field_jenis_transaksi .= form_input(array('type' => 'text', 'disabled' => "disabled", 'class' => 'form-control'), getJenisTransaksi($id_jenis_transaksi));
                                            }
                                        }

                                        echo $field_jenis_transaksi;
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row divbank" style="display:<?php echo @$id_jenis_transaksi == "33" ? "block" : "none" ?>">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= form_label('Bank', "dd_id_cabang", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?= form_dropdown(array('name' => 'id_bank', 'class' => 'form-control selectoption', 'id' => 'dd_id_bank', 'placeholder' => 'Bank'), DefaultEmptyDropdown(@$list_bank, "json", "Bank"), @$id_bank); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= form_label('Payment Voucher', "txt_payment_voucher", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?= form_input(array('name' => 'payment_voucher', 'class' => 'form-control', 'id' => 'txt_payment_voucher', 'placeholder' => 'Payment Voucher', 'rows' => 2, 'maxlenght' => 255), @$paymentvoucher); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">

                        <div class="row">


                            <div class="col-sm-12">
                                <div class="form-group" id="field_tgl_trasfer" style="display: <?= @$terlihat ?>;">
                                    <?= form_label('Tgl Transfer', "txt_tgl_transfer", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-8">
                                        <?php
                                        $field_tgl_transfer = form_input(array('type' => 'text', 'name' => 'tgl_transfer', 'value' => DefaultDatePicker(@$tgl_transfer), 'class' => 'form-control text-center datepicker_transfer', 'id' => 'txt_tgl_transfer', 'placeholder' => 'Tgl Transfer'));
                                        if ($is_edit) {
                                            if ($edit_tgl_transfer == 0 || $edit_jenis_transaksi == 0) {
                                                $field_tgl_transfer = form_input(array('type' => 'hidden', 'name' => 'tgl_transfer'), $tgl_transfer);
                                                $field_tgl_transfer .= form_input(array('type' => 'text', 'disabled' => "disabled", 'class' => 'form-control'), DefaultDatePicker(@$tgl_transfer));
                                            }
                                        }

                                        echo $field_tgl_transfer;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <hr/>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Customer', "txt_id_jenis_kas", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_dropdown(array("name" => "id_customer"), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer, array('class' => 'form-control', 'id' => 'dd_id_customer')); ?>
                            </div>   
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Unit', "txt_id_jenis_kas", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_dropdown(array("name" => "id_unit_serial"), DefaultEmptyDropdown(@$list_serial_unit, "json", "Serial Unit"), @$id_unit_serial, array('class' => 'form-control ', 'id' => 'dd_id_serial_unit')); ?>
                            </div>   
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Jenis Transaksi', "txt_id_jenis_kas", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_dropdown(array('name' => 'id_jenis_kas', 'class' => 'form-control', 'id' => 'dd_id_jenis_kas', 'placeholder' => 'Jenis Transaksi'), DefaultEmptyDropdown(getJenisAliranKas(), "json", "Jenis"), @$id_jenis_kas); ?>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" id="field_biaya">
                            <?= form_label('Group', "txt_id_biaya", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?php
                                $field_group_biaya = form_dropdown(array('name' => 'id_biaya_group', 'class' => 'form-control', 'id' => 'dd_id_biaya_group', 'placeholder' => 'Jenis Biaya'), DefaultEmptyDropdown(@$list_biaya_group, "json", "Biaya Group"), @$id_biaya_group);
                                if ($is_edit) {
                                    if ($edit_jenis_biaya == 0) {
                                        $field_group_biaya = form_input(array('type' => 'hidden', 'name' => 'id_jenis_pengeluaran'), $id_jenis_pengeluaran);
                                        $field_group_biaya .= form_input(array('type' => 'text', 'disabled' => "disabled", 'class' => 'form-control'), getGroupBiaya($id_jenis_pengeluaran));
                                    }
                                }

                                echo $field_group_biaya;
                                ?>
                            </div>
                        </div>
                    </div>



                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group" id="field_biaya">
                            <?= form_label('Account', "txt_id_jenis_biaya", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?php
                                $field_jenis_biaya = form_dropdown(array('name' => 'id_biaya_master', 'class' => 'form-control', 'id' => 'dd_id_biaya_master', 'placeholder' => 'Biaya'), DefaultEmptyDropdown(@$list_biaya_master, "json", "Biaya"), @$id_biaya_master);
                                if ($is_edit) {
                                    if ($edit_jenis_biaya == 0) {
                                        $field_jenis_biaya = form_input(array('type' => 'hidden', 'name' => 'id_jenis_biaya'), $id_jenis_biaya);
                                        $field_jenis_biaya .= form_input(array('type' => 'text', 'disabled' => "disabled", 'class' => 'form-control'), getJenisBiayaKas($id_jenis_biaya));
                                    }
                                }

                                echo $field_jenis_biaya;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Jumlah', "txt_jumlah", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_input(array('type' => 'text', 'name' => 'jumlah', 'value' => DefaultCurrency(@$jumlah), 'class' => 'form-control text-right', 'id' => 'txt_jumlah', 'placeholder' => 'Jumlah', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= form_label('Keperluan', "txt_keterangan", array("class" => 'col-sm-4 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_textarea(array('name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keperluan', 'rows' => 2, 'maxlenght' => 255), @$keterangan); ?>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-sm-6">


                        <div class="form-group">
                            <?= form_label('Print Receipt', "txt_tanggal_pengeluaran", array("class" => 'col-sm-4 control-label')); ?>

                            <div class="col-sm-8">
                                <div class="upload-btn-wrapper">
                                    <button type="button" class="btn" id="btt_upload_receipt">Upload Receipt</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8">
                        <div class="portlet-body form">
                            <table class="table table-striped table-bordered table-hover" id="receipttable">

                            </table>
                        </div>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 text-center">
                    </div>
                    <div class="col-sm-8 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block" id="btt_save_detail">Save</button>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center">
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet-body form">
                            <table class="table table-striped table-bordered table-hover" id="mytable">
                                <tfoot><th></th><th></th><th></th><th class="text-right"></th><th class="text-right"></th><th class="text-right"></th><th></th><th></th></tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo base_url() ?>index.php/kas" class="btn btn-default"
                       style="float:left;">Cancel</a>
                    <button type="button" class="btn btn-primary" id="btt_modal_ok" style="float:right">
                        Save
                    </button>
                </div>

            </form>
        </div>
    </div>
</section>
<script>
    var table, receipttable;
    var listdetail = [];
    var listreceipt = [];

    $("#dd_id_customer").change(function(){
        
        var id_customer = $("#dd_id_customer").val();
        LoadBar.show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_unit_serial_full',
            dataType: 'json',
            data: {
                id_customer: id_customer,
                isfull:true
            },
            success: function (data) {
                $("#dd_id_unit_serial" ).empty();
                if (data.st)
                {
                    $("#dd_id_serial_unit" ).select2({data: data.list_unit_serial});

                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    })
    
    function RefreshGridDetail() {
        table.fnClearTable();
        if (listdetail.length > 0)
        {
            table.fnDraw(true);
            table.fnAddData(listdetail);
        }

    }
    $("#dd_id_jenis_transaksi").change(function () {
        listdetail = [];
        RefreshGridDetail();
        if ($(this).val() == "33")
        {
            $(".divbank").css("display", "block");
        } else
        {
            $(".divbank").css("display", "none");
        }

    })
    $("#dd_id_project").change(function () {
        $(".summaryproject").html("");
        if ($(this).val() != "0") {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/project/GetTotalByCabang',
                data: $("#frm_kas").serialize(),
                dataType: "json",
                success: function (data) {
                    if (data.data.length > 0) {
                        var message = "";
                        $(data.data).each(function (index, element) {
                            message += "Nama Jenis Transaksi : " + element.nama_jenis_transaksi + "<br/>";
                            message += "Debet : " + CommaMin(element.debet) + "<br/>";
                            message += "Kredit : " + CommaMin(element.kredit) + "<br/>";
                            message += "======================================<br/>";
                        });
                        $(".summaryproject").html(message);
                    }
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            })
        }
    });



    function RefreshImageThumbnails() {
        $(".popimage").click(function () {
            modalbootstrap("<div class='modal-body text-center'><img src='" + $(this).attr("src") + "'/></div>", "Receipt File");
        })
    }

    function deletereceipt(imagename) {
        var indexnum = findIndexInData(listreceipt, "receiptfile", imagename);
        listreceipt.splice(indexnum, 1);
        RefreshImageList();
    }

    $(document).ready(function () {
        $("select").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $("#txt_tanggal_kas").datepicker({
            autoclose: true,
            format: 'dd M yyyy',
            weekStart: 1,
            language: "id",
            todayHighlight: true,

        });
        UpdateBiaya();



        table = $("#mytable").dataTable({
            oLanguage: {
                sProcessing: "loading..."
            },
            data: listdetail,
            pageLength: 25,
            columns: [
                {data: "no", orderable: false, title: "No"}
                , { orderable: true, title: "Unit",
                    mRender: function (data, type, row) {
                        return row['engine_no']+" - "+row['vin_number']+" - "+row['no_prospek'];
                    }}
                , {data: "nama_group", orderable: true, title: "Group"}
                , {data: "nama_biaya", orderable: false, title: "Biaya"}
                , {
                    data: "debet", orderable: false, title: "Debet", className: 'text-right',
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }
                }
                , {
                    data: "kredit", orderable: false, title: "Kredit", className: 'text-right',
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }
                }
                , {data: "keterangan", orderable: false, title: "Keterangan"}
                , {
                    data: "listreceipt", orderable: false, title: "Receipt",
                    mRender: function (data, type, row) {
                        var img = "";
                        $.each(data, function (key, value) {
                            img += "<img src='<?php echo base_url() ?>uploads/receipts/" + value.receiptfile + "' class='popimage'/>";
                        });
                        return img;
                    }
                }
                , {
                    data: "action", orderable: false, title: "", className: 'text-center', width: "100px",
                    mRender: function (data, type, row, meta) {
                        if (row['deleted_date'] != null)
                            return "Batal";
                        else
                            return "<a class=\"btn btn-xs btn-danger\" onclick=\"deletebiaya(" + meta.row + ")\"><i class=\"fa fa-close\"></i>&nbsp;Delete</a>";
                    }
                }
            ],
            fnFooterCallback: function (row, data, start, end, display) {
                var api = this.api();
                var preview_total = 0;
                var preview_debet = 0;
                var preview_kredit = 0;

                for (var zz = 0; zz < listdetail.length; zz++) {
                    preview_debet += parseInt(listdetail[zz].debet);
                    preview_kredit += parseInt(listdetail[zz].kredit);
                }
                preview_total = preview_debet - preview_kredit;
//				$( api.column( 4 ).footer() ).html(
//					Comma(preview_debet)
//				);
//				$( api.column( 5 ).footer() ).html(
//					Comma(preview_kredit)
//				);
//				$( api.column( 6 ).footer() ).html(
//					Comma(preview_total)
//				);
                $("#mytable tfoot th:nth-child(4)").html(Comma(preview_debet));
                $("#mytable tfoot th:nth-child(5)").html(Comma(preview_kredit));
                $("#mytable tfoot th:nth-child(6)").html('= ' + Comma(preview_total));
            }
        });

        receipttable = $("#receipttable").dataTable({
            oLanguage: {
                sProcessing: "loading..."
            },
            data: listreceipt,
            pageLength: 25,
            columns: [
                {
                    data: "receiptfile", orderable: false, title: "File", className: 'text-right',
                    mRender: function (data, type, row) {
                        return "<img height='50px' class='popimage' src='<?php echo base_url() ?>uploads/receipts/" + data + "' />";
                    }
                },
                {
                    data: "receiptfile", orderable: false, title: "", className: 'text-center', width: "100px",
                    mRender: function (data, type, row) {
                        return "<a class='btn btn-xs btn-danger' onclick=\"deletereceipt('" + data + "')\"><i class='fa fa-close'></i>&nbsp;Delete</a>";
                    }
                }
            ],
            order: []
        });



        $("#dd_id_biaya_group , #dd_id_jenis_kas , #dd_id_jenis_transaksi ").change(function ()
        {
            UpdateBiaya();
        });
    })
    function UpdateBiaya()
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/biaya_master/getdropdownbiaya',
            dataType: 'json',
            data: $("#frm_kas").serialize(),
            success: function (data) {
                $("#dd_id_biaya_master").empty();
                $.each(data, function (key, value) {
                    $("#dd_id_biaya_master").append("<option value='" + value.id + "'>" + value.text + "</option>");
                });
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    }
    $("#btt_upload_receipt").click(function () {

        $.ajax({
            url: baseurl + 'index.php/kas/receipt_file',
            success: function (data) {
                modalbootstrap(data, "Receipt File");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    });

    function deletebiaya(indexnum) {
        listdetail.splice(indexnum, 1);
        table.fnClearTable();
        if (listdetail.length > 0) {
            table.fnDraw(true);
            table.fnAddData(listdetail);
        }
    }

    $("#btt_modal_ok").click(function () {
        swal({
            title: "Apakah Kamu yakin akan menginput data berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Saya Yakin!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                var lanjut = true;

                if (lanjut) {
                    $.ajax({
                        type: 'POST',
                        url: baseurl + 'index.php/kas/kas_manipulate',
                        dataType: 'json',
                        data: $("#frm_kas").serialize() + '&' + $.param({listdetail: listdetail}),
                        success: function (data) {
                            if (data.st) {
                                listdetail = [];
                                RefreshGridDetail();
                                // messageerror("Sukses");
                                window.location.href = "<?php echo base_url() ?>index.php/kas?tgl=" + $("#txt_tanggal_kas").val();
                            } else {
                                messageerror(data.msg);
                            }

                        },
                        error: function (xhr, status, error) {
                            messageerror(xhr.responseText);
                        }
                    });
                }
            }
        });
        return false;
    });

    function RefreshImageList() {
        receipttable.fnClearTable();
        if (listreceipt.length > 0) {
            receipttable.fnDraw(true);
            receipttable.fnAddData(listreceipt);
        }
        RefreshImageThumbnails();
    }


    $("#frm_kas").submit(function () {
        $.ajax({
            url: baseurl + 'index.php/kas/save_detail',
            method: 'POST',
            dataType: "Json",
            data: $("#frm_kas").serialize() + '&' + $.param({listdetail: listdetail, listreceipt: listreceipt}),
            success: function (data) {
//				var preview_total = 0;
//				var preview_debet = 0;
//				var preview_kredit = 0;
//				$("#mytable tfoot").remove();
                if (data.st) {
                    listdetail = data.listdetail;
//					$("#mytable").append('<tfoot><th></th><th></th><th></th><th></th><th class="text-right"></th><th class="text-right"></th><th class="text-right"></th><th></th><th></th></tfoot>');
                    table.fnClearTable();
                    table.fnDraw(true);
                    table.fnAddData(listdetail);

                    $("#dd_id_biaya_group").val(0);
                    $("#dd_id_biaya_group").trigger("change");
                    $("#dd_id_biaya_master").val(0);
                    $("#dd_id_biaya_master").trigger("change");
                    $("#dd_id_jenis_kas").val(0);
                    $("#dd_id_jenis_kas").trigger("change");
                    $("#txt_keterangan").val("").empty();
                    $("#txt_jumlah").val(0);
                    $("#btt_modal_ok").css("display", "block");
                    listreceipt = [];
                    RefreshImageList();
//					for(var zz=0;zz<listdetail.length;zz++){
//						preview_debet += parseInt(listdetail[zz].debet);
//						preview_kredit += parseInt(listdetail[zz].kredit);
//					}
//					preview_total = preview_debet - preview_kredit;
                } else {
                    messageerror(data.msg);
                }
                $("#btt_save_detail").addClass("btn-primary").removeClass("btn-success");
            },
            error: function (xhr, status, error) {
                messageerror("Terjadi Kesalahan!");
                $("#btt_save_detail").addClass("btn-primary").removeClass("btn-success");
            },
            statusCode: {
                401: function () {
                    messageerror("Sesi Anda telah habis! Silahkan Login ulang");
                },
                404: function () {
                    messageerror("Halaman yang Anda akses tidak ditemukan!");
                },
                303: function () {
                    messageerror("Anda tidak memiliki Hak Akses!");
                }
            }
        });
        return false;
    })

</script>

