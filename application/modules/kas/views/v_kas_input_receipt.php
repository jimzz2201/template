<div class="modal-header">
    Upload Receipt
</div>
<div class="modal-body">
    <form id="frmuploadreceipt" class="form-horizontal form-groups-bordered validate" enctype="multipart/form-data" method="post">
        <input type="file" name="userfile" id="myfile" /><br/><br/>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $("form#frmuploadreceipt").submit(function () {
        var file = document.getElementById("myfile").files[0]; //fetch file
        var formData = new FormData(document.getElementById('frmuploadreceipt'));
        formData.append('file', file); //append file to formData object

        $.ajax({
            url: "<?php echo base_url() . 'index.php/kas/receipt_upload'; ?>",
            data: formData,
            contentType: false,
            processData: false,
            type: 'POST',
            mimeType: "multipart/form-data",
            dataType: 'Json',
            cache: false,
            success: function (data) {
                if (data.st)
                {
                  
                    listreceipt.push(data.receiptfile);
                    RefreshImageList();


                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }
            }
            , error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });

        return false;
    })


</script>