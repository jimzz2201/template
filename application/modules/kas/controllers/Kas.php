<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_kas');
       
    }
    private $_prefix = "_kas";
    public function index() {
        $javascript = array();
        $data = [];
       
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_kas", "text" => "Tanggal Transaksi");
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PROSPEK");
        $jenis_keyword[] = array("id" => "nomor_buka_jual", "text" => "NO BUKA JUAL");
        $jenis_keyword[] = array("id" => "engine_no", "text" => "NO Mesin Kendaraan");
        $jenis_keyword[] = array("id" => "vin_number", "text" => "NO Rangka Kendaraan");
        $data['list_pencarian'] = $jenis_pencarian;
        $status[] = array("id" => "2", "text" => "Batal");
        $status[] = array("id" => "1", "text" => "Non Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $data['list_status'] = $status;
        $data['start_date'] = GetCookieSetting("start_date".$this->_prefix, AddDays(GetDateNow(), "-1 month"));
        $data['end_date'] = GetCookieSetting("end_date".$this->_prefix, GetDateNow());
        
        $data['value_status'] = GetCookieSetting("status".$this->_prefix, GetCookieSetting("search".$this->_prefix)=="1"?0:7);
        $data['id_cabang'] = GetCookieSetting("id_cabang".$this->_prefix, 0);
        $data['jenis_pencarian'] = GetCookieSetting("jenis_pencarian".$this->_prefix);
        $data['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_sufix, "nomor".$this->_prefix);
        $data['list_keyword'] = $jenis_keyword;
        $css = [];
        $css[] = 'assets/plugins/iCheck/all.css';
        $data['openmenu'] = "menudateentry";
        $data['currentmenu'] = "likas";
        //$this->load->model("jenis_transaksi/m_jenis_transaksi");
        //$data['listtransaksi'] = $this->m_jenis_transaksi->GetDropDownJenisTransaksi(GetDefaultCabang());
        $module = "K085";
        $header = "K059";
        $title = "Aliran Kas";
        $data['title'] = "Aliran Kas";
        CekModule($module);
        $data['form'] = $header;
        $data['formsubmenu'] = $module;
        $this->load->model("cabang/m_cabang");
        $data['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        LoadTemplate($data, "kas/v_kas_index", $javascript, $css);
    }

    public function receipt_upload() {
        $config['upload_path'] = './uploads/receipts';
        $config['allowed_types'] = 'gif|jpg|png';
        $data = array();
        $data['st'] = false;
        $config['max_size'] = '1000000';


        if ($_FILES['userfile']['name'] != "") {
            $filename = date('Ymdhis') . '_' . preg_replace("/[^.\w]+/", "", $_FILES['userfile']['name']);

            $config['file_name'] = $filename;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $data = array('st' => false, 'error' => $this->upload->display_errors());
            } else {
                $data = array('st' => true, 'upload_data' => $this->upload->data(), "receiptfile" => array("receiptfile" => $filename));
            }
        } else {
            $data["msg"] = "Data Upload masih kosong";
        }
        echo json_encode($data);
    }

    public function getdatakas() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $search);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_prefix, 1);
            }
            SetCookieSetting($key . $this->_prefix, $value);
        }
        echo $this->m_kas->GetDataKas($search);

//        echo $this->db->last_query();
    }

    public function getdatakasmaster() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $search);
        echo $this->m_kas->GetDataKasMaster($search);
//        echo $this->db->last_query();
    }

    public function receipt_file() {

        $this->load->view("v_kas_input_receipt");
    }

    public function create_kas($tanggal = "") {
        $row = array();
        $curDate = $this->input->post('curDate');
        $row['tanggal_kas'] = GetDateNow();

        if (!CheckEmpty($tanggal)) {
            $row['tanggal_kas'] = DefaultDatePicker(urldecode($tanggal));
        }

        $module = "K085";
        $header = "K059";
        $title = "Aliran Kas";
        $row['title'] = "Aliran Kas";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;

        $this->load->model("cabang/m_cabang");
        $this->load->model("bank/m_bank");

        $paymentvoucher = "";




        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";

        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';

        $css = [];
        $this->load->model("cabang/m_cabang");

        $css[] = 'assets/plugins/iCheck/all.css';
        $this->load->model("biaya_master/m_biaya_master");
        $row['list_biaya_group'] = $this->m_biaya_master->GetDropDownBiaya_group();
        $row['paymentvoucher'] = $paymentvoucher;
        $row['id_jenis_transaksi'] = 7;
        $row['tgl_transfer'] = date('Y-m-d');
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $row['list_bank'] = $this->m_bank->GetDropDownBank();
        if ($curDate) {
            $row['tanggal_kas'] = $curDate;
        }
        $row['button'] = 'Tambah';
        $row['terlihat'] = 'none';
        $row['is_edit'] = false;
        $row['list_jenis_transaksi'] = array(array("id" => "32", "text" => "Kas Kecil"), array("id" => "3", "text" => "Kas Besar"), array("id" => "33", "text" => "Bank"));


        LoadTemplate($row, 'kas/v_kas_manipulate', $javascript, $css);
    }

    public function kas_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('tanggal_kas', 'tanggal kas', 'trim|required');
        $this->form_validation->set_rules('id_jenis_transaksi', 'Pembayaran', 'trim|required');
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
//        $this->form_validation->set_rules('biaya_bank', 'biaya bank', 'trim|numeric');
        if (CheckArray($model, 'listdetail'))
            $listdetail = $model["listdetail"];
        else {
            $listdetail = [];
        }
        if (count($listdetail) == 0) {
            $message .= "Detail Transaksi harus dimasukkan<br/>";
        }


        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim');
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_kas->KasManipulate($model);
            echo json_encode($status);
        }
    }

    public function viewdetail($id_kas_master = 0) {
        $listdetail = $this->m_kas->GetDataDetailKas($id_kas_master);
        $this->load->view("v_kas_detail", array("listdetail" => $listdetail));
    }

    public function save_detail() {
        $this->load->model("unit/m_unit");
        $model = $this->input->post();
        $message = '';
        $this->form_validation->set_rules('id_jenis_kas', 'Jenis Transaksi', 'trim|required');
        $this->form_validation->set_rules('id_biaya_master', 'Biaya', 'trim|required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
        if (CheckArray($model, 'listdetail'))
            $listdetail = $model["listdetail"];
        else {
            $listdetail = [];
        }
        if (CheckArray($model, 'listreceipt'))
            $listreceipt = $model["listreceipt"];
        else {
            $listreceipt = [];
        }
        if (@$model['id_jenis_transaksi'] != 33) {
            $model['id_bank'] = null;
        }
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {

            $inputan = [];
            $this->load->model("biaya_master/m_biaya_master");
            $biaya = $this->m_biaya_master->GetOneBiaya_master($model["id_biaya_master"], "id_biaya_master", true);
            for ($i = 0; $i < count($listdetail); $i++) {
                $listdetail[$i]["no"] = $i + 1;
            }
            if ($biaya != null) {
                $inputan["id_jenis_kas"] = $model["id_jenis_kas"];
                $inputan["id_biaya_group"] = $biaya->id_biaya_group;
                $inputan["id_biaya_master"] = $model["id_biaya_master"];
                $inputan["jumlah"] = DefaultCurrencyDatabase($model["jumlah"]);
                $inputan["keterangan"] = $model["keterangan"];
                $inputan["nama_group"] = $biaya->nama_biaya_group;
                $inputan["id_unit_serial"] = ForeignKeyFromDb($model["id_unit_serial"]);
                $inputan["vin_number"] = "";
                $inputan["engine_no"] = "";
                $inputan["no_prospek"] = "";
                if(!CheckEmpty($inputan["id_unit_serial"]))
                {
                    $unit=$this->m_unit->GetOneUnitSerial($inputan["id_unit_serial"]);
                    if($unit)
                    {
                         $inputan["vin_number"]=$unit->vin_number;
                         $inputan["engine_no"]=$unit->engine_no;
                         $inputan["no_prospek"]=$unit->no_prospek;
                    }
                   
                }
                $inputan["no"] = count($listdetail) + 1;
                $inputan["nama_biaya"] = $biaya->nama_biaya_master;
                $inputan["debet"] = 0;
                $inputan["kredit"] = 0;
                $inputan["listreceipt"] = $listreceipt;
                if ($model["id_jenis_kas"] == "1") {
                    $inputan["debet"] = $inputan["jumlah"];
                } else {
                    $inputan["kredit"] = $inputan["jumlah"];
                }
                $inputan["action"] = "<a class='btn btn-xs btn-danger' onclick='deletebiaya(" . $inputan["no"] . ")'><i class='fa fa-close'></i>&nbsp;Delete</a>";


                array_push($listdetail, $inputan);
            }
            echo json_encode(array('st' => true, 'listdetail' => $listdetail));
        }
    }

    function biaya_check($val) {
        $model = $this->input->post();
        if ($model['id_jenis_kas'] == 2 && CheckEmpty($val)) {
            $this->form_validation->set_message('biaya_check', 'Silahkan pilih Jenis Biaya!');
            return FALSE;
        }

        return TRUE;
    }

    function getHakEdit() {
        $group = GetRowData('tic_group', ['id_group' => GetGroupId()]);
        return [
            'edit_jenis_transaksi' => $group['edit_jenis_transaksi'],
            'edit_jenis_biaya' => $group['edit_jenis_biaya'],
            'edit_tgl_transfer' => $group['edit_tgl_transfer'],
        ];
    }

    public function edit_kas($id = 0) {
        $hakEdit = $this->getHakEdit();
        if ($hakEdit['edit_jenis_transaksi'] || $hakEdit['edit_jenis_biaya'] || $hakEdit['edit_tgl_transfer']) {
            // berarti bisa ngedit
        } else {
            echo "<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button><i class='icon fa fa-ban'></i> Anda Tidak mempunya hak untuk edit data!</div>";
            die();
        }

        $row = $this->m_kas->GetOneKas($id);

        if ($row) {
//            dumperror($row);
            $this->load->model("jenis_pengeluaran/m_jenis_pengeluaran");
            $this->load->model("jenis_transaksi/m_jenis_transaksi");
            $this->load->model("jenis_biaya/m_jenis_biaya");

            $row = (array) $row;
            $row['id_jenis_pengeluaran'] = GetScalarData('tic_jenis_biaya', 'id_jenis_biaya', $row['id_jenis_biaya'], 'id_jenis_pengeluaran');
            $row['button'] = 'Update';
            $row['listjenispengeluaran'] = $this->m_jenis_pengeluaran->GetDropDownJenisPengeluaran();
            $row['listjenistransaksi'] = $this->m_jenis_transaksi->GetDropDownJenisTransaksi();
            $row['listjenisbiaya'] = $this->m_jenis_biaya->GetDropDownJenisBiaya($row['id_jenis_pengeluaran']);
            $shift = $this->shift();
            $row['listshift'] = $shift['listshift'];
            $row['terlihat'] = 'none';
            if ($row['id_jenis_transaksi'] > 1) {
                $row['terlihat'] = 'block';
            }
            $row['is_edit'] = true;
            if ($row['id_karyawan']) {
                $karyawan = GetRowData('tic_karyawan', ['id_karyawan' => $row['id_karyawan']]);
                if ($karyawan) {
                    $row['nic_karyawan'] = $karyawan['nic_karyawan'];
                    $row['nama_karyawan'] = $karyawan['nama_karyawan'];
                }
            }
            $row = $row + $hakEdit;
            $this->load->view('kas/v_kas_manipulate', $row);
        } else {
            SetMessageSession(0, "Kas cannot be found in database");
            redirect(site_url('kas'));
        }
    }

    function shift() {
        $data = [];
        $this->load->model("shift/m_shift");
        $data['listshift'] = $this->m_shift->GetDropDownShift();
        $listshift = $this->m_shift->GetFullDataShift();
        $id_shift = 3;
        foreach ($listshift as $shift) {

            if (time() > strtotime($shift->start_shift) && time() <= strtotime($shift->end_shift)) {

                $id_shift = $shift->id_shift;
                break;
            }
        }
        $data['id_shift'] = $id_shift;

        return $data;
    }

    public function LihatReceipt($id_kas) {
        $listreceipt = $this->m_kas->ListReceiptKas($id_kas);
        echo "<div class='modal-body text-center'>";
        for ($i = 0; $i < count($listreceipt); $i++) {
            echo "<img src='" . base_url() . "uploads/receipts/" . $listreceipt[$i]->receipt_name . "'/><br/>";
        }
        if (count($listreceipt) == 0) {
            echo "<h1>No Receipt in this transaction</h1>";
        }
        echo "</div>";
    }

    public function kas_batal() {
        $message = '';
        $this->form_validation->set_rules('id_kas_master', 'Kas', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_kas->KasBatal($model['id_kas_master']);
        }

        echo json_encode($result);
    }

    public function getUser() {
        $resp = ['st' => true];
        $id_pangkalan = $this->input->post('id_pangkalan');

        if (!$id_pangkalan) {
            $resp['st'] = false;
            $resp['msg'] = "Silahkan pilih pangkalan!";
        } else {
            $resp['list_agen'] = GetTableData('tic_user', 'id_user', 'nama_user', array('status' => 1, 'id_pangkalan' => $id_pangkalan), 'json');
        }

        echo json_encode($resp);
    }

    public function print_kas($id) {

        $kas = $this->m_kas->GetKasMaster($id);

        $data = [
            'kas' => $kas,
            'lokasi' => $kas[0]['nama_cabang'],
            'pembuat' => GetScalarData('tic_user', 'id_user', $kas[0]['created_by'], 'nama_user'),
            'pengedit' => ($kas[0]['updated_by']) ? GetScalarData('tic_user', 'id_user', $kas[0]['updated_by'], 'nama_user') : null,
            'pencetak' => GetScalarData('tic_user', 'id_user', GetUserId(), 'nama_user'),
            'jenis' => getJenisTransaksi($kas[0]['id_jenis_transaksi'])
        ];

        $this->load->view('v_kas_print', $data);
    }

    public function get_nominal() {
        $resp = ['st' => false];
        $curDate = $this->input->post('curDate');
        if ($curDate) {
            if (GetDefaultCabang()) {
                try {
                    $this->load->model("jenis_transaksi/m_jenis_transaksi");
                    $listjenis_transaksi = $this->m_jenis_transaksi->GetDropDownJenisTransaksi();
                    foreach ($listjenis_transaksi as $key => $jenistran) {
                        $resp['trans_' . $key . '_debit'] = $this->m_kas->getNominal(DefaultTanggalDatabase($curDate), 'debit', $key);
                        $resp['trans_' . $key . '_kredit'] = $this->m_kas->getNominal(DefaultTanggalDatabase($curDate), 'kredit', $key);

                        if (isAllowViewSaldo()) {
                            $saldo_awal = $this->m_kas->getSaldoAwal(DefaultTanggalDatabase($curDate), $key);
                            $resp['trans_' . $key . '_saldo_awal'] = $saldo_awal > 0 ? $saldo_awal : $this->m_kas->getSaldo(DefaultTanggalDatabase($curDate), 'awal', $key);
                            $resp['trans_' . $key . '_saldo_akhir'] = $this->m_kas->getSaldo(DefaultTanggalDatabase($curDate), 'akhir', $key);
                        }
                    }

                    $resp['st'] = true;
                } catch (Exception $ex) {
                    $resp['msg'] = $ex->getMessage();
                }
            } else {
                $resp['msg'] = "Cabang belum dipilih!";
            }
        } else {
            $resp['msg'] = "Tanggal belum dipilih!";
        }

        echo json_encode($resp);
    }

    function set_check($id_kas) {

        $model = GetRowData('tic_kas', ['id_kas' => $id_kas], [], [], [], true);

        $model['id_kas'] = $id_kas;
        $model['is_checked'] = 1;
        $model['id_check'] = GetUserId();

        $resp = $this->m_kas->KasManipulate($model);

        echo json_encode($resp);
    }

    public function edit_kategori($id_kas) {
        $this->load->model('jenis_biaya/m_jenis_biaya');
        $this->load->model("jenis_pengeluaran/m_jenis_pengeluaran");
        $data = $this->m_kas->detailKas($id_kas);
        $data['list_jenis_pengeluaran'] = $this->m_jenis_pengeluaran->GetDropDownJenisPengeluaran($data['id_cabang']);
        $data['list_jenis_biaya'] = $this->m_jenis_biaya->GetDropDownJenisBiaya($data['id_jenis_pengeluaran'], $data['id_cabang']);
        echo $this->load->view('kas/v_kategori_edit', $data, true);
    }

    public function update_kategori() {
        $resp = [];
        $id_kas = $this->input->post('id_kas');
        $id_jenis_biaya = $this->input->post('id_jenis_biaya');
        $keterangan = $this->input->post('keterangan');
        $resp['st'] = $this->m_kas->updateKas($id_kas, ['id_jenis_biaya' => $id_jenis_biaya, 'keterangan' => $keterangan]);
        if ($resp['st']) {
            $resp['msg'] = 'Update berhasil';
        } else {
            $resp['msg'] = 'Update gagal!';
        }

        echo json_encode($resp);
    }

}

/* End of file Kas.php */