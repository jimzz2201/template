<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kas extends CI_Model {

    public $table = '#_kas_master';
    public $id = 'id_kas_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;
    private $_prefix = "AK";

    function __construct() {
        parent::__construct();
    }

    function ListReceiptKas($id_kas) {
        $this->db->from("#_kas_receipt");
        $this->db->where(array("id_kas" => $id_kas));
        return $this->db->get()->result();
    }

    // datatables
    function GetDataKas($params = array()) {
        
        $listidkasmaster=[];
        
        if((!CheckEmpty(@$params['keyword'])&&@$params['jenis_keyword']!="no_kas_master")||!CheckEmpty(@$params['id_customer']))
        {
            if (@$params['jenis_keyword'] == "no_prospek") {
                $pencariankeyword = "#_prospek.no_prospek";
            } else if (@$params['jenis_keyword'] == "vin_number" || @$params['jenis_keyword'] == "engine_no") {
                if (@$params['jenis_keyword'] == "no_prospek") {
                    $pencariankeyword = "#_prospek.no_prospek";
                } else {
                    $pencariankeyword = @$params['jenis_keyword'];
                }
            }
            
            $this->db->from("#_kas_detail");
            $this->db->join("#_unit_serial","#_unit_serial.id_unit_serial=#_kas_detail.id_unit_serial");
            $this->db->join("#_buka_jual","#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual");
            $this->db->join("#_customer","#_customer.id_customer=#_buka_jual.id_customer");
            $this->db->join("#_prospek","#_prospek.id_prospek=#_buka_jual.id_prospek");
            $this->db->select("#_kas_detail.id_kas_master");
            $this->db->distinct();
            if(!CheckEmpty(@$params['keyword']))
            {
                 $this->db->where($pencariankeyword,@$params['keyword']);
            }
            else
            {
                $this->db->where("#_buka_jual.id_customer",$params['id_customer']);
            }
            $listid=$this->db->get()->result_array();
            $listid= array_column($listid, "id_kas_master");
            
            if(count($listid)==0)
            {
                $listid=[-1];
            }
            
        }
        



        $this->load->library('datatables');
        $cols = '#_kas_master.tanggal_kas  ,#_kas_master.id_jenis_transaksi, no_kas_master, nama_cabang,#_kas_master.status,
                #_kas_master.id_kas_master,payment_voucher, #_kas_master.deleted_date, payment_voucher, jumlah_total , #_user.username as pembuat,pembayaran.nama_gl_account as glheader,
                ifnull(group_concat(concat(\'<li>\',if(id_jenis_kas=1, \'Pemasukan \',\'Pengeluaran \'),vin_number,\' &nbsp; \',engine_no,\' &nbsp; \',no_prospek,\' &nbsp; \',CAST(FORMAT(#_kas_detail.jumlah,2) AS CHAR)," ",nama_biaya_master,"<br/>(",#_kas_detail.keterangan,")") SEPARATOR \'</li>\'),\'\') as details';
        $this->datatables->select($cols);
        $this->datatables->from('#_kas_master');
        $this->datatables->join('#_gl_account pembayaran', 'pembayaran.id_gl_account = #_kas_master.id_jenis_transaksi', 'left');
        //add this line for join
        $this->datatables->join('#_kas_detail', '#_kas_detail.id_kas_master = #_kas_master.id_kas_master', 'left');
        $this->datatables->join('#_biaya_master', '#_biaya_master.id_biaya_master = #_kas_detail.id_biaya_master', 'left');
        $this->datatables->join('#_user', '#_kas_master.created_by = #_user.id_user', 'left');
        $this->datatables->join('#_cabang', '#_kas_master.id_cabang = #_cabang.id_cabang', 'left');
        $this->datatables->join('#_unit_serial', '#_unit_serial.id_unit_serial = #_kas_detail.id_unit_serial', 'left');
        $this->datatables->join('#_buka_jual', '#_buka_jual.id_buka_jual = #_unit_serial.id_buka_jual', 'left');
        $this->datatables->join('#_prospek', '#_prospek.id_prospek = #_buka_jual.id_prospek', 'left');
        $extra = [];
        
    
        if (!CheckEmpty(@$params['id_cabang'])) {
            $this->datatables->where('#_kas_master.id_cabang', @$params['id_cabang'], false);
            unset($params['id_cabang']);
        }

        if (!CheckEmpty(@$params['keyword'])&&@$params['jenis_keyword']=="no_kas_master") {
            $pencariankeyword = "no_kas_master";
            $where[$pencariankeyword] = trim($params['keyword']);
        } else {
            $tanggal = "#_kas_master.tanggal_kas";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_kas_master.tanggal_kas)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_kas_master.tanggal_kas)";
            } 
            $this->db->order_by($tanggal, "desc");


            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                $where['#_kas_master.status'] = $params['status'];
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_buka_jual.id_customer'] = $params['id_customer'];
            }
        }
        
        
        if(count($listid)>0)
        {
            $this->db->where_in("#_kas_master.id_kas_master",$listid);
        }
        
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }


        $isedit = true;

        $isprint = true;
        $straction = '';

        if ($isprint) {
       //     $straction .= anchor("", '<i class="fa fa-print"></i>', array('class' => 'btn btn-info btn-xs', "onclick" => "lihatreceipt($1);return false;"));
        }
        if ($isedit) {
            $straction .= anchor("", '<i class="fa fa-trash"></i>', array('class' => 'btn btn-danger btn-xs', "onclick" => "BatalKas($1);return false;"));
        }
        $this->datatables->group_by("#_kas_master.id_kas_master");
        $this->datatables->add_column('action', $straction, 'id_kas_master');

        return $this->datatables->generate();
    }

    function GetDataKasMaster($params = array()) {
        $hak = GetRowData('#_group', ['id_group' => GetGroupId()]);
        $this->load->library('datatables');
        $cols = 'id_kas_master, deleted_date, payment_voucher,
                kode_kas_master, jumlah_total, is_approved, nama_jenis_transaksi,
                nama_karyawan,#_kas_master.status,
                #_kas_master.tanggal_kas, nama_project, nama_user, #_kas_master.created_date';
        $this->datatables->select($cols);
        $this->datatables->from('#_kas_master');

        //add this line for join
        $this->datatables->join('#_user', '#_kas_master.created_by = #_user.id_user', 'left');
        $this->datatables->join('#_project', '#_project.id_project = #_kas_master.id_project', 'left');

        $this->datatables->join('#_karyawan', '#_karyawan.id_karyawan = #_kas_master.id_karyawan', 'left');

        $this->datatables->join('#_jenis_transaksi', '#_jenis_transaksi.id_jenis_transaksi = #_kas_master.id_jenis_transaksi', 'left');

        $this->datatables->where('#_kas_master.id_cabang', GetDefaultCabang(), false);

        $search = array_filter($params);

        $this->datatables->where("#_kas_master.tanggal_kas", DefaultTanggalDatabase($search['tanggal']));

        $isbatal = $hak['hak_batal'];
        $isedit = false;
        if ($hak['edit_jenis_biaya'] || $hak['edit_jenis_transaksi'] || $hak['edit_tgl_transfer']) {
            $isedit = true;
        }
        $isprint = true;
        $straction = '';

        if ($isprint) {
            $straction .= anchor("", '<i class="fa fa-print"></i>', array('class' => 'btn btn-info btn-xs', "onclick" => "printKas($1);return false;"));
        }
        if ($isbatal) {
            $straction .= anchor("", '<i class="fa fa-trash"></i>', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletekas($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_kas_master');
        return $this->datatables->generate();
    }

    // get all
    function GetOneKas($keyword, $type = 'id_kas_master') {
        $this->db->where($type, $keyword);
        $kas = $this->db->get($this->table)->row();
        return $kas;
    }

    function GetDataDetailKas($id_kas_master = 0) {

        $this->db->from("#_kas");
        $this->db->where(array("id_kas_master" => $id_kas_master));
        $this->db->select("id_kas,#_kas.tanggal_kas as tanggal_transaksi,nama_jenis_biaya,nama_jenis_pengeluaran,jumlah,id_jenis_kas");
        $this->db->join("#_jenis_biaya", "#_jenis_biaya.id_jenis_biaya=#_kas.id_jenis_biaya");
        $this->db->join("#_jenis_pengeluaran", "#_jenis_pengeluaran.id_jenis_pengeluaran=#_jenis_biaya.id_jenis_pengeluaran");
        $listresult = $this->db->get()->result();
        return $listresult;
    }

    function KasManipulate($model) {
        try {
            
            $this->db->trans_start();
            $listdetail = [];
            if (CheckArray($model, 'listdetail'))
                $listdetail = $model["listdetail"];

            $kasmaster = array();
            $kasmaster["no_kas_master"] = $this->getKodeKasMaster(@$model['id_cabang']);
            $kasmaster['tanggal_kas'] = DefaultTanggalDatabase($model['tanggal_kas']);
            $kasmaster['id_jenis_transaksi'] = ForeignKeyFromDb($model['id_jenis_transaksi']);
            $kasmaster['id_cabang'] = ForeignKeyFromDb(@$model['id_cabang']);
            $kasmaster['id_bank'] = ForeignKeyFromDb(@$model['id_bank']);
            $this->load->model("gl/m_gl");
            $this->load->model("bank/m_bank");

            $bank = $this->m_bank->GetOneBank($kasmaster['id_bank']);

            $gl = $this->m_gl->GetOneGl($kasmaster['id_jenis_transaksi']);

            $jumlahtotal = 0;
            $keterangan="";
            for ($i = 0; $i < count($listdetail); $i++) {
                 if ($listdetail[$i]["id_jenis_kas"] == 1) {
                    $jumlahtotal += DefaultCurrencyDatabase($listdetail[$i]['jumlah']);
                    if(!CheckEmpty(@$listdetail[$i]['keterangan']))
                    $keterangan.="".$listdetail[$i]['keterangan'].' , ';
              
                } else {
                    $jumlahtotal -= DefaultCurrencyDatabase($listdetail[$i]['jumlah']);
                    if(!CheckEmpty(@$listdetail[$i]['keterangan']))
                    $keterangan.="".$listdetail[$i]['keterangan'].' , ';
                    
                }
            }

            $kasmaster['created_date'] = GetDateNow();
            $kasmaster['jumlah_total'] = DefaultCurrencyDatabase($jumlahtotal);
            $kasmaster['created_by'] = ForeignKeyFromDb(GetUserId());
            $kasmaster['payment_voucher'] = $model["payment_voucher"];
            $kasmaster['deleted_date'] = null;
            $kasmaster['status'] = 1;
            $this->db->insert("#_kas_master", $kasmaster);
            $id_kas_master = $this->db->insert_id();
            $glmasukkanheader = [];
            $glmasukkanheader['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal_kas']);
            $glmasukkanheader['keterangan'] = 'Aliran kas ' . @$gl->nama_gl_account . " " . $kasmaster["no_kas_master"] .' untuk '.$keterangan;
            $glmasukkanheader['dokumen'] = $kasmaster["no_kas_master"];
            $glmasukkanheader['id_subject'] = ForeignKeyFromDb($kasmaster['id_bank']);
            $glmasukkanheader['subject_name'] = @$bank->kode_bank;
            $glmasukkanheader['debet'] = $jumlahtotal > 0 ? $jumlahtotal : 0;
            $glmasukkanheader['id_gl_account'] = ForeignKeyFromDb($model['id_jenis_transaksi']);
            $glmasukkanheader['kredit'] = $jumlahtotal > 0 ? 0 : $jumlahtotal * -1;
            $glmasukkanheader['created_by'] = GetUserId();
            $glmasukkanheader['created_date'] = GetDateNow();
            $glmasukkanheader['id_cabang'] = ForeignKeyFromDb(@$model['id_cabang']);
            $glmasukkanheader['id_detail'] = $id_kas_master;
            if ($jumlahtotal > 0) {
                $this->db->insert("#_buku_besar", $glmasukkanheader);
            }


            $this->load->model("biaya_master/m_biaya_master");
            for ($i = 0; $i < count($listdetail); $i++) {
                if (CheckArray($listdetail[$i], 'listreceipt'))
                    $listreceipt = $listdetail[$i]["listreceipt"];
                else {
                    $listreceipt = [];
                }
                $kas['id_kas_master'] = $id_kas_master;
                $kas['id_jenis_kas'] = ForeignKeyFromDb($listdetail[$i]['id_jenis_kas']);
                $kas['id_biaya_master'] = ForeignKeyFromDb($listdetail[$i]['id_biaya_master']);
                $kas['jumlah'] = DefaultCurrencyDatabase($listdetail[$i]['jumlah']);
                $kas['kode_kas'] = $this->getKodeKas($kasmaster["no_kas_master"]);
                $kas['created_date'] = GetDateNow();
                $kas['created_by'] = ForeignKeyFromDb(GetUserId());
                $kas['id_unit_serial'] = ForeignKeyFromDb($listdetail[$i]['id_unit_serial']);
                $kas['status'] = 1;
                $kas['keterangan'] = $listdetail[$i]['keterangan'];
                $kas['deleted_date'] = null;
//            $model['biaya_bank'] = DefaultCurrencyDatabase($model['biaya_bank']);
                $this->db->insert("#_kas_detail", $kas);
                $id_kas = $this->db->insert_id();
                $biaya = $this->m_biaya_master->GetOneBiaya_master($kas['id_biaya_master']);

                if ($biaya) {
                    $arrgl=explode(",",$biaya->id_gl_debet);
                    if(array_search($model['id_jenis_transaksi'], $arrgl)!==false){
                        $glid=$biaya->id_gl_kredit;
                    }
                    else
                    {
                        $glid=$biaya->id_gl_debet;
                    }
                    
                    IF($glid==33)
                    {
                        IF($bank)
                        {
                            $glid=$bank->id_gl_account;
                        }
                        
                    }
                    
                    
                    $glmasukkandetail['tanggal_buku'] = DefaultTanggalDatabase($model['tanggal_kas']);
                    $glmasukkandetail['keterangan'] = 'Aliran kas ' . @$gl->nama_gl_account . ' ' . ($kas['id_jenis_kas'] == 2 ? "Pengeluaran" : "Pemasukan") . " " . $listdetail[$i]['keterangan'];
                    $glmasukkandetail['dokumen'] = $kasmaster["no_kas_master"];
                    $glmasukkandetail['id_subject'] = null;
                    $glmasukkandetail['id_cabang'] = ForeignKeyFromDb(@$model['id_cabang']);
                    $glmasukkandetail['id_gl_account'] = $glid;
                    $glmasukkandetail['kredit'] = $kas['id_jenis_kas'] == "1" ? DefaultCurrencyDatabase($listdetail[$i]['jumlah']) : 0;
                    $glmasukkandetail['debet'] = $kas['id_jenis_kas'] == "2" ? DefaultCurrencyDatabase($listdetail[$i]['jumlah']) : 0;
                    $glmasukkandetail['created_by'] = GetUserId();
                    $glmasukkandetail['created_date'] = GetDateNow();
                    $glmasukkandetail['id_detail'] = $id_kas;
                    $this->db->insert("#_buku_besar", $glmasukkandetail);
                }

                for ($j = 0; $j < count($listreceipt); $j++) {
                    $this->db->insert("#_kas_receipt", array("id_kas_detail" => $id_kas, "receipt_name" => $listreceipt[$j]["receiptfile"], "created_date" => GetDateNow(), "created_by" => GetUserId()));
                }
            }
            if ($jumlahtotal <= 0) {
                $this->db->insert("#_buku_besar", $glmasukkanheader);
            }



            $message = "";
            $st = false;

            if (CheckEmpty(@$model['id_kas_master'])) {
                SetMessageSession($st, "Kas dengan code " . $kasmaster["no_kas_master"] . " berhasil dimasukkan ke dalam database");
                $message = "Transaksi berhasil diinput";
                $this->session->set_userdata(array("id_kas_master" => $id_kas_master));
                $st = true;
                $id = $id_kas_master;
            } else {
                $kas['is_checked'] = ForeignKeyFromDb($model['is_checked']);
                $kas['id_check'] = ForeignKeyFromDb($model['id_check']);
                $kas['updated_date'] = GetDateNow();
                $kas['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $kas, array("id_kas" => $model['id_kas']));

                $message = "Transaksi berhasil dibatalkan";
                $st = true;
                $id = $id_kas_master;
            }

            $this->session->set_userdata("id_kas_master", $id);
            $this->session->set_userdata("no_kas_master", $kasmaster["no_kas_master"]);
            $this->db->trans_commit();
            return array("st" => $st, "msg" => $message, "id" => $id);
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function KasBatal($id_kas) {
        $resp = ['st' => true];
        $kas = $this->GetOneKas($id_kas);
        if ($kas) {
            try {

                $model['status'] = 2;
                $model['deleted_date'] = GetDateNow();
                $model['deleted_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update("#_kas_master", $model, array('id_kas_master' => $id_kas));
                $this->db->set('keterangan', 'CONCAT(\'[Batal]\',keterangan)', FALSE);
                $this->db->update("#_buku_besar",array("debet"=>0,"kredit"=>0,"updated_date"=> GetDateNow(),"updated_by"=> GetUserId()),array("dokumen"=>$kas->no_kas_master));
                $resp['msg'] = "Transaksi Berhasil dihapus";
            } catch (Exception $ex) {
                $resp['st'] = false;
                $resp['msg'] = $ex;
            }
        } else {
            $resp['st'] = false;
            $resp['msg'] = "Data Kas sudah dibatalkan sebelumnya";
        }


        return $resp;
    }

    function getKodeKas($kasmaster) {


        $this->db->select("(CAST(SUBSTR(kode_kas, " . (int) -4 . ", 4) AS UNSIGNED) + 1) AS next", FALSE);
        $this->db->from("#_kas_detail");
        $this->db->where("kode_kas LIKE '" . $kasmaster . "%'");
        $this->db->order_by($this->id, 'DESC');
        $this->db->limit(1);

        $kode = $this->db->get()->row();
        $next = "0001";
        if ($kode) {
            $next = str_pad($kode->next, 4, '0', STR_PAD_LEFT);
        }

        return $kasmaster . $next;
    }

    function getKodeKasMaster($id_cabang) {
        $date = date('ym');
        $kodeCabang = GetScalarData('#_cabang', 'id_cabang', $id_cabang, "kode_cabang");
        $kode_kas = $this->_prefix . str_pad($id_cabang, 4, '0', STR_PAD_LEFT) . $date;
        if ($kodeCabang) {
            $kode_kas = $this->_prefix . $kodeCabang . $date;
        }

        $this->db->select("(CAST(SUBSTR(no_kas_master, " . (int) -4 . ", 4) AS UNSIGNED) + 1) AS next", FALSE);
        $this->db->from("#_kas_master");
        $this->db->where("no_kas_master LIKE '" . $kode_kas . "%'");
        $this->db->order_by("id_kas_master", 'DESC');
        $this->db->limit(1);

        $kode = $this->db->get()->row();
        $next = "0001";

        if ($kode) {
            $next = str_pad($kode->next, 4, '0', STR_PAD_LEFT);
        }


        return $kode_kas . $next;
    }

    function getNominal($tgl, $tipe, $id_jenis_transaksi) {
        $this->db->select("SUM(jumlah) AS nominal");
        $this->db->from($this->table);
        $this->db->join("#_kas_master", "#_kas.id_kas_master=#_kas_master.id_kas_master", "left");
        $this->db->where('#_kas_master.id_jenis_transaksi', $id_jenis_transaksi, false);
        $this->db->where('#_kas_master.id_cabang', GetDefaultCabang(), false);
        $this->db->where('#_kas_master.tanggal_kas', $tgl);
        switch ($tipe) {
            case 'debit' :
                $this->db->where('id_jenis_kas', 1, false);
                $this->db->where('id_jenis_biaya <>', 203, false);
                break;
            case 'kredit' : $this->db->where('id_jenis_kas', 2, false);
                break;
        }
        $this->db->where('#_kas_master.deleted_date IS NULL');

        $result = $this->db->get()->row_array();

        return (int) $result['nominal'];
    }

    function getSaldo($tgl, $tipe, $id_jenis_transaksi) {
        $this->db->select("(SUM(IF(id_jenis_kas = 1, jumlah, 0)) - SUM(IF(id_jenis_kas = 2, jumlah, 0))) AS nominal");
        $this->db->from($this->table);
        $this->db->join("#_kas_master", "#_kas.id_kas_master=#_kas_master.id_kas_master", "left");
        $this->db->where('#_kas_master.id_jenis_transaksi', $id_jenis_transaksi, false);
        $this->db->where('#_kas_master.id_cabang', GetDefaultCabang(), false);
        switch ($tipe) {
            case 'awal' :
                $this->db->where('#_kas_master.tanggal_kas <', $tgl);
//                $this->db->where('#_kas.id_jenis_biaya', 203, false);
                break;
            case 'akhir' : $this->db->where('#_kas_master.tanggal_kas <=', $tgl);
                break;
        }
        $this->db->where('#_kas_master.deleted_date IS NULL');

        $result = $this->db->get()->row_array();

        return (int) $result['nominal'];
    }

    function getSaldoAwal($tgl, $id_jenis_transaksi) {
        $this->db->select("(SUM(IF(id_jenis_kas = 1, jumlah, 0)) - SUM(IF(id_jenis_kas = 2, jumlah, 0))) AS nominal");
        $this->db->from($this->table);
        $this->db->join("#_kas_master", "#_kas.id_kas_master=#_kas_master.id_kas_master", "left");
        $this->db->where('#_kas_master.id_jenis_transaksi', $id_jenis_transaksi, false);
        $this->db->where('#_kas_master.id_cabang', GetDefaultCabang(), false);
        $this->db->where('#_kas_master.tanggal_kas =', $tgl);
        $this->db->where('#_kas.id_jenis_biaya', 203, false);
        $this->db->where('#_kas_master.deleted_date IS NULL');

        $result = $this->db->get()->row_array();

        return (int) $result['nominal'];
    }

    function detailKas($id_kas) {
        $select = '#_kas.*, kode_kas_master, payment_voucher, id_jenis_transaksi, id_jenis_pengeluaran, #_kas_master.id_cabang';
        $this->db->select($select);
        $this->db->from($this->table);
        $this->db->join('#_kas_master', '#_kas_master.id_kas_master = #_kas.id_kas_master');
        $this->db->join('#_jenis_biaya', '#_jenis_biaya.id_jenis_biaya = #_kas.id_jenis_biaya');
        $this->db->where('id_kas', (int) $id_kas, false);

        return $this->db->get()->row_array();
    }

    function updateKas($id, $model) {
        $this->db->where('id_kas', $id, false);
        return $this->db->update('#_kas', $model);
    }

}
