<div class="modal-header">
    <?php echo @$title; ?>
</div>
<div class="modal-body">
    <form id="frm_jadwal_kunjungan" class="form-horizontal form-groups-bordered validate" method="post" autocomplete="off">
        <input type="hidden" name="id_jadwal_kunjungan" id="id_jadwal_kunjungan" value="<?php echo @$id_jadwal_kunjungan; ?>" />
        <input type="hidden" name="id_sales" id="id_sales" value="<?php echo @$id_sales; ?>" />
        <div class="form-group">
            <?= form_label('Tanggal', "txt_tanggal", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
                <?= form_input(array('type' => 'text', 'name' => 'tanggal', 'class' => 'form-control datepicker', 'id' => 'tanggal', 'value' => DefaultDatePicker(@$date), 'placeholder' => 'Tanggal')); ?>
            </div>
        </div>
        <div class="form-group" style="<?php echo $button == 'Add' ? 'display:none' : '' ?>">
            <?= form_label('Waktu Mulai', "txt_start", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'name' => 'start_time', 'class' => 'form-control datetimepicker', 'id' => 'start_time', 'value' => @$start_time, 'placeholder' => 'Start Time')); ?>
            </div>
            <?= form_label('Waktu Selesai', "txt_end", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_input(array('type' => 'text', 'name' => 'end_time', 'class' => 'form-control datetimepicker', 'id' => 'end_time', 'value' => @$end_time, 'placeholder' => 'End Time')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Customer', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array('type' => 'text', 'name' => 'id_customer', 'class' => 'form-control', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown($list_customer, "json", "Customer"), @$id_customer); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Plafon', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-2">
                <?= form_input(array('type' => 'text', 'name' => 'plafond', 'class' => 'form-control', 'id' => 'plafond', 'value' => DefaultCurrency(@$plafond), 'placeholder' => 'Plafon', 'disabled' => 'disabled')); ?>
            </div>
            <?= form_label('Piutang', "txt_status", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-3">
                <?= form_input(array('type' => 'text', 'name' => 'piutang', 'class' => 'form-control', 'id' => 'piutang', 'value' => DefaultCurrency(@$piutang), 'placeholder' => 'Piutang', 'disabled' => 'disabled')); ?>
            </div>
            <?= form_label('Sisa', "txt_status", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-3">
                <?= form_input(array('type' => 'text', 'name' => 'sisa', 'class' => 'form-control', 'id' => 'sisa', 'value' => DefaultCurrency(@$sisa), 'placeholder' => 'Sisa Plafon', 'disabled' => 'disabled')); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Catatan', "txt_notes", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
                <?= form_textarea(array('type' => 'text', 'rows' => '5', 'cols' => '10', 'class' => 'form-control', 'name' => 'notes', 'id' => 'notes', 'placeholder' => 'Catatan', 'value' => @$notes)); ?>
            </div>
        </div>
        <div class="form-group" style="<?php echo $button == 'Add' ? 'display:none' : '' ?>">
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
                <?= form_dropdown(array('type' => 'text', 'name' => 'status', 'class' => 'form-control', 'id' => 'dd_status', 'placeholder' => 'Status'), DefaultEmptyDropdown(@$list_status, "json", "Status"), @$status); ?>
            </div>
        </div>
        <div class="form-group" style="<?php echo $button == 'Add' ? 'display:none' : '' ?>">
            <?= form_label('Laporan Keterangan', "txt_description", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-8">
                <?= form_textarea(array('type' => 'text', 'rows' => '5', 'cols' => '10', 'class' => 'form-control', 'name' => 'results', 'id' => 'results', 'placeholder' => 'Laporan Keterangan', 'value' => @$results)); ?>
            </div>
        </div>
        <div class="form-group" style="<?php echo $button == 'Add' ? 'display:none' : '' ?>">
            <?= form_label('Gambar', "txt_description", array("class" => 'col-sm-2 control-label')); ?>
        </div>
        <div class="form-group" style="<?php echo $button == 'Add' ? 'display:none' : '' ?>">
            <?php
            foreach (@$list_images as $img) {
                $path = base_url() . 'upload/upload-foto/' . $img['filename'];
                echo "<div class='col-sm-3' style='position:relative'><img src='" . $path . "'>
						<span class='remove glyphicon glyphicon-remove' style='position: absolute; top: 4px; right: 5px; cursor:pointer' id='" . $img['id'] . "'></span>
					 </div>";
            }
            ?>
        </div>
        <div class="form-group" style="<?php echo $button == 'Add' ? 'display:none' : '' ?>">
            <div class="dropzone">
                <div class="dz-message">
                    <h3> Klik atau Drop gambar disini</h3>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" ><?php echo $button == 'Update' ? 'Update' : 'Simpan' ?></button>
        </div>

    </form>
</div>
<script>
    Dropzone.autoDiscover = false;

    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('index.php/jadwal_kunjungan/upload_foto') ?>",
        maxFilesize: 2,
        method: "post",
        acceptedFiles: "image/*",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
    });
    //Event ketika Memulai mengupload
    foto_upload.on("sending", function (a, b, c) {
        a.token = Math.random();
        c.append("token_foto", a.token);
        c.append("id_jadwal", $("#id_jadwal_kunjungan").val());
    });

    //Event ketika foto dihapus
    foto_upload.on("removedfile", function (a) {
        var token = a.token;
        $.ajax({
            type: "post",
            data: {token: token},
            url: "<?php echo base_url('index.php/jadwal_kunjungan/remove_foto') ?>",
            cache: false,
            dataType: 'json',
            success: function (z) {
                if (z.st) {
                    console.log('berhasil dihapus');
                }
            },
            error: function () {
                console.log("Error");

            }
        });
    });
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $("select").select2();
        
        $('.datetimepicker').timepicker({
            template: 'dropdown'
        });
    })

    $("#dd_id_customer").change(function () {
        var id_cust = $(this).val();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/customer/get_one_customer',
            data: 'id_customer=' + id_cust,
            dataType: 'json',
            success: function (data) {
                $("#plafond").val(Comma(data.obj.plafond));
                $("#piutang").val(Comma(data.obj.piutang));
                $("#sisa").val(Comma(data.obj.plafond - data.obj.piutang));
            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        })
    })


    $(".remove").click(function () {
        var id = $(this).attr('id');
        $.ajax({
            type: "post",
            data: 'id=' + id,
            url: "<?php echo base_url('index.php/jadwal_kunjungan/deleteFotoJadwalKunjungan') ?>",
            cache: false,
            dataType: 'json',
            success: function (z) {
                if (z.st) {
                    console.log('berhasil dihapus');
                }
            },
            error: function () {
                console.log("Error");

            }
        });
    });

    $("#frm_jadwal_kunjungan").submit(function () {
        // console.log($(this).serialize());
        // return false;
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/jadwal_kunjungan/jadwal_kunjungan_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                    refresh($("#dd_sales").val());
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<script type="text/javascript" src="http://dgmi.local/assets/plugins/timepicker/bootstrap-timepicker.min.js" ></script>
<style>
    .control-label {
        text-align: left !important;
    }
    .bootstrap-timepicker-widget.dropdown-menu.open {
        display: inline-block;
        z-index: 99999 !important;
    }
    .glyphicon {
        font-size: 20px;
        color: red;
    }
</style>