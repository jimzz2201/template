<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jadwal_kunjungan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_jadwal_kunjungan');
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("customer/m_customer");
        $this->load->database();
		$this->load->helper(array('url','file'));
    }

    public function index()
    {
        $javascript = array();
        $css = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/plugins/select2/select2.js";
        $javascript[] = "assets/plugins/timepicker/bootstrap-timepicker.min.js";
        $javascript[] = "assets/plugins/fullcalendar/core/main.js";
        $javascript[] = "assets/plugins/fullcalendar/daygrid/main.js";
        $javascript[] = "assets/plugins/fullcalendar/interaction/main.js";
        $javascript[] = "assets/plugins/fullcalendar/core/locales/id.js";
        $javascript[] = "assets/plugins/fullcalendar/bootstrap/main.min.js";
        $javascript[] = "assets/plugins/dropzone/dropzone.min.js";
        $css[] = "assets/plugins/fullcalendar/core/main.css";
        $css[] = "assets/plugins/fullcalendar/daygrid/main.css";
        $css[] = "assets/plugins/fullcalendar/bootstrap/main.min.css";
        $css[] = "assets/plugins/select2/select2.css";
        $css[] = "assets/plugins/timepicker/bootstrap-timepicker.min.css";
        $css[] = "assets/plugins/dropzone/dropzone.min.css";
        $module = "K075";
        $header = "K074";
        $title = "Jadwal Kunjungan";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
	    CekModule($module);
	    LoadTemplate($model, "jadwal_kunjungan/v_jadwal_kunjungan_index", $javascript, $css);
        
    } 
    public function get_one_jadwal_kunjungan() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_jadwal_kunjungan', 'Jadwal Kunjungan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_jadwal_kunjungan->GetOneJadwal_kunjungan($model["id_jadwal_kunjungan"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Jadwal Kunjungan tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function get_jadwal() {
        $start = $this->input->get("start");
        $end = $this->input->get("end");
        $id_sales = $this->input->get("id_sales");

        $startdt = new DateTime('now');
        $startdt->setTimestamp(strtotime($start));
        $start_format = $startdt->format('Y-m-d H:i:s');

        $enddt = new DateTime('now');
        $enddt->setTimestamp(strtotime($end));
        $end_format = $enddt->format('Y-m-d H:i:s');

        $listJadwal = $this->m_jadwal_kunjungan->GetListJadwal($start_format, $end_format, $id_sales);

        $data_events = array();

        foreach($listJadwal as $data) {

            $data_events[] = array(
                "id" => $data->id_jadwal_kunjungan,
                "title" => $data->nama_customer,
                "description" => $data->notes,
                "start" => $data->tanggal,
                "color" => $data->statusJk == 1 ? 'green' : ($data->statusJk == 2 ? 'red' : '')
            );
        }
        echo json_encode($data_events);
        

    }
    
    public function get_jadwal_api() {
        $id_sales = $this->input->get("id_sales");
        $year = $this->input->get("year");
        $month = $this->input->get("month");
        $id_pelanggan = $this->input->get("id_pelanggan");
        $status = $this->input->get("status");
        // print_r($status);exit();
        $listTanggal = $this->m_jadwal_kunjungan->GetAvailDateApi($year, $month, $id_sales, $status);
        $listJadwal = array();
        foreach($listTanggal as $tg) {
            $res = $this->m_jadwal_kunjungan->GetListJadwalApi($tg->tanggal, $id_sales , $status);
            if($res)
                $listJadwal[$tg->tanggal] = $res;
        }
        echo json_encode($listJadwal);
    }

    public function get_jadwal_api_new() {
        $id_sales = $this->input->get("id_sales");
        $year = $this->input->get("year");
        $month = $this->input->get("month");
        $id_pelanggan = $this->input->get("id_pelanggan");
        $status = $this->input->get("status");
        $listTanggal = $this->m_jadwal_kunjungan->GetAvailDateApi($year, $month, $id_sales, $status);
        $res = array();
        if(count($listTanggal) > 0) {
            $tgl = array();
            foreach($listTanggal as $tg) {
                array_push($tgl, $tg->tanggal);
            }
            $res = $this->m_jadwal_kunjungan->GetListJadwalApi2($tgl, $id_sales , $status);
        }
        echo json_encode($res);
    }

    public function detil($id_jadwal) {
        $data = array();
        $data = $this->m_jadwal_kunjungan->GetDetil($id_jadwal);
        $data['pelanggan'] = $this->m_jadwal_kunjungan->GetPelanggan($data['id_customer']);
        $data['pelanggan']->latitude = (float) $data['pelanggan']->latitude;
        $data['pelanggan']->longitude = (float) $data['pelanggan']->longitude;
        $data['foto'] = $this->m_jadwal_kunjungan->GetFoto($id_jadwal);
        $data['lampiran'] = $this->m_jadwal_kunjungan->GetLampiran($id_jadwal);
        $data['editable'] = true;
        
        echo json_encode($data);
    }
    
    public function getdatajadwal_kunjungan() {
        header('Content-Type: application/json');
        echo $this->m_jadwal_kunjungan->GetDatajadwal_kunjungan();
    }

    public function customer_list() {
        $tanggal = $this->input->get('date');
        $id_pegawai = $this->input->get('id_pegawai');
        $q = $this->input->get('q');
        $listCust = $this->m_jadwal_kunjungan->availableCustomer($tanggal, $q, $id_pegawai);//die(var_dump($listCust));
        // $list_cust = array();
        // foreach($listCust as $dt) {
        //     $list_cust[] = array('id' => $dt->id_customer, 'text' => $dt->nama_customer);
        // }
        // $row['list_customer'] = $list_cust;

        echo json_encode($listCust);
    }
    
    public function create_jadwal_kunjungan() 
    {
        $tanggal = $this->input->get('date');
        $id_sales = $this->input->get('id_sales');
        $row=['button'=>'Add'];
        $javascript = array();
        $css = array();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
	    $module = "K075";
        $header = "K074";
        $row['title'] = "Add Jadwal Kunjungan";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['date'] = $tanggal;
        $listCust = $this->m_jadwal_kunjungan->availableCustomer($tanggal);//die(var_dump($listCust));
        foreach($listCust as $dt) {
            $list_cust[] = array('id' => $dt['id_customer'], 'text' => $dt['nama_customer']);
        }
        $row['list_customer'] = $list_cust;
        $row['id_sales'] = $id_sales;
        $this->load->view('jadwal_kunjungan/v_jadwal_kunjungan_manipulate', $row);
    }
    
    public function jadwal_kunjungan_manipulate() 
    {
        $message='';

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $this->form_validation->set_rules('id_sales', 'Sales', 'trim|required');
        $this->form_validation->set_rules('notes', 'Notes', 'trim');
        $this->form_validation->set_rules('results', 'Results', 'trim');
        $this->form_validation->set_rules('type', 'Type', 'trim');
        $this->form_validation->set_rules('start_time', 'Start Time', 'trim');
        $this->form_validation->set_rules('end_time', 'End Time', 'trim');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim');
        $this->form_validation->set_rules('device_id', 'Device Id', 'trim');
        $this->form_validation->set_rules('device_name', 'Device Name', 'trim');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_jadwal_kunjungan->Jadwal_kunjunganManipulate($model);
            echo json_encode($status);
        }
    }

    public function create_api() 
    {
        ini_set('memory_limit', '200M');
        ini_set('upload_max_filesize', '200M');  
        ini_set('post_max_size', '200M');  
        ini_set('max_input_time', 3600);  
        ini_set('max_execution_time', 3600);
        $message='';

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim');
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim');
        $this->form_validation->set_rules('id_sales', 'Sales', 'trim');
        $this->form_validation->set_rules('notes', 'Notes', 'trim');
        $this->form_validation->set_rules('results', 'Results', 'trim');
        $this->form_validation->set_rules('type', 'Type', 'trim');
        $this->form_validation->set_rules('start_time', 'Start Time', 'trim');
        $this->form_validation->set_rules('end_time', 'End Time', 'trim');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim');
        $this->form_validation->set_rules('device_id', 'Device Id', 'trim');
        $this->form_validation->set_rules('device_name', 'Device Name', 'trim');
        $model=$this->input->post();
        if ($model['id_jadwal_kunjungan']) {
            $data = $this->m_jadwal_kunjungan->GetOneJadwal_kunjungan($model['id_jadwal_kunjungan']);
            $model['id_sales'] = $data->id_sales;
            $model['id_customer'] = $data->id_customer;
            $model['tanggal'] = $data->tanggal;
        }
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            if(!empty($_FILES['foto']['name'])) {
                $uploadPath = './upload/upload-foto/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['overwrite'] = true;
                $config['file_name'] = $_FILES['foto']['name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('foto')){
                    $id_jadwal=$this->input->post('id_jadwal_kunjungan');
                    $nama=$this->upload->data('file_name');
                    $file_path = FCPATH . 'upload/upload-foto/';
                    $image_path = base_url('upload/upload-foto');
                    $created_at = date('Y-m-d');
                    $this->db->insert('#_foto_kunjungan',array('id_jadwal'=>$id_jadwal, 'filename'=>$nama, 'file_path'=>$file_path, 'image_path'=>$image_path, 'created_at'=>$created_at));
                }
            }
            if(!empty($_FILES['attachments']['name'])) {
                $uploadPath = './upload/upload-foto/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['overwrite'] = true;
                $config['file_name'] = $_FILES['attachments']['name'];
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('attachments')){
                    $id_jadwal=$this->input->post('id_jadwal_kunjungan');
                    $nama=$this->upload->data('file_name');
                    $file_path = FCPATH . 'upload/upload-foto/';
                    $image_path = base_url('upload/upload-foto');
                    $created_at = date('Y-m-d');
                    $this->db->insert('#_foto_kunjungan',array('id_jadwal'=>$id_jadwal, 'filename'=>$nama, 'file_path'=>$file_path, 'image_path'=>$image_path, 'created_at'=>$created_at));
                }
            }
            // die;
            $status=$this->m_jadwal_kunjungan->Jadwal_kunjunganManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_jadwal_kunjungan($id=0) 
    {
        $row = $this->m_jadwal_kunjungan->GetOneJadwal_kunjungan($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K075";
            $header = "K074";
            $row['title'] = "Edit Jadwal Kunjungan";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
            $row['date'] = $row['tanggal'];
            $row['sisa'] = $row['plafond'] - $row['piutang'];
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_images'] = $this->m_jadwal_kunjungan->getImages($id);
            $row['list_status'] = array('1' => 'Sudah Dikunjungi', '2' => 'Tidak Dikunjungi');
	        $this->load->view('jadwal_kunjungan/v_jadwal_kunjungan_manipulate', $row);
        } else {
            SetMessageSession(0, "Jadwal_kunjungan cannot be found in database");
            redirect(site_url('jadwal_kunjungan'));
        }
    }

    public function proses_kunjungan($id=0) 
    {
        $data = $this->input->post();
        
        if(!empty($_FILES['files']['name'])){
            $filesCount = count($_FILES['files']['name']);
            $fileName = array();
            for($i = 0; $i < $filesCount; $i++){

                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                
                $uploadPath = './upload/upload-foto/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['overwrite'] = true;
                $config['file_name'] = $_FILES['files']['name'];
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload()){
                    print_r($config);
                    $token=$this->input->post('token_foto');
                    $id_jadwal=$this->input->post('id_jadwal');
                    $nama=$this->upload->data('file_name');
                    $file_path = FCPATH . 'upload/upload-foto/';
                    // $image_path = FCPATH . 'upload/upload-foto/';
                    $image_path = base_url('upload/upload-foto');
                    $created_at = date('Y-m-d');
                    $this->db->insert('#_foto_kunjungan',array('id_jadwal'=>$id_jadwal, 'filename'=>$nama, 'file_path'=>$file_path, 'image_path'=>$image_path, 'created_at'=>$created_at, 'token'=>$token));
                }
            }
        }
        // unset($data['token']);
        // $model['updated_date'] = GetDateNow();
        // $model['updated_by'] = ForeignKeyFromDb(GetUserId());
        // $res = $this->db->update("dgmi_jadwal_kunjungan", $data, array("id_jadwal_kunjungan" => $data['id_jadwal_kunjungan']));
        // echo json_encode(array("st" => true, "msg" => "Jadwal_kunjungan has been updated"));
    }

    function upload_foto(){

        $config['upload_path']   = FCPATH.'/upload/upload-foto/';
        $config['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload',$config);

        if($this->upload->do_upload('userfile')){
        	$token=$this->input->post('token_foto');
        	$id_jadwal=$this->input->post('id_jadwal');
            $nama=$this->upload->data('file_name');
            $file_path = FCPATH . 'upload/upload-foto/';
            // $image_path = FCPATH . 'upload/upload-foto/';
            $image_path = base_url('upload/upload-foto');
            $created_at = date('Y-m-d');
        	$this->db->insert('#_foto_kunjungan',array('id_jadwal'=>$id_jadwal, 'filename'=>$nama, 'file_path'=>$file_path, 'image_path'=>$image_path, 'created_at'=>$created_at, 'token'=>$token));
        }
    }

    function remove_foto(){
		$token=$this->input->post('token');
		$foto=$this->db->get_where('#_foto_kunjungan',array('token'=>$token));

		if($foto->num_rows()>0){
			$hasil=$foto->row();
			$nama_foto=$hasil->filename;
            $st = false;
			if(file_exists($file=FCPATH.'upload/upload-foto/'.$nama_foto)){
                unlink($file);
                $del = $this->db->delete('#_foto_kunjungan', array('token' => $token));
                if ($del){
                    $st = true;
                }
			}
		}
        // echo "{}";
        echo json_encode(array('msg' => $file, 'st' => $st));
    }
    
    function deleteFotoJadwalKunjungan(){
        $id=$this->input->post('id');
        
        $foto=$this->db->get_where('#_foto_kunjungan',array('id'=> $id));
        if($foto->num_rows() > 0){
            $res = $foto->row();
            $nama_foto = $res->filename;
            $st = false;
			if(file_exists($file=FCPATH.'upload/upload-foto/'.$nama_foto)){
                unlink($file);
                $del = $this->db->query("delete from #_foto_kunjungan where id=$id");
                if ($del){
                    $st = true;
                }
			}
        }
        echo json_encode(array('msg' => $id, 'st' => $st));
    }
    
    public function jadwal_kunjungan_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_jadwal_kunjungan', 'Jadwal_kunjungan', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_jadwal_kunjungan->Jadwal_kunjunganDelete($model['id_jadwal_kunjungan']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Jadwal_kunjungan.php */