<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_jadwal_kunjungan extends CI_Model
{

    public $table = '#_jadwal_kunjungan';
    public $id = 'id_jadwal_kunjungan';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatajadwal_kunjungan() {
        $this->load->library('datatables');
        $this->datatables->select('id_jadwal_kunjungan,tanggal,id_customer,id_sales,notes,results,status,type,start_time,end_time,longitude,latitude,device_id,device_name');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_jadwal_kunjungan.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('jadwal_kunjungan/edit_jadwal_kunjungan/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletejadwal_kunjungan($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_jadwal_kunjungan');
        return $this->datatables->generate();
    }

    // get all
    function GetOneJadwal_kunjungan($keyword, $type = 'id_jadwal_kunjungan', $is_full=true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        if($is_full){
            $this->db->join('#_customer', '#_customer.id_customer=#_jadwal_kunjungan.id_customer');
        }
        $jadwal_kunjungan = $this->db->get($this->table)->row();
        return $jadwal_kunjungan;
    }

    function Jadwal_kunjunganManipulate($model) {
        try {
            $model['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            if (CheckEmpty($model['id_jadwal_kunjungan'])) {
                $model['id_customer'] = ForeignKeyFromDb($model['id_customer']);
                $model['id_sales'] = ForeignKeyFromDb($model['id_sales']);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Jadwal_kunjungan successfull added into database');
                return array("st" => true, "msg" => "Jadwal_kunjungan successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_jadwal_kunjungan" => $model['id_jadwal_kunjungan']));
                SetMessageSession(1, 'Jadwal_kunjungan has been updated');
                return array("st" => true, "msg" => "Jadwal_kunjungan has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownJadwal_kunjungan() {
        $listjadwal_kunjungan = GetTableData($this->table, 'id_jadwal_kunjungan', '', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listjadwal_kunjungan;
    }

    function GetListJadwal ($start_format, $end_format, $id_sales) {
        $this->db->join('dgmi_customer', 'dgmi_customer.id_customer=dgmi_jadwal_kunjungan.id_customer');
        $this->db->where('tanggal >=', $start_format);
        $this->db->where('tanggal <=', $end_format);
        $this->db->where('id_sales', $id_sales);
        $this->db->select('id_jadwal_kunjungan, nama_customer, tanggal, notes, #_jadwal_kunjungan.status as statusJk');
        return $listJadwal = $this->db->get($this->table)->result();
    }

    function GetListJadwalApi ($tanggal, $id_sales, $status) {
        $this->db->join('dgmi_customer', 'dgmi_customer.id_customer=dgmi_jadwal_kunjungan.id_customer');
        $this->db->where('tanggal', $tanggal);
        if($status != null || $status != '') {
            $this->db->where('dgmi_jadwal_kunjungan.status', $status);
        }
        $this->db->where('id_sales', $id_sales);
        $this->db->select("id_jadwal_kunjungan as id, nama_customer as name, tanggal as `date`, device_id, device_name, 
                                    #_jadwal_kunjungan.latitude, #_jadwal_kunjungan.longitude, dgmi_jadwal_kunjungan.status as `status`, `type`, 
                                    IF(dgmi_jadwal_kunjungan.status = '1', '8BC34A', IF(dgmi_jadwal_kunjungan.status = '2', 'F44336', 'FFC107')) as color,
                                    IF(dgmi_jadwal_kunjungan.status = '1', 'true', 'false') as editable");
        return $listJadwal = $this->db->get($this->table)->result_array();
    }

    function GetListJadwalApi2 ($tanggal, $id_sales, $status) {
        $this->db->join('dgmi_customer', 'dgmi_customer.id_customer=dgmi_jadwal_kunjungan.id_customer');
        $this->db->where_in('tanggal', $tanggal);
        if($status != null || $status != '') {
            $this->db->where('dgmi_jadwal_kunjungan.status', $status);
        }
        $this->db->where('id_sales', $id_sales);
        $this->db->select("id_jadwal_kunjungan as id, nama_customer as name, tanggal as `date`, device_id, device_name, 
                                    #_jadwal_kunjungan.latitude, #_jadwal_kunjungan.longitude, dgmi_jadwal_kunjungan.status as `status`, `type`, 
                                    IF(dgmi_jadwal_kunjungan.status = '1', '8BC34A', IF(dgmi_jadwal_kunjungan.status = '2', 'F44336', 'FFC107')) as color,
                                    IF(dgmi_jadwal_kunjungan.status = '1', 'true', 'false') as editable");
        return $listJadwal = $this->db->get($this->table)->result_array();
    }

    function GetDetil($id_jadwal){
        $this->db->where('id_jadwal_kunjungan', $id_jadwal);
        $this->db->join('dgmi_pegawai', 'dgmi_pegawai.id_pegawai=dgmi_jadwal_kunjungan.id_sales', 'left');
        $this->db->select("dgmi_jadwal_kunjungan.*, nama_pegawai, IF(dgmi_jadwal_kunjungan.status = '1', '8BC34A', IF(dgmi_jadwal_kunjungan.status = '2', 'F44336', 'FFC107')) as color");
        return $detil = $this->db->get($this->table)->row_array();
    }

    function GetFoto($id_jadwal) {
        $this->db->where('id_jadwal', $id_jadwal);
        return $foto = $this->db->get('dgmi_foto_kunjungan')->result_array();
    }

    function GetLampiran($id_jadwal) {
        $this->db->where('id_jadwal', $id_jadwal);
        return $lampiran = $this->db->get('dgmi_lampiran_kunjungan')->result_array();
    }

    function GetPelanggan($id_pelanggan) {
        $this->db->where('id_customer', $id_pelanggan);
        $this->db->select('id_customer as id_pelanggan, nama_customer as nama_pelanggan, alamat as alamat_pelanggan, no_telp as no_telp_pelanggan, contact_person as pic_pelanggan, plafond as plafon_pelanggan, npwp as npwp_pelanggan, alamat_npwp as alamat_pajak, 
                            COALESCE(dgmi_customer.latitude, 0) as latitude, 
                            COALESCE(dgmi_customer.longitude, 0) as longitude,');
        return $this->db->get('dgmi_customer')->row();
    }

    function GetAvailDateApi($year, $month, $id_sales, $status) {
        $this->db->where('year(tanggal)', $year);
        $this->db->where_in('month(tanggal)', $month);
        $this->db->where('id_sales', $id_sales);
        if($status != null || $status != '') {
            $this->db->where('dgmi_jadwal_kunjungan.status', $status);
        }
        $this->db->group_by('tanggal');
        $list_tanggal = $this->db->select('tanggal')->get($this->table)->result();

        return $list_tanggal;
    }

    function availableCustomer($tanggal, $q=null, $id_pegawai=null) {
        $this->db->where('dgmi_customer.deleted_date', null);
        $this->db->where("not exists(select id_jadwal_kunjungan from dgmi_jadwal_kunjungan where tanggal = '$tanggal' and dgmi_customer.id_customer = dgmi_jadwal_kunjungan.id_customer)");
        if($q!=null){
            $this->db->like('dgmi_customer.nama_customer', $q);
        }
        $customer = $this->db->get('dgmi_customer')->result_array();
        $dataCust = array();
        foreach($customer as $data) {
            $message = 'Customer Diluar Jangkauan';
            $statusCustomer = 0;
            $this->db->where('dgmi_customer_pegawai.id_customer', $data['id_customer']);
            $this->db->where('dgmi_customer_pegawai.id_pegawai', $id_pegawai);
            $cust = $this->db->get('dgmi_customer_pegawai')->row_array();
            if($cust && $cust['id_pegawai'] == $id_pegawai) {
                $message = '';
                $statusCustomer = 1;
            }
            $data['message'] = $message;
            $data['status_customer'] = $statusCustomer;
            $dataCust[] = $data;
        }
       
        return $dataCust;
    }

    function getImages($id_jadwal) {
        $this->db->where('id_jadwal', $id_jadwal);
        $images = $this->db->get('#_foto_kunjungan')->result_array();
        return $images;
    }

    function Jadwal_kunjunganDelete($id_jadwal_kunjungan) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_jadwal_kunjungan' => $id_jadwal_kunjungan));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Jadwal_kunjungan has been deleted from database");
    }


}