<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
<style>
.tg {
    border-collapse: collapse;
    border-spacing: 0;
    border-color: #ccc;
    width: 100%;
}

.tg td {
    font-family: Arial, sans-serif;
    font-size: 11px;
    padding: 3px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: #ccc;
    color: #333;
    background-color: #fff;
}

.tg th {
    font-family: Arial, sans-serif;
    font-size: 12px;
    font-weight: bold;
    padding: 3px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: #ccc;
    color: #333;
    background-color: #f0f0f0;
}

.tg .tg-lqy6 {
    text-align: right;
    vertical-align: top
}

.tg .tg-yw4l {
    vertical-align: top
}

.tg .tg-amwm {
    font-weight: bold;
    text-align: center;
    vertical-align: top
}

.top-ver {
    vertical-align: top
}

.modal-content {
    border: none !Important;
    box-shadow: none !important;
}

.modal-body {
    font-family: Arial, sans-serif;
    font-size: 12px;
    padding: 3px;
    overflow: hidden;
    word-break: normal;
    color: #333;
    background-color: #fff;
}

@page {
    margin: 0
}

body {
    margin: 0
}

.sheet {
    margin: 0;
    overflow: hidden;
    position: relative;
    page-break-after: always;
}

/** Paper sizes **/
body.A3 .sheet {
    width: 297mm;
    height: 419mm
}

body.A3.landscape .sheet {
    width: 420mm;
    height: 296mm
}

body.A4 .sheet {
    width: 210mm;
    height: 296mm
}

body.A4.landscape .sheet {
    width: 297mm;
    height: 209mm
}

body.A5 .sheet {
    width: 148mm;
    height: 209mm
}

body.A5.landscape .sheet {
    width: 210mm;
    height: 147mm
}

/** Padding area **/
.sheet.padding-10mm {
    padding: 8mm
}

.sheet.padding-15mm {
    padding: 8mm
}

.sheet.padding-20mm {
    padding: 18mm
}

.sheet.padding-25mm {
    padding: 23mm
}

/** For screen preview **/
@media screen {
    .sheet {
        background: white;
        margin: 5mm;
    }

    .footerpage {
        margin-bottom: 100px;
    }

}

/** Fix for Chrome issue #273306 **/
@media print {
    .footerpage {
        position: fixed
    }

    body.A3.landscape {
        width: 420mm
    }

    body.A3,
    body.A4.landscape {
        width: 297mm
    }

    body.A4,
    body.A5.landscape {
        width: 210mm
    }

    body.A5 {
        width: 148mm
    }

    /* .sheet { page-break-after: always } */
}

@page {
    size: A4
}
</style>


<div class="modal-content">
    <div class="sheet padding-10mm modal-body" style="padding-top:0px;">
        <img src="<?php echo base_url()?>assets/images/kop_atas.jpg" style="width:100%" />
        <hr />
        <div class="col-sm-12">
            <div class="row">
                <div style="width:50%;float:left;">
                    Jakarta, <?php echo DefaultTanggalWithday($created_date) ?>
                </div>
                <div style="text-align:right;width:50%;float:right;">
                    No.<?php echo @$nomor_mutasi ?>
                </div>
            </div>
            <br />
            <br />
            <div class="row">

                <div style="font-weight:bold;font-size:12px;text-align:left;">Kepada YTH :
                    <br /><?php echo $nama_ekspedisi ."<br/>". $cp_ekspedisi?><br /><br />Perihal : DELIVERY ORDER
                    PENGAMBILAN DAN PENGIRIMAN CHASSIS
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12" style="height:20px;"></div>
            </div>
            <div class="row">
                <br /><br />
                DenganHormat,<br /><br />

                Mohon chassis dengan data – data di bawah ini dipersiapkan untuk :<br /><br />

            </div>
            <div class="row">
                <section>
                    <table class="tg">
                        <tbody>
                            <tr style="font-weight: bold">
                                <td width="10px">No</td>
                                <td>Type</td>
                                <td>Jumlah</td>
                                <td width="15%">Tanggal Kirim</td>
                                <td>No Rangka</td>
                                <td>No Mesin</td>
                                <td>Ambil Di</td>
                                <td>Kirim Ke</td>
                            </tr>
                            <?php foreach (@$listdetail as $key => $detail) { ?>
                            <tr>
                                <td width="10px"><?php echo @$key + 1 ?></td>
                                <?php if ($key == "0") { ?>
                                <td rowspan="<?php echo count(@$list_detail) ?>">
                                    <?php echo@$detail->nama_kategori . " " . $detail->nama_unit . " VIN " . $detail->vin ?>
                                </td>
                                <td rowspan="<?php echo count(@$list_detail) ?>">
                                    <?php echo count(@$listdetail) . " Unit "; ?></td>
                                <td rowspan="<?php echo count(@$list_detail) ?>">
                                    <?php echo DefaultDatePicker(@$tanggal_do); ?></td>
                                <?php } ?>
                                <td><?php echo@$detail->vin_number ?></td>
                                <td><?php echo@$detail->engine_no ?></td>
                                <?php if ($key == "0") { ?>
                                <td style="text-align:center" rowspan="<?php echo count(@$list_detail) ?>">
                                    <?php echo @$atas_nama_pengambilan . "<br/>" . @$alamat_pengambilan; ?><br /><?php echo CheckEmpty($up_pengambilan)?"":"UP ". @$up_pengambilan; ?>
                                </td>
                                <td style="text-align:center" rowspan="<?php echo count(@$list_detail) ?>">
                                    <?php echo (CheckEmpty(@$keterangan_kirim)?"":@$keterangan_kirim."<br/><br/>"). @$atas_nama . "<br/>" . @$alamat; ?><br />UP<br /><?php echo @$up; ?>
                                </td>
                                <?php } ?>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                    <h3 style="font-weight:bold;">CUST : <?php echo @$nama_customer ?></h3>
                    <span style="font-weight:bold;font-style: italic">• Mohon Unit diesek – esek Samsat Polda Metro &
                        LAKBAN dua set sebelum dikirim ( Mohon Gesekan jelas)
                        Dan dikirim ke Jl Pemuda No 7 dua hari sejak tanggal pengiriman<br />
                        • Mohon untuk buku service dikirim ke kantor Jl Pemuda No 7 dua hari dari tanggal
                        pengiriman</span><br />
                    Hormat Kami ,
                    <table class="footerpage"
                        style="bottom:200px;margin-top:20px;border-style:none;width:100%; border-width:0px;font-size:11px">
                        <tbody>
                            <tr style="height:70px;">
                                <td width="50%" class="top-ver" style="text-align:left">Dibuat Oleh:</td>
                                <td width="50%" class="top-ver" style="text-align:center"></td>
                            </tr>
                            <tr style="margin-top:-20px;">
                                <td width="50%" style="text-align:left"><b><?php echo @$nama_user_login ?></b></td>
                                <td width="20%" style="text-align:center"><b></b></td>
                            </tr>
                        </tbody>
                    </table>
                </section>

            </div>
        </div>
    </div>
    <img src="<?php echo base_url()?>assets/images/kop_bawah.jpg" style="width:100%;position:fixed;bottom:40px;" />
</div>


<script>
window.print();
// window.close();
</script>