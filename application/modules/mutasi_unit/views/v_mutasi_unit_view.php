<section class="content-header">
    <h1>
        Surat Jalan Mutasi <?= @$button ?>
        <small>Surat Jalan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Surat Jalan </li>
        <li class="active">Surat Jalan Mutasi</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_mutasi_unit" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_mutasi" value="<?php echo @$id_mutasi; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Type DO', "type_do", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "type_do"), DefaultEmptyDropdown(@$list_type_do, "json", "Jenis Do"), @$type_do, array('class' => 'form-control select2', 'id' => 'dd_type_do')); ?>
                                    </div>
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal_do', 'value' => DefaultDatePicker(@$tanggal_do), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <?= form_label('Pool Tujuan', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_pool"), DefaultEmptyDropdown(@$list_pool, "json", "Tujuan"), @$id_pool, array('class' => 'form-control select2', 'id' => 'dd_id_pool')); ?>
                                    </div>
                                    <?= form_label('Ekspedisi', "dd_id_ekspedisi", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_ekspedisi', 'class' => 'form-control select2', 'id' => 'dd_id_ekspedisi', 'placeholder' => 'Ekspedisi'), DefaultEmptyDropdown(@$list_ekspedisi, "json", "Ekspedisi"), @$id_ekspedisi); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?php
                                        if (@$id_mutasi_unit)
                                            echo form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'));
                                        else
                                            echo form_textarea(array('type' => 'text', "rows" => 4, 'value' => "Unit Tidak Boleh Keluar Dari Karoseri Tanpa Persetujuan DGMI", 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'));
                                        ?>
                                    </div>
                                </div>
                                <div>
                                    <table class="td30 tr30 table table-striped table-bordered table-hover dataTable no-footer">
                                        <thead>
                                            <tr style="height:30px;background-color: blue;color:#FFFFFF;">
                                                <th>Nama Unit</th>
                                                <th>Nama Kategori</th>
                                                <th>Nama Customer</th>
                                                <th>VIN</th>
                                                <th>VIN Num</th>
                                                <th>Engine No</th>
                                                <th>Pool</th>
                                            </tr>
                                        </thead>
                                        <tbody id="serial_unit_list">
                                            <?php
                                            $arr_type_do = array('-', 'Masuk', 'Keluar', 'Customer');
                                            foreach (@$listdetail as $dt) {
                                                echo "<tr>
                                                        <td>" . $dt['nama_unit'] . "</td>
                                                        <td>" . $dt['nama_kategori'] . "</td>
                                                        <td>" . $dt['nama_customer'] . "</td>
                                                        <td>" . $dt['vin'] . "</td>
                                                        <td>" . $dt['vin_number'] . "</td>
                                                        <td>" . $dt['engine_no'] . "</td>
                                                        <td>" . $dt['nama_pool'] . "</td>
                                                    </tr>";
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="areaUnit">

                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('UP', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$up, 'name' => 'up', 'class' => 'form-control', 'id' => 'txt_up', 'placeholder' => 'UP')); ?>
                                    </div>
                                    <?= form_label('Order', "txt_alamat_kirim", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'value' => @$order, 'name' => 'order', 'class' => 'form-control', 'id' => 'txt_order_ket', 'placeholder' => 'Order')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat Kirim', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat, 'name' => 'alamat', 'class' => 'form-control', 'id' => 'txt_alamat_kirim', 'placeholder' => 'Alamat Kirim')); ?>
                                    </div>
                                </div>
                                <hr/>
                                <?php if (CheckEmpty(@$is_view)) { ?>
                                    <div class="form-group" style="margin-top:50px">
                                        <a style="float:right;" href="<?php echo base_url() . 'index.php/mutasi_unit' ?>" class="btn btn-default"  >Cancel</a>
                                        <div class="col-sm-2">
                                            <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                        </div>
                                    </div>
                                <?php } ?>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    $(document).ready(function () {
        $("input").attr("readonly", "readonly");
        $("textarea").attr("disabled", "disabled");
        $(".select2").select2({disabled: "readonly"});

    });

</script>