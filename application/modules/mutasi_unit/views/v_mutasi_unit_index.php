
<section class="content-header noprint">
    <h1>
        Surat Jalan Mutasi
        <small>Data Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">Surat Jalan Mutasi</li>
    </ol>


</section>

<section class="content noprint">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), @$list_pencarian); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'selected' => @$value_status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                        </div>


                    </div>
                </div>  
                <div class="row">
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_keyword', 'selected' => @$jenis_keyword, 'class' => 'form-control select2', 'id' => 'dd_jenis_keyword', 'placeholder' => 'Jenis Keyword'), @$list_keyword); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                    </div>
                </div>



            </form>
            <hr/>
            <div class=" headerbutton">

                <?php echo anchor(site_url('mutasi_unit/create_mutasi_unit'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    $("#dd_jenis_keyword").change(function () {
        LoadAreaKeyword();
    })
    function cetaksuratperintah(id_mutasi_unit) {
        openmodaldefault(baseurl + 'index.php/mutasi_unit/surat_perintah_mutasi_unit/' + id_mutasi_unit, "800px", "1000px");
    }
    function LoadAreaKeyword()
    {
        if (CheckEmpty($("#dd_jenis_keyword").val()))
        {
            $("#txt_keyword").val("");
            $("#txt_keyword").attr("readonly", "readonly");
        } else
        {
            $("#txt_keyword").removeAttr("readonly");
        }
    }


    function ubahstatus(id, status)
    {
        swal({
            title: "Apakah kamu yakin ingin " + (status == "1" ? "mengapprove" : status == "3" ? "Mereject" : (status == "5" ? "Membatalkan" : "Menutup")) + " Mutasi Berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/mutasi_unit/mutasi_unit_change_status',
                    dataType: 'json',
                    data: {
                        id_mutasi: id,
                        status: status

                    },
                    success: function (data) {
                        if (data.st)
                        {
                            RefreshGrid();
                            messagesuccess(data.msg);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
    }
    function bataldoprospek(id_mutasi_unit) {
        swal({
            title: "Apakah kamu ingin membatalkan do prospek berikut?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, batalkan!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/mutasi_unit/mutasi_unit_batal',
                    dataType: 'json',
                    data: {
                        id_mutasi_unit: id_mutasi_unit
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }

    function RefreshGrid()
    {
        table.fnDraw(false);
    }
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $(document).ready(function () {
        $(".datepicker").datepicker();
        LoadAreaKeyword();
        $(".select2").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "mutasi_unit/getdatamutasi", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_mutasi",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal_do", orderable: false, title: "Tanggal", "className": "text-center",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
                {data: "nomor_mutasi", orderable: false, title: "Nomor DO"},
                {data: "qty_do", orderable: false, title: "Jumlah Unit"},
                {data: "type_do", orderable: false, title: "Type DO"},
                {data: "keterangan", orderable: false, title: "Keterangan"},
                {data: "up", orderable: false, title: "UP"},
                {data: "order", orderable: false, title: "Order"},
                {data: "nama_ekspedisi", orderable: false, title: "Ekspedisi"},
                {data: "nama_pool", orderable: false, title: "Pool"},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        if (data == 1)
                        {
                            return "Approved";

                        } else if (data == 2)
                        {
                            return "Rejected";
                        } else if (data == 3)
                        {
                            return "Closed";
                        } else if (data == 4)
                        {
                            return "Finished";
                        } else if (data == 5)
                        {
                            return "Batal";
                        } else
                        {
                            return "Pending";
                        }

                    }},
                {
                    "data": "view",
                    "orderable": false,
                    "className": "text-center",
                    width: "200px",
                    mRender: function (data, type, row) {
                        var action = "";
                        if (row['status'] != 5)
                        {


<?php
if (GetUserId() == "1") {
    echo "action += row['edit'];";
} else {
    ?>
                                if (row['status'] == 0)
                                {
                                    action += row['edit'];
                                }

<?php } ?>

                            if (row['status'] == 0)
                            {
                                action += row['approved'] + row['rejected'];
                            } else if (row['status'] == 1 && (row['pengemudi'] == '' || row['pengemudi'] == null))
                            {
                                action += row['process'];
                            } else if ((row['pengemudi'] != '' || row['pengemudi'] != null)) {
                                action += row['cetak'];
                            }




                            if (row['status'] != 2)
                            {
                                action += row['batal'];
                                if ((row['tanggal_selesai'] == null || row['tanggal_selesai'] == ''))
                                {
                                    action += row['selesai'];
                                }
                            }

                        }

                        return action;
                    }}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });

    function selesaiDo(id_mutasi_unit) {
        $.ajax({url: baseurl + 'index.php/mutasi_unit/selesai_mutasi_unit',
            data: 'id_mutasi=' + id_mutasi_unit,
            success: function (data) {
                modalbootstrap(data, '', 800);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    function proses(id_mutasi_unit) {
        $.ajax({url: baseurl + 'index.php/mutasi_unit/proses_mutasi_unit',
            data: 'id_mutasi=' + id_mutasi_unit,
            success: function (data) {
                modalbootstrap(data, '', 800);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    function printDo(id_mutasi_unit) {
        $.ajax({url: baseurl + 'index.php/mutasi_unit/print_mutasi',
            data: 'id_mutasi_unit=' + id_mutasi_unit,
            success: function (data) {
                // console.log('data > ',data);return false;
                modalbootstrap(data, 'Print Mutasi', 1200);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
</script>
<style>
    @media print
    {
        .noprint {
            display:none;
        }

        .modal-footer, .modal-header {
            display: none;
        }

        .modal-dialog{
            border:0px;
        }
    }
</style>