<div class="modal-header">
<?php echo @$title; ?>
    </div>
<div class="modal-body">
<form id="frm_selesai_do_prospek" class="form-horizontal form-groups-bordered validate" method="post" autocomplete="off">
	<input type="hidden" name="id_mutasi" id="id_do_prospek" value="<?php echo @$id_mutasi; ?>" />
	<?= form_input(array('type' => 'hidden', 'name' => 'tanggal_selesai', 'class' => 'form-control', 'id' => 'tanggal_selesai', 'value' => @$tanggal_selesai, 'placeholder' => 'tanggal_selesai')); ?>
	<div class="form-group">
		<?= form_label('Laporan Keterangan', "txt_description", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?= form_textarea(array('type' => 'text', 'rows' => '5', 'cols' => '10', 'class' => 'form-control', 'name' => 'keterangan_selesai', 'id' => 'keterangan_selesai', 'placeholder' => 'Laporan Keterangan', 'value' => @$keterangan_selesai)); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Gambar', "txt_description", array("class" => 'col-sm-2 control-label')); ?>
	</div>
	<div class="form-group">
		<?php 
			// foreach(@$list_images as $img) {
			// 	$path = base_url() . 'upload/upload-foto/' . $img['filename'];
			// 	echo "<div class='col-sm-3' style='position:relative'><img src='" . $path. "'>
			// 			<span class='remove glyphicon glyphicon-remove' style='position: absolute; top: 4px; right: 5px; cursor:pointer' id='" . $img['id']. "'></span>
			// 		 </div>";
			// }
		 ?>
	</div>
	<div class="form-group">
		<div class="dropzone">
			<div class="dz-message">
				<h3> Klik atau Drop gambar disini</h3>
			</div>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" ><?php echo $button == 'Update' ? 'Update' : 'Simpan' ?></button>
    </div>

</form>
</div>
<script>
	Dropzone.autoDiscover = false;

	var foto_upload= new Dropzone(".dropzone",{
		url: "<?php echo base_url('index.php/do_prospek/upload_foto') ?>",
		maxFilesize: 2,
		method:"post",
		acceptedFiles:"image/*",
		paramName:"userfile",
		dictInvalidFileType:"Type file ini tidak dizinkan",
		addRemoveLinks:true,
	});
	//Event ketika Memulai mengupload
	foto_upload.on("sending",function(a,b,c){
		a.token=Math.random();
		c.append("token_foto",a.token);
		c.append("id_do_prospek", $("#id_do_prospek").val());
	});

	//Event ketika foto dihapus
	foto_upload.on("removedfile",function(a){
		var token=a.token;
		$.ajax({
			type:"post",
			data:{token:token},
			url:"<?php echo base_url('index.php/mutasi_unit/remove_foto') ?>",
			cache:false,
			dataType: 'json',
			success: function(z){
				if (z.st) {
					console.log('berhasil dihapus');
				}
			},
			error: function(){
				console.log("Error");

			}
		});
	});
	$(document).ready(function() {
	})

	$("#dd_id_customer").change(function(){
		var id_cust = $(this).val();
		$.ajax({
			type: 'POST',
			url: baseurl + 'index.php/customer/get_one_customer',
			data: 'id_customer=' + id_cust,
			dataType: 'json',
			success: function (data) {
				$("#plafond").val(Comma(data.obj.plafond));
				$("#piutang").val(Comma(data.obj.piutang));
				$("#sisa").val(Comma(data.obj.plafond - data.obj.piutang));
			},
			error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
		})
	})

	$(".remove").click(function () {
		var id = $(this).attr('id');
		$.ajax({
			type:"post",
			data:'id=' + id,
			url:"<?php echo base_url('index.php/mutasi_unit/deleteFotoDoProspek') ?>",
			cache:false,
			dataType: 'json',
			success: function(z){
				if (z.st) {
					console.log('berhasil dihapus');
				}
			},
			error: function(){
				console.log("Error");

			}
		});
	});

	$("#frm_selesai_do_prospek").submit(function () {
		// console.log($(this).serialize());
		// return false;
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/mutasi_unit/save_selesai_mutasi_unit',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
				console.log(data);
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
					$("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
	.bootstrap-timepicker-widget.dropdown-menu.open {
		display: inline-block;
		z-index: 99999 !important;
	}
	.glyphicon {
		font-size: 20px;
		color: red;
	}
</style>