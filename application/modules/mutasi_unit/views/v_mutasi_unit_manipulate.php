<section class="content-header">
    <h1>
        Surat Jalan Mutasi <?= @$button ?>
        <small>Surat Jalan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Surat Jalan </li>
        <li class="active">Surat Jalan Mutasi</li>
    </ol>

</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_mutasi_unit" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_mutasi" value="<?php echo @$id_mutasi; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal_do', 'value' => DefaultDatePicker(@$tanggal_do), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Supplier', "dd_id_customer_search", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', "name" => "id_supplier", 'class' => 'form-control select2', 'id' => 'dd_id_supplier_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier); ?>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <?= form_label('Pool Asal', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array("name" => "id_pool_from"), DefaultEmptyDropdown(@$list_pool, "json", "Asal"), @$id_pool_from, array('class' => 'form-control select2', 'id' => 'dd_id_pool')); ?>
                                    </div>
                                    <?= form_label('Pool Tujuan', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array("name" => "id_pool"), DefaultEmptyDropdown(@$list_pool_tujuan, "json", "Tujuan"), @$id_pool, array('class' => 'form-control select2', 'id' => 'dd_id_pool_before')); ?>
                                    </div>
                                    <?= form_label('Ekspedisi', "dd_id_ekspedisi", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_ekspedisi', 'class' => 'form-control select2', 'id' => 'dd_id_ekspedisi', 'placeholder' => 'Ekspedisi'), DefaultEmptyDropdown(@$list_ekspedisi, "json", "Ekspedisi"), @$id_ekspedisi); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?php
                                        if (@$id_mutasi_unit)
                                            echo form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'));
                                        else
                                            echo form_textarea(array('type' => 'text', "rows" => 4, 'value' => "Unit Tidak Boleh Keluar Dari Karoseri Tanpa Persetujuan DGMI", 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'));
                                        ?>
                                    </div>
                                </div>






                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Customer', "dd_id_customer_search", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', "name" => "id_customer", 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer); ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <?= form_label('Kategori', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori', 'class' => 'form-control select2', 'id' => 'dd_id_kategori_search', 'placeholder' => 'Kategori'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$id_kategori); ?>
                                    </div>
                                    <?= form_label('Unit', "dd_id_unit_search", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit', 'class' => 'form-control select2', 'id' => 'dd_id_unit_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                                    </div>

                                </div>


                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Prospek', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        // if (!CheckEmpty(@$id_prospek)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_prospek, 'name' => 'id_prospek'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_prospek), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Prospek'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_prospek', 'class' => 'form-control select2', 'id' => 'dd_id_prospek', 'placeholder' => 'Prospek'), DefaultEmptyDropdown(@$list_prospek, "json", "Prospek"), @$id_prospek);
                                        // }
                                        ?>
                                    </div>
                                    <?= form_label('Buka Jual', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?php
                                        // if (!CheckEmpty(@$id_buka_jual)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_buka_jual, 'name' => 'id_buka_jual'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_buka_jual), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Buka Jual'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_buka_jual', 'class' => 'form-control select2', 'id' => 'dd_id_buka_jual', 'placeholder' => 'Buka Jual'), DefaultEmptyDropdown(@$list_buka_jual, "json", "Buka Jual"), @$id_buka_jual);
                                        echo form_input(array('type' => 'hidden', 'name' => 'id_customer', 'value' => @$id_customer, 'class' => 'form-control', 'id' => 'id_cust'));
                                        // }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('KPU', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        // if (!CheckEmpty(@$id_prospek)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_prospek, 'name' => 'id_prospek'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_prospek), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Prospek'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_kpu', 'class' => 'form-control select2', 'id' => 'dd_id_kpu', 'placeholder' => 'KPU'), DefaultEmptyDropdown(@$list_kpu, "json", "KPU"), @$id_kpu);
                                        // }
                                        ?>
                                    </div>
                                    <?= form_label('DO KPU', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?php
                                        // if (!CheckEmpty(@$id_buka_jual)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_buka_jual, 'name' => 'id_buka_jual'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_buka_jual), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Buka Jual'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_do_kpu', 'class' => 'form-control select2', 'id' => 'dd_id_do_kpu', 'placeholder' => 'DO KPU'), DefaultEmptyDropdown(@$list_do_kpu, "json", "DO KPU"), @$id_do_kpu);
                                        // }
                                        ?>
                                    </div>
                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        // if (!CheckEmpty(@$id_buka_jual)) {
                                        //     echo form_input(array('type' => 'hidden', 'value' => @$id_buka_jual, 'name' => 'id_buka_jual'));
                                        //     echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_buka_jual), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'Buka Jual'));
                                        // } else {
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_unit_serial', 'class' => 'form-control select2', 'id' => 'dd_id_unit_serial', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit_serial, "json", "Unit"), @$id_unit_serial);
                                        // }
                                        ?>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>

                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Warna')); ?>


                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('No Prospek', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$no_prospek, 'class' => 'form-control', 'id' => 'txt_no_prospek', 'placeholder' => 'No Prospek')); ?>
                                    </div>

                                    <?= form_label('Nomor Mesin', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_buka_jual, 'class' => 'form-control', 'id' => 'txt_nomor_buka_jual', 'placeholder' => 'Nomor Buka Jual')); ?>


                                    </div>
                                    <?= form_label('Nama  Customer', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Nama Customer')); ?>


                                    </div>
                                </div>


                                <div class="form-group">
                                    <?= form_label('Nomor Kendaraan', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => @$vin_number, 'class' => 'form-control', 'id' => 'txt_vin_number', 'placeholder' => 'Nomor Kendaraan')); ?>
                                    </div>

                                    <?= form_label('Nomor Mesin', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$engine_no, 'class' => 'form-control', 'id' => 'txt_engine_no', 'placeholder' => 'Nomor Mesin')); ?>

                                    </div>

                                    <div class="col-sm-1">
                                        <button type="button" class="btn btn-block btn bg-navy" id="btt_tambah_unit" >Tambah Unit</button>
                                    </div>
                                </div>



                                <div>
                                    <table  id="mytable" class="td30 tr30 table table-striped table-bordered table-hover dataTable no-footer">

                                    </table>
                                </div>
                                <div id="areaUnit">

                                </div>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Nama', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$atas_nama_pengambilan, 'name' => 'atas_nama_pengambilan', 'class' => 'form-control', 'id' => 'txt_atas_nama_pengambilan', 'placeholder' => 'Nama Pengambilan')); ?>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <?= form_label('UP', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$up_pengambilan, 'name' => 'up_pengambilan', 'class' => 'form-control', 'id' => 'txt_up_pengambilan', 'placeholder' => 'UP Pengambilan')); ?>
                                    </div>
                                    <?= form_label('Order', "txt_alamat_kirim", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'value' => @$order_pengambilan, 'name' => 'order_pengambilan', 'class' => 'form-control', 'id' => 'txt_order_ket_pengambilan', 'placeholder' => 'Order Pengambilan')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-block btn-success" id="btt_search_address_pengambilan" >Pencarian  Alamat</button>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat Pengambilan', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat_pengambilan, 'name' => 'alamat_pengambilan', 'class' => 'form-control', 'id' => 'txt_alamat_kirim_pengambilan', 'placeholder' => 'Alamat Pengambilan')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Nama', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$atas_nama, 'name' => 'atas_nama', 'class' => 'form-control', 'id' => 'txt_atas_nama', 'placeholder' => 'Nama')); ?>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <?= form_label('UP', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$up, 'name' => 'up', 'class' => 'form-control', 'id' => 'txt_up', 'placeholder' => 'UP')); ?>
                                    </div>
                                    <?= form_label('Order', "txt_alamat_kirim", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'value' => @$order, 'name' => 'order', 'class' => 'form-control', 'id' => 'txt_order_ket', 'placeholder' => 'Order')); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" class="btn btn-block btn-success" id="btt_search_address" >Pencarian  Alamat</button>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat Kirim', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat, 'name' => 'alamat', 'class' => 'form-control', 'id' => 'txt_alamat_kirim', 'placeholder' => 'Alamat Kirim')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan Kirim', "txt_alamat_kirim", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan_kirim, 'name' => 'keterangan_kirim', 'class' => 'form-control', 'id' => 'txt_keterangan_kirim', 'placeholder' => 'Keterangan Kirim')); ?>
                                    </div>
                                </div>
                                <hr/>
                                <?php if (CheckEmpty(@$is_view)) { ?>
                                    <div class="form-group" style="margin-top:50px">
                                        <a style="float:right;" href="<?php echo base_url() . 'index.php/mutasi_unit' ?>" class="btn btn-default"  >Cancel</a>
                                        <div class="col-sm-2">
                                            <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                        </div>
                                    </div>
                                <?php } ?>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    var table;
    var listdetail = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;
    var jumlahgenerate =<?= count(@$list_detail) ?>;
    $("#btt_search_address").click(function () {
        $.ajax({url: baseurl + 'index.php/module/address_book_search',
            success: function (data) {
                modalbootstrap(data, "Search Address Book", "1024px");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });


    })
    $("#btt_search_address_pengambilan").click(function () {
        $.ajax({url: baseurl + 'index.php/module/address_book_search/_pengambilan',
            success: function (data) {
                modalbootstrap(data, "Search Address Book", "1024px");
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });


    })

    function RefreshUnit()
    {
        var id_kategori = $("#dd_id_kategori_search").val();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_dropdown_unit',
            dataType: 'json',
            data: {
                id_kategori: id_kategori
            },
            success: function (data) {
                $("#dd_id_unit_search").empty();
                if (data.st)
                {
                    $("#dd_id_unit_search").select2({data: data.list_unit})
                }
                ClearFormDetail();
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }

    function RefreshProspek() {

        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/mutasi_unit/get_prospek_list',
            dataType: 'json',
            data: {
                id_kategori: $("#dd_id_kategori_search").val(),
                id_unit: $("#dd_id_unit_search").val(),
                id_customer: $("#dd_id_customer_search").val()
            },
            success: function (data) {
                $("#dd_id_prospek").empty();
                if (data.st)
                {
                    $("#dd_id_prospek").select2({data: data.list_prospek});
                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }

    function RefreshSerialUnit() {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_list_unit',
            data: $("#frm_mutasi_unit").serialize(),
            dataType: "json",
            success: function (data) {
                if (data.st)
                {
                    $("#dd_id_unit_serial").empty();
                    $("#dd_id_unit_serial").select2({data: data.obj});

                }

            }
        })
    }

    function RefreshBukaJual() {

        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/mutasi_unit/get_buka_jual_list',
            dataType: 'json',
            data: $("#frm_mutasi_unit").serialize(),
            success: function (data) {
                $("#dd_id_buka_jual").empty();
                if (data.st)
                {
                    $("#dd_id_buka_jual").select2({data: data.list_buka_jual});

                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }
    function RefreshDoKPU()
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/do_kpu/get_dropdown_do_kpu_for_unit',
            dataType: 'json',
            data: $("#frm_mutasi_unit").serialize(),
            success: function (data) {
                $("#dd_id_do_kpu").empty();
                if (data.st)
                {
                    $("#dd_id_do_kpu").select2({data: data.list_do_kpu});

                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }

    function RefreshKPU()
    {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/kpu/get_dropdown_kpu_for_unit',
            dataType: 'json',
            data: $("#frm_mutasi_unit").serialize(),
            success: function (data) {
                $("#dd_id_kpu").empty();
                if (data.st)
                {
                    $("#dd_id_kpu").select2({data: data.list_kpu});

                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }



    function GetOneSerialUnit() {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_one_serial',
            dataType: 'json',
            data: {
                id_unit_serial: $("#dd_id_unit_serial").val()
            },
            success: function (data) {
                if (data.st)
                {
                    $("#txt_nama_unit").val(data.obj.nama_unit);
                    $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                    $("#txt_nama_warna").val(data.obj.nama_warna);
                    $("#txt_nama_kategori").val(data.obj.nama_kategori);
                    $("#txt_vin").val(data.obj.vin);
                    $("#txt_nomor_buka_jual").val(data.obj.nomor_buka_jual);
                    $("#txt_no_prospek").val(data.obj.no_prospek);
                    $("#txt_nama_customer").val(data.obj.nama_customer);
                    $("#txt_vin_number").val(data.obj.vin_number);
                    $("#txt_engine_no").val(data.obj.engine_no);
                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }

    function ClearFormDetail()
    {
        $("#txt_nama_unit").val("");
        $("#txt_nama_type_unit").val("");
        $("#txt_nama_kategori").val("");
        $("#txt_top").val("");
        $("#txt_vin").val("");
        $("#txt_nama_warna").val("");
        jumlahgenerate = 0;
        $("#areado").html("");

    }

    function RefreshGrid()
    {
        table.fnClearTable();
        if (listdetail.length > 0)
        {
            table.fnDraw(true);
            table.fnAddData(listdetail);
        }

    }

    function deleterow(indrow) {
        RefreshGrid();
    }
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".icheck").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
<?php if (CheckEmpty(@$id_mutasi_unit)) { ?>
            $(".select2").select2();
<?php } else { ?>
            // $(".select2").select2({disabled: true});
            $(".select2").select2();
    <?php if (!CheckEmpty(@$is_view)) { ?>
                $("input").attr("readonly", true);
                $("textarea").attr("readonly", true);
    <?php } ?>
<?php } ?>
        $('#dd_id_customer_search').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            data: listdetail,
            columns: [
                {
                    data: "id_unit_serial",
                    title: "No",
                    orderable: false,

                    width: "10px",
                    className: "text-right"
                },
                {data: "nama_unit", orderable: false, title: "Nama Unit"},
                {data: "vin_number", orderable: true, title: "No Rangka"},
                {data: "engine_no", orderable: true, title: "No Mesin"},
                {data: "nomor_buka_jual", orderable: true, title: "Buka Jual"},
                {data: "nama_customer", orderable: false, title: "Nama Customer"},
                {data: "nama_pool", orderable: false, title: "Lokasi"},

                {data: "id_unit_serial",
                    mRender: function (data, type, row) {
                        return "<a onclick='hapusrow(\"" + data + "\")' style='float:right;' href='javascript:;' class='btn-xs btn-danger'  >Hapus</a>"

                    }},
            ],

            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });

        $("#btt_tambah_unit").click(function () {

            clearmessage();
            if (!CheckEmpty($("#dd_id_unit_serial").val()))
            {

                var res = alasql('SELECT * FROM ? where id_unit_serial=\'' + $("#dd_id_unit_serial").val() + '\'', [listdetail]);
                if (res.length > 0)
                {
                    messageerror("Data Unit Sudah ada dalam list do");
                } else
                {

                    $.ajax({
                        type: 'POST',
                        url: baseurl + 'index.php/unit/get_one_serial',
                        dataType: "json",
                        data: {
                            id_unit_serial: $("#dd_id_unit_serial").val()
                        },
                        success: function (data) {
                            if (data.st)
                            {
                                listdetail.push(data.obj);
                                RefreshGrid();
                                $("#dd_id_unit_serial").val(0).trigger("change");
                            }

                        },
                        error: function (xhr, status, error) {
                            messageerror(xhr.responseText);

                        }
                    });
                }

            } else
            {
                messageerror("Pilih Data Unit yang ingin dimasukkan");
            }
        })
        $("#dd_id_supplier_search").change(function () {
         
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/supplier/get_one_supplier',
                dataType: 'json',
                data: $("form#frm_mutasi_unit").serialize(),
                success: function (data) {
                    if (data.st)
                    {
                        $("#txt_atas_nama").val(data.obj.nama_supplier);
                        $("#txt_up").val(data.obj.up);
                        $("#txt_order_ket").val(data.obj.order);
                        $("#txt_alamat_kirim").val(data.obj.alamat);

                    } else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });


        })
        $("#dd_id_unit_search,#dd_id_prospek, #dd_id_kategori_search , #dd_id_customer_search , #dd_id_kpu , #dd_id_do_kpu , #dd_id_unit_serial").change(function () {
            var id = $(this).attr("id");
            switch (id) {
                case "dd_id_kategori_search":
                    $("#dd_id_unit_search").val(0).trigger("change.select2");
                    $("#dd_id_prospek").val(0).trigger("change.select2");
                    $("#dd_id_buka_jual").val(0).trigger("change.select2");
                    RefreshUnit();
                    RefreshProspek();
                    RefreshBukaJual();
                    RefreshDoKPU();
                    RefreshKPU();
                    RefreshSerialUnit();
                    break;
                case "dd_id_customer_search":
                    $("#dd_id_kpu").val(0).trigger("change.select2");
                    $("#dd_id_prospek").val(0).trigger("change.select2");
                    $("#dd_id_buka_jual").val(0).trigger("change.select2");
                    $("#dd_id_do_kpu").val(0).trigger("change.select2");
                    RefreshProspek();
                    RefreshBukaJual();
                    RefreshSerialUnit();
                    break;
                case "dd_id_unit_search":
                    $("#dd_id_kpu").val(0).trigger("change.select2");
                    $("#dd_id_prospek").val(0).trigger("change.select2");
                    $("#dd_id_buka_jual").val(0).trigger("change.select2");
                    $("#dd_id_do_kpu").val(0).trigger("change.select2");
                    RefreshProspek();
                    RefreshBukaJual();
                    RefreshKPU();
                    RefreshDoKPU();
                    RefreshSerialUnit();
                    break;
                case "dd_id_prospek":
                    $("#dd_id_buka_jual").val(0).trigger("change.select2");
                    RefreshBukaJual();
                    RefreshSerialUnit();
                    break;
                case "dd_id_kpu":
                    $("#dd_id_do_kpu").val(0).trigger("change.select2");
                    RefreshDoKPU();
                    RefreshSerialUnit();
                    break;
                case "dd_id_do_kpu":
                    RefreshSerialUnit();
                    break;
                case "dd_id_unit_serial":
                    GetOneSerialUnit();
                    break;


            }






        })

        $("#dd_id_prospek").change(function () {
            var id = $(this).attr("id");


        })
    })

    function hapusrow(id_unit_serial)
    {
        var index = listdetail.findIndex(function (o) {
            return o.id_unit_serial === id_unit_serial;
        })
        if (index !== -1)
            listdetail.splice(index, 1);
        RefreshGrid();
    }

    $("#frm_mutasi_unit").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data DO Prospek berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                // console.log($("#frm_mutasi_unit").serialize());return false;
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/mutasi_unit/mutasi_unit_manipulate',
                    dataType: 'json',
                    data: $("#frm_mutasi_unit").serialize() + "&" + $.param({dataset: listdetail}),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/mutasi_unit";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;
    })
</script>