<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mutasi_unit extends CI_Controller {

    private $_prefix = "_surat_jalan";

    function __construct() {
        parent::__construct();
        $this->load->model('m_mutasi_unit');
        $this->load->model("warna/m_warna");
        $this->load->model("vrf/m_vrf");
        $this->load->model("model/m_model");
        $this->load->model("unit/m_unit");
        $this->load->model("kategori/m_kategori");
        $this->load->model("pool/m_pool");
        $this->load->model("buka_jual/m_buka_jual");
        $this->load->model("prospek/m_prospek");
    }

    public function setunitpool($id_unit_serial) {

        $this->m_mutasi_unit->SetUnitSerialPool($id_unit_serial);
    }

    public function surat_perintah_mutasi_unit($id_mutasi_unit) {
        $mutasiunit = $this->m_mutasi_unit->GetOneMutasi($id_mutasi_unit);
        $this->load->model("user/m_user");
        $user = $this->m_user->GetOneUser(GetUserId(), 'id_user');
        $customer = "";
        foreach ($mutasiunit->listdetail as $detail) {
            if (strpos($customer, $detail->nama_customer) === false)
                $customer .= $detail->nama_customer . " , ";
        }

        if (strlen($customer) > 0) {
            $customer = substr($customer, 0, strlen($customer) - 2);
        }

        $mutasiunit->nama_customer = $customer;
        if ($user) {
            $mutasiunit->nama_user_login = $user->nama_user;
        } else {
            $mutasiunit->nama_user_login = "(Nama dan Cap)";
        }

        $this->load->view("v_mutasi_unit_surat_perintah", $mutasiunit);
    }

    public function shortcut_do_prospek($id_do_kpu) {
        $row = ['button' => 'Add'];
        $jenis_pembayaran = array();
        $javascript = array();
        $module = "K116";
        $header = "K059";
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("ekspedisi/m_ekspedisi");
        $this->load->model("supplier/m_supplier");
        $this->load->model("do_kpu/m_do_kpu");
        $this->load->model("kpu/m_kpu");

        $row['free_top_proposed'] = 0;
        $row['title'] = "Add Surat Jalan Mutasi";
        $row['tanggal_do'] = GetDateNow();
        $row['type_pembayaran'] = 2;
        $row['free_parking'] = AddDays(GetDateNow(), "5 days");
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['listdetail'] = array();
        $row['list_ekspedisi'] = $this->m_ekspedisi->GetDropDownEkspedisi();
        $row['list_pool'] = $this->m_pool->GetDropDownPool();
        $row['list_pool_tujuan'] = $this->m_pool->GetDropDownPool([1, 2, 3, 5, 6, 7, 8]);
        // $row['list_buka_jual'] =$this->m_buka_jual->GetDropDownBukaJual();
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_do_kpu'] = $this->m_do_kpu->GetDropDownDoKpuForUnit();
        $row['list_kpu'] = $this->m_kpu->GetDropDownKPUUnitSerial();
        $row['list_type_do'] = array(array('id' => 1, 'text' => 'Do Masuk'), array('id' => 2, 'text' => 'Do Keluar'));
        $row['type_do'] = 3;
        $this->load->model("mutasi_unit/m_mutasi_unit");
        $listunit = $this->m_mutasi_unit->GetUnitPendingFromDOKPU($id_do_kpu);
        $row['listdetail'] = $listunit;

        $dokpu = $this->m_do_kpu->GetOneDoKPU($id_do_kpu);
        if ($dokpu) {
            $row['id_pool_from'] = $dokpu->id_pool_from;
            $row['id_pool_to'] = $dokpu->id_pool_to;
            $row['id_supplier'] = $dokpu->id_supplier;
            $row['atas_nama'] = $dokpu->nama_supplier;
            $row['up'] = $dokpu->up;
            $row['alamat'] = $dokpu->alamat;
            $row['order'] = $dokpu->nama_supplier;
        }
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css[] = 'assets/plugins/iCheck/all.css';
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        LoadTemplate($row, 'mutasi_unit/v_mutasi_unit_manipulate', $javascript, $css);
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/plugins/dropzone/dropzone.min.js";
        $css[] = "assets/plugins/dropzone/dropzone.min.css";
        $module = "K116";
        $header = "K059";
        $title = "DO Prospek";
        $this->load->model("customer/m_customer");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_do", "text" => "Tanggal DO");
        $jenis_pencarian[] = array("id" => "tanggal_kirim", "text" => "Tanggal Kirim");
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "2", "text" => "Batal");
        $status[] = array("id" => "5", "text" => "Menunggu Approval");
        $model['start_date'] = GetCookieSetting("start_date" . $this->_prefix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $this->_prefix, GetDateNow());
        $model['list_pencarian'] = $jenis_pencarian;
        $model['value_status'] = GetCookieSetting("status" . $this->_prefix, GetCookieSetting("search_buka_jual") == "1" ? 0 : 7);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $this->_prefix);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_prefix, "nomor_buka_jual");
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PROSPEK");
        $jenis_keyword[] = array("id" => "nomor_buka_jual", "text" => "NO BUKA JUAL");
        $jenis_keyword[] = array("id" => "engine_no", "text" => "NO Mesin Kendaraan");
        $jenis_keyword[] = array("id" => "vin_number", "text" => "NO Rangka Kendaraan");
        $jenis_keyword[] = array("id" => "nomor_mutasi", "text" => "NO Mutasi");
        $model['list_keyword'] = $jenis_keyword;
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "mutasi_unit/v_mutasi_unit_index", $javascript, $css);
    }

    public function get_one_prospek() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Kpu', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_prospek->GetOneProspek($model["id_prospek"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Kpu tidak ditemukan dalam database" : "";
            if (CheckEmpty(@$model['typehtml'])) {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_content", $data['obj'], true);
            } else {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_hutang_content", $data['obj'], true);
            }
            echo json_encode($data);
        }
    }

    public function get_one_mutasi_unit() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_mutasi_unit', 'Do Prospek', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_mutasi_unit->GetOneDoProspek($model["id_mutasi_unit"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data DO Prospek tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function view_mutasi($id) {
        $row = $this->m_mutasi_unit->GetOneMutasi($id);
        if ($row) {
            $row->button = 'View';
            $this->load->model("customer/m_customer");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $this->load->model("ekspedisi/m_ekspedisi");
            $row = json_decode(json_encode($row), true);

            $row['list_ekspedisi'] = $this->m_ekspedisi->GetDropDownEkspedisi();
            $row['list_pool'] = $this->m_pool->GetDropDownPool();
            // $row['list_buka_jual'] =$this->m_buka_jual->GetDropDownBukaJual();
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_detail'][] = array("vin_number" => "", "vin" => "", "manufacture_code" => "", "engine_no" => "", "free_parking" => "");
            $row['list_type_do'] = array(array('id' => 1, 'text' => 'Do Masuk'), array('id' => 2, 'text' => 'Do Keluar'));
            $javascript = array();
            $module = "K116";
            $header = "K059";
            $row['title'] = "View Mutasi";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $arr_type_do = array('-', 'Masuk', 'Keluar', 'Customer');
            $row['nama_type_do'] = $arr_type_do[$row['type_do']];
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            // echo "<pre>";print_r($row);exit;
            LoadTemplate($row, 'mutasi_unit/v_mutasi_unit_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Buka Jual tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

    public function getdatamutasi() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
       
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_prefix, 1);
            }
            SetCookieSetting($key . $this->_prefix, $value);
        }
        echo $this->m_mutasi_unit->GetDataMutasi($params);
    }

    public function getdoprospek_api() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        // $from = 0;
        $count = true;
        $jumlah_data = $this->m_mutasi_unit->GetDoProspek_api($params, null, null, $count);

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/mutasi_unit/getdoprospek_api';
        $config['total_rows'] = count($jumlah_data);
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['num_links'] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $from = $page * $config['per_page'];
        $next_page = $page + 2;
        $data['data'] = $this->m_mutasi_unit->GetDoProspek_api($params, $config['per_page'], $from, false);
        $data['from'] = $from;
        $data['per_page'] = $config['per_page'];
        $data['next_page_url'] = $config['base_url'] . '/' . $next_page;
        $data['path'] = $config['base_url'];
        $data['total'] = $config['total_rows'];
        $data['current_page'] = $page + 1;
        $data['last_page'] = ceil($config['total_rows'] / $config['per_page']);

        return $this->sendResponse("Success", $data, 200);
    }

    public function sendResponse($msg, $data = null, $status) {
        echo json_encode([
            "msg" => $msg,
            "data" => $data,
            'api_token' => '',
            'expired_at' => ''
                ], $status);
    }

    public function mutasi_unit_change_status() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_mutasi', 'id DO Prospek', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_mutasi_unit->ChangeStatus($model["id_mutasi"], $model['status']);
            echo json_encode($data);
        }
    }

    public function getDataVrf() {
        $id_Vrf = $this->input->get('id_vrf');
        // header('Content-Type: application/json');
        $res = $this->m_vrf->GetOneVrf($id_Vrf);
        echo $json = json_encode($res);
    }

    public function get_buka_jual_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_buka_jual = $this->m_buka_jual->GetDropDownBukaJualFromCustomer($model);
        echo json_encode(array("list_buka_jual" => DefaultEmptyDropdown($list_buka_jual, "json", "Buka Jual"), "st" => true));
    }

    public function get_prospek_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_prospek = $this->m_prospek->GetDropDownProspekForBukaJual($model);
        echo json_encode(array("list_prospek" => DefaultEmptyDropdown($list_prospek, "json", "Prospek"), "st" => true));
    }

    public function create_mutasi_unit() {
        $row = ['button' => 'Add'];
        $jenis_pembayaran = array();
        $javascript = array();
        $module = "K116";
        $header = "K059";
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("ekspedisi/m_ekspedisi");
        $this->load->model("supplier/m_supplier");
        $this->load->model("do_kpu/m_do_kpu");
        $this->load->model("kpu/m_kpu");

        $row['free_top_proposed'] = 0;
        $row['title'] = "Add Surat Jalan Mutasi";
        $row['tanggal_do'] = GetDateNow();
        $row['type_pembayaran'] = 2;
        $row['free_parking'] = AddDays(GetDateNow(), "5 days");
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['listdetail'] = array();
        $row['list_ekspedisi'] = $this->m_ekspedisi->GetDropDownEkspedisi();
        $row['list_pool'] = $this->m_pool->GetDropDownPool();
        $row['list_pool_tujuan'] = $this->m_pool->GetDropDownPool([1, 2, 3, 5, 6, 7, 8]);
        // $row['list_buka_jual'] =$this->m_buka_jual->GetDropDownBukaJual();
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_do_kpu'] = $this->m_do_kpu->GetDropDownDoKpuForUnit();
        $row['list_kpu'] = $this->m_kpu->GetDropDownKPUUnitSerial();
        $row['list_detail'][] = array("vin_number" => "", "vin" => "", "manufacture_code" => "", "engine_no" => "", "free_parking" => "");
        $row['list_type_do'] = array(array('id' => 1, 'text' => 'Do Masuk'), array('id' => 2, 'text' => 'Do Keluar'));
        $row['type_do'] = 3;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css[] = 'assets/plugins/iCheck/all.css';
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        LoadTemplate($row, 'mutasi_unit/v_mutasi_unit_manipulate', $javascript, $css);
    }

    public function mutasi_unit_detail($id = 0) {
        $row = $this->m_mutasi_unit->GetOneDoProspek($id);

        if ($row) {
            $row = json_decode(json_encode($row), true);
            $module = "K077";
            $header = "K059";
            CekModule($module);
            // $this->db->where('dgmi_prospek_equipment.id_prospek', $id);
            // $this->db->where('dgmi_prospek_equipment.deleted_date', null);
            // $this->db->join('dgmi_unit', 'dgmi_unit.id_unit=dgmi_prospek_equipment.id_unit');
            // $this->db->select('dgmi_prospek_equipment.*, nama_unit');
            // $row['equipment'] = $this->db->get('dgmi_prospek_equipment')->result_array();

            echo json_encode(array('st' => true, 'msg' => '', 'data' => $row));
        } else {
            echo json_encode(array('st' => false, 'msg' => "Prospek cannot be found in database", 'data' => null));
        }
    }

    public function prospek_content() {
        $model = $this->input->post();
        $model['detail'] = array("vin_number" => "", "vin" => "", "manufacture_code" => "", "engine_no" => "", "free_parking" => "");
        $this->load->view("v_mutasi_unit_content", $model);
    }

    public function mutasi_unit_manipulate() {
        $message = '';
        $model = $this->input->post();

        // $this->form_validation->set_rules('id_prospek', 'KPU', 'trim');
        $this->form_validation->set_rules('id_pool', 'Pool', 'trim|required');
        $this->form_validation->set_rules('tanggal_do', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim');
        $this->load->model("prospek/m_prospek");
        $sisa = 0;
        $dataset = CheckArray($model, "dataset");
        $qtydo = 0;
        $islanjut = false;
        foreach ($dataset as $key => $detail) {
            if ($detail != "") {
                $islanjut = true;
                $serialunit = $this->m_unit->GetOneUnitSerial($detail['id_unit_serial']);
                if ($serialunit) {
                    if (!CheckEmpty($serialunit->id_do_prospek_detail)) {
                        $message .= "Unit sudah dikeluarkan dari database<br/>";
                    }
                }
            }
        }
        if (!$islanjut) {
            $message .= "Tidak ada unit yang dikeluarkan<br/>";
        }

        // if($sisa< DefaultCurrencyDatabase(count(@$model['id_serial_unit'])))
        // {
        //     $message.="QTY KPU tidak cukup untuk prospek ini<br/>";
        // }
        $model['dataset'] = $dataset;

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_mutasi_unit->MutasiUnitManipulate($model);
            echo json_encode($status);
        }
    }

    public function prospek_save_do() {
        $message = '';

        $tgl_do = $this->input->post('tgl_do');
        foreach ($tgl_do as $key => $value) {
            $data[] = array(
                'id_prospek' => $_POST['idprospek'],
                'tgl_do' => $_POST['tgl_do'][$key],
                'no_do' => $_POST['no_do'][$key],
                'manufacture_no' => $_POST['manufacture_no'][$key],
                'engine_no' => $_POST['engine_no'][$key],
                'created_by' => GetUserId(),
                'created_date' => GetDateNow(),
            );
        }
        $this->db->insert_batch('dgmi_do', $data);
        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function prospek_edit_do() {
        $message = '';
        $this->db->where('id_prospek', $_POST['idprospek']);
        $this->db->delete('dgmi_do');

        if ($this->input->post('tgl_do')) {
            $tgl_do = $this->input->post('tgl_do');
            foreach ($tgl_do as $key => $value) {
                $data[] = array(
                    'id_prospek' => $_POST['idprospek'],
                    'tgl_do' => $_POST['tgl_do'][$key],
                    'no_do' => $_POST['no_do'][$key],
                    'manufacture_no' => $_POST['manufacture_no'][$key],
                    'engine_no' => $_POST['engine_no'][$key],
                    'created_by' => GetUserId(),
                    'created_date' => GetDateNow(),
                );
            }
            $this->db->insert_batch('dgmi_do', $data);
        }

        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function edit_mutasi($id = 0, $no = "") {
        $row = $this->m_mutasi_unit->GetOneMutasi($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K116";
            $header = "K059";
            $row['title'] = "Edit Kpu";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->model("customer/m_customer");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $this->load->model("kpu/m_kpu");
            $this->load->model("ekspedisi/m_ekspedisi");
            $this->load->model("supplier/m_supplier");
            $row['title'] = "Update DO Prospek";
            $row['type_pembayaran'] = 2;
            $row['free_parking'] = AddDays(GetDateNow(), "5 days");
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_ekspedisi'] = $this->m_ekspedisi->GetDropDownEkspedisi();
            $row['list_pool'] = $this->m_pool->GetDropDownPool();

            $row['list_pool_tujuan'] = $this->m_pool->GetDropDownPool([1, 2, 3, 5, 6, 7, 8]);
            $params_prospek['id_kategori'] = $row['id_kategori'];
            $params_prospek['id_unit'] = $row['id_unit'];
            $params_prospek['id_type_unit'] = $row['id_type_unit'];
            $params_prospek['id_customer'] = $row['id_customer'];
            $row['list_prospek'] = $this->m_prospek->GetDropDownProspekForBukaJual($params_prospek);
            $params_buka_jual['id_prospek'] = $row['id_prospek'];
            $row['list_kpu'] = $this->m_kpu->GetDropDownKPUUnitSerial();
            $row['list_buka_jual'] = $this->m_buka_jual->GetDropDownBukaJualFromCustomer($params_buka_jual);
            $row['list_unit_serial'] = $this->m_unit->GetListUnitSerial($row['type_do'], $row['id_buka_jual'], $row['id_customer']);
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_type_do'] = array(array('id' => 1, 'text' => 'DO Masuk'), array('id' => 2, 'text' => 'DO Keluar'), array('id' => 3, 'text' => 'DO Customer'));
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
            $css[] = 'assets/plugins/iCheck/all.css';
            $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
            $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
            // echo "<pre>";
            // print_r($row);exit;
            LoadTemplate($row, 'mutasi_unit/v_mutasi_unit_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Kpu cannot be found in database");
            redirect(site_url('prospek'));
        }
    }

    public function edit_prospek($id = 0) {
        $row = $this->m_prospek->GetOneKpu($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K060";
            $header = "K059";
            $row['title'] = "Edit Kpu";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $jenis_pembayaran = array();
            $jenis_pembayaran[1] = "Tunai";
            $jenis_pembayaran[2] = "Kredit";
            $row['list_type_pembayaran'] = $jenis_pembayaran;
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $this->load->model("customer/m_customer");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_type_ppn'] = $list_type_ppn;
            $this->load->model("vrf/m_vrf");
            $vrf = $this->m_vrf->GetOneVrf($row['id_vrf']);
            $row['qty_sisa'] = $vrf->qty - $vrf->qty_total + $row['qty'];
            $list_type_pembulatan = array();
            $list_type_pembulatan[1] = "Sebelum PPN";
            $list_type_pembulatan[2] = "Sesudah PPN";
            $row['list_type_pembulatan'] = $list_type_pembulatan;
            $row['list_vrf'] = $this->m_vrf->GetDropDownVrf();
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_model'] = $this->m_model->GetDropDownModel();

            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'prospek/v_prospek_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Kpu cannot be found in database");
            redirect(site_url('prospek'));
        }
    }

    public function prospek_batal() {
        $message = '';
        $this->form_validation->set_rules('id_prospek', 'Kpu', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_prospek->KpuDelete($model['id_prospek']);
        }

        echo json_encode($result);
    }

    public function proses_mutasi_unit() {
        $id_mutasi = $this->input->get('id_mutasi');
        $row = ['button' => 'Add'];
        $javascript = array();
        $css = array();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $row['id_mutasi'] = $id_mutasi;
        $row['tanggal_proses'] = date('Y-m-d h:i:s');
        $this->load->view('mutasi_unit/v_proses_mutasi_unit', $row);
    }

    public function save_proses_mutasi_unit() {
        $model = $this->input->post();
        $result = $this->m_mutasi_unit->SaveProses($model);

        echo json_encode($result);
    }

    public function selesai_mutasi_unit() {
        $id_mutasi_unit = $this->input->get('id_mutasi');
        $row = ['button' => 'Add'];
        $javascript = array();
        $css = array();
        $javascript[] = "assets/plugins/select2/select2.js";
        $javascript[] = "assets/plugins/dropzone/dropzone.min.js";
        $css[] = "assets/plugins/dropzone/dropzone.min.css";
        $css[] = "assets/plugins/select2/select2.css";
        $row['id_mutasi'] = $id_mutasi_unit;
        $row['tanggal_selesai'] = date('Y-m-d h:i:s');
        $this->load->view('mutasi_unit/v_selesai_mutasi_unit', $row);
    }

    public function save_selesai_mutasi_unit() {
        $model = $this->input->post();
        $result = $this->m_mutasi_unit->SelesaiProses($model);

        echo json_encode($result);
    }

    function upload_foto() {

        $config['upload_path'] = FCPATH . '/upload/do-prospek/';
        $config['allowed_types'] = 'gif|jpg|png|ico';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $token = $this->input->post('token_foto');
            $id_mutasi_unit = $this->input->post('id_mutasi_unit');
            $nama = $this->upload->data('file_name');
            $file_path = FCPATH . 'upload/do-prospek/';
            // $image_path = FCPATH . 'upload/upload-foto/';
            $image_path = base_url('upload/do-prospek');
            $created_at = date('Y-m-d h:i:s');
            $this->db->insert('#_foto_mutasi_unit', array('id_mutasi_unit' => $id_mutasi_unit, 'filename' => $nama, 'file_path' => $file_path, 'image_path' => $image_path, 'created_at' => $created_at, 'token' => $token));
        }
    }

    function remove_foto() {
        $token = $this->input->post('token');
        $foto = $this->db->get_where('#_foto_mutasi_unit', array('token' => $token));

        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->filename;
            $st = false;
            if (file_exists($file = FCPATH . 'upload/do-prospek/' . $nama_foto)) {
                unlink($file);
                $del = $this->db->delete('#_foto_mutasi_unit', array('token' => $token));
                if ($del) {
                    $st = true;
                }
            }
        }
        // echo "{}";
        echo json_encode(array('msg' => $file, 'st' => $st));
    }

    function deleteFotoDoProspek() {
        $id = $this->input->post('id');

        $foto = $this->db->get_where('#_foto_mutasi_unit', array('id' => $id));
        if ($foto->num_rows() > 0) {
            $res = $foto->row();
            $nama_foto = $res->filename;
            $st = false;
            if (file_exists($file = FCPATH . 'upload/do-prospek/' . $nama_foto)) {
                unlink($file);
                $del = $this->db->query("delete from #_foto_mutasi_unit where id=$id");
                if ($del) {
                    $st = true;
                }
            }
        }
        echo json_encode(array('msg' => $id, 'st' => $st));
    }

    function print_mutasi($id_mutasi_unit) {
        $CI = get_instance();
        $print = $this->input->get('print');
        $data = $this->m_mutasi_unit->getPrint($id_mutasi_unit);
        $data = json_decode(json_encode($data), true);
        $dt['data'] = $data['mutasi'];
        $dt['data']['print'] = $print;
        $dt['data']['unit'] = $data['unit'];
        $CI->load->view('mutasi_unit/v_mutasi_unit_print', $dt);
    }

    public function view_mutasi_unit($id) {
        $row = $this->m_mutasi_unit->GetOneDoProspek($id);
        if ($row) {
            $row->button = 'View';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K068";
            $header = "K059";
            $row['title'] = "View DO Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $arr_type_do = array('-', 'Masuk', 'Keluar', 'Customer');
            $row['nama_type_do'] = $arr_type_do[$row['type_do']];
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            // echo "<pre>";print_r($row);exit;
            LoadTemplate($row, 'mutasi_unit/v_mutasi_unit_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Buka Jual tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

}

/* End of file Kpu.php */