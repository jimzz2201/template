<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_mutasi_unit extends CI_Model {

    public $table = '#_mutasi';
    public $id = 'id_mutasi';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    function ChangeStatus($id_mutasi, $status) {
        try {
            $message = "";
            // $do_prospek = $this->GetOneDoProspek($id_do_prospek);
            // $do_prospek = json_decode(json_encode($do_prospek), true);
            // $this->load->model("customer/m_customer");
            $this->load->model("do_kpu/m_do_kpu");
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $array = array("st" => true, "msg" => "Data Mutasi Berhasil " . ($status == "1" ? "Diapprove" : ($status == "2" ? "Direject" : "Diclose")));
            $update = array();
            $update['status'] = $status;
            $update['approved_date'] = GetDateNow();
            $update['approved_by'] = GetUserId();
            $update['updated_by'] = GetUserId();
            $update['updated_date'] = GetDateNow();
            
            $this->db->update("#_mutasi", $update, array("id_mutasi" => $id_mutasi));
            if($status=="5"|| $status=="2")
            {
                $this->db->delete("#_history_stok_unit",array("id_master_transaction"=>$id_mutasi,"module"=>"mutasi_unit"));
                $this->db->from("#_mutasi_detail");
                $this->db->join("#_unit_serial", "#_unit_serial.id_unit_serial=#_mutasi_detail.id_unit_serial");
                $this->db->join("#_do_kpu_detail", "#_do_kpu_detail.id_do_kpu_detail=#_unit_serial.id_do_kpu_detail");
                $this->db->where(array("id_mutasi" => $id_mutasi, "id_do_kpu !=" => null));
                $this->db->select("id_do_kpu,#_mutasi_detail.id_unit_serial");
                $this->db->distinct();
                $listiddokpu = $this->db->get()->result();

                foreach ($listiddokpu as $dokpustatusupdate) {
                    $this->m_do_kpu->UpdateDoKPU($dokpustatusupdate->id_do_kpu);
                    $this->SetUnitSerialPool($dokpustatusupdate->id_unit_serial);                    
                }
            }
            
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                $arrayreturn["st"] = true;
                $arrayreturn['msg']="Data Mutasi Sudah di" . (CheckEmpty(@$model['id_mutasi']) ? "masukkan" : "update") . " ke dalam database";
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    // datatables
    function GetDataMutasi($params) {
        $isdelete = GetGroupId()==1;
        $this->load->library('datatables');
        $this->datatables->select('#_mutasi.*,nama_ekspedisi,#_mutasi.id_mutasi,replace(#_mutasi.nomor_mutasi,\'/\',\'-\') as id,if(type_do=1,\'Masuk\',\'Keluar\') as type_do,nama_pool');
        $this->datatables->join("#_ekspedisi", "#_ekspedisi.id_ekspedisi=#_mutasi.id_ekspedisi","left");
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $extra=[];
        $where=[];
        if (@$params['jenis_keyword'] == "vin_number" || !CheckEmpty(@$params['id_customer']) || @$params['jenis_keyword'] == "engine_no")
        {
            $this->datatables->join("#_mutasi_detail", "#_mutasi_detail.id_mutasi=#_mutasi.id_mutasi");
            $this->datatables->join("#_unit_serial", "#_mutasi_detail.id_unit_serial=#_unit_serial.id_unit_serial");
            $this->datatables->join("#_buka_jual", "#_unit_serial.id_buka_jual=#_buka_jual.id_buka_jual","left");
            if( !CheckEmpty(@$params['id_customer']) )
            {
                 $where['#_buka_jual.id_customer']=@$params['id_customer'];
            }
           
        }
          
        $this->datatables->group_by("#_mutasi.id_mutasi");
        $this->datatables->join('#_pool', '#_pool.id_pool = #_mutasi.id_pool', 'left');
       
        if (!CheckEmpty(@$params['keyword'])) {
            $pencariankeyword = "nomor_mutasi";
            if (@$params['jenis_keyword'] == "nomor_mutasi") {
                $pencariankeyword = "#_mutasi.nomor_mutasi";
            } else if (@$params['jenis_keyword'] == "vin_number" || @$params['jenis_keyword'] == "engine_no") {
                $pencariankeyword = @$params['jenis_keyword'];
              
            }
            if($pencariankeyword=="engine_no"||$pencariankeyword=="vin_number")
            {
                $this->datatables->like($pencariankeyword, strtolower($params['keyword']), 'both'); 
            }
            else
            {
                  $where["lower(".$pencariankeyword.")"] = trim(strtolower($params['keyword']));
            }
        } else {
            $tanggal = "#_mutasi.tanggal_do";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_mutasi.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_mutasi.updated_date)";
            }
            
            $this->db->order_by($tanggal, "desc");


            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }
            


            if (!CheckEmpty($params['status'])) {
                if ($params['status'] == "6") {
                    $this->db->group_start();
                    $this->datatables->where("#_mutasi.status", 0);
                    $this->datatables->or_where("#_mutasi.status", 7);
                    $this->db->group_end();
                } else {
                    $where['#_mutasi.status'] = $params['status'];
                }
            }
            
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        $isedit = true;
        $straction = '';
        $strview = '';
        $stredit = '';
        $strapproved = '';
        $strrejected = '';
        $strprocess = '';
        $strselesai = '';
        $strbatal = '';
        $strprint = '';
        
        $strview .= anchor(site_url('mutasi_unit/view_mutasi/$1/$2'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $stredit .= anchor(site_url('mutasi_unit/edit_mutasi/$1/$2'), 'Update', array('class' => 'btn btn-primary btn-xs'));
            $strapproved .= anchor("", 'Approve', array('class' => 'btn btn-success btn-xs', "onclick" => "ubahstatus($1,1);return false;"));
            $strrejected .= anchor("", 'Reject', array('class' => 'btn btn-warning btn-xs', "onclick" => "ubahstatus($1,3);return false;"));
            $strprocess .= anchor("", 'Proses', array('class' => 'btn btn-warning btn-xs', "onclick" => "proses($1);return false;"));
            $strselesai .= anchor("", 'Selesai', array('class' => 'btn btn-warning btn-xs', "onclick" => "selesaiDo($1);return false;"));
            // $strprint .= anchor("", 'Cetak', array('class' => 'btn btn-print btn-xs', "onclick" => "printDo($1);return false;"));
            $strprint .= anchor(site_url('mutasi_unit/print_mutasi/$1'), 'Cetak', array('class' => 'btn btn-info btn-print btn-xs', 'target' => '_blank'));
            $strprint .= anchor("", 'Surat Perintah', array('class' => 'btn bg-blue btn-print btn-xs', 'onclick' => 'cetaksuratperintah($1);return false;'));

        }
        if ($isdelete) {
            $strbatal .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "ubahstatus($1,5);return false;"));
        }
	$this->db->order_by("#_mutasi.tanggal_do","desc");
        $this->datatables->add_column('edit', $stredit, 'id_mutasi,id');
        $this->datatables->add_column('view', $strview, 'id_mutasi,id');
        $this->datatables->add_column('batal', $strbatal, 'id_mutasi');
        $this->datatables->add_column('rejected', $strrejected, 'id_mutasi');
        $this->datatables->add_column('approved', $strapproved, 'id_mutasi');
        $this->datatables->add_column('process', $strprocess, 'id_mutasi');
        $this->datatables->add_column('selesai', $strselesai, 'id_mutasi');
        $this->datatables->add_column('cetak', $strprint, 'id_mutasi');
        return $this->datatables->generate();
    }

    function GetOneMutasi($keyword, $type = '#_mutasi.id_mutasi',$isfull=true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        if($isfull)
        {
            // $this->db->select('#_mutasi.*,nama_supplier,vrf_code,nama_unit,nama_type_unit,nama_kategori,#_vrf.qty as qty_total');
            $this->db->select('#_mutasi.*,nama_ekspedisi,cp_ekspedisi,pool_dari.nama_pool as asal_pool_from');
			
            $this->db->join("#_user","#_user.id_user=#_mutasi.approved_by","left");
			 $this->db->join("#_ekspedisi","#_ekspedisi.id_ekspedisi=#_mutasi.id_ekspedisi","left");
            $this->db->join("#_pool pool_dari","pool_dari.id_pool=#_mutasi.id_pool_from","left");
            $this->db->join("#_pool pool_tujuan","pool_tujuan.id_pool=#_mutasi.id_pool","left");
            
            
            
        }
        $mutasi_unit = $this->db->get($this->table)->row();
        if($isfull&&$mutasi_unit)
        {
            $this->db->select("#_unit_serial.*, nama_unit, nama_kategori, nama_type_unit,ifnull(#_customer.nama_customer,kpucustomer.nama_customer) as nama_customer, nama_pool,nomor_buka_jual");
            $this->db->from("#_mutasi_detail");
            $this->db->join("#_unit_serial", "#_unit_serial.id_unit_serial = #_mutasi_detail.id_unit_serial", "left");
            $this->db->join("#_buka_jual", "#_buka_jual.id_buka_jual = #_unit_serial.id_buka_jual", "left");
           
             $this->db->join("#_customer", "#_customer.id_customer = #_buka_jual.id_customer", "left");
           
            $this->db->join("#_warna","#_warna.id_warna=#_unit_serial.id_warna","left");
            $this->db->join("#_unit", "#_unit.id_unit = #_unit_serial.id_unit", "left");
            $this->db->join("#_kategori","#_kategori.id_kategori=#_unit.id_kategori","left");
            $this->db->join("#_type_unit","#_unit.id_type_unit=#_type_unit.id_type_unit","left");
            $this->db->join("#_mutasi", "#_mutasi.id_mutasi=#_mutasi_detail.id_mutasi", "left");
            $this->db->join("#_pool","#_pool.id_pool=#_unit_serial.id_pool","left");
            $this->db->join("#_kpu_detail","#_kpu_detail.id_kpu_detail=#_unit_serial.id_kpu_detail","left");
            $this->db->join("#_kpu","#_kpu.id_kpu=#_kpu_detail.id_kpu","left");
            $this->db->join("#_customer kpucustomer","kpucustomer.id_customer=#_kpu.id_customer","left");
            $this->db->where(array("#_mutasi_detail.id_mutasi"=>$mutasi_unit->id_mutasi,"#_unit_serial.status !="=>2));
            $list_detail=$this->db->get()->result();
            $mutasi_unit->listdetail=$list_detail;
        }
        
        return $mutasi_unit;
    }
    
    
    
    function GetUnitPendingFromDOKPU($id_do_kpu) {
        $this->db->from("#_unit_serial");
        $this->db->join("#_do_kpu_detail","#_unit_serial.id_do_kpu_detail=#_do_kpu_detail.id_do_kpu_detail");
        $this->db->join("#_do_kpu","#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu");
        $this->db->join("#_kpu","#_kpu.id_kpu=#_do_kpu.id_kpu","left");
        $this->db->join("#_customer","#_customer.id_customer=#_kpu.id_customer","left");

        $this->db->join("#_pool","#_pool.id_pool=#_unit_serial.id_pool");
        $this->db->join("#_unit","#_unit_serial.id_unit=#_unit.id_unit");
        $this->db->select("#_unit_serial.id_unit_serial,#_unit_serial.engine_no,#_unit_serial.vin_number,#_pool.nama_pool,nama_unit,'' as nomor_buka_jual,#_customer.nama_customer");
        $this->db->where(" not exists(select id_mutasi from #_mutasi_detail where #_mutasi_detail.id_unit_serial=#_unit_serial.id_unit_serial)");
        $this->db->where(array("#_unit_serial.id_do_prospek_detail"=>null,"#_do_kpu_detail.id_do_kpu"=>$id_do_kpu));
        $listunit=$this->db->get()->result_array();
        return $listunit;
    }

    function MutasiUnitManipulate($model) {
        try {
            $this->load->model("do_kpu/m_do_kpu");
            $message = "";
           
            $this->db->trans_rollback();
            $this->db->trans_begin();
            
            // $mutasi_unit['id_prospek'] = ForeignKeyFromDb($model['id_prospek']);
            $mutasi_unit['id_ekspedisi'] = ForeignKeyFromDb($model['id_ekspedisi']);
            $mutasi_unit['qty_do'] = count(@$model['dataset']);
            $mutasi_unit['keterangan'] = $model['keterangan'];
            $mutasi_unit['alamat'] = $model['alamat'];
            $mutasi_unit['up'] = $model['up'];
            $mutasi_unit['atas_nama'] = $model['atas_nama'];
            $mutasi_unit['alamat_pengambilan'] = $model['alamat_pengambilan'];
            $mutasi_unit['up_pengambilan'] = $model['up_pengambilan'];
            $mutasi_unit['atas_nama_pengambilan'] = $model['atas_nama_pengambilan'];
            $mutasi_unit['order_pengambilan'] = $model['order_pengambilan'];
            $mutasi_unit['tanggal_do'] = DefaultTanggalDatabase($model['tanggal_do']);
            $mutasi_unit['keterangan_kirim'] = $model['keterangan_kirim'];
            $mutasi_unit['order'] = $model['order'];
            $mutasi_unit['tujuan'] = $model['tujuan'];
            $mutasi_unit['type_do'] = $model['type_do'];
            $mutasi_unit['id_pool'] = $model['id_pool'];
            $mutasi_unit['id_pool_from'] = $model['id_pool_from'];
            $userid= GetUserId();
            $id_prospek=0;
            if (CheckEmpty(@$model['id_mutasi'])) {
                $mutasi_unit['created_date'] = GetDateNow();
                $mutasi_unit['created_by'] = ForeignKeyFromDb(GetUserId());
		$mutasi_unit['status'] =0;
                $mutasi_unit['nomor_mutasi'] = AutoIncrement('#_mutasi', ($mutasi_unit['type_do']=="1"?'MM/':"MK/") . date("y") . '/', 'nomor_mutasi', 5);
                $res = $this->db->insert($this->table, $mutasi_unit);
                $id_mutasi = $this->db->insert_id();
                
            } else {
                $mutasi_unit['updated_date'] = GetDateNow();
                $mutasi_unit['updated_by'] = ForeignKeyFromDb(GetUserId());
                $res = $this->db->update($this->table, $mutasi_unit, array("id_mutasi" => $model['id_mutasi']));
                $id_mutasi =$model['id_mutasi'];
                $mutasidb = $this->GetOneMutasi($id_mutasi,"id_mutasi",false);
                $mutasi_unit['nomor_mutasi'] = $mutasidb->nomor_mutasi;
                
            }
            
            $mutasi_unit_detail['id_mutasi'] = $id_mutasi;
            $mutasidetailid=[];
            $historylist=[];
            foreach($model['dataset'] as $detail)
            {
                $this->db->from("#_mutasi_detail");
                $this->db->where(array("id_mutasi"=> $id_mutasi,"id_unit_serial"=>$detail['id_unit_serial']));
                $mutasidetaildb = $this->db->get()->row();
                
                $id_mutasi_detail=0;
                if($mutasidetaildb)
                {
                    $id_mutasi_detail=$mutasidetaildb->id_mutasi_detail_unit;
                }
                else
                {
                    $mutasidetailinsert=[];
                    $mutasidetailinsert['id_unit_serial']=$detail['id_unit_serial'];
                    $mutasidetailinsert['id_mutasi']=$id_mutasi;
                    $this->db->insert("#_mutasi_detail",$mutasidetailinsert);
                    $id_mutasi_detail=$this->db->insert_id();
                    
                }
                $mutasidetailid[]=$id_mutasi_detail;
                
                
                $this->db->from("#_history_stok_unit");
                $this->db->where(array("ref_doc"=> $mutasi_unit['nomor_mutasi'],"id_unit_serial"=>$detail['id_unit_serial']));
                $historydetaildb = $this->db->get()->row();
                
                
                $data['id_unit_serial'] = $detail['id_unit_serial'];
                $data['ref_doc'] = $mutasi_unit['nomor_mutasi'];
                $data['id_pool'] = $mutasi_unit['id_pool'];
                $data['tanggal'] = $mutasi_unit['tanggal_do'];
                $data['keterangan'] = "Mutasi ".($mutasi_unit['type_do']=="1"?'Masuk':"Keluar");
                $data['id_master_transaction'] = $id_mutasi;
                $data['id_detail_transaction'] = $id_mutasi_detail;
                $data['module'] = "mutasi_unit";
                if (CheckEmpty($historydetaildb)) {
                    $data['created_by'] = GetUserId();
                    $data['created_date'] = GetDateNow();
                    $res = $this->db->insert('#_history_stok_unit', $data);
                    $historylist[] = $this->db->insert_id();
                } else {
                    $data['updated_by'] = GetUserId();
                    $data['updated_date'] = GetDateNow();
                    $this->db->update("#_history_stok_unit", $data, array("id_history_stok_unit" => $historydetaildb->id_history_stok_unit));
                    $historylist[] = $historydetaildb->id_history_stok_unit;
                }
                $this->SetUnitSerialPool($data['id_unit_serial']);

            }
            
            
            
            $this->db->where(array("ref_doc"=>$mutasi_unit['nomor_mutasi']));
            if(count($historylist)>0)
            {
                $this->db->where_not_in("id_history_stok_unit",$historylist);
            }
            $this->db->delete("#_history_stok_unit");
            
            $this->db->where(array("id_mutasi"=>$id_mutasi));
            if(count($historylist)>0)
            {
                $this->db->where_not_in("id_mutasi_detail_unit",$mutasidetailid);
            }
            $this->db->delete("#_mutasi_detail");
            
            $this->db->from("#_mutasi_detail");
            $this->db->join("#_unit_serial","#_unit_serial.id_unit_serial=#_mutasi_detail.id_unit_serial");
            $this->db->join("#_do_kpu_detail","#_do_kpu_detail.id_do_kpu_detail=#_unit_serial.id_do_kpu_detail");
            $this->db->where(array("id_mutasi"=>$id_mutasi,"id_do_kpu !="=>null));
            $this->db->select("id_do_kpu");
            $this->db->distinct();
            $listiddokpu=$this->db->get()->result();
       
            foreach($listiddokpu as $dokpustatusupdate)
            {
                $this->m_do_kpu->UpdateDoKPU($dokpustatusupdate->id_do_kpu);
            }
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data DO Prospek Sudah di" . (CheckEmpty(@$model['id_mutasi']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_prospek, $mutasi_unit['nomor_mutasi'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    function SetUnitSerialPool($id_unit_serial)
    {
        $this->db->from("#_history_stok_unit");
        $this->db->where(array("id_unit_serial"=>$id_unit_serial,"deleted_date"=>null));
        $this->db->order_by("tanggal","desc");
        $this->db->order_by("created_date","desc");
        $row=$this->db->get()->row();
       $this->db->update("#_unit_serial",array("id_pool"=>$row!=null?$row->id_pool:null),array("id_unit_serial"=>$id_unit_serial));
    }

    

    function GetDropDownKpu() {
        $listprospek = GetTableData($this->table, 'id_prospek', '', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listprospek;
    }

    function GetDoProspekByVrf ($id_vrf) {
        $this->db->select("#_mutasi.tanggal_do, nama_customer, nomor_master, nama_unit, nama_warna, vin_number, #_unit_serial.vin, engine_no,  #_mutasi.alamat, pengemudi");
        $this->db->where('#_vrf.id_vrf', $id_vrf);
        $this->db->where('#_vrf.deleted_date', null);
        $this->db->join('#_kpu_detail', '#_kpu_detail.id_vrf = #_vrf.id_vrf', 'left');
        $this->db->join('#_kpu', '#_kpu_detail.id_kpu = #_kpu.id_kpu', 'left');
        $this->db->join('#_do_kpu_detail', '#_do_kpu_detail.id_kpu_detail = #_kpu_detail.id_kpu_detail', 'left');
        $this->db->join('#_do_kpu', '#_do_kpu.id_do_kpu = #_do_kpu_detail.id_do_kpu', 'left');
        $this->db->join('#_unit_serial', '#_do_kpu_detail.id_do_kpu_detail = #_unit_serial.id_do_kpu_detail', 'left');
        $this->db->join('#_mutasi_detail', '#_unit_serial.id_mutasi_detail = #_mutasi_detail.id_mutasi_detail');
        $this->db->join('#_mutasi', '#_mutasi.id_mutasi = #_mutasi_detail.id_mutasi');
        $this->db->join('#_unit', '#_unit.id_unit = #_unit_serial.id_unit');
        $this->db->join('#_prospek', '#_mutasi_detail.id_prospek = #_prospek.id_prospek');
        $this->db->join('#_prospek_detail', '#_prospek_detail.id_prospek = #_prospek.id_prospek');
        $this->db->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna', 'left');
        $this->db->join('#_customer', '#_prospek.id_customer = #_customer.id_customer', 'left');
        $grid_do_kpu = $this->db->get('#_vrf')->result_array();
        return $grid_do_kpu;
    }

    function SaveProses ($model) {
        try {
            $mutasi_unit['pengemudi'] = $model['pengemudi'];
            $mutasi_unit['keterangan_proses'] = $model['keterangan_proses'];
            $mutasi_unit['tanggal_proses'] = $model['tanggal_proses'];
            $res = $this->db->update($this->table, $mutasi_unit, array("id_mutasi" => $model['id_mutasi']));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Berhasil memproses Mutasi Unit");
    }

    function SelesaiProses ($model) {
        try {
            $mutasi_unit['keterangan_selesai'] = $model['keterangan_selesai'];
            $mutasi_unit['tanggal_selesai'] = $model['tanggal_selesai'];
            $res = $this->db->update("#_mutasi", $mutasi_unit, array("id_mutasi" => $model['id_mutasi']));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "DO Prospek Selesai");
    }

    function getPrint ($id_mutasi) {
        $this->db->where('#_mutasi.id_mutasi', $id_mutasi);
        $this->db->join('#_ekspedisi', "#_ekspedisi.id_ekspedisi=#_mutasi.id_ekspedisi","left");
        $this->db->select('#_mutasi.*,#_ekspedisi.nama_ekspedisi as cp_ekspedisi');
        $result = $this->db->get('#_mutasi')->row();

        $this->db->where(array("#_mutasi_detail.id_mutasi"=>$id_mutasi) );
        $this->db->join('#_unit_serial', '#_mutasi_detail.id_unit_serial=#_unit_serial.id_unit_serial', 'left');
        $this->db->join('#_unit', '#_unit.id_unit=#_unit_serial.id_unit', 'left');
        $this->db->join('#_warna', '#_warna.id_warna=#_unit_serial.id_warna', 'left');
        $this->db->join('#_type_body', '#_type_body.id_type_body=#_unit.id_type_body', 'left');
        $this->db->join('#_type_unit', '#_type_unit.id_type_unit=#_unit.id_type_unit', 'left');
        $this->db->join('#_bast', '#_bast.id_unit_serial=#_unit_serial.id_unit_serial', 'left');
        $this->db->join('#_pool', '#_pool.id_pool=#_unit_serial.id_pool', 'left');
        $result2 = $this->db->get('#_mutasi_detail')->result();

        return array('mutasi' => $result, 'unit' => $result2);

    }

    function GetDropDownDoProspekForRetur($params) {
        $where = array('#_mutasi.status' => 1, $this->table . '.deleted_date' => null);
        
        if (!CheckEmpty(@$params['id_kategori']) || @$params['id_kategori'] > 0) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_unit']) || @$params['id_unit'] > 0) {
            $where['#_mutasi_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit']) || @$params['id_type_unit'] > 0) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer']) || @$params['id_customer'] > 0) {
            $where['#_mutasi.id_customer'] = $params['id_customer'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_mutasi_detail", "condition" => "#_mutasi_detail.id_mutasi=#_mutasi.id_mutasi");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_mutasi_detail.id_unit");
        $select = 'DATE_FORMAT(#_mutasi.tanggal_do, "%d %M %Y") as mutasi_date_convert,nomor_mutasi,nama_customer,nama_unit,#_mutasi.id_mutasi';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_mutasi.id_customer");
        $listdoprospek = GetTableData($this->table, 'id_mutasi', array('nomor_mutasi', "nama_customer", "nama_unit", "mutasi_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listdoprospek;
    }

}