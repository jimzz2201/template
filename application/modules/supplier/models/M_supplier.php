<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_supplier extends CI_Model {

    public $table = '#_supplier';
    public $id = 'id_supplier';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetListUnitSupplier($model) {
        $this->db->from("#_view_utang_detail");
        $params=[];
        if(!CheckEmpty(@$model['id_supplier']))
        {
            $params['id_supplier']=@$model['id_supplier'];
        }
        
        if(!CheckEmpty(@$model['invoice_number']))
        {
            $params['invoice_number']=@$model['invoice_number'];
        }
        
        if(!CheckEmpty(@$model['id_kpu']))
        {
            $params['id_kpu']=@$model['id_kpu'];
        }
        if(count($params>0))
        {
            $this->db->where($params);
        }
        
        $this->db->select("concat(nama_unit,' - vin : ',vin_number,' - engine no : ',engine_no) as text,id_unit_serial as id");
        $list_unit = $this->db->get()->result();
        return $list_unit;
    }
    
    function GetListInvoiceSupplier($model) {
        $this->db->from("#_view_utang_detail");
        $params=[];
        if(!CheckEmpty(@$model['id_supplier']))
        {
            $params['id_supplier']=@$model['id_supplier'];
        }
        
        
        if(!CheckEmpty(@$model['id_kpu']))
        {
            $params['id_kpu']=@$model['id_kpu'];
        }
        if(count($params>0))
        {
            $this->db->where($params);
        }
        
        $this->db->select("invoice_number as text,invoice_number as id");
        $this->db->distinct();
        $list_invoice = $this->db->get()->result();
        return $list_invoice;
    }
    

    // datatables
    function GetDatasupplier() {
        $this->load->library('datatables');
        $this->datatables->select('id_supplier,kode_supplier,nama_supplier,telp_supplier,email,alamat,nama_kota,kode_pos');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_kota', 'dgmi_supplier.id_kota = dgmi_kota.id_kota');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('supplier/edit_supplier/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletesupplier($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_supplier');
        return $this->datatables->generate();
    }

    // get all
    function GetOneSupplier($keyword, $type = 'id_supplier', $isfull = false) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $supplier = $this->db->get($this->table)->row();
        if ($supplier && $isfull) {
            $supplier->list_doc = $this->GetDocSupplier($supplier->id_supplier);
        }
        return $supplier;
    }

    public function GetDocSupplier($id_supplier) {
        $where = array('#_pembelian.status !=' => 2, "id_supplier" => $id_supplier, "tanggal_pelunasan" => null, '#_pembelian.deleted_date' => null);
        $arrayjoin = [];
        $this->db->order_by("jth_tempo","asc");
        $select = 'DATE_FORMAT(#_pembelian.tanggal_invoice, "%d %M %Y") as tanggal_invoice_convert,nomor_invoice,#_pembelian.id_pembelian,concat(#_pembelian.jumlah_unit," unit") as qty_unit,concat(\'Jth Tempo \',DATE_FORMAT(#_pembelian.jth_tempo, "%d %M %Y")) as tanggal_jatuh_tempo';
        //$this->db->group_by("#_kpu.id_kpu");
        return GetTableData("#_pembelian", 'id_pembelian', array('nomor_invoice', 'qty_unit',  "tanggal_invoice_convert",'tanggal_jatuh_tempo'), $where, "json", array(), array(), $arrayjoin, $select);
    }

    function SupplierManipulate($model) {
        try {
            $model['id_kota'] = ForeignKeyFromDb($model['id_kota']);

            if (CheckEmpty($model['id_supplier'])) {
                $model['kode_supplier'] = AutoIncrement("#_supplier", "S", "kode_supplier", 4);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Supplier successfull added into database');
                return array("st" => true, "msg" => "Supplier successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_supplier" => $model['id_supplier']));
                SetMessageSession(1, 'Supplier has been updated');
                return array("st" => true, "msg" => "Supplier has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownSupplier() {
        $listsupplier = GetTableData($this->table, 'id_supplier', 'nama_supplier', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listsupplier;
    }

    function SupplierDelete($id_supplier) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_supplier' => $id_supplier));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Supplier has been deleted from database");
    }

    function KoreksiSaldoHutang($supplier_id, $saldo_awal = null) {
        try {
            if ($saldo_awal == null) {
                $supplier = $this->GetOneSupplier($supplier_id);
                if ($supplier) {
                    $saldo_awal = $supplier->saldo_awal;
                }
            }
            $this->load->model("gl_config/m_gl_config");
            $akunglutang = $this->m_gl_config->GetIdGlConfig("utang", "", array());

            $this->db->trans_begin();
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglutang, "id_subject" => $supplier_id));
            $this->db->select("sum(kredit)-sum(debet) as totalutang");
            $totalutang = $this->db->get()->row()->totalutang;
            $hutang = DefaultCurrencyDatabase($saldo_awal);
            $hutang += CheckEmpty($totalutang) ? 0 : $totalutang;
            $update = array();
            $update['updated_date'] = GetDateNow();
            $update['updated_by'] = ForeignKeyFromDb(GetUserId());
            $update['hutang'] = $hutang;
            $update['saldo_awal'] = DefaultCurrencyDatabase($saldo_awal);
            $message = "";
            $this->db->update("#_supplier", $update, array("id_supplier" => $supplier_id));
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured";
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                $arrayreturn["msg"] = "Hutang dan Saldo Awal Supplier telah diupdate";
                $arrayreturn["st"] = true;
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

}
