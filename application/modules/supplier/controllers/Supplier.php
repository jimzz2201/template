<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supplier extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_supplier');
    }
    
    public function getlistunitsupplier(){
        $model=$this->input->post();
        $listunit=$this->m_supplier->GetListUnitSupplier($model);
        
        echo json_encode(array("st"=>true,"listunit"=>$listunit));
        
    }
    
    public function getlistinvoicesupplier()
    {
        $model=$this->input->post();
        $listinvoice=$this->m_supplier->GetListInvoiceSupplier($model);
        $listunit=$this->m_supplier->GetListUnitSupplier($model);
        echo json_encode(array("st"=>true,"listinvoice"=>$listinvoice,"listunit"=>$listunit));
        
        
    }
    
    

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K046";
        $header = "K001";
        $title = "Supplier";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "supplier/v_supplier_index", $javascript);
        
    } 
    public function get_one_supplier() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_supplier->GetOneSupplier($model["id_supplier"],"id_supplier",true);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Supplier tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatasupplier() {
        header('Content-Type: application/json');
        echo $this->m_supplier->GetDatasupplier();
    }
    
    public function create_supplier() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
        $module = "K046";
        $header = "K001";
        $row['title'] = "Add Supplier";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $this->load->model("kota/m_kota");
        $row['list_kota'] = $this->m_kota->GetDropDownKota();
	LoadTemplate($row,'supplier/v_supplier_manipulate', $javascript);       
    }
    
    public function supplier_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('nama_supplier', 'Nama Supplier', 'trim|required');
	$this->form_validation->set_rules('telp_supplier', 'Telp Supplier', 'trim|required');
	$this->form_validation->set_rules('email', 'Email', 'trim|required');
	$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
	$this->form_validation->set_rules('id_kota', 'Id Kota', 'trim|required');
	$this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_supplier->SupplierManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_supplier($id=0) 
    {
        $row = $this->m_supplier->GetOneSupplier($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K046";
            $header = "K001";
            $row['title'] = "Edit Supplier";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
            $this->load->model("kota/m_kota");
            $row['list_kota'] = $this->m_kota->GetDropDownKota();
	    LoadTemplate($row,'supplier/v_supplier_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Supplier cannot be found in database");
            redirect(site_url('supplier'));
        }
    }
    
    public function supplier_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_supplier->SupplierDelete($model['id_supplier']);
        }
        
        echo json_encode($result);
        
    }

    public function search_supplier() {
        $term = $this->input->post('term');
        $resp = [];
        if (strlen($term) >= 3) {
            $term = strtolower($term);
            $where = "(LOWER(nama_supplier) LIKE '%" . $term . "%') AND status = 1";
            $resp['results'] = GetTableData('#_supplier', 'id_supplier', ['kode_suppliser', 'nama_supplier'], $where);
        }
        echo json_encode($resp);
    }

}

/* End of file Supplier.php */