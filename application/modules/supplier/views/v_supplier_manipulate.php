<section class="content-header">
    <h1>
        Supplier <?= @$button ?>
        <small>Supplier</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Supplier</li>
        <li class="active">Supplier <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_supplier" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_supplier" value="<?php echo @$id_supplier; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Kode Supplier', "txt_kode_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $mergearray = array();
                                         $mergearray['readonly'] = "readonly";
                                        if (!CheckEmpty(@$id_supplier)) {
                                           
                                            $mergearray['name'] = "kode_supplier";
                                        } else {
                                            $mergearray['name'] = "kode_supplier";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_supplier, 'class' => 'form-control', 'id' => 'txt_kode_supplier', 'placeholder' => 'Kode Supplier'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Supplier', "txt_nama_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_supplier', 'value' => @$nama_supplier, 'class' => 'form-control', 'id' => 'txt_nama_supplier', 'placeholder' => 'Nama Supplier')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Telp Supplier', "txt_telp_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'telp_supplier', 'value' => @$telp_supplier, 'class' => 'form-control', 'id' => 'txt_telp_supplier', 'placeholder' => 'Telp Supplier')); ?>
                                    </div>
                                    <?= form_label('Email', "txt_email", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'email', 'value' => @$email, 'class' => 'form-control', 'id' => 'txt_email', 'placeholder' => 'Email')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('UP', "txt_id_kota", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => "up", 'value' => @$up, 'class' => 'form-control', 'id' => 'txt_up', 'placeholder' => 'UP')); ?>
                                    </div>
                                    <?= form_label('Order', "txt_kode_pos", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => "order", 'value' => @$order, 'class' => 'form-control', 'id' => 'txt_order', 'placeholder' => 'Order')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_alamat", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat', 'rows' => '3', 'cols' => '10', 'value' => @$alamat, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Kota', "txt_id_kota", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kota', 'class' => 'form-control', 'id' => 'dd_id_kota', 'placeholder' => 'kota'), DefaultEmptyDropdown($list_kota, "json", "Kota"), @$id_kota); ?>
                                    </div>
                                    <?= form_label('Kode Pos', "txt_kode_pos", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">

                                        <?= form_input(array('type' => 'text', 'name' => "kode_pos", 'value' => @$kode_pos, 'class' => 'form-control', 'id' => 'txt_kode_pos', 'placeholder' => 'Kode Pos')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'keterangan', 'rows' => '3', 'cols' => '10', 'value' => @$keterangan, 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Saldo Awal', "txt_saldo_awal", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'saldo_awal', 'value' => DefaultCurrency(@$saldo_awal), 'class' => 'form-control', 'id' => 'txt_saldo_awal', 'placeholder' => 'Saldo Awal', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Hutang', "txt_piutang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'hutang', 'value' => DefaultCurrency(@$hutang), 'class' => 'form-control', 'id' => 'txt_hutang', 'placeholder' => 'Hutang', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/supplier' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
    $(document).ready(function(){
        $("select").select2();
    })
    $("#frm_supplier").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/supplier/supplier_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/supplier';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>