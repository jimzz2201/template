
<section class="content-header">
    <h1>
        Supplier
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Supplier</li>
    </ol>


</section>
    
<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">

                <?php echo anchor(site_url('supplier/create_supplier'), 'Create', 'class="btn btn-success"'); ?>
	    </div>
        </div>
         <div class="row">
                <div class="col-md-12">
                    <div class="portlet-body form">
        <table class="table table-striped table-bordered table-hover" id="mytable">
	    
        </table>
        </div>
                </div>

            </div>
        </div>

</section>
        <script type="text/javascript">
             var table;
            function deletesupplier(id_supplier) {


        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/supplier/supplier_delete',
                dataType: 'json',
                data: {
                    id_supplier: id_supplier
                },
                success: function (data) {
                    if (data.st)
                    {
                        messagesuccess(data.msg);
                        table.fnDraw(false);
                    }
                    else
                    {
                        messageerror(data.msg);
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
       }});


    }
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                table = $("#mytable").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    processing: true,
                    serverSide: true,
                    scrollX: true,
                    ajax: {"url": "supplier/getdatasupplier", "type": "POST"},
                    columns: [
                        {
                           data: "id_supplier",
                           title: "No",
                           orderable: false,
                           "className" : "text-right",
                           width:"10px"
                        },
			{data: "kode_supplier" , orderable:false , title :"Kode Supplier"},
			{data: "nama_supplier" , orderable:false , title :"Nama Supplier"},
			{data: "telp_supplier" , orderable:false , title :"Telp Supplier"},
			{data: "email" , orderable:false , title :"Email"},
			{data: "alamat" , orderable:false , title :"Alamat"},
			{data: "nama_kota" , orderable:false , title :"Nama Kota"},
			{data: "kode_pos" , orderable:false , title :"Kode Pos"},
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center",
                            "title":"&nbsp;",
                            "width":"100px"
                        }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
    