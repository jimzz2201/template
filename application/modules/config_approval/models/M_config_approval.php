<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_config_approval extends CI_Model
{

    public $table = '#_config_approval';
    public $id = 'id_config';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDataconfig_approval() {
        $this->load->library('datatables');
        $this->datatables->select('id_config,nama_config,var_1,operation,var_2,approver,status,kode_form');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_config_approval.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('config_approval/edit_config_approval/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteconfig_approval($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_config');
        return $this->datatables->generate();
    }

    // get all
    function GetOneConfig_approval($keyword, $type = 'id_config') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $config_approval = $this->db->get($this->table)->row();
        return $config_approval;
    }

    function Config_approvalManipulate($model) {
        try {

            if (CheckEmpty($model['id_config'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		SetMessageSession(1, 'Config_approval successfull added into database');
		return array("st" => true, "msg" => "Config_approval successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_config" => $model['id_config']));
		SetMessageSession(1, 'Config_approval has been updated');
		return array("st" => true, "msg" => "Config_approval has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownConfig_approval() {
        $listconfig_approval = GetTableData($this->table, 'id_config', 'nama_config', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listconfig_approval;
    }

    function Config_approvalDelete($id_config) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_config' => $id_config));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Config_approval has been deleted from database");
    }


}