<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_do_kpu extends CI_Model {

    public $table = '#_do_kpu';
    public $id = 'id_do_kpu';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }
    
    function GetOutstandingKPUBlmFinished($params) {
        $where = array('#_kpu.status' => 1,'#_kpu.deleted_date' => null);
        $where['#_kpu.id_supplier'] = @$params['id_supplier'];

        if (!CheckEmpty(@$params['id_kategori'])) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_unit'])) {
            $where['#_kpu_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit'])) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer'])) {
            $where['#_kpu.id_customer'] = $params['id_customer'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_kpu_detail", "condition" => "#_kpu_detail.id_kpu=#_kpu.id_kpu");
        $arrayjoin[] = array("table" => "#_vrf", "condition" => "#_vrf.id_vrf=#_kpu_detail.id_vrf");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_kpu_detail.id_unit");
        $select = 'DATE_FORMAT(#_kpu.tanggal, "%d %M %Y") as kpu_date_convert,nomor_master,nama_customer,nama_unit,#_kpu.id_kpu';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_vrf.id_customer");
        $listkpu = GetTableData("#_kpu", 'id_kpu', array('nomor_master', "nama_customer", "nama_unit", "kpu_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listkpu;
    }
    
    

    function ReceiveDoKPU($model, $arrbast) {

        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $this->load->model("kpu/m_kpu");
            $this->load->model("gl_config/m_gl_config");
            $dokpu = $this->GetOneDoKpu($model['id_do_kpu']);
            $id_do_kpu = 0;
            $nomor_do_kpu = "";
            $arr_id_bast = [];

            $st = false;
            $msg = "";
            $userid = GetUserId();
            if (!$dokpu) {
                $msg .= "DO KPU tidak terdapat dalam database<br/>";
            } else {
                $id_do_kpu = $dokpu->id_do_kpu;
            }
            $dokpuupdate = array("status" => 4, "id_pool_to" => ForeignKeyFromDb(@$model['id_pool_to']));
            $dokpuupdate['tanggal_terima'] = DefaultTanggalDatabase(@$model['tanggal']);


            $this->db->update("#_do_kpu_detail", MergeUpdate(array("status" => 4, "qty_receive" => $dokpu->qty_do)), array("id_do_kpu" => $id_do_kpu));
            $this->db->update("#_do_kpu", MergeUpdate($dokpuupdate), array("id_do_kpu" => $id_do_kpu));
            $this->db->select("#_unit_serial.id_unit_serial");
            $this->db->from("#_unit_serial");
            $listid = $this->db->get()->result_array();

            $this->db->where_in("#_unit_serial.id_unit_serial", array_column($listid, "id_unit_serial"));
            $this->db->set(MergeUpdate(array("#_unit_serial.status" => 1, "#_unit_serial.id_pool" => ForeignKeyFromDb(@$model['id_pool_to']))));
            $this->db->update("#_unit_serial");
            $kpu = $this->m_kpu->GetOneKpu($dokpu->id_kpu);
            $arrakun = [];
            $akunglpersediaan = $this->m_gl_config->GetIdGlConfig("persediaan", $kpu->id_cabang, array("kategori" => $kpu->id_kategori, "unit" => $kpu->id_unit));
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglpersediaan, "dokumen" => $dokpu->nomor_do_kpu));
            $akunpersediaan = $this->db->get()->row();

            foreach ($arrbast as $detunit) {
                $this->db->from("#_bast");
                $this->db->where(array("id_unit_serial" => $detunit['id_unit_serial'], "doc_reference" => $dokpu->nomor_do_kpu, "status" => 1));
                $bastsatuan = $this->db->get()->row();
                $bast = [];
                $bast['ket_det'] = $detunit['ket_det'];
                $bast['pengecek'] = $detunit['pengecek'];
                $bast['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
                $bast['status'] = 1;
                $bast['ket_interior'] = $arrbast['ket_interior'];
                $bast['ket_eksterior'] = $arrbast['ket_eksterior'];
                $bast['interior'] = DefaultCurrencyDatabase($detunit['interior']);
                $bast['eksterior'] = DefaultCurrencyDatabase($detunit['eksterior']);
                $bast['km'] = DefaultCurrencyDatabase($detunit['km']);
                $bast['id_pool'] = ForeignKeyFromDb(@$model['id_pool_to']);
                $id_bast = 0;
                if ($bastsatuan) {
                    $id_bast = $bastsatuan->id_bast;
                    $this->db->update("#_bast", MergeUpdate($bast), array("id_bast" => $id_bast));
                } else {
                    $bast['kode_bast'] = AutoIncrement("#_bast", "BS" . date("y"), 'kode_bast', 5);
                    $bast['doc_reference'] = $dokpu->nomor_do_kpu;
                    $bast['type'] = 'DOKPU';
                    $bast['id_reference'] = $dokpu->id_do_kpu;
                    $bast['id_unit_serial'] = $detunit['id_unit_serial'];

                    $this->db->insert("#_bast", MergeCreated($bast));
                    $id_bast = $this->db->insert_id();
                }

                $this->db->from("#_bast_check");
                $this->db->where(array("id_bast" => $id_bast));
                $tempkelengkapan = $this->db->get()->result_array();
                /*
                  $arlengkapdb=[];
                  foreach($tempkelengkapan as $itemdetbast)
                  {
                  $arlengkapdb[$itemdetbast['id_bast_item']]= $itemdetbast;
                  }
                  $arridcheck=[];
                  foreach($detunit['listkelengkapan'] as $kelengkapan)
                  {
                  $detcheck=[];
                  $detcheck['value_det'] = $kelengkapan['value'];
                  $detcheck['ket_check'] = $kelengkapan['ket_check'];
                  $detcheck['status_det'] = 1;
                  $detcheck['id_bast'] = $id_bast;
                  $detcheck['id_bast_item'] = $kelengkapan['id'];
                  if(!CheckEmpty(@$arlengkapdb[$kelengkapan['id']]))
                  {
                  $arridcheck[]=$arlengkapdb[$kelengkapan['id']]['id_bast_check'];
                  $this->db->update("#_bast_check", MergeUpdate($detcheck),array("id_bast_check"=>$arlengkapdb[$kelengkapan['id']]['id_bast_check']));
                  }
                  else{
                  $this->db->insert("#_bast_check", MergeCreated($detcheck));
                  $arridcheck[]=$this->db->insert_id();
                  }

                  }
                  $this->db->where(array("id_bast" =>$id_bast));
                  if (count($arridcheck) > 0) {
                  $this->db->where_not_in("id_bast_check", $arridcheck);
                  }
                  $this->db->update("#_bast_check", MergeUpdate(array("status_det" => 5)));
                 */
                $this->db->from("#_history_stok_unit");
                $this->db->where(array("ref_doc" => $dokpu->nomor_do_kpu, "id_unit_serial" => $detunit['id_unit_serial']));
                $history_stock_unit = $this->db->get()->row();

                $hist['id_unit_serial'] = $detunit['id_unit_serial'];
                $hist['keterangan'] = 'Receive DO ' . $detunit['ket_det'];
                $hist['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
                $hist['id_bast'] = $id_bast;
                $hist['ref_doc'] = $dokpu->nomor_do_kpu;
                $hist['id_pool'] = ForeignKeyFromDb(@$model['id_pool_to']);
                $hist['module'] = "receive_do_kpu";
                $hist['id_master_transaction'] = $dokpu->id_do_kpu;
                $hist['id_detail_transaction'] = $dokpu->id_do_kpu_detail;
                if ($history_stock_unit) {
                    $this->db->update("#_history_stok_unit", MergeCreated($hist), array("id_history_stok_unit" => $history_stock_unit->id_history_stok_unit));
                } else {
                    $this->db->insert("#_history_stok_unit", MergeCreated($hist));
                }
            }



            if (CheckEmpty($akunpersediaan)) {
                $akunpersediaaninsert = array();
                $akunpersediaaninsert['id_gl_account'] = $akunglpersediaan;
                $akunpersediaaninsert['tanggal_buku'] = $dokpuupdate['tanggal_terima'];
                $akunpersediaaninsert['keterangan'] = "Penerimaan Unit  dari  " . @$dokpu->nama_supplier . ' untuk KPU : ' . $kpu->nomor_master;
                $akunpersediaaninsert['dokumen'] = $dokpu->nomor_do_kpu;
                $akunpersediaaninsert['subject_name'] = "";
                $akunpersediaaninsert['id_subject'] = null;
                $akunpersediaaninsert['debet'] = ($kpu->total - $kpu->nominal_ppn) / $kpu->qty * $dokpu->qty_do;
                $akunpersediaaninsert['kredit'] = 0;
                $akunpersediaaninsert['created_date'] = GetDateNow();
                $akunpersediaaninsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpersediaaninsert);
                $arrakun[] = $this->db->insert_id();
            } else {
                $akunpersediaanupdate['tanggal_buku'] = $dokpuupdate['tanggal_terima'];
                $akunpersediaanupdate['keterangan'] = "Penerimaan Unit  dari  " . @$dokpu->nama_supplier . ' untuk KPU : ' . $kpu->nomor_master;
                $akunpersediaanupdate['dokumen'] = $dokpu->nomor_do_kpu;
                $akunpersediaanupdate['subject_name'] = "";
                $akunpersediaanupdate['id_subject'] = null;
                $akunpersediaanupdate['debet'] = ($kpu->total - $kpu->nominal_ppn) / $kpu->qty * $dokpu->qty_do;
                $akunpersediaanupdate['kredit'] = 0;
                $akunpersediaanupdate['updated_date'] = GetDateNow();
                $akunpersediaanupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpersediaanupdate, array("id_buku_besar" => $akunpersediaan->id_buku_besar));
                $arrakun[] = $akunpersediaan->id_buku_besar;
            }


            $akunglpembelian = $this->m_gl_config->GetIdGlConfig("pembelian", $kpu->id_cabang, array("kategori" => $kpu->id_kategori, "unit" => $kpu->id_unit));
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglpembelian, "dokumen" => $dokpu->nomor_do_kpu));
            $akun_pembelian = $this->db->get()->row();

            if (CheckEmpty($akun_pembelian)) {
                $akunpembelianinsert = array();
                $akunpembelianinsert['id_gl_account'] = $akunglpembelian;
                $akunpembelianinsert['tanggal_buku'] = $dokpuupdate['tanggal_terima'];
                $akunpembelianinsert['keterangan'] = "Penerimaan Unit  dari  " . @$dokpu->nama_supplier . ' untuk KPU : ' . $kpu->nomor_master;
                $akunpembelianinsert['dokumen'] = $dokpu->nomor_do_kpu;
                $akunpembelianinsert['subject_name'] = "";
                $akunpembelianinsert['id_subject'] = null;
                $akunpembelianinsert['debet'] = 0;
                $akunpembelianinsert['kredit'] = ($kpu->total - $kpu->nominal_ppn) / $kpu->qty * $dokpu->qty_do;
                $akunpembelianinsert['created_date'] = GetDateNow();
                $akunpembelianinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpembelianinsert);
                $arrakun[] = $this->db->insert_id();
            } else {
                $akunpembelianupdate['tanggal_buku'] = $dokpuupdate['tanggal_terima'];
                $akunpembelianupdate['keterangan'] = "Penerimaan Unit  dari  " . @$dokpu->nama_supplier . ' untuk KPU : ' . $kpu->nomor_master;
                $akunpembelianupdate['dokumen'] = $dokpu->nomor_do_kpu;
                $akunpembelianupdate['subject_name'] = "";
                $akunpembelianupdate['id_subject'] = null;
                $akunpembelianupdate['debet'] = 0;
                $akunpembelianupdate['kredit'] = ($kpu->total - $kpu->nominal_ppn) / $kpu->qty * $dokpu->qty_do;
                $akunpembelianupdate['updated_date'] = GetDateNow();
                $akunpembelianupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpembelianupdate, array("id_buku_besar" => $akun_pembelian->id_buku_besar));
                $arrakun[] = $akun_pembelian->id_buku_besar;
            }


            $this->db->where(array("dokumen" => $dokpu->nomor_do_kpu));
            if (count($arrakun) > 0) {
                $this->db->where_not_in("id_buku_besar", $arrakun);
            }
            $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)));

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
            } else {
                $this->db->trans_commit();
               $st = true;
                SetPrint($id_do_kpu, $nomor_do_kpu, 'pembelian');
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            $msg = $ex->getMessage();
        }


        return array("st" => true && $msg == "", "msg" => $msg);
    }

    function GetListKelengkapan($type = "") {
        $this->db->from("#_bast_item");
        $this->db->where(array("status" => 1));
        $this->db->order_by("group");
        $this->db->order_by("urut");
        if ($type == "prospek")
            $this->db->where("default_do_prospek", 1);
        else
            $this->db->where("default_do_kpu", 1);
        return $this->db->get()->result();
    }

    // datatables
    function GetDataDokpu($params) {
        $this->load->library('datatables');
        $this->datatables->select('#_do_kpu.*,#_do_kpu.id_do_kpu as id,#_kpu.nomor_master,nama_unit,nama_warna,vrf_code,nama_supplier');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_do_kpu_detail', '#_do_kpu_detail.id_do_kpu = #_do_kpu.id_do_kpu', 'left');
        $this->datatables->join('#_kpu', '#_kpu.id_kpu = #_do_kpu.id_kpu', 'left');
        $this->datatables->join('#_kpu_detail', '#_kpu_detail.id_kpu = #_kpu.id_kpu', 'left');
        $this->datatables->join('#_vrf', '#_vrf.id_vrf = #_kpu_detail.id_vrf', 'left');
        $this->datatables->join('#_warna', '#_warna.id_warna = #_kpu_detail.id_warna', 'left');
        $this->datatables->join('#_unit', '#_unit.id_unit = #_do_kpu_detail.id_unit', 'left');
        $this->datatables->join('#_supplier', '#_supplier.id_supplier = #_do_kpu.id_supplier', 'left');
        $extra = [];
        $where = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $pencariankeyword = "nomor_master";

            if (@$params['jenis_keyword'] == "nomor_master") {
                $pencariankeyword = "#_kpu.nomor_master";
            } else if (@$params['jenis_keyword'] == "no_vrf") {
                $pencariankeyword = "#_vrf.vrf_code";
            } else if (@$params['jenis_keyword'] == "nomor_do_kpu") {
                $pencariankeyword = "#_do_kpu.nomor_do_kpu";
            } else if (@$params['jenis_keyword'] == "no_prospek" || @$params['jenis_keyword'] == "vin_number" || @$params['jenis_keyword'] == "engine_no") {
                if (@$params['jenis_keyword'] == "no_prospek") {
                    $pencariankeyword = "#_prospek.no_prospek";
                } else {
                    $pencariankeyword = @$params['jenis_keyword'];
                }

                $this->datatables->join("#_unit_serial", "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail");
                $this->datatables->join("#_prospek_detail", "#_unit_serial.id_prospek_detail=#_prospek_detail.id_prospek_detail","left");
                $this->datatables->join("#_prospek", "#_prospek.id_prospek=#_prospek_detail.id_prospek","left");
                $this->datatables->group_by("#_do_kpu.id_do_kpu");
            }





            if($pencariankeyword=="engine_no"||$pencariankeyword=="vin_number")
            {
                $this->datatables->like($pencariankeyword, strtolower($params['keyword']), 'both'); 
            }
            else
            {
                  $where["lower(".$pencariankeyword.")"] = trim(strtolower($params['keyword']));
            }
        } else {
            $tanggal = "date(#_do_kpu.tanggal_do)";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_do_kpu.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_do_kpu.updated_date)";
            } else if ($params['jenis_pencarian'] == "tanggal") {
                $tanggal = "#_do_kpu.tanggal_do";
            }
            $this->db->order_by($tanggal, "desc");
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }

            if (!CheckEmpty($params['status'])) {
                $where['#_kpu.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_supplier'])) {
                $where['#_kpu.id_supplier'] = $params['id_supplier'];
            }
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        $isedit = true;
        $isdelete = true;
        $straction = '';
        $strview = anchor(site_url('do_kpu/view_do_kpu/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $straction .= anchor(site_url('do_kpu/edit_do_kpu/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "bataldokpu($1);return false;"));
        }
        $this->datatables->add_column('view', $strview, 'id');
        $this->datatables->add_column('action', $straction, 'id');
        return $this->datatables->generate();
    }

    function GetUnitSerialFromDo($id_do_kpu) {
        $this->db->from("#_do_kpu");
        $this->db->join("#_do_kpu_detail", "#_do_kpu_detail.id_do_kpu=#_do_kpu.id_do_kpu");
        $this->db->where(array("#_do_kpu.id_do_kpu" => $id_do_kpu, "#_do_kpu.status !=" => 2, "#_do_kpu_detail.status !=" => 2));
        $this->db->join("#_unit_serial", "#_unit_serial.id_do_kpu_detail=#_do_kpu_detail.id_do_kpu_detail");
        $this->db->join("#_pool", "#_pool.id_pool=#_unit_serial.id_pool","left");
        $listunitserial = $this->db->get()->result_array();
        return $listunitserial;
    }

    // get all
    function GetOneDoKpu($keyword, $type = '#_do_kpu.id_do_kpu', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $this->db->join("#_do_kpu_detail", "#_do_kpu_detail.id_do_kpu=#_do_kpu.id_do_kpu");
        if ($isfull) {
            $this->db->select('#_supplier.alamat,pooldari.nama_pool as nama_pool_asal,poolke.nama_pool as nama_pool_tujuan,#_kpu.id_customer,#_unit.id_type_unit,#_kpu.nomor_master,#_do_kpu_detail.*,nama_warna,#_do_kpu.*,#_kpu_detail.vin,nama_customer,#_kpu.tanggal as "tanggal_kpu",nama_supplier,vrf_code,nama_unit,nama_type_unit,nama_kategori,#_kpu_detail.qty as qty_total');
            $this->db->join('#_kpu_detail', '#_kpu_detail.id_kpu_detail = #_do_kpu_detail.id_kpu_detail', 'left');
            $this->db->join('#_kpu', '#_kpu.id_kpu = #_kpu_detail.id_kpu', 'left');
            $this->db->join('#_customer', '#_customer.id_customer = #_kpu.id_customer', 'left');
            $this->db->join('#_warna', '#_kpu_detail.id_warna = #_warna.id_warna', 'left');
            $this->db->join('#_vrf', '#_vrf.id_vrf = #_kpu_detail.id_vrf', 'left');
            $this->db->join('#_unit', '#_unit.id_unit = #_do_kpu_detail.id_unit', 'left');
            $this->db->join('#_supplier', '#_kpu.id_supplier = #_supplier.id_supplier', 'left');
            $this->db->join('#_pool pooldari', 'pooldari.id_pool = #_do_kpu.id_pool_from', 'left');
            $this->db->join('#_pool poolke', 'poolke.id_pool = #_do_kpu.id_pool_to', 'left');
            $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
        }
		else
		{
			$this->db->select('#_do_kpu.*');
		}
        $do_kpu = $this->db->get($this->table)->row();
        if ($isfull && $do_kpu) {
            $this->db->from("#_unit_serial");
            $this->db->where(array("id_do_kpu_detail" => $do_kpu->id_do_kpu_detail, "#_unit_serial.status !=" => 2));
            $list_detail = $this->db->get()->result();
            $do_kpu->list_detail = $list_detail;
        }
        return $do_kpu;
    }

    function BatalDoKPU($id_do_kpu) {
        $dokpu = $this->GetOneDoKpu($id_do_kpu);
        $this->db->update("#_do_kpu_detail", MergeUpdate(array("status" => 2)), array("id_do_kpu" => $id_do_kpu));
        $this->db->update("#_do_kpu", MergeUpdate(array("status" => 2)), array("id_do_kpu" => $id_do_kpu));

        $this->db->from("#_unit_serial");
        $this->db->where(array("#_do_kpu_detail.id_do_kpu" => $id_do_kpu));
        $this->db->join("#_do_kpu_detail", "#_unit_serial.id_do_kpu_detail=#_do_kpu_detail.id_do_kpu_detail");
        $this->db->select("#_unit_serial.id_unit_serial");
        $listid = $this->db->get()->result_array();

        $this->db->where_in("#_unit_serial.id_unit_serial", array_column($listid, "id_unit_serial"));
        $this->db->set(MergeUpdate(array("#_unit_serial.status" => 2)));
        $this->db->update("#_unit_serial");
        $this->load->model("kpu/m_kpu");
        $this->m_kpu->ChangeStatusKPU($dokpu->id_kpu);

        return array("st" => true, "msg" => "Do Berhasil dibatalkan");
    }

    function GetDoKPUByKey($keyword, $type = "#_do_kpu.id_kpu") {
        $this->db->from("#_do_kpu");
        $this->db->join("#_do_kpu_detail", "#_do_kpu_detail.id_do_kpu=#_do_kpu.id_do_kpu");
        $this->db->where(array($type => $keyword, "#_do_kpu_detail.status !=" => 2, "#_do_kpu.status !=" => 2));
        $listdokpu = $this->db->get()->result();
        return $listdokpu;
    }

    function DO_KpuManipulate($model) {
        try {
            $jenis_pencarian='updated_date';
            $message = "";
            $this->load->model("kpu/m_kpu");
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $do_kpu['id_kpu'] = ForeignKeyFromDb($model['id_kpu']);

            $do_kpu['keterangan'] = $model['keterangan'];
            $do_kpu['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $do_kpu['tanggal_do'] = DefaultTanggalDatabase($model['tanggal']);
            $this->load->model("kpu/m_kpu");
            $do_kpu['free_parking'] = DefaultTanggalDatabase($model['free_parking']);
            $do_kpu['id_pool_from'] = ForeignKeyFromDb($model['id_pool_from']);
            $do_kpu['id_pool_to'] = ForeignKeyFromDb($model['id_pool_to']);

            $userid = GetUserId();
            $this->load->model("supplier/m_supplier");
            $supplier = $this->m_supplier->GetOneSupplier($do_kpu['id_supplier']);
            $kpu = $this->m_kpu->GetOneKpu($model['id_kpu']);
            if (!$kpu) {
                $message .= "KPU Tidak ditemukan dalam database";
            } else {
                if (CheckEmpty(@$do_kpu_detail['id_unit'])) {
                    $do_kpu_detail['id_unit'] = $kpu->id_unit;
                }
            }

            $id_do_kpu = 0;

            if (CheckEmpty(@$model['id_do_kpu'])) {
                $do_kpu_detail['qty_do'] = count(@$model['dataset']);
                $do_kpu_detail['qty_initial_do'] = count(@$model['dataset']);
                $do_kpu_detail['qty_receive'] = 0;
                $do_kpu_detail['status'] = 1;
                $do_kpu_detail['id_kpu_detail'] = $kpu->id_kpu_detail;

                $do_kpu_detail['id_unit'] = ForeignKeyFromDb($kpu->id_unit);
                $do_kpu['status'] = 0;


                $do_kpu['created_date'] = GetDateNow();
                $do_kpu['created_by'] = ForeignKeyFromDb(GetUserId());
                $do_kpu['nomor_do_kpu'] = AutoIncrement('#_do_kpu', 'DK/' . date("y") . '/', 'nomor_do_kpu', 5);
                $res = $this->db->insert($this->table, $do_kpu);
                $id_do_kpu = $this->db->insert_id();
            } else {
                $do_kpu['updated_date'] = GetDateNow();
                $do_kpu['updated_by'] = ForeignKeyFromDb(GetUserId());
                $res = $this->db->update($this->table, $do_kpu, array("id_do_kpu" => $model['id_do_kpu']));
                $id_do_kpu = $model['id_do_kpu'];
                $kpudb = $this->GetOneDoKpu($id_do_kpu, "#_do_kpu.id_do_kpu", false);
                $do_kpu['nomor_do_kpu'] = $kpudb->nomor_do_kpu;
            }

            $this->db->from("#_do_kpu_detail");
            $this->db->where("id_do_kpu", $id_do_kpu);
            $kpudetail = $this->db->get()->row();
            $id_do_kpu_detail = 0;
            if ($kpudetail) {
                $this->db->update("#_do_kpu_detail", MergeUpdate($do_kpu_detail), array("id_do_kpu_detail" => $kpudetail->id_do_kpu_detail));
                $id_do_kpu_detail = $kpudetail->id_do_kpu_detail;
            } else {
                $do_kpu_detail['id_do_kpu'] = $id_do_kpu;
                $this->db->insert("#_do_kpu_detail", MergeCreated($do_kpu_detail));
                $id_do_kpu_detail = $this->db->insert_id();
            }


            $listidhistory = [];
            foreach (@$model['dataset'] as $det) {
                $serialid = 0;
                $unitserial = [];
                $unitserial['id_unit'] = $do_kpu_detail['id_unit'];
                $unitserial['date_do_in'] = $do_kpu['tanggal_do'];
                $unitserial['id_do_kpu_detail'] = $id_do_kpu_detail;
                $unitserial['id_kpu_detail'] = $kpu->id_kpu_detail;
                $unitserial['vin_number'] = $det['vin_number'];
                $unitserial['vin'] = $model['vin'];
                $unitserial['engine_no'] = $det['engine_no'];
                $unitserial['nomor_do_supplier'] = $det['nomor_do_supplier'];
                $unitserial['invoice_number'] = $det['invoice_number'];
                $unitserial['id_warna'] = $kpu->id_warna;
                $unitserial['free_parking'] = $do_kpu['free_parking'];
                $unitserial['dnp'] = $kpu->price;
                $unitserial['discount'] = $kpu->disc1;
                $unitserial['status'] = 0;
                 if (CheckEmpty(@$det['id_unit_serial'])) {
                    $unitserial['id_pool'] = $do_kpu['id_pool_from'];
                    $unitserial['created_date'] = GetDateNow();
                    $unitserial['created_by'] = GetUserId();
                    $this->db->insert("#_unit_serial", $unitserial);
                    $serialid = $this->db->insert_id();
                } else {
                    $unitserial['updated_date'] = GetDateNow();
                    $unitserial['updated_by'] = GetUserId();
                    $this->db->update("#_unit_serial", $unitserial, array("id_unit_serial" => $det['id_unit_serial']));
                    $serialid = $det['id_unit_serial'];
                }

                $historystock = array("id_unit_serial" => $serialid, "module" => "do_kpu", "ref_doc" => $do_kpu['nomor_do_kpu']);

                $this->db->from("#_history_stok_unit");
                $this->db->where($historystock);
                $rowhistory = $this->db->get()->row();
                $historystock['id_pool'] = $do_kpu['id_pool_from'];
                $historystock['id_bast'] = null;
                $historystock['tanggal'] = $do_kpu['tanggal_do'];
                $historystock['keterangan'] = "Set Pangkalan Pertama";
                $historystock['id_master_transaction'] = $id_do_kpu;
                $historystock['id_detail_transaction'] = $id_do_kpu_detail;
                 if (CheckEmpty($rowhistory)) {
                    $historystock = MergeCreated($historystock);
                    $this->db->insert("#_history_stok_unit", $historystock);
                    $historystock[] = $this->db->insert_id();
                } else {
                    $historystock = MergeUpdate($historystock);
                    $this->db->update("#_history_stok_unit", $historystock, array("id_history_stok_unit" => $rowhistory->id_history_stok_unit));
                    $historystock[] = $rowhistory->id_history_stok_unit;
                };
            }
            $this->m_kpu->ChangeStatusKPU($kpu->id_kpu);
            $this->UpdateDoKPU($id_do_kpu);
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                ClearCookiePrefix("_dokpu", $jenis_pencarian);
                SetMessageSession(true, "Data KPU Sudah di" . (CheckEmpty(@$model['id_do_kpu']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_do_kpu, $do_kpu['nomor_do_kpu'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    function UpdateDoKPU($id_do_kpu)
    {
        $do_kpu=$this->GetOneDoKpu($id_do_kpu);
      
        if(@$do_kpu !=null&&$do_kpu->status!=2)
        {
            $this->db->from("#_do_kpu");
            $this->db->join("#_do_kpu_detail", "#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu");
            $this->db->join("#_unit_serial", "#_unit_serial.id_do_kpu_detail=#_do_kpu_detail.id_do_kpu_detail");
            $this->db->join("#_history_stok_unit", "#_history_stok_unit.id_unit_serial=#_unit_serial.id_unit_serial and #_history_stok_unit.module!='do_kpu' and #_history_stok_unit.deleted_date is null", "left");
            $this->db->group_by("#_unit_serial.id_unit_serial");
            $this->db->select("#_unit_serial.id_unit_serial,#_do_kpu.status as status_do_kpu,SUM(IF(#_history_stok_unit.id_history_stok_unit  is not null and #_history_stok_unit.tanggal is null, 1,0)) as pending_stok,SUM(IF(#_history_stok_unit.id_history_stok_unit is not null, 1,0)) as confirm_stok");
            $this->db->where(array("#_do_kpu.id_do_kpu" => $id_do_kpu));
            $unitserial = $this->db->get()->result();
            $countcomplete = 0;
            $countpending = 0;
            $status_do_kpu = 0;
           
            foreach ($unitserial as $unitdes) {
                $status_do_kpu = $unitdes->status_do_kpu;
                if ($unitdes->confirm_stok > 0) {
                    $countcomplete += 1;
                } else if ($unitdes->pending_stok > 0) {
                    $countpending += 1;
                }
            }
           
            $type = 0;
            if ($countcomplete == count($unitserial)) {
                $type = 4;
            } else if ($countpending > 0 || $countcomplete > 0) {
                $type = 1;
            }
            
            if ($status_do_kpu != $type) {
                $this->db->update("#_do_kpu", array("status" => $type), array("id_do_kpu" => $id_do_kpu));
            }
        }
    }
    
    
    
    

    function DeleteSerialDoKpu($id_unit_serial) {
        $this->db->update("#_unit_serial", array("id_kpu_detail" => null, "id_do_kpu_detail" => null, "deleted_date" => GetDateNow(), "deleted_by" => GetUserId()), array("id_unit_serial" => $id_unit_serial));
        return array("st" => true, "msg" => $this->db->last_query());
    }

    function GetDropDownDoKpuForUnit($param=[]) {
        
        if(!CheckEmpty(@$param['id_kpu']))
        {
            $where['#_kpu.id_kpu']=$param['id_kpu'];
        }
        if(!CheckEmpty(@$param['id_unit']))
        {
            $where['#_unit.id_unit']=$param['id_unit'];
        }
        if(!CheckEmpty(@$param['id_kategori']))
        {
            $where['#_unit.id_kategori']=$param['id_kategori'];
        }
        
        if(count($where)>0)
        {
            $this->db->where($where);
        }
        
        
        $this->db->join("#_kpu","#_kpu.id_kpu=#_do_kpu.id_kpu");
        $this->db->join("#_customer","#_kpu.id_customer=#_customer.id_customer","left");
        $this->db->join("#_kpu_detail","#_kpu.id_kpu=#_kpu_detail.id_kpu","left");
        $this->db->join("#_unit","#_kpu_detail.id_unit=#_unit.id_unit","left");
        $this->db->join("#_unit_serial","#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail","left");
        $this->db->where(array("id_do_prospek_detail"=>null));
        $this->db->group_by("id_do_kpu");
        $select="id_do_kpu,nomor_do_kpu,nomor_master,nama_customer,nama_unit,concat(#_kpu_detail.qty,' unit') as qty";
        $listkpu = GetTableData("#_do_kpu", 'id_do_kpu', array('nomor_do_kpu','nomor_master',"nama_customer","nama_unit","qty"), array( '#_do_kpu.status !=' => 2,  '#_do_kpu.deleted_date' => null),"json",[],[],[],$select);
        return $listkpu;
    }

    function GetDoKpuByVrf($id_vrf) {
        $this->db->where(array('#_do_kpu.status!=' => 2, '#_do_kpu_detail.status!=' => 2, '#_vrf.id_vrf' => $id_vrf));
        $this->db->join('#_kpu_detail', '#_kpu_detail.id_vrf = #_vrf.id_vrf', 'left');
        $this->db->join('#_kpu', '#_kpu.id_kpu = #_kpu_detail.id_kpu', 'left');
        $this->db->join('#_do_kpu_detail', '#_kpu_detail.id_kpu_detail = #_do_kpu_detail.id_kpu_detail', 'left');
        $this->db->join('#_do_kpu', '#_do_kpu.id_do_kpu = #_do_kpu_detail.id_do_kpu', 'left');
        $grid_do_kpu = $this->db->get('#_vrf')->result_array();
        return $grid_do_kpu;
    }

    function GetDropDownDoKpuForRetur($params) {
        $where = array('#_do_kpu.status' => 4, $this->table . '.deleted_date' => null);

        if (!CheckEmpty(@$params['id_kategori']) || @$params['id_kategori'] > 0) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_unit']) || @$params['id_unit'] > 0) {
            $where['#_do_kpu_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit']) || @$params['id_type_unit'] > 0) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_supplier']) || @$params['id_supplier'] > 0) {
            $where['#_do_kpu.id_supplier'] = $params['id_supplier'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_do_kpu_detail", "condition" => "#_do_kpu_detail.id_do_kpu=#_do_kpu.id_do_kpu");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_do_kpu_detail.id_unit");
        $select = 'DATE_FORMAT(#_do_kpu.tanggal_do, "%d %M %Y") as do_kpu_date_convert,nomor_do_kpu,nama_supplier,nama_unit,#_do_kpu.id_do_kpu';
        $arrayjoin[] = array("table" => "#_supplier", "condition" => "#_supplier.id_supplier=#_do_kpu.id_supplier");
        $listdokpu = GetTableData($this->table, 'id_do_kpu', array('nomor_do_kpu', "nama_supplier", "nama_unit", "do_kpu_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listdokpu;
    }

}
