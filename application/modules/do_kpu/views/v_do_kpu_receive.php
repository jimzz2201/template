<section class="content-header">
    <h1>
        Receive Delivery Order <?= @$button ?>
        <small>Delivery Order KPU</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Delivery Order KPU</li>
        <li class="active">KPU <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_kpu" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_do_kpu" value="<?php echo @$id_do_kpu; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        if (!CheckEmpty(@$id_supplier)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_supplier, 'name' => 'id_supplier'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_supplier, 'class' => 'form-control', 'id' => 'txt_nama_supplier', 'placeholder' => 'Supplier'));
                                        } else {
                                            echo form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier, array('class' => 'form-control select2dis', 'id' => 'dd_id_supplier'));
                                        }
                                        ?>
                                    </div>    
                                </div>

                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'readonly' => 'readonly', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <hr/>

                                <div class="form-group">
                                    <?= form_label('KPU', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        if (!CheckEmpty(@$id_kpu)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_kpu, 'name' => 'id_kpu'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_kpu), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'KPU'));
                                        } else {
                                            echo form_dropdown(array('type' => 'text', 'name' => 'id_kpu', 'class' => 'form-control select2', 'id' => 'dd_id_kpu', 'placeholder' => 'KPU'), DefaultEmptyDropdown(@$list_kpu, "json", "KPU"), @$id_kpu);
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>

                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Warna')); ?>


                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Qty Total', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => DefaultCurrency(@$qty_total), 'class' => 'form-control', 'id' => 'txt_qty_total', 'placeholder' => 'Qty Total', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Qty Sisa', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => DefaultCurrency(@$qty_sisa), 'class' => 'form-control', 'id' => 'txt_qty_sisa', 'placeholder' => 'Qty Sisa', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?php if (CheckEmpty(@$id_do_kpu)) { ?>
                                        <div class="col-sm-4" style="text-align: right">
                                            <?php echo anchor("", 'Ambil Semua', 'class="btn btn-warning" id="btt_ambil_semua"'); ?>
                                            <?php echo anchor("", 'Tambah', 'class="btn btn-success" id="btt_tambah"'); ?>
                                        </div>
                                    <?php } ?> 

                                </div>
                                <div class="form-group">
                                    <?= form_label('Tanggal Terima', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Pool', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_pool_to', 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Pool'), DefaultEmptyDropdown(@$list_pool, "json", "Pool"), @$id_pool_to); ?>
                                    </div>
                                </div>
                                <div id="areado" style="position:relative">

                                    <?php
                                    foreach (@$list_detail as $key => $detail) {
                                        ?>
                                        <hr/>
                                        <div style="position:relative">
                                            <div class="form-group">

                                                <h3 style="font-weight: bold;">Berita Acara Serah Terima Unit Ke : <?= $key + 1 ?></h3>
                                                <?= form_input(array('type' => 'hidden', 'name' => 'id_unit_serial[]', 'value' => @$detail['id_unit_serial'], 'id' => 'txt_id_unit_serial' . $key, 'placeholder' => 'Unit Serial')); ?>
                                                <h3 style="font-weight: bold;">Engine No : <span style="color:red"><?= @$detail['engine_no'] ?></span> | VIN Number : <span style="color:red"><?= @$detail['vin_number'] ?></span></h3>

                                            </div>
                                            <div class="form-group">

                                                <div class="col-sm-12" >
                                                    <table class="table table-striped table-bordered table-hover" >
                                                        <tr>
                                                            <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Penerima", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                            <td colspan="2"><?= form_input(array('type' => 'text', 'name' => 'pengecek[]', 'autocomplete' => 'off', 'value' => @$detail['pengecek'], 'class' => 'form-control', 'placeholder' => 'Pengecek')); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "KM", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                            <td colspan="2"><?= form_input(array('type' => 'text', 'name' => 'km[]', 'autocomplete' => 'off', 'value' => DefaultCurrency(@$detail['km']), 'class' => 'form-control', 'placeholder' => 'KM', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Interior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                            <td>
                                                                <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "10", "name" => "penilaian_interior[]", 'value' => DefaultCurrency(@$penilaian_interior), 'class' => 'form-control', 'id' => 'txt_range_penilaian_interior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Interior', "oninput" => "$('#txt_value_penilaian_interior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                            </td>
                                                            <td>
                                                                <?= form_input(array('type' => 'number', "name" => "penilaian_interior_value[]", 'min' => "0", 'max' => "10", 'value' => DefaultCurrency(@$penilaian_interior), 'class' => 'form-control', 'id' => 'txt_value_penilaian_interior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Interior', "oninput" => "$('#txt_range_penilaian_interior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Interior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '10', 'name' => 'ket_interior[]', 'autocomplete' => 'off', 'value' => @$detail['ket_interior'], 'class' => 'form-control', 'placeholder' => 'Keterangan Interior')); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Eksterior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                            <td>
                                                                <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "10", "name" => "penilaian_eksterior[]", 'value' => DefaultCurrency(@$penilaian_eksterior), 'class' => 'form-control', 'id' => 'txt_range_penilaian_eksterior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Eksterior', "oninput" => "$('#txt_value_penilaian_eksterior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                            </td>
                                                            <td>
                                                                <?= form_input(array('type' => 'number', "name" => "penilaian_eksterior_value[]", 'min' => "0", 'max' => "10", 'value' => DefaultCurrency(@$penilaian_eksterior), 'class' => 'form-control', 'id' => 'txt_value_penilaian_eksterior_' . @$detail['id_unit_serial'], 'placeholder' => 'Penilaian Eksterior', "oninput" => "$('#txt_value_penilaian_eksterior_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Eksterior", 'class' => 'form-control labeltextbox', 'placeholder' => 'Eksterior')); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '10', 'name' => 'ket_eksterior[]', 'autocomplete' => 'off', 'value' => @$detail['ket_eksterior'], 'class' => 'form-control', 'placeholder' => 'Keterangan Eksterior')); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:200px;"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Solar", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                            <td>
                                                                <?= form_input(array('type' => 'range', 'min' => "0", 'max' => "100", "name" => "solar[]", 'value' => DefaultCurrency(@$solar), 'class' => 'form-control', 'id' => 'txt_range_solar_' . @$detail['id_unit_serial'], 'placeholder' => 'Solar', "oninput" => "$('#txt_value_solar_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                            </td>
                                                            <td>
                                                                <?= form_input(array('type' => 'number', 'min' => "0", 'max' => "100", "name" => "solar_value[]", 'value' => DefaultCurrency(@$solar), 'class' => 'form-control', 'id' => 'txt_value_solar_' . @$detail['id_unit_serial'], 'placeholder' => 'Solar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);', "oninput" => "$('#txt_value_solar_" . @$detail['id_unit_serial'] . "').val(this.value);")); ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"><?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => "Keterangan Tambahan", 'class' => 'form-control labeltextbox', 'placeholder' => 'Pengecek')); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3"><?= form_textarea(array('type' => 'text', 'rows' => '20', 'name' => 'ket_det[]', 'autocomplete' => 'off', 'value' => @$detail['ket_det'], 'class' => 'form-control', 'placeholder' => 'Description')); ?></td>
                                                        </tr>


                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                    ?>

                                </div>
                                <hr/>

                                <?php if (CheckEmpty(@$is_view)) { ?>
                                    <div class="form-group" style="margin-top:50px">
                                        <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                    </div>
                                <?php } ?>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    $(document).ready(function () {
        $("input[type=radio]").iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue'
        });
        $(".select2").select2();
        $(".datepicker").datepicker();
    })




    $("#frm_kpu").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data receiving  berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_kpu/do_receive_manipulate',
                    dataType: 'json',
                    data: $("#frm_kpu").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/do_kpu";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })

</script>