<section class="content-header">
    <h1>
        Delivery Order KPU <?= @$button ?> <span style="color:red;font-weight:bold;font-style:italic"><?php echo @$nomor_do_kpu?></span>
        <small>Delivery Order KPU </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Delivery Order KPU</li>
        <li class="active">Delivery Order KPU <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_kpu" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_do_kpu" value="<?php echo @$id_do_kpu; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        if (!CheckEmpty(@$id_supplier)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_supplier, 'name' => 'id_supplier'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_supplier, 'class' => 'form-control', 'id' => 'txt_nama_supplier', 'placeholder' => 'Supplier'));
                                        } else {
                                            echo form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier, array('class' => 'form-control select2', 'id' => 'dd_id_supplier'));
                                        }
                                        ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Free Parking', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'free_parking', 'value' => DefaultDatePicker(@$free_parking), 'class' => 'form-control datepicker', 'id' => 'txt_free_parking', 'placeholder' => 'Free Parking')); ?>
                                    </div>
                                    <?= form_label('Unit Position', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "id_pool_from"), DefaultEmptyDropdown(@$list_pool, "json", "Pool"), @$id_pool_from, array('class' => 'form-control select2', 'id' => 'dd_id_pool_from')); ?>
                                    </div>
                                     <?= form_label('To', "txt_tgl_po", array("class" => 'col-sm-1 control-label','style'=>"text-align:center;")); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "id_pool_to"), DefaultEmptyDropdown(@$list_pool, "obj", "Pool"), @$id_pool_to, array('class' => 'form-control select2', 'id' => 'dd_id_pool_to')); ?>

                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Type Unit', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori_search', 'class' => 'form-control select2', 'id' => 'dd_id_kategori_search', 'placeholder' => 'Kategori'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$id_kategori); ?>
                                    </div>
                                    <?= form_label('Unit', "dd_id_unit_search", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit_search', 'class' => 'form-control select2', 'id' => 'dd_id_unit_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                                    </div>
                                    <?= form_label('Customer', "dd_id_customer_search", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_customer_search', 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('KPU', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        if (!CheckEmpty(@$id_kpu)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_kpu, 'name' => 'id_kpu'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nomor_master . ' - ' . @$nama_customer . ' - ' . @$nama_unit . ' - ' . DefaultTanggalIndo(@$tanggal_kpu), 'class' => 'form-control', 'id' => 'txt_kpu_code', 'placeholder' => 'KPU'));
                                        } else {
                                            echo form_dropdown(array('type' => 'text', 'name' => 'id_kpu', 'class' => 'form-control select2', 'id' => 'dd_id_kpu', 'placeholder' => 'KPU'), DefaultEmptyDropdown(@$list_kpu, "json", "KPU"), @$id_kpu);
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>

                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_warna, 'class' => 'form-control', 'id' => 'txt_nama_warna', 'placeholder' => 'Warna')); ?>


                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Qty Total', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => DefaultCurrency(@$qty_total), 'class' => 'form-control', 'id' => 'txt_qty_total', 'placeholder' => 'Qty Total', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Qty Sisa', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => DefaultCurrency(@$qty_sisa), 'class' => 'form-control', 'id' => 'txt_qty_sisa', 'placeholder' => 'Qty Sisa', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                     

                                </div>
                                <div id="headerdo" style="position:relative">
                                    <div class="form-group">
                                        <?= form_label('No', "txt_qty", array("class" => 'col-sm-2 control-label text-center')); ?>
                                        <?= form_label('Engine No', "txt_qty", array("class" => 'col-sm-3 control-label text-center')); ?>
                                        <?= form_label('Vin Number', "txt_qty", array("class" => 'col-sm-3 control-label text-center')); ?>
                                        <?= form_label('Nomor DO', "txt_qty", array("class" => 'col-sm-3 control-label text-center')); ?>
                                        <?= form_label('Action', "txt_qty", array("class" => 'col-sm-1 control-label text-center')); ?>
                                    </div>
                                </div>
                                
                                <div id="areado" style="position:relative">
                                    
                                    <?php
									
                                    foreach (@$list_detail as $key => $detail) {
                                        include APPPATH . 'modules/do_kpu/views/v_do_kpu_content.php';
                                    }
                                    ?>
                                </div>
                                <hr/>

                                <?php if (CheckEmpty(@$is_view)) { ?>
                                    <div class="form-group" style="margin-top:50px">
                                        <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                    </div>
                                <?php } ?>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;
    var jumlahgenerate =<?= count(@$list_detail) ?>;

    $("#btt_tambah").click(function () {
        AppendDetail(true);
    })
    
    

    function AppendDetail(isshow)
    {
        if (jumlahgenerate < $("#txt_qty_sisa").val())
        {


            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/do_kpu/kpu_content',
                data: {
                    key: jumlahgenerate,
                    jumlah: $("#txt_qty_sisa").val() - jumlahgenerate
                },
                success: function (data) {
                    jumlahgenerate = $("#txt_qty_sisa").val();
                    $("#areado").append(data);
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);

                }
            });
        } else
        {
            if (isshow)
                alert("Smua Field DO untuk KPU ini sudah terpenuhi");
        }
    }
    $("#btt_ambil_semua").click(function () {

        for (i = jumlahgenerate; i < $("#txt_qty_sisa").val(); i++) {
            AppendDetail(false);
        }


    })
    function RefreshUnit()
    {
        var id_kategori = $("#dd_id_kategori_search").val();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_dropdown_unit',
            dataType: 'json',
            data: {
                id_kategori: id_kategori
            },
            success: function (data) {
                $("#dd_id_unit_search").empty();
                if (data.st)
                {
                    $("#dd_id_unit_search").select2({data: data.list_unit})
                }
                ClearFormDetail();
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });

    }
    function UpdateKPU()
    {

        $("#dd_id_kpu").change(function () {
            var id_kpu = $("#dd_id_kpu").val();

            if (id_kpu != 0)
            {
                LoadBar.show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/kpu/get_one_kpu',
                    dataType: 'json',
                    data: {
                        id_kpu: id_kpu
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            $("#txt_nama_unit").val(data.obj.nama_unit);
                            $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                            $("#txt_nama_kategori").val(data.obj.nama_kategori);
                            $("#txt_vin").val(data.obj.vin);
                            $("#txt_qty_total").val(Comma(data.obj.qty));
                            $("#txt_qty_sisa").val(Comma(data.obj.qty - data.obj.qty_kirim));
                            $("#txt_nama_warna").val(data.obj.nama_warna);
                            $("#areado").html("");
                            jumlahgenerate = 0;
                            AppendDetail(false);

                        } else
                        {
                            ClearFormDetail();
                        }
                        LoadBar.hide();
                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                        ClearFormDetail();
                        LoadBar.hide();
                    }
                });
            } else
            {
                ClearFormDetail();
                LoadBar.hide();
            }
        })
    }
    function ClearFormDetail()
    {
        $("#txt_nama_unit").val("");
        $("#txt_nama_type_unit").val("");
        $("#txt_nama_kategori").val("");
        $("#txt_top").val("");
        $("#txt_vin").val("");
        $("#txt_nama_warna").val("");
        jumlahgenerate = 0;
        $("#areado").html("");

    }

    function RefreshGrid()
    {


    }

    function deleterow(indrow) {
        RefreshGrid();
    }
    $(document).ready(function () {

<?php if (CheckEmpty(@$id_do_kpu)) { ?>
            $(".select2").select2();
<?php } else { ?>
            $(".select2").select2({disabled: true});
            $("#dd_id_pool_from , #dd_id_pool_to").select2({disabled: false});
    <?php if (!CheckEmpty(@$is_view)) { ?>
                $("input").attr("readonly", true);
                $("textarea").attr("readonly", true);
    <?php } ?>
<?php } ?>
    $('#dd_id_customer_search').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $("#dd_id_supplier , #dd_id_unit_search, #dd_id_kategori_search , #dd_id_customer_search").change(function () {
            var id = $(this).attr("id");
            var id_unit_search = $("#dd_id_unit_search").val();
            if (id == "dd_id_kategori_search")
            {
                id_unit_search = 0;
                RefreshUnit();

            }
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/do_kpu/get_outstanding_kpu_belum_finished',
                dataType: 'json',
                data: {
                    id_supplier: $("#dd_id_supplier").val(),
                    id_kategori: $("#dd_id_kategori_search").val(),
                    id_unit: id_unit_search,
                    id_customer: $("#dd_id_customer_search").val()
                },
                success: function (data) {
                    $("#dd_id_kpu").empty();
                    if (data.st)
                    {
                        $("#dd_id_kpu").select2({data: data.list_kpu});
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        })


    })



    $(document).ready(function () {
        $(".datepicker").datepicker();

        UpdateKPU();
    })
    function DeleteDOKPUSerial(id_unit_serial)
    {
        swal({
            title: "Apakah kamu yakin ingin membatalkan registasi unit berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_kpu/deleteserialdokpu',
                    dataType: 'json',
                    data: {
                        id_unit_serial:id_unit_serial
                    },
                    success: function (data) {
                        if (data.st)
                        {
                           $(".areadetail"+id_unit_serial).remove();
                        } 
                        else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;
    }


    $("#frm_kpu").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data kpu berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_kpu/do_kpu_manipulate',
                    dataType: 'json',
                    data: $("#frm_kpu").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/do_kpu";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })

</script>