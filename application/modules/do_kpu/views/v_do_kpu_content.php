
<?php
for ($i = 0; $i < $jumlah; $i++) {
    $mergearray = [];
    if (!CheckEmpty(@$detail['id_prospek_detail']) || !CheckEmpty(@$detail['id_buka_jual'])) {
        $mergearray['readonly'] = 'readonly';
    }
    ?>
    <hr class="areadetail<?php echo @$detail['id_unit_serial'] ?>" />
    <div class="areadetail<?php echo @$detail['id_unit_serial'] ?>" style="position:relative">
        <div class="form-group">
            <?= form_input(array('type' => 'hidden', 'name' => 'id_unit_serial[]', 'value' => @$detail['id_unit_serial'], 'id' => 'txt_id_unit_serial' . $key, 'placeholder' => 'Unit Serial')); ?>
            <?= form_label('Unit Ke ' . ($key + $i + 1), "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
             <div class="col-sm-3">
                <?= form_input(array_merge($mergearray, array('type' => 'text', 'name' => 'engine_no[]', 'value' => @$detail['engine_no'], 'class' => 'form-control', 'id' => 'txt_engine_no' . $key, 'placeholder' => 'Engine NO'))); ?>
            </div>
            <div class="col-sm-3">
                <?= form_input(array_merge($mergearray, array('type' => 'text', 'name' => 'vin_number[]', 'value' => @$detail['vin_number'], 'class' => 'form-control', 'id' => 'txt_vin_number' . $key, 'placeholder' => 'VIN Number'))); ?>
            </div>
            <div class="col-sm-3">
                <?= form_input(array_merge($mergearray, array('type' => 'text', 'name' => 'nomor_do_supplier[]', 'value' => @$detail['nomor_do_supplier'], 'class' => 'form-control', 'id' => 'txt_nomro_do_supplier' . $key, 'placeholder' => 'Nomor Do Supplier'))); ?>
            </div>
         
            <div class="col-sm-1">
                <?php if (!CheckEmpty(@$detail['id_unit_serial']) && count($mergearray) == 0 && @$button != "view") { ?>
                    <button type="button" class="btn btn-danger btn-block" onclick="DeleteDOKPUSerial(<?php echo @$detail['id_unit_serial'] ?>);" id="btt_delete<?php echo @$detail['id_unit_serial'] ?>" >Delete</button>
                <?php } ?>

            </div>
        </div>
    </div>

<?php } ?>
