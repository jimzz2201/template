
<section class="content-header">
    <h1>
        Delivery Order KPU
        <small>Data Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">Delivery Order KPU</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), @$list_pencarian); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                        </div>


                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_supplier', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'selected' => @$status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                        </div>



                    </div>
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_keyword', 'selected' => @$jenis_keyword, 'class' => 'form-control select2', 'id' => 'dd_jenis_keyword', 'placeholder' => 'Jenis Keyword'), @$list_keyword); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                    </div>


                </div>



            </form>
            <hr/>
            <div class=" headerbutton">

                <?php echo anchor(site_url('do_kpu/create_do_kpu'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    $("#dd_jenis_keyword").change(function () {

        LoadAreaKeyword();
    })
    function LoadAreaKeyword()
    {

        if (CheckEmpty($("#dd_jenis_keyword").val()))
        {
            $("#txt_keyword").val("");
            $("#txt_keyword").attr("readonly", "readonly");
        } else
        {
            $("#txt_keyword").removeAttr("readonly");
        }
    }
    function bataldokpu(id_do_kpu) {


        swal({
            title: "Apakah kamu ingin membatalkan Data Do KPU berikut?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, batalkan!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_kpu/do_kpu_batal',
                    dataType: 'json',
                    data: {
                        id_do_kpu: id_do_kpu
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }
    function RefreshGrid()
    {
        table.fnDraw(false);
    }
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "do_kpu/getdatadokpu", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_do_kpu",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal_do", orderable: false, title: "Tanggal", "className": "text-center",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
                {data: "nomor_do_kpu", orderable: false, title: "Nomor DO", "width": "100px"},
                {data: "nomor_master", orderable: false, title: "KPU", "width": "100px"},
                {data: "nama_supplier", orderable: false, title: "Supplier"},
                {data: "nama_unit", orderable: false, title: "Unit", className: "text-center"},
                {data: "tanggal_terima", orderable: false, title: "Tanggal Terima", "className": "text-right",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},

                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        if (data == 0)
                            return "Pending";
                        else if (data == 1)
                            return "Proses";
                        else if (data == 2)
                            return "Batal";
                        else if (data == 4)
                            return "Finished";
                        else
                            return "";
                    }},
                {
                    "data": "action",
                    "orderable": false,
                    "width": "200px",
                    "className": "text-center",
                    mRender: function (data, type, row) {
                        var returnhtml = row['view'];
                        if (row['status'] == 0)
                        {
                            returnhtml = returnhtml + data;
                        }
                        return returnhtml;
                    },
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
