<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Do_kpu extends CI_Controller {

     private $_sufix = "_dokpu";
    function __construct() {
        parent::__construct();
        $this->load->model('m_do_kpu');
        $this->load->model("warna/m_warna");
        $this->load->model("vrf/m_vrf");
        $this->load->model("model/m_model");
    }
    
    public function get_outstanding_kpu_belum_finished() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_kpu = $this->m_do_kpu->GetOutstandingKPUBlmFinished($model);
        echo json_encode(array("list_kpu" => DefaultEmptyDropdown($list_kpu, "json", "KPU"), "st" => true));
    }
    
    

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K061";
        $header = "K059";
        $title = "DO KPU";
        $this->load->model("supplier/m_supplier");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $jenis_pencarian = [];
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_vrf", "text" => "NO VRF");
        $jenis_keyword[] = array("id" => "nomor_master", "text" => "NO KPU");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PPROSPEK");
        $jenis_keyword[] = array("id" => "nomor_do_kpu", "text" => "NO DO KPU");
        $jenis_keyword[] = array("id" => "engine_no", "text" => "NO Mesin Kendaraan");
        $jenis_keyword[] = array("id" => "vin_number", "text" => "NO Rangka Kendaraan");
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_do", "text" => "Tanggal DO");
        $model['start_date'] = GetCookieSetting("start_date" . $this->_sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $this->_sufix, GetDateNow());
        $model['list_status'] = $status;
        $model['status'] = GetCookieSetting("status" . $this->_sufix, 0);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_sufix, "nomor_master");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_keyword'] = $jenis_keyword;
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $this->_sufix, "created_date");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "do_kpu/v_do_kpu_index", $javascript,$css);
    }

    public function get_one_kpu() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_kpu', 'Kpu', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_kpu->GetOneKpu($model["id_kpu"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Kpu tidak ditemukan dalam database" : "";
            if(CheckEmpty(@$model['typehtml']))
            {
                $data['html']=$this->load->view("pembayaran/v_pembayaran_content",$data['obj'],true);
            }
            else
            {
                $data['html']=$this->load->view("pembayaran/v_pembayaran_hutang_content",$data['obj'],true);
            }
            echo json_encode($data);
        }
    }

    public function get_one_do_kpu() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_do_kpu', 'Do Kpu', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_do_kpu->GetOneDoKpu($model["id_do_kpu"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data DO Kpu tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdatadokpu() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
         foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_sufix, 1);
            }
            SetCookieSetting($key . $this->_sufix, $value);
        }
        echo $this->m_do_kpu->GetDataDokpu($params);
       
    }

    public function getDataVrf() {
        $id_Vrf = $this->input->get('id_vrf');
        // header('Content-Type: application/json');
        $res = $this->m_vrf->GetOneVrf($id_Vrf);
        echo $json = json_encode($res);
    }
    
    public function do_receive_manipulate(){
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('id_do_kpu', 'Do KPU', 'trim|required');
        $this->form_validation->set_rules('id_pool_to', 'Pool', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        
        $this->load->model("do_kpu/m_do_kpu");
        $sisa=0;
        $dokpu = $this->m_do_kpu->GetOneDoKpu(@$model['id_do_kpu']);
        if(!$dokpu)
        {
             $message.="DO KPU Tidak ditemukan dalam database<br/>";
        }
        else if($dokpu->status==4)
        {
            $message.="DO KPU Sudah diterima<br/>";
        }
        else if($dokpu->status==2)
        {
            $message.="DO KPU Sudah dibatalkan<br/>";
        }
        else if($dokpu->status==5)
        {
            $message.="DO KPU Sudah diclose<br/>";
        }
        $list_unit_serial= CheckArray($model, 'id_unit_serial');
        
        if(count($list_unit_serial)==0)
        {
            $message.="Tidak ada item yang dipilih<br/>";
        }
        $kelengkapantemp= $this->m_do_kpu->GetListKelengkapan();
        $interior = CheckArray($model, 'penilaian_interior');
        $ket_interior= CheckArray($model, 'ket_interior');
        $eksterior = CheckArray($model, 'penilaian_eksterior');
        $ket_eksterior= CheckArray($model, 'ket_eksterior');
        $solar = CheckArray($model, 'solar');
        $ket_det = CheckArray($model, 'ket_det');
        $km = CheckArray($model, 'km');
        $pengecek = CheckArray($model, 'pengecek');
        $listkelengkapan=[];
        foreach($kelengkapantemp as $item)
        {
            $listkelengkapan[$item->id_bast_item]=$item->nama_bast_item;
        }
        $arrbast=[];
        foreach($list_unit_serial as $keyunit=>$unitsatuan)
        {
            $itembast=[];
            $itembast['listkelengkapan']= [];
            $itembast['id_unit_serial']=$unitsatuan;
            $itembast['interior'] = @$interior[$keyunit];
            $itembast['eksterior'] = @$eksterior[$keyunit];
            $itembast['solar'] = @$solar[$keyunit];
            $itembast['ket_interior'] = @$ket_interior[$keyunit];
            $itembast['ket_eksterior'] = @$ket_eksterior[$keyunit];
            $itembast['ket_det'] = @$ket_det[$keyunit];
            $itembast['km'] = DefaultCurrencyDatabase(@$km[$keyunit]);
            $itembast['pengecek'] = @$pengecek[$keyunit];
            
            /*if(CheckEmpty(@$itembast['km']))
            {
                $message.="KM unit ke ".($keyunit+ 1).' tidak boleh kosong<br/>';
            }
            /*foreach($listkelengkapan as $key=>$item)
            {
                if(!CheckKey($model, "item_".$key."_".$unitsatuan))
                {
                    $message.=$item. " pada unit ke ".($keyunit + 1)." tidak boleh kosong<br/>";
                }
                else
                {
                    $det=[];
                    $det['id']=$key;
                    $det['value']=$model["item_".$key."_".$unitsatuan];
                    $det['ket_check'] =@$model["keterangan_".$key."_".$unitsatuan];
                    $itembast['listkelengkapan'][]=$det;
                }
            }*/
            $arrbast[]=$itembast;
        }
        
        
      
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_do_kpu->ReceiveDoKPU($model,$arrbast);
            echo json_encode($status);
        }
    }
    
    
    
    public function receive_do_kpu($id_do_kpu){
         $this->load->model("do_kpu/m_do_kpu");
        $row = $this->m_do_kpu->GetOneDoKpu($id_do_kpu);
        $row = json_decode(json_encode($row), true);
       
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $javascript = array();
        $module = "K061";
        $header = "K059";
        $this->load->model("supplier/m_supplier");
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $row['free_top_proposed'] = 0;
        $row['title'] = "Add KPU";
        $row['tanggal'] = GetDateNow();
        $row['type_pembayaran']=2;
        $row['kelengkapan']=$this->m_do_kpu->GetListKelengkapan();
        CekModule($module);
        $row['form'] = $header;
        $row['jumlah'] = 1;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_detail'] =$this->m_do_kpu->GetUnitSerialFromDo($id_do_kpu);
        $this->load->model("pool/m_pool");
        $row['list_pool']=$this->m_pool->GetDropDownPool([],[]);
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = 'assets/plugins/iCheck/icheck.min.js';
        $css[] = 'assets/plugins/iCheck/all.css';
        
        LoadTemplate($row, 'do_kpu/v_do_kpu_receive', $javascript, $css);
        
        
    }
    
    
    public function do_kpu_batal(){
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('id_do_kpu', 'Do KPU', 'trim|required');
        $this->load->model("do_kpu/m_do_kpu");
        $sisa=0;
        $dokpu = $this->m_do_kpu->GetOneDoKpu(@$model['id_do_kpu']);
		
        IF(!$dokpu)
        {
             $message.="DO KPU Tidak ditemukan dalam database<br/>";
        }
        else if($dokpu->status==1)
        {
            $message.="DO KPU Sudah diterima<br/>";
        }
        else if($dokpu->status==2)
        {
            $message.="DO KPU Sudah dibatalkan<br/>";
        }
        else if($dokpu->status==5)
        {
            $message.="DO KPU Sudah diclose<br/>";
        }
        
      
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_do_kpu->BatalDoKPU($model['id_do_kpu']);
            echo json_encode($status);
        }
    }
    public function get_dropdown_do_kpu_for_unit() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_do_kpu = $this->m_do_kpu->GetDropDownDoKpuForUnit($model);
        echo json_encode(array("list_do_kpu" => DefaultEmptyDropdown($list_do_kpu, "json", "DO KPU"), "st" => true));
    }
    
    
    
    public function create_do_kpu() {
        $row = ['button' => 'Add'];
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $this->load->model("pool/m_pool");
        $row['id_pool_from']=4;
        $row['id_pool_to']=0;
        $row['list_pool'] =$this->m_pool->GetDropDownPool(null,array(2,3));
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $javascript = array();
        $module = "K061";
        $header = "K059";
        $this->load->model("supplier/m_supplier");
        $this->load->model("customer/m_customer");
        $this->load->model("kategori/m_kategori");
        $this->load->model("unit/m_unit");
        $row['free_top_proposed'] = 0;
        $row['title'] = "Add DO KPU";
        $row['tanggal'] = GetDateNow();
        $row['type_pembayaran']=2;
        $row['free_parking'] = AddDays(GetDateNow(), "5 days");
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['is_pool_edit']= CheckEmpty(@$row['tanggal_terima']);
        $row['list_detail']=array();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'do_kpu/v_do_kpu_manipulate', $javascript, $css);
    }
    public function kpu_content()
    {
        $model=$this->input->post();
        $model['detail']=array("vin_number"=>"","engine_no"=>"","free_parking"=>"");
        $this->load->view("v_do_kpu_content",$model);
    }
    public function do_kpu_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('id_kpu', 'KPU', 'trim|required');
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'trim|required');
        $this->form_validation->set_rules('id_pool_from', 'Position dari', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('free_parking', 'Tanggal', 'trim|required');
        $this->load->model("kpu/m_kpu");
        $sisa=0;
        $kpu = $this->m_kpu->GetOneKpu(@$model['id_kpu']);
        IF($kpu)
        $sisa = $kpu->qty - $kpu->qty_kirim;
        $dataset=[];
        $qtydo=0;
        if(!CheckEmpty(@$model['vin_number']))
        {
            foreach($model['vin_number'] as $key=>$detail)
            {
                if($detail!="")
                {
                    $qtydo+=1;
                    array_push($dataset, array("vin_number"=>$detail
                            ,"id_unit_serial"=>@$model['id_unit_serial'][$key]
                            ,"engine_no"=>@$model['engine_no'][$key]
                            ,"nomor_do_supplier"=>@$model['nomor_do_supplier'][$key]
                            ,"invoice_number"=>@$model['invoice_number'][$key]
                            // ,"manufacture_code"=>@$model['manufacture_code'][$key]
                            // ,"no_bpkb"=>@$model['engine_no'][$key]
                            // ,"no_polisi"=>@$model['no_polisi'][$key]
                            // ,"vin"=>@$model['vin'][$key]
                            ));
                }
            }
        }
        
        if(count($dataset)==0)
        {
            $message.="Tidak Ada Unit yang diregistrasikan<br/>";
        }
        /*
        if(!CheckEmpty(@$model['id_do_kpu']))
        {
            $do_kpu=$this->m_do_kpu->GetOneDoKpu(@$model['id_do_kpu']);
            if($kpu)
            {
                $sisa+=$kpu->qty_do;
            }
        }
         
         */
        if($sisa< DefaultCurrencyDatabase(@$model['qty']))
        {
            $message.="QTY KPU tidak cukup untuk kpu ini<br/>";
        }
        $model['dataset']=$dataset;
      
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_do_kpu->DO_KpuManipulate($model);
            echo json_encode($status);
        }
    }

    public function kpu_save_do() {
        $message = '';

        $tgl_do = $this->input->post('tgl_do');
        foreach ($tgl_do as $key => $value) {
            $data[] = array(
                'id_kpu' => $_POST['idkpu'],
                'tgl_do' => $_POST['tgl_do'][$key],
                'no_do' => $_POST['no_do'][$key],
                'manufacture_no' => $_POST['manufacture_no'][$key],
                'engine_no' => $_POST['engine_no'][$key],
                'created_by' => GetUserId(),
                'created_date' => GetDateNow(),
            );
        }
        $this->db->insert_batch('dgmi_do', $data);
        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function kpu_edit_do() {
        $message = '';
        $this->db->where('id_kpu', $_POST['idkpu']);
        $this->db->delete('dgmi_do');

        if ($this->input->post('tgl_do')) {
            $tgl_do = $this->input->post('tgl_do');
            foreach ($tgl_do as $key => $value) {
                $data[] = array(
                    'id_kpu' => $_POST['idkpu'],
                    'tgl_do' => $_POST['tgl_do'][$key],
                    'no_do' => $_POST['no_do'][$key],
                    'manufacture_no' => $_POST['manufacture_no'][$key],
                    'engine_no' => $_POST['engine_no'][$key],
                    'created_by' => GetUserId(),
                    'created_date' => GetDateNow(),
                );
            }
            $this->db->insert_batch('dgmi_do', $data);
        }

        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }
    
    public function deleteserialdokpu(){
        $message = '';
        $model = $this->input->post();
        $this->load->model("unit/m_unit");
        $this->form_validation->set_rules('id_unit_serial', 'Unit', 'trim|required');
        
        if(!CheckEmpty($model['id_unit_serial']))
        {
            $unitserial=$this->m_unit->GetOneUnitSerial($model['id_unit_serial']);
            if(CheckEmpty($unitserial))
            {
                $message.="Unit tidak ditemukan dalam<br/>";
            }
            else
            {
                if(!CheckEmpty($unitserial->id_buka_jual))
                {
                       $message.="Unit sudah dibuka jual<br/>";
                }
            }
            
        }
        
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_do_kpu->DeleteSerialDoKpu($model['id_unit_serial']);
            echo json_encode($status);
        }
    }
    
    
    
     public function edit_do_kpu($id_do_kpu) {
        $this->load->model("do_kpu/m_do_kpu");
        $this->load->model("pool/m_pool");
        $row = $this->m_do_kpu->GetOneDoKpu($id_do_kpu);
        //$this->m_do_kpu->UpdateDoKPU($id_do_kpu);
        $row = json_decode(json_encode($row), true);
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $javascript = array();
        $module = "K061";
        $header = "K059";
        $this->load->model("supplier/m_supplier");
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("kpu/m_kpu");
        $listunitkpu=$this->m_kpu->GetUnitFromKPU($row['id_kpu']);
        $row['free_top_proposed'] = 0;
        $row['title'] = "Add KPU";
        $row['tanggal'] = GetDateNow();
        $row['type_pembayaran']=2;
        $row['free_parking'] = AddDays(GetDateNow(), "5 days");
        CekModule($module);
        $row['form'] = $header;
        $row['jumlah'] = 1;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_pool'] =$this->m_pool->GetDropDownPool();
        $listdetail =$this->m_do_kpu->GetUnitSerialFromDo($id_do_kpu);
        $row['qty_sisa']=$row['qty_total']-count($listunitkpu)+count($row['list_detail']);
        $indexfor=0;
        $row['button']="Edit";
        
        $jumlahsisa=($row['qty_sisa']-count($listdetail));
        for($indexfor=0;$indexfor<$jumlahsisa;$indexfor++)
        {
           array_push($listdetail,array("id_unit_serial"=>null));
        }
        $row['list_detail']=$listdetail;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'do_kpu/v_do_kpu_manipulate', $javascript, $css);
    }
    
    public function view_do_kpu($id_do_kpu) {
        $this->load->model("do_kpu/m_do_kpu");
        $row = $this->m_do_kpu->GetOneDoKpu($id_do_kpu);
        $row = json_decode(json_encode($row), true);
	
        $jenis_pembayaran = array();
        $jenis_pembayaran[1] = "Tunai";
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $javascript = array();
        $module = "K061";
        $header = "K059";
        $this->load->model("supplier/m_supplier");
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("pool/m_pool");
        $row['free_top_proposed'] = 0;
        $row['title'] = "VIEW DO KPU";
		$row['tanggal'] = $row['tanggal_do'];
        $row['button']="view";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_detail'] =$this->m_do_kpu->GetUnitSerialFromDo($id_do_kpu);
		$row['list_pool'] =$this->m_pool->GetDropDownPool();
        $row['is_view'] = "view";
		$row['jumlah'] = 1;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'do_kpu/v_do_kpu_manipulate', $javascript, $css);
    }
    
}

/* End of file Kpu.php */