<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Do_customer extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_do_customer');
        $this->load->model("warna/m_warna");
        $this->load->model("vrf/m_vrf");
        $this->load->model("model/m_model");
        $this->load->model("unit/m_unit");
        $this->load->model("customer/m_customer");
    }
    
    public function generatedocustomer() {


        $this->db->from("#_unit_serial");
        $this->db->join("#__import_unit","#_unit_serial.engine_no like concat('%',#__import_unit.no_mesin,'%') and #_unit_serial.vin_number like concat('%',#__import_unit.no_rangka,'%')","left");
        $this->db->where(array("#__import_unit.no_rangka"=>null,"#_unit_serial.id_do_prospek_detail"=>null,"#_unit_serial.id_buka_jual !="=>null));
        $this->db->join("#_buka_jual","#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual");
        $this->db->join("#_prospek","#_prospek.id_prospek=#_buka_jual.id_prospek");
        $this->db->join("#_customer","#_customer.id_customer=#_buka_jual.id_customer");
        
        $this->load->model('do_prospek/m_do_prospek');        
        $this->db->group_by("#_buka_jual.id_buka_jual");
        $this->db->select("#_buka_jual.id_buka_jual,#_unit_serial.id_pool,#_prospek.nama_stnk,#_prospek.alamat_stnk,#_customer.nama_customer,#_customer.alamat,#_customer.contact_person,#_buka_jual.id_prospek,group_concat(#_unit_serial.id_unit_serial) as unit,#_buka_jual.tanggal_buka_jual,#_buka_jual.nomor_buka_jual");
        $listbukajual=$this->db->get()->result();
        
        foreach($listbukajual as $bukjual)
        {
            $dopros=[];
            $dopros['id_buka_jual']=$bukjual->id_buka_jual;
            $dopros['id_ekspedisi']=3;
            $dopros['id_customer']=$bukjual->id_buka_jual;
            $listid= explode(",", $bukjual->unit);
            $dopros['id_unit_serial']=$listid;
            $dopros['keterangan']="";
            $dopros['up']= CheckEmpty($bukjual->contact_person)?$bukjual->nama_customer:$bukjual->contact_person;
            $dopros['atas_nama']=$bukjual->nama_customer;
            $dopros['tanggal_do']=$bukjual->tanggal_buka_jual;
            $dopros['free_parking'] = AddDays($bukjual->tanggal_buka_jual, "5 days");
            $dopros['order'] =$bukjual->nama_customer;
            $dopros['tujuan']=null;
            $dopros['type_do'] = 3;
            $dopros['id_pool'] = $bukjual->id_pool;
            $ischeked=[];
            $kolom1=[];
            $kolom2=[];
            $keteranganbawah=[];
            foreach($listid as $id)
            {
                $ischeked = "yes";
                $kolom1 = "1. Ban Cadangan dan Velg : 1 Buah 
2. Pompa Pelumas : - 
3. Perkakas terdiri dari: 
- Dongkrak dan Stang 
- Kunci Roda dan Stang 
- Engkol Ban Cadangan";
                $kolom2 = "4. Toolset terdiri dari : 
- Kunci Pas : 5 Buah 
- Tang : 1 Buah 
- Obeng : 1 Buah 
- Kunci Inggris : 1 (T) 
5. Palu : 1 Buah";
                $keteranganbawah = "Unit Tidak Boleh Keluar Dari Karoseri Tanpa Persetujuan DGMI";
            }
            $dopros['is_checked']=$ischeked;
            $dopros['kolom1']=$kolom1;
            $dopros['kolom2']=$kolom2;
            $dopros['keterangan_bawah']=$keteranganbawah;
            
           $returndo= $this->m_do_prospek->DO_ProspekManipulate($dopros);
              dumperror($returndo);
            
            
           
        }
        
        
      
        
        die();
        
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K074";
        $header = "K059";
        $title = "DO Customer";
        $this->load->model("customer/m_customer");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "do_customer/v_do_customer_index", $javascript,$css);
    }

    public function getdatadocustomer() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_do_customer->GetDataDoCustomer($params);
    }

    public function create_do_customer() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K074";
        $header = "K059";
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $row['free_top_proposed'] = 0;
        $row['title'] = "Add DO Customer";
        $row['tanggal'] = GetDateNow();
        $row['type_pembayaran']=2;
        $row['free_parking'] = AddDays(GetDateNow(), "5 days");
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_detail']=array();
        $row['listserial'] =$this->m_unit->GetDropDownSerialUnit();
        $row['list_detail'][]=array("vin_number"=>"","vin"=>"","manufacture_code"=>"","engine_no"=>"","free_parking"=>"");
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'do_customer/v_do_customer_manipulate', $javascript, $css);
    }
    
    public function do_customer_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $detail = [];
        $id_unit = @$model['id_unit_serial'];
        if (CheckEmpty(@$id_unit)) {
            $id_unit = [];
        }
        foreach($id_unit as $key=>$value) {
            if ($model['id_unit_serial'][$key] != '') {
                $detail[] = array(
                    'id_unit_serial' => $model['id_unit_serial'][$key],
                    'id_alamat_customer' => $model['alamat_do'][$key]
                );
            }
        }
      
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } elseif ($detail == null) {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/> Pilih minimal satu unit !'));
        } else {
            unset($model['alamat_do']);
            unset($model['id_unit_serial']);
            $model['detail'] = $detail;
            $status = $this->m_do_customer->Do_customerManipulate($model);
            echo json_encode($status);
            // print_r($model);
        }
    }

    public function prospek_save_do() {
        $message = '';

        $tgl_do = $this->input->post('tgl_do');
        foreach ($tgl_do as $key => $value) {
            $data[] = array(
                'id_prospek' => $_POST['idprospek'],
                'tgl_do' => $_POST['tgl_do'][$key],
                'no_do' => $_POST['no_do'][$key],
                'manufacture_no' => $_POST['manufacture_no'][$key],
                'engine_no' => $_POST['engine_no'][$key],
                'created_by' => GetUserId(),
                'created_date' => GetDateNow(),
            );
        }
        $this->db->insert_batch('dgmi_do', $data);
        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function prospek_edit_do() {
        $message = '';
        $this->db->where('id_prospek', $_POST['idprospek']);
        $this->db->delete('dgmi_do');

        if ($this->input->post('tgl_do')) {
            $tgl_do = $this->input->post('tgl_do');
            foreach ($tgl_do as $key => $value) {
                $data[] = array(
                    'id_prospek' => $_POST['idprospek'],
                    'tgl_do' => $_POST['tgl_do'][$key],
                    'no_do' => $_POST['no_do'][$key],
                    'manufacture_no' => $_POST['manufacture_no'][$key],
                    'engine_no' => $_POST['engine_no'][$key],
                    'created_by' => GetUserId(),
                    'created_date' => GetDateNow(),
                );
            }
            $this->db->insert_batch('dgmi_do', $data);
        }

        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function edit_do_customer($id = 0) {
        $row = $this->m_do_customer->GetOneDoCustomer($id);
        
        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K074";
            $header = "K059";
            $row['title'] = "Edit DO";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->model("customer/m_customer");
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['listAlamat'] = $this->m_customer->getAlamatCustomer($row['id_customer']);
            $row['list_unit'] = array();
            foreach($this->m_do_customer->GetListUnitByCustomer($row['id_customer']) as $dt) {
                $dt['is_checked'] = false;
                $is_available = $this->m_do_customer->GetListAvailable($row['id_customer'], $dt['id_unit_serial']);
                if ($is_available['idUnitSerial']) {
                    $dt['is_checked'] = true;
                }
                $dt['alamatDO'] = $this->getAlamat($row['listAlamat'], $is_available['dtAlamat']);
                array_push($row['list_unit'], $dt);
            }
            // echo "<pre>";print_r($row);
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'do_customer/v_do_customer_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "DO Customer cannot be found in database");
            redirect(site_url('do_customer'));
        }
    }

    public function getAlamat($listAlamat, $id_alamat) {
        $selectAlamat = '<select type="text" name="alamat_do[]" class="form-control select2" id="select_alamat" placeholder="Pilih Alamat Kirim">';
        $selectAlamat .= "<option value='0'> Pilih Alamat Pengiriman </option>";
        foreach(@$listAlamat as $rowAlamat) {
            $selected = $rowAlamat['id_alamat'] == $id_alamat ? 'selected' : '';
            $selectAlamat .= "<option value='" . $rowAlamat['id_alamat'] . "'" . $selected . ">" . $rowAlamat['nama_alamat'] . "</option>";
        }
        $selectAlamat .= '</select>';

        return $selectAlamat;
    }

    public function prospek_batal() {
        $message = '';
        $this->form_validation->set_rules('id_prospek', 'Kpu', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_prospek->KpuDelete($model['id_prospek']);
        }

        echo json_encode($result);
    }

    public function get_unit_customer ($id_customer) {
        $data['cust'] =  $this->m_customer->GetDetCustomer($id_customer);
        $data['alamat'] = $this->m_customer->getAlamatCustomer($id_customer);
        $data['unit'] = array();
        foreach($this->m_do_customer->GetListUnitByCustomer($id_customer) as $row) {
            $is_available = $this->m_do_customer->GetListAvailable($id_customer, $row['id_unit_serial']);
            if (!$is_available['idUnitSerial']) {
                array_push($data['unit'], $row);
            }
        }

        echo json_encode($data);
    }

}

/* End of file Kpu.php */