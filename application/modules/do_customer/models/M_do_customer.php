<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_do_customer extends CI_Model {

    public $table = '#_do_customer';
    public $id = 'id_do_customer';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDataDoCustomer($params) {
        $this->load->library('datatables');
        $this->datatables->select('dgmi_do_customer.id_do_customer, nomor_do_customer, nama_customer, tanggal_do, IF (COUNT(id_detail_do_customer) > 0, COUNT(id_detail_do_customer), NULL) as jml_do, diterima_oleh, diperiksa_oleh, disetujui_oleh, disiapkan_oleh');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('dgmi_detail_do_customer', 'dgmi_do_customer.id_do_customer = dgmi_detail_do_customer.id_do_customer');
        $this->datatables->join('#_customer', '#_customer.id_customer = dgmi_do_customer.id_customer');
        $extra=[];
        $where=[];
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_do_customer.nomor_do_customer'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_do_customer.tanggal_do BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_do_customer.tanggal_do"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_do_customer.tanggal_do"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                $where['#_do_customer.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_do_customer.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $this->db->where($where);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('do_customer/edit_do_customer/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Hapus', array('class' => 'btn btn-danger btn-xs', "onclick" => "hapus($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_do_customer');
        return $this->datatables->generate();
    }

    // get all
    function GetOneDoCustomer($keyword, $type = '#_do_customer.id_do_customer',$isfull=true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        if($isfull)
        {
            $this->db->select('#_do_customer.*, #_customer.keterangan as ket_cust, #_customer.id_customer, #_customer.contact_person as atas_nama, #_customer.id_customer, nama_customer, tanggal_do as tanggal, #_customer.alamat');
            $this->db->join('#_detail_do_customer', '#_detail_do_customer.id_do_customer = #_do_customer.id_do_customer','left');
            $this->db->join('#_customer', '#_customer.id_customer = #_do_customer.id_customer','left');
            
        }
        $do_prospek = $this->db->get($this->table)->row();
        return $do_prospek;
    }
    
    function UpdateStatusPembayaranKPU($id_prospek)
    {
        $prospek=$this->GetOneKpu($id_prospek,"id_prospek",true);
        if($prospek)
        {
            if (CheckEmpty($prospek->tanggal_pelunasan)) {
                if ($prospek->sisa <= 0) {
                    if (count($prospek->listpembayaran) > 0) {
                        $this->db->update("#_prospek", array("tanggal_pelunasan" => $prospek->listpembayaran[count($prospek->listpembayaran) - 1]->tanggal_transaksi), array("id_prospek" => $id_prospek));
                    }
                }
            } 
            else if(!CheckEmpty($prospek->tanggal_pelunasan))
            {   
                if ($prospek->sisa > 0) {
                    $this->db->update("#_prospek",array("tanggal_pelunasan"=>null),array("id_prospek"=>$id_prospek));
                }
            }
        }
        
    }
    

    function Do_customerManipulate($model) {
        try {
            $message = "";
           
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $do_customer['id_customer'] = ForeignKeyFromDb($model['id_customer']);
            $do_customer['qty_do'] = count(@$model['id_serial_unit']);
            $do_customer['keterangan'] = $model['keterangan'];
            $do_customer['tanggal_do'] = DefaultTanggalDatabase($model['tanggal']);
            $do_customer['diterima_oleh'] = $model['diterima_oleh'];
            $do_customer['diperiksa_oleh'] = $model['diperiksa_oleh'];
            $do_customer['disetujui_oleh'] = $model['disetujui_oleh'];
            $do_customer['disiapkan_oleh'] = $model['disiapkan_oleh'];
            $do_customer['status'] =1;
            
            $detailDo = $model['detail'];

            $userid= GetUserId();
            $id_do_customer=0;
            if (CheckEmpty(@$model['id_do_customer'])) {
                $do_customer['created_date'] = GetDateNow();
                $do_customer['created_by'] = ForeignKeyFromDb(GetUserId());
                $do_customer['nomor_do_customer'] = AutoIncrement('#_do_customer', 'DB/' . date("y") . '/', 'nomor_do_customer', 5);
                $res = $this->db->insert($this->table, $do_customer);
                $id_do_customer = $this->db->insert_id();
                
            } else {
                $do_customer['updated_date'] = GetDateNow();
                $do_customer['updated_by'] = ForeignKeyFromDb(GetUserId());
                $res = $this->db->update($this->table, $do_customer, array("id_do_customer" => $model['id_do_customer']));
                $id_do_customer =$model['id_do_customer'];
                
            }
            
            foreach ($detailDo as $key=>$value) {
                if (CheckEmpty(@$model['id_do_customer'])) {
                    $this->db->where('id_do_customer', $model['id_do_customer']);
                    $this->db->delete('#_detail_do_customer');
                }
                
                $value['id_do_customer']=$id_do_customer;
                $this->db->insert("#_detail_do_customer", $value);
            }
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data DO Customer Sudah di" . (CheckEmpty(@$model['id_do_customer']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                // SetPrint($id_prospek, $do_prospek['nomor_do_customer'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownKpu() {
        $listprospek = GetTableData($this->table, 'id_prospek', '', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listprospek;
    }

    function KpuDelete($id_prospek) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_prospek' => $id_prospek));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Kpu has been deleted from database");
    }

    function GetDoProspekByVrf ($id_vrf) {
        $this->db->select("dgmi_do_customer.tanggal_do, nama_customer, nomor_master, nama_unit, nama_warna, vin_number, dgmi_unit_serial.vin, manufacture_code, engine_no, no_bpkb, no_polisi, dgmi_do_customer.alamat, pengemudi");
        $this->db->where('dgmi_vrf.id_vrf', $id_vrf);
        $this->db->where('dgmi_vrf.deleted_date', null);
        $this->db->join('dgmi_kpu', 'dgmi_kpu.id_vrf = dgmi_vrf.id_vrf', 'left');
        $this->db->join('dgmi_do_kpu', 'dgmi_do_kpu.id_kpu = dgmi_kpu.id_kpu', 'left');
        $this->db->join('dgmi_unit_serial', 'dgmi_do_kpu.id_do_kpu = dgmi_unit_serial.id_do_kpu', 'left');
        $this->db->join('dgmi_do_customer', 'dgmi_unit_serial.id_do_so = dgmi_do_customer.id_do_customer');
        $this->db->join('dgmi_unit', 'dgmi_unit.id_unit = dgmi_unit_serial.id_unit');
        $this->db->join('dgmi_prospek', 'dgmi_do_customer.id_prospek = dgmi_prospek.id_prospek');
        $this->db->join('dgmi_warna', 'dgmi_prospek.id_warna = dgmi_warna.id_warna', 'left');
        $this->db->join('dgmi_customer', 'dgmi_prospek.id_customer = dgmi_customer.id_customer', 'left');
        $grid_do_kpu = $this->db->get('dgmi_vrf')->result_array();
        return $grid_do_kpu;
    }

    function GetListUnitByCustomer ($id_customer) {
        $this->db->select("id_unit_serial, dgmi_do_prospek.tanggal_do, nama_customer, `nama_unit`, `nama_warna`, `vin_number`, dgmi_unit_serial.vin, `manufacture_code`, `engine_no`, `no_bpkb`, `no_polisi`, dgmi_do_prospek.alamat");
        $this->db->where(array('dgmi_customer.id_customer' => $id_customer, 'dgmi_customer.deleted_date' => NULL));
        $this->db->join('dgmi_do_prospek', 'dgmi_unit_serial.id_do_so = dgmi_do_prospek.id_do_prospek');
        $this->db->join('dgmi_unit', '`dgmi_unit`.`id_unit` = `dgmi_unit_serial`.`id_unit`');
        $this->db->join('dgmi_prospek', '`dgmi_do_prospek`.`id_prospek` = `dgmi_prospek`.`id_prospek`');
        $this->db->join('dgmi_warna', '`dgmi_prospek`.`id_warna` = `dgmi_warna`.`id_warna`', 'left');
        $this->db->join('dgmi_customer', 'dgmi_prospek.id_customer = dgmi_customer.id_customer', 'left');
        $result = $this->db->get('dgmi_unit_serial')->result_array();
        return $result;
    }

    function GetListAvailable ($id_customer, $id_unit_serial) {
        $this->db->where('id_customer', $id_customer);
        $this->db->where('id_unit_serial', $id_unit_serial);
        $this->db->join('dgmi_detail_do_customer', 'dgmi_do_customer.id_do_customer = dgmi_do_customer.id_do_customer');
        $data = $this->db->get('dgmi_do_customer')->row();
        $dtAlamat = 0;
        $idUnitSerial = false;
        if ($data) {
            $idUnitSerial = true;
            $dtAlamat = $data->id_alamat_customer;
        }
        
        return array('idUnitSerial' => $idUnitSerial, 'dtAlamat' => $dtAlamat);
    }

}
