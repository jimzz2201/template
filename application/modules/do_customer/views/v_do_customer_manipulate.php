<section class="content-header">
    <h1>
        Delivery Order Prospek <?= @$button ?>
        <small>Delivery Order Prospek</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Delivery Order Prospek</li>
        <li class="active">Prospek <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_do_customer" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_do_customer" value="<?=@$id_do_customer?>">
                                <div class="form-group">
                                    <?= form_label('Customer', "txt_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        if (!CheckEmpty(@$id_customer)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_customer, 'name' => 'id_customer'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Customer'));
                                        } else {
                                            echo form_dropdown(array("name" => "id_customer"), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer, array('class' => 'form-control select2', 'id' => 'dd_id_customer'));
                                        }
                                        ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$ket_cust, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan', 'disabled' => 'disabled')); ?>
                                    </div>

                                </div>  
                                <div class="form-group">
                                     <?= form_label('Atas Nama', "txt_tgl_po", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'atas_nama', 'value' => @$atas_nama, 'class' => 'form-control ', 'id' => 'txt_atas_nama', 'placeholder' => 'Atas Nama', 'disabled' => 'disabled')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$alamat, 'name' => 'alamat', 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat', 'readonly' => 'readonly')); ?>
                                    </div>

                                </div>
                                <hr />
                                <div class="form-group">
                                    <?= form_label('Diterima Oleh', "txt_Diterima", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'diterima_oleh', 'value' => @$diterima_oleh, 'class' => 'form-control', 'id' => 'txt_diterima_oleh', 'placeholder' => 'Diterima Oleh')); ?>
                                    </div>

                                    <?= form_label('Diperiksa Oleh', "txt_Diterima", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'diperiksa_oleh', 'value' => @$diperiksa_oleh, 'class' => 'form-control', 'id' => 'txt_diperiksa_oleh', 'placeholder' => 'Diperiksa Oleh')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Disetujui Oleh', "txt_Diterima", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'disetujui_oleh', 'value' => @$disetujui_oleh, 'class' => 'form-control', 'id' => 'txt_disetujui_oleh', 'placeholder' => 'Disetujui Oleh')); ?>
                                    </div>

                                    <?= form_label('Disiapkan Oleh', "txt_Diterima", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'disiapkan_oleh', 'value' => @$disiapkan_oleh, 'class' => 'form-control', 'id' => 'txt_disiapkan_oleh', 'placeholder' => 'Disiapkan Oleh')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <hr/>
                                <div class="portlet-body form" id="tbl" style="overflow-x:auto;">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr class="headTable">
                                                <th>No</th>
                                                <th style="width:100px;">Nama Unit</th>
                                                <th>Warna</th>
                                                <th style="width:200px;">VIN Number</th>
                                                <th>VIN</th>
                                                <th>Manufacture Code</th>
                                                <th>Engine NO</th>
                                                <th>No BPKB</th>
                                                <th style="width:100px;">No Polisi</th>
                                                <th>Alamat Kirim</th>
                                                <th style="text-align:center"><input type="checkbox" id="checkAll"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="list_unit">
                                        <?php
                                            if (@$id_do_customer) {
                                                $num = 1;
                                                foreach (@$list_unit as $row) {
                                                    $checked = $row['is_checked'] ? "checked" : "";
                                                    echo "<tr>
                                                            <td>". $num ."</td>
                                                            <td>". $row['nama_unit'] ."</td>
                                                            <td>". $row['nama_warna'] ."</td>
                                                            <td>". $row['vin_number'] ."</td>
                                                            <td>". $row['vin'] ."</td>
                                                            <td>". $row['manufacture_code'] ."</td>
                                                            <td>". $row['engine_no'] ."</td>
                                                            <td>". $row['no_bpkb'] ."</td>
                                                            <td>". $row['no_polisi'] ."</td>
                                                            <td>". $row['alamatDO'] ."</td>
                                                            <td style='text-align:center;'><input type='checkbox' name='id_unit_serial[]' class='checkUnit' " . $checked . " value='" . $row['id_unit_serial'] . "'></td>
                                                        </tr>";
                                                    $num++;
                                                }
                                            } else {
                                                echo '<tr><td colspan="10" style="text-align:center">Tidak Ada Data</td></tr>';
                                            }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <hr/>

                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>
    function RefreshGrid()
    {


    }

    function deleterow(indrow) {
        RefreshGrid();
    }
    $(document).ready(function () {
        $(".select2").select2();
        $("#dd_id_customer").change(function () {
            var id_customer = $(this).val();
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/do_customer/get_unit_customer/' + id_customer,
                dataType: 'json',
                data: {
                    id_customer: id_customer
                },
                success: function (data) {
                    $("#txt_keterangan").val(data.cust['keterangan']);
                    $("#txt_atas_nama").val(data.cust['contact_person']);
                    $("#txt_alamat").val(data.cust['alamat']);
                    var selectAlamat = '<select type="text" name="alamat_do[]" class="form-control select2" id="select_alamat" placeholder="Pilih Alamat Kirim">';
                    selectAlamat += "<option value='0'> Pilih Alamat Pengiriman </option>";
                    data.alamat.forEach(dataAlamat);
                    function dataAlamat (row) {
                        selectAlamat += "<option value='" + row['id_alamat'] + "'>" + row['nama_alamat'] + "</option>";
                    }
                    selectAlamat += '</select>'
                    var list = '';
                    var num = 1;
                    data.unit.forEach(dataUnit);
                    function dataUnit (row) {
                        list += "<tr>" +
                                    "<td>" + num + "</td>" +
                                    "<td>" + row['nama_unit'] + "</td>" +
                                    "<td>" + row['nama_warna'] + "</td>" +
                                    "<td>" + row['vin_number'] + "</td>" +
                                    "<td>" + row['vin'] + "</td>" +
                                    "<td>" + row['manufacture_code'] + "</td>" +
                                    "<td>" + row['engine_no'] + "</td>" +
                                    "<td>" + row['no_bpkb'] + "</td>" +
                                    "<td>" + row['no_polisi'] + "</td>" +
                                    "<td>" + selectAlamat + "</td>" +
                                    "<td style='text-align:center;'><input type='checkbox' name='id_unit_serial[]' class='checkUnit' value='" + row['id_unit_serial'] + "'></td>" +
                                "</tr>";
                                num++;
                    }
                    $('#list_unit').html(list);
                    $(".select2").select2();
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
            $("#checkAll").change(function () {
                $(".checkUnit").prop('checked', this.checked);
            })
        })


    })



    $(document).ready(function () {
        $(".datepicker").datepicker();
    })


    $("#frm_do_customer").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data prospek berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_customer/do_customer_manipulate',
                    dataType: 'json',
                    data: $("#frm_do_customer").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/do_customer";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })

</script>
<style>
    .control-label {
        text-align: left !important;
    }
    .control-label {
        text-align: left !important;
    }
    th {
        text-align: center !important;
    }
    .right {
        text-align: right !important;
    }
    .headTable {
        background-color: #0396B0 !important;
        color: #FFFFFF !important;
    }
</style>