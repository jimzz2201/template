<div style="position:relative">
    <h3 style="font-weight: bold;">Unit ke <?= $key + 1 ?></h3>
    <hr/>
    <div class="form-group">
        <?= form_label('Select Unit', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-6">
            <?php
            echo form_dropdown(array('type' => 'text', "onchange" => "ChangeSerial(" . $key . ")", 'name' => 'id_serial_unit[]', 'class' => 'form-control select2', 'id' => 'dd_id_serial_unit' . $key, 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_serial_unit, "json", "Unit"), @$id_serial_unit);
            ?>
        </div>
        <?= form_label('Warna', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'warna[]', 'value' => @$detail['warna'], 'class' => 'form-control', 'id' => 'txt_warna' . $key, 'placeholder' => 'Warna')); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-2">
            <?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit'.$key, 'placeholder' => 'Unit')); ?>
        </div>
        <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit'.$key, 'placeholder' => 'Tipe Unit')); ?>
        </div>
        <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori'.$key, 'placeholder' => 'Kategori Unit')); ?>
        </div>
    </div>
    <div class="form-group">
        <?= form_label('VIN Number', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-2">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin_number[]', 'value' => @$detail['vin_number'], 'class' => 'form-control', 'id' => 'txt_vin_number' . $key, 'placeholder' => 'VIN Number')); ?>
        </div>
        <?= form_label('Vin', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'vin[]', 'value' => @$detail['vin'], 'class' => 'form-control', 'id' => 'txt_vin' . $key, 'placeholder' => 'Vin')); ?>
        </div>
        <?= form_label('Manufacture&nbsp;Code', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'manufacture_code[]', 'value' => @$detail['manufacture_code'], 'class' => 'form-control', 'id' => 'txt_manufacture_code' . $key, 'placeholder' => 'Manufacture Code')); ?>
        </div>

    </div>
    <div class="form-group">
        <?= form_label('Engine No', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
        <div class="col-sm-2">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'engine_no[]', 'value' => @$detail['engine_no'], 'class' => 'form-control', 'id' => 'txt_engine_no' . $key, 'placeholder' => 'Engine NO')); ?>
        </div>
        <?= form_label('NO BPKB', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'no_bpkb[]', 'value' => @$detail['no_bpkb'], 'class' => 'form-control', 'id' => 'txt_no_bpkb' . $key, 'placeholder' => 'NO BPKB')); ?>
        </div>
        <?= form_label('No Polisi', "txt_qty", array("class" => 'col-sm-1 control-label')); ?>
        <div class="col-sm-3">
            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'no_polisi[]', 'value' => @$detail['no_polisi'], 'class' => 'form-control', 'id' => 'txt_no_polisi' . $key, 'placeholder' => 'No Polisi')); ?>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#dd_id_serial_unit<?= @$key ?>").select2({data: listserial});
    })

</script>