<div class="modal-header">
        Satuan <?php echo @$button ?>
    </div>
<div class="modal-body">
<form id="frm_satuan" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_satuan" value="<?php echo @$id_satuan; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Satuan', "txt_kode_satuan", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?php $mergearray=array();
                            if(!CheckEmpty(@$id_satuan))
                            {
                                $mergearray['disabled']="disabled";
                            }
                            else
                            {
                                $mergearray['name'] = "kode_satuan";
                            }?>
                    <?= form_input(array_merge($mergearray,array('type' => 'text', 'value' => @$kode_satuan, 'class' => 'form-control', 'id' => 'txt_kode_satuan', 'placeholder' => 'Kode Satuan'))); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Satuan', "txt_nama_satuan", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nama_satuan', 'value' => @$nama_satuan, 'class' => 'form-control', 'id' => 'txt_nama_satuan', 'placeholder' => 'Nama Satuan')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Id Satuan Parent', "txt_id_satuan_parent", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'id_satuan_parent', 'value' => @$id_satuan_parent, 'class' => 'form-control', 'id' => 'txt_id_satuan_parent', 'placeholder' => 'Id Satuan Parent')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nominal Satuan', "txt_nominal_satuan", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'nominal_satuan', 'value' => DefaultCurrency(@$nominal_satuan), 'class' => 'form-control', 'id' => 'txt_nominal_satuan', 'placeholder' => 'Nominal Satuan' , 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Keterangan Satuan', "txt_keterangan_satuan", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_input(array('type' => 'text', 'name' => 'keterangan_satuan', 'value' => @$keterangan_satuan, 'class' => 'form-control', 'id' => 'txt_keterangan_satuan', 'placeholder' => 'Keterangan Satuan')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>

</form>
</div>
<script>
$("#frm_satuan").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/satuan/satuan_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>