<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Satuan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_satuan');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K018";
        $header = "K003";
        $title = "Satuan";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	CekModule($module);
	LoadTemplate($model, "satuan/v_satuan_index", $javascript);
        
    } 
    public function get_one_satuan() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_satuan', 'Satuan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_satuan->GetOneSatuan($model["id_satuan"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Satuan tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatasatuan() {
        header('Content-Type: application/json');
        echo $this->m_satuan->GetDatasatuan();
    }
    
    public function create_satuan() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	$module = "K018";
        $header = "K003";
        $row['title'] = "Add Satuan";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	    $this->load->view('satuan/v_satuan_manipulate', $row);       
    }
    
    public function satuan_manipulate() 
    {
        $message='';

	$this->form_validation->set_rules('kode_satuan', 'kode satuan', 'trim|required');
	$this->form_validation->set_rules('nama_satuan', 'nama satuan', 'trim|required');
	$this->form_validation->set_rules('id_satuan_parent', 'id satuan parent', 'trim|required');
	$this->form_validation->set_rules('nominal_satuan', 'nominal satuan', 'trim|required|numeric');
	$this->form_validation->set_rules('keterangan_satuan', 'keterangan satuan', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_satuan->SatuanManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_satuan($id=0) 
    {
        $row = $this->m_satuan->GetOneSatuan($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	    $module = "K018";
            $header = "K003";
            $row['title'] = "Edit Satuan";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    $this->load->view('satuan/v_satuan_manipulate', $row);
        } else {
            SetMessageSession(0, "Satuan cannot be found in database");
            redirect(site_url('satuan'));
        }
    }
    
    public function satuan_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_satuan', 'Satuan', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_satuan->SatuanDelete($model['id_satuan']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Satuan.php */