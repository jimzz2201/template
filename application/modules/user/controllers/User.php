<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_user');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";


        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K037";
        $header = "K006";
        $title = "User";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        LoadTemplate($model, "user/v_user_index", $javascript);
    }

    public function getoneuserfull() {
        $model = $this->input->post();
        $kode = '';

        if (CheckKey($model, 'kode')) {
            $kode = $model['kode'];
        }
        $this->load->model("user/m_user");
        $row = $this->m_user->GetOneUserKomplit($kode);

        if ($row == null) {
            $row = $this->m_user->GetOneUserKomplit($kode, "kode_user");
        }


        if ($row != null) {
            echo json_encode(array("st" => true, "obj" => $row));
        } else {
            echo json_encode(array("st" => false));
        }
    }

    public function getdatauser() {
        header('Content-Type: application/json');
        echo $this->m_user->GetDatauser();
    }

    public function create_user() {
        $row = (object) array();
        $row->button = 'Add';
        $row->openmenu = "menusetting";
        $this->load->model("cabang/m_cabang");
        $row = json_decode(json_encode($row), true);
        $module = "K037";
        $header = "K006";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_group'] = GetTableData('#_group', 'id_group', 'nama_group', array(), 'obj');
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $row['list_user_parent'] = $this->m_user->GetDropDownUser();
        $css[] = "assets/plugins/select2/select2.css";
        $javascript[] = "assets/plugins/select2/select2.js";
        $javascript = array();
        LoadTemplate($row, 'user/v_user_manipulate', $javascript, $css);
    }

    public function user_manipulate() {
        $message = '';
        $id_user = $this->input->post('id_user');
        $this->form_validation->set_rules('nama_user', 'nama user', 'trim|required');
        $this->form_validation->set_rules('id_group', 'id group', 'trim|required');
        if ($id_user) {
            $this->form_validation->set_rules('password_user', 'password user', 'trim');
        } else {
            $this->form_validation->set_rules('username', 'username', 'trim|required|is_unique[#_user.username]');
            $this->form_validation->set_rules('password_user', 'password user', 'trim|required');
        }
        $model = $this->input->post();
       
        if ($id_user && empty($model['password_user'])) {
            unset($model['password_user']);
        } elseif (!empty($model['password_user'])) {
            $model['password_user'] = sha1($model['password_user']);
        }

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_user->UserManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_user($id = 0) {
        $row = $this->m_user->GetUserDetail($id);
        $this->load->model("jabatan/m_jabatan");
        if ($row) {
            $row->button = 'Update';
            $row->openmenu = "menusetting";
            $row = json_decode(json_encode($row), true);
            $row['password_user'] = null;
            $module = "K037";
            $header = "K006";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['list_group'] = GetTableData('#_group', 'id_group', 'nama_group', array(), 'obj');
            $this->load->model("cabang/m_cabang");
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("obj");
            $row['list_jabatan'] = $this->m_jabatan->GetDropDownJabatan();
            $row['list_user_parent'] = $this->m_user->GetDropDownUser();
            if($row['arr_cabang']!="all")
            {
                $row['allow_all_cabang']=0;
                $row['arr_cabang']= explode( ",",$row['arr_cabang']);
            }
            $css = array();
            $css[] = "assets/plugins/select2/select2.css";
            $javascript = array();
            $javascript[] = "assets/plugins/select2/select2.js";
            LoadTemplate($row, 'user/v_user_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "User cannot be found in database");
            redirect(site_url('user'));
        }
    }

    public function user_delete() {
        $message = '';
        $this->form_validation->set_rules('id_user', 'User', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_user->UserDelete($model['id_user']);
        }

        echo json_encode($result);
    }

    public function checkUser() {
        $resp = array('ada' => false);
        $username = $this->input->post('username');
        $user = $this->m_user->GetOneUser($username);
        if ($user) {
            $resp['ada'] = true;
        }
        echo json_encode($resp);
    }

}

/* End of file User.php */