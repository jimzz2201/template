
<section class="content-header">
    <h1>
        User <?= @$button ?>
        <small>User</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>User</li>
        <li class="active">User <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <form id="frm_user" class="form-horizontal form-groups-bordered validate" method="post">
                                <div class="col-md-6">
                                    <input type="hidden" name="id_user" value="<?php echo @$id_user; ?>" />
                                    <div class="form-group">
                                        <?= form_label('Kode User', "txt_kode_user", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?= form_input(array('type' => 'text', 'value' => @$kode_user, 'class' => 'form-control', 'id' => 'txt_kode_user', 'placeholder' => 'Kode User', 'disabled' => true)); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Nama User', "txt_nama_user", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?= form_input(array('type' => 'text', 'name' => 'nama_user', 'value' => @$nama_user, 'class' => 'form-control', 'id' => 'txt_nama_user', 'placeholder' => 'Nama User')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Username', "txt_username", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?php
                                            $merge = array();
                                            if (!CheckEmpty(@$username)) {
                                                $merge['disabled'] = true;
                                            } else {
                                                $merge['name'] = 'username';
                                            }
                                            ?>

                                            <?= form_input(array_merge($merge, array('type' => 'text', 'value' => @$username, 'class' => 'form-control', 'id' => 'txt_username', 'placeholder' => 'Username'))); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Group', "txt_id_group", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_group', 'class' => 'form-control', 'id' => 'txt_id_group', 'placeholder' => 'Group'), DefaultEmptyDropdown($list_group, "obj", "Group"), @$id_group); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <?= form_label('Password User', "txt_password_user", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?= form_input(array('type' => 'password', 'name' => 'password_user', 'value' => @$password_user, 'class' => 'form-control', 'id' => 'txt_password_user', 'placeholder' => 'Password User')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Default Cabang', "txt_cabang", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_cabang', 'class' => 'form-control', 'id' => 'dd_id_cabang', 'placeholder' => 'cabang'), DefaultEmptyDropdown($list_cabang, "obj", "Cabang"), @$id_cabang); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?= form_label('Akses semua Cabang', "dd_allow_all_cabang", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?= form_dropdown(array("name" => "allow_all_cabang"), GetYaTidak(), @$allow_all_cabang, array('class' => 'form-control', 'id' => 'dd_allow_all_cabang')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group" id="list_cabang" style="display:none">
                                        <?= form_label('Cabang', "txt_id_cabang", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?= form_dropdown(array("name" => "arr_cabang[]"), $list_cabang, $arr_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang', 'multiple' => 'multiple')); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <?= form_label('Status', "txt_status", array("class" => 'col-sm-4 control-label')); ?>
                                        <div class="col-sm-8">
                                            <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <a href="<?php echo base_url() . 'index.php/user' ?>" class="btn btn-default"  >Cancel</a>
                                        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?php echo base_url() ?>assets/plugins/select2/select2.js"></script>
<script>
    $(document).ready(function () {
        $("#dd_allow_all_cabang").change(function () {
            var allow_all_cabang = $(this).val();
            if (allow_all_cabang == 0) {
                $("#list_cabang").show();
            } else {
                $("#list_cabang").hide();
            }
        }).change();

        $("select").select2();
    })
    $("#frm_user").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/user/user_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/user';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    });

</script>