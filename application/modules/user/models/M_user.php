<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_user extends CI_Model {

    public $table = '#_user';
    public $id = 'id_user';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function forgot_password($id_user) {
        $this->db->update($this->table, array("forgot_password" => RandomPassword()), array("id_user" => $id_user));
    }

    function clear_forgot_password($id_user) {
        $this->db->update("", array("forgot_password" => ''), array("id_user" => $id_user));
    }

    function AuthUserWithCodemd5($email, $code) {
        $user = $this->GetOneUser($email, 'email');
        if ($user != null) {
            if (md5($user->forgot_password) == $code) {
                return true;
            } else
                return false;
        } else {
            return false;
        }
    }

    function profile_change_password($model) {
        $user = $this->GetOneUser($model['user_id'], 'id_user');
        $message = '';
        if ($user != null) {
            if ($model['password'] == $model['confirm']) {
                if ($user->password_user <> sha1($model['lastpassword'])) {
                    $message .= 'Your last password didn\'t match with our password in the system<br/>';
                } else {
                    $this->db->update("#_user", array("password_user" => sha1($model['password'])), array("id_user" => $model['user_id']));
                }
            } else {
                $message .= 'Your password and confirm pin didn\'t match<br/>';
            }
        } else {
            $message .= 'We cannot find the user in database<br/>';
        }
        return $message;
    }
    
    function GetCabangUser($id_user)
    {
        $row=$this->GetOneUser($id_user,"id_user");
        if ($row != null) {
            if ($row->arr_cabang != "all") {
                $listidcabang = explode(",", $row->arr_cabang);
            } else {
                $this->db->from("#_cabang");
                $this->db->select("#_cabang.id_cabang");
                $listidcabang = $this->db->get()->result_array();
                $listidcabang = array_column($listidcabang, "id_cabang");
            }
        }
        else
        {
            $listidcabang=[];
        }
        return $listidcabang;
    }
    
    

    function login($username, $password, $type = 'username') {

        $message = '';
        $wherearray = array();
        $wherearray['lower(' . $type . ')'] = strtolower($username);
        $wherearray['password_user'] = sha1($password);
        $wherearray['status !='] = 2;
        $this->db->from($this->table);
        $this->db->where($wherearray);
        $row = $this->db->get()->row();
        if ($row == null) {
            $message .= "Username or password is wrong<br/>";
        } else {
            if ($row->status == 0) {
                $message .= 'your account not activated yet ';
            }
        }


        if ($message == '') {
            SetUserId($row->id_user);
            SetGroupId($row->id_group);
            SetUsername($row->username);
            SetCabangId($row->id_cabang);
            if($row->arr_cabang!="all")
            {
                $listidcabang= explode(",", $row->arr_cabang);
                
            }
            else
            {
                $this->db->from("#_cabang");
                $this->db->select("#_cabang.id_cabang");
                $listidcabang=$this->db->get()->result_array();
                $listidcabang= array_column($listidcabang, "id_cabang");
                
            }
            SetCabangAkses($listidcabang);
            $this->load->model("akses/m_akses");
            $group = $this->m_akses->GetOneAkses($row->id_group);
            $this->insert_log(['id_user' => $row->id_user, 'username' => $row->username]);
        }

        return $message;
    }

    function login_api($username, $password, $type = 'username') {

        $message = '';
        $data_akses = array();
        $data_login = array();
        $wherearray = array();
        $wherearray['lower(' . $type . ')'] = strtolower($username);
        $wherearray['password_user'] = sha1($password);
        $wherearray[$this->table.'.status !='] = 2;
        $this->db->from($this->table);
        $this->db->where($wherearray);
        // $this->db->join("#_pegawai","#_pegawai.id_user=#_user.id_user", "left");
        $row = $this->db->get()->row();
        if ($row == null) {
            $message .= "Username or password is wrong<br/>";
        } else {
            if ($row->status == 0) {
                $message .= 'your account not activated yet ';
            }
        }


        if ($message == '') {
            $row->id_pegawai = '';
            $this->db->from('#_pegawai');
            $this->db->where('#_pegawai.id_user', $row->id_user);
            $rowP = $this->db->get()->row();
            $row->id_pegawai = $rowP ? $rowP->id_pegawai : $row->id_pegawai;
            SetUserId($row->id_user);
            SetGroupId($row->id_group);
            SetUsername($row->username);
            $this->load->model("akses/m_akses");
            $group = $this->m_akses->GetOneAkses($row->id_group);
            $this->insert_log(['id_user' => $row->id_user, 'username' => $row->username]);
            $this->db->where('dgmi_akses_group.id_group', $row->id_group);
            $this->db->join('dgmi_form', 'dgmi_form.id_form=dgmi_akses_group.id_akses');
            $this->db->select('kode_form, form_name');
            $data_akses = $this->db->from('dgmi_akses_group')->get()->result_array();
            $data_login = array('id_user' => $row->id_user, 'username' => $row->username, 'id_group' => $row->id_group, 'nama_user' => $row->nama_user, 'id_cabang' => $row->id_cabang);
        }

        return array('data_login' => $row, 'data_akses' => $data_akses, 'message' => $message);
    }

    public function set_new_password($password, $pin = '') {
        $this->db->update("", array("password" => sha1($password), 'pin_user' => sha1($pin), 'forgot_password' => ''), array("id_user" => GetUserId()));
        SetMessageSession(1, 'Password has updated');
    }

    function GetOneUser($value, $key = 'username') {
        $this->db->from("#_user");
        $where = array();
        if ($key == 'username')
            $where["lower(" . $key . ")"] = strtolower($value);
        else
            $where[$key] = $value;

        $this->db->where($where);
        $user = $this->db->get()->row();


        return $user;
    }

    function GetDropDownUser($id_pangkalan = 0) {

        $listkota = GetTableData('#_user', 'id_user', array('username', 'nama_user'), array('#_user.status' => 1));

        return $listkota;
    }

    function profile_edit($model, $userid = 0) {
        if (CheckEmpty($userid)) {
            $userid = GetUserId();
        }

        if (array_key_exists('profilpic', $_FILES)) {
            if (CheckKey($_FILES['profilpic'], 'name')) {

                $config['upload_path'] = './assets/images/profile/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '1000000';
                $filename = date('Ymd') . '_' . $_FILES['profilpic']['name'];
                $config['file_name'] = $filename;
                $this->load->library('upload', $config);


                if (!$this->upload->do_upload('profilpic')) {
                    $filename = '';
                }
                unset($model['profilpic']);
                if (!CheckEmpty($filename)) {
                    $model['profilpic'] = $filename;
                }
            }
        }
        $model['update_date'] = GetDateNow();
        $model['update_by'] = GetUsername();

        $model['date_birth'] = DefaultTanggalDatabase($model['date_birth']);
        $this->db->update("", $model, array("id_user" => $userid));
    }

    // datatables
    function GetDatauser() {
        $this->load->library('datatables');
        $this->datatables->select('id_user,kode_user,nama_user,username,#_group.nama_group,#_cabang.nama_cabang,#_user.status');
        $this->datatables->from('#_user');
        //add this line for join
        $this->datatables->join('#_cabang', '#_user.id_cabang = #_cabang.id_cabang', 'left');
        $this->datatables->join('#_group', '#_user.id_group = #_group.id_group', 'left');
        $this->datatables->where(array('#_user.status !=' => 2));
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor(site_url('user/edit_user/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteuser($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_user');
        return $this->datatables->generate();
    }

    // get all
    function GetUserDetail($keyword, $type = 'id_user') {
        $this->db->where($type, $keyword);
        $user = $this->db->get($this->table)->row();
       

        return $user;
    }

    function UserManipulate($model) {
        $message = "";
        $array_cabang = @$model['arr_cabang'];
        if (CheckEmpty($array_cabang)) {
            $array_cabang = [];
        }

        
        if ((!in_array($model['id_cabang'], $array_cabang)) && !CheckEmpty($model['id_cabang'])) {
            array_push($array_cabang, $model['id_cabang']);
        }
        
        try {
            $model['id_group'] = ForeignKeyFromDb($model['id_group']);
            $isinclude = false;
            foreach ($array_cabang as $cabsatuan) {
                if ($cabsatuan == $model['id_cabang']) {
                    $isinclude = true;
                }
            }
            if (!$isinclude) {
                $array_cabang[] = $model['id_cabang'];
            }
            $model['arr_cabang'] = implode((array)$array_cabang,',');
            if(!CheckEmpty($model['allow_all_cabang']))
            {
                 $model['arr_cabang']='all';
            }
           
            unset($model['allow_all_cabang']);
              
            $id_user = $model['id_user'];
            if (CheckEmpty($model['id_user'])) {
                $model['created_date'] = GetDateNow();
                $model['kode_user'] = AutoIncrement("#_user", "P", "kode_user", 3);
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                $id_user = $this->db->insert_id();
                $message = 'User successfull added into database';
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_user" => $model['id_user']));
                $message = 'User has been updated';
            }
            if (@$model['arr_cabang']=="all") {
                $this->db->delete("#_user_cabang", array("id_user" => $id_user));
            } else {
                
                foreach ($array_cabang as $cabsatuan) {
                    $this->db->from("#_user_cabang");
                    $cab = $this->db->where(array("id_cabang" => $cabsatuan, "id_user" => $id_user))->get()->row();
                    if (!$cab) {
                        $this->db->insert("#_user_cabang", array("id_user" => $id_user, "id_cabang" => $cabsatuan));
                    }
                }
                $this->db->from("#_user_cabang");
                if (count($array_cabang) > 0) {
                    $this->db->where_not_in("id_cabang", $array_cabang);
                }
                $this->db->where(array("id_user" => $id_user));
                $this->db->delete();
            }

            SetMessageSession(1, $message);
            return array("st" => true, "msg" => $message);
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function UserDelete($id_user) {
//        try {
//            $this->db->delete($this->table, array('id_user' => $id_user));
//        } catch (Exception $ex) {
//            $model['updated_date'] = GetDateNow();
//            $model['status'] = 2;
//            $model['updated_by'] = ForeignKeyFromDb(GetUserId());
//            $this->db->update($this->table, $model, array('id_user' => $id_user));
//        }

        $model['updated_date'] = GetDateNow();
        $model['status'] = 2;
        $model['updated_by'] = ForeignKeyFromDb(GetUserId());
        $this->db->update($this->table, $model, array('id_user' => $id_user));
        return array("st" => true, "msg" => "User has been deleted from database");
    }

    function insert_log($model) {
        $model['last_login_date'] = date('Y-m-d');
        $model['last_login_time'] = date('H:i:s');
        $model['last_login_ip'] = $this->input->ip_address();

        $this->db->insert('#_user_log', $model);
    }

}
