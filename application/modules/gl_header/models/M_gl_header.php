<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_gl_header extends CI_Model
{

    public $table = '#_gl_header';
    public $id = 'id_gl_header';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDatagl_header() {
        $this->load->library('datatables');
        $this->datatables->select('id_gl_header,urut,kode_gl_header,nama_gl_header,type_umum,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        $this->db->order_by("urut","Asc");
        $this->db->order_by("nama_gl_header","asc");
        //add this line for join
        //$this->datatables->join('table2', 'lian_gl_header.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor("", 'Update', array('class' => 'btn btn-primary btn-xs', "onclick" => "editgl_header($1);return false;"));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletegl_header($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_gl_header');
        return $this->datatables->generate();
    }

    // get all
    function GetOneGl_header($keyword, $type = 'id_gl_header') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $gl_header = $this->db->get($this->table)->row();
        return $gl_header;
    }

    function Gl_headerManipulate($model) {
        try {
                $model['status'] = DefaultCurrencyDatabase($model['status']);
                $model['urut'] = DefaultCurrencyDatabase($model['urut']);

            if (CheckEmpty($model['id_gl_header'])) {
                $model['kode_gl_header'] = AutoIncrement($this->table, 'GH', 'kode_gl_header',3);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		return array("st" => true, "msg" => "Gl_header successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_gl_header" => $model['id_gl_header']));
		return array("st" => true, "msg" => "Gl_header has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownGl_header() {
        $listgl_header = GetTableData($this->table, 'id_gl_header', 'nama_gl_header', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listgl_header;
    }

    function Gl_headerDelete($id_gl_header) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_gl_header' => $id_gl_header));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Gl_header has been deleted from database");
    }


}