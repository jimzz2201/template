<div class="modal-header">
    Akun Type <?php echo @$button ?>
</div>
<div class="modal-body">
    <form id="frm_gl_header" class="form-horizontal form-groups-bordered validate" method="post">
        <input type="hidden" name="id_gl_header" value="<?php echo @$id_gl_header; ?>" /> 
        <div class="form-group">
            <?= form_label('Kode Gl', "txt_kode_gl_header", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?php
                $mergearray = array();
                $mergearray['disabled'] = "disabled";
                ?>
                <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_gl_header, 'name' => 'kode_gl_header', 'class' => 'form-control', 'id' => 'txt_kode_gl_header', 'placeholder' => 'Kode'))); ?>
            </div>
        </div>
        <div class="form-group">
            <?= form_label('Nama Gl', "txt_nama_gl_header", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?= form_input(array('type' => 'text', 'name' => 'nama_gl_header', 'value' => @$nama_gl_header, 'class' => 'form-control', 'id' => 'txt_nama_gl_header', 'placeholder' => 'Nama Gl')); ?>
            </div>

        </div>

        <div class="form-group">
            <?= form_label('Urutan', "txt_nama_gl_header", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-10">
                <?= form_input(array('type' => 'text', 'name' => 'urut', 'value' => DefaultCurrency(@$urut), 'class' => 'form-control', 'id' => 'txt_urut', 'placeholder' => 'Disc Pembulatan', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>

        </div>


        <div class="form-group">
            <?= form_label('Type Akun', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$type_umum, "name" => "type_umum"), DefaultEmptyDropdown(@$list_type_umum, "json", "Type Umum"), @$type_umum, array('class' => 'form-control', 'id' => 'type_umum')); ?>
            </div>
            <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
            <div class="col-sm-4">
                <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
            </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
        </div>

    </form>
</div>
<script>
    $("#frm_gl_header").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/gl_header/gl_header_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.reload();
                    $("#modalbootstrap").modal("hide");
                } else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;

    })
</script>