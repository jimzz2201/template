<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gl_header extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_gl_header');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K082";
        $header = "K080";
        $title = "Akun Type";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        CekModule($module);
        LoadTemplate($model, "gl_header/v_gl_header_index", $javascript);
    }

    public function get_one_gl_header() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_gl_header', 'Gl_header', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_gl_header->GetOneGl_header($model["id_gl_header"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Gl_header tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdatagl_header() {
        header('Content-Type: application/json');
        echo $this->m_gl_header->GetDatagl_header();
    }

    public function create_gl_header() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K041";
        $header = "K003";
        $row['title'] = "Add Gl_header";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_type_umum'] =array("A" => "Activa", "P" => "Passiva");
        $this->load->view('gl_header/v_gl_header_manipulate', $row);
    }

    public function gl_header_manipulate() {
        $message = '';

        $this->form_validation->set_rules('nama_gl_header', 'Nama Gl_header', 'trim|required');
        $this->form_validation->set_rules('type_umum', 'Type umum', 'trim|required');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_gl_header->Gl_headerManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_gl_header($id = 0) {
        $row = $this->m_gl_header->GetOneGl_header($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K082";
            $header = "K080";
            $row['title'] = "Edit Gl_header";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['list_type_umum'] =array("A" => "Activa", "P" => "Passiva");
            $this->load->view('gl_header/v_gl_header_manipulate', $row);
        } else {
            SetMessageSession(0, "Gl_header cannot be found in database");
            redirect(site_url('gl_header'));
        }
    }

    public function gl_header_delete() {
        $message = '';
        $this->form_validation->set_rules('id_gl_header', 'Gl_header', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_gl_header->Gl_headerDelete($model['id_gl_header']);
        }

        echo json_encode($result);
    }

}

/* End of file Gl_header.php */