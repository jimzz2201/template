<section class="content-header">
    <h1>
        Retur Kpu <?= @$button ?>
        <small>Retur Kpu</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Retur Kpu</li>
        <li class="active">Retur Kpu <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                        <form id="frm_retur_kpu" class="form-horizontal form-groups-bordered validate" method="post">
                        <input type="hidden" name="id_retur_kpu" value="<?php echo @$id_retur_kpu; ?>" /> 
                        <div class="form-group">
                            <?= form_label('Tanggal Retur Kpu', "txt_tanggal_retur_kpu", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'name' => 'tanggal_retur_kpu', 'value' => DefaultDatePicker(@$tanggal_retur_kpu), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_retur_kpu', 'placeholder' => 'Tanggal Retur Kpu')); ?>
                            </div>
                            <?= form_label('Type Retur', "type_retur", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_dropdown(array("name" => "type_retur"), DefaultEmptyDropdown(@$list_type_retur, "json", "Type Retur"), @$type_retur, array('class' => 'form-control select2', 'id' => 'dd_type_retur'));?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= form_label('No Retur Kpu', "txt_no_retur_kpu", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'name' => 'no_retur_kpu', 'value' => @$no_retur_kpu, 'class' => 'form-control', 'id' => 'txt_no_retur_kpu', 'placeholder' => 'No Retur Kpu')); ?>
                            </div>
                        </div>
                        <hr />

                        <div class="form-group">
                            <?= form_label('Type Unit', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-2">
                                <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori_search', 'class' => 'form-control select2', 'id' => 'dd_id_kategori_search', 'placeholder' => 'Kategori'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$id_kategori); ?>
                            </div>
                            <?= form_label('Unit', "dd_id_unit_search", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-3">
                                <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit_search', 'class' => 'form-control select2', 'id' => 'dd_id_unit_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                            </div>
                            <?= form_label('Supplier', "dd_id_customer_search", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-3">
                                <?= form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier, array('class' => 'form-control select2', 'id' => 'dd_id_supplier_search')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= form_label('DO Kpu', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-6">
                                <?php
                                    echo form_dropdown(array('type' => 'text', 'name' => 'id_do_kpu', 'class' => 'form-control select2', 'id' => 'dd_id_do_kpu', 'placeholder' => 'DO Kpu'), DefaultEmptyDropdown(@$list_do_kpu, "json", "DO Kpu"), @$id_kpu);
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= form_label('No Do Kpu', "txt_id_do_kpu", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'hidden', 'name' => 'id_do_kpu', 'value' => @$id_do_kpu, 'class' => 'form-control', 'id' => 'dd_id_do_kpu', 'placeholder' => 'Id Do Kpu')); ?>
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'no_do_kpu', 'value' => @$no_do_kpu, 'class' => 'form-control', 'id' => 'no_do_kpu', 'placeholder' => 'No Do Kpu')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?= form_label('No KPU', "txt_tanggal_retur_kpu", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'nomor_kpu', 'placeholder' => 'Nomor KPU')); ?>
                            </div>
                            <?= form_label('Supplier', "nama_supplier", array("class" => 'col-sm-1 control-label')); ?>
                            <div class="col-sm-4">
                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'nama_supplier', 'placeholder' => 'Nama Supplier')); ?>
                            </div>
                        </div>
                        <hr />
                        <div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr style="height:30px;background-color: blue;color:#FFFFFF;">
                                    <th>Unit</th>
                                    <th>Type Unit</th>
                                    <th>Kategori Unit</th>
                                    <th>VIN Num</th>
                                    <th>VIN</th>
                                    <th>Manuvacture Code</th>
                                    <th>Engine No</th>
                                    <th>No BPKB</th>
                                    <th>No Polisi</th>
                                    <th>Pool</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="serial_unit_list">
                                <?php
                                    if (!CheckEmpty(@$id_retur_kpu)) {
                                        foreach(@$list_unit_serial as $dt){
                                            $checked = ($dt['date_do_out'] == null) ? '' : 'checked';
                                            $table .= "<tr onclick='selectRow(".$dt['id_unit_serial'].")'>
                                                <td>".$dt['nama_unit']."</td>
                                                <td>".$dt['nama_type_unit']."</td>
                                                <td>".$dt['nama_kategori']."</td>
                                                <td>".$dt['vin_number']."</td>
                                                <td>".$dt['vin']."</td>
                                                <td>".$dt['manufacture_code']."</td>
                                                <td>".$dt['engine_no']."</td>
                                                <td>".$dt['no_bpkb']."</td>
                                                <td>".$dt['no_polisi']."</td>
                                                <td>".$dt['nama_pool']."</td>
                                                <td><input type='checkbox' style='width:20px;' class='selectUnit' " . $checked . " onclick='selectUnit(".$dt['id_unit_serial'].")' id='unit".$dt['id_unit_serial']."'></td>
                                            </tr>";
                                        }
                                        echo $table;
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <div id="areaUnit">
                        <?php
                            if (!CheckEmpty(@$id_retur_kpu)) {
                                foreach(@$list_unit_serial as $dt){
                                    if($dt['date_do_out'] != null)
                                        echo "<input type='hidden' name='id_serial_unit[]' value='" . $dt['id_unit_serial'] . "' id='unitInput" . $dt['id_unit_serial'] . "'>";
                                }
                            }
                        ?>
                        </div>
                        <hr />
                        <div class="form-group">
                            <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                            <div class="col-sm-8">
                                <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => "", 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <a href="<?php echo base_url().'index.php/retur_kpu' ?>" class="btn btn-default"  >Cancel</a>
                            <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                        </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });
        UpdateDOKpu();
        searchListDoKpu();
    });
    $(document).ready(function () {

        <?php if (CheckEmpty(@$id_retur_kpu)) { ?>
                    $(".select2").select2();
        <?php } else { ?>
                    $(".select2").select2();
                    <?php if (!CheckEmpty(@$is_view)) { ?>
                            $("input").attr("readonly", true);
                            $("textarea").attr("readonly", true);
                    <?php } ?>
        <?php } ?>
        $('#dd_id_supplier_search').select2({
            placeholder: "Pilih Supplier",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/supplier/search_supplier',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $("#dd_id_unit_search, #dd_id_kategori_search , #dd_id_supplier_search").change(function () {
            var id = $(this).attr("id");
            var id_unit_search = $("#dd_id_unit_search").val();
            if (id == "dd_id_kategori_search")
            {
                id_unit_search = 0;
                RefreshUnit();

            }
            LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/retur_kpu/get_do_kpu_list',
                dataType: 'json',
                data: {
                    id_kategori: $("#dd_id_kategori_search").val(),
                    id_unit: id_unit_search,
                    id_supplier: $("#dd_id_supplier_search").val()
                },
                success: function (data) {
                    $("#dd_id_do_kpu").empty();
                    if (data.st)
                    {
                        $("#dd_id_do_kpu").select2({data: data.list_do_kpu});
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
        })
    })
    function RefreshUnit()
    {
        var id_kategori = $("#dd_id_kategori_search").val();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_dropdown_unit',
            dataType: 'json',
            data: {
                id_kategori: id_kategori
            },
            success: function (data) {
                $("#dd_id_unit_search").empty();
                if (data.st)
                {
                    $("#dd_id_unit_search").select2({data: data.list_unit})
                }
                // ClearFormDetail();
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });

    }
    function searchListDoKpu () {
        LoadBar.show();
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/retur_kpu/get_do_kpu_list',
                dataType: 'json',
                data: {
                    id_kategori: $("#dd_id_kategori_search").val(),
                    id_unit: $("#dd_id_unit_search").val(),
                    id_supplier: $("#dd_id_supplier_search").val()
                },
                success: function (data) {
                    $("#dd_id_do_kpu").empty();
                    if (data.st)
                    {
                        $("#dd_id_do_kpu").select2({data: data.list_do_kpu});
                    }
                    LoadBar.hide();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                    LoadBar.hide();
                }
            });
    }

    function UpdateDOKpu()
    {

        $("#dd_id_do_kpu").change(function () {
            var id_do_kpu = $("#dd_id_do_kpu").val();
            var type_do = $("#dd_type_do").val();
            if (id_do_kpu != 0)
            {
                LoadBar.show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/do_kpu/get_one_do_kpu',
                    dataType: 'json',
                    data: {
                        id_do_kpu: id_do_kpu
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            $("#no_do_kpu").val(data.obj.nomor_do_kpu);
                            $("#nomor_kpu").val(data.obj.nomor_master);
                            $("#nama_supplier").val(data.obj.nama_supplier);
                            $("#areado").html("");
                            jumlahgenerate = 0;
                            showListUnit(data.obj.id_do_kpu_detail);
                        } else
                        {
                            ClearFormDetail();
                        }
                        LoadBar.hide();
                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                        ClearFormDetail();
                        LoadBar.hide();
                    }
                });
            } else
            {
                ClearFormDetail();
                LoadBar.hide();
            }
        })
    }

    function showListUnit(id_do_kpu_detail) {
        
        $.ajax({
            type: 'GET',
            url: baseurl + 'index.php/unit/get_list_unit_serial_kpu',
            data: {
                id_do_kpu_detail: id_do_kpu_detail
            },
            success: function (data) {
                // console.log(data);
                $("#serial_unit_list").html(data);
            }
        })
    }

    function selectRow(id) {
        var chk = $("#unit" + id).prop('checked');
        if(chk) {
            $("#unit" + id).prop('checked', false);
        } else {
            $("#unit" + id).prop('checked', true);

        }
        selectUnit(id);
    }

    function selectUnit(id_unit_serial) {
        var chk = $("#unit" + id_unit_serial).prop('checked');
        if(chk == true){
            var idUnit = "<input type='hidden' name='id_serial_unit[]' value='" + id_unit_serial + "' id='unitInput" + id_unit_serial + "'>";
            $("#areaUnit").append(idUnit);
        }else{
            $("#unitInput" + id_unit_serial).remove();
        }
    }

    $("#frm_retur_kpu").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/retur_kpu/retur_kpu_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/retur_kpu';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>