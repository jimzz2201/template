<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Retur_kpu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_retur_kpu');
        $this->load->model("do_kpu/m_do_kpu");
        $this->load->model("supplier/m_supplier");
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K108";
        $header = "K059";
        $title = "Retur Prospek";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        CekModule($module);
        LoadTemplate($model, "retur_kpu/v_retur_kpu_index", $javascript);
        
    } 
    public function get_one_retur_kpu() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_retur_kpu', 'Retur Prospek', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_retur_kpu->GetOneRetur_kpu($model["id_retur_kpu"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Retur Prospek tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdataretur_kpu() {
        header('Content-Type: application/json');
        echo $this->m_retur_kpu->GetDataretur_kpu();
    }
    
    public function create_retur_kpu() 
    {
        $this->load->model("customer/m_customer");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $this->load->model("kategori/m_kategori");
        $row=['button'=>'Add'];
        $javascript = array();
    	$module = "K108";
        $header = "K059";
        $row['title'] = "Add Retur Prospek";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_type_retur'] = array(array('id' => 1, 'text' => 'Pengurangan Piutang'), array('id' => 2, 'text' => 'Cash'));
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
	    LoadTemplate($row,'retur_kpu/v_retur_kpu_manipulate', $javascript);       
    }

    public function get_do_kpu_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();
       
        $list_do_kpu=$this->m_do_kpu->GetDropDownDoKpuForRetur($model);
        echo json_encode(array("list_do_kpu"=> DefaultEmptyDropdown($list_do_kpu,"json","DO KPU"),"st"=>true));
    }
    
    public function retur_kpu_manipulate() 
    {
        $message='';

        $this->form_validation->set_rules('tanggal_retur_kpu', 'Tanggal Retur KPU', 'trim|required');
        $this->form_validation->set_rules('id_do_kpu', 'Id Do KPU', 'trim|required');
        $this->form_validation->set_rules('no_retur_kpu', 'No Retur Prospek', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_retur_kpu->Retur_kpuManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_retur_kpu($id=0) 
    {
        $row = $this->m_retur_kpu->GetOneRetur_kpu($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K108";
            $header = "K059";
            $row['title'] = "Edit Retur Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	        LoadTemplate($row,'retur_kpu/v_retur_kpu_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Retur_kpu cannot be found in database");
            redirect(site_url('retur_kpu'));
        }
    }
    
    public function retur_kpu_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_retur_kpu', 'Retur_kpu', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_retur_kpu->Retur_kpuDelete($model['id_retur_kpu']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Retur_kpu.php */