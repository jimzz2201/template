<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_retur_kpu extends CI_Model
{

    public $table = '#_retur_kpu';
    public $id = 'id_retur_kpu';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDataretur_kpu() {
        $this->load->library('datatables');
        $this->datatables->select('id_retur_kpu,tanggal_retur_kpu,id_do_kpu,no_retur_kpu,keterangan');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_retur_kpu.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('retur_kpu/edit_retur_kpu/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteretur_kpu($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_retur_kpu');
        return $this->datatables->generate();
    }

    // get all
    function GetOneRetur_kpu($keyword, $type = 'id_retur_kpu') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $retur_kpu = $this->db->get($this->table)->row();
        return $retur_kpu;
    }

    function Retur_kpuManipulate($model) {
        try {
                $model['tanggal_retur_kpu'] = DefaultTanggalDatabase($model['tanggal_retur_kpu']);
                $model['id_do_kpu'] = ForeignKeyFromDb($model['id_do_kpu']);

            if (CheckEmpty($model['id_retur_kpu'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		SetMessageSession(1, 'Retur_kpu successfull added into database');
		return array("st" => true, "msg" => "Retur_kpu successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_retur_kpu" => $model['id_retur_kpu']));
		SetMessageSession(1, 'Retur_kpu has been updated');
		return array("st" => true, "msg" => "Retur_kpu has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownRetur_kpu() {
        $listretur_kpu = GetTableData($this->table, 'id_retur_kpu', '', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listretur_kpu;
    }

    function Retur_kpuDelete($id_retur_kpu) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_retur_kpu' => $id_retur_kpu));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Retur_kpu has been deleted from database");
    }


}