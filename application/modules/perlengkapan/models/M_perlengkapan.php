<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_perlengkapan extends CI_Model
{

    public $table = '#_perlengkapan';
    public $id = 'id_perlengkapan';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }

    // datatables
    function GetDataperlengkapan($params) {
        $this->load->library('datatables');
        $this->datatables->select('id_perlengkapan,id_unit_sereal,tanggal_do,tanggal_terima_dokumen,tanggal_kirim_dokumen,nama_jenis_dokumen,keterangan,penyerah,penerima,file');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_jenis_dokumen', 'dgmi_perlengkapan.id_jenis_dokumen = #_jenis_dokumen.id_jenis_dokumen', 'left');
        $extra=[];
        $where=[];
        if (!CheckEmpty(@$params['id_unit_serial'])) {
            $where['#_perlengkapan.id_unit_sereal'] = $params;
        }
        if (count($where)) {
            $this->db->where($where);
        }
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url("perlengkapan/edit_perlengkapan/$1/".@$params['id_unit_serial']), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteperlengkapan($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_perlengkapan');
        return $this->datatables->generate();
    }

    // get all
    function GetOnePerlengkapan($keyword, $type = 'id_perlengkapan') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $perlengkapan = $this->db->get($this->table)->row();
        return $perlengkapan;
    }

    function CountImg ($id_perlengkapan) {
        $this->db->where('id_perlengkapan', $id_perlengkapan);
        $this->db->select('file');
        $data = $this->db->get('dgmi_perlengkapan')->row();
        return $data;
    }

    function PerlengkapanManipulate($model) {
        try {
                $model['id_unit_sereal'] = ForeignKeyFromDb($model['id_unit_sereal']);
                $model['tanggal_do'] = DefaultTanggalDatabase($model['tanggal_do']);
                $model['tanggal_terima_dokumen'] = DefaultTanggalDatabase($model['tanggal_terima_dokumen']);
                $model['tanggal_kirim_dokumen'] = DefaultTanggalDatabase($model['tanggal_kirim_dokumen']);
                $model['id_jenis_dokumen'] = ForeignKeyFromDb($model['id_jenis_dokumen']);

            if (CheckEmpty($model['id_perlengkapan'])) {
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Perlengkapan successfull added into database');
                return array("st" => true, "msg" => "Perlengkapan successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_perlengkapan" => $model['id_perlengkapan']));
                SetMessageSession(1, 'Perlengkapan has been updated');
                return array("st" => true, "msg" => "Perlengkapan has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownPerlengkapan() {
        $listperlengkapan = GetTableData($this->table, 'id_perlengkapan', '', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listperlengkapan;
    }

    function PerlengkapanDelete($id_perlengkapan) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_perlengkapan' => $id_perlengkapan));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Perlengkapan has been deleted from database");
    }


}