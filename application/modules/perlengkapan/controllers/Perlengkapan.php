<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Perlengkapan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_perlengkapan');
        $this->load->helper(array('form', 'url', 'file'));
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K076";
        $header = "K059";
        $title = "Perlengkapan";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	    CekModule($module);
	    LoadTemplate($model, "perlengkapan/v_perlengkapan_index", $javascript);
        
    } 
    public function get_one_perlengkapan() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_perlengkapan', 'Perlengkapan', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_perlengkapan->GetOnePerlengkapan($model["id_perlengkapan"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Perlengkapan tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdataperlengkapan() {
        header('Content-Type: application/json');
        $params = $this->input->post("id_unit_serial");
        echo $this->m_perlengkapan->GetDataperlengkapan($params);
    }

    public function create_perlengkapan($id_unit_sereal) {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K076";
        $header = "K059";
        $title = "Perlengkapan";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("jenis_dokumen/m_jenis_dokumen");
        $model['list_jenis_dokumen'] = $this->m_jenis_dokumen->GetDropDownJenis_dokumen();
        $model['id_unit_sereal'] = $id_unit_sereal;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "perlengkapan/v_perlengkapan_manipulate", $javascript, $css);
    }
    
    public function perlengkapan_manipulate() 
    {
        $message='';

        $this->form_validation->set_rules('id_unit_sereal', 'Id Unit Sereal', 'trim|required');
        $this->form_validation->set_rules('tanggal_do', 'Tanggal Do', 'trim|required');
        $this->form_validation->set_rules('tanggal_terima_dokumen', 'Tanggal Terima Dokumen', 'trim|required');
        $this->form_validation->set_rules('tanggal_kirim_dokumen', 'Tanggal Kirim Dokumen', 'trim|required');
        $this->form_validation->set_rules('id_jenis_dokumen', 'Id Jenis Dokumen', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');
        $this->form_validation->set_rules('penyerah', 'Penyerah', 'trim|required');
        $this->form_validation->set_rules('penerima', 'Penerima', 'trim|required');
        $model=$this->input->post();
        
        if ($this->form_validation->run() === FALSE || $message !== '') {
            // echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
            $this->session->set_flashdata(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
            // $this->create_perlengkapan($model['id_unit_sereal']);
            redirect('perlengkapan/create_perlengkapan/'.$model['id_unit_sereal']);
            // $anchor = $model['id_perlengkapan'] ? $this->edit_perlengkapan($model['id_perlengkapan'], $model['id_unit_sereal']) : $this->create_perlengkapan($model['id_unit_sereal']);
            // return $anchor;
        } else {
            if(!empty($_FILES['files']['name'])){
                $cnt_img = 0;
                if (CheckEmpty($model['id_perlengkapan'])){
                    $dataImg = $this->m_perlengkapan->CountImg($model['id_perlengkapan']);
                    $cnt_img = count(explode('#', $dataImg));
                }
                
                $filesCount = count($_FILES['files']['name']);
                $fileName = array();
                for($i = 0; $i < $filesCount; $i++){
                    $cnt_img++;
                    $code = date('ymdhis');
                    $path = base_url(). 'upload/doc/file' . $model['id_unit_sereal'] . '_' . $model['id_jenis_dokumen'] . '_' . ($cnt_img + 1);
                    if (file_exists($path)){
                        delete_files($path);
                    }

                    $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['files']['size'][$i];
                    
                    $uploadPath = './upload/doc/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                    $config['overwrite'] = true;
                    $config['file_name'] = 'file' . $model['id_unit_sereal'] . '_' . $model['id_jenis_dokumen'] . '_' . ($cnt_img + 1);
                    
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    
                    if($this->upload->do_upload('file')){
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                        $fileName[] = $fileData['file_name'];
                    }
                }
                $model['file'] = implode('#', $fileName);
                if (!empty($uploadData)) {
                    $status=$this->m_perlengkapan->PerlengkapanManipulate($model);
                    SetMessageSession($status['st'], $status['msg']);
                    $model['id_perlengkapan'] ? $this->edit_perlengkapan($model['id_perlengkapan'], $model['id_unit_sereal']) : $this->create_perlengkapan($model['id_unit_sereal']);
                    // echo json_encode($status);
                } else {
                    // echo json_encode(array('st' => false, 'msg' => 'Upload Error '));
                    SetMessageSession(false, 'Gagal Menyimpan Data');
                    $model['id_perlengkapan'] ? $this->edit_perlengkapan($model['id_perlengkapan'], $model['id_unit_sereal']) : $this->create_perlengkapan($model['id_unit_sereal']);

                }
            }
        }
    }
    
    public function edit_perlengkapan($id=0, $id_unit_sereal) 
    {

        $row = $this->m_perlengkapan->GetOnePerlengkapan($id);
        
        if ($row) {
            $javascript = array();
            $css = array();
            $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
            $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
	        $module = "K076";
            $header = "K059";
            $row['title'] = "Edit Perlengkapan";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
            $this->load->model("jenis_dokumen/m_jenis_dokumen");
            $row['list_jenis_dokumen'] = $this->m_jenis_dokumen->GetDropDownJenis_dokumen();
            $row['id_unit_sereal'] = $row['id_unit_sereal'];
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
	        LoadTemplate($row,'perlengkapan/v_perlengkapan_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Perlengkapan cannot be found in database");
            redirect(site_url('perlengkapan'));
        }
    }
    
    public function perlengkapan_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_perlengkapan', 'Perlengkapan', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_perlengkapan->PerlengkapanDelete($model['id_perlengkapan']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Perlengkapan.php */