
<section class="content-header">
    <h1>
        Perlengkapan
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Perlengkapan</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <div class=" headerbutton">

                <?php echo anchor(site_url('perlengkapan/create_perlengkapan'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    function deleteperlengkapan(id_perlengkapan) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/perlengkapan/perlengkapan_delete',
                    dataType: 'json',
                    data: {
                        id_perlengkapan: id_perlengkapan
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
    }
    $(document).ready(function () {
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "perlengkapan/getdataperlengkapan", "type": "POST"},
            columns: [
                {
                    data: "id_perlengkapan",
                    title: "Kode",
                    orderable: false
                },
                {data: "id_unit_sereal", orderable: false, title: "Id Unit Sereal"},
                {data: "tanggal_do", orderable: false, title: "Tanggal Do",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "tanggal_terima_dokumen", orderable: false, title: "Tanggal Terima Dokumen",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "tanggal_kirim_dokumen", orderable: false, title: "Tanggal Kirim Dokumen",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "id_jenis_dokumen", orderable: false, title: "Id Jenis Dokumen"},
                {data: "keterangan", orderable: false, title: "Keterangan"},
                {data: "penyerah", orderable: false, title: "Penyerah"},
                {data: "penerima", orderable: false, title: "Penerima"},
                {data: "file", orderable: false, title: "File"},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        return data == 1 ? "Active" : "Not Active";
                    }},
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
