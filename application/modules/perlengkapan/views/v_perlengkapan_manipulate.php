<section class="content-header">
    <h1>
        Perlengkapan <?= @$button ?>
        <small>Perlengkapan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Perlengkapan</li>
        <li class="active">Perlengkapan <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <form id="frm_perlengkapan" class="form-horizontal form-groups-bordered validate" method="post" autocomplete="off" enctype="multipart/form-data" action="<?php echo base_url() . 'index.php/perlengkapan/perlengkapan_manipulate' ?>">
                                <!-- <form id="frm_perlengkapan" class="form-horizontal form-groups-bordered validate" autocomplete="off"> -->
                                <input type="hidden" name="id_perlengkapan" value="<?php echo @$id_perlengkapan; ?>" /> 
                                <input type="hidden" name="id_unit_sereal" id="id_unit_sereal" value="<?php echo @$id_unit_sereal; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Tanggal Do', "txt_tanggal_do", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'tanggal_do', 'value' => DefaultDatePicker(@$tanggal_do), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_do', 'placeholder' => 'Tanggal Do')); ?>
                                    </div>
                                    <?= form_label('Tanggal Terima Dokumen', "txt_tanggal_terima_dokumen", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'tanggal_terima_dokumen', 'value' => DefaultDatePicker(@$tanggal_terima_dokumen), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_terima_dokumen', 'placeholder' => 'Tanggal Terima Dokumen')); ?>
                                    </div>
                                    <?= form_label('Tanggal Kirim Dokumen', "txt_tanggal_kirim_dokumen", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'tanggal_kirim_dokumen', 'value' => DefaultDatePicker(@$tanggal_kirim_dokumen), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_kirim_dokumen', 'placeholder' => 'Tanggal Kirim Dokumen')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Jenis Dokumen', "txt_id_jenis_dokumen", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_jenis_dokumen', 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Jenis Dokumen'), DefaultEmptyDropdown(@$list_jenis_dokumen, "json", "Jenis Dokumen"), @$id_jenis_dokumen); ?>
                                    </div>
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'keterangan', 'value' => @$keterangan, 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Penyerah', "txt_penyerah", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'penyerah', 'value' => @$penyerah, 'class' => 'form-control', 'id' => 'txt_penyerah', 'placeholder' => 'Penyerah')); ?>
                                    </div>
                                    <?= form_label('Penerima', "txt_penerima", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'penerima', 'value' => @$penerima, 'class' => 'form-control', 'id' => 'txt_penerima', 'placeholder' => 'Penerima')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('File', "txt_file", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <input type="file" name="files[]" multiple>
                                        <div>
                                            <?php
                                            if (@$id_perlengkapan) {
                                                $img = explode('#', @$file);
                                                foreach ($img as $row) {
                                                    echo "<img src='" . base_url() . 'upload/doc/' . $row . "' style='padding:3px;max-width:150px;max-height:150px;'>";
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/persediaan_unit' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="portlet-body form">
                        <table class="table table-striped table-bordered table-hover" id="mytable"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("select").select2();
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'dd M yy',
        });

        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": baseurl + "index.php/perlengkapan/getdataperlengkapan", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "id_unit_serial": $("#id_unit_sereal").val()
                    });
                }},
            columns: [
                {
                    data: "id_perlengkapan",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal_do", orderable: false, title: "Tanggal Do",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "tanggal_terima_dokumen", orderable: false, title: "Tanggal Terima Dokumen",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "tanggal_kirim_dokumen", orderable: false, title: "Tanggal Kirim Dokumen",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "nama_jenis_dokumen", orderable: false, title: "Jenis Dokumen"},
                {data: "keterangan", orderable: false, title: "Keterangan"},
                {data: "penyerah", orderable: false, title: "Penyerah"},
                {data: "penerima", orderable: false, title: "Penerima"},
                {data: "file", orderable: false, title: "File",
                    mRender: function (data, type, row) {
                        var img = '';
                        data.split("#").forEach(myFunction);
                        function myFunction(item) {
                            img += "<img src='" + baseurl + 'upload/doc/' + item + "' style='padding:3px;max-width:70px;max-height:70px;'>";
                        }
                        return img;
                    }},
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });

    function deleteperlengkapan(id_perlengkapan) {
        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/perlengkapan/perlengkapan_delete',
                    dataType: 'json',
                    data: {
                        id_perlengkapan: id_perlengkapan
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
    }

    // $('#btt_modal_ok').click(function () {
    // 		console.log('test');
    // 		var file_data = $('#frm_perlengkapan').prop('files')[0];
    // 		var form_data = new FormData();
    // 		form_data.append('file', file_data);
    // 		console.log(dataToSend);
    // 		console.log('form_data');return false;
    // })

    // $("#frm_perlengkapan").submit(function () {
    // 		// var dataToSend = $(this).serialize() + '&file='+new FormData(this);
    // 		// var attach = new FormData($("#frm_perlengkapan")[0]);
    // var file_data = $(this).prop('files')[0];return false;
    // 		var form_data = new FormData();
    // 		// form_data.append('file', file_data);
    // console.log(dataToSend);
    // 		console.log('form_data');return false;
    //     $.ajax({
    //         type: 'POST',
    //         url: baseurl + 'index.php/perlengkapan/perlengkapan_manipulate',
    //         dataType: 'json',
    //         data: dataToSend,
    //         success: function (data) {
    //             if (data.st)
    //             {
    //                 // window.location.href=baseurl + 'index.php/perlengkapan';
    // 								messagesuccess(data.msg);
    //             }
    //             else
    //             {
    //                 messageerror(data.msg);
    //             }

    //         },
    //         error: function (xhr, status, error) {
    //             messageerror(xhr.responseText);
    //         }
    //     });
    //     return false;

    // })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>