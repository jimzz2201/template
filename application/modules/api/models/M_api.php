<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_api extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function login_api($username, $password, $type = 'username') {
        $message = [];
        $wherearray = array();
        $wherearray['lower(' . $type . ')'] = strtolower($username);
        $wherearray['password_user'] = sha1($password);
        $wherearray['#_user.status !='] = 2;
        $this->db->from("#_user");
        $this->db->join("#_pegawai", "#_pegawai.id_user=#_user.id_user", "left");
        $this->db->select("#_user.*,#_pegawai.id_pegawai");
        $this->db->where($wherearray);
        $row = $this->db->get()->row();
        if ($row == null) {
            $message[] = "Username or password is wrong";
        } else {
            if ($row->status == 0) {
                $message[] = 'your account not activated yet ';
            }
            unset($row->password_user);
        }

        if (count($message) == 0) {

            if ($row->arr_cabang != "all") {
                $listidcabang = explode(",", $row->arr_cabang);
            } else {
                $this->db->from("#_cabang");
                $this->db->select("#_cabang.id_cabang");
                $listidcabang = $this->db->get()->result_array();
                $listidcabang = array_column($listidcabang, "id_cabang");
            }
            $row->listcabangakses = $listidcabang;
        }



        return array("st" => count($message) == 0, "msg" => $message, "obj" => count($message) == 0?$row:null);
    }

}
