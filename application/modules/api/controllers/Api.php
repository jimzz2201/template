<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends BD_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('api/m_api');
        $this->output->set_header('Access-Control-Allow-Origin: *');
        $this->output->set_header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
    }

    public function login_post() {
        $model = $this->input->post();
        $message = [];
        $data['st'] = false;
        if (CheckEmpty(@$model['username'])) {
            $message[] = "Username Harus Diisi";
        }
        if (CheckEmpty(@$model['password'])) {
            $message[] = "Password Harus Diisi";
        }
        if (count($message) == 0) {
            $data = $this->m_api->login_api($model['username'], $model['password']);
        } else {
            $data['msg'] = implode("\n ", $message);
        }
        ReturnApi($data);
    }

    public function total_piutang_post() {
        $model = $this->input->post();
        $message = [];
        $listcabangakses = [];

        $piutang = 0;
        if (CheckEmpty($model['id_user'])) {
            $message[] = "User required";
        } else {
            if (CheckEmpty(@$model['listcabangakses'])) {
                $this->load->model("user/m_user");
                $listcabangakses = $this->m_user->GetCabangUser($model['id_user']);
            } else {
                $listcabangakses = explode(",", $model['listcabangakses']);
            }
        }
        if (count($message) == 0) {
            $this->load->model("pembayaran/m_pembayaran_piutang");
            $piutang = $this->m_pembayaran_piutang->GetTotalPiutang($listcabangakses);
            //  $data['total_piutang']=
        }
        $data['total_piutang'] = DefaultCurrency($piutang);
        $data['st'] = count($message) == 0;
        $data['msg'] = implode("\n ", $message);

        ReturnApi($data);
    }

    public function getdataprospek_post() {
        $str = $this->input->post("extra_search");
        $this->load->model("prospek/m_prospek");
        parse_str($str, $params);
        // $from = 0;
        $count = true;
        $jumlah_data = $this->m_prospek->GetDataprospek_api($params, null, null, $count);

        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'index.php/api/getdataprospek';
        $config['total_rows'] = count($jumlah_data);
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
        $config['num_links'] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $from = $page * $config['per_page'];
        $next_page = $page + 2;
        $data['data'] = $this->m_prospek->GetDataprospek_api($params, $config['per_page'], $from, false);
        $data['from'] = $from;
        $data['per_page'] = $config['per_page'];
        $data['next_page_url'] = $config['base_url'] . '/' . $next_page;
        $data['path'] = $config['base_url'];
        $data['total'] = $config['total_rows'];
        $data['current_page'] = $page + 1;
        $data['last_page'] = ceil($config['total_rows'] / $config['per_page']);
     
        ReturnApi($data);
    }

    public function requiretment_prospek_post() {

        $model = $this->input->post();
        $message = [];
        $listcabangakses = [];
        $id_cabang = null;
        if (CheckEmpty($model['id_user'])) {
            $message[] = "User required";
        } else {
            if (CheckEmpty(@$model['listcabangakses'])) {
                $this->load->model("user/m_user");
                $listcabangakses = $this->m_user->GetCabangUser($model['id_user']);
            } else {
                $listcabangakses = explode(",", $model['listcabangakses']);
            }
        }
        $row['st'] = false;
        if (count($message) == 0) {
            $this->load->model('prospek/m_prospek');
            $this->load->model("warna/m_warna");
            $this->load->model("jenis_plat/m_jenis_plat");
            $this->load->model("customer/m_customer");
            $this->load->model("kategori/m_kategori");
            $this->load->model("type_body/m_type_body");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("payment_method/m_payment_method");
            $this->load->model("unit/m_unit");
            $this->load->model("pegawai/m_pegawai");
            $this->load->model("leasing/m_leasing");
            $this->load->model("user/m_user");
            $this->load->model("cabang/m_cabang");
            $row['tanggal_prospek'] = GetDateNow();
            $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
            array_unshift($row['list_jenis_plat'], array('id' => 0, 'text' => 'Pilih Jenis Plat'));
            $row['list_warna'] = $this->m_warna->GetDropDownWarna();
            array_unshift($row['list_warna'], array('id' => 0, 'text' => 'Pilih Warna'));
            $is_lihat_cabang = CekModule("K118", false, $model['id_user']);
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer($model['id_pegawai'], null, $is_lihat_cabang ? $listcabangakses : []);
            array_unshift($row['list_customer'], array('id' => 0, 'text' => 'Pilih Customer'));
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            array_unshift($row['list_kategori'], array('id' => 0, 'text' => 'Pilih Kategori'));
            $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
            array_unshift($row['list_type_body'], array('id' => 0, 'text' => 'Pilih Type Body'));
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            array_unshift($row['list_type_unit'], array('id' => 0, 'text' => 'Pilih Type Unit'));
            $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
            array_unshift($row['list_payment_method'], array('id' => 0, 'text' => 'Pilih Paymen Method'));
            $row['list_unit'] = $this->m_unit->GetDropDownEquipment_api();
            array_unshift($row['list_unit'], array('id' => 0, 'text' => 'Pilih Unit', 'price' => 0));
            $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
            array_unshift($row['list_unit_bus'], array('id' => 0, 'text' => 'Pilih Unit Bus'));
            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai("json", $is_lihat_cabang ? $listcabangakses : []);
            array_unshift($row['list_pegawai'], array('id' => 0, 'text' => 'Pilih Pegawai'));
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
            array_unshift($row['list_leasing'], array('id' => 0, 'text' => 'Pilih Leasing'));
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", null, $listcabangakses);
            array_unshift($row['list_cabang'], array('id' => 0, 'text' => 'Pilih Cabang'));
            $data["row"] = $row;
        }

        $row['st'] = count($message) == 0;
        $row['msg'] = implode("\n ", $message);

        ReturnApi($row);
    }

}

/* End of file Customer.php */