<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_penjualan extends CI_Controller {

    private $_title = "Laporan Penjualan Detail";
    private $_title_penjualan = "Laporan Penjualan";
    private $_equipment = 0;
    private $_off_road = 0;
    private $_on_road = 0;
    private $_harga_unit = 0;
    private $_biaya_bbn = 0;

    function __construct() {
        parent::__construct();
        $this->load->model('m_report_penjualan');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K067";
        $header = "K005";
        $title = "Penjualan";
        CekModule($module);
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("customer/m_customer");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $status[] = array();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Approved");
        $status[] = array("id" => "2", "text" => "Rejected");
        $status[] = array("id" => "3", "text" => "Nego");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $status[] = array("id" => "7", "text" => "Approved Sudah Di Buka Jual");
        $status[] = array("id" => "8", "text" => "Approved Belum Di Buka Jual");
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal SPK");
		$jenis_pencarian[] = array("id" => "tanggal_buka_jual", "text" => "Tanggal Buka Jual");
        $model['list_pencarian'] = $jenis_pencarian;
        $sufix = "_r_prospek";
        $model['start_date'] = GetCookieSetting("start_date" . $sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $sufix, GetDateNow());
        $model['status'] = GetCookieSetting("status" . $sufix, GetCookieSetting("search".$sufix)=="1"?0:7);
        $model['id_cabang'] = GetCookieSetting("id_cabang" . $sufix, 0);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian".$sufix);
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['status'] = 1;
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "report/v_report_penjualan_index", $javascript, $css);
    }

    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K103";
        $header = "K005";
        $title = "Penjualan";
        CekModule($module);
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $status[] = array();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Approved");
        $status[] = array("id" => "2", "text" => "Rejected");
        $status[] = array("id" => "3", "text" => "Nego");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $status[] = array("id" => "7", "text" => "Approved Sudah Di Buka Jual");
        $status[] = array("id" => "8", "text" => "Approved Belum Di Buka Jual");
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal SPK");
        $model['list_pencarian'] = $jenis_pencarian;
        $sufix = "_r_prospek_detail";
        $model['start_date'] = GetCookieSetting("start_date" . $sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $sufix, GetDateNow());
        $model['status'] = GetCookieSetting("status" . $sufix, GetCookieSetting("search".$sufix)=="1"?0:7);
        $model['id_cabang'] = GetCookieSetting("id_cabang" . $sufix, 0);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian".$sufix);
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("kategori/m_kategori");
        $model['status'] = 1;
    
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $model['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai("json", GetCabangAkses());
        
        $model['list_supervisor'] = $this->m_pegawai->GetDropDownSupervisor();
        
        $this->load->model("customer/m_customer");
        $model['list_supplier'] = $this->m_customer->GetDropDownCustomer();
           

        LoadTemplate($model, "report/v_report_penjualan_detail", $javascript, $css);
    }

    function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function mainTitle2() {
        $title = array('text' => $this->_title_penjualan, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }

    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal', 'valueKey' => 'tanggal_prospek', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Bulan', 'valueKey' => 'bulan', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Customer', 'valueKey' => 'customer_name', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Alamat', 'valueKey' => 'alamat', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'Telp', 'valueKey' => 'no_telp', 'colWidth' => 15),
            array('colName' => 'NO SPK', 'valueKey' => 'no_prospek', 'colWidth' => 15),
            array('colName' => 'Salesman', 'valueKey' => 'nama_pegawai', 'colWidth' => 15),
            array('colName' => 'Cabang', 'valueKey' => 'nama_cabang', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Manager', 'valueKey' => 'supervisor', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Kategori', 'valueKey' => 'nama_kategori', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Unit', 'valueKey' => 'nama_unit', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Tanggal DO', 'valueKey' => 'tanggal_do', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'No DO', 'valueKey' => 'nomor_do_prospek', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Buka Jual', 'valueKey' => 'tanggal_buka_jual', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'No Buka Jual', 'valueKey' => 'nomor_buka_jual', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'No Rangka', 'valueKey' => 'vin_number', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'No Mesin', 'valueKey' => 'engine_no', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'VIN', 'valueKey' => 'tahun', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Off Road', 'valueKey' => 'harga_off_the_road', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'BBN', 'valueKey' => 'biaya_bbn', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'OTR', 'valueKey' => 'harga_on_the_road', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Equipment', 'valueKey' => 'total_equipment', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Grandtotal', 'valueKey' => 'harga_jual_unit', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
        );

        return $col;
    }

    function colSet2() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal', 'valueKey' => 'tanggal_prospek', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Bulan', 'valueKey' => 'bulan', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Customer', 'valueKey' => 'nama_customer', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Jenis Plat', 'valueKey' => 'nama_jenis_plat', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'Nama Unit', 'valueKey' => 'nama_unit', 'colWidth' => 15),
            array('colName' => 'Jumlah Unit', 'valueKey' => 'jumlah_unit', 'colWidth' => 15),
            array('colName' => 'Warna', 'valueKey' => 'nama_warna', 'colWidth' => 15),
            array('colName' => 'Tahun', 'valueKey' => 'tahun', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Harga Jual', 'valueKey' => 'harga_jual_unit', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Payment Method', 'valueKey' => 'nama_payment_method', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Salesman', 'valueKey' => 'nama_pegawai', 'colWidth' => 15, 'bold' => true),
        );

        return $col;
    }

    function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }

    function export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_penjualan->GetDataReportpenjualan($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Penjualan",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title_penjualan),
                'mainTitle' => $this->mainTitle2(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet2(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    function detail_export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_penjualan->GetDataReportpenjualanDetail($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Penjualan Detail",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    function footInfo() {
        $info = array();
        $info[] = array(
            array('text' => 'Total', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 4),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
        );

        return $info;
    }

    function rowSet($data) {
        $result = array();
        $nomor = 1;
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['tanggal_prospek'] = DefaultTanggal($row['tanggal_prospek']);
            $row['tanggal_do'] = DefaultTanggal($row['tanggal_do']);
            $row['tanggal_buka_jual'] = DefaultTanggal($row['tanggal_buka_jual']);
            $row['harga_off_road'] = (int) $row['harga_off_road'];
            $row['harga_on_the_road'] = (int) $row['harga_on_the_road'];
            $row['total_equipment'] = (int) $row['total_equipment'];
            $row['harga_jual_unit'] = (int) $row['harga_jual_unit'];
            $row['biaya_bbn'] = (int) $row['biaya_bbn'];
            $row['bulan'] = GetMonth((int)date('m', strtotime($row['tanggal_prospek'])));
           
            $nomor++;

            if ($row['status'] != 2) {
                $this->_off_road += (int) $row['harga_off_road'];
                $this->_on_road += (int) $row['harga_on_the_road'];
                $this->_equipment += (int) $row['total_equipment'];
                $this->_harga_unit += (int) $row['harga_jual_unit'];
                $this->_biaya_bbn += (int) $row['biaya_bbn'];
            }
            $result[]=$row;
        }

        return $result;
    }

    public function per_group() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K055";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        $this->load->model("jenis/m_jenis");
        $model['list_jenis'] = $this->m_jenis->GetDropDownJenis();
        $this->load->model("merek/m_merek");
        $model['list_merek'] = $this->m_merek->GetDropDownMerek();
        LoadTemplate($model, "report/v_report_penjualan_per_group", $javascript, $css);
    }

    public function pajak_masukan() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K056";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/js/icheck/icheck.min.js";
        $javascript[] = "assets/js/icheck/icheck-active.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        LoadTemplate($model, "report/v_report_penjualan_pajak_masukan", $javascript, $css);
    }

    public function getdatapenjualan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        
        
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if($key=="status"&&$value==0)
            {
                SetCookieSetting( "search_prospek", 1);
            }
            SetCookieSetting($key . "_r_prospek", $value);
        }
        
        echo $this->m_report_penjualan->GetDataReportpenjualan($params);
    }

    public function getdatapenjualandetail() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if($key=="status"&&$value==0)
            {
                SetCookieSetting( "search"."_r_prospek_detail", 1);
            }
            SetCookieSetting($key . "_r_prospek_detail", $value);
        }
        echo $this->m_report_penjualan->GetDataReportpenjualanDetail($params);
    }

    public function getdatapenjualanpergroup() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdatapenjualanpajakmasukan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

}

/* End of file Barang.php */