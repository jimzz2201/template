<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_mutasi extends CI_Controller {

    private $_title = "Laporan Mutasi";
    private $_title_mutasi = "Laporan Mutasi";
    private  $_prefix="_report_mutasi";
    function __construct() {
        parent::__construct();
        $this->load->model('m_report_mutasi');
        $this->load->model("cabang/m_cabang");
        $this->load->model("type_unit/m_type_unit");
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K119";
        $header = "K005";
        $title = "Mutasi";
        CekModule($module);
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("customer/m_customer");
        $this->load->model("pool/m_pool");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $status[] = array();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Approved");
        $status[] = array("id" => "2", "text" => "Rejected");
        $status[] = array("id" => "3", "text" => "Closed");
        $status[] = array("id" => "5", "text" => "Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['status'] = 1;
        $model['start_date'] = GetCookieSetting("start_date".$this->_prefix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date".$this->_prefix, GetDateNow());
        $model['list_pencarian'] = $jenis_pencarian;
        $model['value_status'] = GetCookieSetting("status".$this->_prefix, GetCookieSetting("search_buka_jual") == "1" ? 0 : 7);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian".$this->_prefix);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_prefix, "nomor_buka_jual");
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PROSPEK");
        $jenis_keyword[] = array("id" => "nomor_buka_jual", "text" => "NO BUKA JUAL");
        $jenis_keyword[] = array("id" => "engine_no", "text" => "NO Mesin Kendaraan");
        $jenis_keyword[] = array("id" => "vin_number", "text" => "NO Rangka Kendaraan");
        $model['list_keyword'] = $jenis_keyword;
        
        
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        $model['list_pool'] = $this->m_pool->GetDropDownPool();
        $model['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "report/v_report_mutasi", $javascript, $css);
    }

    function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }

    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Cabang', 'valueKey' => 'nama_cabang', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal Do', 'valueKey' => 'tanggal_do', 'colWidth' => 10, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Nomor Do', 'valueKey' => 'nomor_mutasi', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'From', 'valueKey' => 'nama_pool_from', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'To', 'valueKey' => 'nama_pool_to', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Nama Customer', 'valueKey' => 'nama_customer', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Up', 'valueKey' => 'up', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Alamat', 'valueKey' => 'alamat', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'NO SPK', 'valueKey' => 'no_prospek', 'colWidth' => 15),
            array('colName' => 'Type', 'valueKey' => 'nama_type_unit', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Buka Jual', 'valueKey' => 'tanggal_buka_jual', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'No Rangka', 'valueKey' => 'vin_number', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'No Mesin', 'valueKey' => 'engine_no', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'VIN', 'valueKey' => 'vin', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Keterangan', 'valueKey' => 'keterangan', 'colWidth' => 30, 'bold' => true),
        );

        return $col;
    }

    function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }

    function export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_mutasi->GetDataReportMutasi($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Do_prospek",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title_mutasi),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    function footInfo() {
        $info = array();
        $info[] = array(
            array('text' => 'Total', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 4),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
        );

        return $info;
    }

    function rowSet($data) {
        $result = array();
        $nomor = 1;
        $list_cabang = $this->m_cabang->GetDropDownCabang();
        $list_type = $this->m_type_unit->GetDropDownType_unit();
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['tanggal_do'] = DefaultTanggal($row['tanggal_do']);
            $row['tanggal_buka_jual'] = DefaultTanggal($row['tanggal_buka_jual']);
            
            $nomor++;
            $result[]=$row;
        }
        // $arr_group = [];
        // foreach($result as $res) {
        //     foreach($list_cabang as $cb) {
        //         if($res['id_cabang'] == $cb['id']) {
        //             foreach($list_type as $tp) {
        //                 if($res['id_type_unit'] == $tp['id']) {
        //                     $arr_group[$cb['text']][$tp['id']][] = $res;
        //                 }
        //             }
        //         }
        //     }
        // }

        return $result;
    }

    public function group_by($key, $data) {
        $result = array();
    
        foreach($data as $val) {
            if(array_key_exists($key, $val)){
                $result[$val[$key]][] = $val;
            }else{
                $result[""][] = $val;
            }
        }
    
        return $result;
    }

    public function getdatamutasi() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        
         foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search".$this->_prefix, 1);
            }
            SetCookieSetting($key . $this->_prefix, $value);
        }
        echo $this->m_report_mutasi->GetDataReportMutasi($params);
    }

}

/* End of file Barang.php */