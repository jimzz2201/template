<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_utang extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_report_utang');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K094";
        $header = "K005";
        $title = "Rekap Hutang";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $css = array();
        LoadTemplate($model, "report/v_report_utang_index", $javascript, $css);
    }
    public function kartu() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K097";
        $header = "K005";
        $title = "Rekap Hutang";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $css = array();
        LoadTemplate($model, "report/v_report_utang_kartu_index", $javascript, $css);
    }
    
    public function pembayaran() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K096";
        $header = "K005";
        $title = "Rekap Hutang";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
         $this->load->model("supplier/m_supplier");
         $jenis_pembayaran = array();
        $jenis_pembayaran["Tunai Kas Kandang"] = "Tunai Kas Kandang";
        $jenis_pembayaran["Tunai Kas Besar"] = "Tunai Kas Besar";
        $jenis_pembayaran["Transfer"] = "Transfer";
        $jenis_pembayaran["Potongan"] = "Potongan";
        $jenis_pembayaran["Cek & Giro"] = "Cek & Giro";
        $model['list_type_pembayaran'] = $jenis_pembayaran;
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $css = array();
        LoadTemplate($model, "report/v_report_utang_pembayaran_index", $javascript, $css);
    }

    public function get_data_viewpembayaran() {
        $model = $this->input->post();
        $data = $this->m_report_utang->GetDataPembayaranHutang($model);

        $this->load->view("v_report_utang_pembayaran_result", array("data" => $data));
    }

    public function printutangpembayaran() {
        $model = $this->input->get();
        $info = "";
        $title = "Pembayaran Hutang";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        $data = $this->m_report_utang->GetDataPembayaranHutang($model);
        $html = $this->load->view("v_report_utang_pembayaran_result", array("data" => $data), true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }

    public function exportutangpembayaran() {
        $model = $this->input->get();

        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "buku_jurnal");
        set_time_limit(600);
        $data = [];
        $judul = 'Pembayaran Hutang';
        $data['title'] = [$judul];
        $info = InfoExcel("spout");
        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $result = $this->m_report_utang->GetDataPembayaranHutang($model);
        $no = 0;


        $data['header'] = array();
        $data['header'][] = 'Tanggal';
        $data['header'][] = 'No Dokumen';
        $data['header'][] = 'Nama';
        $data['header'][] = 'Faktur Pembayaran';
        $data['header'][] = 'Pembayaran';
        $data['header'][] = 'Tunai';
        $data['header'][] = 'Transfer';
        $data['header'][] = 'Cek & Giro';
        $data['header'][] = 'Retur';
        $data['header'][] = 'Potongan';
        $data['header'][] = 'Nama Bank';
        $data['row'] = [];
        $data['cols'] = [
            20, 25, 30, 40, 35,35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        //$data['info'][] = array_merge(InfoExcel('spout'), ['', '', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']]);
        $totaltunai = 0;
        $totaltransfer = 0;
        $totalcekgiro = 0;
        $totalretur = 0;
        $totalpotongan = 0;
        foreach ($result as $key => $detail) {
            $tunai = 0;
            $transfer = 0;
            $cekgiro = 0;
            $retur = 0;
            $potongan = 0;
            if (strpos($detail->jenis_pembayaran, "Tunai") !== false || $detail->jenis_pembayaran == "Pembayaran Di Muka"|| $detail->jenis_pembayaran == "Cash") {
                $tunai = $detail->total_bayar;
                $totaltunai += $tunai;
            } else if ($detail->jenis_pembayaran == "retur") {
                $retur = $detail->total_bayar;
                $totalretur += $retur;
            } else if ($detail->jenis_pembayaran == "Transfer") {
                $transfer = $detail->total_bayar;
                $totaltransfer += $transfer;
            } else if ($detail->jenis_pembayaran == "Potongan") {
                $potongan = $detail->total_bayar;
                $totalpotongan += $potongan;
            } else if ($detail->jenis_pembayaran == "Cek & Giro") {
                $cekgiro = $detail->total_bayar;
                $totalcekgiro += $cekgiro;
            }
            $data['row'][$no] = array();
            $data['row'][$no][] = DefaultTanggal(@$detail->tanggal_transaksi);
            $data['row'][$no][] = @$detail->dokumen;
            $data['row'][$no][] = @$detail->nama_supplier;
            $data['row'][$no][] = @$detail->no_faktur;
            $data['row'][$no][] = @$detail->jenis_pembayaran;
            $data['row'][$no][] = ['value' => (double) @$tunai, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) @$transfer, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) @$cekgiro, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) @$retur, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) @$potongan, 'format' => 'number'];
            $data['row'][$no][] = @$detail->nama_bank;
            $data['row'][$no]['ht'] = 15;
            $no++;
        }




        $this->spout->export($filename, $data);
        set_time_limit(0);
    }
    
    
    public function get_data_viewkartu() {
        $model = $this->input->post();
        $this->load->model("m_report_buku");
        $model['id_gl_account'] = 10;
        $data = $this->m_report_buku->GetDataJurnal($model);

        $this->load->view("v_report_utang_kartu_result", $data);
    }

    public function printutangkartu() {
        $model = $this->input->get();
        $info = "";
        $this->load->model("m_report_buku");
        $title = "Kartu Hutang";
        $info = InfoExcel("spout", 1, "Hutang");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        $model['id_gl_account'] = 10;
        $data = $this->m_report_buku->GetDataJurnal($model);
        $html = $this->load->view("v_report_utang_kartu_result", $data, true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }

    public function exportutangkartu() {
        $model = $this->input->get();
        $this->load->model("m_report_buku");
        
        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "buku_jurnal");
        set_time_limit(600);
        $data = [];
        $judul = 'Kartu Hutang';
        $data['title'] = [$judul];
        $info = InfoExcel("spout", 1, "Hutang");
        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $model['id_gl_account'] = 10;
        $result = $this->m_report_buku->GetDataJurnal($model, "");
        $listbukujurnal = $result['data'];
        $no = 0;
        $increment = $result['saldo_awal'];
        $data['info'][] = ['', '', '','', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']];


        $data['header'] = array();
        $data['header'][] = 'Tanggal';
        $data['header'][] = 'No Dokumen';
        $data['header'][] = 'No Faktur';
        
        $data['header'][] = 'Keterangan';
        $data['header'][] = 'Debit';
        $data['header'][] = 'Kredit';
        $data['header'][] = 'Saldo Akhir';
        $data['row'] = [];
        $data['cols'] = [
            20, 25,20, 100, 20, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        //$data['info'][] = array_merge(InfoExcel('spout'), ['', '', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']]);

        foreach ($listbukujurnal as $key => $detail) {
            $increment += @$detail->debet - @$detail->kredit;
            $data['row'][$no] = array();
            $data['row'][$no][] = DefaultTanggal(@$detail->tanggal_buku);
            $data['row'][$no][] = @$detail->dokumen;
            $data['row'][$no][] = @$detail->no_faktur;
            $data['row'][$no][] = $detail->keterangan;
            $data['row'][$no][] = ['value' => (double) $detail->debet, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->kredit, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $increment, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }




        $this->spout->export($filename, $data);
        set_time_limit(0);
    }

    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K095";
        $header = "K005";
        $title = "Rekap Hutang Detail";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $css = array();
        LoadTemplate($model, "report/v_report_utang_detail_index", $javascript, $css);
    }

    public function getdatarekaputang() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_utang->GetDataRekapUtang($params);
    }
     public function printdetailutang() {
        $model = $this->input->get();
        $info = "";
        $title = "Laporan Utang Detail";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        $listdetail = $this->m_report_utang->GetDataDetailUtang($model, "");
        $html = $this->load->view("v_report_utang_detail_result", array("listdetail" => $listdetail), true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }
    public function printrekaputang() {
        $model = $this->input->get();
        $info = "";
        $title = "Laporan Rekap Hutang";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '( Per Tanggal ' . GetDateNow() . ')';
        $listbukukas = $this->m_report_utang->GetDataRekapUtang($model, "");
        $html = $this->load->view("v_report_utang_result", array("listdetail" => $listbukukas), true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }
    public function exportdetailutang() {
        $model = $this->input->get();
        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "r_detail_utang");
        set_time_limit(600);
        $data = [];
        $judul = 'Laporan Detail Utang';
        $data['title'] = [$judul];
        $info = InfoExcel('spout');

        $data['title'] = [$judul];

        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $data['info'][] = [''];
        $data['header'] = array();
        $data['header'][] = 'No';
        $data['header'][] = 'Nama Customer';
        $data['header'][] = 'Tanggal';
        $data['header'][] = 'No Faktur';
        $data['header'][] = 'Invoice';
        $data['header'][] = 'Jatuh Tempo';
        $data['header'][] = 'Lama';
        $data['header'][] = 'GrandTotal';
        $data['header'][] = 'Sisa';
        $data['row'] = [];
        $data['cols'] = [
            8, 35, 20, 15, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        $listdetailutang = $this->m_report_utang->GetDataDetailUtang($model, "");
        $no = 0;
        foreach ($listdetailutang as $key => $detail) {
            $data['row'][$no] = array();
            $data['row'][$no][] = $no + 1;
            $data['row'][$no][] = $detail->nama_supplier;
            $data['row'][$no][] = DefaultTanggal(@$detail->tanggal);
            $data['row'][$no][] = $detail->no_faktur;
            $data['row'][$no][] = $detail->nomor_master;
            $data['row'][$no][] = DefaultTanggal(@$detail->jth_tempo);
            $data['row'][$no][] = ['value' => (double) $detail->lama_hari, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->grandtotal, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->sisa, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }
        if (count($listdetailutang)) {
            $data['footer'][] = [
                null, null, null, null, null, null, null,
                'Total',
                    ['value' => 'SUM(' . getAlphas(8, 9) . ':' . getAlphas(8, $no + 7) . ')', 'type' => 'formula', 'format' => 'number'],
                'ht' => 15
            ];
            $data['merge'][] = getAlphas(1, $no + 9) . ":" . getAlphas(6, $no + 9);
        }
        $this->spout->export($filename, $data);
        set_time_limit(0);
    }
    public function exsportrekaputang() {
        $model = $this->input->get();
        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "r_rekap_utang");
        set_time_limit(600);
        $data = [];
        $judul = 'Laporan Rekap Utang';
        $info = InfoExcel('spout');
        $data['title'] = [$judul];
        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $data['info'][] = [''];
        $data['header'] = array();
        $data['header'][] = 'No';
        $data['header'][] = 'Nama Customer';
        $data['header'][] = 'Saldo Awal';
        $data['header'][] = 'Utang';
        $data['header'][] = 'Total Due';
        $data['header'][] = '<=0';
        $data['header'][] = '1 - 30';
        $data['header'][] = '31 - 60';
        $data['header'][] = '61 - 90';
        $data['header'][] = '>= 90';
        $data['row'] = [];
        $data['cols'] = [
            8, 12, 35, 35, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        $listrekaputang = $this->m_report_utang->GetDataRekapUtang($model, "");
        $no = 0;
        foreach ($listrekaputang as $key => $detail) {
            $data['row'][$no] = array();
            $data['row'][$no][] = $no + 1;
            $data['row'][$no][] = $detail->nama_supplier;
            $data['row'][$no][] = ['value' => (double) $detail->saldo_awal, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->hutang, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->total_due, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->due0, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->due30, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->due60, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->due90, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->due90plus, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }




        $this->spout->export($filename, $data);
        set_time_limit(0);
    }

    public function getdatadetailutang() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_utang->GetDataDetailUtang($params);
    }
    
    
    public function unit() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K104";
        $header = "K005";
        $title = "Penjualan";
        CekModule($module);
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
         $status[] = array();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Approved");
        $status[] = array("id" => "2", "text" => "Rejected");
        $status[] = array("id" => "3", "text" => "Closed");
        $status[] = array("id" => "5", "text" => "Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $this->load->model("kategori/m_kategori");
        $model['status'] = 1;
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();

        LoadTemplate($model, "report/v_report_utang_unit", $javascript, $css);
    }
    function utang_unit_export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_utang->GetDataReportUtangUnit($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Penjualan Unit",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }
    
     function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }
    function rowSet($data) {
        $result = array();
        $nomor = 1;
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['tanggal_kpu'] = DefaultTanggal($row['tanggal_kpu']);
            $row['harga_off_road'] = (int) $row['harga_off_road'];
            $row['harga_jual_unit'] = (int) $row['harga_jual_unit'];
            $row['sisa'] = (int) $row['sisa'];
            $row['jumlah_pembayaran'] = (int) $row['jumlah_pembayaran'];
            $row['bulan'] = GetMonth((int)date('m', strtotime($row['tanggal_kpu'])));
           
            $nomor++;

            if ($row['status'] != 2) {
                $this->_off_road += (int) $row['harga_off_road'];
                $this->_on_road += (int) $row['harga_on_the_road'];
                $this->_equipment += (int) $row['total_equipment'];
                $this->_harga_unit += (int) $row['harga_jual_unit'];
                $this->_biaya_bbn += (int) $row['biaya_bbn'];
            }
            $result[]=$row;
        }

        return $result;
    }
    function footInfo() {
        $info = array();
        $info[] = array(
            array('text' => 'Total', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 4),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
        );

        return $info;
    }
     function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }
    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal', 'valueKey' => 'tanggal_kpu', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Bulan', 'valueKey' => 'bulan', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Supplier', 'valueKey' => 'supplier_name', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'NO KPU', 'valueKey' => 'no_kpu', 'colWidth' => 15),
            array('colName' => 'Cabang', 'valueKey' => 'nama_cabang', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Kategori', 'valueKey' => 'nama_kategori', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Unit', 'valueKey' => 'nama_unit', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'No Rangka', 'valueKey' => 'vin_number', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'No Mesin', 'valueKey' => 'engine_no', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Invoice Number', 'valueKey' => 'invoice_number', 'colWidth' => 15),
            array('colName' => 'VIN', 'valueKey' => 'vin', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Harga Beli', 'valueKey' => 'harga_jual_unit', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Jumlah Pembayaran', 'valueKey' => 'jumlah_pembayaran', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Sisa', 'valueKey' => 'sisa', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
        );

        return $col;
    }

    
    
      public function getdatautangunit() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_utang->GetDataReportUtangUnit($params);
    }

}

/* End of file Barang.php */