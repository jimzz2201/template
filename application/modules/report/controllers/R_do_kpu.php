<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_do_kpu extends CI_Controller {

    private $_title = "Laporan DO KPU";
    private $_title_do_kpu = "Laporan DO KPU";

    function __construct() {
        parent::__construct();
        $this->load->model('m_report_do');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K112";
        $header = "K005";
        $title = "DO KPU";
        CekModule($module);
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("customer/m_customer");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $status[] = array();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Approved");
        $status[] = array("id" => "2", "text" => "Rejected");
        $status[] = array("id" => "3", "text" => "Closed");
        $status[] = array("id" => "5", "text" => "Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['status'] = 1;
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "report/v_report_do_kpu", $javascript, $css);
    }

    function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }

    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal', 'valueKey' => 'tanggal_do', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Nomor DO', 'valueKey' => 'nomor_do_kpu', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'KPU', 'valueKey' => 'nomor_master', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'Supplier', 'valueKey' => 'nama_supplier', 'colWidth' => 25),
            array('colName' => 'Unit', 'valueKey' => 'nama_unit', 'colWidth' => 15),
            array('colName' => 'Tanggal Terima', 'valueKey' => 'tanggal_terima', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Status', 'valueKey' => 'status', 'colWidth' => 15),
        );

        return $col;
    }

    function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }

    function export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_do->GetDataReportDoKpu($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "do_kpu",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title_do_kpu),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    function footInfo() {
        $info = array();
        $info[] = array(
            array('text' => 'Total', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 4),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
        );

        return $info;
    }

    function rowSet($data) {
        $result = array();
        $nomor = 1;
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['tanggal_do'] = DefaultTanggal($row['tanggal_do']);
            $row['nomor_do_kpu'] = $row['nomor_do_kpu'];
            $row['nomor_master'] = $row['nomor_master'];
            $row['nama_unit'] = $row['nama_unit'];
            $row['tanggal_terima'] = DefaultTanggal($row['tanggal_terima']);
            $row['status'] = $row['status'];
           
            $nomor++;
            $result[]=$row;
        }

        return $result;
    }

    public function getdatadokpu() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_do->GetDataReportDoKpu($params);
    }

}