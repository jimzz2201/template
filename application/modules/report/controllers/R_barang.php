<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_barang extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("m_report_barang");
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K061";
        $header = "K006";
        $title = "Kartu Stock";
        CekModule($module);
        $css = array();
        $this->load->model("barang/m_barang");
        $this->load->model("cabang/m_cabang");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        LoadTemplate($model, "report/v_report_barang_index", $javascript, $css);
    }

    public function get_data_view() {
        $model = $this->input->post();
        $data = $this->m_report_barang->GetDataReportBarang($model);
        $this->load->view("v_report_barang_result", $data);
    }

    public function printbarang() {
        $model = $this->input->get();
        $info = "";
        $title = "Laporan Kartu Stock";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        $listpembelian = $this->m_report_barang->GetDataReportBarang($model);
        $html = $this->load->view("v_report_barang_result", $listpembelian, true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }

    public function exportbarang() {
        $model = $this->input->get();
        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "buku_barang");
        set_time_limit(600);
        $data = [];
        $judul = 'Laporan Kartu Stock';
        $data['title'] = [$judul];
        $info = InfoExcel('spout');
        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $result = $this->m_report_barang->GetDataReportBarang($model, "");
        $listbukubarang = $result['data'];
        $no = 0;
        $increment = $result['saldo_awal'];
        $data['info'][] = ['', '', '', '','',  'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']];


        $data['header'] = array();
        $data['header'][] = 'Tanggal';
        $data['header'][] = 'No Dokumen';
        $data['header'][] = 'No Faktur';
        $data['header'][] = 'Keterangan';
        $data['header'][] = 'Debit';
        $data['header'][] = 'Kredit';
        $data['header'][] = 'Saldo Akhir';
        $data['row'] = [];
        $data['cols'] = [
            20, 25,20, 100, 20, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        //$data['info'][] = array_merge(InfoExcel('spout'), ['', '', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']]);

        foreach ($listbukubarang as $key => $detail) {
            $increment += @$detail->debet - @$detail->kredit;
            $data['row'][$no] = array();
            $data['row'][$no][] = DefaultTanggal(@$detail->tanggal_jurnal);
            $data['row'][$no][] = @$detail->dokumen;
            $data['row'][$no][] = @$detail->add_info;
            $data['row'][$no][] = ['value' => @$detail->description, 'format' => 'string'];
            $data['row'][$no][] = ['value' => (double) $detail->debet, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->kredit, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $increment, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }




        $this->spout->export($filename, $data);
        set_time_limit(0);
    }

}

/* End of file Barang.php */