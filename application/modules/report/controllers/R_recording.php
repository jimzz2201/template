<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_recording extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_report_recording');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K028";
        $header = "K006";
        $title = "Recording";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module); 
        $javascript[] = "assets/plugins/select2/select2.js";

        $css = array();
        $css[] = "assets/plugins/select2/select2.css";
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        LoadTemplate($model, "report/v_report_recording_index", $javascript, $css);
    }
    public function get_data_view()
    {
        $model=$this->input->post();
        $data=$this->m_report_recording->GetDataRecording($model);
        $this->load->view("v_report_recording_result",array("data"=>$data));
    }
}

/* End of file Barang.php */