<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_pembelian extends CI_Controller {

    private $_title = "Laporan Pembelian";
    private $_grandtotal = 0;
    private $_qty = 0;
    private $_qty_do = 0;
    function __construct() {
        parent::__construct();
        $this->load->model('m_report_pembelian');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
         $module = "K070";
        $header = "K005";
        $title = "KPU";
        CekModule($module);
        $this->load->model("supplier/m_supplier");
        $this->load->model("kategori/m_kategori");
        $sufix="_report_kpu";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $jenis_pencarian = [];
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_vrf", "text" => "NO VRF");
        $jenis_keyword[] = array("id" => "nomor_master", "text" => "NO KPU");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PPROSPEK");
        $jenis_keyword[] = array("id" => "nomor_do_kpu", "text" => "NO DO KPU");
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal", "text" => "Tanggal KPU");
        $model['start_date'] = GetCookieSetting("start_date" . $sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $sufix, GetDateNow());
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['status'] = GetCookieSetting("status" . $sufix, 0);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $sufix, "nomor_master");
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $$sufix, "created_date");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_keyword'] = $jenis_keyword;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "report/v_report_pembelian_index", $javascript, $css);
    }
    
    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        LoadTemplate($model, "report/v_report_pembelian_detail", $javascript, $css);        
    }
    
    public function per_group() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K055";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang']=$this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier']=$this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        $this->load->model("jenis/m_jenis");
        $model['list_jenis'] = $this->m_jenis->GetDropDownJenis();
        $this->load->model("merek/m_merek");
        $model['list_merek'] = $this->m_merek->GetDropDownMerek();        
        LoadTemplate($model, "report/v_report_pembelian_per_group", $javascript, $css);        
    }
    
    public function pajak_masukan() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K056";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/js/icheck/icheck.min.js";
        $javascript[] = "assets/js/icheck/icheck-active.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);               
        LoadTemplate($model, "report/v_report_pembelian_pajak_masukan", $javascript, $css);        
    }
    
    public function getdatareportkpu() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        $sufix="_report_kpu";
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $sufix, 1);
            }
            SetCookieSetting($key . $sufix, $value);
        }
        
        
        echo $this->m_report_pembelian->GetDataReportKPU($params);
    }
        
    public function getdatapembeliandetail() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
    
    public function getdatapembelianpergroup() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }
    
    public function getdatapembelianpajakmasukan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw"=>1,"recordsTotal"=>"2","recordsFiltered"=>"2","data"=>[]));
    }

    function export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_pembelian->GetDataReportKPU($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Pembelian",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }

    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal', 'valueKey' => 'tanggal', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Nomor Master', 'valueKey' => 'nomor_master', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Kode VRF', 'valueKey' => 'vrf_code', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Nomor DO', 'valueKey' => 'nomor_do_kpu', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Customer', 'valueKey' => 'nama_customer', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Nama Supplier', 'valueKey' => 'nama_supplier', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'VIN', 'valueKey' => 'vin', 'colWidth' => 15),
            array('colName' => 'QTY', 'valueKey' => 'qty', 'colWidth' => 15),
            array('colName' => 'QTY DO', 'valueKey' => 'qty_do', 'colWidth' => 15),
            array('colName' => 'TOP', 'valueKey' => 'top', 'colWidth' => 15),
			array('colName' => 'Jatuh Tempo', 'valueKey' => 'jth_tempo', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Warna', 'valueKey' => 'nama_warna', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Tanggal Pelunasan', 'valueKey' => 'tanggal_pelunasan', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Grandtotal', 'valueKey' => 'grandtotal', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Status', 'valueKey' => 'status', 'colWidth' => 15, 'bold' => true),
        );

        return $col;
    }

    function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }

    function footInfo() {
        $info = array();
        $info[] = array(
            array('text' => 'Total', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 8),
            array('text' => $this->_qty, 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => $this->_qty_do, 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => "", 'align' => 'right', 'border' => true, 'merge' => 4),
            array('text' => $this->_grandtotal, 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => "", 'align' => 'right', 'border' => true),
        );

        return $info;
    }

    function rowSet($data) {
        $result = array();
        $nomor = 1;
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['tanggal'] = DefaultTanggal($row['tanggal']);
            $row['qty'] = (int) $row['qty'];
            $row['top'] = (int) $row['top'];
            $row['tanggal_pelunasan'] = DefaultTanggal($row['tanggal_pelunasan']);
			$row['jth_tempo'] = DefaultTanggal($row['jth_tempo']);
            $row['grandtotal'] = (int) $row['grandtotal'];
            $row['status'] =  $row['status']=="4"?"Finished":($row['status']=="2"?"Batal":"Active");
            $nomor++;
            $this->_grandtotal += (int) $row['grandtotal'];
            $this->_qty += (int) $row['qty'];
            $this->_qty_do += (int) $row['qty_do'];
            $result[]=$row;
        }

        return $result;
    }
}

/* End of file Barang.php */