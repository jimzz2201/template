<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_bank extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_report_bank');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K049";
        $header = "K006";
        $title = "Kartu Bank";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $this->load->model("bank/m_bank");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_bank'] = $this->m_bank->GetDropDownBank(1);
         $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        LoadTemplate($model, "report/v_report_bank_index", $javascript, $css);
    }

    public function get_data_view() {
        $model = $this->input->post();
        $data = $this->m_report_bank->GetDataReportBank($model);
        $this->load->view("v_report_bank_result", $data);
    }

}

/* End of file Barang.php */