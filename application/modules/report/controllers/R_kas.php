<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_kas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("m_report_kas");
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K089";
        $header = "K005";
        $title = "Kas";
        CekModule($module);
        $css = array();
        $this->load->model("kas/m_kas");
        $this->load->model("cabang/m_cabang");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("gl_config/m_gl_config");
        $akunglkasbesar = $this->m_gl_config->GetIdGlConfig("kasbesar", "");
        $akunglkaskecil = $this->m_gl_config->GetIdGlConfig("kaskecil", "");
        $model['list_jenis_transaksi'] = array(array("id" => $akunglkasbesar, "text" => "Kas Besar"), array("id" => $akunglkaskecil, "text" => "Kas Kecil"));
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        LoadTemplate($model, "report/v_report_kas_index", $javascript, $css);
    }

    public function get_data_view() {
        $model = $this->input->post();
        $data = $this->m_report_kas->GetDataReportKas($model);
        $this->load->view("v_report_kas_result", $data);
    }

    public function printkas() {
        $model = $this->input->get();
        $info = "";
        $title = "Laporan Buku Kas";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        $listpembelian = $this->m_report_kas->GetDataReportKas($model);
        $html = $this->load->view("v_report_kas_result", $listpembelian, true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }

    public function exportkas() {
        $model = $this->input->get();
        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "buku_kas");
        set_time_limit(600);
        $data = [];
        $judul = 'Laporan Buku Kas';
        $data['title'] = [$judul];
        $info = InfoExcel('spout');
        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $result = $this->m_report_kas->GetDataReportKas($model, "");
        $listbukukas = $result['data'];
        $no = 0;
        $increment = $result['saldo_awal'];
        $data['info'][] = ['', '', '', '', '', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']];


        $data['header'] = array();
        $data['header'][] = 'Tanggal';
        $data['header'][] = 'No Dokumen';
        $data['header'][] = 'No Faktur';
        $data['header'][] = 'Keterangan';
        $data['header'][] = 'Jenis';
        $data['header'][] = 'Debit';
        $data['header'][] = 'Kredit';
        $data['header'][] = 'Saldo Akhir';
        $data['row'] = [];
        $data['cols'] = [
            20, 25, 20, 100, 20, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        //$data['info'][] = array_merge(InfoExcel('spout'), ['', '', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']]);

        foreach ($listbukukas as $key => $detail) {
            $increment += @$detail->debet - @$detail->kredit;
            $data['row'][$no] = array();
            $data['row'][$no][] = DefaultTanggal(@$detail->tanggal_buku);
            $data['row'][$no][] = @$detail->dokumen;
            $data['row'][$no][] = @$detail->add_info;
            $data['row'][$no][] = ['value' => @$detail->keterangan, 'format' => 'string'];
            $data['row'][$no][] = @$detail->nama_gl_account;
            $data['row'][$no][] = ['value' => (double) $detail->debet, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->kredit, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $increment, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }




        $this->spout->export($filename, $data);
        set_time_limit(0);
    }

}

/* End of file Barang.php */