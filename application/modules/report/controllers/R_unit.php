<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_unit extends CI_Controller {

    private $_title = "Laporan Unit";
    private $_grandtotal = 0;
    private  $_prefix_kartu="_report_kartu";
    

    function __construct() {
        parent::__construct();
        $this->load->model('m_report_unit');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K071";
        $header = "K005";
        $title = "Unit";
        CekModule($module);
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("kategori/m_kategori");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("type_body/m_type_body");
        $this->load->model('unit/m_unit');
        $this->load->model('pool/m_pool');
        $vin= date("Y");
        $listvin=[];
        for($i=0;$i<=10;$i++)
        {
            $listvin[]=array("id"=>$vin-$i,"text"=>$vin-$i);
        }
        
        
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal Buat SPK");
        $jenis_pencarian[] = array("id" => "tanggal_buka_jual", "text" => "Tanggal Buka Jual");
        $jenis_pencarian[] = array("id" => "tanggal_do", "text" => "Tanggal DO Prospek");
        $jenis_pencarian[] = array("id" => "tanggal_do_kpu", "text" => "Tanggal DO KPU");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_vin'] = $listvin;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $jenis_pencariankeyword = [];
        $jenis_pencariankeyword[] = array("id" => "engine_no", "text" => "No Mesin");
        $jenis_pencariankeyword[] = array("id" => "vin_number", "text" => "No Rangka");
        $jenis_pencariankeyword[] = array("id" => "no_prospek", "text" => "No SPK");
        $jenis_pencariankeyword[] = array("id" => "nomor_buka_jual", "text" => "No Buka Jual");
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $model['list_pool'] = $this->m_pool->GetDropDownPool();
        $model['list_pencarian_keyword'] = $jenis_pencariankeyword;
        $model['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $model['list_type_body'] = $this->m_type_body->GetDropDownType_body();
        $model['list_unit'] = $this->m_unit->GetDropDownUnit();
        LoadTemplate($model, "report/v_report_unit_index", $javascript, $css);
    }
    
    public function GetDataReportUnitSummary()
    {
         $this->m_report_unit->GetDataReportUnitSummary($params);
    }
    
    
    
    public function kartu() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K071";
        $header = "K005";
        $title = "Unit";
        CekModule($module);
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("kategori/m_kategori");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("type_body/m_type_body");
        $this->load->model('unit/m_unit');
        $this->load->model('pool/m_pool');
        $vin= date("Y");
        $listvin=[];
        for($i=0;$i<=10;$i++)
        {
            $listvin[]=array("id"=>$vin-$i,"text"=>$vin-$i);
        }
        
        
        $jenis_pencarian = [];
         $jenis_pencarian[] = array("id" => "tanggal", "text" => "Tanggal");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['jenis_pencarian'] = "tanggal";
        $model['list_vin'] = $listvin;
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        $jenis_pencariankeyword = [];
        $jenis_pencariankeyword[] = array("id" => "engine_no", "text" => "No Mesin");
        $jenis_pencariankeyword[] = array("id" => "vin_number", "text" => "No Rangka");
        $jenis_pencariankeyword[] = array("id" => "no_prospek", "text" => "No SPK");
        $jenis_pencariankeyword[] = array("id" => "nomor_buka_jual", "text" => "No Buka Jual");
        $model['start_date'] = GetCookieSetting("start_date".$this->_prefix_kartu, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date".$this->_prefix_kartu, GetDateNow());
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $model['list_pool'] = $this->m_pool->GetDropDownPool();
        $model['list_pencarian_keyword'] = $jenis_pencariankeyword;
        $model['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $model['list_type_body'] = $this->m_type_body->GetDropDownType_body();
        $model['list_unit'] = $this->m_unit->GetDropDownUnit();
        LoadTemplate($model, "report/v_report_unit_kartu_index", $javascript, $css);
    }


    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        LoadTemplate($model, "report/v_report_unit_detail", $javascript, $css);
    }

    public function per_group() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K055";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        $this->load->model("jenis/m_jenis");
        $model['list_jenis'] = $this->m_jenis->GetDropDownJenis();
        $this->load->model("merek/m_merek");
        $model['list_merek'] = $this->m_merek->GetDropDownMerek();
        LoadTemplate($model, "report/v_report_unit_per_group", $javascript, $css);
    }

    public function pajak_masukan() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K056";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/js/icheck/icheck.min.js";
        $javascript[] = "assets/js/icheck/icheck-active.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        LoadTemplate($model, "report/v_report_unit_pajak_masukan", $javascript, $css);
    }

    public function getdataunit() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_unit->GetDataReportunit($params);
    }
    
    public function getdataunitkartu() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_unit->GetDataReportunitKartu($params);
    }

    public function getdataunitdetail() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdataunitpergroup() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdataunitpajakmasukan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    function export() {
        $this->load->library('Excel');

        $search = $this->input->get();
		ini_set('memory_limit', '512M'); 
        $data = $this->m_report_unit->GetDataReportunit($search, "export");
		
        if (count($data)) {
            $params = array(
                'sNAMESS' => "Unit",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }
    function exportkartu() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_unit->GetDataReportunitKartu($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Unit",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSetkartu(),
                'rowSet' => $this->rowSetkartu($data)
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }
    
    function exportkartusummary() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_unit->GetDataReportUnitSummary($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Unit",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSetkartusummary(),
                'rowSet' => $data
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }

    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Kategori', 'valueKey' => 'nama_kategori', 'colWidth' => 15, 'bold' => true,),
            array('colName' => 'Nama Unit', 'valueKey' => 'nama_unit', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Vin Number', 'valueKey' => 'vin_number', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Engine No', 'valueKey' => 'engine_no', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'Warna', 'valueKey' => 'nama_warna', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'VIN', 'valueKey' => 'vin', 'colWidth' => 15),
            array('colName' => 'No KPU', 'valueKey' => 'no_kpu', 'colWidth' => 15),
            array('colName' => 'Tanggal KPU', 'valueKey' => 'tanggal_kpu', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'No DO KPU', 'valueKey' => 'no_do_kpu', 'colWidth' => 15),
            array('colName' => 'Tanggal DO', 'valueKey' => 'tanggal_do_kpu', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'No Prospek', 'valueKey' => 'no_prospek', 'colWidth' => 15),
            array('colName' => 'Tanggal Prospek', 'valueKey' => 'tanggal_prospek', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Nama Customer', 'valueKey' => 'nama_customer', 'colWidth' => 15),
            array('colName' => 'No Buka Jual', 'valueKey' => 'nomor_buka_jual', 'colWidth' => 15),
            array('colName' => 'Tanggal Buka Jual', 'valueKey' => 'tanggal_buka_jual', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Status Buka Jual', 'valueKey' => 'status_buka_jual', 'colWidth' => 15),
            array('colName' => 'No DO Prospek', 'valueKey' => 'nomor_do_prospek', 'colWidth' => 15),
            array('colName' => 'Tanggal Do Prospek', 'valueKey' => 'tanggal_do_prospek', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Free Parking', 'valueKey' => 'free_parking', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'DNP', 'valueKey' => 'dnp', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Unit Position', 'valueKey' => 'nama_pool', 'colWidth' => 15, 'bold' => true),
        );

        return $col;
    }
    
    function colSetkartu() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal', 'valueKey' => 'tanggal', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
           array('colName' => 'Dokumen', 'valueKey' => 'ref_doc', 'colWidth' => 15, 'bold' => true,),
            array('colName' => 'Kategori', 'valueKey' => 'nama_kategori', 'colWidth' => 15, 'bold' => true,),
            array('colName' => 'Nama Unit', 'valueKey' => 'nama_unit', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Vin Number', 'valueKey' => 'vin_number', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Engine No', 'valueKey' => 'engine_no', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'Warna', 'valueKey' => 'nama_warna', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'VIN', 'valueKey' => 'vin', 'colWidth' => 15),
            array('colName' => 'No KPU', 'valueKey' => 'no_kpu', 'colWidth' => 15),
            array('colName' => 'No Prospek', 'valueKey' => 'no_prospek', 'colWidth' => 15),
            array('colName' => 'Tanggal Prospek', 'valueKey' => 'tanggal_prospek', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Nama Customer', 'valueKey' => 'nama_customer', 'colWidth' => 15),
            array('colName' => 'No Buka Jual', 'valueKey' => 'nomor_buka_jual', 'colWidth' => 15),
            array('colName' => 'Tanggal Buka Jual', 'valueKey' => 'tanggal_buka_jual', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Status Buka Jual', 'valueKey' => 'status_buka_jual', 'colWidth' => 15),
            array('colName' => 'Free Parking', 'valueKey' => 'free_parking', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'DNP', 'valueKey' => 'dnp', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Unit Position', 'valueKey' => 'nama_pool', 'colWidth' => 15, 'bold' => true),
        );

        return $col;
    }
    
    
    function colSetkartusummary() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal Masuk', 'valueKey' => 'tanggal', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'No SPK', 'valueKey' => 'no_prospek', 'colWidth' => 15, 'bold' => true,),
            array('colName' => 'Kategori', 'valueKey' => 'nama_kategori', 'colWidth' => 15, 'bold' => true,),
            array('colName' => 'Nama Unit', 'valueKey' => 'nama_unit', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Vin Number', 'valueKey' => 'vin_number', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Engine No', 'valueKey' => 'engine_no', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'VIN', 'valueKey' => 'vin', 'colWidth' => 15),
            array('colName' => 'Nama Customer', 'valueKey' => 'nama_customer', 'colWidth' => 15),
            array('colName' => 'Tanggal Buka Jual', 'valueKey' => 'tanggal_buka_jual', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Unit Position', 'valueKey' => 'nama_pool', 'colWidth' => 15, 'bold' => true),
        );

        return $col;
    }
    
    function rowSetkartu($data) {
        $result = array();
        $nomor = 1;
        
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['vin'] = $row['vin'];
            $row['nomor_do_kpu'] = $row['nomor_do_kpu'];
            $row['tanggal_do_prospek'] = DefaultTanggal($row['tanggal_do_prospek']);
            $row['free_parking'] = DefaultTanggal($row['free_parking']);
           
            $nomor++;
            $this->_grandtotal += (int) $row['dnp'];
            $result[]=$row;
        }

        return $result;
    }
    
    

    function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }

    function footInfo() {
        $info = array();
        $info[] = array(
            array('text' => 'Total', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 10),
            array('text' => $this->_grandtotal, 'align' => 'right', 'border' => true, 'format' => 'nominal'),
        );

        return $info;
    }

    function rowSet($data) {
        $result = array();
        $nomor = 1;
        
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['vin'] = $row['vin'];
            $row['nomor_do_kpu'] = $row['nomor_do_kpu'];
            $row['tanggal_do'] = DefaultTanggal($row['tanggal_do']);
            $row['tanggal_do_kpu'] = DefaultTanggal($row['tanggal_do_kpu']);
            $row['free_parking'] = DefaultTanggal($row['free_parking']);
           
            $nomor++;
            $this->_grandtotal += (int) $row['dnp'];
            $result[]=$row;
        }

        return $result;
    }

}

/* End of file Barang.php */