<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_buka_jual extends CI_Controller {

    private $_title = "Laporan Buka Jual";
    private $_grandtotal = 0;
    private $_jumlah_unit = 0;

    function __construct() {
        parent::__construct();
        $this->load->model('m_report_buka_jual');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K102";
        $header = "K005";
        $title = "Penjualan";
        CekModule($module);
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("customer/m_customer");
        $this->load->model("cabang/m_cabang");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal SPK");
        $jenis_pencarian[] = array("id" => "tanggal_buka_jual", "text" => "Tanggal Buka Jual");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", GetUserId());
        $model['list_level'] = GetLevel();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "3", "text" => "Nego");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $status[] = array("id" => "7", "text" => "Permintaan Buka Jual");
        $model['list_status'] = $status;
        $model['start_date'] = GetCookieSetting("start_date_report_buka_jual", AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date_report_buka_jual", GetDateNow());
        $model['value_status'] = GetCookieSetting("status_report_buka_jual", 7);
        $model['id_cabang'] = GetCookieSetting("id_cabang_report_buka_jual", 0);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian_report_buka_jual");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "report/v_report_buka_jual_index", $javascript, $css);
    }

    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        LoadTemplate($model, "report/v_report_penjualan_detail", $javascript, $css);
    }

    public function per_group() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K055";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("supplier/m_supplier");
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $this->load->model("barang/m_barang");
        $model['list_barang'] = $this->m_barang->GetDropDownBarang();
        $this->load->model("jenis/m_jenis");
        $model['list_jenis'] = $this->m_jenis->GetDropDownJenis();
        $this->load->model("merek/m_merek");
        $model['list_merek'] = $this->m_merek->GetDropDownMerek();
        LoadTemplate($model, "report/v_report_penjualan_per_group", $javascript, $css);
    }

    public function pajak_masukan() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K056";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);
        $javascript[] = 'assets/js/datapicker/bootstrap-datepicker.js';
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $javascript[] = "assets/js/icheck/icheck.min.js";
        $javascript[] = "assets/js/icheck/icheck-active.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        LoadTemplate($model, "report/v_report_penjualan_pajak_masukan", $javascript, $css);
    }

    public function getdatabukajual() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            SetCookieSetting($key . "_report_buka_jual", $value);
        }
        echo $this->m_report_buka_jual->GetDataReportBukaJual($params);
    }

    public function getdatapenjualandetail() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdatapenjualanpergroup() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdatapenjualanpajakmasukan() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    function export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_buka_jual->GetDataReportBukaJual($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Buka Jual",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }

    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal', 'valueKey' => 'tanggal_buka_jual', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Prospek', 'valueKey' => 'no_prospek', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Buka Jual', 'valueKey' => 'nomor_buka_jual', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Nama Customer', 'valueKey' => 'nama_customer', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'Jenis Plat', 'valueKey' => 'nama_jenis_plat', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Nama Unit', 'valueKey' => 'nama_unit', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'QTY', 'valueKey' => 'jumlah_unit', 'colWidth' => 15),
            array('colName' => 'Harga Jual', 'valueKey' => 'harga_jual_unit', 'colWidth' => 15, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Warna', 'valueKey' => 'nama_warna', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Tahun', 'valueKey' => 'tahun', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Grandtotal', 'valueKey' => 'grandtotal', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Payment Method', 'valueKey' => 'nama_payment_method', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Salesman', 'valueKey' => 'nama_pegawai', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Cabang', 'valueKey' => 'nama_cabang', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Status', 'valueKey' => 'status', 'colWidth' => 15, 'bold' => true),
        );

        return $col;
    }

    function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }

    function footInfo() {
        $info = array();
        $info[] = array(
            array('text' => 'Total', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 7),
            array('text' => $this->_jumlah_unit, 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => '', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 3),
            array('text' => $this->_grandtotal, 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => '', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 3),
        );

        return $info;
    }

    function rowSet($data) {
        $result = array();
        $nomor = 1;
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['tanggal_buka_jual'] = DefaultTanggal($row['tanggal_buka_jual']);
            $row['grandtotal'] = (int) $row['jumlah_unit'] * $row['harga_jual_unit'];
            $status = "";
            if ($row['status'] == 1) {
                $status = "Approved";
            } else if ($row['status'] == 2) {
                $status = "Rejected";
            } else if ($row['status'] == 3) {
                $status = "Nego";
            } else if ($row['status'] == 4) {
                $status = "Finished";
            } else if ($row['status'] == 5) {
                $status = "Batal";
            } else if ($row['status'] == 7) {
                $status = "Permintaan Buka Jual";
            } else {
                $status = "Pending";
            }
            $row['status'] = $status;
            $nomor++;
            $this->_grandtotal += (int) $row['grandtotal'];
            $this->_jumlah_unit += (int) $row['jumlah_unit'];
            $result[] = $row;
        }

        return $result;
    }

}

/* End of file Barang.php */