<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_piutang extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_report_piutang');
    }

    public function printdetailpiutang() {
        $model = $this->input->get();
        $info = "";
        $title = "Laporan Piutang Detail";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        $listdetail = $this->m_report_piutang->GetDataDetailPiutang($model, "");
        $html = $this->load->view("v_report_piutang_detail_result", array("listdetail" => $listdetail), true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K090";
        $header = "K005";
        $title = "Rekap Piutang";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json","", GetCabangAkses());
        $this->load->model("customer/m_customer");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $css = array();
        LoadTemplate($model, "report/v_report_piutang_index", $javascript, $css);
    }

    public function kartu() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K093";
        $header = "K005";
        $title = "Rekap Piutang";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("customer/m_customer");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $css = array();
        LoadTemplate($model, "report/v_report_piutang_kartu_index", $javascript, $css);
    }

    public function pembayaran() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K092";
        $header = "K005";
        $title = "Rekap Piutang";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("customer/m_customer");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $jenis_pembayaran = array();
        $jenis_pembayaran["Tunai Kas Kandang"] = "Tunai Kas Kandang";
        $jenis_pembayaran["Tunai Kas Besar"] = "Tunai Kas Besar";
        $jenis_pembayaran["Transfer"] = "Transfer";
        $jenis_pembayaran["Potongan"] = "Potongan";
        $jenis_pembayaran["Cek & Giro"] = "Cek & Giro";
        $model['list_type_pembayaran'] = $jenis_pembayaran;
        $css = array();
        LoadTemplate($model, "report/v_report_piutang_pembayaran_index", $javascript, $css);
    }
    public function unit() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K104";
        $header = "K005";
        $title = "Penjualan";
        CekModule($module);
        $css = array();
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
         $status[] = array();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Approved");
        $status[] = array("id" => "2", "text" => "Rejected");
        $status[] = array("id" => "3", "text" => "Closed");
        $status[] = array("id" => "5", "text" => "Batal");
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("kategori/m_kategori");
        $model['status'] = 1;
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", "", GetCabangAkses());
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $model['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai("json", GetCabangAkses());
        $this->load->model("customer/m_customer");
        $model['list_supplier'] = $this->m_customer->GetDropDownCustomer();

        LoadTemplate($model, "report/v_report_piutang_unit", $javascript, $css);
    }
      public function getdatapiutangunit() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_piutang->GetDataReportPiutangUnit($params);
    }
    public function get_data_viewpembayaran() {
        $model = $this->input->post();
        $data = $this->m_report_piutang->GetDataPembayaranPiutang($model);

        $this->load->view("v_report_piutang_pembayaran_result", array("data" => $data));
    }

    public function printpiutangpembayaran() {
        $model = $this->input->get();
        $info = "";
        $title = "Pembayaran Piutang";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        $data = $this->m_report_piutang->GetDataPembayaranPiutang($model);
        $html = $this->load->view("v_report_piutang_pembayaran_result", array("data" => $data), true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }

    public function exportpiutangpembayaran() {
        $model = $this->input->get();

        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "buku_jurnal");
        set_time_limit(600);
        $data = [];
        $judul = 'Kartu Piutang';
        $data['title'] = [$judul];
        $info = InfoExcel("spout");
        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $result = $this->m_report_piutang->GetDataPembayaranPiutang($model);
        $no = 0;


        $data['header'] = array();
        $data['header'][] = 'Tanggal';
        $data['header'][] = 'No Dokumen';
        $data['header'][] = 'Nama';
        $data['header'][] = 'Faktur Pembayaran';
        $data['header'][] = 'Pembayaran';
        $data['header'][] = 'Tunai';
        $data['header'][] = 'Transfer';
        $data['header'][] = 'Cek & Giro';
        $data['header'][] = 'Retur';
        $data['header'][] = 'Potongan';
        $data['header'][] = 'Nama Bank';
        $data['row'] = [];
        $data['cols'] = [
            20, 25, 30, 40,35, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        //$data['info'][] = array_merge(InfoExcel('spout'), ['', '', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']]);
        $totaltunai = 0;
        $totaltransfer = 0;
        $totalcekgiro = 0;
        $totalretur = 0;
        $totalpotongan = 0;
        foreach ($result as $key => $detail) {
            $tunai = 0;
            $transfer = 0;
            $cekgiro = 0;
            $retur = 0;
            $potongan = 0;
            if (strpos($detail->jenis_pembayaran, "Tunai") !== false ||strpos($detail->jenis_pembayaran, "Cash")!==false || $detail->jenis_pembayaran == "Pembayaran Di Muka") {
                $tunai = $detail->total_bayar;
                $totaltunai += $tunai;
            } else if ($detail->jenis_pembayaran == "retur") {
                $retur = $detail->total_bayar;
                $totalretur += $retur;
            } else if ($detail->jenis_pembayaran == "Transfer") {
                $transfer = $detail->total_bayar;
                $totaltransfer += $transfer;
            } else if ($detail->jenis_pembayaran == "Potongan") {
                $potongan = $detail->total_bayar;
                $totalpotongan += $potongan;
            } else if ($detail->jenis_pembayaran == "Cek & Giro") {
                $cekgiro = $detail->total_bayar;
                $totalcekgiro += $cekgiro;
            }
            $data['row'][$no] = array();
            $data['row'][$no][] = DefaultTanggal(@$detail->tanggal_transaksi);
            $data['row'][$no][] = @$detail->dokumen;
            $data['row'][$no][] = @$detail->nama_customer;
            $data['row'][$no][] = @$detail->no_faktur;
            $data['row'][$no][] = @$detail->jenis_pembayaran;
            $data['row'][$no][] = ['value' => (double) @$tunai, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) @$transfer, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) @$cekgiro, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) @$retur, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) @$potongan, 'format' => 'number'];
            $data['row'][$no][] = @$detail->nama_bank;
            $data['row'][$no]['ht'] = 15;
            $no++;
        }




        $this->spout->export($filename, $data);
        set_time_limit(0);
    }

    public function get_data_viewkartu() {
        $model = $this->input->post();
        $this->load->model("m_report_buku");
        $model['id_gl_account'] = 4;
        $data = $this->m_report_buku->GetDataJurnal($model);

        $this->load->view("v_report_piutang_kartu_result", $data);
    }

    public function printpiutangkartu() {
        $model = $this->input->get();
        $info = "";
        $this->load->model("m_report_buku");
        $title = "Kartu Piutang";
        $info = InfoExcel("spout", 1, "Piutang");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        $model['id_gl_account'] = 4;
        $data = $this->m_report_buku->GetDataJurnal($model);
        
        $html = $this->load->view("v_report_piutang_kartu_result", $data, true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }

    public function exportpiutangkartu() {
        $model = $this->input->get();
        $this->load->model("m_report_buku");

        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "buku_jurnal");
        set_time_limit(600);
        $data = [];
        $judul = 'Kartu Piutang';
        $data['title'] = [$judul];
        $info = InfoExcel("spout", 1, "Piutang");
        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $model['id_gl_account'] = 4;
        $result = $this->m_report_buku->GetDataJurnal($model, "");
        $listbukujurnal = $result['data'];
        $no = 0;
        $increment = $result['saldo_awal'];
        $data['info'][] = ['', '', '','', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']];


        $data['header'] = array();
        $data['header'][] = 'Tanggal';
        $data['header'][] = 'No Dokumen';
        $data['header'][] = 'No Faktur';
        $data['header'][] = 'Keterangan';
        $data['header'][] = 'Debit';
        $data['header'][] = 'Kredit';
        $data['header'][] = 'Saldo Akhir';
        $data['row'] = [];
        $data['cols'] = [
            20, 25,20, 100, 20, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        //$data['info'][] = array_merge(InfoExcel('spout'), ['', '', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']]);
        foreach ($listbukujurnal as $key => $detail) {
            $increment += @$detail->debet - @$detail->kredit;
            $data['row'][$no] = array();
            $data['row'][$no][] = DefaultTanggal(@$detail->tanggal_buku);
            $data['row'][$no][] = @$detail->dokumen;
            $data['row'][$no][] = @$detail->add_info;
            $data['row'][$no][] = $detail->keterangan;
            $data['row'][$no][] = ['value' => (double) $detail->debet, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->kredit, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $increment, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }

        $this->spout->export($filename, $data);
        set_time_limit(0);
    }

    public function detail() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K091";
        $header = "K005";
        $title = "Rekap Piutang Detail";
        CekModule($module);
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json","", GetCabangAkses());
        $this->load->model("customer/m_customer");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $css = array();
        LoadTemplate($model, "report/v_report_piutang_detail_index", $javascript, $css);
    }

    public function printrekappiutang() {
        $model = $this->input->get();
        $info = "";
        $title = "Laporan Rekap Piutang";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '( Per Tanggal ' . GetDateNow() . ')';
        $listbukujurnal = $this->m_report_piutang->GetDataRekapPiutang($model, "");
        $html = $this->load->view("v_report_piutang_result", array("listdetail" => $listbukujurnal), true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }

    public function exsportrekappiutang() {
        $model = $this->input->get();
        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "r_rekap_piutang");
        set_time_limit(600);
        $data = [];
        $judul = 'Laporan Rekap Piutang';
        $data['title'] = [$judul];
        $info = InfoExcel('spout');

        $data['title'] = [$judul];

        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $data['info'][] = [''];
        $data['header'] = array();
        $data['header'][] = 'No';
        $data['header'][] = 'Nama Customer';
        $data['header'][] = 'Saldo Awal';
        $data['header'][] = 'Piutang';
        $data['header'][] = 'Total Due';
        $data['header'][] = '<=0';
        $data['header'][] = '1 - 30';
        $data['header'][] = '31 - 60';
        $data['header'][] = '61 - 90';
        $data['header'][] = '>= 90';
        $data['row'] = [];
        $data['cols'] = [
            8, 50, 35, 35, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        $listrekappiutang = $this->m_report_piutang->GetDataRekapPiutang($model, "");
        
        $no = 0;
        foreach ($listrekappiutang as $key => $detail) {
            $data['row'][$no] = array();
            $data['row'][$no][] = $no + 1;
            $data['row'][$no][] = $detail->nama_customer;
            $data['row'][$no][] = ['value' => (int) $detail->saldo_awal, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (int) $detail->piutang, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (int) $detail->total_due, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (int) $detail->due0, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (int) $detail->due30, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (int) $detail->due60, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (int) $detail->due90, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (int) $detail->due90plus, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }
        $this->spout->export($filename, $data);
        set_time_limit(0);
    }

    public function exportdetailpiutang() {
        $model = $this->input->get();
        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "r_detail_piutang");
        set_time_limit(600);
        $data = [];
        $judul = 'Laporan Detail Piutang';
        $data['title'] = [$judul];
        $info = InfoExcel('spout');

        $data['title'] = [$judul];

        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $data['info'][] = [''];
        $data['header'] = array();
        $data['header'][] = 'No';
        $data['header'][] = 'Nama Customer';
        $data['header'][] = 'Tanggal';
        $data['header'][] = 'No Faktur';
        $data['header'][] = 'Invoice';
        $data['header'][] = 'Jatuh Tempo';
        $data['header'][] = 'Lama';
        $data['header'][] = 'GrandTotal';
        $data['header'][] = 'Sisa';
        $data['row'] = [];
        $data['cols'] = [
            8, 35, 20, 15, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        $listdetailpiutang = $this->m_report_piutang->GetDataDetailPiutang($model, "");
        $no = 0;
        foreach ($listdetailpiutang as $key => $detail) {
            $data['row'][$no] = array();
            $data['row'][$no][] = $no + 1;
            $data['row'][$no][] = $detail->nama_customer;
            $data['row'][$no][] = DefaultTanggal(@$detail->tanggal);
            $data['row'][$no][] = $detail->no_faktur;
            $data['row'][$no][] = $detail->nomor_master;
            $data['row'][$no][] = DefaultTanggal(@$detail->jth_tempo);
            $data['row'][$no][] = ['value' => (double) $detail->lama_hari, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->grandtotal, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $detail->sisa, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }
        if (count($listdetailpiutang)) {
            $data['footer'][] = [
                null, null, null, null, null, null, null,
                'Total',
                    ['value' => 'SUM(' . getAlphas(9, 9) . ':' . getAlphas(9, $no + 7) . ')', 'type' => 'formula', 'format' => 'number'],
                'ht' => 15
            ];
            $data['merge'][] = getAlphas(1, $no + 9) . ":" . getAlphas(6, $no + 9);
        }
        $this->spout->export($filename, $data);
        set_time_limit(0);
    }
    
    function mainTitle() {
        $title = array('text' => $this->_title, 'style' => array('align' => 'center', 'fontSize' => 20, 'bold' => true));

        return $title;
    }

    function headInfo() {
        $info = array();
        $info[] = array('label' => 'Tanggal', 'value' => date('d M Y H:i'), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => 'Pembuat', 'value' => GetUsername(), 'style' => array('align' => 'left', 'fontSize' => 12));
        $info[] = array('label' => '', 'value' => $this->getAddInfo(), 'style' => array('align' => 'left', 'fontSize' => 12));

        return $info;
    }
    function rowSet($data) {
        $result = array();
        $nomor = 1;
        foreach ($data as $row) {
            $row = json_decode(json_encode($row), true);
            $row['nomor'] = $nomor;
            $row['tanggal_prospek'] = DefaultTanggal($row['tanggal_prospek']);
            $row['tanggal_do'] = DefaultTanggal($row['tanggal_do']);
            $row['tanggal_buka_jual'] = DefaultTanggal($row['tanggal_buka_jual']);
            $row['harga_off_road'] = (int) $row['harga_off_road'];
            $row['harga_on_the_road'] = (int) $row['harga_on_the_road'];
            $row['total_equipment'] = (int) $row['total_equipment'];
            $row['harga_jual_unit'] = (int) $row['harga_jual_unit'];
            $row['sisa'] = (int) $row['sisa'];
            $row['jumlah_pembayaran'] = (int) $row['jumlah_pembayaran'];
            $row['biaya_bbn'] = (int) $row['biaya_bbn'];
            $row['bulan'] = GetMonth((int)date('m', strtotime($row['tanggal_prospek'])));
           
            $nomor++;

            if ($row['status'] != 2) {
                $this->_off_road += (int) $row['harga_off_road'];
                $this->_on_road += (int) $row['harga_on_the_road'];
                $this->_equipment += (int) $row['total_equipment'];
                $this->_harga_unit += (int) $row['harga_jual_unit'];
                $this->_biaya_bbn += (int) $row['biaya_bbn'];
            }
            $result[]=$row;
        }

        return $result;
    }
    function footInfo() {
        $info = array();
        $info[] = array(
            array('text' => 'Total', 'bold' => true, 'align' => 'right', 'border' => true, 'merge' => 4),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
            array('text' => "", 'align' => 'right', 'border' => true, 'format' => 'nominal'),
        );

        return $info;
    }
    function colSet() {
        $col = array(
            array('colName' => 'No', 'valueKey' => 'nomor', 'colWidth' => 10, 'bold' => true),
            array('colName' => 'Tanggal', 'valueKey' => 'tanggal_prospek', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'Bulan', 'valueKey' => 'bulan', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Customer', 'valueKey' => 'customer_name', 'colWidth' => 30, 'bold' => true),
            array('colName' => 'Alamat', 'valueKey' => 'alamat', 'colWidth' => 25, 'bold' => true),
            array('colName' => 'Telp', 'valueKey' => 'no_telp', 'colWidth' => 15),
            array('colName' => 'NO SPK', 'valueKey' => 'no_prospek', 'colWidth' => 15),
            array('colName' => 'Salesman', 'valueKey' => 'nama_pegawai', 'colWidth' => 15),
            array('colName' => 'Cabang', 'valueKey' => 'nama_cabang', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Manager', 'valueKey' => 'nama_manager', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Kategori', 'valueKey' => 'nama_kategori', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Unit', 'valueKey' => 'nama_unit', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Tanggal DO', 'valueKey' => 'tanggal_do', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'No DO', 'valueKey' => 'nomor_do_prospek', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Buka Jual', 'valueKey' => 'tanggal_buka_jual', 'colWidth' => 15, 'bold' => true, 'format' => 'datetime'),
            array('colName' => 'No Buka Jual', 'valueKey' => 'nomor_buka_jual', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'No Rangka', 'valueKey' => 'vin_number', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'No Mesin', 'valueKey' => 'engine_no', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'VIN', 'valueKey' => 'tahun', 'colWidth' => 15, 'bold' => true),
            array('colName' => 'Off Road', 'valueKey' => 'harga_off_the_road', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'BBN', 'valueKey' => 'biaya_bbn', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'OTR', 'valueKey' => 'harga_on_the_road', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Equipment', 'valueKey' => 'total_equipment', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Grandtotal', 'valueKey' => 'harga_jual_unit', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Total Pembayaran', 'valueKey' => 'jumlah_pembayaran', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
            array('colName' => 'Sisa', 'valueKey' => 'sisa', 'colWidth' => 15, 'bold' => true, 'format' => 'nominal', 'align' => 'right'),
        );

        return $col;
    }

    function getAddInfo() {
        $search = $this->input->get();
        $info = array();
        $text = "";

        if (count($search)) {

            $params = array_filter($search);

            if (isset($params['tahun']) && !CheckEmpty($params['tahun']))
                array_push($info, "Tahun: " . $params['tahun']);

            if (isset($params['bulan']) && !CheckEmpty($params['bulan']))
                array_push($info, "Bulan: " . GetMonth($params['bulan']));
        }
        if (count($info)) {
            $text = implode(" | ", $info);
        }

        return $text;
    }
    function piutang_unit_export() {
        $this->load->library('Excel');

        $search = $this->input->get();

        $data = $this->m_report_piutang->GetDataReportPiutangUnit($search, "export");

        if (count($data)) {
            $params = array(
                'sNAMESS' => "Penjualan Unit",
                'sFILENAM' => date('Ymd') . '_' . str_replace(" ", "_", $this->_title),
                'mainTitle' => $this->mainTitle(),
                'headInfo' => $this->headInfo(),
                'colSet' => $this->colSet(),
                'rowSet' => $this->rowSet($data),
                'footInfo' => $this->footInfo(),
            );
            $this->excel->genExcel($params, false);
        } else {
            echo 'Data yang sesuai dengan kriteria pencarian tidak ditemukan.';
        }
    }

    public function getdatarekappiutang() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_piutang->GetDataRekapPiutang($params);
        
    }

    public function getdatadetailpiutang() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo $this->m_report_piutang->GetDataDetailPiutang($params);
    }

}

/* End of file Barang.php */