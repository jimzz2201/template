<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class R_akuntansi extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_akuntansi');
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K100";
        $header = "K005";
        $title = "Pembelian";
        CekModule($module);

        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("pegawai/m_pegawai");
        $model['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
        LoadTemplate($model, "report/v_report_akuntansi_index", $javascript, $css);
    }
    
    
    public function buku_besar_view()
    {
        
        $model=$this->input->post();
        $this->load->model("gl/m_gl");
        $id_gl_header=CheckIfIsset($model,"id_gl_header",0);
        $id_gl_parent=CheckIfIsset($model,"id_gl_parent",0);
        $is_hpp=CheckIfIsset($model,"is_hpp",0);
        $id_gl_labarugi=CheckIfIsset($model,"id_gl_labarugi",0);
        $id_gl_account=CheckIfIsset($model,"id_gl_account",0);
        $listgl=$this->m_gl->GetDropDownGl(0,$id_gl_header,$id_gl_parent,$is_hpp,$id_gl_labarugi,$id_gl_account);
        $listgl=array_column($listgl,"id");
        $model['listgl']=$listgl;
        $listdata=$this->m_akuntansi->GetDataBukuBesar($model);
        
        $listtampung=[];
        if(CheckEmpty($model['grouping']))
        {
            $listtampung[""]=[];
            foreach($listdata as $datasat)
            {
                $listtampung[""][]=$datasat;
            }
        }
        else
        {
            foreach($listdata as $datasat)
            {
                if(!CheckKey($listtampung, $datasat->nama_gl_account))
                {
                    $listtampung[$datasat->nama_gl_account]=[];
                }
                $listtampung[$datasat->nama_gl_account][]=$datasat;
            }
        }
        
        
        $this->load->view("v_report_buku_besar_result", array("data"=>$listtampung));
    }
    public function printbukubesar() {
        $model = $this->input->get();
        $info = "";
        $title = "Laporan Buku Besar";
        $info = InfoExcel("spout");
        $keyword = $info[2];
        $subheader = '(' . @$info[3][2] . ')';
        
        $this->load->model("gl/m_gl");
        $id_gl_header=CheckIfIsset($model,"id_gl_header",0);
        $id_gl_parent=CheckIfIsset($model,"id_gl_parent",0);
        $is_hpp=CheckIfIsset($model,"is_hpp",0);
        $id_gl_labarugi=CheckIfIsset($model,"id_gl_labarugi",0);
        $id_gl_account=CheckIfIsset($model,"id_gl_account",0);
        $listgl=$this->m_gl->GetDropDownGl(0,$id_gl_header,$id_gl_parent,$is_hpp,$id_gl_labarugi,$id_gl_account);
        $listgl=array_column($listgl,"id");
        $model['listgl']=$listgl;
        $listdata=$this->m_akuntansi->GetDataBukuBesar($model);
        
        $listtampung=[];
        if(CheckEmpty($model['grouping']))
        {
            $listtampung[""]=[];
            foreach($listdata as $datasat)
            {
                $listtampung[""][]=$datasat;
            }
        }
        else
        {
            foreach($listdata as $datasat)
            {
                if(!CheckKey($listtampung, $datasat->nama_gl_account))
                {
                    $listtampung[$datasat->nama_gl_account]=[];
                }
                $listtampung[$datasat->nama_gl_account][]=$datasat;
            }
        }
        $html = $this->load->view("v_report_buku_besar_result",  array("data"=>$listtampung), true);
        $this->load->view("v_report_print", array("html" => $html, "info" => $info, "title" => $title, "subheader" => $subheader, "keyword" => $keyword));
    }
    
    
    public function exportbukubesar() {
        $model = $this->input->get();
        $this->load->library('Spout');
        $filename = date('Ymd') . '_' . str_replace(" ", "_", GetUsername() . '_' . "buku_kas");
        set_time_limit(600);
        $data = [];
        $judul = 'Laporan Buku Besar';
        $data['title'] = [$judul];
        $info = InfoExcel('spout');
        $data['info'] = array();
        $data['info'][] = [''];
        $data['info'][] = ['', 'Tanggal', date('d-m-Y H:i:s')];
        $data['info'][] = ['', 'Pembuat', GetUsername()];
        $data['info'][] = $info[3];
        unset($info[3]);
        $data['info'][] = $info;
        $this->load->model("gl/m_gl");
        $id_gl_header=CheckIfIsset($model,"id_gl_header",0);
        $id_gl_parent=CheckIfIsset($model,"id_gl_parent",0);
        $is_hpp=CheckIfIsset($model,"is_hpp",0);
        $id_gl_labarugi=CheckIfIsset($model,"id_gl_labarugi",0);
        $id_gl_account=CheckIfIsset($model,"id_gl_account",0);
        $listgl=$this->m_gl->GetDropDownGl(0,$id_gl_header,$id_gl_parent,$is_hpp,$id_gl_labarugi,$id_gl_account);
        $listgl=array_column($listgl,"id");
        $model['listgl']=$listgl;
        $listdata=$this->m_akuntansi->GetDataBukuBesar($model);
        
        $listtampung=[];
        if(CheckEmpty($model['grouping']))
        {
            $listtampung[""]=[];
            foreach($listdata as $datasat)
            {
                $listtampung[""][]=$datasat;
            }
        }
        else
        {
            foreach($listdata as $datasat)
            {
                if(!CheckKey($listtampung, $datasat->nama_gl_account))
                {
                    $listtampung[$datasat->nama_gl_account]=[];
                }
                $listtampung[$datasat->nama_gl_account][]=$datasat;
            }
        }
        
        $no = 0;
        

       
        $data['cols'] = [
            20, 25, 20, 100, 20, 35, 35, 35, 35, 35, 35, 15, 10, 8, 15, 10, 15, 15, 15, 15, 16, 20, 20, 21, 15, 20, 15, 15, 15, 15, 15, 15
        ];

        //$data['info'][] = array_merge(InfoExcel('spout'), ['', '', '', 'Saldo Awal', ['value' => (double) $increment, 'format' => 'number']]);

        foreach ($listtampung as $key => $detsat) {
            $data['row'][$no] = array();
            $data['row'][$no]['ht']= 20;
            $no++;
            $data['row'][$no] = array();
            $data['row'][$no][] = 'Tanggal';
            $data['row'][$no][] = 'No Dokumen';
            $data['row'][$no][] = 'No Faktur';
            $data['row'][$no][] = 'Keterangan';
            $data['row'][$no][] = 'Jenis';
            $data['row'][$no][] = 'Debit';
            $data['row'][$no][] = 'Kredit';
            $data['row'][$no]['ht']= 15;
            $no++;
            $debet=0;
            $kredit=0;
            foreach ($detsat as $detail) {
                $data['row'][$no] = array();
                $data['row'][$no][] = DefaultTanggal(@$detail->tanggal_buku);
                $data['row'][$no][] = @$detail->dokumen;
                $data['row'][$no][] = @$detail->add_info;
                $data['row'][$no][] = ['value' => @$detail->keterangan, 'format' => 'string'];
                $data['row'][$no][] = @$detail->nama_gl_account;
                $data['row'][$no][] = ['value' => (double) $detail->debet, 'format' => 'number'];
                $data['row'][$no][] = ['value' => (double) $detail->kredit, 'format' => 'number'];
                $data['row'][$no]['ht'] = 15;
                $debet+=$detail->debet;
                $kredit+=$detail->kredit;
                $no++;
            }
            $data['row'][$no] = array();
            $data['row'][$no][] = "";
            $data['row'][$no][] = "";
            $data['row'][$no][] = "";
            $data['row'][$no][] = "Total";
            $data['row'][$no][] = $key;
            $data['row'][$no][] = ['value' => (double) $debet, 'format' => 'number'];
            $data['row'][$no][] = ['value' => (double) $kredit, 'format' => 'number'];
            $data['row'][$no]['ht'] = 15;
            $no++;
        }




        $this->spout->export($filename, $data);
        set_time_limit(0);
    }

    public function bukubesar() {
        $javascript = array();
        $this->load->model("gl/m_gl");
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K100";
        $header = "K005";
        $title = "Buku Besar";
        CekModule($module);
        $css = array();
        $this->load->model("kas/m_kas");
        $this->load->model("cabang/m_cabang");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_jenis_transaksi'] = array(array("id" => "32", "text" => "Kas Kandang"), array("id" => "3", "text" => "Kas Kantor"));
        $model['list_status'] = array("2" => "Batal", "1" => "Non Batal");
        $model['list_is_hpp'] = array("2" => "Non HPP", "1" => "HPP");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $model['list_gl_header'] = $this->m_gl->GetDropDownGlHeader();
        $model['list_gl_account_parent'] = $this->m_gl->GetDropDownGl(1);
        $model['list_gl_account'] = $this->m_gl->GetDropDownGl(0);
        $model['list_gl_labarugi'] = $this->m_gl->GetDropDownGlLabaRugi();

        LoadTemplate($model, "report/v_report_buku_besar_index", $javascript, $css);
    }

    public function omzet_customer() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);

        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("customer/m_customer");
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        LoadTemplate($model, "report/v_report_akuntansi_omzet_customer", $javascript, $css);
    }

    public function biaya() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);

        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("biaya_master/m_biaya_master");
        $model['list_group'] = $this->m_biaya_master->GetDropDownGroup();
        $this->load->model("biaya_master/m_biaya_master");
        $model['list_master'] = $this->m_biaya_master->GetDropDownBiayaMaster();
        LoadTemplate($model, "report/v_report_akuntansi_biaya", $javascript, $css);
    }

    public function biaya_bank() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K054";
        $header = "K045";
        $title = "Pembelian";
        CekModule($module);

        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $css = array();
        $css[] = 'assets/css/datapicker/datepicker3.css';
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $this->load->model("biaya_master/m_biaya_master");
        $model['list_group'] = $this->m_biaya_master->GetDropDownGroup();
        $this->load->model("biaya_master/m_biaya_master");
        $model['list_master'] = $this->m_biaya_master->GetDropDownBiayaMaster();
        LoadTemplate($model, "report/v_report_akuntansi_biaya_bank", $javascript, $css);
    }

    public function getdataakuntansi() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdataakuntansiomzetcustomer() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdataakuntansibiaya() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

    public function getdataakuntansibiayabank() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        echo json_encode(array("draw" => 1, "recordsTotal" => "2", "recordsFiltered" => "2", "data" => []));
    }

}

/* End of file Barang.php */