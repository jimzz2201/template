
<table class="table table-striped table-bordered table-hover" id="mytable">
    <thead>
        <tr>
            <th>Tanggal</th>

            <th>No Dokumen</th>
            <th>Nama</th>
            <th>Faktur Pembayaran</th>
            <th>Pembayaran</th>
            <th>Tunai</th>
            <th>Transfer</th>
            <th>Cek & Giro</th>
            <th>Retur</th>
            <th>Potongan</th>
            <th>Nama Bank</th>
        </tr>
    </thead>
    <tbody id="">
        <?php
        if (count(@$data) > 0) {
            $totaltunai = 0;
            $totaltransfer = 0;
            $totalcekgiro = 0;
            $totalretur = 0;
            $totalpotongan = 0;
            foreach (@$data as $detail) {
                $tunai = 0;
                $transfer = 0;
                $cekgiro = 0;
                $retur = 0;
                $potongan =0;
                if(strpos($detail->jenis_pembayaran, "Tunai")!==false ||$detail->jenis_pembayaran=="Pembayaran Di Muka"||$detail->jenis_pembayaran=="Cash")
                {
                    $tunai=$detail->total_bayar;
                    $totaltunai+=$tunai;
                }
                else if($detail->jenis_pembayaran=="retur")
                {
                    $retur=$detail->total_bayar;
                    $totalretur+=$retur;
                }
                else if($detail->jenis_pembayaran=="Transfer")
                {
                    $transfer=$detail->total_bayar;
                    $totaltransfer+=$transfer;
                }
                else if($detail->jenis_pembayaran=="Potongan")
                {
                    $potongan=$detail->total_bayar;
                    $totalpotongan+=$potongan;
                }
                else if($detail->jenis_pembayaran=="Cek & Giro")
                {
                    $cekgiro=$detail->total_bayar;
                    $totalcekgiro+=$cekgiro;
                }
                
                ?>
                <tr>
                    <td><?php echo DefaultTanggal(@$detail->tanggal_transaksi) ?></td>

                    <td><?= @$detail->dokumen ?></td>
                    <td><?= @$detail->nama_supplier ?></td>
                    <td><?= @$detail->no_faktur ?></td>
                    <td><?= @$detail->jenis_pembayaran ?></td>
                    <td align="right"><?= DefaultCurrency(@$tunai) ?></td>
                    <td align="right"><?= DefaultCurrency(@$transfer) ?></td>
                    <td align="right"><?= DefaultCurrency(@$cekgiro) ?></td>
                    <td align="right"><?= DefaultCurrency(@$retur) ?></td>
                    <td align="right"><?= DefaultCurrency(@$potongan) ?></td>
                    <td align="right"><?= @$detail->nama_bank ?></td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td colspan="5" align="right"> Total</td>
                <td><?= DefaultCurrency($totaltunai) ?></td>
                <td><?= DefaultCurrency($totaltransfer) ?></td>
                <td><?= DefaultCurrency($totalcekgiro) ?></td>
                <td><?= DefaultCurrency($totalretur) ?></td>
                <td><?= DefaultCurrency($totalpotongan) ?></td>
                <td>&nbsp;</td>
            </tr>
            <?php
        } else {
            echo "<td style='text-align:center'  colspan=\"11\">No Data Found</td>";
        }
        ?>
    </tbody>
</table>