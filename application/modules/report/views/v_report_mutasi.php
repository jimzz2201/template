<section class="content-header">
    <h1>
        Rekap Mutasi <?= @$button ?>
        <small>Mutasi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Mutasi</li>
        <li class="active">Mutasi <?= @$button ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        
                        
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'selected' => @$value_status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                        </div>
                      
                       
                    </div>


                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$cabang, array('class' => 'form-control select2', 'id' => 'dd_cabang')); ?>
                        </div>
                         <div class="col-sm-2">
                            <?= form_dropdown(array("name" => "id_pool_from"), DefaultEmptyDropdown(@$list_pool, "json", "Pool"), @$id_pool_from, array('class' => 'form-control select2', 'id' => 'dd_id_pool_from')); ?>
                        </div>
                         <div class="col-sm-2">
                            <?= form_dropdown(array("name" => "id_pool"), DefaultEmptyDropdown(@$list_pool, "json", "Pool"), @$id_pool, array('class' => 'form-control select2', 'id' => 'dd_id_pool')); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                       
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                          <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                    </div>
                </div>


            </form>
            <hr/>
            <div class="portlet-body form">
                <table class="table table-striped table-bordered table-hover" id="mytable">

                </table>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        table.fnDraw(false);
        return false;
    })
 $("#btt_Eksport").click(function(){
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_mutasi/export?'+search;
         window.open(url);
    })

    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "<?php echo base_url()?>index.php/report/r_mutasi/getdatamutasi", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal_do", orderable: false, title: "Tanggal DO",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "nomor_mutasi", orderable: false, title: "Nomor Mutasi"},
                {data: "nama_customer", orderable: true, title: "Nama Customer"},
                {data: "nama_pool_from", orderable: true, title: "From"},
                {data: "nama_pool_to", orderable: true, title: "To"},
                {data: "up", orderable: false, title: "Up"},
                {data: "no_prospek", orderable: false, title: "No SPK"},
                {data: "nama_type_unit", orderable: false, title: "Type"},
                
                {data: "tanggal_buka_jual", orderable: false, title: "Buka Jual",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "vin_number", orderable: false, title: "No Rangka"},
                {data: "engine_no", orderable: false, title: "No Mesin"},
                {data: "vin", orderable: false, title: "VIN"}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            }
        });

    });
</script>
