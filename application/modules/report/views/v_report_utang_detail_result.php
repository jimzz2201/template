
<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Tanggal</th>
            <th>No Faktur</th>
            <th>Invoice</th>
            <th>Jatuh Tempo</th>
            <th>Lama</th>
            <th>GrandTotal</th>
            <th>Sisa</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 0;
        $jumlah = 0;
        $sisa = 0;
        foreach ($listdetail as $detail) {
            $no++;
            $sisa += $detail->sisa;
            ?>
            <tr>
                <td><?php echo $no ?></td>

                <td><?php echo $detail->nama_supplier ?></td>
                <td><?php echo str_replace(" ", "&nbsp;", DefaultTanggal($detail->tanggal)) ?></td>
                <td><?php echo $detail->no_faktur ?></td>
                <td style="width:100px;"><?php echo $detail->nomor_master ?></td>
                <td><?php echo DefaultTanggal($detail->jth_tempo) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->lama_hari) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->grandtotal) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->sisa) ?></td>

            </tr>
        <?php } ?>
        <tr style="border-top:solid 1px #000;">
            <td colspan="8" class="tdnumber">Total</td>

            <td class="tdnumber"><?php echo DefaultCurrency($sisa) ?></td>

        </tr>
    </tbody>
</table>
