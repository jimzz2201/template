<section class="content-header">
    <h1>
        Rekap Unit <?= @$button ?>
        <small>Unit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Unit</li>
        <li class="active">Unit <?= @$button ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), DefaultEmptyDropdown(@$list_pencarian, "json", "Pencarian")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date)); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_kategori', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_kategori', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori")); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_type_body', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_type_body', 'placeholder' => 'Type Body'), DefaultEmptyDropdown(@$list_type_body, "json", "Type Body")); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('', "", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_type_unit', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_type_unit', 'placeholder' => 'Type Unit'), DefaultEmptyDropdown(@$list_type_unit, "json", "Type Unit")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_unit', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_unit', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'vin', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_vin', 'placeholder' => 'Vin'), DefaultEmptyDropdown(@$list_vin, "json", "Vin")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_pool', 'value' => @$id_pool, 'class' => 'form-control select2', 'id' => 'dd_id_pool', 'placeholder' => 'Pool'), DefaultEmptyDropdown(@$list_pool, "json", "Pool")); ?>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'pencarian_keyword', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_pencarian_keyword', 'placeholder' => 'Pencarian'), DefaultEmptyDropdown(@$list_pencarian_keyword, "json", "Pencarian Keyword")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'Placeholder' => 'Keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('', "", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                    </div>
                </div>
            </form>
            <hr/>
            <div class="portlet-body form">
                <table class="table table-striped table-bordered table-hover" id="mytable">

                </table>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;

    function CekPencarian() {
        if (CheckEmpty($("#dd_jenis_pencarian").val()))
        {
            $("#txt_tanggal_awal").datepicker("disable");
            $("#txt_tanggal_akhir").datepicker("disable");
        } else
        {
            $("#txt_tanggal_awal").datepicker("enable");
            $("#txt_tanggal_awal").val("<?php echo DefaultDatePicker(GetDateNow())?>");
            $("#txt_tanggal_akhir").val("<?php echo DefaultDatePicker(GetDateNow())?>");
            $("#txt_tanggal_akhir").datepicker("enable");
        }
    }
    function CekKeyword() {
        if (CheckEmpty($("#dd_pencarian_keyword").val()))
        {
            $("#txt_keyword").attr("disabled", "disabled");

        } else
        {
            $("#txt_keyword").removeAttr("disabled");
        }
    }

    $("#dd_pencarian_keyword").change(function () {
        CekKeyword();
    })
    $("#dd_jenis_pencarian").change(function () {
        CekPencarian();
     })


    $("form#frm_search").submit(function () {
        table.fnDraw(false);
        return false;
    })
    $("#btt_Eksport").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_unit/export?' + search;
        window.open(url);
    })
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
         $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        CekPencarian();
        CekKeyword();
   
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "r_unit/getdataunit", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_unit_serial",
                    title: "No",
                    orderable: false
                },
				{data: "nama_customer", orderable: false, title: "Customer"},
                {data: "nama_kategori", orderable: false, title: "Category"},
                {data: "nama_unit", orderable: false, title: "Unit"},
                {data: "vin_number", orderable: false, title: "Vin Number"},
                {data: "engine_no", orderable: false, title: "Engine"},
                {data: "nama_warna", orderable: false, title: "Warna"},
                {data: "vin", orderable: false, title: "Vin", "width": "30px", className: "text-center"},
                {data: "tanggal_buka_jual", orderable: false, title: "Tanggal Buka Jual",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
                {data: "tanggal_prospek", orderable: false, title: "Tanggal SPK",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
               
                {data: "dnp", orderable: false, title: "DNP", "className": "text-right",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "nama_pool", orderable: false, title: "Position"},
                {data: "status_buka_jual", orderable: false, title: "Status"},
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            }
        });

    });
</script>
