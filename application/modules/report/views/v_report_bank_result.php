
<div style="float:right;font-weight:bold;padding-right:40px;padding-bottom:20px;">Saldo Awal : <?= DefaultCurrency(@$saldo_awal); 
$increment= DefaultCurrencyDatabase(@$saldo_awal);

?></div>

<table class="table table-striped table-bordered table-hover" id="mytable">
    <thead>
        <tr>
            <th>Tanggal</th>
            <th>Debit</th>
            <th>Kredit</th>
            <th>No Dokumen</th>
            <th>Keterangan</th>
            <th>Saldo Akhir</th>
        </tr>
    </thead>

    <tbody id="">
     <?php
        if (count(@$data) > 0) {
            foreach (@$data as $detail) {
                $increment+=@$detail->debet-@$detail->kredit;
                ?>
                <tr>
                    <td><?php echo DefaultTanggal(@$detail->tanggal_buku) ?></td>
                    <td><?= DefaultCurrency(@$detail->debet) ?></td>
                    <td><?= DefaultCurrency(@$detail->kredit) ?></td>
                    <td><?= @$detail->dokumen ?></td>
                    <td><?= @$detail->keterangan ?></td>
                    <td><?= DefaultCurrency($increment) ?></td>
                </tr>
                <?php
            }
        } else {
            echo "<td style='text-align:center'  colspan=\"6\">No Data Found</td>";
        }
        ?>
    </tbody>
</table>