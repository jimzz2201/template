<?php
    if(CekModule("K053",false))
    {
        echo anchor(site_url('report/r_pembelian'), '<i class="fa fa-file"></i> Rekap ', 'class="btn btn-success"'); 
    }
    
    if(CekModule("K054",false))
    {
        echo anchor(site_url('report/r_pembelian/detail'), '<i class="fa fa-file"></i> Detail ', 'class="btn btn-info"');
    }
    if(CekModule("K055",false))
    {
        echo anchor(site_url('report/r_pembelian/per_group'), '<i class="fa fa-file"></i> Per Group ', 'class="btn btn-danger"');
    }
    if(CekModule("K056",false))
    {
        echo anchor(site_url('report/r_pembelian/pajak_masukan'), '<i class="fa fa-file"></i> Pajak Masukan ', 'class="btn btn-warning"');
    }
    