<section class="content-header">
    <h1>
        Rekap Penjualan <?= @$button ?>
        <small>Penjualan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Penjualan</li>
        <li class="active">Penjualan <?= @$button ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                         <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), @$list_pencarian); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'selected' => @$id_customer, 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'selected' => @$status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                        </div>
                     
                       
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'selected' => @$id_cabang, 'class' => 'form-control select2', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'level', 'selected' => @$level -1 , 'class' => 'form-control select2', 'id' => 'dd_level', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_level, "json", "Level")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                    </div>
                </div>
                
                
            </form>
            <hr/>
            <div class="portlet-body form">
                <table class="table table-striped table-bordered table-hover" id="mytable">

                </table>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        table.fnDraw(false);
        return false;
    })

    $("#btt_Eksport").click(function(){
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_penjualan/export?'+search;
         window.open(url);
    })

    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "r_penjualan/getdatapenjualan", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_prospek",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal_prospek", orderable: false, title: "Tanggal",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "nama_customer", orderable: true, title: "Customer"},
                {data: "nama_jenis_plat", orderable: false, title: "Jenis Plat"},
                {data: "nama_unit", orderable: false, title: "Unit"},
                {data: "jumlah_unit", orderable: false, title: "Qty", "className": "text-center", "width": "20px"},
                {data: "nama_warna", orderable: false, title: "Warna"},
                {data: "tahun", orderable: true, title: "Tahun"},
                {data: "harga_jual_unit", orderable: false, title: "Harga Jual Unit", "className": "text-right",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "nama_payment_method", orderable: true, title: "Payment Method"},
                {data: "nama_pegawai", orderable: true, title: "Salesman"},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        if (data == 1)
                        {
                            return "Approved";
                        } else if (data == 2)
                        {
                            return "Rejected";
                        } else if (data == 3)
                        {
                            return "Closed";
                        } else if (data == 4)
                        {
                            return "Finished";
                        } else if (data == 5)
                        {
                            return "Batal";
                        } else
                        {
                            return "Pending";
                        }

                    }}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            }
        });

    });
</script>
