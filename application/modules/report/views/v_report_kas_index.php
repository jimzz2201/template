<section class="content-header">
    <h1>
        Kartu Kas <?= @$button ?>
        <small>Kas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Kas</li>
        <li class="active">Kas <?= @$button ?></li>
    </ol>
</section>

<style>
    th{
        text-align: center;
    }

    tbody td{
        padding:10px;
        text-align: center;
    }
</style>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_gl_account', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_kas', 'placeholder' => 'Transaksi'), DefaultEmptyDropdown(@$list_jenis_transaksi, "json", "Jenis Kas")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_cabang', 'placeholder' => 'Transaksi'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'value' => "", 'class' => 'form-control', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "obj", "Status")); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Print" type="button" class="btn btn-block btn-default pull-right">Print</button>
                        </div>
                    </div>
                </div>

            </form>

            <div class="portlet-body form" id="databody">

            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $("#btt_Print").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_kas/printkas?' + search;
        window.open(url);
    })
    $("#btt_Eksport").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_kas/exportkas?' + search;
        window.open(url);
    })
    function RefreshGrid()
    {
        LoadBar.show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/report/r_kas/get_data_view',
            data: $("#frm_search").serialize(),
            success: function (data) {
                $("#databody").html(data);
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });

    }
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $("select").select2();

    });
</script>
