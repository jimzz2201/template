<section class="content-header">
    <h1>
        Rekap DO KPU <?= @$button ?>
        <small>DO KPU</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>DO KPU</li>
        <li class="active">DO KPU <?= @$button ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'value' => "", 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$cabang, array('class' => 'form-control select2', 'id' => 'dd_cabang')); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("name" => "id_pegawai"), DefaultEmptyDropdown(@$list_pegawai, "json", "Pegawai"), @$pegawai, array('class' => 'form-control select2', 'id' => 'dd_id_pegawai')); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                          <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                    </div>
                </div>


            </form>
            <hr/>
            <div class="portlet-body form">
                <table class="table table-striped table-bordered table-hover" id="mytable">

                </table>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        table.fnDraw(false);
        return false;
    })
 $("#btt_Eksport").click(function(){
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_do_kpu/export?'+search;
         window.open(url);
    })

    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "<?php echo base_url()?>index.php/report/r_do_kpu/getdatadokpu", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
                columns: [
                {
                    data: "id_do_kpu",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal_do", orderable: false, title: "Tanggal", "className": "text-center",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
                {data: "nomor_do_kpu", orderable: false, title: "Nomor DO"},
                {data: "nomor_master", orderable: false, title: "KPU"},
                {data: "nama_supplier", orderable: false, title: "Supplier"},
                {data: "nama_unit", orderable: false, title: "Unit", className: "text-center"},
                {data: "tanggal_terima", orderable: false, title: "Tanggal Terima", "className": "text-right",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},

                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        if(data==0)
                            return "Pending";
                        else if(data==1)
                            return "Selesai";
                        else if(data==2)
                            return "Batal";
                        else if(data==5)
                            return "Close";
                        else 
                            return "";
                    }}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            }
        });

    });
</script>
