
<section class="content-header">
    <h1>
        Rekap Piutang
        <small>Laporan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Laporan</li>
        <li class="active">Rekap Piutang</li>
    </ol>


</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'value' => @$id_cabang, 'class' => 'form-control', 'id' => 'id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>

                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Print" type="button" class="btn btn-block btn-default pull-right">Print</button>
                        </div>
                    </div>
                </div>



            </form>
        </div>
        <div class="box-body">
            <div id="notification" ></div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {

        table.fnDraw(false);
        return false;
    })
    $("#btt_Print").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_piutang/printrekappiutang?' + search;
        window.open(url);
    })
    $("#btt_Eksport").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_piutang/exsportrekappiutang?' + search;
        window.open(url);
    })
    $(document).ready(function () {
        $("select").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $(".datepicker").datepicker();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl + "index.php/report/r_piutang/getdatarekappiutang", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_customer",
                    title: "Kode",
                    orderable: false,
                    width: "50px"
                }
                , {data: "nama_customer", orderable: false, title: "Nama", width: "80px"}
                , {data: "saldo_awal", orderable: false, class: 'text-right', title: "Saldo&nbsp;Awal", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "piutang", orderable: false, class: 'text-right', title: "Piutang", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "total_due", orderable: false, class: 'text-right', title: "Total&nbsp;Due", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "due0", orderable: false, class: 'text-right', title: "<=0", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "due30", orderable: false, class: 'text-right', title: "1&nbsp-&nbsp30", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "due60", orderable: false, class: 'text-right', title: "31&nbsp-&nbsp60", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "due90", orderable: false, class: 'text-right', title: "61&nbsp-&nbsp90", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "due90plus", orderable: false, class: 'text-right', title: ">=90", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {

            }
        });

    });
</script>
