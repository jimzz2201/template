<section class="content-header">
    <h1>
        Rekap Recording <?= @$button ?>
        <small>Recording</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Recording</li>
        <li class="active">Recording <?= @$button ?></li>
    </ol>
</section>
<style>
    th{
        text-align: center;
    }
   
    tbody td{
        padding:10px;
         text-align: center;
    }
</style>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>
                        <?= form_label('s / d', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                        <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'value' => @$id_cabang, 'class' => 'form-control', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                        
                    </div>
                </div>

            </form>

            <div class="portlet-body form">
                <table class="table table-striped table-bordered table-hover" id="mytable">
                    <thead>
                        <tr>
                            <th rowspan="2">Minggu Ke</th>
                            <th rowspan="2">Kandang</th>
                            <th rowspan="2">Umur</th>
                            <th colspan="7">Populasi</th>
                            <th colspan="11">Produksi</th>
                            <th colspan="4">Pakan</th>
                        </tr>
                        <tr>
                            <th>Awal</th>
                            <th>SO</th>
                            <th>Pindah</th>
                            <th>Afkit</th>
                            <th>Mati</th>
                            <th>Mati&nbsp;% </th>
                            <th>Akhir</th>
                            <th>Utuh</th>
                            <th>Utuh&nbsp;%</th>
                            <th>Putih</th>
                            <th>Putih&nbsp;% </th>
                            <th>Retak</th>
                            <th>Retak&nbsp;% </th>
                            <th>HD&nbsp;%</th>
                            <th>Σ&nbsp;Butir</th>
                            <th>Σ&nbsp;KG</th>
                            <th>Gr&nbsp;/&nbsp;Butir</th>
                            <th>KG&nbsp;/&nbsp;1000</th>
                            <th>Gr&nbsp;/&nbsp;Ekor</th>
                            <th>Σ&nbsp;KG</th>
                           
                            <th>FCR</th>
                        </tr>
                    </thead>
                    <tbody id="databody">
                    <td style='text-align:center'  colspan="25">No Data Found</td>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    
    $("#btt_Eksport").click(function(){
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_recording/export?'+search;
         window.open(url);
    })
    function RefreshGrid()
    {
        LoadBar.show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/report/r_recording/get_data_view',
            data: $("#frm_search").serialize(),
            success: function (data) {
                $("#databody").html(data);
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $("select").select2();



    });
</script>
