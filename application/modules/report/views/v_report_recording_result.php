
<?php
if (count(@$data) > 0) {
    foreach (@$data as $detail) {
        ?>

        <tr>
            <td><?php echo DefaultTanggal(@$detail->tanggal) ?></td>
            <td><?= @$detail->nama_kandang ?></td>
            <td><?= DefaultCurrency(@$detail->umur) ?></td>
            <td><?= DefaultCurrency(@$detail->jml_awal) ?></td>
            <td><?= DefaultCurrency(@$detail->jml_so) ?></td>
            <td><?= DefaultCurrency(@$detail->jml_pindah) ?></td>
            <td><?= DefaultCurrency(@$detail->jml_afkir) ?></td>
            <td><?= DefaultCurrency(@$detail->jml_mati) ?></td>
            <td style="width:20px"><?php
                $persenmati = (@$detail->jml_awal - @$detail->jml_pindah) > 0 ? (@$detail->jml_mati / (@$detail->jml_awal - @$detail->jml_pindah) * 100) : 0;
                if ($persenmati > 0.38) {
                    echo '<b style="color:red;">' . number_format($persenmati, 2, '.', ',') . '</b>';
                } else {
                    echo number_format($persenmati, 2, '.', ',');
                }
                ?></td>
            <td><?= DefaultCurrency(@$detail->jml_akhir) ?></td>
            <td><?= DefaultCurrency(@$detail->p_utuh_butir) ?></td>
            <td><?php
                $persenutuh = @$detail->ttl_butir == 0 ? 0 : (@$detail->p_utuh_butir / @$detail->ttl_butir * 100);
                echo number_format($persenutuh, 2, '.', ',')
                ?> </td>
            <td><?= DefaultCurrency(@$detail->p_putih_butir) ?></td>
            <td><?php
                $persenputih = @$detail->ttl_butir == 0 ? 0 : (@$detail->p_putih_butir / @$detail->ttl_butir * 100);
                if ($persenputih > 1) {
                    echo '<b style="color:red;">' . number_format($persenputih, 2, '.', ',') . '</b>';
                } else {
                    echo number_format($persenputih, 2, '.', ',');
                }
                ?></td>
            <td><?= DefaultCurrency(@$detail->p_retak_butir) ?></td>
            <td><?php
                $persenretak = @$detail->ttl_butir == 0 ? 0 : (@$detail->p_retak_butir / @$detail->ttl_butir * 100);
                if ($persenretak > 1.5) {
                    echo '<b style="color:red;">' . number_format($persenretak, 2, '.', ',') . '</b>';
                } else {
                    echo number_format($persenretak, 2, '.', ',');
                }
                ?> </td>
            <td>
                <?php
                $hdpersen = @$detail->jml_akhir == 0 ? 0 : (@$detail->ttl_butir / @$detail->jml_akhir * 100);
                if ($hdpersen > 1.5) {
                    echo '<b style="color:red;">' . number_format($hdpersen, 2, '.', ',') . '</b>';
                } else {
                    echo number_format($hdpersen, 2, '.', ',');
                }
                ?>
            </td>
            <td><?= DefaultCurrency(@$detail->ttl_butir) ?></td>
            <td><?= DefaultCurrency(@$detail->ttl_kg) ?></td>
            <td><?= DefaultCurrency(@$detail->gr_butir) ?></td>
            <td><?= DefaultCurrency(@$detail->kg_1000) ?></td>
            <td>
                <?php
                $grekor = @$detail->jml_akhir == 0 ? 0 : (@$detail->qtytotal / @$detail->jml_akhir * 100);
                echo number_format($grekor, 2, '.', ',')
                ?>
            </td>
            <td><?= DefaultCurrency(@$detail->qtytotal) ?></td>

            <td>
                <?php
                $fcr = @$detail->ttl_kg == 0 ? 0 : (@$detail->qtytotal / @$detail->ttl_kg * 100);
                echo number_format($fcr, 2, '.', ',')
                ?>
            </td>
        </tr>

        <?php
    }
} else {
    echo "<td style='text-align:center'  colspan=\"25\">No Data Found</td>";
}?>