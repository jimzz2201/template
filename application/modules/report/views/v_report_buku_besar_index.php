<section class="content-header">
    <h1>
        Buku Besar <?= @$button ?>
        <small>Akuntansi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Buku Besar</li>
        <li class="active">Akuntansi <?= @$button ?></li>
    </ol>
</section>

<style>
    th{
        text-align: center;
    }

    tbody td{
        padding:10px;
        text-align: center;
    }
</style>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                        <div class="col-sm-2">
                             <?= form_dropdown(array("selected" => @$groping, "name" => "grouping"), GetYaTidak(), @$grouping, array('class' => 'form-control', 'id' => 'dd_grouping')); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_cabang', 'placeholder' => 'Transaksi'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'value' => "", 'class' => 'form-control', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "obj", "Status")); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("selected" => @$id_parent_gl_account, "name" => "id_parent_gl_account"), DefaultEmptyDropdown(@$list_gl_account_parent, "json", "Parent Akun"), @$id_parent_gl_account, array('class' => 'form-control', 'id' => 'dd_id_parent_gl_account')); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_dropdown(array("selected" => @$id_gl_labarugi, "name" => "id_gl_labarugi"), DefaultEmptyDropdown(@$list_gl_labarugi, "json", "Laba Rugi"), @$id_gl_labarugi, array('class' => 'form-control', 'id' => 'dd_id_gl_labarugi')); ?>

                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("selected" => @$is_hpp, "name" => "is_hpp"), DefaultEmptyDropdown(@$list_is_hpp, "json", "HPP"), @$is_hpp, array('class' => 'form-control', 'id' => 'dd_is_hpp')); ?>

                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("selected" => @$id_gl_header, "name" => "id_gl_header"), DefaultEmptyDropdown(@$list_gl_header, "json", "Type Akun"), @$id_gl_header, array('class' => 'form-control', 'id' => 'dd_id_gl_header')); ?>

                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("selected" => @$id_gl, "name" => "id_gl_account"), DefaultEmptyDropdown(@$list_gl_account, "json", "GL"), @$id_gl_account, array('class' => 'form-control', 'id' => 'dd_id_gl_account')); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Print" type="button" class="btn btn-block btn-default pull-right">Print</button>
                        </div>
                    </div>
                </div>

            </form>

            <div class="portlet-body form" id="databody">

            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $("#btt_Print").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_akuntansi/printbukubesar?' + search;
        window.open(url);
    })
    $("#btt_Eksport").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_akuntansi/exportbukubesar?' + search;
        window.open(url);
    })
    
    function RefreshGl()
    {
        LoadBar.show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/gl/getdropdowngl',
            data: $("#frm_search").serialize(),
            dataType:"json",
            success: function (data) {
                $("#dd_id_gl_account").empty();
                $("#dd_id_gl_account").select2({data:data});
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });

    }
    
    
    
    function RefreshGrid()
    {
        LoadBar.show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/report/r_akuntansi/buku_besar_view',
            data: $("#frm_search").serialize(),
            success: function (data) {
                $("#databody").html(data);
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });

    }
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $("select").select2();
        $("#dd_id_parent_gl_account, #dd_id_gl_labarugi , #dd_is_hpp, #dd_id_gl_header").change(function(){
            RefreshGl();
        })

    });
</script>
