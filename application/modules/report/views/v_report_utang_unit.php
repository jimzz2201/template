<section class="content-header">
    <h1>
        Rekap Hutang Unit <?= @$button ?>
        <small>Report</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Report</li>
        <li class="active"> Rekap Hutang Unit <?= @$button ?></li>
    </ol>
</section>

<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification"></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_supplier', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("name" => "status"), DefaultEmptyDropdown(@$list_status, "json", "Status"), @$status, array('class' => 'form-control select2', 'id' => 'dd_status')); ?>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$cabang, array('class' => 'form-control select2', 'id' => 'dd_cabang')); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                      
                        <div class="col-sm-2">
                            <?= form_dropdown(array("name" => "id_kategori"), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$kategori, array('class' => 'form-control select2', 'id' => 'dd_id_kategori')); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                    </div>
                </div>


            </form>
            <hr/>
            <div class="portlet-body form">
                <table class="table table-striped table-bordered table-hover" id="mytable">

                </table>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {
        table.fnDraw(false);
        return false;
    })
    $("#btt_Eksport").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_utang/utang_unit_export?' + search;
        window.open(url);
    })

    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
       
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": "<?php echo base_url() ?>index.php/report/r_utang/getdatautangunit", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_kpu",
                    title: "No",
                    orderable: false
                },
                {data: "tanggal_kpu", orderable: false, title: "Tanggal",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "nama_supplier", orderable: true, title: "Customer"},
                {data: "no_kpu", orderable: false, title: "No KPU"},
                {data: "invoice_number", orderable: false, title: "Invoice"},
                {data: "nama_cabang", orderable: false, title: "Cabang"},
                {data: "nama_kategori", orderable: false, title: "Kategori"},
                {data: "nama_unit", orderable: false, title: "Unit"},

                {data: "harga_jual_unit", orderable: false, title: "Harga Jual", "className": "text-right",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                { orderable: false, title: "Pembayaran", "className": "text-right",
                    mRender: function (data, type, row) {
                        return Comma(row['harga_jual_unit']-row['sisa']);
                    }},
                {data: "sisa", orderable: false, title: "Sisa", "className": "text-right",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        if (data == 1)
                        {
                            return "Approved";
                        } else if (data == 2)
                        {
                            return "Rejected";
                        } else if (data == 3)
                        {
                            return "Closed";
                        } else if (data == 4)
                        {
                            return "Finished";
                        } else if (data == 5)
                        {
                            return "Batal";
                        } else
                        {
                            return "Pending";
                        }

                    }}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            }
        });

    });
</script>
