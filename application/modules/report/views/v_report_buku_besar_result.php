
<?php

$nama_gl_account = "";
$increment = 0;
$debit = 0;
$kredit = 0;

if (count($data) > 0) {
    foreach (@$data as $key=>$detsat) {
        echo "<h3>".$key."</h3>
            <table class='table table-striped table-bordered table-hover' id='mytable'><thead>
                <tr>
                    <th>Tanggal</th>
                    <th>No Dokumen</th>
                    <th>No Faktur</th>
                    <th>Keterangan</th>
                    <th>Jenis</th>
                    <th>Debit</th>
                    <th>Kredit</th>
                </tr>
            </thead><tbody>";
        foreach ($detsat as $detail) {
            echo "<tr>
                    <td>" . DefaultTanggal(@$detail->tanggal_buku) . "</td>
                    <td>" . @$detail->dokumen . "</td>
                    <td>" . @$detail->add_info . "</td>
                    <td class='text-left'>" . @$detail->keterangan . "</td>
                    <td>" . @$detail->nama_gl_account . "</td>
                    <td>" . DefaultCurrency(@$detail->debet) . "</td>
                    <td>" . DefaultCurrency(@$detail->kredit) . "</td>
            </tr>";
            $increment += @$detail->debet - @$detail->kredit;
            $debit += @$detail->debet;
            $kredit += @$detail->kredit;
        }
        echo "<tr>
                <td colspan='5' align='right'> Total</td>

                <td>" . DefaultCurrency($debit) . "</td>
                <td>" . DefaultCurrency($kredit) . "</td>
            </tr></tbody></table>";
        $debit = 0;
        $kredit = 0;
?>

        <?php

    }
} else {
    echo "
        <table class='table table-striped table-bordered table-hover' id='mytable'><thead>
        <tr>
            <th>Tanggal</th>
            <th>No Dokumen</th>
            <th>No Faktur</th>
            <th>Keterangan</th>
            <th>Jenis</th>
            <th>Debit</th>
            <th>Kredit</th>
        </tr>
    </thead><tbody><td style='text-align:center'  colspan=\"7\">No Data Found</td><tbody></table>";
}
?>
