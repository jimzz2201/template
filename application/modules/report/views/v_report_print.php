<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.printelement.js"></script>

<button type="button" onclick="$('#preview-bps').printElement();" class="btn btn-primary" id="btt_modal_ok">Print</button>
<div id="preview-bps">
    <h3 style="text-align: center;"><?php echo @$title ?></h3>
    <h4 style="text-align: center;"><?php echo @$subheader ?></h4>
    <style>
        table.table-bordered th:last-child, table.table-bordered td:last-child{
            border-right-width: 1px;
        }

        td{
            padding-right:5px;
            padding-left:5px;
            border-collapse: collapse;
        }
        th{
            border:none;
            border-top:solid 1px #000;
            border-bottom:solid 1px #000;
            text-align: center;
        }
        table{
            width:100%;font-family:Arial, Helvetica, sans-serif; font-size:10px;page-break-after:always;border:none;
            border-collapse: collapse;
        }
        .tdnumber{
            text-align:right;
        }
    </style>
    <pre>
    Tanggal : <?php echo date('d-m-Y H:i:s') ?>

    Pembuat : <?php echo GetUsername() ?>

    Keyword : <?php echo @$keyword ?>
    </pre>
    <?php echo @$html ?>
</div>