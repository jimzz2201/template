
<table>
    <thead>
        <tr>
            <th>Kode</th>
            <th>Nama</th>
            <th>Saldo Awal</th>
            <th>Piutang</th>
            <th>Total Due</th>
            <th>< = 0</th>
            <th>1 - 30</th>
            <th>31 - 60</th>
            <th>61 - 90</th>
            <th>> = 90</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no = 0;
        $saldo_awal = 0;
        $total_due = 0;
        $utang=0;
        $due0 = 0;
        $due30 = 0;
        $due60 = 0;
        $due90 = 0;
        $due90plus = 0;
        foreach ($listdetail as $detail) {
            $no++;
            $utang += $detail->hutang;
            $saldo_awal += $detail->saldo_awal;
            $total_due += $detail->total_due;
            $due0 += $detail->due0;
            $due30 += $detail->due30;
            $due60 += $detail->due60;
            $due90 += $detail->due90;
            $due90plus += $detail->due90plus;
            ?>
            <tr>
                <td><?php echo $no ?></td>
                <td><?php echo $detail->nama_supplier ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->saldo_awal) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->hutang) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->total_due) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->due0) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->due30) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->due60) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->due90) ?></td>
                <td class="tdnumber"><?php echo DefaultCurrency($detail->due90plus) ?></td>
            </tr>
        <?php } ?>
        <tr style="border-top:solid 1px #000;">
            <td colspan="2" class="tdnumber">Total</td>
            <td class="tdnumber"><?php echo DefaultCurrency($saldo_awal) ?></td>
            <td class="tdnumber"><?php echo DefaultCurrency($utang) ?></td>
            <td class="tdnumber"><?php echo DefaultCurrency($total_due) ?></td>
            <td class="tdnumber"><?php echo DefaultCurrency($due0) ?></td>
            <td class="tdnumber"><?php echo DefaultCurrency($due30) ?></td>
            <td class="tdnumber"><?php echo DefaultCurrency($due60) ?></td>
            <td class="tdnumber"><?php echo DefaultCurrency($due90) ?></td>
            <td class="tdnumber"><?php echo DefaultCurrency($due90plus) ?></td>
            
        </tr>
    </tbody>
</table>


