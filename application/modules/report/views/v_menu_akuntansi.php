<?php    
    if(CekModule("K065",false))
    {
        echo anchor(site_url('report/r_akuntansi'), '<i class="fa fa-file"></i> Buku Kas ', 'class="btn btn-success"');
    }
    if(CekModule("K066",false))
    {
        echo anchor(site_url('report/r_akuntansi/omzet_customer'), '<i class="fa fa-file"></i> Omzet Customer ', 'class="btn btn-info"');
    }
    if(CekModule("K067",false))
    {
        echo anchor(site_url('report/r_akuntansi/biaya'), '<i class="fa fa-file"></i> Biaya ', 'class="btn btn-danger"');
    }
    if(CekModule("K068",false))
    {
        echo anchor(site_url('report/r_akuntansi/biaya_bank'), '<i class="fa fa-file"></i> Biaya Bank ', 'class="btn btn-warning"');
    }