
<section class="content-header">
    <h1>
        Rekap Hutang
        <small>Laporan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Laporan</li>
        <li class="active">Rekap Hutang</li>
    </ol>


</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Tanggal', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker(date('Y-m-d')), array('required' => 'required')); ?>
                        </div>
                        <?= form_label('s / d', "txt_tanggal_akhir", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker(date('Y-m-d'))); ?>
                        </div>
                       
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_supplier', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier")); ?>
                        </div>


                    </div>
                    <div class="form-group">
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Eksport" type="button" class="btn btn-block btn-warning pull-right">Eksport</button>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Print" type="button" class="btn btn-block btn-default pull-right">Print</button>
                        </div>
                    </div>
                </div>



            </form>
        </div>
        <div class="box-body">
            <div id="notification" ></div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    $("form#frm_search").submit(function () {

        table.fnDraw(false);
        return false;
    })
    $("#btt_Print").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_utang/printdetailutang?' + search;
        window.open(url);
    })
    $("#btt_Eksport").click(function () {
        var search = $("form#frm_search").serialize();
        var url = baseurl + 'index.php/report/r_utang/exportdetailutang?' + search;
        window.open(url);
    })

    $(document).ready(function () {
        $("select").select2();
      
        $(".datepicker").datepicker();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: false,
            ajax: {"url": baseurl + "index.php/report/r_utang/getdatadetailutang", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_pembelian",
                    title: "Kode",
                    orderable: false,
                    width: "50px"
                }
                , {data: "nama_supplier", orderable: false, title: "Nama", width: "200px"}
                , {data: "tanggal", orderable: false, title: "Tanggal", width: "120px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data == undefined ? 0 : data);
                    }}
                , {data: "nomor_invoice", orderable: false, title: "Invoice", width: "80px"}
                , {data: "jth_tempo", orderable: false, title: "Jatuh&nbsp;Tempo", width: "120px",
                    mRender: function (data, type, row) {
                        return DefaultDateFormat(data == undefined ? 0 : data);
                    }}
                , {data: "lama_hari", orderable: false, class: 'text-right', title: "Lama", width: "30px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "grandtotal", orderable: false, class: 'text-right', title: "GrandTotal", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
                , {data: "sisa", orderable: false, class: 'text-right', title: "Sisa", width: "120px",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);

            },
            initComplete: function () {

            }
        });

    });
</script>
