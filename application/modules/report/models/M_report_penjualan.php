<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_penjualan extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function GetDataReportpenjualan($params, $mode="datatables") {
        $this->load->library('datatables');
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
        $DB->select('#_prospek.*,#_prospek_detail.harga_jual_unit,#_prospek.id_prospek as id,#_prospek_detail.jumlah_unit,supervisor.nama_pegawai as nama_supervisor,#_prospek_detail.tahun,#_prospek_detail.biaya_bbn,#_prospek_detail.total_equipment,nama_kategori,nama_type_unit,nama_warna,nama_payment_method,#_pegawai.nama_pegawai,nama_customer,nama_jenis_plat,nama_type_body,nama_unit');
        $DB->from("#_prospek");
        $DB->where('#_prospek.deleted_date', null);
        $DB->join('#_prospek_detail', '#_prospek_detail.id_prospek = #_prospek.id_prospek');
        //add this line for join
        $DB->join('#_buka_jual', '#_buka_jual.id_prospek = #_prospek.id_prospek',"left");
        $DB->join('#_customer', '#_prospek.id_customer = #_customer.id_customer');
        $DB->join('#_jenis_plat', '#_prospek_detail.id_jenis_plat = #_jenis_plat.id_jenis_plat');
        $DB->join('#_unit', '#_prospek_detail.id_unit = #_unit.id_unit');
        $DB->join('#_type_body', '#_unit.id_type_body = #_type_body.id_type_body');
        $DB->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori');
        $DB->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna');
        $DB->join('#_payment_method', '#_prospek.id_payment_method = #_payment_method.id_payment_method','left');
        $DB->join('#_pegawai', '#_pegawai.id_pegawai = #_prospek.id_pegawai', 'left');
		$DB->join('#_pegawai supervisor', 'supervisor.id_pegawai = #_pegawai.id_pegawai_parent', 'left');
        $DB->group_by("#_prospek.id_prospek");
        $isedit = true;
        $isdelete = true;
        $where = [];
        $extra = [];
        $strview = '';
        $strbatal = '';
        $strapproved = '';
        $strrejected = '';
        $stredit = '';
        $strclosed = '';
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_prospek.no_prospek'] = $params['keyword'];
        } else {
            $tanggal="date(#_prospek.tanggal_prospek)";
            if($params['jenis_pencarian']=="created_date")
            {
                $tanggal="date(#_prospek.created_date)";
            }
            else if($params['jenis_pencarian']=="updated_date")
            {
                 $tanggal="date(#_prospek.updated_date)";
            }
			 else if($params['jenis_pencarian']=="tanggal_buka_jual")
            {
                 $tanggal="#_buka_jual.tanggal_buka_jual";
            }
            $this->db->order_by($tanggal,"desc");
            
            
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal." BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                if($params['status']<4)
                {
                     $where['#_prospek.status'] = $params['status'];
                }
                else if($params['status']=="4")
                {
                    $where['#_prospek.close_status'] = 1;
                }
                else if($params['status']==5)
                {
                    $where['#_prospek.status'] = 5;
                }
                else if($params['status']==6)
                {
                     $where['#_prospek.status'] = 0;
                }
                else if($params['status']==7)
                {
                     $where['#_prospek.status'] = 1;
                     $where['#_buka_jual.id_buka_jual !='] = null;
                }
                else if($params['status']==8)
                {
                    $where['#_prospek.status'] = 1;
                    $where['#_buka_jual.id_buka_jual'] = null;
                }
               
            }
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_prospek.id_cabang'] = $params['id_cabang'] ;
            }
            else
            {
                $DB->where_in('#_prospek.id_cabang', GetCabangAkses());
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_prospek.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        return $result;
    }
    
    function GetDataReportpenjualanDetail($params,$mode="datatables") {
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
       
        $DB->select('#_prospek.*,#_prospek_detail.tahun,parent.nama_pegawai as supervisor,#_unit_serial.vin_number,#_do_prospek.tanggal_do,nomor_do_prospek,#_unit_serial.engine_no,#_customer.no_telp,tanggal_buka_jual,nomor_buka_jual,nama_customer as customer_name,#_customer.alamat,nama_cabang,#_prospek_detail.harga_jual_unit,#_prospek_detail.harga_off_the_road,#_prospek_detail.biaya_bbn,#_prospek_detail.total_equipment,#_prospek_detail.harga_on_the_road,#_prospek.id_prospek as id,#_prospek_detail.tahun,nama_kategori,nama_type_unit,nama_warna,nama_payment_method,#_pegawai.nama_pegawai,nama_customer,nama_jenis_plat,nama_type_body,nama_unit');
        $DB->from("#_prospek");
        $DB->where('#_prospek.deleted_date', null);
        $DB->join('#_prospek_detail', '#_prospek_detail.id_prospek = #_prospek.id_prospek');
        //add this line for join
        $DB->join('#_customer', '#_prospek.id_customer = #_customer.id_customer');
        $DB->join('#_prospek_detail_unit', '#_prospek_detail.id_prospek_detail = #_prospek_detail_unit.id_prospek_detail');
        $DB->join('#_unit_serial', '#_unit_serial.id_unit_serial = #_prospek_detail_unit.id_unit_serial',"left");
        $DB->join('#_unit', '#_prospek_detail.id_unit = #_unit.id_unit');
        $DB->join('#_buka_jual', '#_unit_serial.id_buka_jual = #_buka_jual.id_buka_jual',"left");
        $DB->join('#_jenis_plat', '#_prospek_detail.id_jenis_plat = #_jenis_plat.id_jenis_plat');
        $DB->join('#_type_body', '#_unit.id_type_body = #_type_body.id_type_body');
        $DB->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori');
        $DB->join('#_cabang', '#_prospek.id_cabang = #_cabang.id_cabang');
        $DB->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna');
        $DB->join('#_payment_method', '#_prospek.id_payment_method = #_payment_method.id_payment_method','left');
        $DB->join('#_do_prospek_detail', '#_do_prospek_detail.id_do_prospek_detail = #_unit_serial.id_do_prospek_detail','left');
        $DB->join('#_do_prospek', '#_do_prospek.id_do_prospek = #_do_prospek_detail.id_do_prospek','left');
        
        $DB->join('#_pegawai', '#_pegawai.id_pegawai = #_prospek.id_pegawai', 'left');
        $DB->join('#_pegawai parent', 'parent.id_pegawai = #_pegawai.id_pegawai_parent', 'left');
        $where = [];
        $extra = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_prospek.no_prospek'] = $params['keyword'];
        } else {
            $tanggal="date(#_prospek.tanggal_prospek)";
            if($params['jenis_pencarian']=="created_date")
            {
                $tanggal="date(#_prospek.created_date)";
            }
            else if($params['jenis_pencarian']=="updated_date")
            {
                 $tanggal="date(#_prospek.updated_date)";
            }
            $this->db->order_by($tanggal,"desc");
            
            
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal." BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                if($params['status']<4)
                {
                     $where['#_prospek.status'] = $params['status'];
                }
                else if($params['status']=="4")
                {
                    $where['#_prospek.close_status'] = 1;
                }
                else if($params['status']==5)
                {
                    $where['#_prospek.status'] = 5;
                }
                else if($params['status']==6)
                {
                     $where['#_prospek.status'] = 0;
                }
                else if($params['status']==7)
                {
                     $where['#_prospek.status'] = 1;
                     $where['#_buka_jual.id_buka_jual !='] = null;
                }
                else if($params['status']==8)
                {
                    $where['#_prospek.status'] = 1;
                    $where['#_buka_jual.id_buka_jual'] = null;
                }
               
            }
            if (!CheckEmpty($params['id_kategori'])) {
                $where['#_kategori.id_kategori'] = $params['id_kategori'] ;
            }
            if (!CheckEmpty($params['id_pegawai'])) {
                $where['#_prospek.id_pegawai'] = $params['id_pegawai'] ;
            }
             if (!CheckEmpty($params['id_pegawai_parent'])) {
                $where['#_pegawai.id_pegawai_parent'] = $params['id_pegawai_parent'] ;
            }
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_prospek.id_cabang'] = $params['id_cabang'] ;
            }
            else
            {
                $DB->where_in('#_prospek.id_cabang', GetCabangAkses());
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_prospek.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
         $this->db->order_by("tanggal_prospek");
        $result=null;
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        return $result;
    }

}
