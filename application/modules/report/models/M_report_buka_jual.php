<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_buka_jual extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function GetDataReportBukaJual($params, $mode="datatables") {
        $this->load->library('datatables');
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
        $DB->select('#_buka_jual.*,nama_cabang,#_prospek_detail.harga_jual_unit,#_prospek.no_prospek,#_buka_jual.id_buka_jual as id,count(#_unit_serial.id_unit_serial) as jumlah_unit, #_prospek_detail.tahun,#_prospek_detail.biaya_bbn ,#_prospek_detail.total_equipment,nama_kategori,nama_type_unit,nama_warna,nama_payment_method,nama_pegawai,nama_customer,nama_jenis_plat,nama_type_body,nama_unit');
        $DB->from("#_buka_jual");
        $DB->join('#_unit_serial', '#_unit_serial.id_buka_jual = #_buka_jual.id_buka_jual',"left");
        $DB->join('#_prospek_detail_unit', '#_prospek_detail_unit.id_unit_serial = #_unit_serial.id_unit_serial',"left");
        $DB->join('#_prospek_detail', '#_prospek_detail.id_prospek_detail = #_prospek_detail_unit.id_prospek_detail',"left");
        $DB->where('#_buka_jual.deleted_date', null);
        $DB->join('#_prospek', '#_prospek_detail.id_prospek = #_prospek.id_prospek', 'left');
        //add this line for join
        $DB->join('#_customer', '#_prospek.id_customer = #_customer.id_customer', 'left');
           $DB->join('#_cabang', '#_buka_jual.id_cabang = #_cabang.id_cabang', 'left');
        $DB->join('#_jenis_plat', '#_prospek_detail.id_jenis_plat = #_jenis_plat.id_jenis_plat', 'left');
        $DB->join('#_unit', '#_prospek_detail.id_unit = #_unit.id_unit', 'left');
        $DB->join('#_type_body', '#_unit.id_type_body = #_type_body.id_type_body', 'left');
        $DB->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit', 'left');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori', 'left');
        $DB->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna', 'left');
        $DB->join('#_payment_method', '#_prospek.id_payment_method = #_payment_method.id_payment_method','left');
        $DB->join('#_pegawai', '#_pegawai.id_pegawai = #_prospek.id_pegawai', 'left');
        $where = [];
        $extra = [];
     
         if (!CheckEmpty(@$params['keyword'])) {
            $where['#_buka_jual.nomor_buka_jual'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_buka_jual.tanggal_buka_jual BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                
                
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_buka_jual.tanggal_buka_jual"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_buka_jual.tanggal_buka_jual"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                if($params['status']=="6")
                {
                    $this->db->group_start();
                    $this->datatables->where("#_buka_jual.status",0);
                    $this->datatables->or_where("#_buka_jual.status",7);
                    $this->db->group_end();
                }
                else
                {
                   $where['#_buka_jual.status'] =  $params['status']; 
                }
                
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_buka_jual.id_customer'] = $params['id_customer'];
            }
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_buka_jual.id_cabang'] = $params['id_cabang'];
            }
        }
        $DB->group_by('#_buka_jual.id_buka_jual');
        $DB->group_by('#_buka_jual.id_prospek');
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        return $result;
    }
    
    function GetDataReportpenjualanDetail($params,$mode="datatables") {
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
       
        $DB->select('#_prospek.*,#_prospek_detail.tahun,#_unit_serial.vin_number,#_do_prospek.tanggal_do,nomor_do_prospek,#_unit_serial.engine_no,#_customer.no_telp,tanggal_buka_jual,nomor_buka_jual,nama_customer as customer_name,#_customer.alamat,nama_cabang,#_prospek_detail.harga_jual_unit,#_prospek_detail.harga_off_the_road,#_prospek_detail.biaya_bbn,#_prospek_detail.total_equipment,#_prospek_detail.harga_on_the_road,#_prospek.id_prospek as id,#_prospek_detail.tahun,nama_kategori,nama_type_unit,nama_warna,nama_payment_method,nama_pegawai,nama_customer,nama_jenis_plat,nama_type_body,nama_unit');
        $DB->from("#_prospek");
        $DB->where('#_prospek.deleted_date', null);
        $DB->join('#_prospek_detail', '#_prospek_detail.id_prospek = #_prospek.id_prospek');
        //add this line for join
        $DB->join('#_customer', '#_prospek.id_customer = #_customer.id_customer');
        $DB->join('#_prospek_detail_unit', '#_prospek_detail.id_prospek_detail = #_prospek_detail_unit.id_prospek_detail');
        $DB->join('#_unit_serial', '#_unit_serial.id_unit_serial = #_prospek_detail_unit.id_unit_serial',"left");
        $DB->join('#_unit', '#_prospek_detail.id_unit = #_unit.id_unit');
        $DB->join('#_buka_jual', '#_unit_serial.id_buka_jual = #_buka_jual.id_buka_jual',"left");
        $DB->join('#_jenis_plat', '#_prospek_detail.id_jenis_plat = #_jenis_plat.id_jenis_plat');
        $DB->join('#_type_body', '#_unit.id_type_body = #_type_body.id_type_body');
        $DB->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori');
        $DB->join('#_cabang', '#_prospek.id_cabang = #_cabang.id_cabang');
        $DB->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna');
        $DB->join('#_payment_method', '#_prospek.id_payment_method = #_payment_method.id_payment_method','left');
        $DB->join('#_do_prospek_detail', '#_do_prospek_detail.id_do_prospek_detail = #_unit_serial.id_do_prospek_detail','left');
        $DB->join('#_do_prospek', '#_do_prospek.id_do_prospek = #_do_prospek_detail.id_do_prospek','left');
        
        $DB->join('#_pegawai', '#_pegawai.id_pegawai = #_prospek.id_pegawai', 'left');
        $where = [];
        $extra = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_prospek.no_prospek'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_prospek.tanggal_prospek BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_prospek.tanggal_prospek"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_prospek.tanggal_prospek"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                $where['#_prospek.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_kategori'])) {
                $where['#_kategori.id_kategori'] = $params['id_kategori'] ;
            }
            if (!CheckEmpty($params['id_pegawai'])) {
                $where['#_prospek.id_pegawai'] = $params['id_pegawai'] ;
            }
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_prospek.id_cabang'] = $params['id_cabang'] ;
            }
            else
            {
                $DB->where_in('#_prospek.id_cabang', GetCabangAkses());
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_prospek.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
         $this->db->order_by("tanggal_prospek");
        $result=null;
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        return $result;
    }

}
