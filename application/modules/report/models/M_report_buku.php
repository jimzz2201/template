<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_buku extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function GetSaldoAwal($tanggal, $id_gl_account,$id_subject, $id_cabang) {
        $this->db->from("#_buku_besar");
        $this->db->where(array("id_gl_account" => $id_gl_account));

        if (!CheckEmpty($id_cabang)) {
            $this->db->where(array("id_cabang" => $id_cabang));
        }
        $this->db->where(array("id_subject" => $id_subject));
        $this->db->where(array("tanggal_buku <" => DefaultTanggalDatabase($tanggal)));
        $this->db->select("ifnull(sum(debet-kredit),0)as jml");
        $row = $this->db->get()->row();
        $this->load->model("gl/m_gl");
        $saldo_awal = $this->m_gl->GetSaldoAwalGl($id_gl_account, $id_cabang);


        if ($saldo_awal) {
            if ($row) {
                $row->jml = $row->jml + $saldo_awal;
            }
        }
        return $row;
    }

    function GetDataJurnal($params) {

        $this->db->select('#_buku_besar.*,nama_gl_account');
        $this->db->from("#_buku_besar");
        $extra = [];
        $id_gl_account = 0;
        $this->db->join("#_gl_account", "#_gl_account.id_gl_account=#_buku_besar.id_gl_account");
        $id_gl_account = @$params['id_gl_account'];
        $this->db->where(array("#_buku_besar.id_gl_account" => @$params['id_gl_account']));
        unset($params['id_gl_account']);

        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_buku_besar.tanggal_buku BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal_buku'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }

        if (isset($params['start_date'])) {
            $params['tanggal_buku'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
        
        
        if (isset($params['status'])) {
            if(!CheckEmpty($params['status']))
            {
                if($params['status']=="1")
                {
                    $this->db->group_start();
                    $this->db->where("debet !=",0);
                    $this->db->or_where("kredit !=",0);
                    $this->db->group_end();
                }
                else
                {
                     $this->db->where(array("debet"=>0,"kredit"=>0));
                }
            }
            unset($params['status']);
        }
        if (CheckEmpty(@$params['id_cabang'])) {
            unset($params['id_cabang']);
        }

        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $this->db->order_by("tanggal_buku,created_date");
        $data = $this->db->get()->result();
        $saldoawal = 0;

        if (count($data) > 0) {
            $saldoawal = $this->GetSaldoAwal($data[0]->tanggal_buku, $id_gl_account,@$params['id_subject'], @$params['id_cabang'])->jml;
        } else {
            $saldoawal = $this->GetSaldoAwal(GetDateNow(), $id_gl_account,@$params['id_subject'], @$params['id_cabang'])->jml;
        }
        return array("data" => $data, "saldo_awal" => $saldoawal);
    }

}
