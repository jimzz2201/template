<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_akuntansi extends CI_Model {

    public $table = 'uap_pembelian';
    public $id = 'id_pembelian_po_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDataBukuBesar($params) {

        $this->db->select('#_buku_besar.*,nama_gl_account');
        $this->db->from("#_buku_besar");
        $extra = [];
        $where=array();
        $id_gl_account = 0;
        $this->db->join("#_gl_account", "#_gl_account.id_gl_account=#_buku_besar.id_gl_account");
        $this->db->where_in("#_buku_besar.id_gl_account", $params['listgl']);
        unset($params['listgl']);

        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_buku_besar.tanggal_buku BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $where['tanggal_buku'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }

        if (isset($params['start_date'])) {
            $where['tanggal_buku'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
        if (isset($params['status'])) {
            if (!CheckEmpty($params['status'])) {
                if ($params['status'] == "1") {
                    $this->db->group_start();
                    $this->db->where("debet !=", 0);
                    $this->db->or_where("kredit !=", 0);
                    $this->db->group_end();
                } else {
                    $this->db->where(array("debet" => 0, "kredit" => 0));
                }
            }
            unset($params['status']);
        }
        if (!CheckEmpty(@$params['id_cabang'])) {
            $where['id_cabang']=@$params['id_cabang'];
            unset($params['id_cabang']);
        }

        if (count($where)) {
            $this->db->where($where);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        if(!CheckEmpty($params['grouping']))
        {
             $this->db->order_by("nama_gl_account,tanggal_buku,created_date");
        }
        else
        {
             $this->db->order_by("tanggal_buku,created_date");
        }
       
        $data = $this->db->get()->result();
    
        return $data;
    }

}
