<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_penjualan extends CI_Model {

    public $table = 'uap_sales_order';
    public $id = 'id_sales_order_po_master';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDetailPo($id) {
        $this->db->from("uap_sales_order_po_detail");
        $this->db->join("uap_satuan", "uap_sales_order_po_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_sales_order_po_detail.id_barang", "left");
        $this->db->select("uap_sales_order_po_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_sales_order_po_master" => $id, "uap_sales_order_po_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
    function GetDetail($id) {
        $this->db->from("uap_sales_order_detail");
        $this->db->join("uap_satuan", "uap_sales_order_detail.id_satuan=uap_satuan.id_satuan", "left");
        $this->db->join("uap_barang", "uap_barang.id_barang=uap_sales_order_detail.id_barang", "left");
        $this->db->select("uap_sales_order_detail.*,sku_barang as sku,nama_satuan,harga_beli_akhir");
        $this->db->where(array("id_sales_order_master" => $id, "uap_sales_order_detail.status !=" => 5));
        $listdetail = $this->db->get()->result();
        return $listdetail;
    }
    function GetOnePembelianPo($id, $isfull = false) {
        $this->db->from("uap_sales_order_po_master");
        $this->db->where(array("id_sales_order_po_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetailPo($id);
        }

        return $row;
    }
    function GetOnePembelian($id, $isfull = false) {
        $this->db->from("uap_sales_order_master");
        $this->db->where(array("id_sales_order_master" => $id));
        $row = $this->db->get()->row();
        if ($isfull) {

            $row->listdetail = $this->GetDetail($id);
            $this->db->from("uap_sales_order_link");
            $this->db->join("uap_sales_order_po_master","uap_sales_order_po_master.id_sales_order_po_master=uap_sales_order_link.id_sales_order_po_master");
            $this->db->where(array("uap_sales_order_link.id_sales_order_master"=>$id));
            $this->db->select("uap_sales_order_po_master.id_sales_order_po_master,nomor_po_master,tgl_po");
            $row->listheader=$this->db->get()->result();
        }

        return $row;
    }
    function PembelianPOManipulate($model) {
        try {
            $message = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $sales_orderpo = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("barang/m_barang");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
            $sales_orderpo['kepada'] = $model['kepada'];
            $sales_orderpo['no_fax'] = $model['no_fax'];
            $sales_orderpo['pembuat'] = $model['pembuat'];

            $sales_orderpo['keterangan'] = $model['keterangan'];
            $sales_orderpo['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $sales_orderpo['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $sales_orderpo['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $sales_orderpo['tgl_po'] = DefaultTanggalDatabase($model['tgl_po']);
            $sales_orderpo['nominal_po'] = 0;
            $id_sales_order = 0;
            if (CheckEmpty(@$model['id_sales_order_po_master'])) {
                $sales_orderpo['created_date'] = GetDateNow();
                $sales_orderpo['nomor_po_master'] = AutoIncrement('uap_sales_order_po_master', $cabang->kode_cabang . '/PO/' . date("y") . '/', 'nomor_po_master', 5);
                $sales_orderpo['created_by'] = ForeignKeyFromDb($userid);
                $sales_orderpo['status'] = '1';
                $this->db->insert("uap_sales_order_po_master", $sales_orderpo);
                $id_sales_order = $this->db->insert_id();
            } else {
                $sales_orderpo['updated_date'] = GetDateNow();
                $sales_orderpo['updated_by'] = ForeignKeyFromDb($userid);
                $id_sales_order = $model['id_sales_order_po_master'];
                $sales_orderdb = $this->GetOnePembelianPo($id_sales_order);
                $sales_orderpo['nomor_po_master'] = $sales_orderdb->nomor_po_master;
                $this->db->update("uap_sales_order_po_master", $sales_orderpo, array("id_sales_order_po_master" => $id_sales_order));
            }
            $arraydetailsales_order = array();
            foreach ($model['dataset'] as $key => $det) {
                $detailsales_order = array();
                $detailsales_order['id_barang'] = $det['id_barang'];
                $detailsales_order['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailsales_order['nama_barang'] = $det['nama_barang'];
                $detailsales_order['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailsales_order['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailsales_order['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailsales_order['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);

                if (CheckEmpty($det['id_sales_order_po_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailsales_order['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailsales_order['created_date'] = GetDateNow();
                    $detailsales_order['created_by'] = $userid;
                    $detailsales_order['status'] = 1;
                    $detailsales_order['id_sales_order_po_master'] = $id_sales_order;
                    $this->db->insert("uap_sales_order_po_detail", $detailsales_order);
                    $arraydetailsales_order[] = $this->db->insert_id();
                } else {

                    $detailsales_order['updated_date'] = GetDateNow();
                    $detailsales_order['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailsales_order['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("uap_sales_order_po_detail", $detailsales_order, array("id_sales_order_po_detail" => $det['id_sales_order_po_detail']));
                    $arraydetailsales_order[] = $det['id_sales_order_po_detail'];
                }
            }


            if (count($arraydetailsales_order) > 0) {
                $this->db->where_not_in("id_sales_order_po_detail", $arraydetailsales_order);
                $this->db->where(array("id_sales_order_po_master" => $id_sales_order));
                $this->db->update("uap_sales_order_po_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("uap_sales_order_po_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_sales_order_po_master" => $sales_orderpo));
            }




            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_sales_order_po_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_sales_order, $sales_orderpo['nomor_po_master'], 'sales_orderpo');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function PembelianManipulate($model) {
        try {
            
            
            $message = "";
            $listheader=CheckArray($model, 'listheader');
            $listposales_order=array();
            foreach($listheader as $headersatuan)
            {
                array_push($listposales_order,array("id_sales_order_po_master"=>$headersatuan['id_sales_order_po_master']) );
            }
            
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $userid = GetUserId();
            $sales_order = array();
            $this->load->model("cabang/m_cabang");
            $this->load->model("supplier/m_supplier");
            $this->load->model("barang/m_barang");
            $cabang = $this->m_cabang->GetOneCabang($model['id_cabang']);
           
            $sales_order['keterangan'] = $model['keterangan'];
            $sales_order['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $sales_order['id_gudang'] = ForeignKeyFromDb($model['id_gudang']);
            $sales_order['id_cabang'] = ForeignKeyFromDb($model['id_cabang']);
            $sales_order['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $sales_order['jth_tempo'] = DefaultTanggalDatabase($model['jth_tempo']);
            $sales_order['type_ppn'] = $model['type_ppn'];
            $sales_order['type_pembayaran'] = $model['type_pembayaran'];
            $sales_order['type_pembulatan'] = $model['type_pembulatan'];
            $sales_order['disc_pembulatan'] = DefaultCurrencyDatabase($model['disc_pembulatan']);
            $sales_order['nominal_ppn'] = DefaultCurrencyDatabase($model['nominal_ppn']);
            $sales_order['grandtotal'] = DefaultCurrencyDatabase($model['grandtotal']);
            $sales_order['jumlah_bayar'] = DefaultCurrencyDatabase($model['jumlah_bayar'])>$sales_order['grandtotal']?$sales_order['grandtotal']:DefaultCurrencyDatabase($model['jumlah_bayar']);
            $sales_order['total'] = DefaultCurrencyDatabase($model['total']);
            $sales_order['ppn'] = DefaultCurrencyDatabase($model['ppn']);
            $supplier=$this->m_supplier->GetOneSupplier($sales_order['id_supplier']);
            if (CheckEmpty(@$model['id_sales_order_master'])) {
                $sales_order['created_date'] = GetDateNow();
                $sales_order['nomor_master'] = AutoIncrement('uap_sales_order_master', $cabang->kode_cabang . '/PR/' . date("y") . '/', 'nomor_master', 5);
                $sales_order['created_by'] = ForeignKeyFromDb($userid);
                $sales_order['status'] = '1';
                $this->db->insert("uap_sales_order_master", $sales_order);
                $id_sales_order = $this->db->insert_id();
            } else {
                $sales_order['updated_date'] = GetDateNow();
                $sales_order['updated_by'] = ForeignKeyFromDb($userid);
                $id_sales_order = $model['id_sales_order_master'];
                $sales_orderdb = $this->GetOnesales_order($id_sales_order);
                $sales_order['nomor_master'] = $sales_orderdb->nomor_master;
                $this->db->update("uap_sales_order_master", $sales_order, array("id_sales_order_master" => $id_sales_order));
            }
            
            $this->db->delete("uap_sales_order_link",array("id_sales_order_master"=>$id_sales_order));
            foreach($listposales_order as $linksatuan)
            {
                $linksatuan['id_sales_order_master']=$id_sales_order;
                $this->db->insert("uap_sales_order_link",$linksatuan);
            }
            $arraydetailsales_order = array();
            $totalsales_orderwithoutppn=0;
            $totalsales_orderwithppn=0;
            foreach ($model['dataset'] as $key => $det) {
                $detailsales_order = array();
                $detailsales_order['id_barang'] = $det['id_barang'];
                $detailsales_order['price_barang'] = DefaultCurrencyDatabase($det['price_barang']);
                $detailsales_order['nama_barang'] = $det['nama_barang'];
                $detailsales_order['qty'] = DefaultCurrencyDatabase($det['qty']);
                $detailsales_order['disc_1'] = DefaultCurrencyDatabase($det['disc_1']);
                $detailsales_order['disc_2'] = DefaultCurrencyDatabase($det['disc_2']);
                $detailsales_order['subtotal'] = DefaultCurrencyDatabase($det['subtotal']);
                if($sales_order['type_ppn']=="1")
                {
                    $detailsales_order['ppn_satuan'] = $detailsales_order['subtotal']*((float)$sales_order['ppn']/(float)$sales_order['ppn']+(float)100);
                    $totalsales_orderwithoutppn+=$detailsales_order['subtotal']-$detailsales_order['ppn_satuan']; 
                }
                else  if($sales_order['type_ppn']=="2")
                {
                    $detailsales_order['ppn_satuan'] = $detailsales_order['subtotal']*((float)$sales_order['ppn']/(float)100);
                    $totalsales_orderwithoutppn+=$detailsales_order['subtotal']; 
                }
                else
                {
                    $detailsales_order['ppn_satuan'] = 0;
                    $totalsales_orderwithoutppn+=$detailsales_order['subtotal']; 
                }
                
                $totalsales_orderwithppn+=$detailsales_order['subtotal'];
                if (CheckEmpty(@$det['id_sales_order_detail'])) {
                    $barang = $this->m_barang->GetOneBarang($det['id_barang']);
                    $detailsales_order['id_satuan'] = ForeignKeyFromDb(@$barang->id_satuan);
                    $detailsales_order['created_date'] = GetDateNow();
                    $detailsales_order['created_by'] = $userid;
                    $detailsales_order['status'] = 1;
                    $detailsales_order['id_sales_order_master'] = $id_sales_order;
                    $this->db->insert("uap_sales_order_detail", $detailsales_order);
                    $arraydetailsales_order[] = $this->db->insert_id();
                } else {

                    $detailsales_order['updated_date'] = GetDateNow();
                    $detailsales_order['updated_by'] = $userid;
                    if (!CheckEmpty(@$det['id_satuan']))
                        $detailsales_order['id_satuan'] = ForeignKeyFromDb($det['id_satuan']);
                    $this->db->update("uap_sales_order_detail", $detailsales_order, array("id_sales_order_detail" => $det['id_sales_order_detail']));
                    $arraydetailsales_order[] = $det['id_sales_order_detail'];
                }
            }


            if (count($arraydetailsales_order) > 0) {
                $this->db->where_not_in("id_sales_order_detail", $arraydetailsales_order);
                $this->db->where(array("id_sales_order_master" => $id_sales_order));
                $this->db->update("uap_sales_order_detail", array("status" => 5, 'updated_date' => GetDateNow(), 'updated_by' => $userid));
            } else {
                $this->db->update("uap_sales_order_detail", array("status" => 5, 'updated_date' => GetDateNow()), array("id_sales_order_master" => $sales_order));
            }
            
            $this->db->from("uap_pembayaran_utang_detail");
            $this->db->join("uap_pembayaran_utang_master","uap_pembayaran_utang_master.pembayaran_utang_id=uap_pembayaran_utang_detail.pembayaran_utang_id");
            $this->db->where(array("keterangan"=>"Pembayaran Di Muka","id_sales_order_master"=>$id_sales_order));
            
            $pembayaran_utang_master=$this->db->get()->row();
            if($pembayaran_utang_master!=null && $sales_order['jumlah_bayar']==0)
            {
                $this->db->delete("uap_pembayaran_utang_detail",array("pembayaran_utang_detail_id"=>$pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->delete("uap_pembayaran_utang_master",array("pembayaran_utang_id"=>$pembayaran_utang_master->pembayaran_utang_id));
            }
            else if ($pembayaran_utang_master!=null&&$pembayaran_utang_master->jumlah_bayar!=$sales_order['jumlah_bayar'])
            {
                $updatedet=array();
                $updatedet['updated_date']= GetDateNow();
                $updatedet['updated_by']= $userid;
                $updatedet['jumlah_bayar']=$sales_order['jumlah_bayar'];
                $updatemas=array();
                $updatemas['updated_date']= GetDateNow();
                $updatemas['updated_by']= $userid;
                $updatemas['total_bayar']=$sales_order['jumlah_bayar'];
                $this->db->update("uap_pembayaran_utang_detail",$updatedet,array("pembayaran_utang_detail_id"=>$pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->update("uap_pembayaran_utang_master",$updatemas,array("pembayaran_utang_id"=>$pembayaran_utang_master->pembayaran_utang_id));
            }
            else if($pembayaran_utang_master==null&&$sales_order['jumlah_bayar']>0)
            {
                $dokumenpembayaran=AutoIncrement('uap_pembayaran_utang_master', $cabang->kode_cabang . '/BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster=array();
                $pembayaranmaster['dokumen']=$dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi']=$sales_order['tanggal'];
                $pembayaranmaster['id_supplier']=$sales_order['id_supplier'];
                $pembayaranmaster['jenis_pembayaran']="cash";
                $pembayaranmaster['total_bayar']=$sales_order['jumlah_bayar']>$sales_order['grandtotal']?$sales_order['grandtotal']:$sales_order['jumlah_bayar'];
                $pembayaranmaster['biaya']=0;
                $pembayaranmaster['potongan']=0;
                $pembayaranmaster['status']=1;
                $pembayaranmaster['keterangan']="Pembayaran Di Muka";
                $pembayaranmaster['id_cabang']=ForeignKeyFromDb($model['id_cabang']);
                $pembayaranmaster['created_date']= GetDateNow();
                $pembayaranmaster['created_by']=$userid;
                $this->db->insert("uap_pembayaran_utang_master",$pembayaranmaster);
                $pembayaran_utang_id=$this->db->insert_id();
                $pembayarandetail=array();
                $pembayarandetail['id_sales_order_master']=$id_sales_order;
                $pembayarandetail['pembayaran_utang_id']=$pembayaran_utang_id;
                $pembayarandetail['jumlah_bayar']=$sales_order['jumlah_bayar'];
                $pembayarandetail['created_date']= GetDateNow();
                $pembayarandetail['created_by']=$userid;
                $this->db->insert("uap_pembayaran_utang_detail",$pembayarandetail);
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"24","dokumen"=>$sales_order['nomor_master'] ));
            $akun_sales_order=$this->db->get()->row();
            
            if(CheckEmpty($akun_sales_order))
            {
                $akunsales_orderinsert=array();
                $akunsales_orderinsert['id_gl_account']="24";
                $akunsales_orderinsert['tanggal_buku']=$sales_order['tanggal'];
                $akunsales_orderinsert['id_cabang'] = $sales_order['id_cabang'];
                $akunsales_orderinsert['keterangan']="Pembelian Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunsales_orderinsert['dokumen']=$sales_order['nomor_master'] ;
                $akunsales_orderinsert['subject_name']=@$supplier->kode_supplier;
                $akunsales_orderinsert['id_subject']=$sales_order['id_supplier'];
                $akunsales_orderinsert['debet']=$totalsales_orderwithoutppn;
                $akunsales_orderinsert['kredit']=0;
                $akunsales_orderinsert['created_date']= GetDateNow();
                $akunsales_orderinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunsales_orderinsert);
            } 
            else
            {
                $akunsales_orderupdate['tanggal_buku']=$sales_order['tanggal'];
                $akunsales_orderupdate['id_cabang'] = $sales_order['id_cabang'];
                $akunsales_orderupdate['keterangan']="Pembelian Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunsales_orderupdate['dokumen']=$sales_order['nomor_master'] ;
                $akunsales_orderupdate['subject_name']=@$supplier->kode_supplier;
                $akunsales_orderupdate['id_subject']=$sales_order['id_supplier'];
                $akunsales_orderupdate['debet']=$totalsales_orderwithoutppn;
                $akunsales_orderupdate['kredit']=0;
                $akunsales_orderupdate['updated_date']= GetDateNow();
                $akunsales_orderupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunsales_orderupdate,array("id_buku_besar"=>$akun_sales_order->id_buku_besar));
            }
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"33","dokumen"=>$sales_order['nomor_master'] ));
            $akun_ppn=$this->db->get()->row();
            
            if(CheckEmpty($akun_ppn)&&$sales_order['nominal_ppn']>0)
            {
                $akunppninsert=array();
                $akunppninsert['id_gl_account']="33";
                $akunppninsert['tanggal_buku']=$sales_order['tanggal'];
                $akunppninsert['id_cabang'] = $sales_order['id_cabang'];
                $akunppninsert['keterangan']="PPN sales_order Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunppninsert['dokumen']=$sales_order['nomor_master'] ;
                $akunppninsert['subject_name']=@$supplier->kode_supplier;
                $akunppninsert['id_subject']=$sales_order['id_supplier'];
                $akunppninsert['debet']=$sales_order['nominal_ppn'];
                $akunppninsert['kredit']=0;
                $akunppninsert['created_date']= GetDateNow();
                $akunppninsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunppninsert);
            } 
            else if(!CheckEmpty($akun_ppn))
            {
                $akunppnupdate['tanggal_buku']=$sales_order['tanggal'];
                $akunppnupdate['id_cabang'] = $sales_order['id_cabang'];
                $akunppnupdate['keterangan']="PPN sales_order Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunppnupdate['dokumen']=$sales_order['nomor_master'] ;
                $akunppnupdate['subject_name']=@$supplier->kode_supplier;
                $akunppnupdate['id_subject']=$sales_order['id_supplier'];
                $akunppnupdate['debet']=$sales_order['nominal_ppn'];
                $akunppnupdate['kredit']=0;
                $akunppnupdate['updated_date']= GetDateNow();
                $akunppnupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunppnupdate,array("id_buku_besar"=>$akun_ppn->id_buku_besar));
            }
            
            
            
            
            
            
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"11","dokumen"=>$sales_order['nomor_master'] ));
            $akun_utang=$this->db->get()->row();
            
            if(CheckEmpty($akun_utang)&&$sales_order['jumlah_bayar']<$sales_order['grandtotal'])
            {
                $akunutanginsert=array();
                $akunutanginsert['id_gl_account']="11";
                $akunutanginsert['tanggal_buku']=$sales_order['tanggal'];
                $akunutanginsert['id_cabang'] = $sales_order['id_cabang'];
                $akunutanginsert['keterangan']="Utang sales_order Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunutanginsert['dokumen']=$sales_order['nomor_master'] ;
                $akunutanginsert['subject_name']=@$supplier->kode_supplier;
                $akunutanginsert['id_subject']=$sales_order['id_supplier'];
                $akunutanginsert['debet']=0;
                $akunutanginsert['kredit']=$sales_order['grandtotal']-$sales_order['jumlah_bayar'];
                $akunutanginsert['created_date']= GetDateNow();
                $akunutanginsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunutanginsert);
            } 
            else if(!CheckEmpty($akun_utang))
            {
                $akunutangupdate['tanggal_buku']=$sales_order['tanggal'];
                $akunutangupdate['id_cabang'] = $sales_order['id_cabang'];
                $akunutangupdate['keterangan']="Utang sales_order Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunutangupdate['dokumen']=$sales_order['nomor_master'] ;
                $akunutangupdate['subject_name']=@$supplier->kode_supplier;
                $akunutangupdate['id_subject']=$sales_order['id_supplier'];
                $akunutangupdate['debet']=0;
                $akunutangupdate['kredit']=$sales_order['grandtotal']-$sales_order['jumlah_bayar'];
                $akunutangupdate['updated_date']= GetDateNow();
                $akunutangupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunutangupdate,array("id_buku_besar"=>$akun_utang->id_buku_besar));
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"25","dokumen"=>$sales_order['nomor_master'] ));
            $akun_potongan=$this->db->get()->row();
            
            if(CheckEmpty($akun_potongan)&&$sales_order['disc_pembulatan']>0)
            {
                $akunpotonganinsert=array();
                $akunpotonganinsert['id_gl_account']="25";
                $akunpotonganinsert['tanggal_buku']=$sales_order['tanggal'];
                $akunpotonganinsert['id_cabang'] = $sales_order['id_cabang'];
                $akunpotonganinsert['keterangan']="Potongan sales_order Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunpotonganinsert['dokumen']=$sales_order['nomor_master'] ;
                $akunpotonganinsert['subject_name']=@$supplier->kode_supplier;
                $akunpotonganinsert['id_subject']=$sales_order['id_supplier'];
                $akunpotonganinsert['debet']=0;
                $akunpotonganinsert['kredit']=$sales_order['disc_pembulatan'];
                $akunpotonganinsert['created_date']= GetDateNow();
                $akunpotonganinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunpotonganinsert);
            } 
            else if(!CheckEmpty($akun_potongan))
            {
                $akunpotonganupdate['tanggal_buku']=$sales_order['tanggal'];
                $akunpotonganupdate['id_cabang'] = $sales_order['id_cabang'];
                $akunpotonganupdate['keterangan']="Potongan sales_order Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunpotonganupdate['dokumen']=$sales_order['nomor_master'] ;
                $akunpotonganupdate['subject_name']=@$supplier->kode_supplier;
                $akunpotonganupdate['id_subject']=$sales_order['id_supplier'];
                $akunpotonganupdate['debet']=0;
                $akunpotonganupdate['kredit']=$sales_order['disc_pembulatan'];
                $akunpotonganupdate['updated_date']= GetDateNow();
                $akunpotonganupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunpotonganupdate,array("id_buku_besar"=>$akun_potongan->id_buku_besar));
            }
            
            
            $this->db->from("uap_buku_besar");
            $this->db->where(array("id_gl_account"=>"3","dokumen"=>$sales_order['nomor_master'] ));
            $akun_kas=$this->db->get()->row();
            
            if(CheckEmpty($akun_kas)&&$sales_order['jumlah_bayar']>0)
            {
                $akunkasinsert=array();
                $akunkasinsert['id_gl_account']="3";
                $akunkasinsert['tanggal_buku']=$sales_order['tanggal'];
                $akunkasinsert['id_cabang'] = $sales_order['id_cabang'];
                $akunkasinsert['keterangan']="Pengeluaran kas untuk sales_order Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunkasinsert['dokumen']=$sales_order['nomor_master'] ;
                $akunkasinsert['subject_name']=@$supplier->kode_supplier;
                $akunkasinsert['id_subject']=$sales_order['id_supplier'];
                $akunkasinsert['debet']=0;
                $akunkasinsert['kredit']=$sales_order['jumlah_bayar'];
                $akunkasinsert['created_date']= GetDateNow();
                $akunkasinsert['created_by']=$userid;
                $this->db->insert("uap_buku_besar",$akunkasinsert);
            } 
            else if(!CheckEmpty($akun_kas))
            {
                $akunkasupdate['tanggal_buku']=$sales_order['tanggal'];
                $akunkasupdate['id_cabang'] = $sales_order['id_cabang'];
                $akunkasupdate['keterangan']="Pengeluaran kas untuk Ke " .@$supplier->nama_supplier.' '.$sales_order['keterangan'];
                $akunkasupdate['dokumen']=$sales_order['nomor_master'] ;
                $akunkasupdate['subject_name']=@$supplier->kode_supplier;
                $akunkasupdate['id_subject']=$sales_order['id_supplier'];
                $akunkasupdate['debet']=0;
                $akunkasupdate['kredit']=$sales_order['jumlah_bayar'];
                $akunkasupdate['updated_date']= GetDateNow();
                $akunkasupdate['updated_by']=$userid;
                $this->db->update("uap_buku_besar",$akunkasupdate,array("id_buku_besar"=>$akun_kas->id_buku_besar));
            }
            
            
           

            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                die();
                SetMessageSession(true, "Data Pembelian PO Sudah di" . (CheckEmpty(@$model['id_sales_order_master']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_sales_order, $sales_order['nomor_master'], 'sales_order');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    
    
  

    // datatables
   
    function GetDatasales_order() {
        $this->load->library('datatables');
        $this->datatables->select('id_sales_order_master,nama_supplier,tanggal,replace(nomor_master,"/","-") as nomor_master,grandtotal,jth_tempo');
        $this->datatables->join("uap_supplier", "uap_supplier.id_supplier=uap_sales_order_master.id_supplier", "left");
        $this->datatables->from('uap_sales_order_master');
        //add this line for join
        //$this->datatables->join('table2', 'uap_sales_order.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';


        if ($isedit) {
            $straction .= anchor("sales_order/editsales_order/$1/$2", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_sales_order_master,nomor_master');
        return $this->datatables->generate();
    }


}
