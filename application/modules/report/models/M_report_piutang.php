<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_piutang extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    function GetDataReportPiutangUnit($params,$mode="datatables",$kode_report="") {
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
             $DB->havingfield("dgmi_prospek_detail.harga_jual_unit");
        }
        else{
            $DB=$this->db;
        }
       
        $DB->select('#_view_piutang_detail.*');
        $DB->from("#_view_piutang_detail");
        
        $where = [];
        $extra = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_view_piutang_detail.no_prospek'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_view_piutang_detail.tanggal_prospek BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_view_piutang_detail.tanggal_prospek"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_view_piutang_detail.tanggal_prospek"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                $where['#_view_piutang_detail.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_kategori'])) {
                $where['#_view_piutang_detail.id_kategori'] = $params['id_kategori'] ;
            }
            if (!CheckEmpty($params['id_pegawai'])) {
                $where['#_view_piutang_detail.id_pegawai'] = $params['id_pegawai'] ;
            }
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_view_piutang_detail.id_cabang'] = $params['id_cabang'] ;
            }
            else
            {
                $DB->where_in('#_view_piutang_detail.id_cabang', GetCabangAkses());
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_view_piutang_detail.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        $this->db->order_by("tanggal_prospek");
        $result=null;
        
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
     
        return $result;
    }
    function GetDataRekapPiutang($params, $type = "datatables") {

        if ($type == "datatables") {
            $this->load->library('datatables');
            $DB = $this->datatables;
        } else {
            $DB = $this->db;
        }
        $DB->select('#_customer.id_customer,#_customer.saldo_awal,#_customer.piutang,#_customer.nama_customer,sum(sisa) as total_due,ifnull(SUM(IF(lama_hari<=0 , sisa, 0)),0) AS due0 ,'
                . 'ifnull(SUM(IF(lama_hari>0 and lama_hari<=30 , sisa, 0)),0) AS due30,'
                . 'ifnull(SUM(IF(lama_hari>30 and lama_hari<=60 , sisa, 0)),0) AS due60,'
                . 'ifnull(SUM(IF(lama_hari>60 and lama_hari<=90 , sisa, 0)),0) AS due90,'
                . 'ifnull(SUM(IF(lama_hari>90 , sisa, 0)),0) AS due90plus');
        $DB->from('#_customer');
        $DB->join("#_view_dokumen_sisa", "#_view_dokumen_sisa.id_customer=#_customer.id_customer", "left");
        $DB->group_by("#_customer.id_customer");
       
        $extra = array();
        array_push($extra, "(#_customer.piutang >0 or #_view_dokumen_sisa.sisa>0)");
        
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['#_view_dokumen_sisa.id_cabang'] = $params['id_cabang'];
        }
        else
        {
            $DB->where_in('#_view_dokumen_sisa.id_cabang', GetCabangAkses());
        }
        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $params['#_view_dokumen_sisa.id_customer'] = $params['id_customer'];
        }
        unset($params['id_customer']);
        unset($params['id_cabang']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_view_dokumen_sisa.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
        
        if (count($params)) {
            $DB->where($params);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        if ($type == "datatables") {
            return $DB->generate();
        } else {
            return $DB->get()->result();
        }
    }

    function GetDataPembayaranPiutang($params) {
        $isedit = true;
        $isdelete = true;
        $this->db->select('#_pembayaran_piutang_master.tanggal_transaksi,nama_bank,#_pembayaran_piutang_master.total_bayar,#_pembayaran_piutang_master.jenis_pembayaran,#_pembayaran_piutang_master.dokumen,#_pembayaran_piutang_master.pembayaran_piutang_id,nama_cabang,nama_customer,replace(#_pembayaran_piutang_master.dokumen,"/","-") as nomor_master_link,group_concat(#_prospek.no_prospek SEPARATOR \',\') as no_faktur,#_pembayaran_piutang_master.status');
        $this->db->join("#_customer", "#_customer.id_customer=#_pembayaran_piutang_master.id_customer", "left");
        $this->db->join("#_pembayaran_piutang_detail", "#_pembayaran_piutang_detail.pembayaran_piutang_id=#_pembayaran_piutang_master.pembayaran_piutang_id", "left");
        $this->db->join("#_prospek", "#_prospek.id_prospek=#_pembayaran_piutang_detail.id_prospek", "left");

        $this->db->join("#_cabang", "#_cabang.id_cabang=#_pembayaran_piutang_master.id_cabang", "left");
        $this->db->join("#_bank", "#_bank.id_bank=#_pembayaran_piutang_master.id_bank", "left");
        $this->db->from('#_pembayaran_piutang_master');
        $straction = '';
        $extra = array();
        $this->db->group_by("#_pembayaran_piutang_master.pembayaran_piutang_id");
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['#_pembayaran_piutang_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $params['#_pembayaran_piutang_master.id_customer'] = $params['id_customer'];
        }
        if (isset($params['status']) && !CheckEmpty($params['status'])) {
            $params['#_pembayaran_piutang_master.status'] = $params['status'];
        }
        if (isset($params['type_pembayaran']) && !CheckEmpty($params['type_pembayaran'])) {
            $params['#_pembayaran_piutang_master.jenis_pembayaran'] = $params['type_pembayaran'];
        }

        unset($params['id_customer']);
        unset($params['id_cabang']);
        unset($params['status']);
        unset($params['type_pembayaran']);

        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_pembayaran_piutang_master.tanggal_transaksi BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        } else if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['#_pembayaran_piutang_master.tanggal_transaksi'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date'], $params['end_date']);
        } else if (isset($params['end_date']) && empty($params['start_date'])) {
            $params['#_pembayaran_piutang_master.tanggal_transaksi'] = DefaultTanggalDatabase($params['end_date']);
            unset($params['start_date'], $params['end_date']);
        }

        $this->db->group_by("#_pembayaran_piutang_master.pembayaran_piutang_id");
        $this->db->order_by("#_pembayaran_piutang_master.tanggal_transaksi");
        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $listresult=$this->db->get()->result();
        return $listresult;
    }

    function GetDataDetailPiutang($params, $type = "datatables") {

        if ($type == "datatables") {
            $this->load->library('datatables');
            $DB = $this->datatables;
        } else {
            $DB = $this->db;
        }
        $DB->select('#_view_dokumen_sisa.*');
        $DB->from('#_view_dokumen_sisa');

        $extra = array();

        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['#_view_dokumen_sisa.id_cabang'] = $params['id_cabang'];
        }
         else
        {
            $DB->where_in('#_view_dokumen_sisa.id_cabang', GetCabangAkses());
        }
        if (isset($params['id_customer']) && !CheckEmpty(@$params['id_customer'])) {
            $params['#_view_dokumen_sisa.id_customer'] = $params['id_customer'];
        }
        unset($params['id_customer']);
        unset($params['id_cabang']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_view_dokumen_sisa.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
         if (count($params)) {
            $DB->where($params);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        if ($type == "datatables") {
            return $DB->generate();
        } else {
            return $DB->get()->result();
        }
    }

}