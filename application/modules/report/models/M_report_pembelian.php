<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_pembelian extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function GetDataReportKPU($params,$mode="datatables") {
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
        $DB->select('#_kpu.*,#_customer.nama_customer,#_kpu.tanggal_pelunasan,sum(#_do_kpu_detail.qty_do) as qty_do,#_kpu_detail.vin,#_kpu_detail.qty,#_kpu.id_kpu as id,nama_unit,nama_warna,vrf_code,nama_supplier,group_concat(distinct nomor_do_kpu) as nomor_do_kpu');
        $DB->from("#_kpu");
        $DB->where('#_kpu.deleted_date', null);
        $DB->join('#_kpu_detail', '#_kpu_detail.id_kpu = #_kpu.id_kpu', 'left');
        $DB->join('#_do_kpu_detail', '#_do_kpu_detail.id_kpu_detail = #_kpu_detail.id_kpu_detail and #_do_kpu_detail.status !=2', 'left');
        $DB->join('#_do_kpu', '#_do_kpu.id_do_kpu = #_do_kpu_detail.id_do_kpu and #_do_kpu.status !=2', 'left');
        //add this line for join
        $DB->join('#_vrf', '#_vrf.id_vrf = #_kpu_detail.id_vrf', 'left');
        $DB->join('#_warna', '#_warna.id_warna = #_kpu_detail.id_warna', 'left');
        $DB->join('#_unit', '#_unit.id_unit = #_kpu_detail.id_unit', 'left');
        $DB->join('#_supplier', '#_supplier.id_supplier = #_kpu.id_supplier', 'left');
        $DB->join('#_customer', '#_customer.id_customer = #_kpu.id_customer', 'left');
        
        
        
        $DB->group_by("#_kpu.id_kpu");
        $extra=[];
        $where=[];
         if (!CheckEmpty(@$params['keyword'])) {
            $pencariankeyword="nomor_master";
            
            if(@$params['jenis_keyword']=="nomor_master")
            {
                $pencariankeyword="#_kpu.nomor_master";
            }
            else if(@$params['jenis_keyword']=="no_vrf")
            {
                $pencariankeyword="#_vrf.vrf_code";
            }
            else if(@$params['jenis_keyword']=="nomor_do_kpu")
            {
                $pencariankeyword="#_do_kpu.nomor_do_kpu";
            }
            else if(@$params['jenis_keyword']=="no_prospek")
            {
                $pencariankeyword="#_prospek.no_prospek";
                $DB->join("#_unit_serial","#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail");
                $DB->join("#_prospek_detail","#_unit_serial.id_prospek_detail=#_prospek_detail.id_prospek_detail");
                $DB->join("#_prospek","#_prospek.id_prospek=#_prospek_detail.id_prospek");
            }
            
            
            
            
            
            $where[$pencariankeyword] = trim($params['keyword']);
            
            
        } else {
            $tanggal="date(#_prospek.tanggal_prospek)";
            if($params['jenis_pencarian']=="created_date")
            {
                $tanggal="date(#_kpu.created_date)";
            }
            else if($params['jenis_pencarian']=="updated_date")
            {
                 $tanggal="date(#_kpu.updated_date)";
            }
            else if($params['jenis_pencarian']=="tanggal")
            {
                 $tanggal="#_kpu.tanggal";
            }
            $this->db->order_by($tanggal,"desc");
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal." BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }

            if (!CheckEmpty($params['status'])) {
                $where['#_kpu.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_kategori'])) {
                $where['#_unit.id_kategori'] = $params['id_kategori'] ;
            }
            if (!CheckEmpty($params['id_supplier'])) {
                $where['#_kpu.id_supplier'] = $params['id_supplier'];
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_kpu.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        return $result;
    }

}
