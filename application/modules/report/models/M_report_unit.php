<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_unit extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function GetDataReportunit($params,$mode="datatables") {
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
        $DB->select('#_unit_serial.*,#_kpu.nomor_master as no_kpu,#_kpu.tanggal as tanggal_kpu,#_do_kpu.tanggal_do as tanggal_do_kpu,#_do_kpu.nomor_do_kpu as no_do_kpu,nama_warna,nama_kategori,nama_unit,nomor_do_kpu,#_pool.nama_pool as unit_position,nama_pool'
                . ',#_prospek.no_prospek,#_prospek.tanggal_prospek,#_customer.nama_customer,nomor_buka_jual,tanggal_buka_jual,(case when #_buka_jual.status=1 then "Buka Jual" when #_buka_jual.status=7 then  "Permintaan Buka Jual" when #_buka_jual.status=0 then  "Pending Buka Jual" else "Stok" end) as status_buka_jual'
                . ',#_do_prospek.nomor_do_prospek,#_do_prospek.tanggal_do as tanggal_do_prospek');
        $DB->from("#_unit_serial");
         //add this line for join
        $DB->join('#_warna', '#_warna.id_warna = #_unit_serial.id_warna', 'left');
        $DB->join('#_do_kpu_detail', '#_do_kpu_detail.id_do_kpu_detail = #_unit_serial.id_do_kpu_detail', 'left');
        $DB->join('#_kpu_detail', '#_kpu_detail.id_kpu_detail = #_do_kpu_detail.id_kpu_detail', 'left');
        
        $DB->join('#_kpu', '#_kpu.id_kpu = #_kpu_detail.id_kpu', 'left');
        $DB->join('#_do_kpu', '#_do_kpu.id_do_kpu = #_do_kpu_detail.id_do_kpu', 'left');
        $DB->join('#_buka_jual', '#_buka_jual.id_buka_jual = #_unit_serial.id_buka_jual', 'left');
        $DB->join('#_unit', '#_unit.id_unit = #_unit_serial.id_unit', 'left');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori', 'left');
        $DB->join('#_pool', '#_unit_serial.id_pool = #_pool.id_pool', 'left');
        $DB->join('#_prospek', '#_prospek.id_prospek = #_buka_jual.id_prospek', 'left');
        $DB->join('#_prospek_detail', '#_prospek.id_prospek = #_prospek_detail.id_prospek', 'left');
        $DB->join('#_do_prospek_detail', '#_do_prospek_detail.id_do_prospek_detail = #_unit_serial.id_do_prospek_detail', 'left');
        $DB->join('#_do_prospek', '#_do_prospek.id_do_prospek = #_do_prospek_detail.id_do_prospek', 'left');
        $DB->join('#_customer', '#_customer.id_customer = #_prospek.id_customer', 'left');
        $extra=[];
        $where=[];
        if(!CheckEmpty($params['pencarian_keyword'])&&!CheckEmpty($params['keyword'])){
            $searchby=$params['pencarian_keyword'];
          
            
            if($params['pencarian_keyword']=="engine_no"||$params['pencarian_keyword']=="vin_number")
            {
                $DB->like($searchby, strtolower($params['keyword']), 'both'); 
            }
            else
            {
                  $where["lower(".$searchby.")"] = strtolower($params['keyword']);
            }
               
            
        }
        else {

            if (!CheckEmpty($params['jenis_pencarian'])) {
                $tanggal = "date(#_prospek.tanggal_prospek)";
                if ($params['jenis_pencarian'] == "tanggal_buka_jual") {
                    $tanggal = "date(#_buka_jual.tanggal_buka_jual)";
                }
                else if ($params['jenis_pencarian'] == "tanggal_do") {
                    $tanggal = "date(#_do_prospek.tanggal_do)";
                }
                else if ($params['jenis_pencarian'] == "tanggal_do_kpu") {
                    $tanggal = "date(#_do_kpu.tanggal_do)";
                }
                $this->db->order_by($tanggal, "desc");


                if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                    array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                    unset($params['start_date'], $params['end_date']);
                } else {

                    if (isset($params['start_date']) && empty($params['end_date'])) {
                        $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                        unset($params['start_date']);
                    } else {
                        $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                        unset($params['end_date']);
                    }
                }
            }



            $where['#_unit_serial.deleted_date'] = null;
            if (!CheckEmpty(@$params['keyword'])) {
                $where['#_unit_serial.vin_number'] = $params['keyword'];
            }

            if (!CheckEmpty(@$params['vin'])) {
                $where['#_unit_serial.vin'] = $params['vin'];
            }


            if (!CheckEmpty(@$params['id_kategori'])) {
                $where['#_unit.id_kategori'] = $params['id_kategori'];
            }
            if (!CheckEmpty(@$params['id_customer'])) {
                $where['#_prospek.id_customer'] = $params['id_customer'];
            }
            if (!CheckEmpty(@$params['id_pool'])) {
                $where['#_unit_serial.id_pool'] = $params['id_pool'];
            }
            if (!CheckEmpty(@$params['id_type_unit'])) {
                $where['#_unit.id_type_unit'] = $params['id_type_unit'];
            }
            if (!CheckEmpty(@$params['id_type_body'])) {
                $where['#_unit.id_type_body'] = $params['id_type_body'];
            }
            if (!CheckEmpty(@$params['id_unit'])) {
                $where['#_unit.id_unit'] = $params['id_unit'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }

        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        
        return $result;

    }
    
    
    function GetDataReportUnitSummary($params,$mode="datatables")
    {
        $this->db->from("#_unit_serial");
        $this->db->join("(select #_history_stok_unit.id_unit_serial,max(tanggal) as tanggal_max from #_history_stok_unit group by #_history_stok_unit.id_unit_serial) history","history on history.id_unit_serial=#_unit_serial.id_unit_serial");
        $this->db->join("#_history_stok_unit","#_history_stok_unit.id_unit_serial=history.id_unit_serial and history.tanggal_max=#_history_stok_unit.tanggal and #_history_stok_unit.tanggal <='". DefaultTanggalDatabase($params['end_date'])."'");
        $this->db->join("#_pool","#_pool.id_pool=#_history_stok_unit.id_pool");
        $this->db->join("#_buka_jual","#_buka_jual.id_buka_jual=#_unit_serial.id_buka_jual","left");
        $this->db->join("#_prospek","#_prospek.id_prospek=#_buka_jual.id_prospek","left");
        $this->db->join("#_unit","#_unit.id_unit=#_unit_serial.id_unit","left");
        $this->db->join("#_kategori","#_kategori.id_kategori=#_unit.id_kategori","left");
        $this->db->join("#_customer","#_buka_jual.id_customer=#_customer.id_customer","left");
        $this->db->group_by("#_unit_serial.id_unit_serial");
        $this->db->select("#_unit_serial.vin_number,#_history_stok_unit.tanggal,no_prospek,#_unit_serial.vin,#_unit_serial.engine_no,no_prospek,nama_unit,nama_kategori,nama_customer,tanggal_buka_jual,nama_pool");
        $tanggal = "date(#_history_stok_unit.tanggal)";
        
        $this->db->order_by($tanggal, "desc");

        

        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_unit_serial.vin_number'] = $params['keyword'];
        }

        if (!CheckEmpty(@$params['vin'])) {
            $where['#_unit_serial.vin'] = $params['vin'];
        }


        if (!CheckEmpty(@$params['id_kategori'])) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_customer'])) {
            $where['#_prospek.id_customer'] = $params['id_customer'];
        }
        if (!CheckEmpty(@$params['id_pool'])) {
            $where['#_unit_serial.id_pool'] = $params['id_pool'];
        }
        
        $where['#_history_stok_unit.id_pool !='] = 3;
        if (!CheckEmpty(@$params['id_type_unit'])) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_type_body'])) {
            $where['#_unit.id_type_body'] = $params['id_type_body'];
        }
        if (!CheckEmpty(@$params['id_unit'])) {
            $where['#_unit.id_unit'] = $params['id_unit'];
        }
        if (count($where)) {
            $this->db->where($where);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        
        
        $listkartu=$this->db->get()->result_array();
        return $listkartu;
    }
    
    function GetDataReportunitKartu($params,$mode="datatables") {
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
        $DB->select('#_unit_serial.*,#_history_stok_unit.tanggal,#_history_stok_unit.id_history_stok_unit,#_history_stok_unit.ref_doc,#_kpu.nomor_master as no_kpu,#_kpu.tanggal as tanggal_kpu,#_do_kpu.nomor_do_kpu as no_do_kpu,nama_warna,nama_kategori,nama_unit,nomor_do_kpu,#_pool.nama_pool as unit_position,nama_pool'
                . ',#_prospek.no_prospek,#_prospek.tanggal_prospek,#_customer.nama_customer,nomor_buka_jual,tanggal_buka_jual,(case when #_buka_jual.status=1 then "Buka Jual" when #_buka_jual.status=7 then  "Permintaan Buka Jual" when #_buka_jual.status=0 then  "Pending Buka Jual" else "Stok" end) as status_buka_jual'
                . ',#_do_prospek.nomor_do_prospek,#_do_prospek.tanggal_do as tanggal_do_prospek');
        $DB->from("#_history_stok_unit");
        $DB->join('#_unit_serial', '#_history_stok_unit.id_unit_serial = #_unit_serial.id_unit_serial', 'left');
         //add this line for join
        $DB->join('#_warna', '#_warna.id_warna = #_unit_serial.id_warna', 'left');
        $DB->join('#_do_kpu_detail', '#_do_kpu_detail.id_do_kpu_detail = #_unit_serial.id_do_kpu_detail', 'left');
        $DB->join('#_kpu_detail', '#_kpu_detail.id_kpu_detail = #_do_kpu_detail.id_kpu_detail', 'left');
        
        $DB->join('#_kpu', '#_kpu.id_kpu = #_kpu_detail.id_kpu', 'left');
        $DB->join('#_do_kpu', '#_do_kpu.id_do_kpu = #_do_kpu_detail.id_do_kpu', 'left');
        $DB->join('#_buka_jual', '#_buka_jual.id_buka_jual = #_unit_serial.id_buka_jual', 'left');
        $DB->join('#_unit', '#_unit.id_unit = #_unit_serial.id_unit', 'left');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori', 'left');
        $DB->join('#_pool', '#_unit_serial.id_pool = #_pool.id_pool', 'left');
        $DB->join('#_prospek', '#_prospek.id_prospek = #_buka_jual.id_prospek', 'left');
        $DB->join('#_prospek_detail', '#_prospek.id_prospek = #_prospek_detail.id_prospek', 'left');
        $DB->join('#_do_prospek_detail', '#_do_prospek_detail.id_do_prospek_detail = #_unit_serial.id_do_prospek_detail', 'left');
        $DB->join('#_do_prospek', '#_do_prospek.id_do_prospek = #_do_prospek_detail.id_do_prospek', 'left');
        $DB->join('#_customer', '#_customer.id_customer = #_prospek.id_customer', 'left');
        $extra=[];
        $where=[];
        if(!CheckEmpty($params['pencarian_keyword'])&&!CheckEmpty($params['keyword'])){
            $searchby=$params['pencarian_keyword'];
            $where["lower(".$searchby.")"] = strtolower($params['keyword']);
        }
        else {

            if (!CheckEmpty($params['jenis_pencarian'])) {
                $tanggal = "date(#_history_stok_unit.tanggal)";
                
                $this->db->order_by($tanggal, "desc");


                if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                    array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                    unset($params['start_date'], $params['end_date']);
                } else {

                    if (isset($params['start_date']) && empty($params['end_date'])) {
                        $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                        unset($params['start_date']);
                    } else {
                        $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                        unset($params['end_date']);
                    }
                }
            }




            if (!CheckEmpty(@$params['keyword'])) {
                $where['#_unit_serial.vin_number'] = $params['keyword'];
            }

            if (!CheckEmpty(@$params['vin'])) {
                $where['#_unit_serial.vin'] = $params['vin'];
            }


            if (!CheckEmpty(@$params['id_kategori'])) {
                $where['#_unit.id_kategori'] = $params['id_kategori'];
            }
            if (!CheckEmpty(@$params['id_customer'])) {
                $where['#_prospek.id_customer'] = $params['id_customer'];
            }
            if (!CheckEmpty(@$params['id_pool'])) {
                $where['#_unit_serial.id_pool'] = $params['id_pool'];
            }
            if (!CheckEmpty(@$params['id_type_unit'])) {
                $where['#_unit.id_type_unit'] = $params['id_type_unit'];
            }
            if (!CheckEmpty(@$params['id_type_body'])) {
                $where['#_unit.id_type_body'] = $params['id_type_body'];
            }
            if (!CheckEmpty(@$params['id_unit'])) {
                $where['#_unit.id_unit'] = $params['id_unit'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }

        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        
        return $result;

    }

}
