<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_utang extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    
    function GetDataReportUtangUnit($params,$mode="datatables",$kode_report="") {
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
             $DB->havingfield("#_prospek_detail.harga_jual_unit");
        }
        else{
            $DB=$this->db;
        }
       
        $DB->select('#_view_utang_detail.*');
        $DB->from("#_view_utang_detail");
        
        $where = [];
        $extra = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_view_utang_detail.no_kpu'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_view_utang_detail.tanggal_kpu BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_view_utang_detail.tanggal_kpu"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_view_utang_detail.tanggal_kpu"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                $where['#_view_utang_detail.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_kategori'])) {
                $where['#_view_utang_detail.id_kategori'] = $params['id_kategori'] ;
            }
            
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_view_utang_detail.id_cabang'] = $params['id_cabang'] ;
            }
            else
            {
                $DB->where_in('#_view_utang_detail.id_cabang', GetCabangAkses());
            }
            if (!CheckEmpty($params['id_supplier'])) {
                $where['#_view_utang_detail.id_supplier'] = $params['id_supplier'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        $this->db->order_by("tanggal_kpu");
        $result=null;
        
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
     
        return $result;
    }
    function GetDataPembayaranHutang($params) {
        $isedit = true;
        $isdelete = true;
        $this->db->select('#_pembayaran_utang_master.tanggal_transaksi,nama_bank,#_pembayaran_utang_master.total_bayar,#_pembayaran_utang_master.jenis_pembayaran,#_pembayaran_utang_master.dokumen,#_pembayaran_utang_master.pembayaran_utang_id,nama_cabang,nama_supplier,replace(#_pembayaran_utang_master.dokumen,"/","-") as nomor_master_link,group_concat(concat(#_kpu.nomor_master,\' => \',#_kpu.no_faktur ) SEPARATOR \' , \') as no_faktur,#_kpu.status');
        $this->db->join("#_supplier", "#_supplier.id_supplier=#_pembayaran_utang_master.id_supplier", "left");
        $this->db->join("#_pembayaran_utang_detail", "#_pembayaran_utang_detail.pembayaran_utang_id=#_pembayaran_utang_master.pembayaran_utang_id", "left");
        $this->db->join("#_kpu", "#_kpu.id_kpu=#_pembayaran_utang_detail.id_kpu", "left");

        $this->db->join("#_cabang", "#_cabang.id_cabang=#_pembayaran_utang_master.id_cabang", "left");
        $this->db->join("#_bank", "#_bank.id_bank=#_pembayaran_utang_master.id_bank", "left");
        $this->db->from('#_pembayaran_utang_master');
        $straction = '';
        $extra = array();
        $this->db->group_by("#_pembayaran_utang_master.pembayaran_utang_id");
        if (isset($params['id_cabang']) && !CheckEmpty(@$params['id_cabang'])) {
            $params['#_pembayaran_utang_master.id_cabang'] = $params['id_cabang'];
        }
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['#_pembayaran_utang_master.id_supplier'] = $params['id_supplier'];
        }
        if (isset($params['status']) && !CheckEmpty($params['status'])) {
            $params['#_pembayaran_utang_master.status'] = $params['status'];
        }
         if (isset($params['type_pembayaran']) && !CheckEmpty($params['type_pembayaran'])) {
            $params['#_pembayaran_piutang_master.jenis_pembayaran'] = $params['type_pembayaran'];
        }

        unset($params['type_pembayaran']);
        unset($params['id_supplier']);
        unset($params['id_cabang']);
        unset($params['status']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_pembayaran_utang_master.tanggal_transaksi BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        } else if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['#_pembayaran_utang_master.tanggal_transaksi'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date'], $params['end_date']);
        } else if (isset($params['end_date']) && empty($params['start_date'])) {
            $params['#_pembayaran_utang_master.tanggal_transaksi'] = DefaultTanggalDatabase($params['end_date']);
            unset($params['start_date'], $params['end_date']);
        }

        $this->db->group_by("#_pembayaran_utang_master.pembayaran_utang_id");
        $this->db->order_by("#_pembayaran_utang_master.tanggal_transaksi");
        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $listresult=$this->db->get()->result();
        return $listresult;
    }

    function GetDataRekapUtang($params, $type = "datatables") {

        if ($type == "datatables") {
            $this->load->library('datatables');
            $DB = $this->datatables;
        } else {
            $DB = $this->db;
        }
        $DB->select('#_supplier.id_supplier,#_supplier.saldo_awal,#_supplier.hutang,#_supplier.nama_supplier,sum(sisa) as total_due,ifnull(SUM(IF(lama_hari<=0 , sisa, 0)),0) AS due0 ,'
                . 'ifnull(SUM(IF(lama_hari>0 and lama_hari<=30 , sisa, 0)),0) AS due30,'
                . 'ifnull(SUM(IF(lama_hari>30 and lama_hari<=60 , sisa, 0)),0) AS due60,'
                . 'ifnull(SUM(IF(lama_hari>60 and lama_hari<=90 , sisa, 0)),0) AS due90,'
                . 'ifnull(SUM(IF(lama_hari>90 , sisa, 0)),0) AS due90plus');
        $DB->from('#_supplier');
        $DB->join("#_view_dokumen_hutang_sisa", "#_view_dokumen_hutang_sisa.id_supplier=#_supplier.id_supplier", "left");
        $DB->group_by("#_supplier.id_supplier");
       
        $extra = array();
        array_push($extra, "(#_supplier.hutang >0 or #_view_dokumen_hutang_sisa.sisa>0)");
        
       
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['#_view_dokumen_hutang_sisa.id_supplier'] = $params['id_supplier'];
        }
        unset($params['id_supplier']);
        unset($params['id_cabang']);


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_view_dokumen_hutang_sisa.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
        
        if (count($params)) {
            $DB->where($params);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        if ($type == "datatables") {
            return $DB->generate();
        } else {
            return $DB->get()->result();
        }
    }

    function GetDataDetailUtang($params, $type = "datatables") {

        if ($type == "datatables") {
            $this->load->library('datatables');
            $DB = $this->datatables;
        } else {
            $DB = $this->db;
        }
        $DB->select('#_view_dokumen_hutang_sisa.*');
        $DB->from('#_view_dokumen_hutang_sisa');
        $DB->where("sisa >",0);
        $extra = array();

       
        if (isset($params['id_supplier']) && !CheckEmpty(@$params['id_supplier'])) {
            $params['#_view_dokumen_hutang_sisa.id_supplier'] = $params['id_supplier'];
        }
        unset($params['id_supplier']);
      


        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_view_dokumen_hutang_sisa.tanggal BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
        if (count($params)) {
            $DB->where($params);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        if ($type == "datatables") {
            return $DB->generate();
        } else {
            return $DB->get()->result();
        }
    }

}
