<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_bank extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    function GetSaldoAwalBank($tanggal,$id_bank)
    {
        $this->db->from("#_buku_besar");
        $this->db->where(array("id_gl_account"=>33,"tanggal_buku <" =>DefaultTanggalDatabase($tanggal),"id_subject"=>$id_bank));
        $this->db->select("ifnull(sum(debet-kredit),0)as jml");
        $row=$this->db->get()->row();
      
        return $row;
    }
    function GetDataReportbank($params) {
       
        $this->db->select('#_buku_besar.*');
        $this->db->from("#_buku_besar");
        $this->db->join("#_bank","#_bank.kode_bank=#_buku_besar.subject_name");
        $extra=[];
        $this->db->where(array("id_gl_account"=>33));
        if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
            array_push($extra, "#_buku_besar.tanggal_buku BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
            unset($params['start_date'], $params['end_date']);
        }
        if (isset($params['start_date']) && empty($params['end_date'])) {
            $params['tanggal_buku'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }
        
        if (isset($params['start_date'])) {
            $params['tanggal_buku'] = DefaultTanggalDatabase($params['start_date']);
            unset($params['start_date']);
        }


        if (count($params)) {
            $this->db->where($params);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }
        $this->db->order_by("tanggal_buku,created_date");
        $data = $this->db->get()->result();
        $saldoawal=0;
        
        if(count($data)>0)
        {
            $saldoawal=$this->GetSaldoAwalBank($data[0]->tanggal_buku,$data[0]->id_subject)->jml;
        }
        else
        {
           $saldoawal=$this->GetSaldoAwalBank(GetDateNow(),$params['id_bank'])->jml; 
        }
         return array("data"=>$data,"saldo_awal"=>$saldoawal);
    }

}
