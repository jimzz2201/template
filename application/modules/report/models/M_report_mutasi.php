<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_report_mutasi extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    function GetDataReportMutasi($params, $mode="datatables") {
        $this->load->library('datatables');
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
        $DB->select('#_mutasi.id_mutasi as id,#_mutasi.keterangan,no_prospek,poolfrom.nama_pool as nama_pool_from,poolto.nama_pool as nama_pool_to,#_mutasi.up, nama_customer, #_mutasi.alamat,  #_mutasi.nomor_mutasi,
                    nama_type_unit, #_mutasi.tanggal_do, #_buka_jual.tanggal_buka_jual,
                    vin_number, engine_no, vin, #_cabang.id_cabang, #_type_unit.id_type_unit, #_cabang.nama_cabang, #_type_unit.nama_type_unit');
        $DB->from('#_mutasi');
        $DB->where('#_mutasi' . '.deleted_date', null);
        $DB->join('#_mutasi_detail', '#_mutasi_detail.id_mutasi = #_mutasi.id_mutasi', 'left');
        $DB->join('#_unit_serial', '#_mutasi_detail.id_unit_serial = #_unit_serial.id_unit_serial');
        $DB->join('#_buka_jual', '#_unit_serial.id_buka_jual = #_buka_jual.id_buka_jual', 'left');
        $DB->join('#_prospek_detail', '#_prospek_detail.id_prospek_detail = #_unit_serial.id_prospek_detail', 'left');
        $DB->join('#_prospek', '#_prospek.id_prospek = #_prospek_detail.id_prospek', 'left');
        $DB->join('#_customer', '#_customer.id_customer = #_buka_jual.id_customer', 'left');
        $DB->join('#_cabang', '#_prospek.id_cabang = #_cabang.id_cabang', 'left');
        $DB->join('#_warna', '#_warna.id_warna = #_unit_serial.id_warna', 'left');
        $DB->join('#_unit', '#_unit.id_unit = #_unit_serial.id_unit', 'left');
        $DB->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori');
        $DB->join('#_pool poolfrom', 'poolfrom.id_pool = #_mutasi.id_pool_from',"left");
        $DB->join('#_pool poolto', 'poolto.id_pool = #_mutasi.id_pool',"left");

        $isedit = true;
        $isdelete = true;
        $where = [];
        $extra = [];
        $strview = '';
        $strbatal = '';
        $strapproved = '';
        $strrejected = '';
        $stredit = '';
        $strclosed = '';
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_mutasi.nomor_do_prospek'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_mutasi.tanggal_do BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_mutasi.tanggal_do"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_mutasi.tanggal_do"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['id_type_unit'])) {
                $where['#_unit.id_type_unit'] = $params['id_type_unit'];
            }
            if (!CheckEmpty($params['id_pool_from'])) {
                $where['#_mutasi.id_pool_from'] = $params['id_pool_from'];
            }
            if (!CheckEmpty($params['id_pool'])) {
                $where['#_mutasi.id_pool'] = $params['id_pool'];
            }
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_buka_jual.id_cabang'] = $params['id_cabang'] ;
            }
            else
            {
                $DB->where_in('#_buka_jual.id_cabang', GetCabangAkses());
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_buka_jual.id_customer'] = $params['id_customer'];
            }
        }
        if($mode!="datatables")
        {
           $DB->order_by('id_cabang', 'asc');
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $DB->order_by('id_cabang asc', 'id_type_unit asc');
            $result=$this->db->get()->result();
        }
        // print_r($this->db->last_query());
        return $result;
    }

    function GetDataReportDoKpu($params, $mode="datatables") {
        $this->load->library('datatables');
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
        $DB->select('#_do_kpu.*,#_do_kpu.id_do_kpu as id,#_kpu.nomor_master,nama_unit,nama_warna,vrf_code,nama_supplier');
        $DB->from('#_do_kpu');
        $DB->where('#_do_kpu' . '.deleted_date', null);
        $DB->join('#_do_kpu_detail', '#_do_kpu_detail.id_do_kpu = #_do_kpu.id_do_kpu', 'left');
        $DB->join('#_kpu', '#_kpu.id_kpu = #_do_kpu_detail.id_kpu', 'left');
        $DB->join('#_kpu_detail', '#_kpu_detail.id_kpu = #_kpu.id_kpu', 'left');
        $DB->join('#_vrf', '#_vrf.id_vrf = #_kpu_detail.id_vrf', 'left');
        $DB->join('#_warna', '#_warna.id_warna = #_kpu_detail.id_warna', 'left');
        $DB->join('#_unit', '#_unit.id_unit = #_do_kpu_detail.id_unit', 'left');
        $DB->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori');
        $DB->join('#_supplier', '#_supplier.id_supplier = #_do_kpu.id_supplier', 'left');

        $isedit = true;
        $isdelete = true;
        $where = [];
        $extra = [];
        $strview = '';
        $strbatal = '';
        $strapproved = '';
        $strrejected = '';
        $stredit = '';
        $strclosed = '';
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_kpu.nomor_master'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_do_kpu.tanggal_do BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_do_kpu.tanggal_do"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_do_kpu.tanggal_do"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                $where['#_kpu.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_supplier'])) {
                $where['#_kpu.id_supplier'] = $params['id_supplier'];
            }
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }
        if($mode!="datatables")
        {
           $DB->order_by('id_cabang', 'asc');
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        // print_r($this->db->last_query());
        return $result;
    }
    
    function GetDataReportpenjualanDetail($params,$mode="datatables") {
        if($mode=="datatables")
        {
             $this->load->library('datatables');
             $DB=$this->datatables;
        }
        else{
            $DB=$this->db;
        }
       
        $DB->select('#_prospek.*,#_prospek_detail.tahun,#_unit_serial.vin_number,#_do_prospek.tanggal_do,nomor_do_prospek,#_unit_serial.engine_no,#_customer.no_telp,tanggal_buka_jual,nomor_buka_jual,nama_customer as customer_name,#_customer.alamat,nama_cabang,#_prospek_detail.harga_jual_unit,#_prospek_detail.harga_off_the_road,#_prospek_detail.biaya_bbn,#_prospek_detail.total_equipment,#_prospek_detail.harga_on_the_road,#_prospek.id_prospek as id,#_prospek_detail.tahun,nama_kategori,nama_type_unit,nama_warna,nama_payment_method,nama_pegawai,nama_customer,nama_jenis_plat,nama_type_body,nama_unit');
        $DB->from("#_prospek");
        $DB->where('#_prospek.deleted_date', null);
        $DB->join('#_prospek_detail', '#_prospek_detail.id_prospek = #_prospek.id_prospek');
        //add this line for join
        $DB->join('#_customer', '#_prospek.id_customer = #_customer.id_customer');
        $DB->join('#_prospek_detail_unit', '#_prospek_detail.id_prospek_detail = #_prospek_detail_unit.id_prospek_detail');
        $DB->join('#_unit_serial', '#_unit_serial.id_unit_serial = #_prospek_detail_unit.id_unit_serial',"left");
        $DB->join('#_unit', '#_prospek_detail.id_unit = #_unit.id_unit');
        $DB->join('#_buka_jual', '#_unit_serial.id_buka_jual = #_buka_jual.id_buka_jual',"left");
        $DB->join('#_jenis_plat', '#_prospek_detail.id_jenis_plat = #_jenis_plat.id_jenis_plat');
        $DB->join('#_type_body', '#_unit.id_type_body = #_type_body.id_type_body');
        $DB->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit');
        $DB->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori');
        $DB->join('#_cabang', '#_prospek.id_cabang = #_cabang.id_cabang');
        $DB->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna');
        $DB->join('#_payment_method', '#_prospek.id_payment_method = #_payment_method.id_payment_method','left');
        $DB->join('#_do_prospek_detail', '#_do_prospek_detail.id_do_prospek_detail = #_unit_serial.id_do_prospek_detail','left');
        $DB->join('#_do_prospek', '#_do_prospek.id_do_prospek = #_do_prospek_detail.id_do_prospek','left');
        
        $DB->join('#_pegawai', '#_pegawai.id_pegawai = #_prospek.id_pegawai', 'left');
        $where = [];
        $extra = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_prospek.no_prospek'] = $params['keyword'];
        } else {
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, "#_prospek.tanggal_prospek BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {
                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where["#_prospek.tanggal_prospek"] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where["#_prospek.tanggal_prospek"] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }


            if (!CheckEmpty($params['status'])) {
                $where['#_prospek.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_kategori'])) {
                $where['#_kategori.id_kategori'] = $params['id_kategori'] ;
            }
            if (!CheckEmpty($params['id_pegawai'])) {
                $where['#_prospek.id_pegawai'] = $params['id_pegawai'] ;
            }
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_prospek.id_cabang'] = $params['id_cabang'] ;
            }
            else
            {
                $DB->where_in('#_prospek.id_cabang', GetCabangAkses());
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_prospek.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $DB->where($where);
        }
        if (count($extra)) {
            $DB->where(implode(" AND ", $extra));
        }
         $this->db->order_by("tanggal_prospek");
        $result=null;
        if($mode=="datatables")
        {
           $result= $DB->generate();
        }
        else{
            $result=$this->db->get()->result();
        }
        return $result;
    }

}
