<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_vrf extends CI_Model {

    public $table = '#_vrf';
    public $id = 'id_vrf';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function ChangeStatus($id, $status) {
        $array = array("st" => true, "msg" => "Data VRF Berhasil " . ($status == "1" ? "Diapprove" : ($status == "2" ? "Direject" : "Diclose")));
        $update = array();
        $update['status'] = $status;
        $update['approved_date'] = GetDateNow();
        $update['approved_by'] = GetUserId();
        $update['updated_by'] = GetUserId();
        $update['updated_date'] = GetDateNow();
        $this->db->update("#_vrf", $update, array("id_vrf" => $id));
        return $array;
    }

    function Batal($id, $message) {
        $array = array("st" => true, "msg" => "Data VRF Berhasil Dibatalkan");
        $update = array();
        $update['status'] = 5;
        $update['keterangan_batal'] = $message;
        $update['updated_by'] = GetUserId();
        $update['updated_date'] = GetDateNow();
        $this->db->update("#_vrf", $update, array("id_vrf" => $id));
        return $array;
    }

    // datatables
    function GetDatavrf($params) {
        $this->load->library('datatables');
        $this->datatables->select('#_vrf.*,#_vrf.id_vrf as id,nama_supplier as nama_supplier,nama_unit,nama_warna,nama_customer');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_unit', '#_vrf.id_unit = #_unit.id_unit', 'left');
        $this->datatables->join('#_warna', '#_vrf.id_colour = #_warna.id_warna', 'left');
        $this->datatables->join('#_supplier', '#_vrf.id_supplier = #_supplier.id_supplier', 'left');
        $this->datatables->join('#_customer', '#_vrf.id_customer = #_customer.id_customer', 'left');
        $isedit = true;
        $isdelete = true;
        $strview = '';
        $strbatal = '';
        $strapproved = '';
        $strrejected = '';
        $stredit = '';
        $strclosed = '';
        $where = array();
        $extra = array();
        if (!CheckEmpty(@$params['keyword'])) {

            $pencariankeyword = "vrf_code";

            if (@$params['jenis_keyword'] == "no_kpu") {
                $pencariankeyword = "#_kpu.nomor_master";
                $this->datatables->join("#_kpu_detail", "#_kpu_detail.id_vrf=#_vrf.id_vrf");
                $this->datatables->join("#_kpu", "#_kpu.id_kpu=#_kpu_detail.id_kpu");
                $this->datatables->group_by("#_vrf.id_vrf");
            }



            $where[$pencariankeyword] = trim($params['keyword']);
        } else {

            $tanggal = "date(#_prospek.tanggal_prospek)";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_vrf.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_vrf.updated_date)";
            } else if ($params['jenis_pencarian'] == "vrf_date") {
                $tanggal = "#_vrf.vrf_date";
            }
            $this->db->order_by($tanggal, "desc");
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }




            if (!CheckEmpty($params['status'])) {
                $where['#_vrf.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_supplier'])) {
                $where['#_vrf.id_supplier'] = $params['id_supplier'];
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_vrf.id_customer'] = $params['id_customer'];
            }
            if (!CheckEmpty($params['id_unit'])) {
                $where['#_vrf.id_unit'] = $params['id_unit'];
            }
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }



        $strview .= anchor(site_url('vrf/view_vrf/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $stredit .= anchor(site_url('vrf/edit_vrf/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
            $strclosed .= anchor("", 'Close', array('class' => 'btn btn-info btn-xs', "onclick" => "ubahstatus($1,3);return false;"));
            $strapproved .= anchor("", 'Approved', array('class' => 'btn btn-success btn-xs', "onclick" => "ubahstatus($1,1);return false;"));
            $strrejected .= anchor("", 'Rejected', array('class' => 'btn btn-warning btn-xs', "onclick" => "ubahstatus($1,2);return false;"));
        }
        if ($isdelete) {
            $strbatal .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batal($1);return false;"));
        }
        $this->datatables->add_column('edit', $stredit, 'id');
        $this->datatables->add_column('view', $strview, 'id');
        $this->datatables->add_column('batal', $strbatal, 'id');
        $this->datatables->add_column('rejected', $strrejected, 'id');
        $this->datatables->add_column('closed', $strclosed, 'id');
        $this->datatables->add_column('approved', $strapproved, 'id');
        return $this->datatables->generate();
    }

    // get all
    function GetOneVrf($keyword, $type = 'id_vrf') {
        $this->db->where("#_vrf.id_vrf", $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $this->db->join("#_unit", "#_unit.id_unit=#_vrf.id_unit", "true");
        $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
        $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
        $this->db->join("#_kpu_detail", "#_kpu_detail.id_vrf=#_vrf.id_vrf and #_kpu_detail.status=1", "left");
        $this->db->join("#_kpu", "#_kpu.id_kpu=#_kpu_detail.id_kpu and #_kpu.status=1 and #_kpu.deleted_date is null", "left");
        $this->db->join("#_customer", "#_customer.id_customer=#_vrf.id_customer and #_customer.status=1", "left");
        $this->db->join("#_warna", "#_warna.id_warna=#_vrf.id_colour and #_warna.status=1", "left");
        $this->db->select("#_vrf.*,nama_type_unit,nama_kategori,nama_unit,ifnull(sum(#_kpu_detail.qty),0) as qty_total,#_customer.alamat as alamat_customer,#_customer.no_telp as no_telp_customer");
        $this->db->group_by("#_vrf.id_vrf");
        $vrf = $this->db->get($this->table)->row();

        return $vrf;
    }

    function ChangeManualVRF($id_vrf) {
        $vrf = $this->GetOneVrf($id_vrf);
        if ($vrf->qty_total >= $vrf->qty) {
            $this->db->update("#_vrf", array("status" => 4), array("id_vrf" => $id_vrf));
        } else if ($vrf->status == 4) {
            $this->db->update("#_vrf", array("status" => 1), array("id_vrf" => $id_vrf));
        }
    }

    function VrfManipulate($model) {
        try {
            $message = "";
            $jenis_pencarian = "created_date";
            $vrf['id_unit'] = ForeignKeyFromDb($model['id_unit']);
            $vrf['qty'] = DefaultCurrencyDatabase($model['qty']);
            $vrf['id_colour'] = ForeignKeyFromDb($model['id_colour']);
            $vrf['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $vrf['id_customer'] = ForeignKeyFromDb($model['id_customer']);
            $vrf['dnp'] = DefaultCurrencyDatabase($model['dnp']);
            $vrf['discount'] = DefaultCurrencyDatabase($model['discount']);
            $vrf['dnp_after_discount'] = DefaultCurrencyDatabase($model['dnp_after_discount']);
            $vrf['vrf_date'] = DefaultTanggalDatabase($model['vrf_date']);
            $vrf['add_free_top'] = DefaultCurrencyDatabase($model['add_free_top']);
            $vrf['total'] = $vrf['qty'] * ($vrf['dnp_after_discount'] + $vrf['add_free_top']);
            $vrf['dokumen'] = $model['dokumen'];
            $vrf['top'] = $model['top'];
            $vrf['vin'] = $model['vin'];
            $vrf['free_top_proposed'] = DefaultCurrencyDatabase($model['free_top_proposed']);
            if (CheckEmpty($model['id_vrf'])) {
                $vrf['vrf_code'] = AutoIncrement('#_vrf', 'VRF/' . date("y") . '/', 'vrf_code', 5);
                $vrf['created_date'] = GetDateNow();
                $vrf['status'] = 0;
                $vrf['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $vrf);
                $message = 'Vrf successfull added into database';
            } else {
                $jenis_pencarian = "updated_date";
                $vrf['updated_date'] = GetDateNow();
                $vrf['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $vrf, array("id_vrf" => $model['id_vrf']));
                $message = 'Vrf has been updated';
            }
            ClearCookiePrefix("_vrf", $jenis_pencarian);
            SetMessageSession(1, $message);
            return array("st" => true, "msg" => $message);
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownVrf() {
        $listvrf = GetTableData($this->table, 'id_vrf', 'vrf_code', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listvrf;
    }

    function GetDropDownVrfFromSupplier($params) {

        $where = array($this->table . '.status' => 1, $this->table . '.deleted_date' => null);
        $where['#_vrf.id_supplier'] = @$params['id_supplier'];
        if (!CheckEmpty(@$params['id_unit'])) {
            $where['#_vrf.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_kategori'])) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_type_unit'])) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer'])) {
            $where['#_vrf.id_customer'] = $params['id_customer'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_vrf.id_unit");
        $select = 'DATE_FORMAT(vrf_date, "%d %M %Y") as vrf_date_convert,vrf_code,nama_customer,nama_unit,id_vrf,nama_kategori';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_vrf.id_customer");
        $arrayjoin[] = array("table" => "#_kategori", "condition" => "#_unit.id_kategori=#_kategori.id_kategori");
		$this->db->order_by("vrf_date","desc");
        $listvrf = GetTableData($this->table, 'id_vrf', array('vrf_code', "nama_customer", "nama_kategori", "nama_unit", "vrf_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);

        return $listvrf;
    }

    function VrfDelete($id_vrf) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_vrf' => $id_vrf));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Vrf has been deleted from database");
    }

}
