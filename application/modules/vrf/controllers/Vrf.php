<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vrf extends CI_Controller {

    private $_sufix = "_vrf";

    function __construct() {
        parent::__construct();
        $this->load->model('m_vrf');
        $this->load->model("warna/m_warna");
        $this->load->model("customer/m_customer");
        $this->load->model("supplier/m_supplier");
        $this->load->model("kpu/m_kpu");
        $this->load->model("do_kpu/m_do_kpu");
        $this->load->model("persediaan_unit/m_persediaan_unit");
        $this->load->model("do_prospek/m_do_prospek");
    }

    public function vrf_change_status() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_vrf', 'Vrf', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_vrf->ChangeStatus($model["id_vrf"], $model['status']);
            echo json_encode($data);
        }
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $this->load->model("unit/m_unit");
        $this->load->model("customer/m_customer");

        $module = "K057";
        $header = "K059";
        $title = "Vrf";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $model['list_unit'] = $this->m_unit->GetDropDownUnit();
        $status[] = array();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Approved");
        $status[] = array("id" => "2", "text" => "Rejected");
        $status[] = array("id" => "3", "text" => "Closed");
        $status[] = array("id" => "5", "text" => "Batal");
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "vrf_date", "text" => "Tanggal VRF");
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_vrf", "text" => "NO VRF");
        $jenis_keyword[] = array("id" => "no_kpu", "text" => "NO KPU");
        
        
        $model['start_date'] = GetCookieSetting("start_date" . $this->_sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $this->_sufix, GetDateNow());
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['status'] = GetCookieSetting("status" . $this->_sufix, 0);
        $model['id_unit'] = GetCookieSetting("id_unit" . $this->_sufix, 0);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_sufix, "no_vrf");
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $this->_sufix, "created_date");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_keyword'] = $jenis_keyword;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "vrf/v_vrf_index", $javascript, $css);
    }

    public function get_one_vrf() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_vrf', 'Vrf', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_vrf->GetOneVrf($model["id_vrf"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Vrf tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdatavrf() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_sufix, 1);
            }
            SetCookieSetting($key . $this->_sufix, $value);
        }
        echo$this->m_vrf->GetDatavrf($params);
    }

    public function get_remaining_vrf() {
        header('Content-Type: application/json');
        $model = $this->input->post();
        $this->load->model("vrf/m_vrf");

        $list_vrf = $this->m_vrf->GetDropDownVrfFromSupplier($model);
        array_unshift($list_vrf, array('id' => 0, 'text' => "KPU Manual"));
        echo json_encode(array("list_vrf" => $list_vrf, "st" => true));
    }

    public function create_vrf() {
        $row = ['button' => 'Add'];
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("unit/m_unit");
        $javascript = array();
        $module = "K057";
        $header = "K059";
        $row['free_top_proposed'] = 0;
        $row['title'] = "Add Vrf";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $row['vrf_date'] = GetDateNow();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'vrf/v_vrf_manipulate', $javascript, $css);
    }

    public function vrf_manipulate() {
        $message = '';

        $this->form_validation->set_rules('vrf_date', 'Vrf Date', 'trim|required');
        $this->form_validation->set_rules('id_unit', 'Unit', 'trim|required');
        $this->form_validation->set_rules('vin', 'Vin', 'trim|required');
        $this->form_validation->set_rules('qty', 'Qty', 'trim|required|numeric');
        $this->form_validation->set_rules('id_colour', 'Id Colour', 'trim');
        $this->form_validation->set_rules('dnp', 'Dnp', 'trim|numeric');
        $this->form_validation->set_rules('dnp_after_discount', 'Dnp After Discount', 'trim|numeric');
        $model = $this->input->post();
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_vrf->VrfManipulate($model);
            echo json_encode($status);
        }
    }

    public function edit_vrf($id = 0) {
        $row = $this->m_vrf->GetOneVrf($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K057";
            $header = "K059";
            $row['title'] = "Edit Vrf";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $this->load->model("unit/m_unit");
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'vrf/v_vrf_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Vrf cannot be found in database");
            redirect(site_url('vrf'));
        }
    }

    public function view_vrf($id = 0) {
        $row = $this->m_vrf->GetOneVrf($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K057";
            $header = "K059";
            $row['title'] = "Edit Vrf";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $this->load->model("unit/m_unit");
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
            $row['grid_kpu'] = $this->m_kpu->GetKpuByVrf($id);
            $row['grid_do_kpu'] = $this->m_do_kpu->GetDoKpuByVrf($id);
            $row['grid_unit_sereal'] = $this->m_persediaan_unit->GetUnitSerialByVrf($id);
            $row['grid_do_prospek'] = $this->m_do_prospek->GetDoProspekByVrf($id);
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'vrf/v_vrf_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Vrf cannot be found in database");
            redirect(site_url('vrf'));
        }
    }

    public function vrf_batal() {
        $message = '';
        $this->form_validation->set_rules('id_vrf', 'Vrf', 'required');
        $this->form_validation->set_rules('message', 'Alasan Batal', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_vrf->Batal($model['id_vrf'], $model['message']);
        }

        echo json_encode($result);
    }

}

/* End of file Vrf.php */