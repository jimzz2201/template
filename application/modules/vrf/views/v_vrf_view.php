<section class="content-header">
    <h1>
        VRF <?= @$button ?>
        <small>VRF</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>VRF</li>
        <li class="active">VRF <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_vrf" class="form-horizontal form-groups-bordered validate" method="post" autocomplete="off">
                                <input type="hidden" name="id_vrf" value="<?php echo @$id_vrf; ?>" /> 

                                <h1 style="font-weight: bold;color:red;"><?php
                                    if (@$status == 1) {
                                        echo "Approved";
                                    } else if (@$status == 2) {
                                        echo "Rejected";
                                    } else if (@$status == 3) {
                                        echo "Closed";
                                    } else if (@status == 5) {
                                        echo "Batal";
                                    } else {
                                        echo "Pending";
                                    }
                                    ?></h1>
                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier, array('class' => 'form-control select2', 'id' => 'dd_id_supplier')); ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'vrf_date', 'value' => DefaultDatePicker(@$vrf_date), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Dokumen VRF', "txt_vrf_code", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'dokumen', 'value' => @$dokumen, 'class' => 'form-control', 'id' => 'txt_dokumen', 'placeholder' => 'Dokumen')); ?>
                                    </div>  



                                </div>

                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit', 'class' => 'form-control select2', 'id' => 'dd_id_unit', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_tipe_unit', 'placeholder' => 'Tipe Unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_kategori_unit', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>


                                </div>

                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>
                                    <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'top', 'value' => @$top, 'class' => 'form-control', 'id' => 'txt_top', 'placeholder' => 'Top')); ?>
                                    </div>
                                    <?= form_label('Colour', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_colour', 'class' => 'form-control select2', 'id' => 'dd_id_colour', 'placeholder' => 'Colour'), DefaultEmptyDropdown($list_colour, "json", "Colour"), @$id_colour); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Qty', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'qty', 'value' => DefaultCurrency(@$qty), 'class' => 'form-control', 'id' => 'txt_qty', 'placeholder' => 'Qty', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Dnp', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'dnp', 'value' => DefaultCurrency(@$dnp), 'class' => 'form-control', 'id' => 'txt_dnp', 'placeholder' => 'Dnp', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                    <?= form_label('Discount', "txt_discount", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'discount', 'value' => DefaultCurrency(@$discount), 'class' => 'form-control', 'id' => 'txt_discount', 'placeholder' => 'Discount', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Dnp After Discount', "txt_dnp_after_discount", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'dnp_after_discount', 'value' => DefaultCurrency(@$dnp_after_discount), 'class' => 'form-control', 'id' => 'txt_dnp_after_discount', 'placeholder' => 'Dnp After Discount', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);;HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Free&nbsp;Top&nbsp;Proposed', "txt_free_top_proposed", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array("selected" => @$free_top_proposed, "name" => "free_top_proposed"), array('1' => 'Yes', '0' => 'No'), @$free_top_proposed, array('class' => 'form-control select2', 'id' => 'free_top_proposed')); ?>
                                    </div>
                                    <div id="freetop">
                                        <?= form_label('Add Free Top', "txt_add_free_top", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-3">
                                            <?= form_input(array('type' => 'text', 'name' => 'add_free_top', 'value' => DefaultCurrency(@$add_free_top), 'class' => 'form-control', 'id' => 'txt_add_free_top', 'placeholder' => 'Add Free Top', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Total', "txt_dnp_after_discount", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                <?php 
                                    echo "<pre>";
                                    // print_r($grid_kpu) . '</hr>';
                                    echo "</pre>";
                                ?>
                                <h3 style="font-weight: bold;color:blue;">List KPU</h3>
                                </div>
                                <div class="portlet-body form" id="tbl">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tbody>
                                        <!-- vrf_code, nomor_master, tanggal, dgmi_kpu.keterangan, dgmi_kpu.total, jth_tempo, dgmi_kpu.qty, dgmi_kpu.dnp, dgmi_kpu.discount, dgmi_kpu.vin, dgmi_kpu.top, 
                                        dgmi_kpu.tanggal_pelunasan, dgmi_kpu.nominal_ppn, dgmi_kpu.jumlah_bayar, dgmi_kpu.disc_pembulatan, dgmi_kpu.ppn, dgmi_kpu.grandtotal -->
                                            <tr class="headTable">
                                                <th>No</th>
                                                <th>VRF Code</th>
                                                <th>No KPU</th>
                                                <th width='100'>Tanggal</th>
                                                <th>Keterangan</th>
                                                <th width='100'>Jth Tempo</th>
                                                <th>QTY</th>
                                                <th>Qty Sisa</th>
                                                <th>Total</th>
                                                <th>DNP</th>
                                                <th>Discount</th>
                                                <th>VIN</th>
                                                <th>TOP</th>
                                                <th>Tanggal Pelunasan</th>
                                                <th>Nominal PPN</th>
                                                <th>Jumlah Bayar</th>
                                                <th>Discount Pembulatan</th>
                                                <th>PPN</th>
                                                <th>Grand Total</th>
                                            </tr>
                                            <?php
                                            $num = 1;
                                            foreach($grid_kpu as $row) {
                                                echo "<tr>
                                                        <td>" . $num . "</td>
                                                        <td>" . $row['vrf_code'] . "</td>
                                                        <td>" . $row['nomor_master'] . "</td>
                                                        <td>" . DefaultDatePicker($row['tanggal']) . "</td>
                                                        <td>" . $row['keterangan'] . "</td>
                                                        <td>" . DefaultDatePicker($row['jth_tempo']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['qty']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['qty_sisa']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['total']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['price']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['disc1']) . "</td>
                                                        <td class='right'>" . $row['vin'] . "</td>
                                                        <td class='right'>" . $row['top'] . "</td>
                                                        <td>" . DefaultDatePicker($row['tanggal_pelunasan']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['nominal_ppn']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['jumlah_bayar']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['disc_pembulatan']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['ppn']) . "</td>
                                                        <td class='right'>" . DefaultCurrency($row['grandtotal']) . "</td>
                                                    </tr>";
                                                    $num++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- <div class="form-group">
                                <?php 
                                    echo "<pre></pre>";
                                ?>
                                <h3 style="font-weight: bold;color:blue;">DO KPU</h3>
                                </div>
                                <div class="portlet-body form" id="tbl">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tbody> -->
                                        <!-- vrf_code, nomor_master, tanggal, nomor_do_kpu, tanggal_do, tanggal_terima, free_parking, unit_position, 
                                        qty_do, dgmi_do_kpu.keterangan -->
                                            <!-- <tr class="headTable">
                                                <th>VRF Code</th>
                                                <th>No KPU</th>
                                                <th width='100'>Tanggal</th>
                                                <th>No DO KPU</th>
                                                <th width='100'>Tanggal DO</th>
                                                <th>Tanggal Terima</th>
                                                <th>Free Parking</th>
                                                <th>Unit Position</th>
                                                <th>Qty DO</th>
                                                <th>Keterangan</th>
                                            </tr> -->
                                            <?php
                                            // foreach($grid_do_kpu as $row) {
                                            //     echo "<tr>
                                            //             <td>" . $row['vrf_code'] . "</td>
                                            //             <td>" . $row['nomor_master'] . "</td>
                                            //             <td>" . DefaultDatePicker($row['tanggal']) . "</td>
                                            //             <td>" . $row['nomor_do_kpu'] . "</td>
                                            //             <td>" . DefaultDatePicker($row['tanggal_do']) . "</td>
                                            //             <td>" . DefaultDatePicker($row['tanggal_terima']) . "</td>
                                            //             <td>" . DefaultDatePicker($row['free_parking']) . "</td>
                                            //             <td>" . $row['unit_position'] . "</td>
                                            //             <td class='right'>" . DefaultCurrency($row['qty_do']) . "</td>
                                            //             <td>" . $row['keterangan'] . "</td>
                                            //         </tr>";
                                            // }
                                            ?>
                                        <!-- </tbody>
                                    </table>
                                </div> -->
                                <div class="form-group">
                                <?php 
                                    echo "<pre></pre>";
                                ?>
                                <h3 style="font-weight: bold;color:blue;">DO KPU</h3>
                                </div>
                                <div class="portlet-body form" id="tbl">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tbody>
                                        <!-- nama_unit, date_do_in, date_do_out, nomor_do_kpu, vin_number, dgmi_unit_serial.vin, manufacture_code, engine_no, nama_warna,
                                         dgmi_unit_serial.free_parking, no_bpkb, no_polisi -->
                                            <tr class="headTable">
                                                <th>No</th>
                                                 <th>KPU</th>
                                                <th>Tanggal DO IN</th>
                                                <th>Tanggal DO OUT</th>
                                                <th>No DO KPU</th>
                                                <th>Vin Number</th>
                                                <th>VIN</th>
                                                <th>Engine NO</th>
                                                <th>Free Parking</th>
                                            </tr>
                                            <?php
                                            $num = 1;
                                            foreach($grid_unit_sereal as $row) {
                                                echo "<tr>
                                                        <td>" . $num . "</td>
                                                        <td>" . $row['nomor_master'] . "</td>
                                                        <td>" . DefaultDatePicker($row['date_do_in']) . "</td>
                                                        <td>" . DefaultDatePicker($row['date_out']) . "</td>
                                                        <td>" . $row['nomor_do_kpu'] . "</td>
                                                        <td>" . $row['vin_number'] . "</td>
                                                        <td>" . $row['vin'] . "</td>
                                                        <td>" . $row['engine_no'] . "</td>
                                                        <td>" . DefaultDatePicker($row['free_parking']) . "</td>
                                                    </tr>";
                                                $num++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                <?php 
                                    echo "<pre></pre>";
                                ?>
                                <h3 style="font-weight: bold;color:blue;">DO Prospek</h3>
                                </div>
                                <div class="portlet-body form" id="tbl" style="overflow-x:auto;">
                                    <table class="table table-striped table-bordered table-hover">
                                        <tbody>
                                        <!-- dgmi_do_prospek.tanggal_do, nomor_master, nama_unit, nama_warna, vin_number, dgmi_unit_serial.vin, manufacture_code, engine_no, no_bpkb, no_polisi, alamat, pengemudi -->
                                            <tr class="headTable">
                                                <th>No</th>
                                                <th style="width:100px;">Tangal DO</th>
                                                <th style="width:100px;">Customer</th>
                                                <th>No DO</th>
                                                <th style="width:100px;">Nama Unit</th>
                                                <th>Warna</th>
                                                <th style="width:200px;">VIN Number</th>
                                                <th>VIN</th>
                                                <th>Manufacture Code</th>
                                                <th>Engine NO</th>
                                                <th>No BPKB</th>
                                                <th style="width:100px;">No Polisi</th>
                                                <th>Alamat</th>
                                                <th>Pengemudi</th>
                                            </tr>
                                            <?php
                                            $num = 1;
                                            foreach($grid_do_prospek as $row) {
                                                echo "<tr>
                                                        <td>" . $num . "</td>
                                                        <td>" . DefaultDatePicker($row['tanggal_do']) . "</td>
                                                        <td>" . $row['nama_customer'] . "</td>
                                                        <td>" . $row['nomor_master'] . "</td>
                                                        <td>" . $row['nama_unit'] . "</td>
                                                        <td>" . $row['nama_warna'] . "</td>
                                                        <td>" . $row['vin_number'] . "</td>
                                                        <td>" . $row['vin'] . "</td>
                                                        <td>" . $row['manufacture_code'] . "</td>
                                                        <td>" . $row['engine_no'] . "</td>
                                                        <td>" . $row['no_bpkb'] . "</td>
                                                        <td>" . $row['no_polisi'] . "</td>
                                                        <td>" . $row['alamat'] . "</td>
                                                        <td>" . $row['pengemudi'] . "</td>
                                                    </tr>";
                                                    $num++;
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group col-md-12">
                                    <a href="<?php echo base_url() . 'index.php/vrf' ?>" class="btn btn-default"  >Kembali</a>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style>
    .control-label {
        text-align: left !important;
    }
    .control-label {
        text-align: left !important;
    }
    th {
        text-align: center !important;
    }
    .right {
        text-align: right !important;
    }
    .headTable {
        background-color: #0396B0 !important;
        color: #FFFFFF !important;
    }
</style>

<script>
    $(document).ready(function () {
        $("input").attr("readonly", "readonly");
        $(".select2").select2({disabled: "readonly"});
    })
</script>