<section class="content-header">
    <h1>
        VRF <?= @$button ?>
        <small>VRF</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>VRF</li>
        <li class="active">VRF <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_vrf" class="form-horizontal form-groups-bordered validate" method="post" autocomplete="off">
                                <input type="hidden" name="id_vrf" value="<?php echo @$id_vrf; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier, array('class' => 'form-control select2', 'id' => 'dd_id_supplier')); ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'vrf_date', 'value' => DefaultDatePicker(@$vrf_date), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Dokumen VRF', "txt_vrf_code", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'dokumen', 'value' => @$dokumen, 'class' => 'form-control', 'id' => 'txt_dokumen', 'placeholder' => 'Dokumen')); ?>
                                    </div>  



                                </div>

                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Customer', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_customer', 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_alamat_cust", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat_cust', 'disabled' => 'disabled', 'rows' => '3', 'cols' => '10', 'value' => @$alamat_customer, 'class' => 'form-control', 'id' => 'txt_alamat_cust', 'placeholder' => 'Alamat Customer')); ?>
                                    </div>
                                    <?= form_label('Telp', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'telp_cust', 'disabled' => 'disabled', 'value' => @$no_telp_customer, 'class' => 'form-control', 'id' => 'txt_telp_cust', 'placeholder' => 'Telepon Customer')); ?>
                                    </div>


                                </div>

                                <hr/>
                                
                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit', 'class' => 'form-control select2', 'id' => 'dd_id_unit', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_tipe_unit', 'placeholder' => 'Tipe Unit')); ?>
                                        <input type="hidden" id="id_type_unit" value="">
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_kategori_unit', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>


                                </div>

                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin ? @$vin : date('Y'), 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>
                                    <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'number', 'name' => 'top', 'value' => @$top, 'class' => 'form-control top', 'id' => 'txt_top', 'placeholder' => 'Top')); ?>
                                        <!-- <?//= form_dropdown(array('type' => 'text', 'name' => 'top', 'class' => 'form-control selectTop', 'id' => 'top', 'placeholder' => 'Top'), '', @$top); ?> -->
                                        <!-- <?//= anchor("", 'History', array('class' => 'btn btn-primary', "id" => "showPricelist"))?> -->
                                    </div>
                                    <?= form_label('Colour', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_colour', 'class' => 'form-control', 'id' => 'dd_id_colour', 'placeholder' => 'Colour'), DefaultEmptyDropdown($list_colour, "json", "Colour"), @$id_colour); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Qty', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'qty', 'value' => DefaultCurrency(@$qty), 'class' => 'form-control', 'id' => 'txt_qty', 'placeholder' => 'Qty', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Dnp', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'dnp', 'value' => DefaultCurrency(@$dnp), 'class' => 'form-control', 'id' => 'txt_dnp', 'placeholder' => 'Dnp', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                    <?= form_label('Vrf', "txt_discount", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'discount', 'value' => DefaultCurrency(@$discount), 'class' => 'form-control', 'id' => 'txt_discount', 'placeholder' => 'Discount', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Dnp After Discount', "txt_dnp_after_discount", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'dnp_after_discount', 'value' => DefaultCurrency(@$dnp_after_discount), 'class' => 'form-control', 'id' => 'txt_dnp_after_discount', 'placeholder' => 'Dnp After Discount', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);;HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Free&nbsp;Top&nbsp;Proposed', "txt_free_top_proposed", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_dropdown(array("selected" => @$free_top_proposed, "name" => "free_top_proposed"), array('1' => 'Yes', '0' => 'No'), @$free_top_proposed, array('class' => 'form-control', 'id' => 'free_top_proposed')); ?>
                                    </div>
                                    <div id="freetop">
                                        <?= form_label('Add Free Top', "txt_add_free_top", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-3">
                                            <?= form_input(array('type' => 'text', 'name' => 'add_free_top', 'value' => DefaultCurrency(@$add_free_top), 'class' => 'form-control', 'id' => 'txt_add_free_top', 'placeholder' => 'Add Free Top', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                </div>

                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Total', "txt_dnp_after_discount", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'disabled' => 'disabled', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total')); ?>
                                    </div>

                                </div>
                                <div class="form-group col-md-12">
                                    <a href="<?php echo base_url() . 'index.php/vrf' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("select").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $('.datepicker').datepicker({
            autoclose: true,
            dateFormat: 'yy-mm-dd',
        });
        updatefreetop();
        detailUnit(<?php echo @$id_unit?>);
    });
    function HitungSemua()
    {
        var qty = Number($("#txt_qty").val().replace(/[^0-9\.]+/g, ""));
        var dnp = Number($("#txt_dnp").val().replace(/[^0-9\.]+/g, ""));
        var discount = Number($("#txt_discount").val().replace(/[^0-9\.]+/g, ""));
        var free_top = Number($("#txt_add_free_top").val().replace(/[^0-9\.]+/g, ""));
        var dnpwitoutdiscount = dnp - discount;
        $("#txt_dnp_after_discount").val(CommaWithoutHitungan(dnpwitoutdiscount));
        $("#txt_total").val(CommaWithoutHitungan((dnpwitoutdiscount + free_top) * qty));
    }
    $("#free_top_proposed").change(function () {
        updatefreetop();
    })
    function updatefreetop()
    {
        if ($("#free_top_proposed").val() == "0")
        {
            $("#txt_add_free_top").val(0);
            $("#freetop").css("display", "none");
        } else
        {
            $("#freetop").css("display", "block");
        }
    }
    function detailUnit (id_unit) {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_one_unit',
            dataType: 'json',
            data: {
                id_unit: id_unit
            },
            success: function (data) {
                if (data.st)
                {
                    $("#txt_tipe_unit").val(data.obj.nama_type_unit);
                    $("#txt_kategori_unit").val(data.obj.nama_kategori);
                    $("#id_type_unit").val(data.obj.id_type_unit);
                    selectTop();
                } else
                {
                    $("#txt_tipe_unit").val("");
                    $("#txt_kategori_unit").val("");
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    $("#dd_id_unit").change(function () {
        var id_unit = $(this).val();
        if (id_unit == "0")
        {
            $("#txt_tipe_unit").val("");
            $("#txt_kategori_unit").val("");
        } else
        {
            detailUnit(id_unit);
        }
    });
    $("#dd_id_customer").change(function () {
        var id_customer = $(this).val();
        if (id_customer == "0")
        {
            $("#txt_alamat_cust").val("");
            $("#txt_telp_cust").val("");
        } else
        {
            $.ajax({
                type: 'POST',
                url: baseurl + 'index.php/customer/get_one_customer',
                dataType: 'json',
                data: {
                    id_customer: id_customer
                },
                success: function (data) {
                    if (data.st)
                    {
                        $("#txt_alamat_cust").val(data.obj.alamat);
                        $("#txt_telp_cust").val(data.obj.no_telp);

                    } else
                    {
                        $("#txt_alamat_cust").val("");
                        $("#txt_telp_cust").val("");
                    }

                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        }
    })
    $(".top").on('keyup', function(){
        if($(this).val() == ''){
            $('#txt_dnp').val(0);
            return false;
        }
        selectTop();
    });

    function selectTop() {
        var top = $("#txt_top").val();
        var id_unit = $("#dd_id_unit").val();
        if(isNaN(top) || id_unit == ''){
            return false;
        }

        $.ajax({
            url: baseurl + 'index.php/price_list/getTopPrice',
            dataType: 'json',
            method: 'post',
            data: {
                id_unit: id_unit,
                top: top
            },
            success: function(data) {
                $('#txt_discount').val(CommaWithoutHitungan(data.vrf));
                $('#txt_dnp').val(CommaWithoutHitungan(data.price));
                HitungSemua();
            },
            error: function(xhr, status, error) {
                messageerror(xhr.responeText);
            }
        });
    }
    $("#frm_vrf").submit(function () {
        swal({
            title: "Apakah kamu yakin ingin menginput data VRF berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/vrf/vrf_manipulate',
                    dataType: 'json',
                    data: $(this).serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            window.location.href = baseurl + 'index.php/vrf';
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>