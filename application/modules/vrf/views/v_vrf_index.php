
<section class="content-header">
    <h1>
        Vrf
        <small>Data Master</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Master</li>
        <li class="active">Vrf</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), @$list_pencarian); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                        </div>


                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_supplier', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'selected' => @$status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                        </div>



                    </div>
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_unit', 'selected' => @$id_unit, 'class' => 'form-control select2', 'id' => 'dd_id_unit', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit   ")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_keyword', 'selected' => @$jenis_keyword, 'class' => 'form-control select2', 'id' => 'dd_jenis_keyword', 'placeholder' => 'Jenis Keyword'), @$list_keyword); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                    </div>
                </div>
            </form>
            <hr/>
            <div class=" headerbutton">
                <?php echo anchor(site_url('vrf/create_vrf'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
    
    $("#dd_jenis_keyword").change(function(){
        
        LoadAreaKeyword();
    })
    function LoadAreaKeyword()
    {
       
        if(CheckEmpty($("#dd_jenis_keyword").val()))
        {
            $("#txt_keyword").val("");
            $("#txt_keyword").attr("readonly","readonly");
        }
        else
        {
            $("#txt_keyword").removeAttr("readonly");
        }
    }
    
    
    function batal(id_vrf) {


        swal({
            title: "Are you sure delete this data?",
            text: "You will not be able to recover this data!",
            type: "warning",
            input: 'text',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/vrf/vrf_batal',
                    dataType: 'json',
                    data: {
                        id_vrf: id_vrf,
                        message: result.value
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }

        });


    }
    function ubahstatus(id, status)
    {
        swal({
            title: "Apakah kamu yakin ingin " + (status == "1" ? "mengapprove" : status == "2" ? "Mereject" : "Menutup") + " VRF Berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/vrf/vrf_change_status',
                    dataType: 'json',
                    data: {
                        id_vrf: id,
                        status: status

                    },
                    success: function (data) {
                        if (data.st)
                        {
                            RefreshGrid();
                            messagesuccess(data.msg);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
    }
    function RefreshGrid()
    {
        table.fnDraw(false);
    }
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $(document).ready(function () {
        $(".datepicker").datepicker();
        LoadAreaKeyword();
        $(".select2").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "vrf/getdatavrf", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_vrf",
                    title: "No",
                    orderable: false
                },
                {data: "vrf_code", orderable: true, title: "Kode VRF", width: "100px"},
                {data: "vrf_date", orderable: false, title: "Tanggal",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "nama_supplier", orderable: true, title: "Supplier", width: "200px"},
                {data: "nama_customer", orderable: true, title: "Customer", width: "200px"},
                {data: "nama_unit", orderable: false, title: "Unit"},
                {data: "vin", orderable: false, title: "VIN"},
                {data: "qty", orderable: false, title: "Qty",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "nama_warna", orderable: false, title: "Colour"},

                {data: "dnp_after_discount", orderable: false, title: "DNP After Discount",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "total", orderable: false, title: "Total",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        if (data == 1)
                        {
                            return "Approved";
                        } else if (data == 2)
                        {
                            return "Rejected";
                        } else if (data == 3)
                        {
                            return "Closed";
                        } else if (data == 4)
                        {
                            return "Finished";
                        } else if (data == 5)
                        {
                            return "Batal";
                        } else
                        {
                            return "Pending";
                        }

                    }},
                {
                    "data": "view",
                    "orderable": false,
                    "className": "text-center",
                    width: "250px",
                    mRender: function (data, type, row) {
                        var action = data;
                        if (row['status'] == 0)
                        {
                            action += row['approved'] + row['rejected'] + row['edit'] + row['batal'];
                        } else if (row['status'] == 1)
                        {
                            action += row['closed'] + row['batal'];
                        }
                        return action;
                    }}
            ],
            order: [[0, 'desc']],
            columnDefs: [
                {"width": "20px", "targets": 0},
                {"width": "70px", "targets": 1},
            ],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
