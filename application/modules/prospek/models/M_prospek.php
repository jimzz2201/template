<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_prospek extends CI_Model {

    public $table = '#_prospek';
    public $id = 'id_prospek';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function ApprovalProspek($model, $type) {
        $message = "";
        $st = false;
        $no_prospek = "";
        try {
            $id_prospek = @$model['id_prospek'];
            $prospek = $this->GetOneProspek($id_prospek);
            
            if ($prospek->status == "1") {
                $message = "Data Prospek sudah diapprove<br/>";
            } else if ($prospek->status == "2") {
                $message = "Data Prospek sudah direject<br/>";
            } else if ($prospek->status == "5") {
                $message = "Data Prospek sudah dibatalkan<br/>";
            }
            $statusapproval = 0;
           
            if ($message == "") {
                
                $level = GetLevelJabatan();
                
                if (GetGroupId() == 1) {
                    $level = 3;
                }
                $this->db->trans_rollback();
                $this->db->trans_begin();

                $id_prospek_history = 0;
               

                if ($type == "Approve") {
                    if ($level == 3) {
                        $statusapproval = 1;
                    }
                } else if ($type == "Reject") {
                    $statusapproval = 2;
                } else if ($type == "Nego") {
                    $statusapproval = 3;
                    $level = 1;
                    $this->db->from("#_prospek");
                    $this->db->where(array("id_prospek" => $id_prospek));
                    $prospekhistory = $this->db->get()->row_array();
                    $this->db->insert("#_prospek_history", MergeCreated($prospekhistory));
                    $id_prospek_history = $this->db->insert_id();
                    $this->db->from("#_prospek_detail");
                    $this->db->where(array("id_prospek" => $id_prospek));
                    $prospekhistorydetail = $this->db->get()->row_array();
                    unset($prospekhistorydetail['id_prospek']);
                    $id_prospek_detail = $prospekhistorydetail['id_prospek_detail'];
                    unset($prospekhistorydetail['id_prospek_detail']);
                    $prospekhistorydetail['id_prospek_history'] = $id_prospek_history;
                    $this->db->insert("#_prospek_history_detail", MergeCreated($prospekhistorydetail));
                    $id_prospek_history_detail = $this->db->insert_id();
                    $this->db->from("#_prospek_equipment");
                    $this->db->where(array("id_prospek_detail" => $id_prospek_detail));
                    $list_equipment = $this->db->get()->result_array();
                    foreach ($list_equipment as $equip) {
                        unset($equip['id_add_equipment']);
                        unset($equip['id_prospek_detail']);
                        $equip['id_prospek_history_detail'] = $id_prospek_history_detail;
                        $this->db->insert("#_prospek_history_equipment", MergeCreated($equip));
                    }
                }
                
                $update = [];
                $update['level'] = $level;
                $update['approved_date'] = GetDateNow();
                $update['approved_by'] = GetUserId();
                $update['status'] = $statusapproval;
                $this->db->update("#_prospek", MergeUpdate($update), array("id_prospek" => $model['id_prospek']));
                if($statusapproval==1)
                {
                    $this->ChangeStatus($model['id_prospek']);
                }
                
                


                $approval = [];
                $approval['id_prospek'] = $id_prospek;
                $approval['status_approval'] = $type;
                $approval['nego_value'] = $type == "Nego" ? DefaultCurrencyDatabase($model['nego_value']) : 0;
                $approval['ket_approve'] = $model['ket_approve'];
                $approval['id_prospek_history'] = ForeignKeyFromDb($id_prospek_history);
                $this->db->insert("#_prospek_approval", MergeCreated($approval));
                ClearCookiePrefix("_prospek", "updated_date");
                
                
                if ($this->db->trans_status() === FALSE || $message != '') {
                    $this->db->trans_rollback();
                    $message = "Some Error Occured<br/>" . $message;
                    return array("st" => false, "msg" => $message);
                } else {
                    $this->db->trans_commit();
                    SetMessageSession(true, "Data Prospek Sudah di" . $type . " ke dalam database");
                    $arrayreturn["st"] = true;
                    SetPrint($id_prospek, $no_prospek, 'Prospek');
                    return $arrayreturn;
                }
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
    }

    function GetDropDownProspekfromCustomer($id_customer) {
        $this->db->where(array("id_customer" => $id_customer));
        $listkpu = GetTableData($this->table, 'id_prospek', 'no_prospek', array($this->table . '.status' => 1,$this->table . '.close_status' => null, $this->table . '.deleted_date' => null));
        return $listkpu;
    }

    function GetDropDownProspekForBukaJual($params) {
        $where = array('#_prospek.status' => 1, $this->table . '.deleted_date' => null);

        if (!CheckEmpty(@$params['id_kategori']) || @$params['id_kategori'] > 0) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_unit']) || @$params['id_unit'] > 0) {
            $where['#_prospek_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit']) || @$params['id_type_unit'] > 0) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer']) || @$params['id_customer'] > 0) {
            $where['#_prospek.id_customer'] = $params['id_customer'];
        }
        if (!CheckEmpty(@$params['id_do_kpu']) || @$params['id_do_kpu'] > 0) {
            $where['#_do_kpu.id_do_kpu'] = $params['id_do_kpu'];
        }
        if (!CheckEmpty(@$params['id_kpu']) || @$params['id_kpu'] > 0) {
            $where['#_kpu.id_kpu'] = $params['id_kpu'];
        }

        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_prospek_detail", "condition" => "#_prospek_detail.id_prospek=#_prospek.id_prospek");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_prospek_detail.id_unit");
        $select = 'DATE_FORMAT(#_prospek.tanggal_prospek, "%d %M %Y") as prospek_date_convert,no_prospek,nama_customer,nama_unit,#_prospek.id_prospek';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_prospek.id_customer");
        $listprospek = GetTableData($this->table, 'id_prospek', array('nomor_master', "nama_customer", "nama_unit", "prospek_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listprospek;
    }

    function UpdateStatusProspek($id_prospek) {
        $prospek = $this->GetOneProspek($id_prospek, "#_prospek.id_prospek", true);
        if ($prospek) {
            if (CheckEmpty($prospek->tanggal_pelunasan)) {
                if ($prospek->sisa <= 0) {
                    if (count($prospek->listpembayaran) > 0) {
                        $this->db->update("#_prospek", array("tanggal_pelunasan" => $prospek->listpembayaran[count($prospek->listpembayaran) - 1]->tanggal_transaksi), array("id_prospek" => $id_prospek));
                    }
                }
            } else if (!CheckEmpty($prospek->tanggal_pelunasan)) {
                if ($prospek->sisa > 0) {
                    $this->db->update("#_prospek", array("tanggal_pelunasan" => null), array("id_prospek" => $id_prospek));
                }
            }
        }
    }

    /*
      function ChangeStatus($id_prospek, $status, $keterangan) {
      try {
      $message = "";
      $prospek = $this->GetOneProspek($id_prospek);
      $prospek = json_decode(json_encode($prospek), true);
      $this->load->model("customer/m_customer");
      $customer = $this->m_customer->GetOneCustomer($prospek['id_customer']);
      $this->db->trans_begin();
      $array = array("st" => true, "msg" => "Data Prospek Berhasil " . ($status == "1" ? "Diapprove" : ($status == "2" ? "Direject" : "Dibatalkan")));
      $update = array();
      $update['status'] = $status;
      $update['approved_date'] = GetDateNow();
      $update['approved_by'] = GetUserId();
      $update['updated_by'] = GetUserId();
      $update['updated_date'] = GetDateNow();
      $update['keterangan_reject'] = $keterangan;
      $this->db->from("#_buku_besar");
      $this->db->where(array("id_gl_account" => "15", "dokumen" => $prospek['no_prospek']));
      $akun_penjualan = $this->db->get()->row();
      $prospek['type_pembulatan'] = 1;
      $prospek['ppn'] = 10;
      $prospek['keterangan'] = "";
      $prospek['jumlah_bayar'] = 0;
      $prospek['disc_pembulatan'] = 0;

      $prospek['tanggal'] = $prospek['tanggal_prospek'];
      $userid = GetUserId();
      $totalpenjualanwithppn = $prospek['harga_jual_unit'] * $prospek['jumlah_unit'];
      $prospek['grandtotal'] = $totalpenjualanwithppn;
      $prospek['nominal_ppn'] = 10 / 110 * $totalpenjualanwithppn;
      if (CheckEmpty($akun_penjualan)) {
      $akunpenjualaninsert = array();
      $akunpenjualaninsert['id_gl_account'] = "15";
      $akunpenjualaninsert['tanggal_buku'] = $prospek['tanggal'];
      $akunpenjualaninsert['keterangan'] = "Penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunpenjualaninsert['dokumen'] = $prospek['no_prospek'];
      $akunpenjualaninsert['subject_name'] = @$customer->kode_customer;
      $akunpenjualaninsert['id_subject'] = $prospek['id_customer'];
      $akunpenjualaninsert['debet'] = 0;
      $akunpenjualaninsert['kredit'] = DefaultCurrencyDatabase($prospek['type_pembulatan'] == "1" ? $totalpenjualanwithppn * 100 / (100 + $prospek['ppn']) : $totalpenjualanwithoutppn, 0);
      $akunpenjualaninsert['created_date'] = GetDateNow();
      $akunpenjualaninsert['created_by'] = $userid;
      $this->db->insert("#_buku_besar", $akunpenjualaninsert);
      } else {
      $akunpenjualanupdate['tanggal_buku'] = $prospek['tanggal'];
      $akunpenjualanupdate['keterangan'] = "Penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunpenjualanupdate['dokumen'] = $prospek['no_prospek'];
      $akunpenjualanupdate['subject_name'] = @$customer->kode_customer;
      $akunpenjualanupdate['id_subject'] = $prospek['id_customer'];
      $akunpenjualanupdate['debet'] = 0;
      $akunpenjualanupdate['kredit'] = DefaultCurrencyDatabase($prospek['type_pembulatan'] == "1" ? $totalpenjualanwithppn * 100 / (100 + $prospek['ppn']) : $totalpenjualanwithoutppn, 0);
      $akunpenjualanupdate['updated_date'] = GetDateNow();
      $akunpenjualanupdate['updated_by'] = $userid;
      $this->db->update("#_buku_besar", $akunpenjualanupdate, array("id_buku_besar" => $akun_penjualan->id_buku_besar));
      }

      $this->db->from("#_buku_besar");
      $this->db->where(array("id_gl_account" => "20", "dokumen" => $prospek['no_prospek']));
      $akun_ppn = $this->db->get()->row();

      if (CheckEmpty($akun_ppn) && $prospek['nominal_ppn'] > 0) {
      $akunppninsert = array();
      $akunppninsert['id_gl_account'] = "20";
      $akunppninsert['tanggal_buku'] = $prospek['tanggal'];
      $akunppninsert['keterangan'] = "PPN penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunppninsert['dokumen'] = $prospek['no_prospek'];
      $akunppninsert['subject_name'] = @$customer->kode_customer;
      $akunppninsert['id_subject'] = $prospek['id_customer'];
      $akunppninsert['debet'] = 0;
      $akunppninsert['kredit'] = $prospek['nominal_ppn'];
      $akunppninsert['created_date'] = GetDateNow();
      $akunppninsert['created_by'] = $userid;
      $this->db->insert("#_buku_besar", $akunppninsert);
      } else if (!CheckEmpty($akun_ppn)) {
      $akunppnupdate['tanggal_buku'] = $prospek['tanggal'];
      $akunppnupdate['keterangan'] = "PPN penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunppnupdate['dokumen'] = $prospek['no_prospek'];
      $akunppnupdate['subject_name'] = @$customer->kode_customer;
      $akunppnupdate['id_subject'] = $prospek['id_customer'];
      $akunppnupdate['debet'] = 0;
      $akunppnupdate['kredit'] = $prospek['nominal_ppn'];
      $akunppnupdate['updated_date'] = GetDateNow();
      $akunppnupdate['updated_by'] = $userid;
      $this->db->update("#_buku_besar", $akunppnupdate, array("id_buku_besar" => $akun_ppn->id_buku_besar));
      }

      $this->db->from("#_buku_besar");
      $this->db->where(array("id_gl_account" => "4", "dokumen" => $prospek['no_prospek']));
      $akun_piutang = $this->db->get()->row();

      if (CheckEmpty($akun_piutang) && $prospek['jumlah_bayar'] < $prospek['grandtotal']) {
      $akunpiutanginsert = array();
      $akunpiutanginsert['id_gl_account'] = "4";
      $akunpiutanginsert['tanggal_buku'] = $prospek['tanggal'];
      $akunpiutanginsert['keterangan'] = "Piutang penjualan dari " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunpiutanginsert['dokumen'] = $prospek['no_prospek'];
      $akunpiutanginsert['subject_name'] = @$customer->kode_customer;
      $akunpiutanginsert['id_subject'] = $prospek['id_customer'];
      $akunpiutanginsert['debet'] = $prospek['grandtotal'] - $prospek['jumlah_bayar'];
      $akunpiutanginsert['kredit'] = 0;
      $akunpiutanginsert['created_date'] = GetDateNow();
      $akunpiutanginsert['created_by'] = $userid;
      $this->db->insert("#_buku_besar", $akunpiutanginsert);
      } else if (!CheckEmpty($akun_piutang)) {
      $akunpiutangupdate['tanggal_buku'] = $prospek['tanggal'];
      $akunpiutangupdate['keterangan'] = "Piutang penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunpiutangupdate['dokumen'] = $prospek['no_prospek'];
      $akunpiutangupdate['subject_name'] = @$customer->kode_customer;
      $akunpiutangupdate['id_subject'] = $prospek['id_customer'];
      $akunpiutangupdate['debet'] = $prospek['grandtotal'] - $prospek['jumlah_bayar'];
      $akunpiutangupdate['kredit'] = 0;
      $akunpiutangupdate['updated_date'] = GetDateNow();
      $akunpiutangupdate['updated_by'] = $userid;
      $this->db->update("#_buku_besar", $akunpiutangupdate, array("id_buku_besar" => $akun_piutang->id_buku_besar));
      }

      $this->m_customer->KoreksiSaldoPiutang($prospek['id_customer']);

      $this->db->from("#_buku_besar");
      $this->db->where(array("id_gl_account" => "16", "dokumen" => $prospek['no_prospek']));
      $akun_potongan = $this->db->get()->row();

      if (CheckEmpty($akun_potongan) && $prospek['disc_pembulatan'] > 0) {
      $akunpotonganinsert = array();
      $akunpotonganinsert['id_gl_account'] = "16";
      $akunpotonganinsert['tanggal_buku'] = $prospek['tanggal'];
      $akunpotonganinsert['keterangan'] = "Potongan penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunpotonganinsert['dokumen'] = $prospek['no_prospek'];
      $akunpotonganinsert['subject_name'] = @$customer->kode_customer;
      $akunpotonganinsert['id_subject'] = $prospek['id_customer'];
      $akunpotonganinsert['debet'] = $prospek['disc_pembulatan'];
      $akunpotonganinsert['kredit'] = 0;
      $akunpotonganinsert['created_date'] = GetDateNow();
      $akunpotonganinsert['created_by'] = $userid;
      $this->db->insert("#_buku_besar", $akunpotonganinsert);
      } else if (!CheckEmpty($akun_potongan)) {
      $akunpotonganupdate['tanggal_buku'] = $prospek['tanggal'];
      $akunpotonganupdate['keterangan'] = "Potongan penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunpotonganupdate['dokumen'] = $prospek['no_prospek'];
      $akunpotonganupdate['subject_name'] = @$customer->kode_customer;
      $akunpotonganupdate['id_subject'] = $prospek['id_customer'];
      $akunpotonganupdate['debet'] = $prospek['disc_pembulatan'];
      $akunpotonganupdate['kredit'] = 0;
      $akunpotonganupdate['updated_date'] = GetDateNow();
      $akunpotonganupdate['updated_by'] = $userid;
      $this->db->update("#_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
      }

      $this->db->from("#_buku_besar");
      $this->db->where(array("id_gl_account" => "3", "dokumen" => $prospek['no_prospek']));
      $akun_kas = $this->db->get()->row();

      if (CheckEmpty($akun_kas) && $prospek['jumlah_bayar'] > 0) {
      $akunkasinsert = array();
      $akunkasinsert['id_gl_account'] = "3";
      $akunkasinsert['tanggal_buku'] = $prospek['tanggal'];
      $akunkasinsert['keterangan'] = "Pengeluaran kas untuk penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunkasinsert['dokumen'] = $prospek['no_prospek'];
      $akunkasinsert['subject_name'] = @$customer->kode_customer;
      $akunkasinsert['id_subject'] = $prospek['id_customer'];
      $akunkasinsert['debet'] = $prospek['jumlah_bayar'];
      $akunkasinsert['kredit'] = 0;
      $akunkasinsert['created_date'] = GetDateNow();
      $akunkasinsert['created_by'] = $userid;
      $this->db->insert("#_buku_besar", $akunkasinsert);
      } else if (!CheckEmpty($akun_kas)) {
      $akunkasupdate['tanggal_buku'] = $prospek['tanggal'];
      $akunkasupdate['keterangan'] = "Pengeluaran kas untuk Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akunkasupdate['dokumen'] = $prospek['no_prospek'];
      $akunkasupdate['subject_name'] = @$customer->kode_customer;
      $akunkasupdate['id_subject'] = $prospek['id_customer'];
      $akunkasupdate['debet'] = $prospek['jumlah_bayar'];
      $akunkasupdate['kredit'] = 0;
      $akunkasupdate['updated_date'] = GetDateNow();
      $akunkasupdate['updated_by'] = $userid;
      $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
      }

      $this->db->from("#_buku_besar");
      $this->db->where(array("dokumen" => $prospek['no_prospek'], "id_gl_account !=" => 38));
      $this->db->select("sum(debet)-sum(kredit) as selisih");
      $row = $this->db->get()->row();
      $this->db->from("#_buku_besar");
      $this->db->where(array("id_gl_account" => "38", "dokumen" => $prospek['no_prospek']));
      $akun_penyeimbang = $this->db->get()->row();

      if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
      $akun_penyeimbanginsert = array();
      $akun_penyeimbanginsert['id_gl_account'] = "38";
      $akun_penyeimbanginsert['tanggal_buku'] = $prospek['tanggal'];
      $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk penjualan Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akun_penyeimbanginsert['dokumen'] = $prospek['no_prospek'];
      $akun_penyeimbanginsert['subject_name'] = @$customer->kode_customer;
      $akun_penyeimbanginsert['id_subject'] = $prospek['id_customer'];
      $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
      $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
      $akun_penyeimbanginsert['created_date'] = GetDateNow();
      $akun_penyeimbanginsert['created_by'] = $userid;
      $this->db->insert("#_buku_besar", $akun_penyeimbanginsert);
      } else if (!CheckEmpty($akun_penyeimbang)) {
      $akun_penyeimbangupdate['tanggal_buku'] = $prospek['tanggal'];
      $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk Ke " . @$customer->nama_customer . ' ' . $prospek['keterangan'];
      $akun_penyeimbangupdate['dokumen'] = $prospek['no_prospek'];
      $akun_penyeimbangupdate['subject_name'] = @$customer->kode_customer;
      $akun_penyeimbangupdate['id_subject'] = $prospek['id_customer'];
      $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
      $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
      $akun_penyeimbangupdate['updated_date'] = GetDateNow();
      $akun_penyeimbangupdate['updated_by'] = $userid;
      $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
      }

      $update['level'] = GetLevelJabatan();
      $update['status'] = (GetLevelJabatan() == 3 ? 4 : $status);
      $this->db->update("#_prospek", $update, array("id_prospek" => $id_prospek));
      if ($this->db->trans_status() === FALSE || $message != '') {
      $this->db->trans_rollback();
      $message = "Some Error Occured<br/>" . $message;
      return array("st" => false, "msg" => $message);
      } else {
      $this->db->trans_commit();
      SetMessageSession(true, "Data KPU Sudah di" . (CheckEmpty(@$model['id_prospek']) ? "masukkan" : "update") . " ke dalam database");
      SetPrint($id_prospek, $prospek['no_prospek'], 'pembelian');
      //create history
      $data = $this->db->where('id_prospek', $id_prospek)->get('#_prospek')->row_array();
      $sumEquipment = $this->db->select_sum('harga')->where('id_prospek', $id_prospek)->get('#_prospek_equipment')->row();
      $_stat = ($data['status'] == 1 ? 'approve' : 'reject');
      $this->createHistory($data, empty($sumEquipment->harga) ? 0 : $sumEquipment->harga, $_stat);
      return $array;
      }
      } catch (Exception $ex) {
      $this->db->trans_rollback();
      return array("st" => false, "msg" => $ex->getMessage());
      }
      }

     */

    function ChangeStatus($id_prospek) {
        $message = "";
        $prospek = $this->GetOneProspek($id_prospek);
        $prospek = json_decode(json_encode($prospek), true);
        $this->load->model("customer/m_customer");
        $customer = $this->m_customer->GetOneCustomer($prospek['id_customer']);
        $userid = GetUserId();
        if ($prospek['status'] == 1) {
            $this->db->from("#_prospek_detail_unit");
            $this->db->where("id_prospek_detail", $prospek['id_prospek_detail']);
            $jumlahdbunit = $this->db->get()->num_rows();

            $jumlahcreate = $prospek['jumlah_unit'];
            if ($jumlahdbunit > $jumlahcreate) {
                $this->db->delete("#_prospek_detail_unit", array("id_prospek_detail" => $prospek['id_prospek_detail'], "id_unit_serial" => null));
            }
            $this->db->from("#_prospek_detail_unit");
            $this->db->where("id_prospek_detail", $prospek['id_prospek_detail']);
            $jumlahdbunit = $this->db->get()->num_rows();
            $jumlahcreate = $jumlahcreate - $jumlahdbunit;


            for ($i = 0; $i < $jumlahcreate; $i++) {
                $unitserial = [];
                $unitserial['id_prospek_detail'] = $prospek['id_prospek_detail'];
                $unitserial['is_batal'] = 0;
                $unitserial['created_date'] = GetDateNow();
                $unitserial['created_by'] = GetUserId();
                $this->db->insert("#_prospek_detail_unit", $unitserial);
            }
        }
    }

    // datatables
    function GetDataprospek($params) {

        $userid = GetUserId();
        $this->load->model("cabang/m_cabang");
        $isedit = false;
        if (CekModule("K112", false)) {
            $isedit = true;
        }
        $listcabang = $this->m_cabang->GetDropDownCabang("json", $userid);
        $listid = array_column($listcabang, "id");
        $this->load->library('datatables');
        $this->datatables->select('#_prospek.*,group_concat(#_jabatan.nama_jabatan separator " , ") as nama_jabatan_approval,group_concat(distinct #_buka_jual.nomor_buka_jual separator " , ") as nomor_buka_jual,nama_cabang,group_concat(current.nama_jabatan separator " , ") as nama_jabatan_current,#_prospek_detail.jumlah_unit,#_prospek_detail.tahun,#_prospek_detail.harga_jual_unit,#_prospek.id_prospek as id,nama_kategori,nama_type_unit,nama_warna,nama_payment_method,nama_pegawai,nama_customer,nama_jenis_plat,nama_type_body,nama_unit');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        $this->datatables->join("#_prospek_detail", "#_prospek_detail.id_prospek=#_prospek.id_prospek");
        //add this line for join
        $this->datatables->join('#_customer', '#_prospek.id_customer = #_customer.id_customer');
        $this->datatables->join('#_buka_jual', '#_buka_jual.id_prospek = #_prospek.id_prospek and #_buka_jual.status!=5', "left");

        $this->datatables->join('#_jenis_plat', '#_prospek_detail.id_jenis_plat = #_jenis_plat.id_jenis_plat', "left");
        $this->datatables->join('#_cabang', '#_prospek.id_cabang = #_cabang.id_cabang', "left");
        $this->datatables->join('#_unit', '#_prospek_detail.id_unit = #_unit.id_unit', "left");
        $this->datatables->join('#_type_body', '#_unit.id_type_body = #_type_body.id_type_body', "left");
        $this->datatables->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit', "left");
        $this->datatables->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori', "left");
        $this->datatables->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna', 'left');
        $this->datatables->join('#_payment_method', '#_prospek.id_payment_method = #_payment_method.id_payment_method', 'left');
        $this->datatables->join('#_pegawai', '#_pegawai.id_pegawai = #_prospek.id_pegawai', 'left');
        $this->datatables->join("#_jabatan", "#_jabatan.level=(#_prospek.level+1)", "left");
        $this->datatables->join("#_jabatan current", "current.level=(#_prospek.level)", "left");

        $this->datatables->group_by("#_prospek.id_prospek");
        $where = [];
        $extra = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $where['#_prospek.no_prospek'] = $params['keyword'];
        } else {

            $tanggal = "date(#_prospek.tanggal_prospek)";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_prospek.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_prospek.updated_date)";
            }
            $this->db->order_by($tanggal, "desc");


            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }
            if (!CheckEmpty($params['status'])) {
                if ($params['status'] < 4) {
                    $where['#_prospek.status'] = $params['status'];
                } else if ($params['status'] == "4") {
                    $where['#_prospek.close_status'] = 1;
                } else if ($params['status'] == 5) {
                    $where['#_prospek.status'] = 5;
                } else if ($params['status'] == 6) {
                    $where['#_prospek.status'] = 0;
                } else if ($params['status'] == 7) {
                    $where['#_prospek.status'] = 1;
                    $where['#_buka_jual.id_buka_jual !='] = null;
                } else if ($params['status'] == 8) {
                    $where['#_prospek.status'] = 1;
                    $where['#_buka_jual.id_buka_jual'] = null;
                }
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_prospek.id_customer'] = $params['id_customer'];
            }
            if (!CheckEmpty($params['id_cabang'])) {
                $where['#_prospek.id_cabang'] = $params['id_cabang'];
            }
            if (!CheckEmpty($params['level'])) {
                $where['#_prospek.level'] = $params['level'];
            }
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }

        if (CheckEmpty(@$model['id_cabang'])) {
            $this->datatables->where_in("#_prospek.id_cabang", $listid);
        }
        $strrevisi = "";
        if ($isedit) {
            $strrevisi = anchor(site_url('prospek/revisi_prospek/$1'), 'Revisi', array('class' => 'btn btn-warning btn-xs'));
        }
        $strview = anchor(site_url('prospek/view_prospek/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
        $stredit = anchor(site_url('prospek/edit_prospek/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        $strapproval = anchor(site_url('prospek/approval_prospek/$1'), 'Approval', array('class' => 'btn btn-success btn-xs'));
        //$strrejected = anchor("", 'Rejected', array('class' => 'btn btn-warning btn-xs', "onclick" => "ubahstatus($1,2);return false;"));
        //$strnego = anchor("", 'Nego', array('class' => 'btn btn-info btn-xs', "onclick" => "negoprospek($1,3);return false;"));
        $strbatal = anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalprospek($1);return false;"));

        $this->datatables->add_column('edit', $stredit, 'id');
        $this->datatables->add_column('view', $strview, 'id');
        $this->datatables->add_column('batal', $strbatal, 'id');
        $this->datatables->add_column('revisi', $strrevisi, 'id');
        //$this->datatables->add_column('rejected', $strrejected, 'id');
        //$this->datatables->add_column('nego', $strnego, 'id');
        $this->datatables->add_column('approval', $strapproval, 'id');

        return $this->datatables->generate();
    }

    function GetDataprospek_api($params, $limit, $from, $count = false) {
        $this->db->select('#_prospek.*,group_concat(#_jabatan.nama_jabatan separator " , ") as nama_jabatan_approval,group_concat(current.nama_jabatan separator " , ") as nama_jabatan_current,#_prospek_detail.jumlah_unit,#_prospek_detail.tahun,#_prospek_detail.harga_jual_unit,#_prospek.id_prospek as id,nama_kategori,nama_type_unit,nama_warna,nama_payment_method,nama_pegawai,nama_customer,nama_jenis_plat,nama_type_body,nama_unit');
        // $this->db->from($this->table);
        $this->db->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->db->join("#_prospek_detail", "#_prospek_detail.id_prospek=#_prospek.id_prospek");
        //add this line for join
        $this->db->join('#_customer', '#_prospek.id_customer = #_customer.id_customer');
        $this->db->join('#_jenis_plat', '#_prospek_detail.id_jenis_plat = #_jenis_plat.id_jenis_plat');
        $this->db->join('#_unit', '#_prospek_detail.id_unit = #_unit.id_unit');
        $this->db->join('#_type_body', '#_unit.id_type_body = #_type_body.id_type_body');
        $this->db->join('#_type_unit', '#_unit.id_type_unit = #_type_unit.id_type_unit');
        $this->db->join('#_kategori', '#_unit.id_kategori = #_kategori.id_kategori');
        $this->db->join('#_warna', '#_prospek_detail.id_warna = #_warna.id_warna');
        $this->db->join('#_payment_method', '#_prospek.id_payment_method = #_payment_method.id_payment_method');
        $this->db->join('#_pegawai', '#_pegawai.id_pegawai = #_prospek.id_pegawai', 'left');
        $this->db->join("#_jabatan", "#_jabatan.level=(#_prospek.level+1)", "left");
        $this->db->join("#_jabatan current", "current.level=(#_prospek.level)", "left");
        $where = [];
        $this->db->group_by("#_prospek.id_prospek");
        $extra = [];
        if ($params['keyword'] != '') {
            // $where['#_prospek.no_prospek'] = $params['keyword'];
            $this->db->like('no_prospek', $params['keyword']);
            $this->db->or_like('nama_customer', $params['keyword']);
        } else {

            if (!CheckEmpty($params['status'])) {
                $where['#_prospek.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_prospek.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $this->db->where($where);
        }
        if (count($extra)) {
            $this->db->where(implode(" AND ", $extra));
        }

        if ($count) {
            return $this->db->get($this->table)->result();
        } else {

            $this->db->limit($limit, $from);
            return $this->db->get($this->table)->result();
        }
    }

    function getDataProspekSelect() {
        $this->load->library('datatables');
        $this->datatables->select('id_prospek, no_prospek, nama_customer, nama_user');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_customer', '#_prospek.id_customer = #_customer.id_customer');
        $this->datatables->join('#_user', '#_user.id_user = #_prospek.id_user', 'left');
        $isselect = true;
        $straction = '';
        if ($isselect) {
            $straction .= anchor('', 'Pilih', array('class' => 'btn btn-primary btn-xs', "onclick" => "pilihProspek($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_prospek');
        return $this->datatables->generate();
    }

    // get all
    function GetOneProspek($keyword, $type = '#_prospek.id_prospek', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $this->db->join("#_prospek_detail", "#_prospek.id_prospek=#_prospek_detail.id_prospek", "left");
        if ($isfull) {
            $this->db->join("#_unit", "#_prospek_detail.id_unit=#_unit.id_unit", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
            $this->db->join("#_kategori", "#_unit.id_kategori=#_kategori.id_kategori", "left");
            $this->db->join("#_type_body", "#_prospek_detail.id_type_body=#_type_body.id_type_body", "left");
            $this->db->join("#_warna", "#_prospek_detail.id_warna=#_warna.id_warna", "left");
            $this->db->join("#_customer", "#_prospek.id_customer=#_customer.id_customer", "left");
            $this->db->join("#_cabang", "#_prospek.id_cabang=#_cabang.id_cabang", "left");
            $this->db->join("#_jenis_customer", "#_jenis_customer.id_jenis_customer=#_jenis_customer.id_jenis_customer", "left");
            $this->db->join("#_jenis_plat", "#_jenis_plat.id_jenis_plat=#_prospek_detail.id_jenis_plat", "left");
            $this->db->join("#_payment_method", "#_payment_method.id_payment_method=#_prospek.id_payment_method", "left");
            $this->db->join("#_pegawai", "#_pegawai.id_pegawai=#_prospek.id_pegawai", "left");
            $this->db->join("#_user", "#_user.id_user=#_prospek.approved_by", "left");
            $this->db->join("#_leasing", "#_prospek.id_leasing = #_leasing.id_leasing", "left");
            $this->db->select("#_prospek.*,#_prospek_detail.id_type_body,#_prospek_detail.id_warna_karoseri,#_prospek_detail.tahun,#_prospek_detail.harga_off_the_road,#_prospek_detail.harga_on_the_road,#_prospek_detail.biaya_bbn,#_prospek_detail.id_unit,#_prospek_detail.id_jenis_plat,#_prospek_detail.id_warna,#_prospek_detail.harga_jual_unit,#_prospek_detail.jumlah_unit,#_prospek_detail.id_prospek_detail,nama_unit,nama_type_unit,nama_kategori,nama_type_body,nama_warna,#_prospek_detail.total_equipment,nama_customer,no_telp,no_telp_2,#_customer.alamat,npwp,nama_npwp,
                                alamat_npwp,#_customer.contact_person as contact_person_cust,email,fax,nama_jenis_customer,nama_cabang, nama_jenis_plat, nama_payment_method, nama_pegawai,
                                #_prospek.id_cabang, #_prospek.level, #_user.nama_user as approved_name, nama_leasing");
        }
        $prospek = $this->db->get($this->table)->row();

        if ($isfull && $prospek) {

            $this->db->from("#_pembayaran_piutang_detail");
            $this->db->join("#_pembayaran_piutang_master", "#_pembayaran_piutang_master.pembayaran_piutang_id=#_pembayaran_piutang_detail.pembayaran_piutang_id", "left");
			$this->db->join("#_unit_serial", "#_unit_serial.id_unit_serial=#_pembayaran_piutang_detail.id_unit_serial", "left");
            $this->db->where(array("id_prospek" => $prospek->id_prospek, "#_pembayaran_piutang_master.status !=" => 2));
            $this->db->order_by("#_pembayaran_piutang_master.tanggal_transaksi", "asc");
            $listpembayaran = $this->db->get()->result();
            $jumlah = 0;
            foreach ($listpembayaran as $pembayaran) {
                $jumlah += $pembayaran->jumlah_bayar;
            }
            $this->db->from("#_prospek_equipment");
            $this->db->join("#_unit", "#_unit.id_unit=#_prospek_equipment.id_unit");
            $this->db->select("#_unit.*,#_prospek_equipment.is_ppn_detail,#_prospek_equipment.harga,#_prospek_equipment.id_add_equipment,#_prospek_equipment.keterangan_detail");
            $this->db->where(array("id_prospek_detail" => $prospek->id_prospek_detail));
            $listequipment = $this->db->get()->result();
            $prospek->list_equipment = $listequipment;
            $this->db->from("#_do_prospek_detail");
            $this->db->join("#_do_prospek", "#_do_prospek.id_do_prospek=#_do_prospek_detail.id_do_prospek");
            $this->db->where(array("id_prospek" => $prospek->id_prospek, "status" => 1));
            $this->db->select("ifnull(sum(#_do_prospek_detail.qty_do),0) as jumlah");
            $rowjumlah = $this->db->get()->row();
            if ($rowjumlah) {
                $prospek->qty_kirim = $rowjumlah->jumlah;
            } else {
                $prospek->qty_kirim = 0;
            }

            $this->db->from("#_buka_jual");
            $this->db->where(array("id_prospek" => $prospek->id_prospek, "status !=" => 2,"status !=" => 5));
            $this->db->select("ifnull(sum(qty_buka_jual),0) as jumlah");
            $rowjumlah = $this->db->get()->row();
            if ($rowjumlah) {
                $prospek->qty_buka_jual = $rowjumlah->jumlah;
            } else {
                $prospek->qty_buka_jual = 0;
            }

            $this->db->from("#_prospek_approval");
            $this->db->where(array("id_prospek" => $prospek->id_prospek));
            $this->db->join("#_user", "#_user.id_user=#_prospek_approval.created_by", "left");
            $prospek->history_approval = $this->db->get()->result();
            $this->load->model("module/m_module");
            $prospek->listimage = $this->m_module->GetFile("Prospek", $prospek->id_prospek);
            $this->db->from("#_prospek");
            $this->db->join("#_pembayaran_piutang_detail", "#_pembayaran_piutang_detail.id_prospek=#_prospek.id_prospek", "left");
            $this->db->join("#_pembayaran_piutang_master", "#_pembayaran_piutang_master.pembayaran_piutang_id=#_pembayaran_piutang_detail.pembayaran_piutang_id", "left");
            $this->db->where(array("#_prospek.id_customer" => $prospek->id_customer, "#_prospek.id_prospek !=" => $prospek->id_prospek, "tanggal_pelunasan" => null, "#_prospek.status" => 1));
            $this->db->select("#_prospek.id_prospek,#_prospek.no_prospek,#_prospek.tanggal_prospek,#_prospek.grandtotal,ifnull(sum(if(#_pembayaran_piutang_master.status=2,0,#_pembayaran_piutang_detail.jumlah_bayar)),0) as terbayar");
            $this->db->group_by("#_prospek.id_prospek");
            $prospek->listar = $this->db->get()->result();



            $prospek->sisa = ($prospek->harga_jual_unit * $prospek->jumlah_unit) - $jumlah;
            $prospek->terbayar = $jumlah;
            $prospek->listpembayaran = $listpembayaran;

            $this->db->from("#_prospek");
            $this->db->join("#_buka_jual", "#_buka_jual.id_prospek=#_prospek.id_prospek");
            $this->db->join("#_unit_serial", "#_unit_serial.id_buka_jual=#_buka_jual.id_buka_jual");
            $this->db->where("#_buka_jual.id_prospek", $prospek->id_prospek);
            $this->db->group_by("#_buka_jual.id_buka_jual");
            $this->db->select("#_buka_jual.id_buka_jual as id,nomor_buka_jual as text");
            $prospek->list_buka_jual = $this->db->get()->result();
            $this->db->from("#_unit_serial");
            $this->db->join("#_buka_jual", "#_unit_serial.id_buka_jual=#_buka_jual.id_buka_jual");
            $this->db->join("#_unit", "#_unit.id_unit=#_buka_jual.id_unit");
            $this->db->where("#_buka_jual.id_prospek", $prospek->id_prospek);
            $this->db->select("concat(nama_unit,' - vin : ',vin_number,' - engine no : ',engine_no) as text,id_unit_serial as id");
            $prospek->list_unit = $this->db->get()->result();
        }

        return $prospek;
    }

    function GetOneProspekHistory($keyword, $type = '#_prospek_history.id_prospek_history', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->where('#_prospek_history.deleted_date', null);
        $this->db->join("#_prospek_history_detail", "#_prospek_history.id_prospek_history=#_prospek_history_detail.id_prospek_history", "left");
        if ($isfull) {
            $this->db->join("#_unit", "#_prospek_history_detail.id_unit=#_unit.id_unit", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
            $this->db->join("#_kategori", "#_unit.id_kategori=#_kategori.id_kategori", "left");
            $this->db->join("#_type_body", "#_unit.id_type_body=#_type_body.id_type_body", "left");
            $this->db->join("#_warna", "#_prospek_history_detail.id_warna=#_warna.id_warna", "left");
            $this->db->join("#_customer", "#_prospek_history.id_customer=#_customer.id_customer", "left");
            $this->db->join("#_cabang", "#_prospek_history.id_cabang=#_cabang.id_cabang", "left");
            $this->db->join("#_jenis_customer", "#_jenis_customer.id_jenis_customer=#_jenis_customer.id_jenis_customer", "left");
            $this->db->join("#_jenis_plat", "#_jenis_plat.id_jenis_plat=#_prospek_history_detail.id_jenis_plat", "left");
            $this->db->join("#_payment_method", "#_payment_method.id_payment_method=#_prospek_history.id_payment_method", "left");
            $this->db->join("#_pegawai", "#_pegawai.id_pegawai=#_prospek_history.id_pegawai", "left");
            $this->db->join("#_user", "#_user.id_user=#_prospek_history.approved_by", "left");
            $this->db->join("#_leasing", "#_prospek_history.id_leasing = #_leasing.id_leasing", "left");
            $this->db->select("#_prospek_history.*,#_prospek_history_detail.tahun,#_prospek_history_detail.harga_off_the_road,#_prospek_history_detail.harga_on_the_road,#_prospek_history_detail.biaya_bbn,#_prospek_history_detail.id_unit,#_prospek_history_detail.id_jenis_plat,#_prospek_history_detail.id_warna,#_prospek_history_detail.harga_jual_unit,#_prospek_history_detail.jumlah_unit,#_prospek_history_detail.id_prospek_history_detail,nama_unit,nama_type_unit,nama_kategori,nama_type_body,nama_warna,nama_customer,no_telp,no_telp_2,#_customer.alamat,npwp,nama_npwp,
                                alamat_npwp,#_customer.contact_person as contact_person_cust,email,fax,nama_jenis_customer,nama_cabang, nama_jenis_plat, nama_payment_method, nama_pegawai,
                                #_prospek_history.id_cabang, #_prospek_history.level, #_user.nama_user as approved_name, nama_leasing");
        }
        $prospek = $this->db->get("#_prospek_history")->row();


        if ($isfull && $prospek) {

            $this->db->from("#_pembayaran_piutang_detail");
            $this->db->join("#_pembayaran_piutang_master", "#_pembayaran_piutang_master.pembayaran_piutang_id=#_pembayaran_piutang_detail.pembayaran_piutang_id", "left");
            $this->db->where(array("id_prospek" => $prospek->id_prospek, "#_pembayaran_piutang_master.status !=" => 2));
            $this->db->order_by("#_pembayaran_piutang_master.tanggal_transaksi", "asc");
            $listpembayaran = $this->db->get()->result();
            $jumlah = 0;
            foreach ($listpembayaran as $pembayaran) {
                $jumlah += $pembayaran->jumlah_bayar;
            }
            $this->db->from("#_prospek_history_equipment");
            $this->db->join("#_unit", "#_unit.id_unit=#_prospek_history_equipment.id_unit");
            $this->db->select("#_unit.*,#_prospek_history_equipment.harga,#_prospek_history_equipment.id_add_history_equipment");
            $this->db->where(array("id_prospek_history_detail" => $prospek->id_prospek_history_detail));
            $listequipment = $this->db->get()->result();
            $prospek->list_equipment = $listequipment;
            $this->db->from("#_do_prospek_detail");
            $this->db->join("#_do_prospek", "#_do_prospek.id_do_prospek=#_do_prospek_detail.id_do_prospek");
            $this->db->where(array("id_prospek" => $prospek->id_prospek, "status" => 1));
            $this->db->select("ifnull(sum(#_do_prospek_detail.qty_do),0) as jumlah");
            $rowjumlah = $this->db->get()->row();
            if ($rowjumlah) {
                $prospek->qty_kirim = $rowjumlah->jumlah;
            } else {
                $prospek->qty_kirim = 0;
            }

            $this->db->from("#_buka_jual");
            $this->db->where(array("id_prospek" => $prospek->id_prospek, "status !=" => 2));
            $this->db->select("ifnull(sum(qty_buka_jual),0) as jumlah");
            $rowjumlah = $this->db->get()->row();
            if ($rowjumlah) {
                $prospek->qty_buka_jual = $rowjumlah->jumlah;
            } else {
                $prospek->qty_buka_jual = 0;
            }

            $this->db->from("#_prospek_approval");
            $this->db->where(array("id_prospek" => $prospek->id_prospek));
            $this->db->join("#_user", "#_user.id_user=#_prospek_approval.created_by", "left");
            $prospek->history_approval = $this->db->get()->result();


            $prospek->sisa = ($prospek->harga_jual_unit * $prospek->jumlah_unit) - $jumlah;
            $prospek->terbayar = $jumlah;
            $prospek->listpembayaran = $listpembayaran;
        }
        return $prospek;
    }

    function GetSelectedProspek($id) {
        $this->db->where('id_prospek', $id);
        $this->db->join('#_customer', '#_customer.id_customer = #_prospek.id_customer');
        $selectedProspek = $this->db->get($this->table)->row();
        return $selectedProspek;
    }

    function ProspekManipulate($model) {
        try {
            $this->load->model("buka_jual/m_buka_jual");
            $jenis_pencarian="created_date";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $message = "";
            try {

                $is_rejournal = false;
                $prospek['top'] = DefaultCurrencyDatabase($model['top']);
                $prospek['id_customer'] = ForeignKeyFromDb($model['id_customer']);
                $prospek['id_pegawai'] = ForeignKeyFromDb($model['id_pegawai']);
                $prospek['id_leasing'] = ForeignKeyFromDb($model['id_leasing']);
                $prospek['tanggal_prospek'] = DefaultTanggalDatabase($model['tanggal_prospek']);
                $prospek['id_payment_method'] = ForeignKeyFromDb($model['id_payment_method']);
                $prospek['jth_tempo'] = DefaultTanggalDatabase(AddDays($prospek['tanggal_prospek'], '+' . $prospek['top'] . 'day'));
                $prospek['type_ppn'] = $model['type_ppn'];
                $prospek['ppn'] = DefaultCurrencyDatabase($model['ppn']);
                $nominalppn = 0;
                $totalequipment = 0;
                $prospek['faktur_pajak'] = @$model['faktur_pajak'];
                $prospek['type'] = 'Prospek';
                $prospek['id_payment_method'] = $model['id_payment_method'];
                $prospek['contact_person'] = $model['contact_person'];
                $prospek['tlp_contact_person'] = $model['tlp_contact_person'];

                $prospek['level'] = GetLevelJabatan();
                $prospek['id_cabang'] = ForeignKeyFromDb(@$model['id_cabang']);
                $prospekdetail['jumlah_unit'] = DefaultCurrencyDatabase($model['jumlah_unit']);
                $prospek['keterangan'] = @$model['keterangan'];
                $prospek['nama_stnk'] = @$model['nama_stnk'];
                $prospek['alamat_stnk'] = @$model['alamat_stnk'];
                $prospek['tdp_prospek'] = @$model['tdp_prospek'];
                $prospek['no_ktp_prospek'] = @$model['no_ktp_prospek'];
                $prospekdetail['id_type_body'] = ForeignKeyFromDb($model['id_type_body']);
                $hitunganppn = 0;
                $qty = $prospekdetail['jumlah_unit'];
                $harga_off_the_road = DefaultCurrencyDatabase(@$model['harga_off_the_road']);
                $biaya_bbn = DefaultCurrencyDatabase(@$model['biaya_bbn']);
                $harga_on_the_road = $harga_off_the_road + $biaya_bbn;
                $hitunganppn = $harga_off_the_road;
                $detailequipment = [];
                $listpriceequipment = CheckArray($model, 'equipment_price');
                $listketerangan = CheckArray($model, 'keterangan_detail');
                $listppndetail = CheckArray($model, 'is_ppn_detail');
                $listunit = CheckArray($model, 'id_unit');
                $listid = CheckArray($model, 'id_add_equipment');
                foreach ($listpriceequipment as $key => $equipmentdetail) {
                    $detarr = [];
                    $detarr['id_unit'] = $listunit[$key];
                    $detarr['id_add_equipment'] = @$listid[$key];
                    $detarr['harga'] = DefaultCurrencyDatabase($equipmentdetail);
                    $detarr['keterangan_detail'] = $listketerangan[$key];
                    $detarr['is_ppn_detail'] = $listppndetail[$key];
                    $totalequipment += $detarr['harga'];
                    $detailequipment[] = $detarr;
                    if (!CheckEmpty($listppndetail[$key])) {

                        $hitunganppn += $detarr['harga'];
                    }
                }
                $harga_jual_unit = $harga_on_the_road + $totalequipment;
                $total_harga_jual = $harga_jual_unit * $qty;

                $hitunganppn = $hitunganppn * $qty;
                $ppn = $prospek['ppn'];
                $grandtotal = $total_harga_jual;

                if ($prospek['type_ppn'] == "2") {
                    $nominalppn = $ppn / 100 * $hitunganppn;
                    $grandtotal = $hitunganppn + $nominalppn;
                } else if ($prospek['type_ppn'] == "1") {
                    $nominalppn = $ppn / (100 + $ppn) * $hitunganppn;
                    $total_harga_jual = $total_harga_jual - $nominalppn;
                }
                if (!CheckEmpty(@$model['id_prospek']) && @$model['type_action'] == "revisi") {

                    $this->db->from("#_prospek");
                    $this->db->where(array("id_prospek" => @$model['id_prospek']));
                    $prospekhistory = $this->db->get()->row_array();

                    if ($prospekhistory && $prospekhistory['grandtotal'] <> $grandtotal) {
                        $is_rejournal = true;
                        $prospekhistory['keterangan_reject'] = "Revisi Harga";
                        $this->db->insert("#_prospek_history", MergeCreated($prospekhistory));
                        $id_prospek_history = $this->db->insert_id();
                        $this->db->from("#_prospek_detail");
                        $this->db->where(array("id_prospek" => @$model['id_prospek']));
                        $prospekhistorydetail = $this->db->get()->row_array();
                        unset($prospekhistorydetail['id_prospek']);
                        $id_prospek_detail = $prospekhistorydetail['id_prospek_detail'];
                        unset($prospekhistorydetail['id_prospek_detail']);
                        $prospekhistorydetail['id_prospek_history'] = $id_prospek_history;
                        $this->db->insert("#_prospek_history_detail", MergeCreated($prospekhistorydetail));
                        $id_prospek_history_detail = $this->db->insert_id();
                        $this->db->from("#_prospek_equipment");
                        $this->db->where(array("id_prospek_detail" => $id_prospek_detail));
                        $list_equipment = $this->db->get()->result_array();
                        foreach ($list_equipment as $equip) {
                            unset($equip['id_add_equipment']);
                            unset($equip['id_prospek_detail']);
                            $equip['id_prospek_history_detail'] = $id_prospek_history_detail;
                            $this->db->insert("#_prospek_history_equipment", MergeCreated($equip));
                        }
                        $approval = [];
                        $approval['id_prospek'] = @$model['id_prospek'];
                        $approval['status_approval'] = "Revisi";
                        $approval['nego_value'] = 0;
                        $approval['ket_approve'] = "Revisi Harga dari " . DefaultCurrencyIndo($prospekhistory['grandtotal']);
                        $approval['id_prospek_history'] = ForeignKeyFromDb($id_prospek_history);
                        $this->db->insert("#_prospek_approval", MergeCreated($approval));
                    }
                }
                $prospek['total_equipment'] = $totalequipment;
                $prospek['grandtotal'] = $grandtotal;
                $prospek['total_price'] = $harga_off_the_road;
                $prospekdetail['harga_off_the_road'] = $harga_off_the_road;
                $prospekdetail['biaya_bbn'] = $biaya_bbn;
                $prospekdetail['harga_on_the_road'] = $harga_on_the_road;
                $prospekdetail['harga_jual_unit'] = $harga_jual_unit;
                $prospekdetail['id_jenis_plat'] = ForeignKeyFromDb($model['id_jenis_plat']);
                $prospekdetail['id_unit'] = ForeignKeyFromDb($model['id_unit_bus']);

                $prospekdetail['id_warna'] = ForeignKeyFromDb($model['id_warna']);
                $prospekdetail['id_warna_karoseri'] = ForeignKeyFromDb($model['id_warna_karoseri']);
                $prospekdetail['tahun'] = ForeignKeyFromDb($model['tahun']);

                $prospekdetail['total_equipment'] = $totalequipment;








                if (@$model['type_action'] != "revisi") {
                    $prospek['status'] = 0;
                    $prospekdetail['status'] = 0;
                }
                $prospek['nominal_ppn'] = $nominalppn;

                $id_prospek = 0;
                $no_prospek = "";
                if (CheckEmpty($model['id_prospek'])) {
                    $prospek['created_date'] = GetDateNow();
                    $prospek['created_by'] = ForeignKeyFromDb(GetUserId());
                    $prospek['no_prospek'] = $model['no_prospek'];
                    $no_prospek = $prospek['no_prospek'];
                    $this->db->insert($this->table, $prospek);
                    $id_prospek = $this->db->insert_id();
                } else {
                    $jenis_pencarian="updated_date";
                    $this->db->from("#_prospek");
                    $this->db->where(array("id_prospek" => $model['id_prospek']));
                    $prospekdb = $this->db->get()->row();
                    $no_prospek = @$prospekdb->no_prospek;
                    $prospek['no_prospek'] = $model['no_prospek'];
                    if ($no_prospek != $prospek['no_prospek']) {
                        $this->db->update("#_buku_besar", array("dokumen" => $prospek['no_prospek']), array("dokumen" => $no_prospek));
                    }
                    $id_prospek = $model['id_prospek'];
                    $prospek['updated_date'] = GetDateNow();
                    $prospek['updated_by'] = ForeignKeyFromDb(GetUserId());
                    $prospek['negotiated_by'] = null;
                    $this->db->update($this->table, $prospek, array("id_prospek" => $model['id_prospek']));
					
                }
              

                $this->db->from("#_prospek_detail");
                $this->db->where(array("id_prospek" => $id_prospek));
                $prospekdet = $this->db->get()->row();

                $id_prospek_detail = 0;
                if ($prospekdet) {
                    $id_prospek_detail = $prospekdet->id_prospek_detail;
                    $this->db->update("#_prospek_detail", MergeUpdate($prospekdetail), array("id_prospek_detail" => $id_prospek_detail));
                } else {
                    $prospekdetail['id_prospek'] = $id_prospek;
                    $this->db->insert("#_prospek_detail", MergeCreated($prospekdetail));
                    $id_prospek_detail = $this->db->insert_id();
                }
                
                $this->db->from("#_buka_jual");
                $this->db->where(array("id_prospek" => $id_prospek, "status <>5"));
                $listbukajual = $this->db->get()->result();

                foreach ($listbukajual as $bukadetail) {
                    $this->db->from("#_unit_serial");
                    $this->db->where(array("id_buka_jual" => $bukadetail->id_buka_jual));
                    $jumlahrow = $this->db->get()->num_rows();
                    if ($jumlahrow > 0) {
                        $qty = $jumlahrow;
                        $nominalppn = 0;
                        $hrg_on_the_road = $prospekdetail['harga_on_the_road'];
                        $hrg_off_the_road = $prospekdetail['harga_off_the_road'];
                        $hitunganppn = $hrg_off_the_road;
                        
                        $harga_jual_unit = $hrg_on_the_road + $totalequipment;
                        $total_harga_jual = $harga_jual_unit * $jumlahrow;
                        $hitunganppn = $hitunganppn * $jumlahrow;
                        $ppn = $prospek['ppn'];
                        $grandtotal = $total_harga_jual;


                        if ($prospek['type_ppn'] == "2") {
                            $nominalppn = $ppn / 100 * $hitunganppn;
                            $grandtotal = $grandtotal + $nominalppn;
                        } else if ($prospek['type_ppn'] == "1") {
                            $nominalppn = $ppn / (100 + $ppn) * $hitunganppn;
                            $total_harga_jual = $total_harga_jual - $nominalppn;
                        }
                        $buka_jual['total_bbn'] = $prospekdetail['biaya_bbn'] * $jumlahrow;
                        $buka_jual['total_price'] = $total_harga_jual;
                        $buka_jual['total_equipment'] = $totalequipment;
                        $buka_jual['ppn_nominal'] = $nominalppn;
                        $buka_jual['grandtotal'] = $grandtotal;
                        $buka_jual['qty_buka_jual'] = $jumlahrow;
                        $this->db->update("#_buka_jual",$buka_jual,array("id_buka_jual"=>$bukadetail->id_buka_jual));
                        $this->m_buka_jual->Rejurnal($bukadetail->id_buka_jual);
                    }
                }


                $arrequip = [];
                foreach ($detailequipment as $det) {
                    if (!CheckEmpty($det['id_add_equipment'])) {
                        $arrequip[] = $det['id_add_equipment'];
                        $this->db->update("#_prospek_equipment", MergeUpdate($det), array("id_add_equipment" => $det['id_add_equipment']));
                    } else {
                        $det['id_prospek_detail'] = $id_prospek_detail;
                        $this->db->insert("#_prospek_equipment", MergeCreated($det));
                        $arrequip[] = $this->db->insert_id();
                    }
                }


                $this->db->where(array("id_prospek_detail" => $id_prospek_detail));
                if (count($arrequip) > 0) {
                    $this->db->where_not_in("id_add_equipment", $arrequip);
                }
                $this->db->delete("#_prospek_equipment");
                $this->load->model("module/m_module");
                $listimage = CheckArray($model, "listimage");
                $this->m_module->SyncDb($listimage, "Prospek", $id_prospek);
               
                if ($statusapproval == "1") {
                    $this->ChangeStatus($model['id_prospek']);
                }
                if ($this->db->trans_status() === FALSE || $message != '') {
                    $this->db->trans_rollback();
                    $message = "Some Error Occured<br/>" . $message;
                    return array("st" => false, "msg" => $message);
                } else {
                    ClearCookiePrefix("_prospek", $jenis_pencarian);
                    $this->db->trans_commit();
                    SetMessageSession(true, "Data Prospek Sudah di" . (CheckEmpty(@$model['id_prospek']) ? "masukkan" : "update") . " ke dalam database");
                    $arrayreturn["st"] = true;
                    SetPrint($id_prospek, $no_prospek, 'Prospek');
                    return $arrayreturn;
                }
            } catch (Exception $ex) {
                $this->db->trans_rollback();
                return array("st" => false, "msg" => $ex->getMessage());
            }

            //$this->createHistory($model, count($dataEquipment) > 0 ? array_sum(array_column($dataEquipment, 'harga')) : 0, 'create');
            //$this->createHistory($model, count($dataEquipment) > 0 ? array_sum(array_column($dataEquipment, 'harga')) : 0, 'update');
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownProspek() {
        $listprospek = GetTableData($this->table, 'id_prospek', 'no_prospek', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listprospek;
    }

    function ProspekBatal($id_prospek) {


        try {
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $message = "";
            $model['updated_date'] = GetDateNow();
            $model['updated_by'] = GetUserId();
            $model['status'] = 5;
            $this->db->from("#_prospek_detail");
            $this->db->where(array("id_prospek" => $id_prospek));
            $this->db->select("id_prospek_detail");
            $listiddetail = $this->db->get()->result_array();
            $listiddetail = array_column($listiddetail, "id_prospek_detail");
            $data = $this->db->where('id_prospek', $id_prospek)->get('#_prospek')->row_array();
            $sumEquipment = $this->db->select_sum('harga')->where_in('id_prospek_detail', $listiddetail)->get('#_prospek_equipment')->row();

            $this->db->from("#_prospek_detail_unit");
            $this->db->where_in("id_prospek_detail", $listiddetail);
            $this->db->where("id_unit_serial !=", "null");
            $rowcheck = $this->db->get()->row();
            if ($rowcheck) {
                $message .= "Data prospek sudah ada yang dibuka jual";
            }

            if ($message == "") {
                $this->db->update($this->table, $model, array('id_prospek' => $id_prospek));
                $this->db->from("#_prospek_detail_unit");
                $this->db->where_in("id_prospek_detail", $listiddetail);
                $this->db->delete();
            }

            $this->load->model("customer/m_customer");
            $this->db->update("#_buku_besar", array("debet" => 0, "kredit" => 0), array("dokumen" => $data['no_prospek']));
            $this->m_customer->KoreksiSaldoPiutang($data['id_customer']);


            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                $this->db->trans_commit();
                return array("st" => true, "msg" => "Prospek sudah dibatalkan");
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => "Some Error Occured");
        }
    }

    function createHistory($data, $sumEquipment, $status) {
        switch ($status) {
            case 'create':
                $affected_by = $data['created_by'];
                break;
            case 'update':
                $affected_by = $data['updated_by'];
                break;
            case 'approve':
            case 'reject':
                $affected_by = $data['approved_by'];
                break;
            case 'delete':
                $affected_by = $data['deleted_by'];
                break;
            case 'nego':
                $affected_by = $data['negotiated_by'];
                break;
            default :
                $affected_by = null;
                break;
        }

        $params = [
            'id_prospek' => $data['id_prospek'],
            'harga_off_the_road' => $data['harga_off_the_road'],
            'biaya_bbn' => $data['biaya_bbn'],
            'harga_on_the_road' => $data['harga_on_the_road'],
            'harga_jual_unit' => $data['harga_jual_unit'],
            'total_perlengkapan' => $sumEquipment,
            'status' => $status,
            'level' => (isset($data['level']) ? $data['level'] : 1),
            'affected_by' => $affected_by,
            'affected_date' => GetDateNow()
        ];
        $this->db->insert("#_history_prospek", $params);
    }

    function NegoProspek($params) {
        $params['created_by'] = GetUserId();
        $params['created_date'] = GetDateNow();
        $params['estimasi_harga'] = DefaultCurrencyDatabase($params['estimasi_harga']);
        $this->db->trans_begin();
        try {
            $this->db->insert('#_prospek_nego', $params);
            $updateProspek = [
                'status' => 3,
                'level' => 1,
                'negotiated_by' => $params['created_by']
            ];
            $this->db->update('#_prospek', $updateProspek, array('id_prospek' => $params['id_prospek']));
            $data = $this->db->where('id_prospek', $params['id_prospek'])->get('#_prospek')->row_array();
            $sumEquipment = $this->db->select_sum('harga')->where('id_prospek', $params['id_prospek'])->get('#_prospek_equipment')->row();
            $this->createHistory($data, empty($sumEquipment->harga) ? 0 : $sumEquipment->harga, 'nego');
            $this->db->trans_commit();
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
        return array("st" => true, "msg" => "Prospek has been negotiated.");
    }

}
