<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>DGMI | Prospek Baru</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="http://68.183.230.253/dgmi/assets/images/logo_dgmi.png" style="width:100%; max-width:150px;">
                            </td>
                            <td>
                                No. Prospek : <?= @$no_prospek?><br>
                                Tanggal: <?= DefaultDatePicker(@$tanggal_prospek)?><br>
                                Cabang: <?= @$nama_cabang?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Nama Sales : <?= @$nama_pegawai?><br>
                                Nama Sales Manager : <?= @$nama_pegawai?><br>
                            </td>
                            <!-- <td>
                                Acme Corp.<br>
                                John Doe<br>
                                john@example.com
                            </td> -->
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    CUSTOMER
                </td>
                <td></td>
            </tr>
            <tr class="details">
                <td>Nama Customer</td>
                <td><?= @$nama_jenis_customer . ' ' .@$nama_customer?></td>
            </tr>
            <tr class="details">
                <td>Alamat Customer</td>
                <td><?= @$alamat?></td>
            </tr>
            <tr class="details">
                <td>No Telp</td>
                <td><?= @$no_telp?></td>
            </tr>
            <tr class="details">
                <td>Email / Fax</td>
                <td><?= @$email . ' / ' . @$fax?></td>
            </tr>
            <tr class="details">
                <td>NPWP</td>
                <td><?= @$npwp . ' atas nama ' . @$nama_npwp?></td>
            </tr>
            <tr class="details">
                <td>Alamat NPWP</td>
                <td><?= @$alamat_npwp?></td>
            </tr>
            <tr class="details">
                <td>Contact Person</td>
                <td><?= @$contact_person . ' / ' . @$no_telp_2?></td>
            </tr>
            <tr class="heading">
                <td>UNIT</td>
                <td></td>
            </tr>
            <tr class="item">
                <td>Unit</td>
                <td><?= @$nama_unit?></td>
            </tr>
            <tr class="item">
                <td>Type Unit</td>
                <td><?= @$nama_type_unit?></td>
            </tr>
            <tr class="item">
                <td>Kategori Unit</td>
                <td><?= @$nama_kategori?></td>
            </tr>
            <tr class="item">
                <td>Type Body</td>
                <td><?= @$nama_type_body?></td>
            </tr>
            <tr class="item">
                <td>Jenis Plat</td>
                <td><?= @$nama_jenis_plat?></td>
            </tr>
            <tr class="item">
                <td>Warna</td>
                <td><?= @$nama_warna?></td>
            </tr>
            <tr class="item">
                <td>Harga Off The Road</td>
                <td><?= DefaultCurrency(@$harga_off_the_road)?></td>
            </tr>
            <tr class="item">
                <td>Biaya BBN</td>
                <td><?= DefaultCurrency(@$biaya_bbn)?></td>
            </tr>
            <tr class="item">
                <td>Harga On The Road</td>
                <td><?= DefaultCurrency(@$harga_on_the_road)?></td>
            </tr>
            <tr class="item last">
                <td>Harga Jual Unit</td>
                <td><?= DefaultCurrency(@$harga_jual_unit)?></td>
            </tr>
            <tr class="total">
                <td>Total Harga Jual</td>
                <td><?= DefaultCurrency(@$jumlah_unit * @$harga_jual_unit)?></td>
            </tr>
            <tr class="heading">
                <td>
                    PEMBAYARAN
                </td>
                <td></td>
            </tr>
            <tr class="item">
                <td>Payment Method</td>
                <td><?= @$nama_payment_method?></td>
            </tr>
            <tr class="item">
                <td>Contact Person</td>
                <td><?= @$contact_person?></td>
            </tr>
            <tr class="item last">
                <td>Telp Contact Person</td>
                <td><?= @$tlp_contact_person?></td>
            </tr>
        </table>
    </div>
</body>
</html>