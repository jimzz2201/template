<div class="modal-header">
  Pilih Prospek
</div>
<div class="modal-body">
  <section class="content">
    <div class="box box-default">
        <div class="box-body">
          <div id="notification" ></div>
          <div class=" headerbutton"></div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="portlet-body form">
              <table class="table table-striped table-bordered table-hover" id="mytable" style="width:100%;"></table>
            </div>
          </div>
        </div>
     </div>
  </section>
</div>

<script>
  $(document).ready(function() {
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    table = $("#mytable").dataTable({
        initComplete: function() {
            var api = this.api();
            $('#mytable_filter input')
              .off('.DT')
              .on('keyup.DT', function(e) {
                if (e.keyCode == 13) {
                    api.search(this.value).draw();
                }
            });
        },
        autowidth: false,
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: {"url": baseurl + 'index.php/prospek/getDataProspekSelect', "type": "POST"},
        columns: [
            {
                data: "id_prospek",
                title: "No",
                orderable: false
            }, 
            {data: "nama_customer" , orderable:true , title :"Customer"},
            {data: "nama_user" , orderable:true , title :"Salesman"},
            {
                "data" : "action",
                "orderable": false,
                "className" : "text-center"
            }
        ],
        order: [[0, 'desc']],
        columnDefs: [],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        }
    });
    $('#modalbootstrap').on('shown.bs.modal', function () {
      var table = $('#mytable').DataTable();
      table.columns.adjust();
    });
  });
</script>
<style>
  .dataTableLayout {
    table-layout:fixed;
    width:100%;
  }
</style>