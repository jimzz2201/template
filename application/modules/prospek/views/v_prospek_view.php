<section class="content-header">
    <h1>
        Prospek <?= @$button ?>
        <small>Prospek View</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Prospek</li>
        <li class="active">Prospek View</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary" data-collapsed="0">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <form id="frm_prospek" class="form-horizontal form-groups-bordered validate" method="post">
                                <div class="areainput">
                                    <h1 style="font-weight: bold;"><?php
                                        echo "<span>No Prospek : </span><span style=color:red;>" . @$no_prospek . "</span>";
                                        ?></h1>
                                    <hr/>
                                    <table class="table table-striped table-bordered table-hover" id="">
                                        <tr>
                                            <th style="width:100px">Status</th>
                                            <th style="width:200px">Pembuat</th>
                                            <th style="width:200px">Tanggal</th>
                                            <th>Notes</th>
                                            <th style="width:200px">Nego Value</th>
                                            <th style="width:100px">History</th>
                                        </tr>
                                        <?php foreach ($history_approval as $approval) { ?>
                                            <tr>
                                                <td><?= $approval['status_approval'] ?></td>
                                                <td><?= $approval['nama_user'] ?></td>
                                                <td><?= DefaultTanggal($approval['created_date']) . ' ' . DefaultTimePicker($approval['created_date']) ?></td>
                                                <td><?= $approval['ket_approve'] ?></td>
                                                <td><?= DefaultCurrencyAkuntansi($approval['nego_value']) ?></td>
                                                <td style="text-align: center;">
                                                    <?php if (!CheckEmpty($approval['id_prospek_history'])) { ?>
                                                        <a target="_blank" href="<?php echo base_url() . 'index.php/prospek/prospek_history/' . $approval['id_prospek_history'] ?>" class="btn btn-info btn-xs"  >View</a>
                                                    <?php } ?>
                                                </td>
                                            </tr>

                                        <?php } ?>
                                    </table>
                                    <input type="hidden" name="id_prospek" id="id_prospek" value="<?php echo @$id_prospek; ?>" /> 
                                    <div class="form-group col-md-12">
                                        <?= form_label('Tanggal', "txt_id_customer", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'name' => 'tanggal_prospek', 'class' => 'form-control datepicker', 'id' => 'tanggal_prospek', 'value' => DefaultDatePicker(@$tanggal_prospek), 'placeholder' => 'Tanggal')); ?>
                                        </div>
                                        <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-2">
                                            <?= form_input(array('type' => 'text', 'name' => 'top', 'value' => @$top, 'class' => 'form-control top', 'id' => 'txt_top', 'placeholder' => 'Top')); ?>
                                        </div>  
                                        <?= form_label('Jth Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-2">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'name' => 'jth_tempo', 'value' => DefaultDatePicker(@$jth_tempo), 'class' => 'form-control', 'id' => 'txt_jth_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Salesman', "txt_salesman", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_pegawai', 'class' => 'form-control', 'id' => 'dd_id_pegawai', 'placeholder' => 'Salesman'), DefaultEmptyDropdown($list_pegawai, "json", "Salesman"), @$id_pegawai); ?>
                                        </div>


                                        <?= form_label('Cabang', "txt_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_cabang', 'class' => 'form-control', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown($list_cabang, "json", "cabang"), @$id_cabang); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Nama Customer', "txt_id_customer", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_customer', 'class' => 'form-control', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown($list_customer, "json", "customer"), @$id_customer); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Alamat', "", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_textarea(array('type' => 'text', 'readonly' => 'true', 'rows' => '3', 'cols' => '10', 'class' => 'form-control', 'id' => 'alamat_cust', 'placeholder' => 'Alamat')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('No Telp / FAX', "", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'no_tlp_cust', 'placeholder' => 'No Telp / FAX')); ?>
                                        </div>
                                        <?= form_label('Faktur Pajak', "txt_faktur_pajak", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_dropdown(array("selected" => @$faktur_pajak, "name" => "faktur_pajak"), array('1' => 'Yes', '0' => 'No'), @$faktur_pajak, array('class' => 'form-control', 'id' => 'faktur_pajak')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <?= form_label('No NPWP', "", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'no_npwp_cust', 'placeholder' => 'No NPWP')); ?>
                                        </div>
                                        <?= form_label('Nama NPWP', "", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'nama_npwp_cust', 'placeholder' => 'Nama NPWP')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <?= form_label('Alamat NPWP', "", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_textarea(array('type' => 'text', 'readonly' => 'true', 'rows' => '3', 'cols' => '10', 'class' => 'form-control', 'id' => 'alamat_npwp_cust', 'placeholder' => 'Alamat NPWP')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Contact Person', "", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'contact_person_cust', 'placeholder' => 'Contact Person')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('No Telp / HP', "", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'telp_cp_cust', 'placeholder' => 'No Telp / HP')); ?>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                
                                <?php echo @$approvalhtml ?>

                                <div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Keterangan Unit dan Estimasi Harga</span></h3></div>

                                <div class="areainput">
                                    <div class="form-group col-md-12">
                                        <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-2">
                                            <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "", "PPN"), @$type_ppn, array('class' => 'form-control', 'id' => 'dd_type_ppn')); ?>
                                        </div>
                                        <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-1">
                                            <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$ppn), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>

                                    </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Nama STNK', "txt_nama_stnk", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_stnk', 'value' => @$nama_stnk, 'class' => 'form-control', 'id' => 'txt_nama_stnk', 'placeholder' => 'Nama STNK')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Alamat STNK', "txt_biaya_bbn", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat_stnk', 'class' => 'form-control', 'id' => 'txt_alamat_stnk', 'placeholder' => 'Alamat STNK'), @$alamat_stnk); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('TDP', "txt_id_tdp", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'tdp_prospek', 'class' => 'form-control', 'id' => 'tdp', 'placeholder' => 'TDP'), @$tdp_prospek); ?>
                                    </div>
                                    <?= form_label('No KTP', "txt_id_no_ktp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'no_ktp_prospek', 'class' => 'form-control', 'id' => 'no_ktp_prospek', 'placeholder' => 'No KTP'), @$no_ktp_prospek); ?>
                                    </div>
                                </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Jumlah Unit', "txt_jumlah_unit", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'name' => 'jumlah_unit', 'value' => @$jumlah_unit, 'class' => 'form-control', 'id' => 'txt_jumlah_unit', 'placeholder' => 'Jumlah Unit')); ?>
                                        </div>

                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Unit', "txt_id_kategori", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit_bus', 'class' => 'form-control', 'id' => 'dd_id_unit_bus', 'placeholder' => 'Kategori'), DefaultEmptyDropdown($list_unit_bus, "json", "Unit"), @$id_unit); ?>
                                        </div>
                                        <?= form_label('Tahun', "txt_tahun", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'name' => 'tahun', 'value' => @$tahun, 'class' => 'form-control', 'id' => 'txt_tahun', 'placeholder' => 'Tahun')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Kategori', "txt_id_kategori", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori'), @$nama_kategori); ?>
                                        </div>
                                        <?= form_label('Type Unit', "txt_id_type_unit", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Type Unit'), @$nama_type_unit); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">

                                        <?= form_label('Type Body', "txt_id_type_body", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_type_body', 'placeholder' => 'Type Body'), @$nama_type_body); ?>
                                        </div>
                                        <?= form_label('Jenis Plat', "txt_id_jenis_plat", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_jenis_plat', 'class' => 'form-control', 'id' => 'dd_jenis_plat', 'placeholder' => 'Jenis Plat'), DefaultEmptyDropdown($list_jenis_plat, "json", "Jenis Plat"), @$id_jenis_plat); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        
                                        <?= form_label('Warna Kabin', "txt_id_warna", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-4">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_warna', 'class' => 'form-control', 'id' => 'dd_warna', 'placeholder' => 'Warna'), DefaultEmptyDropdown($list_warna, "json", "Warna"), @$id_warna); ?>
                                        </div>
                                        <?= form_label('Warna Karoseri', "txt_id_warna", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_warna', 'class' => 'form-control', 'id' => 'dd_warna', 'placeholder' => 'Warna'), DefaultEmptyDropdown($list_warna, "json", "Warna"), @$id_warna); ?>
                                        </div>
                                    </div>
                                     <div class="form-group col-md-12">
                                    <?= form_label('Keterangan', "txt_biaya_bbn", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'), @$keterangan); ?>
                                    </div>
                                    <div style="clear:both"></div><br/>
                                    <?php
                                    $price = array();
                                    $option_unit = '';
                                    $tempunit = [];
                                    foreach ($list_unit as $key => $val) {
                                        $sel = ($val['id'] == @$list_unit) ? ' selected' : '';
                                        $dt = explode('-', $val['text']);
                                        array_push($price, array('id' => $val['id'], 'price' => trim($dt[1])));
                                        $tempunit[] = array("id" => $val['id'], "text" => trim($dt[0]));
                                        $option_unit .= '<option value="' . $val['id'] . '" ' . $sel . '>' . $dt[0] . '</option>';
                                    }
                                    ?>
                                         
                                    <div class="form-group col-md-12">
                                        <?= form_label('Harga Off The Road', "txt_harga_off_the_road", array("class" => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-5">
                                            <div class='input-group'>
                                                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                                <?= form_input(array('type' => 'text', 'name' => 'harga_off_the_road', 'value' => DefaultCurrency(@$harga_off_the_road), 'class' => 'form-control calculate input-currency', 'id' => 'txt_harga_off_the_road', 'placeholder' => 'Harga Off The Road', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Biaya Bbn', "txt_biaya_bbn", array("class" => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-5">
                                            <div class='input-group'>
                                                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                                <?= form_input(array('type' => 'text', 'name' => 'biaya_bbn', 'value' => DefaultCurrency(@$biaya_bbn), 'class' => 'form-control calculate input-currency', 'id' => 'txt_biaya_bbn', 'placeholder' => 'Biaya Bbn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Harga On The Road', "txt_harga_on_the_road", array("class" => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-5">
                                            <div class='input-group'>
                                                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'harga_on_the_road', 'value' => DefaultCurrency(@$harga_on_the_road), 'class' => 'form-control calculate input-currency', 'id' => 'txt_harga_on_the_road', 'placeholder' => 'Harga On The Road', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="equipments">
                                        <?php
                                        $cnt = 0;

                                        $opt_unit = '';
                                        foreach ($list_equipment as $cnt => $dataEq) {
                                               include APPPATH . 'modules/prospek/views/v_prospek_detail_attribute.php'; }
                                        ?>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Harga Jual Unit', "txt_harga_jual_unit", array("class" => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-5">
                                            <div class='input-group'>
                                                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'harga_jual_unit', 'value' => DefaultCurrency(@$harga_jual_unit), 'class' => 'form-control input-currency', 'id' => 'txt_harga_jual_unit', 'placeholder' => 'Harga Jual Unit', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Total Harga Jual', "txt_total_harga_jual", array("class" => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-5">
                                            <div class='input-group'>
                                                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => 0, 'class' => 'form-control input-currency', 'id' => 'total_harga_jual', 'placeholder' => 'Total Harga Jual', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">

                                        <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-5">
                                            <div class='input-group'>
                                                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                                <?= form_input(array('type' => 'text', 'name' => 'nominal_ppn', 'value' => DefaultCurrency(@$nominal_ppn), 'class' => 'form-control  input-currency', 'id' => 'txt_nominal_ppn', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group col-md-12">

                                        <?= form_label('Grandtotal', "txt_kepada", array("class" => 'col-sm-3 control-label')); ?>
                                        <div class="col-sm-5">
                                            <div class='input-group'>
                                                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                                <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$grandtotal), 'class' => 'form-control  input-currency', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
                                            </div>
                                        </div>


                                    </div>


                                    <div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Cara Pembayaran</span></h3></div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Payment Method', "txt_id_payment_method", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_payment_method', 'class' => 'form-control', 'id' => 'dd_payment_method', 'placeholder' => 'Payment Method'), DefaultEmptyDropdown($list_payment_method, "json", "Payment Method"), @$id_payment_method); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 groupleasing" <?php if (@$id_payment_method != "2") echo "style='display:none'"; ?>>
                                        <?= form_label('Leasing', "txt_contact_person", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_leasing', 'class' => 'form-control', 'id' => 'dd_id_leasing', 'placeholder' => 'Leasing'), DefaultEmptyDropdown($list_leasing, "json", "Leasing"), @$id_leasing); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Contact Person', "txt_contact_person", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'name' => 'contact_person', 'value' => @$contact_person, 'class' => 'form-control', 'id' => 'txt_contact_person', 'placeholder' => 'Contact Person')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <?= form_label('Tlp Contact Person', "txt_tlp_contact_person", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'name' => 'tlp_contact_person', 'value' => @$tlp_contact_person, 'class' => 'form-control', 'id' => 'txt_tlp_contact_person', 'placeholder' => 'Tlp Contact Person')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <a href="<?php echo base_url() . 'index.php/prospek' ?>" class="btn btn-default"  >Cancel</a>

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $(".areainput input").attr("readonly", "readonly");
        $(".areainput textarea").attr("disabled", "disabled");
        $(".areainput select").select2({disabled: "readonly"});
        calcEquip();
    })
      function calcEquip() {
        let totalEquip = 0;
        let qty = $('#txt_jumlah_unit').val();
        let hitunganppn = 0;
        let nominalppn = 0;
        let hrg_on_the_road = Number($("#txt_harga_on_the_road").val().replace(/\,/g, ''));
        let hrg_off_the_road = Number($("#txt_harga_off_the_road").val().replace(/\,/g, ''));
        hitunganppn = hrg_off_the_road;
        let indexid = "";
        $('.equip').each(function () {
           
            indexid = $(this).attr("id").replace("hrgEquip", "");
            
            if ($("#buttonppndetail" + indexid).html()=="Ya")
            {
                hitunganppn += Number($("#hrgEquip"+indexid).val().replace(/\,/g, ''));
            }
            totalEquip += Number($("#hrgEquip"+indexid).val().replace(/\,/g, ''));
            $("#hrgEquip"+indexid).val(Comma($("#hrgEquip"+indexid).val()));
        });
        let harga_jual_unit = parseInt(hrg_on_the_road) + totalEquip;
        let total_harga_jual = harga_jual_unit * qty;
        $('#txt_harga_jual_unit').val(number_format(harga_jual_unit));
         hitunganppn=hitunganppn*qty;


        var ppn = Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));


        var grandtotal = total_harga_jual;


        if ($("#dd_type_ppn").val() == "2")
        {
            nominalppn = ppn / 100 * hitunganppn;
            grandtotal = grandtotal + nominalppn;
        } else if ($("#dd_type_ppn").val() == "1")
        {
            nominalppn = ppn / (100 + Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""))) * hitunganppn;
            total_harga_jual = total_harga_jual - nominalppn;
        }
        $('#total_harga_jual').val(number_format(total_harga_jual.toFixed(4)));
        $("#txt_grandtotal").val(number_format(grandtotal.toFixed(4)));
        $("#txt_nominal_ppn").val(number_format(nominalppn.toFixed(4)));



    }
    function UpdateHarga()
    {
        Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));
        let hrg_off_the_road = Number($("#txt_harga_off_the_road").val().replace(/[^0-9\.]+/g, ""));
        let biaya_bbn = Number($("#txt_biaya_bbn").val().replace(/[^0-9\.]+/g, ""));
        let hrg_on_the_road = hrg_off_the_road + biaya_bbn
        $('#txt_harga_on_the_road').val(Comma(hrg_on_the_road));
        $('#txt_harga_jual_unit').val(Comma(hrg_on_the_road));
        calcEquip();
    }
</script>

<style>
    .control-label {
        text-align: left !important;
    }
    .input-currency {
        border: none;
        border-bottom: 1px dotted #000000;
        text-align: right;
        background-color: #FFFFFF;
    }
    .span-currency {
        border: none;
    }
</style>