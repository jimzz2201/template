<div style="clear:both"></div>
<hr/> 
<h1 style="font-weight:bold;color:red;">APPROVAL</h1>
<hr/>
<div class="form-group col-md-12">
    <?= form_label('Notes', "", array("class" => 'col-sm-2 control-label')); ?>
    <div class="col-sm-10">
        <?= form_textarea(array('type' => 'text', 'class' => 'form-control', 'name' => 'ket_approve', 'placeholder' => 'Notes')); ?>
    </div>

</div>
<div class="form-group col-md-12">
    <?= form_label('&nbsp;', "", array("class" => 'col-sm-2 control-label')); ?>
    <div class="col-sm-2">
        <button id="btt_Approve" type="button" onclick="Approval('approve')" class="btn btn-success btn-block">Approve</button>

    </div>
    <div class="col-sm-2">
        <button id="btt_Reject" type="button" onclick="Approval('reject')" class="btn btn-danger btn-block">Reject</button>

    </div>
    <div class="col-sm-1">
        <button  type="button" class="btn labeltextbox" >Nego Di</button>
     </div>
    <div class="col-sm-3">
        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$harga_off_the_road), 'class' => 'form-control', 'id' => 'txt_nego', 'placeholder' => 'Nego', 'name' => 'nego_value', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
    </div>
    <div class="col-sm-2">
        <button id="btt_Nego" type="button" onclick="Approval('nego')" class="btn btn-info  btn-block">Nego</button>
    </div>

</div>
<div style="clear:both"></div>
<script>
  
    
    function Approval(type)
    {
        swal({
            title: "Apakah kamu yakin ingin  "+type+" data Prospek berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/prospek/prospek_'+type,
                    dataType: 'json',
                    data: $("#frm_prospek").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            window.location.href = baseurl + 'index.php/prospek';
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }
    
    
</script>