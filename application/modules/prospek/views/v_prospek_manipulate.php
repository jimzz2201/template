<section class="content-header">
    <h1>
        Prospek <?= @$button ?>
        <small>Prospek</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Prospek</li>
        <li class="active">Prospek <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary" data-collapsed="0">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <form id="frm_prospek" class="form-horizontal form-groups-bordered validate" method="post">

                                <input type="hidden" name="id_prospek" id="id_prospek" value="<?php echo @$id_prospek; ?>" /> 
                                <input type="hidden" name="type_action" id="type_action" value="<?php echo @$type_action; ?>" /> 
                                <div class="form-group col-md-12">
                                    <?= form_label('Tanggal', "txt_id_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'tanggal_prospek', 'class' => 'form-control datepicker', 'id' => 'tanggal_prospek', 'value' => DefaultDatePicker(@$tanggal_prospek), 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "top"), @$list_top, @$top, array('class' => 'form-control', 'id' => 'txt_top')); ?>
                                    </div>   
                                    <?= form_label('Jth Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'name' => 'jth_tempo', 'value' => DefaultDatePicker(@$jth_tempo), 'class' => 'form-control', 'id' => 'txt_jth_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Salesman', "txt_salesman", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_pegawai', 'class' => 'form-control', 'id' => 'dd_id_pegawai', 'placeholder' => 'Salesman'), DefaultEmptyDropdown($list_pegawai, "json", "Salesman"), @$id_pegawai); ?>
                                    </div>


                                    <?= form_label('Cabang', "txt_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_cabang', 'class' => 'form-control', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown($list_cabang, "json", "cabang"), @$id_cabang); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Nama Customer', "txt_id_customer", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_customer', 'class' => 'form-control', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown($list_customer, "json", "customer"), @$id_customer); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Alamat', "", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_textarea(array('type' => 'text', 'readonly' => 'true', 'rows' => '3', 'cols' => '10', 'class' => 'form-control', 'id' => 'alamat_cust', 'placeholder' => 'Alamat')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('No Telp / FAX', "", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'no_tlp_cust', 'placeholder' => 'No Telp / FAX')); ?>
                                    </div>
                                    <?= form_label('Faktur Pajak', "txt_faktur_pajak", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array("selected" => @$faktur_pajak, "name" => "faktur_pajak"), array('1' => 'Yes', '0' => 'No'), @$faktur_pajak, array('class' => 'form-control', 'id' => 'faktur_pajak')); ?>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <?= form_label('No NPWP', "", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'no_npwp_cust', 'placeholder' => 'No NPWP')); ?>
                                    </div>
                                    <?= form_label('Nama NPWP', "", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'nama_npwp_cust', 'placeholder' => 'Nama NPWP')); ?>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <?= form_label('Alamat NPWP', "", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_textarea(array('type' => 'text', 'readonly' => 'true', 'rows' => '3', 'cols' => '10', 'class' => 'form-control', 'id' => 'alamat_npwp_cust', 'placeholder' => 'Alamat NPWP')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Contact Person', "", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'contact_person_cust', 'placeholder' => 'Contact Person')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('No Telp / HP', "", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'true', 'class' => 'form-control', 'id' => 'telp_cp_cust', 'placeholder' => 'No Telp / HP')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Keterangan Unit dan Estimasi Harga</span></h3></div>
                                <div class="form-group col-md-12">
                                    <?= form_label('No Prospek', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'value' => @$no_prospek,'name' => 'no_prospek', 'class' => 'form-control', 'id' => 'txt_no_prospek', 'placeholder' => 'No SPK')); ?>
                                    </div>
                                   

                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "", "PPN"), @$type_ppn, array('class' => 'form-control', 'id' => 'dd_type_ppn')); ?>
                                    </div>
                                    <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$ppn), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Nama STNK', "txt_nama_stnk", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_stnk', 'value' => @$nama_stnk, 'class' => 'form-control', 'id' => 'nama_stnk', 'placeholder' => 'Nama STNK')); ?>
                                    </div>
                                    
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Alamat STNK', "txt_biaya_bbn", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat_stnk', 'class' => 'form-control', 'id' => 'alamat_stnk', 'placeholder' => 'Alamat STNK'), @$alamat_stnk); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('TDP / NIB', "txt_id_tdp", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'tdp_prospek', 'class' => 'form-control', 'id' => 'tdp_prospek', 'placeholder' => 'TDP'), @$tdp_prospek); ?>
                                    </div>
                                    <?= form_label('No KTP', "txt_id_no_ktp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'no_ktp_prospek', 'class' => 'form-control', 'id' => 'no_ktp_prospek', 'placeholder' => 'No KTP'), @$no_ktp_prospek); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Jumlah Unit', "txt_jumlah_unit", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'jumlah_unit', 'value' => @$jumlah_unit, 'class' => 'form-control', 'id' => 'txt_jumlah_unit', 'placeholder' => 'Jumlah Unit')); ?>
                                    </div>

                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Unit', "txt_id_kategori", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit_bus', 'class' => 'form-control', 'id' => 'dd_id_unit_bus', 'placeholder' => 'Kategori'), DefaultEmptyDropdown($list_unit_bus, "json", "Unit"), @$id_unit); ?>
                                    </div>
                                    <?= form_label('Tahun', "txt_tahun", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'tahun', 'value' => @$tahun, 'class' => 'form-control', 'id' => 'txt_tahun', 'placeholder' => 'Tahun')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Kategori', "txt_id_kategori", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori'), @$nama_kategori); ?>
                                    </div>
                                    <?= form_label('Type Unit', "txt_id_type_unit", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Type Unit'), @$nama_type_unit); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">

                                    <?= form_label('Type Body', "txt_id_type_body", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_type_body', 'class' => 'form-control', 'id' => 'dd_id_type_body', 'placeholder' => 'Type Body'), DefaultEmptyDropdown($list_type_body, "json", "type body"), @$id_type_body); ?>
                                    </div>
                                    <?= form_label('Jenis Plat', "txt_id_jenis_plat", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_jenis_plat', 'class' => 'form-control', 'id' => 'dd_jenis_plat', 'placeholder' => 'Jenis Plat'), DefaultEmptyDropdown($list_jenis_plat, "json", "Jenis Plat"), @$id_jenis_plat); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    
                                    <?= form_label('Warna Kabin', "txt_id_warna", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_warna', 'class' => 'form-control', 'id' => 'dd_warna', 'placeholder' => 'Warna'), DefaultEmptyDropdown($list_warna, "json", "Warna"), @$id_warna); ?>
                                    </div>
                                    <?= form_label('Warna Karoseri', "txt_id_warna", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_warna_karoseri', 'class' => 'form-control', 'id' => 'dd_warna_karoseri', 'placeholder' => 'Warna'), DefaultEmptyDropdown($list_warna, "json", "Warna"), @$id_warna_karoseri); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Keterangan', "txt_biaya_bbn", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'keterangan',  'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan'), @$keterangan); ?>
                                    </div>
                                </div>



                                <div class="form-group col-md-12">
                                    <?= form_label('Harga Off The Road', "txt_harga_off_the_road", array("class" => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-5">
                                        <div class='input-group'>
                                            <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                            <?= form_input(array('type' => 'text', 'name' => 'harga_off_the_road', 'value' => DefaultCurrency(@$harga_off_the_road), 'class' => 'form-control calculate input-currency', 'id' => 'txt_harga_off_the_road', 'placeholder' => 'Harga Off The Road', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Biaya Bbn', "txt_biaya_bbn", array("class" => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-5">
                                        <div class='input-group'>
                                            <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                            <?= form_input(array('type' => 'text', 'name' => 'biaya_bbn', 'value' => DefaultCurrency(@$biaya_bbn), 'class' => 'form-control calculate input-currency', 'id' => 'txt_biaya_bbn', 'placeholder' => 'Biaya Bbn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-md-12">
                                    <?= form_label('Harga On The Road', "txt_harga_on_the_road", array("class" => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-5">
                                        <div class='input-group'>
                                            <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'harga_on_the_road', 'value' => DefaultCurrency(@$harga_on_the_road), 'class' => 'form-control calculate input-currency', 'id' => 'txt_harga_on_the_road', 'placeholder' => 'Harga On The Road', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="button" class="btn btn-warning" id="additional_equipment" onclick="addEquipment()">Perlengkapan Tambahan</button>
                                </div>
                                <div id="equipments">
                                    <?php
                                    $cnt = 0;

                                    $opt_unit = '';
                                    foreach ($list_equipment as $cnt => $dataEq) {
                                          include APPPATH . 'modules/prospek/views/v_prospek_detail_attribute.php';
                                    }
                                    ?>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Harga Jual Unit', "txt_harga_jual_unit", array("class" => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-5">
                                        <div class='input-group'>
                                            <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'harga_jual_unit', 'value' => DefaultCurrency(@$harga_jual_unit), 'class' => 'form-control input-currency', 'id' => 'txt_harga_jual_unit', 'placeholder' => 'Harga Jual Unit', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Total Harga Jual', "txt_total_harga_jual", array("class" => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-5">
                                        <div class='input-group'>
                                            <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'value' => 0, 'class' => 'form-control input-currency', 'id' => 'total_harga_jual', 'placeholder' => 'Total Harga Jual', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $price = array();
                                $option_unit = '';
                                $tempunit = [];
                                foreach ($list_unit as $key => $val) {
                                    array_push($price, array('id' => $val['id'], 'price' => trim($dt[1])));
                                }
                                ?>
                                <div class="form-group col-md-12">

                                    <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-5">
                                        <div class='input-group'>
                                            <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                            <?= form_input(array('type' => 'text', 'name' => 'nominal_ppn', 'value' => DefaultCurrency(@$nominal_ppn), 'class' => 'form-control  input-currency', 'id' => 'txt_nominal_ppn', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-group col-md-12">

                                    <?= form_label('Grandtotal', "txt_kepada", array("class" => 'col-sm-3 control-label')); ?>
                                    <div class="col-sm-5">
                                        <div class='input-group'>
                                            <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                                            <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$grandtotal), 'class' => 'form-control  input-currency', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
                                        </div>
                                    </div>


                                </div>


                                <div class="form-group col-md-12" style="font-weight:bold;padding:10px;border-top:2px solid #bcc0c6;"><h3><span class="label label-default">Cara Pembayaran</span></h3></div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Payment Method', "txt_id_payment_method", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_payment_method', 'class' => 'form-control', 'id' => 'dd_payment_method', 'placeholder' => 'Payment Method'), DefaultEmptyDropdown($list_payment_method, "json", "Payment Method"), @$id_payment_method); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 groupleasing" <?php if (@$id_payment_method != "2") echo "style='display:none'"; ?>>
                                    <?= form_label('Leasing', "txt_contact_person", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_leasing', 'class' => 'form-control', 'id' => 'dd_id_leasing', 'placeholder' => 'Leasing'), DefaultEmptyDropdown($list_leasing, "json", "Leasing"), @$id_leasing); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Contact Person Pembayaran', "txt_contact_person", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'contact_person', 'value' => @$contact_person, 'class' => 'form-control', 'id' => 'txt_contact_person', 'placeholder' => 'Contact Person Pembayaran')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <?= form_label('Tlp Contact Person Pembayaran', "txt_tlp_contact_person", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'tlp_contact_person', 'value' => @$tlp_contact_person, 'class' => 'form-control', 'id' => 'txt_tlp_contact_person', 'placeholder' => 'Tlp Contact Person Pembayaran')); ?>
                                    </div>
                                </div>
                                <div style="clear:both"></div>
                                <div class="form-group" style="margin-left:10px;">
                                <?php  include APPPATH . 'modules/module/views/upload_index.php';?>
                                </div>
                                <div class="form-group col-md-12">
                                    <a href="<?php echo base_url() . 'index.php/prospek' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    var list_master_equipment =<?php echo json_encode(@$list_master_equipment) ?>;
    function RefreshPPN()
    {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").removeAttr("readonly");
        } else
        {
            $("#txt_ppn").attr("readonly", "readonly");
        }
    }
    $("#dd_type_ppn").change(function () {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").val(11);
        } else
        {
            $("#txt_ppn").val(0);
        }
        RefreshPPN();
        calcEquip();
    })

    $(document).ready(function () {
        $("select").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $(".datepicker").datepicker();
        RefreshPPN();

    })
    var arr_price = <?php echo json_encode($price); ?>;
    var cnt = <?php echo count(@$list_equipment); ?>;
    var option_unit = '<?php echo $option_unit ?>';

    $('#dd_id_customer').change(function () {
        getDataCustomer($(this).val());
    })
    
    $('#dd_id_unit_bus').change(function () {
        var id_unit = $(this).val();
        if (id_unit != 0)
        {
            $.ajax({
                type: 'post',
                url: baseurl + 'index.php/unit/get_one_unit',
                dataType: 'json',
                data: {
                    id_unit: id_unit
                },
                success: function (data) {
                    if (data.st)
                    {
                        $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                        $("#dd_id_type_body").val(data.obj.id_type_body);
                        $("#txt_nama_kategori").val(data.obj.nama_kategori);
                        $("#txt_harga_off_the_road").val(Comma(data.obj.price));
                        $("#txt_biaya_bbn").val(Comma(data.obj.bbn));
                    } else
                    {
                        $("#txt_nama_type_unit").val("");
                        $("#txt_nama_type_body").val("");
                        $("#txt_nama_kategori").val("");
                        $("#txt_biaya_bbn").val("0");
                    }
                    UpdateHarga();
                },
                error: function (xhr, status, error) {
                    messageerror(xhr.responseText);
                }
            });
        } else
        {
            $("#txt_nama_type_unit").val("");
            $("#txt_nama_type_body").val("");
            $("#txt_nama_kategori").val("");
            $("#txt_harga_off_the_road").val("0");
            UpdateHarga();
        }


    })

    $(window).on('load', function () {
        var idProspek = $('#id_prospek').val();
        var idCust = $('#dd_id_customer').val();
        $("#txt_tanggal").on('dp.change', function (e) {
            $("#txt_jth_tempo").val(DefaultDateFormat(e.date.addMonths(1)));
        });
        if (idProspek != '') {
            getDataCustomer(idCust);
            calcEquip();
            <!-- $("#txt_no_prospek").attr("disabled","disabled");-->
        }
        
    })

    $('#txt_jumlah_unit').keyup(function () {
        calcEquip();
    })
    function UpdateHarga()
    {
        Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));
        let hrg_off_the_road = Number($("#txt_harga_off_the_road").val().replace(/[^0-9\.]+/g, ""));
        let biaya_bbn = Number($("#txt_biaya_bbn").val().replace(/[^0-9\.]+/g, ""));
        let hrg_on_the_road = hrg_off_the_road + biaya_bbn
        $('#txt_harga_on_the_road').val(Comma(hrg_on_the_road));
        $('#txt_harga_jual_unit').val(Comma(hrg_on_the_road));
        calcEquip();
    }
    $("#dd_payment_method").change(function () {
        if ($(this).val() == "2")
        {
            $(".groupleasing").css("display", "block");
        } else
        {
            $(".groupleasing").css("display", "none");
        }
    })
    $('.calculate').keyup(function () {
        UpdateHarga();
    })

    function getDataCustomer(idCust) {
        var idProspek = $('#id_prospek').val();
        $.ajax({
            type: 'get',
            url: baseurl + 'index.php/prospek/getDataCustomer',
            dataType: 'json',
            data: {
                id_cust: idCust
            },
            success: function (data) {
                $('#alamat_cust').val(data.alamat);
                $('#no_tlp_cust').val(data.no_telp + ' / ' + data.fax);
                $('#no_npwp_cust').val(data.npwp);
                $('#nama_npwp_cust').val(data.nama_npwp);
                $('#alamat_npwp_cust').val(data.alamat_npwp);
                $('#contact_person_cust').val(data.contact_person);
                $('#telp_cp_cust').val(data.no_telp_2);
                if(idProspek == '') {
                    $('#nama_stnk').val(data.nama_customer);
                    $('#alamat_stnk').html(data.alamat);
                    $('#tdp_prospek').val(data.tdp);
                    $('#no_ktp_prospek').val(data.no_ktp);
                }
                
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }

    function getPrice(dataEquip) {
        let indexEquip = $('#' + dataEquip.id).parent('div').parent('div').attr('id');
        let id = dataEquip.value;
        arr_price.forEach(function (item, index) {
            if (item.id == id) {
                $('#' + indexEquip).children('div').eq(1).children('div').children('input').val(Comma(item.price));
                this.calcEquip();
            }
        })
    }

    function addEquipment() {
        $.ajax({
            type: 'post',
            url: baseurl + 'index.php/prospek/add_detail',
            data: {
                cnt: cnt
            },
            success: function (data) {
                $('#equipments').append(data);
                $("#row" + cnt + " select").select2({data: list_master_equipment});
                
                cnt++;
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    }

// $('.equip').keyup(function () {
// 	let totalEquip = 0;
// 	let hrg_on_the_road = $("#txt_harga_on_the_road").val().replace(/\,/g,'');
// 	$(this).each(function () {
// 		totalEquip += $(this).val().replace(/\,/g,'');
// 	});
// 	$('#txt_harga_jual_unit').val(Comma(hrg_on_the_road + totalEquip));
// })

    function calcEquip() {
        let totalEquip = 0;
        let qty = $('#txt_jumlah_unit').val();
        let hitunganppn = 0;
        let nominalppn = 0;
        let hrg_on_the_road = Number($("#txt_harga_on_the_road").val().replace(/\,/g, ''));
        let hrg_off_the_road = Number($("#txt_harga_off_the_road").val().replace(/\,/g, ''));
        hitunganppn = hrg_off_the_road;
        let indexid = "";
        $('.equip').each(function () {
            indexid = $(this).attr("id").replace("hrgEquip", "");
            if ($("#cb_is_ppn_" + indexid).is(":checked"))
            {
                hitunganppn += Number($("#hrgEquip"+indexid).val().replace(/\,/g, ''));
            }
            totalEquip += Number($("#hrgEquip"+indexid).val().replace(/\,/g, ''));
            $("#hrgEquip"+indexid).val(Comma($("#hrgEquip"+indexid).val()));
        });
        let harga_jual_unit = parseInt(hrg_on_the_road) + totalEquip;
        let total_harga_jual = harga_jual_unit * qty;
        hitunganppn=hitunganppn*qty;
        $('#txt_harga_jual_unit').val(number_format(harga_jual_unit));
         


        var ppn = Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));


        var grandtotal = total_harga_jual;


        if ($("#dd_type_ppn").val() == "2")
        {
            nominalppn = ppn / 100 * hitunganppn;
            grandtotal = grandtotal + nominalppn;
        } else if ($("#dd_type_ppn").val() == "1")
        {
            nominalppn = ppn / (100 + Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""))) * hitunganppn;
            total_harga_jual = total_harga_jual - nominalppn;
        }
        $('#total_harga_jual').val(number_format(total_harga_jual.toFixed(4)));
        $("#txt_grandtotal").val(number_format(grandtotal.toFixed(4)));
        $("#txt_nominal_ppn").val(number_format(nominalppn.toFixed(4)));



    }

    function deleteEquipment(index) {
        if (confirm('Hapus Perlengkapan Tambahan ini?')) {
            $("#row" + index).remove();
            this.calcEquip();
        }
    }
    $(".top").on('keyup', function () {
        if ($(this).val() == '') {
            $('#txt_dnp').val(0);
            return false;
        }
    });

    $("#frm_prospek").submit(function () {
        let listobject=[];
        var indexid="";
        $('.equip').each(function () {
            indexid = $(this).attr("id").replace("hrgEquip", "");
            listobject.push($("#cb_is_ppn_" + indexid).is(":checked")?1:0);
        });
        swal({
            title: "Apakah kamu yakin ingin menginput data Prospek berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/prospek/prospek_manipulate',
                    dataType: 'json',
                    data: $(this).serialize()+ '&' + $.param({is_ppn_detail:listobject,listimage:listimage}),
                    success: function (data) {
                        if (data.st)
                        {
                            window.location.href = baseurl + 'index.php/prospek';
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });




        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
    .input-currency {
        border: none;
        border-bottom: 1px dotted #000000;
        text-align: right;
        background-color: #FFFFFF;
    }
    .span-currency {
        border: none;
    }
</style>