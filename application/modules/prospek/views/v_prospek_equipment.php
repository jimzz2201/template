
<hr/>

<div class="container">

    <?php if (count(@$list_equpment) > 0) { ?>
        <div class="row justify-content-center">
        <div class="col-sm-12">
            <table class="table table-striped table-hover dataTable no-footer" >
                <tr>
                    <th colspan="2" style="text-align:center;">Perlengkapan Tambahan</th>
                </tr>
                <tr>
                    <th style="width:50px;">No</th>
                    <th>Item</th>
                </tr>
                <?php foreach (@$list_equpment as $key => $equip) { ?>
                    <tr>
                        <td style="width:50px;"><?= $key + 1 ?></td>
                        <td><?= $equip->nama_unit ?></td>
                    </tr>
                <?php }
                ?>
            </table>
        </div>
        </div>
    <?php } ?>

    <div class="row justify-content-center">
    <div class="col-sm-12">
        <table class="table table-striped table-hover dataTable no-footer" >
            <tr>
                <th colspan="4" style="text-align:center;">History Pembayaran</th>
            </tr>
            <tr>
                <th style="width:50px;">No</th>
                <th>Tanggal</th>
                <th>Dokumen</th>
                <th style="width:150px;">Jumlah Bayar</th>
            </tr>
            <?php
            if (count(@$listpembayaran) > 0) {
                foreach (@$listpembayaran as $key => $equip) {
                    ?>
                    <tr>
                        <td style="width:50px;"><?= $key + 1 ?></td>
                        <td><?= DefaultTanggal($equip->tanggal_transaksi) ?></td>
                        <td><?= $equip->dokumen ?></td>
                        <td style="text-align:right"><?= DefaultCurrency($equip->jumlah_bayar) ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td colspan="4" style="text-align:center">Tidak Ada Data Pembayaran</td>
                </tr>
            <?php }
            ?>
        </table>
    </div>
    </div>
</div>
<hr/>
