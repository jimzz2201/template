
<section class="content-header">
    <h1>
        Prospek
        <small>Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">Prospek</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
           
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                         <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), @$list_pencarian); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                        </div>

                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'selected' => @$id_customer, 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'selected' => @$status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                        </div>
                     
                       
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_cabang', 'selected' => @$id_cabang, 'class' => 'form-control select2', 'id' => 'dd_id_cabang', 'placeholder' => 'Cabang'), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'level', 'selected' => @$level -1 , 'class' => 'form-control select2', 'id' => 'dd_level', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_level, "json", "Level")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                        <div class="col-sm-1">
                            <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                        </div>
                    </div>
                </div>
                
                
            </form>
            <hr/>
            <div class=" headerbutton">
                <?php echo anchor(site_url('prospek/create_prospek'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable"></table>
                </div>
            </div>
        </div>
    </div>
    
</section>

<script type="text/javascript">
    var table;
    var level= <?php echo CheckEmpty(@$level)?0:@$level?>;
    var userid=<?php echo GetUserId() ?>;
    function batalprospek (id_prospek) {
        swal({
            title: "Apa Kamu yakin ingin membatalkan prospek berikut?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/prospek/prospek_batal',
                    dataType: 'json',
                    data: {
                        id_prospek: id_prospek
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
    }
    
    function ubahstatus(id, status)
    {
        swal({
            title: "Apakah kamu yakin ingin " + (status == "1" ? "Mengapprove" : status == "2" ? "Mereject" : "Menutup") + " Prospek Berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                if (status == 2){
                    swal({
                        title: "Keterangan Reject.",
                        text: "Masukan alasan mereject prospek:",
                        input: "textarea",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        confirmButtonText: "Reject",
                        inputPlaceholder: "Write something",
                        inputValidator: (value) => {
                            if (!value) {
                                return 'Anda harus memasukan alasan!'
                            }
                        }
                    }).then((result) => {
                        approval(id, status, result.value)
                    });
                } else {
                    approval(id, status)
                }
            }
        });
    }
    function approval(id, status, keterangan = null){
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/prospek/prospek_change_status',
            dataType: 'json',
            data: {
                id_prospek: id,
                status: status,
                keterangan: keterangan
            },
            success: function (data) {
                if (data.st) {
                    RefreshGrid();
                    messagesuccess(data.msg);
                } else {
                    messageerror(data.msg);
                }
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    function negoprospek($id_prospek, $status)
    {
        $.ajax({url: baseurl + 'index.php/prospek/show_nego_form/'+$id_prospek,
            success: function (data) {
                modalbootstrap(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }
    
    function adjustprospek(id_prospek, status)
    {
        $.ajax({
            url: baseurl + 'index.php/adjustment_prospek/create_adjustment_prospek/',
            data: {
                id_prospek: id_prospek
            },
            type: 'GET',
            success: function (data) {
                modalbootstrap(data);
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }

    function RefreshGrid()
    {
        table.fnDraw(false);
    }
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $(document).ready(function () {
        $("select").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $(".datepicker").datepicker();
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            searching: false,
            scrollX: true,
            ajax: {"url": "prospek/getdataprospek", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_prospek",
                    title: "No",
                    orderable: false
                },
                {data: "created_date", orderable: false, title: "Tanggal Buat",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
				 {data: "no_prospek", orderable: true, title: "No&nbsp;Prospek"},
                {data: "tanggal_prospek", orderable: false, title: "Tanggal SPK",
                    mRender: function (data, type, row) {
                        return  DefaultDateFormat(data);
                    }},
                {data: "nama_customer", orderable: true, title: "Customer"},
                {data: "nama_cabang", orderable: false, title: "Cabang"},
                {data: "nama_unit", orderable: false, title: "Unit"},
                {data: "jumlah_unit", orderable: false, title: "Qty", "className": "text-center", "width": "20px"},
                {data: "nomor_buka_jual", orderable: false, title: "Buka Jual"},
                {data: "tahun", orderable: true, title: "Tahun"},
                {data: "harga_jual_unit", orderable: false, title: "Harga Jual Unit", "className": "text-right",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "nama_payment_method", orderable: true, title: "Payment Method"},
                {data: "nama_pegawai", orderable: true, title: "Salesman"},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        var returnhtml="";
                        switch(data) {
                            case "1":
                                returnhtml=  "Approved";
                                break;
                            case "2":
                                returnhtml= "Rejected";
                                break;
                            case "3":
                                returnhtml= "Nego";
                                break;
                            case "4":
                                returnhtml= "Finished";
                                break;
                            case "5":
                                returnhtml= "Batal";
                                break;
                            default:
                                returnhtml="Pending";
                        }
                        if(data!=5)
                        {
                            returnhtml +' '+ (data==0?"Menunggu Approval "+row['nama_jabatan_approval']  : ' oleh '+ row['nama_jabatan_current']) ;
                        }
                        return returnhtml;
                    }
                },
                
                {
                    "data": "view",
                    "orderable": false,
                    "className": "text-center",
                    width: "200px",
                    mRender: function (data, type, row) {
                        
                          
                        var action = data;
                        if ((level) > row['level'] || (row['status']==0 && <?php echo GetGroupId()?>==1)){
                            action += row['approval'];
                        }
                        
                        if (((userid == row['created_by']||<?php echo GetGroupId()?>==1) && (row['status']==0)))
                        {
                            action += row['edit']  ;
                        }
                        else if(row['status']==1||row['status']==3)
                        {
                            action += row['revisi'];
                        }
                        
                        if(row['nomor_buka_jual'] ==null&& row['status']!=5)
                        {
                              action += row['batal']  ;
                        }
                         
                        return action;
                    }}
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
