<div class="modal-header">
    Nego Prospek <?php echo @$button ?>
</div>
<div class="modal-body">
<form id="frm_nego" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_prospek" value="<?php echo @$id_prospek; ?>" /> 
	<div class="form-group">
		<?= form_label('Estimasi Harga', "txt_estimasi_harga", array("class" => 'col-sm-4 control-label')); ?>
		<div class="col-sm-8">
            <div class='input-group'>
                <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                <?= form_input(array('type' => 'text', 'name' => 'estimasi_harga', 'value' => DefaultCurrency(0), 'class' => 'form-control input-currency', 'id' => 'txt_estimasi_harga', 'placeholder' => 'Estimasi Harga', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
            </div>
		</div>
	</div>
    <div class="form-group">
        <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-4 control-label')); ?>
        <div class="col-sm-8">
            <?= form_input(array('type' => 'text', 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
        </div>
    </div>
	<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
    </div>
</form>
</div>

<script>
    $("#frm_nego").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/prospek/nego_prospek',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    messagesuccess(data.msg);
                    table.fnDraw(false);
                    $("#modalbootstrap").modal("hide");
                }
                else
                {
                    modaldialogerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                modaldialogerror(xhr.responseText);
            }
        });
        return false;
    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>