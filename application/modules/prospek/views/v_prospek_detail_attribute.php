    <div id='row<?php echo @$cnt ?>'>

        <div class='form-group col-md-12' >
            <div class='col-sm-3'>
                <input type='hidden'  id='id_equip<?php echo @$cnt ?>' value='<?php echo @$dataEq['id_add_equipment'] ?>' name='id_add_equipment[]' >
                <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit[]', 'onchange' => "getPrice(this)", 'class' => 'form-control', 'id' => 'dd_unit' . @$cnt, 'placeholder' => 'Pilih Attribute Tambahan'), DefaultEmptyDropdown(@$list_unit, "json", "Attribute Tambahan"), @$dataEq['id_unit']); ?>
            </div>
            <div class='col-sm-5'>
                <div class='input-group'>
                    <span class='input-group-addon span-currency' id='basic-addon1'>Rp.</span>
                    <?= form_input(array('type' => 'text', 'name' => 'equipment_price[]', 'aria-describedby' => 'basic-addon1', 'value' => DefaultCurrency(@$dataEq['harga']), 'class' => 'form-control equip col-sm-5 input-currency', 'id' => 'hrgEquip' . $cnt, 'placeholder' => '', 'onkeyup' => 'javascript:this.value = Comma(this.value);calcEquip();', 'onkeypress' => 'return isNumberKey(event)')); ?>
                </div>
            </div>
            <?= form_label('PPN', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
            <div class="col-sm-1">
                <?php if (CheckEmpty(@$is_view)) { ?>
                    <input type="checkbox"<?php echo!CheckEmpty(@$dataEq['is_ppn_detail']) ? "Checked" : "" ?>  id="cb_is_ppn_<?php echo @$cnt ?>" class="minimal" >
                    <?php
                } else {
                    echo "<button type='button' id='buttonppndetail" . @$cnt . "' onclick='javascript:;'" . (CheckEmpty(@$dataEq['is_ppn_detail']) ? "class='btn btn-danger'>Tidak" : "class='btn btn-success'>Ya") . "</button>";
                }
                ?>
            </div>
            <?php if (CheckEmpty(@$is_view)) { ?>
                <div class='col-sm-1'>
                    <button type='button' class='button btn-danger' onclick='deleteEquipment(<?php echo @$cnt ?>)'>Delete</button> </button>
                </div>
            <?php } ?>
        </div>
        <div class='form-group col-md-8'>
            <div class='col-sm-12'>
                <?= form_textarea(array('name' => 'keterangan_detail[]', 'value' => @$contact_person, 'class' => 'form-control', 'id' => 'keterangan_detail' . $cnt, 'placeholder' => '')); ?>
            </div>
        </div>

    </div>
    <script>
        $(document).ready(function () {
    <?php if (CheckEmpty(@$is_view)) { ?>
                $("#cb_is_ppn_<?php echo @$cnt ?>").iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                })
                $("#cb_is_ppn_<?php echo @$cnt ?>").on('ifChanged', function (event) {
                    calcEquip();
                });
    <?php } ?>
        })
    </script>