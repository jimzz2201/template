<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Prospek extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        $this->load->model('m_prospek');
        $this->load->model("warna/m_warna");
        $this->load->model("jenis_plat/m_jenis_plat");
        $this->load->model("customer/m_customer");
        $this->load->model("kategori/m_kategori");
        $this->load->model("type_body/m_type_body");
        $this->load->model("type_unit/m_type_unit");
        $this->load->model("payment_method/m_payment_method");
        $this->load->model("unit/m_unit");
        $this->load->model("pegawai/m_pegawai");
        $this->load->model("leasing/m_leasing");
        $this->load->model("user/m_user");
        $this->load->model("cabang/m_cabang");

        require APPPATH . 'libraries/phpmailer/src/Exception.php';
        require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH . 'libraries/phpmailer/src/SMTP.php';
    }
    
    
    public function add_detail() {
        $model = $this->input->post();
        $this->load->view("v_prospek_detail_attribute", $model);
    }
    
     public function create_prospek_api() {
        // $module = "K063";
        // $header = "K059";
        // CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['tanggal_prospek'] = GetDateNow();
        $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
        array_unshift($row['list_jenis_plat'], array('id' => 0, 'text' => 'Pilih Jenis Plat'));
        $row['list_warna'] = $this->m_warna->GetDropDownWarna();
        array_unshift($row['list_warna'], array('id' => 0, 'text' => 'Pilih Warna'));
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        array_unshift($row['list_customer'], array('id' => 0, 'text' => 'Pilih Customer'));
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        array_unshift($row['list_kategori'], array('id' => 0, 'text' => 'Pilih Kategori'));
        $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
        array_unshift($row['list_type_body'], array('id' => 0, 'text' => 'Pilih Type Body'));
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        array_unshift($row['list_type_unit'], array('id' => 0, 'text' => 'Pilih Type Unit'));
        $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
        array_unshift($row['list_payment_method'], array('id' => 0, 'text' => 'Pilih Paymen Method'));
        $row['list_unit'] = $this->m_unit->GetDropDownEquipment_api();
        array_unshift($row['list_unit'], array('id' => 0, 'text' => 'Pilih Unit', 'price' => 0));
        $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
        array_unshift($row['list_unit_bus'], array('id' => 0, 'text' => 'Pilih Unit Bus'));
        $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
        array_unshift($row['list_pegawai'], array('id' => 0, 'text' => 'Pilih Pegawai'));
        $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
        array_unshift($row['list_leasing'], array('id' => 0, 'text' => 'Pilih Leasing'));
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        array_unshift($row['list_cabang'], array('id' => 0, 'text' => 'Pilih Cabang'));

        echo json_encode($row);
    }


    public function prospek_history($id_prospek) {
        $row = $this->m_prospek->GetOneProspekHistory($id_prospek);

        $this->load->model("unit/m_unit");
        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K063";
            $header = "K059";
            $row['title'] = "View Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
            $row['list_warna'] = $this->m_warna->GetDropDownWarna();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
            $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $row['list_type_ppn'] = $list_type_ppn;
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'prospek/v_prospek_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Prospek tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }
    
    
    public function GenerateProspekUnit()
    {
        $this->db->from("#_prospek");
        $this->db->join("#_prospek_detail","#_prospek_detail.id_prospek=#_prospek.id_prospek");
        $this->db->join("#_prospek_detail_unit","#_prospek_detail_unit.id_prospek_detail=#_prospek_detail.id_prospek_detail","left");
        $this->db->where(array("#_prospek_detail_unit.id_prospek_detail_unit"=>null,"#_prospek.status"=>1));
        $this->db->group_by("#_prospek.id_prospek");
        $this->db->select("#_prospek.id_prospek");
        $list=$this->db->get()->result_array();
        $listid= array_column($list, "id_prospek");
        foreach ($listid as $keyid)
        {
            $this->m_prospek->ChangeStatus($keyid);
        }
        
        $this->db->from("#_prospek_detail_unit");
        $this->db->where(array("id_unit_serial"=>null));
        $listresult=$this->db->get()->result();
        
        foreach($listresult as $result_satuan)
        {
            $this->db->from("#_unit_serial");
            $this->db->where(array("id_prospek_detail"=>$result_satuan->id_prospek_detail));
            $this->db->where(" not exists(select #_prospek_detail_unit.id_unit_serial from #_prospek_detail_unit where #_prospek_detail_unit.id_unit_serial=#_unit_serial.id_unit_serial)",null,false);
            $row=$this->db->get()->row();
            if($row)
            {
                $update=[];
                $update['id_unit_serial']=$row->id_unit_serial;
                $this->db->update("#_prospek_detail_unit",$update,array("id_prospek_detail_unit"=>$result_satuan->id_prospek_detail_unit));
            }
           
        }
        
        
        
        
        
        
    }
    

    public function get_prospek_list() {
        header('Content-Type: application/json');
        @$id_customer = $this->input->post("id_customer");
        if (CheckEmpty(@$id_customer)) {
            @$id_customer = 0;
        }
        $list_prospek = $this->m_prospek->GetDropDownProspekfromCustomer(@$id_customer);
        echo json_encode(array("list_prospek" => $this->DefaultEmptyDropdown_int($list_prospek, "json", "Prospek"), "st" => true, "obj" => $this->m_customer->GetOneCustomer($id_customer, "id_customer", false)));
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K063";
        $header = "K059";
        $title = "Prospek";

        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->model("cabang/m_cabang");
        CekModule($module);
        $this->load->model("customer/m_customer");
        $userid = GetUserId();
        $model['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $model['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", $userid);
        $status[] = array();
        $status[] = array("id" => "6", "text" => "Pending");
        $status[] = array("id" => "1", "text" => "Approved");
        $status[] = array("id" => "2", "text" => "Rejected");
        $status[] = array("id" => "3", "text" => "Nego");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $status[] = array("id" => "7", "text" => "Approved Sudah Di Buka Jual");
        $status[] = array("id" => "8", "text" => "Approved Belum Di Buka Jual");

        $jenis_pencarian = [];
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal_prospek", "text" => "Tanggal SPK");
        $model['list_pencarian'] = $jenis_pencarian;
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['status'] = '6';
        $model['list_level'] = GetLevel();
        $level = GetLevelJabatan();
        if ($level == 1) {
            $model['status'] = '0';
        }
        $model['level'] = $level;
        $css[] = "assets/plugins/select2/select2.css";
        $sufix = "_prospek";
        $model['start_date'] = GetCookieSetting("start_date" . $sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $sufix, GetDateNow());
        $model['status'] = GetCookieSetting("status" . $sufix, GetCookieSetting("search".$sufix)=="1"?0:7);
        $model['id_cabang'] = GetCookieSetting("id_cabang" . $sufix, 0);
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian_prospek");
        

        LoadTemplate($model, "prospek/v_prospek_index", $javascript, $css);
    }

    public function prospek_change_status() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Prospek', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            if ($this->cekAksesApproval($model["id_prospek"])) {
                $data = $this->m_prospek->ChangeStatus($model["id_prospek"], $model['status'], $model['keterangan']);
                echo json_encode($data);
            } else {
                echo json_encode(array('st' => false, 'msg' => 'Anda tidak dapat mengapprove prospek ini!'));
            }
        }
    }

    function cekAksesApproval($id_prospek) {
        $allowed = true;
        $prospek = $this->db->get_where("#_prospek", array("id_prospek" => $id_prospek))->row();
        $user = $this->db->get_where("#_user", array("id_user" => GetUserId()))->row();
        $arr_cabang = explode(',', $user->arr_cabang);
        if ($prospek->status > 1) {
            $allowed = false;
        }
        if ($user->arr_cabang != 'all') {
            if ($user->id_cabang != $prospek->id_cabang) {
                if (!in_array($prospek->id_cabang, $arr_cabang)) {
                    $allowed = false;
                }
            }
        }
        if ($prospek->level + 1 != GetLevelJabatan()) {
            $allowed = false;
        }

        return $allowed;
    }

    public function get_one_prospek() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Prospek', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_prospek->GetOneProspek($model["id_prospek"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Prospek tidak ditemukan dalam database" : "";
            if (CheckEmpty(@$model['typehtml'])) {
                $data['html'] = $this->load->view("prospek/v_prospek_equipment", $data['obj'], true);
            } else if (@$model['typehtml'] == "propekcontent") {
                $this->load->model("unit/m_unit");
                $data['prospek'] = $data['obj'];
                $data['list_unit'] = $this->m_unit->GetDropDownEquipment();

                $data = json_decode(json_encode($data), true);
               
                
                $list_type_ppn = array();
                $list_type_ppn[1] = ['id' => 1, 'text' => 'Include'];
                $list_type_ppn[2] = ['id' => 2, 'text' => 'Exclude'];
                $data['list_type_ppn'] = $list_type_ppn;
                $data['html'] = $this->load->view("buka_jual/v_buka_jual_prospek_content", $data, true);
            } else {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_piutang_content", $data['obj'], true);
            }
            echo json_encode($data);
        }
    }

    public function getSelProspek() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'id prospek', 'trim|required');
        $message = '';
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_prospek->getSelectedProspek($model['id_prospek']);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Prospek tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }

    public function getdataprospek() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");
        parse_str($str, $params);
        $data = json_decode($this->m_prospek->GetDataprospek($params));
        foreach ($params as $key => $value) {
            if($key=="status"&&$value==0)
            {
                SetCookieSetting( "search_prospek", 1);
            }
            SetCookieSetting($key . "_prospek", $value);
        }
        foreach ($data->data as $val) {
            $val->allowed_approval = $this->cekAksesApproval($val->id_prospek);
            $val->editable = ($val->created_by == GetUserId() && ($val->status == 0 || $val->status == 3)) ? true : false;
        }
		

        echo json_encode($data);
    }

   

    public function sendResponse($msg, $data = null, $status) {
        echo json_encode([
            "msg" => $msg,
            "data" => $data,
            'api_token' => '',
            'expired_at' => ''
                ], $status);
    }

    public function getDataProspekSelect() {
        header('content-type: applicaation/json');
        echo $this->m_prospek->getDataProspekSelect();
    }

    public function selectProspek() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K063";
        $header = "K059";
        $title = "Prospek";
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $this->load->view("prospek/v_select_prospek", $model);
    }

    public function getDataCustomer() {
        $id_cust = $this->input->get('id_cust');
        // header('Content-Type: application/json');
        $res = $this->m_customer->GetOneCustomer($id_cust);
        echo $json = json_encode($res);
    }

    public function create_prospek() {
        $row = ['button' => 'Add'];
        $javascript = array();
        $module = "K063";
        $header = "K059";
        $row['title'] = "Add Prospek";
        $this->load->model("cabang/m_cabang");
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $row['tanggal_prospek'] = GetDateNow();
        $row['listimage'] = [];
        $this->load->model("pegawai/m_pegawai");
        $userid = GetUserId();
        $pegawai = $this->m_pegawai->GetOnePegawai($userid, "#_pegawai.id_user");
        if ($pegawai) {
            $row['id_pegawai'] = $pegawai->id_pegawai;
            $row['id_cabang'] = $pegawai->id_cabang;
        }
        $row['list_top'] = array(array("id" => 0, "text" => "0"), array("id" => 30, "text" => "30"), array("id" => 45, "text" => "45"), array("id" => 60, "text" => "60"), array("id" => 90, "text" => "90"));



        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $row['list_equipment'] = [];
        $row['type_upload'] = "Prospek";
        $row['list_master_equipment'] = $this->m_unit->GetDropDownUnit(array("id_tipe_persediaan" => [3,5]));
        $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
        $row['list_warna'] = $this->m_warna->GetDropDownWarna();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
        $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
        $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
        $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
        $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
        $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
        $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang("json", $userid);
        $javascript[] = "assets/plugins/select2/select2.js";
        $javascript[] = "assets/plugins/iCheck/icheck.js";
        $row['top'] = '30';
        $row['jth_tempo'] = AddDays(DefaultTanggalDatabase(@$row['tanggal_prospek']), '+30 day');
        $css[] = "assets/plugins/select2/select2.css";
        $css[] = "assets/plugins/iCheck/all.css";
        LoadTemplate($row, 'prospek/v_prospek_manipulate', $javascript, $css);
    }

    

    public function prospek_manipulate() {
        $message = '';

        $this->form_validation->set_rules('id_customer', 'Customer', 'trim|required');
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('id_jenis_plat', 'Jenis Plat', 'trim|required');
        $this->form_validation->set_rules('jumlah_unit', 'Jumlah Unit', 'trim|required');
        $this->form_validation->set_rules('id_unit_bus', 'Unit', 'trim|required');
        $this->form_validation->set_rules('id_warna', 'Warna', 'trim|required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required');
        $this->form_validation->set_rules('harga_off_the_road', 'Harga Off The Road', 'trim|required|numeric');
        $this->form_validation->set_rules('id_payment_method', 'Payment Method', 'trim|required|numeric');
        $this->form_validation->set_rules('contact_person', 'Contact Person', 'trim|required');
        $this->form_validation->set_rules('tlp_contact_person', 'Tlp Contact Person', 'trim|required');
        $model = $this->input->post();

        if (CheckEmpty(@$model['id_prospek'])) {
            $this->form_validation->set_rules('no_prospek', 'No SPK', 'trim|required|is_unique[#_prospek.no_prospek]');
        }

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status = $this->m_prospek->ProspekManipulate($model);

            //$email = $this->send_email($status['id_prospek']);
            echo json_encode($status);
        }
    }

    public function cek_template() {
        $data = $this->m_prospek->GetOneProspek(4);
        $this->load->view('prospek/v_prospek_email', $data);
    }

    public function edit_prospek($id = 0) {
        $row = $this->m_prospek->GetOneProspek($id);
        
        if ($row&&($row->status=="0"||$row->status=="3")) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K063";
            $header = "K059";
            $row['title'] = "Edit Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['type_upload'] = "Prospek";
            $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
            $row['list_warna'] = $this->m_warna->GetDropDownWarna();
            $this->db->where("id_customer", $row['id_customer']);
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_master_equipment'] = $this->m_unit->GetDropDownUnit(array("id_tipe_persediaan" => 3));
            $row['list_top'] = array(array("id" => 0, "text" => "0"), array("id" => 30, "text" => "30"), array("id" => 45, "text" => "45"), array("id" => 60, "text" => "60"), array("id" => 90, "text" => "90"));
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $row['list_type_ppn'] = $list_type_ppn;
            $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
            $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
            $javascript[] = "assets/plugins/iCheck/icheck.js";

            $row['list_user'] = $this->m_user->GetDropDownUser();
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
            $css[] = "assets/plugins/iCheck/all.css";
            LoadTemplate($row, 'prospek/v_prospek_manipulate', $javascript, $css);
        } else {
            SetMessageSession(2, "Prospek cannot be found in database");
            redirect(site_url('prospek'));
        }
    }
    public function revisi_prospek($id = 0) {
        $row = $this->m_prospek->GetOneProspek($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K063";
            $header = "K059";
            $row['title'] = "Edit Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['type_upload'] = "Prospek";
            $row['type_action'] ="revisi";
            $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
            $row['list_warna'] = $this->m_warna->GetDropDownWarna();
            $this->db->where("id_customer", $row['id_customer']);
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_master_equipment'] = $this->m_unit->GetDropDownUnit(array("id_tipe_persediaan" => [3,5]));
            $row['list_top'] = array(array("id" => 0, "text" => "0"), array("id" => 30, "text" => "30"), array("id" => 45, "text" => "45"), array("id" => 60, "text" => "60"), array("id" => 90, "text" => "90"));
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $row['list_type_ppn'] = $list_type_ppn;
            $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
            $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
            $javascript[] = "assets/plugins/iCheck/icheck.js";

            $row['list_user'] = $this->m_user->GetDropDownUser();
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
            $css[] = "assets/plugins/iCheck/all.css";
            LoadTemplate($row, 'prospek/v_prospek_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Prospek cannot be found in database");
            redirect(site_url('prospek'));
        }
    }

    public function edit_prospek_api($id = 0) {
        $row = $this->m_prospek->GetOneProspek($id);

        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K063";
            $header = "K059";
            $row['title'] = "Edit Prospek";
            // CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
            $row['list_warna'] = $this->m_warna->GetDropDownWarna();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
            $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
            $this->db->where('id_prospek', $id);
            $this->db->where('deleted_date', null);
            $row['equipment'] = $this->db->get('dgmi_prospek_equipment')->result_array();
            $row['list_user'] = $this->m_user->GetDropDownUser();
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();

            echo json_encode(array('st' => true, 'msg' => '', 'data' => $row));
        } else {
            echo json_encode(array('st' => false, 'msg' => "Prospek cannot be found in database", 'data' => null));
        }
    }

    public function prospek_detail($id = 0) {
        $row = $this->m_prospek->GetOneProspek($id);

        if ($row) {
            $row = json_decode(json_encode($row), true);
            $module = "K063";
            $header = "K059";
            // CekModule($module);
            $this->db->where('dgmi_prospek_equipment.id_prospek_detail', $row['id_prospek_detail']);
            $this->db->where('dgmi_prospek_equipment.deleted_date', null);
            $this->db->join('dgmi_unit', 'dgmi_unit.id_unit=dgmi_prospek_equipment.id_unit');
            $this->db->select('dgmi_prospek_equipment.*, nama_unit');
            $row['equipment'] = $this->db->get('dgmi_prospek_equipment')->result_array();

            echo json_encode(array('st' => true, 'msg' => '', 'data' => $row));
        } else {
            echo json_encode(array('st' => false, 'msg' => "Prospek cannot be found in database", 'data' => null));
        }
    }

    public function prospek_batal() {
        $message = '';
        $this->form_validation->set_rules('id_prospek', 'Prospek', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_prospek->ProspekBatal($model['id_prospek']);
        }

        echo json_encode($result);
    }

    public function DefaultEmptyDropdown_int($data, $type = "obj", $text = "") {


        if (CheckEmpty($data)) {
            $data = array();
        }
        if ($type == "obj" || $type == "") {
            $data = array("0" => CheckEmpty($text) ? "" : "Pilih " . $text) + $data;
        } else {
            $inserted = array(array("id" => 0, "text" => CheckEmpty($text) ? "" : "Pilih " . $text));
            $data = array_merge($inserted, $data);
        }


        return $data;
    }

    public function send_email_old($id_prospek) {
        $data = $this->m_prospek->GetOneProspek($id_prospek);
        $data_level = $data->level == '' || $data->level == null ? 0 : $data->level;
        $this->db->where('id_cabang', $data->id_cabang);
        $this->db->where('`level` >', $data_level);
        $this->db->join('#_pegawai', '#_pegawai.id_user=#_user_cabang.id_user', 'left');
        $data_user = $this->db->get('#_user_cabang')->result();
        if ($data_user && count($data_user) > 0) {
            foreach ($data_user as $dt) {
                /*
                  $config = Array(
                  'protocol' => 'smtp',
                  'smtp_host' => 'ssl://smtp.googlemail.com',
                  // 'smtp_host' => 'ssl://smtp.mail.yahoo.com',
                  'smtp_port' => 465,
                  'smtp_user' => 'nizarnur.ind@gmail.com',
                  'smtp_pass' => 'transformer19',
                  'mailtype' => 'html',
                  'charset' => 'iso-8859-1'
                  );
                  $this->load->library('email', $config);
                  $this->email->set_newline("\r\n");
                  $this->email->from('nizarnur.ind@gmail.com', 'Anizar');
                  $this->email->to($dt->email_pegawai);
                  $this->email->subject('Prospek Baru');
                  // $this->email->message('Ini adalah email percobaan untuk Aprroval pada DGMI');
                  $message = '';
                  $message .= $this->load->view('prospek/v_prospek_email', $data, TRUE);
                  $this->email->message($message);
                  if (!$this->email->send()) {
                  show_error($this->email->print_debugger());
                  }else{
                  // echo 'Success to send email';
                  return true;
                  }
                 */
                $data_array = array(
                    "body" => "Selamat Sore   \\\\n <br/>     test test",
                    "phone" => "6285624849794"
                );

                $messagespj .= "test";
                $data_array['body'] = $messagespj;
                $make_call = callAPI('POST', 'https://eu76.chat-api.com/instance92034/sendMessage?token=7r6bqookbg9o446z', json_encode($data_array));
                $response = json_decode($make_call, true);
            }
        } else {
            return true;
        }
    }

    function send_email($id_prospek) {
        $data = $this->m_prospek->GetOneProspek($id_prospek); //echo "<pre>";print_r($data);exit;
        $data_level = $data->level == '' || $data->level == null ? 0 : $data->level;
        $this->db->where('id_cabang', $data->id_cabang);
        $this->db->where('`level` >', $data_level);
        $this->db->join('#_pegawai', '#_pegawai.id_user=#_user_cabang.id_user', 'left');
        $data_user = $this->db->get('#_user_cabang')->result();
        $data_array = array(
            "body" => "Propek dengan nomor : " . $data->no_prospek . " telah ditambahkan di system dengan nominal : " . DefaultCurrency($data->harga_jual_unit) . " Customer : " . $data->nama_customer . " Sales : " . $data->nama_pegawai . " silahkan cek di aplikasi untuk melakukan approval , Tipe yang dibeli : " . $data->nama_unit,
            "phone" => "6281394002403"
        );

        $messagespj = "test";
        //$data_array['body'] = $messagespj;
        $make_call = callAPI('POST', 'https://eu76.chat-api.com/instance92034/sendMessage?token=7r6bqookbg9o446z', json_encode($data_array));
        $response = json_decode($make_call, true);

        $data_array = array(
            "body" => "Propek dengan nomor : " . $data->no_prospek . " telah ditambahkan di system dengan nominal : " . DefaultCurrency($data->harga_jual_unit) . " Customer : " . $data->nama_customer . " Sales : " . $data->nama_pegawai . " silahkan cek di aplikasi untuk melakukan approval , Tipe yang dibeli : " . $data->nama_unit,
            "phone" => "6285794793532"
        );

        $messagespj = "test";
        //$data_array['body'] = $messagespj;
        $make_call = callAPI('POST', 'https://eu76.chat-api.com/instance92034/sendMessage?token=7r6bqookbg9o446z', json_encode($data_array));
        $response = json_decode($make_call, true);
        if ($data_user && count($data_user) > 0) {
            foreach ($data_user as $dt) {
                /*
                  // PHPMailer object
                  $response = false;
                  $mail = new PHPMailer();

                  // SMTP configuration
                  // $mail->isSMTP();
                  $mail->SMTPOptions = array(
                  'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => true
                  )
                  );
                  $mail->Host     = 'ssl://smtp.googlemail.com';
                  // $mail->Host     = 'tls://smtp.gmail.com:587';
                  $mail->SMTPAuth = true;
                  $mail->Username = 'nizarnur.ind@gmail.com';
                  $mail->Password = 'transformer19';
                  $mail->SMTPSecure = 'ssl';
                  // $mail->SMTPSecure = 'tls';
                  $mail->Port     = 465;

                  $mail->setFrom('nizarnur.ind@gmail.com', 'DGMI');
                  $mail->addReplyTo('nizarnur.ind@gmail.com', '');

                  $mail->addAddress($dt->email_pegawai);

                  $mail->Subject = 'DGMI | Prospek Baru';

                  $mail->isHTML(true);
                  $mailContent = '';
                  $mailContent .= $this->load->view('prospek/v_prospek_email', $data, TRUE);
                  $mail->Body = $mailContent;

                  if(!$mail->send()){
                  echo 'Message could not be sent.';
                  echo 'Mailer Error: ' . $mail->ErrorInfo;
                  }else{
                  return true;
                  }
                 */
            }
        } else {
            return true;
        }
    }

    function send_email_mod($id_prospek) {
        $data = $this->m_prospek->GetOneProspek($id_prospek); //echo "<pre>";print_r($data);exit;
        $data_level = $data->level == '' || $data->level == null ? 0 : $data->level;
        $this->db->where('id_cabang', $data->id_cabang);
        $this->db->where('`level` >', $data_level);
        $this->db->join('#_pegawai', '#_pegawai.id_user=#_user_cabang.id_user', 'left');
        $data_user = $this->db->get('#_user_cabang')->result();
        // if ($data_user && count($data_user) > 0) {
        //     foreach($data_user as $dt){
        // PHPMailer object
        // $response = false;
        // $mail = new PHPMailer();
        // // SMTP configuration
        // $mail->isSMTP();
        // $mail->SMTPOptions = array(
        //     'ssl' => array(
        //         'verify_peer' => false,
        //         'verify_peer_name' => false,
        //         'allow_self_signed' => true
        //     )
        // );
        // $mail->Host     = 'ssl://smtp.googlemail.com';
        // // $mail->Host     = 'tls://smtp.gmail.com:587';
        // $mail->SMTPAuth = true;
        // $mail->Username = 'nizarnur.ind@gmail.com';
        // $mail->Password = 'icebear1919';
        // $mail->SMTPSecure = 'ssl';
        // // $mail->SMTPSecure = 'tls';
        // $mail->Port     = 465;
        // $mail->setFrom('nizarnur.ind@gmail.com', 'DGMI');
        // $mail->addReplyTo('nizarnur.ind@gmail.com', '');
        // $mail->addAddress('nizar.nursobah@gmail.com');
        // $mail->Subject = 'DGMI | Prospek Baru';
        // $mail->isHTML(true);
        // $mailContent = '';
        // $mailContent .= $this->load->view('prospek/v_prospek_email', $data, TRUE);
        // $mail->Body = $mailContent;
        // if(!$mail->send()){
        //     echo 'Message could not be sent.';
        //     echo 'Mailer Error: ' . $mail->ErrorInfo;
        // }else{
        //     echo 'berhasil';
        //     return true;
        // }


        $this->load->library("email");

        $this->email->from('nizarnur.ind@gmail.com', 'DGMI');
        $this->email->to('nizar.nursobah@gmail.com');
        $this->email->subject('DGMI | Prospek Baru');
        $this->email->message('test cek');
        $this->email->send();
        echo $this->email->print_debugger();


        // }
        // } else {
        //     return true;
        // }
    }

    public function view_prospek($id) {
        $row = $this->m_prospek->GetOneProspek($id);
        $this->load->model("unit/m_unit");
        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K063";
            $header = "K059";
            $row['title'] = "View Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
            $row['list_warna'] = $this->m_warna->GetDropDownWarna();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer(NULL, $row['id_customer']);
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
            $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $row['list_type_ppn'] = $list_type_ppn;
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            $css[] = "assets/plugins/iCheck/all.css";
            $javascript[] = "assets/plugins/iCheck/icheck.js";
            LoadTemplate($row, 'prospek/v_prospek_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Prospek tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

    public function approval_prospek($id) {
        $row = $this->m_prospek->GetOneProspek($id);
        $this->load->model("unit/m_unit");
        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K063";
            $header = "K059";
            $row['title'] = "Approval Prospek";
            CekModule($module);
            $row['form'] = $header;
            $row['is_view'] = 1;
            $row['formsubmenu'] = $module;
            $row['list_jenis_plat'] = $this->m_jenis_plat->GetDropDownJenis_plat();
            $row['list_warna'] = $this->m_warna->GetDropDownWarna();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer(NULL, $row['id_customer']);
            $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
            $row['list_type_body'] = $this->m_type_body->GetDropDownType_body();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit_bus'] = $this->m_unit->GetDropDownUnit();
            $row['list_pegawai'] = $this->m_pegawai->GetDropDownPegawai();
            $row['list_leasing'] = $this->m_leasing->GetDropDownLeasing();
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
            $row['list_payment_method'] = $this->m_payment_method->GetDropDownPayment_method();
            $row['list_unit'] = $this->m_unit->GetDropDownEquipment();
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $row['list_type_ppn'] = $list_type_ppn;
            $row['approvalhtml'] = $this->load->view("v_prospek_approval", [], true);
            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            $css[] = "assets/plugins/iCheck/all.css";
            $javascript[] = "assets/plugins/iCheck/icheck.js";
            LoadTemplate($row, 'prospek/v_prospek_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Prospek tidak ditemukan pada database");
            redirect(site_url('vrf'));
        }
    }

    public function prospek_approve() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Prospek', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_prospek->ApprovalProspek($model, "Approve");
            echo json_encode($data);
        }
    }

    public function prospek_reject() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Prospek', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_prospek->ApprovalProspek($model, "Reject");
            echo json_encode($data);
        }
    }

    public function prospek_nego() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Prospek', 'trim|required');
        $this->form_validation->set_rules('nego_value', 'Perkiraan Harga', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_prospek->ApprovalProspek($model, "Nego");
            echo json_encode($data);
        }
    }

    public function nego_prospek() {
        $input = $this->input->post();
        $this->form_validation->set_rules('id_prospek', 'Prospek', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        $this->form_validation->set_rules('estimasi_harga', 'Estimasi Harga', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data = $this->m_prospek->NegoProspek($input);
            echo json_encode($data);
        }
    }

    public function show_nego_form($id_prospek) {
        $row = ['button' => 'Add'];
        $javascript = array();
        $row['id_prospek'] = $id_prospek;
        $this->load->view('prospek/v_prospek_nego', $row);
    }

}

/* End of file Prospek.php */ 
