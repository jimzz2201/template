<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_cabang extends CI_Model {

    public $table = '#_cabang';
    public $id = 'id_cabang';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    // datatables
    function GetDatacabang() {
        $this->load->library('datatables');
        $this->datatables->select('id_cabang,kode_cabang,nama_cabang,alamat_cabang,contact_person,phone_number,kota,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_cabang.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction .= anchor("/cabang/edit_cabang/$1", 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction .= anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deletecabang($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_cabang');
        return $this->datatables->generate();
    }

    // get all
    function GetOneCabang($keyword, $type = 'id_cabang') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table . '.deleted_date', null);
        $cabang = $this->db->get($this->table)->row();
        return $cabang;
    }

    function CabangManipulate($model) {
        try {
            $cabang['status'] = DefaultCurrencyDatabase($model['status']);
            $cabang['nama_cabang'] = $model['nama_cabang'];
            $cabang['contact_person'] = $model['contact_person'];
            $cabang['phone_number'] = $model['phone_number'];
            $cabang['kota'] = $model['kota'];
            $cabang['alamat_cabang'] = $model['alamat_cabang'];
            $st=false;
            $msg="";
            $id_cabang=0;
            if (CheckEmpty($model['id_cabang'])) {
                $cabang['created_date'] = GetDateNow();
                $cabang['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $cabang);
                $st=true;
                $msg="Cabang successfull added into database";
                $id_cabang=$this->db->insert_id();
            } else {
                $cabang['updated_date'] = GetDateNow();
                $cabang['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $cabang, array("id_cabang" => $model['id_cabang']));
                $st=true;
                $msg="Cabang has been updated";
                $id_cabang=$model['id_cabang'];
            }
            $description="Setting dari Cabang ".$cabang['nama_cabang'];
            $url = "index.php/cabang/edit_cabang/" . $id_cabang;
            $arrchildid = [];
            foreach ($model['listconfig'] as $child) {
                $childwhere = array("id_gl_module" => $child['id_gl_module'], "id_cabang" => $id_cabang, "type" => "config");
                $this->db->from("#_gl_config");
                $this->db->where($childwhere);
                $rowchild = $this->db->get()->row();
                if ($rowchild) {
                    $arrchildid[] = $rowchild->id_gl_config;
                    $this->db->update("#_gl_config", MergeUpdate(array("id_gl_account" => $child['id_gl_account'], "description" => $description, "url" => $url,"id_subject"=>null)), array('id_gl_config' => $rowchild->id_gl_config));
                } else {
                    $childinsert = $childwhere;
                    $childinsert['id_subject']=null;
                    $childinsert['id_gl_account'] = $child['id_gl_account'];
                    $childinsert['description'] = $description;
                    $childinsert['url'] = $url;
                    $this->db->insert("#_gl_config", MergeCreated($childinsert));
                    $insert_id = $this->db->insert_id();
                    $arrchildid[] = $insert_id;
                }
            }
            $this->db->where(array("type" => "config", "id_cabang" => $id_cabang));
            if (count($arrchildid) > 0) {
                $this->db->where_not_in("id_gl_config", $arrchildid);
            }
            $this->db->delete("#_gl_config");



            return array("st" => $st, "msg" => $msg);
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    function GetDropDownCabang($type = "json",$userid="",$listidcabang=[]) {
        if(!CheckEmpty(@$userid))
        {
            $this->load->model("user/m_user");
            $user=$this->m_user->GetOneUser($userid,"id_user");
            if($user)
            {
                if($user->arr_cabang!="all"&&$user->arr_cabang!="")
                {
                    $listidcabang= explode(",", $user->arr_cabang);
                }
            }
            
        }
        if(count($listidcabang)>0)
        {
            $this->db->where_in("id_cabang",$listidcabang);
        }
        
        $listcabang = GetTableData($this->table, 'id_cabang', 'nama_cabang', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null), $type);
        return $listcabang;
    }

    function CabangDelete($id_cabang) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_cabang' => $id_cabang));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Cabang has been deleted from database");
    }

}
