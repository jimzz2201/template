<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cabang extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_cabang');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K049";
        $header = "K001";
        $title = "Cabang";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	CekModule($module);
	LoadTemplate($model, "cabang/v_cabang_index", $javascript);
        
    } 
    public function get_one_cabang() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_cabang->GetOneCabang($model["id_cabang"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Cabang tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdatacabang() {
        header('Content-Type: application/json');
        echo $this->m_cabang->GetDatacabang();
    }
    
    public function create_cabang() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	$module = "K049";
        $header = "K001";
        $row['title'] = "Add Cabang";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;    
        $this->load->model("gl_config/m_gl_config");
        $this->load->model("gl/m_gl");
        $this->load->model("cabang/m_cabang");
        $row['list_gl_account'] = $this->m_gl->GetDropDownGl(0);
        $row['list_gl_module'] = $this->m_gl_config->GetDropDownGlModule();
        $row['list_gl_config'] = [];
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $javascript = [];
        LoadTemplate($row, "cabang/v_cabang_manipulate", $javascript);    
    }
    
    public function cabang_manipulate() 
    {
        $message='';
        
	$this->form_validation->set_rules('nama_cabang', 'Nama Cabang', 'trim|required');
	$this->form_validation->set_rules('alamat_cabang', 'Alamat Cabang', 'trim|required');
	$this->form_validation->set_rules('contact_person', 'Contact Person', 'trim|required');
	$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required');
	$this->form_validation->set_rules('kota', 'Kota', 'trim|required');
        $model=$this->input->post();
        
        
        if(CheckEmpty(@$model['id_cabang']))
        {
            $this->form_validation->set_rules('kode_cabang', 'Kode Cabang', 'trim|required');
        }
        
        $listmodule= CheckArray($model, 'id_gl_module');
        $listid_gl_account= CheckArray($model, 'id_gl_account');
        $arrconfig=[];
        foreach($listmodule as $key=>$mod)
        {
            
            if(CheckEmpty($listid_gl_account[$key])&& !CheckEmpty($listmodule[$key]))
            {
                $message .= "Akun GL Tidak boleh kosong<br/>";
            }
            else if(!CheckEmpty($listid_gl_account[$key])&& CheckEmpty($listmodule[$key]))
            {
                $message .= "Module Tidak boleh kosong<br/>";
            } 
            else if (!CheckEmpty($listid_gl_account[$key]) && !CheckEmpty($listmodule[$key])) {
                $temparray = Multi_array_search($arrconfig, "id_gl_module", ForeignKeyFromDb($listmodule[$key]));
               
                if ($temparray) {
                    $message .= "GL  sudah memiliki config yang serupa sebelumnya<br/>";
                }

                if ($message == "" ) {
                    $configdetail = [];
                    $configdetail['id_gl_module'] = ForeignKeyFromDb($listmodule[$key]);
                    $configdetail['id_gl_account'] = ForeignKeyFromDb($listid_gl_account[$key]);
                    $arrconfig[] = $configdetail;
                }
            }
        }
        $model['listconfig']=$arrconfig;
     
        
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_cabang->CabangManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_cabang($id=0) 
    {
        $row = $this->m_cabang->GetOneCabang($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	    $module = "K049";
            $header = "K001";
            $row['title'] = "Edit Cabang";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;      
            $this->load->model("gl_config/m_gl_config");
            $this->load->model("gl/m_gl");
            $this->load->model("cabang/m_cabang");
            $row['list_gl_account']=$this->m_gl->GetDropDownGl(0);
            $row['list_gl_module']=$this->m_gl_config->GetDropDownGlModule();
            $row['list_gl_config']=$this->m_gl_config->GetGlConfig(null,"config",$id);
            LoadTemplate($row,'cabang/v_cabang_manipulate');
        } else {
            SetMessageSession(0, "Cabang cannot be found in database");
            redirect(site_url('cabang'));
        }
    }
    
    public function cabang_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_cabang->CabangDelete($model['id_cabang']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Cabang.php */