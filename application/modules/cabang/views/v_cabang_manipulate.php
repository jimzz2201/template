<section class="content-header">
    <h1>
        Cabang <?= @$button ?>
        <small>Customer</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Cabang</li>
        <li class="active">Cabang <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">
                            <form id="frmupload" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_cabang" value="<?php echo @$id_cabang; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Kode', "txt_kode_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $mergearray = array();
                                        if (!CheckEmpty(@$id_cabang)) {
                                            $mergearray['disabled'] = "disabled";
                                        } else {
                                            $mergearray['name'] = "kode_cabang";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_cabang, 'class' => 'form-control', 'id' => 'txt_kode_cabang', 'placeholder' => 'Kode Cabang'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama', "txt_nama_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_cabang', 'value' => @$nama_cabang, 'class' => 'form-control', 'id' => 'txt_nama_cabang', 'placeholder' => 'Nama Cabang')); ?>
                                    </div>
                                </div>
                                <div class="form-group">

                                    <?= form_label('Contact', "txt_contact_person", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'contact_person', 'value' => @$contact_person, 'class' => 'form-control', 'id' => 'txt_contact_person', 'placeholder' => 'Contact Person')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Phone&nbsp;Number', "txt_phone_number", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'phone_number', 'value' => @$phone_number, 'class' => 'form-control', 'id' => 'txt_phone_number', 'placeholder' => 'Phone Number')); ?>
                                    </div>
                                    <?= form_label('Kota', "txt_kota", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'kota', 'value' => @$kota, 'class' => 'form-control', 'id' => 'txt_kota', 'placeholder' => 'Kota')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Alamat', "txt_alamat_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat_cabang', 'value' => @$alamat_cabang, 'class' => 'form-control', 'id' => 'txt_alamat_cabang', 'placeholder' => 'Alamat Cabang')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <table class="table table-striped table-bordered table-hover" id="mytable">
                                        <thead>
                                        <th>Module</th>
                                        <th>GL Account</th>
                                        <th style="width:150px;">Action <a style="float:right" class='btn-xs  btn-success' href='javascript:;' onclick="AddGlModule()">Add Child</a></th>
                                        </thead>
                                        <tbody>
                                            <?php if (count(@$list_gl_config) > 0) {
                                                foreach (@$list_gl_config as $config) {
                                                    ?>
                                                <tr class="tr<?php echo $config->id_gl_config ?>">
                                                <input type="hidden" name="id_gl_config[]" value="<?php echo $config->id_gl_config; ?>" /> 
                                                <td><?= form_dropdown(array("selected" => @$config->id_gl_module, "id" => "dd_id_gl_module" . $config->id_gl_module, "name" => "id_gl_module[]"), DefaultEmptyDropdown(@$list_gl_module, "json", "Module"), @$config->id_gl_module, array('class' => 'form-control')); ?></td>

                                                <td><?= form_dropdown(array("selected" => @$config->id_gl_account, "id" => "dd_id_gl_account" . $config->id_gl_module, "name" => "id_gl_account[]"), DefaultEmptyDropdown(@$list_gl_account, "json", "Akun"), @$config->id_gl_account, array('class' => 'form-control')); ?></td>
                                                <td style="width:200px;">
                                                    <a class="btn  btn-danger" href="javascript:;" onclick="DeleteGlModule(this)">Delete</a>
                                                </td>
                                                </tr>

                                            <?php
                                            }
                                        } else {
                                            echo "<tr class='trkosong'><td colspan='4' style='padding:10px;text-align:center'>Tidak ada setting akun</td></tr>";
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/cabang' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        $("select").select2();
    });
    var datagl =<?php echo json_encode(DefaultEmptyDropdown(@$list_gl_account, "json", "Akun")) ?>;
    var datacabang =<?php echo json_encode(DefaultEmptyDropdown(@$list_cabang, "json", "Cabang")) ?>;
    var datamodule =<?php echo json_encode(DefaultEmptyDropdown(@$list_gl_module, "json", "Module")) ?>;

    function AddGlModule()
    {
        $(".trkosong").remove();
        var newRow = $("<tr class='trchild'>");
        var cols = "";
        cols += '<td><select name="id_gl_module[]" class="form-control modulechild" ></select></td>';
        cols += '<td><select name="id_gl_account[]" class="form-control child" ></select></td>';
        cols += '<td><a class="btn  btn-danger" href="javascript:;" onclick="DeleteGlModule(this)">Delete</a></td>';
        newRow.append(cols);
        $("tbody").append(newRow);
        $(".child:last").select2({data: datagl});
        $(".modulechild:last").select2({data: datamodule});
        $(".cabangchild:last").select2({data: datacabang});
        $(".child:last").val(0);
        $(".child:last").trigger("change");
    }
    function DeleteGlModule(module)
    {
        $(module).parent().parent().remove();
        if ($(".child").length == 0)
        {
            $("tbody").append("<tr class='trkosong'><td colspan='4' style='padding:10px;text-align:center'>Tidak ada setting akun</td></tr>");
        }
    }

    $("#frmupload").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/cabang/cabang_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/cabang';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>

<style>
    .control-label {
        text-align: left !important;
    }
</style>