<section class="content-header">
    <h1>
        Ekspedisi <?= @$button ?>
        <small>Ekspedisi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Ekspedisi</li>
        <li class="active">Ekspedisi <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_ekspedisi" class="form-horizontal form-groups-bordered validate" method="post">
	<input type="hidden" name="id_ekspedisi" value="<?php echo @$id_ekspedisi; ?>" /> 
	<div class="form-group">
		<?= form_label('Kode Ekspedisi', "txt_kode_ekspedisi", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
            <?php 
                $mergearray=array();
                $mergearray['readonly'] = "readonly";
                $mergearray['name'] = "kode_ekspedisi";
                if(!CheckEmpty(@$id_ekspedisi)){
                    $mergearray['readonly'] = "readonly";
                } else {
                    $mergearray['name'] = "kode_ekspedisi";
                }
            ?>
            <?= form_input(array_merge($mergearray,array('type' => 'text', 'value' => @$kode_ekspedisi, 'class' => 'form-control', 'id' => 'txt_kode_ekspedisi', 'placeholder' => 'Kode Ekspedisi'))); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Nama Ekspedisi', "txt_nama_ekspedisi", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-10">
			<?= form_input(array('type' => 'text', 'name' => 'nama_ekspedisi', 'value' => @$nama_ekspedisi, 'class' => 'form-control', 'id' => 'txt_nama_ekspedisi', 'placeholder' => 'Nama Ekspedisi')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Alamat Ekspedisi', "txt_alamat_ekspedisi", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-10">
            <?= form_textarea(array('type' => 'text', 'name' => 'alamat_ekspedisi', 'rows' => '3', 'cols' => '10', 'value' => @$alamat_ekspedisi, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Tlp Ekspedisi', "txt_tlp_ekspedisi", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?= form_input(array('type' => 'text', 'name' => 'tlp_ekspedisi', 'value' => @$tlp_ekspedisi, 'class' => 'form-control', 'id' => 'txt_tlp_ekspedisi', 'placeholder' => 'Tlp Ekspedisi')); ?>
		</div>
		<?= form_label('Contact Person Ekspedisi', "txt_cp_ekspedisi", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-4">
			<?= form_input(array('type' => 'text', 'name' => 'cp_ekspedisi', 'value' => @$cp_ekspedisi, 'class' => 'form-control', 'id' => 'txt_cp_ekspedisi', 'placeholder' => 'Contact Person')); ?>
		</div>
	</div>
	<div class="form-group">
		<?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
		<div class="col-sm-8">
			<?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
		</div>
	</div>
	<div class="form-group">
        <a href="<?php echo base_url().'index.php/ekspedisi' ?>" class="btn btn-default"  >Cancel</a>
        <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
	</div>
</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
$("#frm_ekspedisi").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/ekspedisi/ekspedisi_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href=baseurl + 'index.php/ekspedisi';
                }
                else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>