<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ekspedisi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_ekspedisi');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K105";
        $header = "K001";
        $title = "Ekspedisi";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	CekModule($module);
	LoadTemplate($model, "ekspedisi/v_ekspedisi_index", $javascript);
        
    } 
    public function get_one_ekspedisi() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_ekspedisi', 'Ekspedisi', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_ekspedisi->GetOneEkspedisi($model["id_ekspedisi"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Ekspedisi tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdataekspedisi() {
        header('Content-Type: application/json');
        echo $this->m_ekspedisi->GetDataekspedisi();
    }
    
    public function create_ekspedisi() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	    $module = "K105";
        $header = "K001";
        $row['title'] = "Add Ekspedisi";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
	    LoadTemplate($row,'ekspedisi/v_ekspedisi_manipulate', $javascript);       
    }
    
    public function ekspedisi_manipulate() 
    {
        $message='';

        // $this->form_validation->set_rules('kode_ekspedisi', 'Kode Ekspedisi', 'trim|required');
        $this->form_validation->set_rules('nama_ekspedisi', 'Nama Ekspedisi', 'trim|required');
        $this->form_validation->set_rules('alamat_ekspedisi', 'Alamat Ekspedisi', 'trim|required');
        $this->form_validation->set_rules('tlp_ekspedisi', 'Tlp Ekspedisi', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_ekspedisi->EkspedisiManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_ekspedisi($id=0) 
    {
        $row = $this->m_ekspedisi->GetOneEkspedisi($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	    $module = "K105";
            $header = "K001";
            $row['title'] = "Edit Ekspedisi";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
	    LoadTemplate($row,'ekspedisi/v_ekspedisi_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Ekspedisi cannot be found in database");
            redirect(site_url('ekspedisi'));
        }
    }
    
    public function ekspedisi_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_ekspedisi', 'Ekspedisi', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_ekspedisi->EkspedisiDelete($model['id_ekspedisi']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Ekspedisi.php */