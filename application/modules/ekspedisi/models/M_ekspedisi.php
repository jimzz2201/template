<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_ekspedisi extends CI_Model
{

    public $table = '#_ekspedisi';
    public $id = 'id_ekspedisi';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDataekspedisi() {
        $this->load->library('datatables');
        $this->datatables->select('id_ekspedisi,kode_ekspedisi,nama_ekspedisi,alamat_ekspedisi,tlp_ekspedisi,cp_ekspedisi,status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        //$this->datatables->join('table2', 'dgmi_ekspedisi.field = table2.field');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('ekspedisi/edit_ekspedisi/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteekspedisi($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_ekspedisi');
        return $this->datatables->generate();
    }

    // get all
    function GetOneEkspedisi($keyword, $type = 'id_ekspedisi') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $ekspedisi = $this->db->get($this->table)->row();
        return $ekspedisi;
    }

    function EkspedisiManipulate($model) {
        try {

            if (CheckEmpty($model['id_ekspedisi'])) {
                $model['kode_ekspedisi'] = AutoIncrement($this->table, 'EKP', 'kode_ekspedisi',3);
                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
                SetMessageSession(1, 'Ekspedisi successfull added into database');
                return array("st" => true, "msg" => "Ekspedisi successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_ekspedisi" => $model['id_ekspedisi']));
                SetMessageSession(1, 'Ekspedisi has been updated');
                return array("st" => true, "msg" => "Ekspedisi has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownEkspedisi() {
        $listekspedisi = GetTableData($this->table, 'id_ekspedisi', 'nama_ekspedisi', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listekspedisi;
    }

    function EkspedisiDelete($id_ekspedisi) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_ekspedisi' => $id_ekspedisi));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Ekspedisi has been deleted from database");
    }


}