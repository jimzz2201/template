<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Apk extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $version['version'] = '1.0.0';
        $version['link'] = 'https://play.google.com/store/apps/details?id=com.dgti.dgmi';
        $version['tracking_interval'] = 10;

        echo json_encode($version);
    }
}

/* End of file Customer.php */