<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Kpu extends CI_Controller {

    private $_sufix = "_kpu";

    function __construct() {
        parent::__construct();
        $this->load->model('m_kpu');
        $this->load->model("warna/m_warna");
        $this->load->model("vrf/m_vrf");
        $this->load->model("model/m_model");
    }
    
    public function GenerateJurnal()
    {
        $this->db->from("#_kpu");
        $this->db->where(array("#_kpu.status !="=>2,"#_kpu.deleted_date"=>null));
        $listkpu=$this->db->get()->result();
        //$this->m_kpu->GenerateJurnalKPU(1274 ,"#_kpu.id_kpu",'SO/VJDMI1/10/2020/0084');
        
        foreach($listkpu as $kpu)
        {
           $this->m_kpu->GenerateJurnalKPU($kpu->id_kpu ,"#_kpu.id_kpu",$kpu->nomor_master);
        }
        
        
    }
    
    public function GenerateUnitKPU()
    {
        $this->db->from("#_kpu_detail");
        $this->db->join("#_kpu","#_kpu.id_kpu=#_kpu_detail.id_kpu");
        $this->db->where(array("#_kpu.type"=>"KPU","#_kpu.status !="=>2));
        $this->db->select("#_kpu_detail.qty,#_kpu_detail.id_kpu_detail");
        $listdetail=$this->db->get()->result_array();
        foreach($listdetail as $kpudetail)
        {
            $id_kpu_detail=$kpudetail['id_kpu_detail'];
            $this->db->from("#_kpu_detail_unit");
            $this->db->where("id_kpu_detail", $kpudetail['id_kpu_detail']);
            $jumlahdbunit = $this->db->get()->num_rows();

            $jumlahcreate = $kpudetail['qty'];
            if ($jumlahdbunit > $jumlahcreate) {
                $this->db->delete("#_kpu_detail_unit", array("id_kpu_detail" => $id_kpu_detail, "id_unit_serial" => null));
            }
            $this->db->from("#_kpu_detail_unit");
            $this->db->where("id_kpu_detail", $id_kpu_detail);
            $jumlahdbunit = $this->db->get()->num_rows();
            $jumlahcreate = $jumlahcreate - $jumlahdbunit;


            for ($i = 0; $i < $jumlahcreate; $i++) {
                $this->db->from("#_kpu_detail_unit");
                $this->db->where(array("id_kpu_detail"=>$id_kpu_detail,"id_unit_serial !="=>null));
                $listid=$this->db->get()->result_array();
                $listid= array_column($listid, "id_unit_serial");
                
                $this->db->from("#_unit_serial");
                $this->db->where(array("id_kpu_detail"=>$id_kpu_detail));
                if(count($listid)>0)
                {
                      $this->db->where_not_in("id_unit_serial",$listid);
                }
              
                $unitserialblmterdaftar=$this->db->get()->row();
                $unitserial = [];
                $unitserial['id_kpu_detail'] = $id_kpu_detail;
                $unitserial['is_batal'] = 0;
                if($unitserialblmterdaftar!=null)
                {
                    $unitserial['id_unit_serial'] = $unitserialblmterdaftar->id_unit_serial;
                }
                $unitserial['created_date'] = GetDateNow();
                $unitserial['created_by'] = GetUserId();
                $this->db->insert("#_kpu_detail_unit", $unitserial);
            }
            
             $this->db->from("#_kpu_detail_unit");
             $this->db->where(array("id_kpu_detail"=>$id_kpu_detail,"id_unit_serial"=>null));
             $list_kpu_detail_unit=$this->db->get()->result();
             foreach($list_kpu_detail_unit as $det_non)
             {
                $this->db->from("#_kpu_detail_unit");
                $this->db->where(array("id_kpu_detail" => $id_kpu_detail, "id_unit_serial !=" => null));
                $listid = $this->db->get()->result_array();
                $listid = array_column($listid, "id_unit_serial");

                $this->db->from("#_unit_serial");
                $this->db->where(array("id_kpu_detail" => $id_kpu_detail));
                if (count($listid) > 0) {
                    $this->db->where_not_in("id_unit_serial", $listid);
                }
                $unitserialblmterdaftar=$this->db->get()->row();
                
                if($unitserialblmterdaftar!=null)
                {
                    $unitserialupdate=[];
                    $unitserialupdate['id_unit_serial'] = $unitserialblmterdaftar->id_unit_serial;
                    $this->db->update("#_kpu_detail_unit",$unitserialupdate,array("id_kpu_detail_unit"=>$det_non->id_kpu_detail_unit));
                }
                
            }
             
            
        }
        
        
        
    }

    public function get_kpu_list() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_kpu = $this->m_kpu->GetDropDownKPUFromSupplier($model);
        echo json_encode(array("list_kpu" => DefaultEmptyDropdown($list_kpu, "json", "KPU"), "st" => true));
    }

    public function get_kpu_list_from_customer() {
        $model = $this->input->post();
        header('Content-Type: application/json');

        $list_kpu = $this->m_kpu->GetDropDownKPUFromCustomer($model);
        echo json_encode(array("list_kpu" => DefaultEmptyDropdown($list_kpu, "json", "KPU"), "st" => true));
    }

    public function index() {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K060";
        $header = "K059";
        $title = "KPU";
        $this->load->model("supplier/m_supplier");
        $this->load->model("kategori/m_kategori");
        $model = array("title" => $title, "form" => $header, "formsubmenu" => $module);
        $model['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $model['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $status[] = array();
        $status[] = array("id" => "1", "text" => "Active");
        $status[] = array("id" => "4", "text" => "Finished");
        $status[] = array("id" => "5", "text" => "Batal");
        $jenis_pencarian = [];
        $jenis_keyword[] = array("id" => "0", "text" => "Pilih Pencarian Keyword");
        $jenis_keyword[] = array("id" => "no_vrf", "text" => "NO VRF");
        $jenis_keyword[] = array("id" => "nomor_master", "text" => "NO KPU");
        $jenis_keyword[] = array("id" => "no_prospek", "text" => "NO PPROSPEK");
        $jenis_keyword[] = array("id" => "nomor_do_kpu", "text" => "NO DO KPU");
        $jenis_pencarian[] = array("id" => "created_date", "text" => "Tanggal Buat");
        $jenis_pencarian[] = array("id" => "updated_date", "text" => "Tanggal Update");
        $jenis_pencarian[] = array("id" => "tanggal", "text" => "Tanggal KPU");
        $model['start_date'] = GetCookieSetting("start_date" . $this->_sufix, AddDays(GetDateNow(), "-1 month"));
        $model['end_date'] = GetCookieSetting("end_date" . $this->_sufix, GetDateNow());
        $javascript[] = "assets/plugins/select2/select2.js";
        $model['list_status'] = $status;
        $model['status'] = GetCookieSetting("status" . $this->_sufix, 0);
        $model['jenis_keyword'] = GetCookieSetting("jenis_keyword" . $this->_sufix, "nomor_master");
        $model['jenis_pencarian'] = GetCookieSetting("jenis_pencarian" . $this->_sufix, "created_date");
        $model['list_pencarian'] = $jenis_pencarian;
        $model['list_keyword'] = $jenis_keyword;
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($model, "kpu/v_kpu_index", $javascript, $css);
    }

    public function get_one_kpu() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_kpu', 'Kpu', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_kpu->GetOneKpu($model["id_kpu"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Kpu tidak ditemukan dalam database" : "";
            if (CheckEmpty(@$model['typehtml'])) {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_content", $data['obj'], true);
            } else {
                $data['html'] = $this->load->view("pembayaran/v_pembayaran_hutang_content", array("detail"=>[$data['obj']]), true);
            }
            echo json_encode($data);
        }
    }

    public function getdatakpu() {
        header('Content-Type: application/json');
        $str = $this->input->post("extra_search");

        parse_str($str, $params);
        foreach ($params as $key => $value) {
            if ($key == "status" && $value == 0) {
                SetCookieSetting("search" . $this->_sufix, 1);
            }
            SetCookieSetting($key . $this->_sufix, $value);
        }
        echo $this->m_kpu->GetDatakpu($params);
    }

    public function getDataVrf() {
        $id_Vrf = $this->input->get('id_vrf');
        // header('Content-Type: application/json');
        $res = $this->m_vrf->GetOneVrf($id_Vrf);
        echo $json = json_encode($res);
    }

    public function create_kpu() {
        $this->load->model("customer/m_customer");
        $row = ['button' => 'Add'];
        $jenis_pembayaran = array();
        $jenis_pembayaran[2] = "Kredit";
        $row['list_type_pembayaran'] = $jenis_pembayaran;
        $list_type_ppn = array();
        $list_type_ppn[1] = "Include";
        $list_type_ppn[2] = "Exclude";
        $row['list_type_ppn'] = $list_type_ppn;
        $list_type_pembulatan = array();
        $list_type_pembulatan[1] = "Sebelum PPN";
        $list_type_pembulatan[2] = "Sesudah PPN";
        $row['list_type_pembulatan'] = $list_type_pembulatan;
        $javascript = array();
        $module = "K060";
        $header = "K059";
        $this->load->model("supplier/m_supplier");
        $this->load->model("kategori/m_kategori");
        $this->load->model("unit/m_unit");
        $row['free_top_proposed'] = 0;
        $row['title'] = "Add KPU";
        $row['tanggal'] = GetDateNow();
        $row['type_pembayaran'] = 2;
        $row['top'] = 30;
        $row['list_vrf'] = array(array("id" => 0, "text" => "KPU Manual"));
        $row['list_top'] = array(array("id" => 0, "text" => "0"), array("id" => 30, "text" => "30"), array("id" => 45, "text" => "45"), array("id" => 60, "text" => "60"), array("id" => 75, "text" => "75"), array("id" => 90, "text" => "90"));
        $row['jth_tempo'] = AddDays(GetDateNow(), "30 days");
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;
        $this->load->model("cabang/m_cabang");
        $row['id_cabang'] = GetIdCabang();
        $row['list_colour'] = $this->m_warna->GetDropDownWarna();
        $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
        $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();
        $row['list_kategori'] = $this->m_kategori->GetDropDownKategori();
        $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
        $row['list_unit'] = $this->m_unit->GetDropDownUnit();
        $javascript[] = "assets/plugins/select2/select2.js";
        $css[] = "assets/plugins/select2/select2.css";
        LoadTemplate($row, 'kpu/v_kpu_manipulate', $javascript, $css);
    }

    public function get_dropdown_kpu_for_unit() {
        header('Content-Type: application/json');
        $model = $this->input->post();

        $list_kpu = $this->m_kpu->GetDropDownKPUUnitSerial($model);
        echo json_encode(array("list_kpu" => DefaultEmptyDropdown($list_kpu, "json", "KPU"), "st" => true));
    }
    public function kpu_manipulate() {
        $message = '';
        $model = $this->input->post();
        $this->form_validation->set_rules('id_cabang', 'Cabang', 'trim|required');
        $this->form_validation->set_rules('qty', 'Qty', 'trim|required');
        $this->form_validation->set_rules('id_supplier', 'Supplier', 'trim|required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('price', 'Price', 'trim|required|numeric');
        $this->form_validation->set_rules('nomor_master', 'No KPU', 'trim|required');
        
         if (CheckEmpty(@$model['id_kpu'])) {
            $this->form_validation->set_rules('nomor_master', 'No KPU', 'trim|required|is_unique[#_kpu.nomor_master]');
        }
        $this->load->model("vrf/m_vrf");
        if (@$model['id_vrf'] > 0) {
            $this->form_validation->set_rules('id_vrf', 'Id Vrf', 'trim|required');
            $vrf = $this->m_vrf->GetOneVrf(@$model['id_vrf']);
            $sisa = $vrf->qty - $vrf->qty_total;
            if (!CheckEmpty(@$model['id_kpu'])) {
                $kpu = $this->m_kpu->GetOneKpu(@$model['id_kpu']);
                if ($kpu) {
                    $sisa += $kpu->qty;
                }
            }
            if ($sisa < DefaultCurrencyDatabase(@$model['qty'])) {
                $message .= "QTY VRF tidak cukup untuk kpu ini<br/>";
            }
        }
        $no_before="";
        $no_after="";
        $no_after=@$model['nomor_master'];
        $this->load->model("neraca/m_neraca");
        if(!CheckEmpty(@$model['id_kpu']))
        {
            $listunit=$this->m_kpu->GetUnitSerialFromKPU($model['id_kpu']);
            if(count($listunit)>$model['qty'])
            {
                $message .= "Unit yang sudah dialokasikan lebih besar daripada qty di KPU ini<br/>";
            }
            $kpu=$this->m_kpu->GetOneKpu(@$model['id_kpu'],"#_kpu.id_kpu",false);
            if($kpu!=null){
                $no_before=$kpu->nomor_master;
            }
        }
        if($this->m_neraca->IsExistDokumen($no_after,$no_before))
        {
            $message .= "Nomor Sudah dipakai pada transaksi yang lain<br/>";
        }
        
        
        

        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/> ' . validation_errors() . $message));
        } else {
            $status = $this->m_kpu->KpuManipulate($model);
            echo json_encode($status);
        }
    }

    public function kpu_save_do() {
        $message = '';

        $tgl_do = $this->input->post('tgl_do');
        foreach ($tgl_do as $key => $value) {
            $data[] = array(
                'id_kpu' => $_POST['idkpu'],
                'tgl_do' => $_POST['tgl_do'][$key],
                'no_do' => $_POST['no_do'][$key],
                'manufacture_no' => $_POST['manufacture_no'][$key],
                'engine_no' => $_POST['engine_no'][$key],
                'created_by' => GetUserId(),
                'created_date' => GetDateNow(),
            );
        }
        $this->db->insert_batch('dgmi_do', $data);
        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function kpu_edit_do() {
        $message = '';
        $this->db->where('id_kpu', $_POST['idkpu']);
        $this->db->delete('dgmi_do');

        if ($this->input->post('tgl_do')) {
            $tgl_do = $this->input->post('tgl_do');
            foreach ($tgl_do as $key => $value) {
                $data[] = array(
                    'id_kpu' => $_POST['idkpu'],
                    'tgl_do' => $_POST['tgl_do'][$key],
                    'no_do' => $_POST['no_do'][$key],
                    'manufacture_no' => $_POST['manufacture_no'][$key],
                    'engine_no' => $_POST['engine_no'][$key],
                    'created_by' => GetUserId(),
                    'created_date' => GetDateNow(),
                );
            }
            $this->db->insert_batch('dgmi_do', $data);
        }

        $status = array("st" => true, "msg" => "KPU and DO successfull added into database");
        echo json_encode($status);
    }

    public function edit_kpu($id = 0) {
        $row = $this->m_kpu->GetOneKpu($id);
        $this->load->model("do_kpu/m_do_kpu");
        $listdokpu = $this->m_do_kpu->GetDoKPUByKey($id);
        if (count($listdokpu) > 0&&!CekModule("K114", false)) {
            redirect(base_url() . 'index.php/kpu/view_kpu/' . $id);
            die();
        }
        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K060";
            $header = "K059";
            $row['title'] = "Edit Kpu";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $jenis_pembayaran = array();
            $jenis_pembayaran[1] = "Tunai";
            $jenis_pembayaran[2] = "Kredit";
            $row['list_type_pembayaran'] = $jenis_pembayaran;
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $this->load->model("supplier/m_supplier");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $this->load->model("customer/m_customer");
            $this->load->model("cabang/m_cabang");
            $row['list_top'] = array(array("id" => 0, "text" => "0"), array("id" => 30, "text" => "30"), array("id" => 45, "text" => "45"), array("id" => 60, "text" => "60"), array("id" => 75, "text" => "75"), array("id" => 90, "text" => "90"));
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_type_ppn'] = $list_type_ppn;
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();

            $this->load->model("vrf/m_vrf");
            $vrf = $this->m_vrf->GetOneVrf($row['id_vrf']);
            if ($vrf)
                $row['qty_sisa'] = $vrf->qty - $vrf->qty_total + $row['qty'];
            $list_type_pembulatan = array();
            $list_type_pembulatan[1] = "Sebelum PPN";
            $list_type_pembulatan[2] = "Sesudah PPN";
            $row['list_type_pembulatan'] = $list_type_pembulatan;
            $row['list_vrf'] = $this->m_vrf->GetDropDownVrf();
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_model'] = $this->m_model->GetDropDownModel();

            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'kpu/v_kpu_manipulate', $javascript, $css);
        } else {
            SetMessageSession(0, "Kpu cannot be found in database");
            redirect(site_url('kpu'));
        }
    }

    public function view_kpu($id = 0) {
        $row = $this->m_kpu->GetOneKpu($id);
        if ($row) {
            $row->button = 'Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
            $module = "K060";
            $header = "K059";
            $row['title'] = "Edit Kpu";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;
            $jenis_pembayaran = array();
            $jenis_pembayaran[1] = "Tunai";
            $jenis_pembayaran[2] = "Kredit";
            $row['list_type_pembayaran'] = $jenis_pembayaran;
            $list_type_ppn = array();
            $list_type_ppn[1] = "Include";
            $list_type_ppn[2] = "Exclude";
            $this->load->model("supplier/m_supplier");
            $this->load->model("type_unit/m_type_unit");
            $this->load->model("unit/m_unit");
            $this->load->model("customer/m_customer");
            $this->load->model("cabang/m_cabang");
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_supplier'] = $this->m_supplier->GetDropDownSupplier();
            $row['list_type_unit'] = $this->m_type_unit->GetDropDownType_unit();
            $row['list_unit'] = $this->m_unit->GetDropDownUnit();
            $row['list_customer'] = $this->m_customer->GetDropDownCustomer();
            $row['list_type_ppn'] = $list_type_ppn;
            $row['list_cabang'] = $this->m_cabang->GetDropDownCabang();

            $this->load->model("vrf/m_vrf");
            $vrf = $this->m_vrf->GetOneVrf($row['id_vrf']);
            if ($vrf)
                $row['qty_sisa'] = $vrf->qty - $vrf->qty_total + $row['qty'];
            $list_type_pembulatan = array();
            $list_type_pembulatan[1] = "Sebelum PPN";
            $list_type_pembulatan[2] = "Sesudah PPN";
            $row['list_type_pembulatan'] = $list_type_pembulatan;
            $row['list_vrf'] = $this->m_vrf->GetDropDownVrf();
            $row['list_colour'] = $this->m_warna->GetDropDownWarna();
            $row['list_model'] = $this->m_model->GetDropDownModel();

            $javascript[] = "assets/plugins/select2/select2.js";
            $css[] = "assets/plugins/select2/select2.css";
            LoadTemplate($row, 'kpu/v_kpu_view', $javascript, $css);
        } else {
            SetMessageSession(0, "Kpu cannot be found in database");
            redirect(site_url('kpu'));
        }
    }

    public function kpu_batal() {
        $message = '';
        $this->form_validation->set_rules('id_kpu', 'Kpu', 'required');
        if ($this->form_validation->run() == FALSE || $message != '') {
            $result = array();
            $result['st'] = false;
            $result['msg'] = 'Error :<br/>' . validation_errors() . $message;
        } else {
            $model = $this->input->post();
            $result = $this->m_kpu->KPUBatal($model['id_kpu']);
        }

        echo json_encode($result);
    }

}

/* End of file Kpu.php */