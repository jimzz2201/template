<section class="content-header">
    <h1>
        KPU  <?=   @$button .' '.(!CheckEmpty(@$nomor_master)?'<span class="headerred">'.@$nomor_master.'</span>':'') ?>
        <small>KPU</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>KPU</li>
        <li class="active">KPU <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_kpu" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_kpu" value="<?php echo @$id_kpu; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        if (!CheckEmpty(@$id_supplier)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_supplier, 'name' => 'id_supplier'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_supplier, 'class' => 'form-control', 'id' => 'txt_nama_supplier', 'placeholder' => 'Supplier'));
                                        } else {
                                            echo form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier, array('class' => 'form-control select2', 'id' => 'dd_id_supplier'));
                                        }
                                        ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "top"), @$list_top, @$top, array('class' => 'form-control', 'id' => 'txt_top')); ?>
                                    </div>  
                                  
                                </div>

                                <div class="form-group">
                                    <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                     <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "", "PPN"), @$type_ppn, array('class' => 'form-control', 'id' => 'dd_type_ppn')); ?>
                                    </div>
                                    <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text', 'value' => DefaultCurrency(@$ppn), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <?= form_label('Nomor KPU', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'nomor_master',  'value' => @$nomor_master, 'class' => 'form-control', 'id' => 'txt_nomor_master', 'placeholder' => 'No KPU')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>
                                </div>  
                                <hr/>
                                <?php if (CheckEmpty(@$id_kpu)) { ?>
                                    <div class="form-group">
                                        <?= form_label('Kategori', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-2">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_kategori_search', 'class' => 'form-control select2', 'id' => 'dd_id_kategori_search', 'placeholder' => 'Type Unit'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori"), @$id_kategori); ?>
                                        </div>
                                        <?= form_label('Customer', "dd_id_customer_search", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-3">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_customer_search', 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-group">

                                    <?= form_label('VRF', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        if (!CheckEmpty(@$id_kpu)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_vrf, 'name' => 'id_vrf'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$vrf_code, 'class' => 'form-control', 'id' => 'txt_vrf_code', 'placeholder' => 'VRF'));
                                        } else {
                                            echo form_dropdown(array('type' => 'text', 'name' => 'id_vrf', 'class' => 'form-control select2', 'id' => 'dd_id_vrf', 'placeholder' => 'VRF'), @$list_vrf, @$id_vrf);
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    if (!CheckEmpty(@$id_kpu)) {
                                        echo form_label('Customer', "txt_id_model", array("class" => 'col-sm-1 control-label'));
                                        echo "<div class='col-sm-3'>";
                                        echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_customer, 'class' => 'form-control', 'id' => 'txt_nama_customer', 'placeholder' => 'Customer'));
                                        echo "</div>";
                                    }
                                    ?>

                                </div>

                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <div id="unit-input"><?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?></div>
                                        <div id="unit-select" style="display:none"><?= form_dropdown(array('type' => 'text', 'name' => 'id_unit', 'class' => 'form-control', 'id' => 'dd_id_unit', 'placeholder' => 'Unit'), DefaultEmptyDropdown($list_unit, "json", "Unit"), @$id_unit); ?></div>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                        <?= form_input(array('type' => 'hidden', 'class' => 'form-control', 'id' => 'id_type_unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>

                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_warna', 'class' => 'form-control', 'id' => 'dd_id_colour', 'placeholder' => 'Colour'), DefaultEmptyDropdown($list_colour, "json", "Warna"), @$id_warna);
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group frmqty">
                                    <?= form_label('Qty Total', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => DefaultCurrency(@$qty_total), 'class' => 'form-control', 'id' => 'txt_qty_total', 'placeholder' => 'Qty Total', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Qty Sisa', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => DefaultCurrency(@$qty_sisa), 'class' => 'form-control', 'id' => 'txt_qty_sisa', 'placeholder' => 'Qty Sisa', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Qty', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'qty', 'value' => DefaultCurrency(@$qty), 'class' => 'form-control', 'id' => 'txt_qty', 'placeholder' => 'Qty', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Dnp', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'price', 'value' => DefaultCurrency(@$price), 'class' => 'form-control', 'id' => 'txt_dnp', 'placeholder' => 'Dnp', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                    <?= form_label('Discount', "txt_discount", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'disc1', 'value' => DefaultCurrency(@$disc1), 'class' => 'form-control', 'id' => 'txt_discount', 'placeholder' => 'Discount', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>




                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total', "readonly" => "readonly")); ?>
                                    </div>
                                    <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'nominal_ppn', 'value' => DefaultCurrency(@$nominal_ppn), 'class' => 'form-control', 'id' => 'txt_nominal_ppn', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
                                    </div>


                                </div>
                                <div class="form-group" style="display:none">
                                    <?= form_label('Type Pembulatan', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "type_pembulatan"), @$list_type_pembulatan, @$type_pembulatan, array('class' => 'form-control', 'id' => 'dd_type_pembulatan')); ?>
                                    </div>
                                    <?= form_label('Pembulatan', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'disc_pembulatan', 'value' => DefaultCurrency(@$disc_pembulatan), 'class' => 'form-control', 'id' => 'txt_disc_pembulatan', 'placeholder' => 'Disc Pembulatan', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Grand Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$grandtotal), 'class' => 'form-control', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
                                    </div>
                                    <div style="display:none">
                                        <?= form_label('Jumlah&nbsp;Bayar', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-5">
                                            <?= form_input(array('type' => 'text', 'name' => 'jumlah_bayar', 'value' => DefaultCurrency(@$jumlah_bayar), 'class' => 'form-control', 'id' => 'txt_jumlah_bayar', 'placeholder' => 'Jumlah Bayar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-group" style="margin-top:50px">
                                    <button type="submit" class="btn btn-primary btn-block" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


    var table;
    var dataset = <?php echo json_encode(@$listdetail) ?>;
    var listheader = <?php echo json_encode(@$listheader) ?>;
    $("#dd_type_pembulatan").change(function () {
        HitungSemua();
    })

    function ShowArea()
    {
        $("#unit-select").hide();
        $("#unit-input").show();
        $(".frmqty").css("display", "none");
        if ($("#dd_id_vrf").val() == "0")
        {
            $("#txt_qty_sisa").val(0);
            $("#txt_top, #txt_vin").removeAttr("disabled");
            $("#txt_top, #txt_vin").removeAttr("readonly");
            $("#unit-select").show();
            $("#unit-input").hide();
            $("#dd_id_colour").select2({disabled: "false"});
            $("#dd_id_colour").removeAttr("disabled");

        } else
        {
            $(".frmqty").css("display", "block");
        }
    }
    function UpdateVRF()
    {

        $("#dd_id_vrf").change(function () {
            var id_vrf = $("#dd_id_vrf").val();
            // $("#txt_top, #txt_vin").attr("disabled", "disabled");
            $("#unit-select").hide();
            $("#unit-input").show();

            if (id_vrf != 0)
            {
                 $(".frmqty").css("display","none");
                LoadBar.show();
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/vrf/get_one_vrf',
                    dataType: 'json',
                    data: {
                        id_vrf: id_vrf
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            $("#txt_nama_unit").val(data.obj.nama_unit);
                            $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                            $("#txt_nama_kategori").val(data.obj.nama_kategori);
                            $("#txt_top").val(data.obj.top);
                            $(".frmqty").css("display","block");
                            if (Number.isInteger(data.obj.top))
                            {
                                if (data.obj.top % 2 == 0)
                                {
                                    // $("#txt_jth_tempo").val(DefaultDateFormat(e.date.addMonths(1))); //Where e contains date, dates and format
                                } else
                                {

                                }
                            }
                            $("#dd_id_unit").val(data.obj.id_unit);
                            $("#txt_vin").val(data.obj.vin);
                            $("#txt_dnp").val(Comma(data.obj.dnp));
                            $("#txt_discount").val(Comma(data.obj.discount));
                            $("#txt_qty_total").val(Comma(data.obj.qty));
                            $("#txt_qty_sisa").val(Comma(data.obj.qty - data.obj.qty_total));
                            if (data.obj.id_colour != null)
                                $("#dd_id_colour").val(data.obj.id_colour).trigger("change");


                        } else
                        {
                            $("#txt_nama_unit").val("");
                            $("#txt_nama_type_unit").val("");
                            $("#txt_nama_kategori").val("");
                            $("#txt_top").val("");
                            $("#txt_vin").val("");
                            $("#dd_id_colour").select2();
                        }
                        LoadBar.hide();
                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                        $("#txt_nama_unit").val("");
                        $("#txt_nama_type_unit").val("");
                        $("#txt_nama_kategori").val("");
                        LoadBar.hide();
                    }
                });
            } else
            {

                $("#txt_qty_sisa").val(0);
                $("#txt_top, #txt_vin, #txt_qty_total").removeAttr("disabled");
                $("#txt_top, #txt_vin, #txt_qty_total").removeAttr("readonly");
                $("#unit-select").show();
                $("#unit-input").hide();
                $("#dd_id_colour").select2({disabled: "false"});
                $("#dd_id_colour").removeAttr("disabled");
            }
        })
    }

    $("#dd_id_unit").change(function () {
        // console.log('idunit');
        var id_unit = $(this).val();
        detailUnit(id_unit);
    })

    function detailUnit(id_unit) {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_one_unit',
            dataType: 'json',
            data: {
                id_unit: id_unit
            },
            success: function (data) {
                if (data.st)
                {
                    $("#txt_nama_type_unit").val(data.obj.nama_type_unit);
                    $("#txt_nama_kategori").val(data.obj.nama_kategori);
                    $("#id_type_unit").val(data.obj.id_type_unit);
                    selectTop();
                } else
                {
                    $("#txt_nama_type_unit").val("");
                    $("#txt_nama_kategori").val("");
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
    }

    $("#txt_top").change(function () {
        if ($(this).val() == '') {
            $('#txt_dnp').val(0);
            return false;
        }
        $("#txt_jth_tempo").val(AddDays($("#txt_tanggal").val(),$("#txt_top").val()));
        selectTop();
    });

    function selectTop() {
        var top = $("#txt_top").val();
        var id_unit = $("#dd_id_unit").val();
        if (isNaN(top) || id_unit == '') {
            return false;
        }

        $.ajax({
            url: baseurl + 'index.php/price_list/getTopPrice',
            dataType: 'json',
            method: 'post',
            data: {
                id_unit: id_unit,
                top: top
            },
            success: function (data) {
                $('#txt_discount').val(CommaWithoutHitungan(data.vrf));
                $('#txt_dnp').val(CommaWithoutHitungan(data.price));
                HitungSemua();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responeText);
            }
        });
    }

    function RefreshPPN()
    {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").removeAttr("readonly");
        } else
        {
            $("#txt_ppn").attr("readonly", "readonly");
        }
    }
    $("#dd_type_ppn").change(function () {
        if ($("#dd_type_ppn").val() != 0)
        {
            $("#txt_ppn").val(11);
        } else
        {
            $("#txt_ppn").val(0);
        }
        RefreshPPN();
        HitungSemua();
    })
    function HitungSemua() {
        var total = 0;
        var nominalppn = 0;
        var ppn = Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""));
        var qty = Number($("#txt_qty").val().replace(/[^0-9\.]+/g, ""));
        var dnp = Number($("#txt_dnp").val().replace(/[^0-9\.]+/g, ""));
        var discount = Number($("#txt_discount").val().replace(/[^0-9\.]+/g, ""));
        var grandtotal = qty * (dnp - discount);
        var total = grandtotal;
        var totalsemua = total;

        if ($("#dd_type_pembulatan").val() == "1")
        {
            total = total - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
            grandtotal = grandtotal - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
        }


        if ($("#dd_type_ppn").val() == "2")
        {
            nominalppn = ppn / 100 * grandtotal;
            grandtotal = grandtotal + nominalppn;
        } else if ($("#dd_type_ppn").val() == "1")
        {
            nominalppn = ppn / (100 + Number($("#txt_ppn").val().replace(/[^0-9\.]+/g, ""))) * grandtotal;
            total = total - nominalppn;
        }
        if ($("#dd_type_pembulatan").val() == "2")
        {
            grandtotal = grandtotal - Number($("#txt_disc_pembulatan").val().replace(/[^0-9\.]+/g, ""));
        }
        $("#txt_grandtotal").val(number_format(grandtotal.toFixed(4)));
        $("#txt_nominal_ppn").val(number_format(nominalppn.toFixed(4)));
        $("#txt_total").val(number_format(totalsemua.toFixed(4)));
    }
    function deleterow(indrow) {
        RefreshGrid();
    }
    function RefreshUnit()
    {
        var id_kategori = $("#dd_id_kategori_search").val();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/unit/get_dropdown_unit',
            dataType: 'json',
            data: {
                id_kategori: id_kategori
            },
            success: function (data) {
                $("#dd_id_unit").empty();
                if (data.st)
                {
                    $("#dd_id_unit").select2({data: data.list_unit})
                }
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });

    }
    function GetVRfRemaining(id)
    {

        var id_unit_search = $("#dd_id_unit_search").val();
        if (id == "dd_id_kategori_search")
        {
            id_unit_search = 0;
            RefreshUnit();

        }

        LoadBar.show();
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/vrf/get_remaining_vrf',
            dataType: 'json',
            data: {
                id_supplier: $("#dd_id_supplier").val(),
                id_kategori: $("#dd_id_kategori_search").val(),
                id_unit: id_unit_search,
                id_customer: $("#dd_id_customer_search").val()
            },
            success: function (data) {
                $("#dd_id_vrf").empty();
                if (data.st)
                {
                    $("#dd_id_vrf").select2({data: data.list_vrf});
                    UpdateVRF();
                }
                ShowArea();
                LoadBar.hide();
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
                LoadBar.hide();
            }
        });
    }
   
    function RefreshDetail() {
        var pricelist = Number($("#txt_price_list").val().replace(/[^0-9\.]+/g, ""));
        var disc1 = Number($("#txt_disc_1").val().replace(/[^0-9\.]+/g, ""));
        var disc2 = Number($("#txt_disc_2").val().replace(/[^0-9\.]+/g, ""));
        var qty = Number($("#txt_quantity").val().replace(/[^0-9\.]+/g, ""));
        var subtotal = pricelist * ((100 - disc1) / 100) * ((100 - disc2) / 100) * qty;
        $("#txt_subtotal").val(Comma(subtotal));
    }



    $(document).ready(function () {
        $("#txt_tanggal").datepicker();
        ShowArea();
        $("#txt_tanggal").on('change', function (e) {
              $("#txt_jth_tempo").val(AddDays($("#txt_tanggal").val(),$("#txt_top").val()));
        });
        RefreshPPN();
        $("select").select2();
        $('#dd_id_customer_search').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        $("#dd_id_supplier ,#dd_id_kategori_search , #dd_id_customer_search").change(function () {
            var id = $(this).attr("id");
            GetVRfRemaining(id);
        })

    })

    $("#btt_search_po").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/pengajuan/searchpengajuan',
            data: $("#frm_kpu").serialize() + '&' + $.param({"dataset": dataset}),
            success: function (data) {
                modalbootstrap(data, "Search PO", "80%");

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });

    })
    $("#frm_kpu").submit(function () {

        swal({
            title: "Apakah kamu yakin ingin menginput data kpu berikut?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/kpu/kpu_manipulate',
                    dataType: 'json',
                    data: $("#frm_kpu").serialize(),
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            window.location.href = "<?php echo base_url() ?>index.php/kpu";
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });
        return false;


    })

</script>