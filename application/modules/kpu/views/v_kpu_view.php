<section class="content-header">
    <h1>
        KPU View
        <small>KPU</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>KPU</li>
        <li class="active">KPU View</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default form-element-list">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body">

                            <form id="frm_kpu" class="form-horizontal form-groups-bordered validate" method="post">
                                <div id="notification" ></div>
                                <input type="hidden" name="id_kpu" value="<?php echo @$id_kpu; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Supplier', "txt_supplier", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        if (!CheckEmpty(@$id_supplier)) {
                                            echo form_input(array('type' => 'hidden','readonly' => 'readonly', 'value' => @$id_supplier, 'name' => 'id_supplier'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$nama_supplier, 'class' => 'form-control', 'id' => 'txt_nama_supplier', 'placeholder' => 'Supplier'));
                                        } else {
                                            echo form_dropdown(array("name" => "id_supplier"), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier"), @$id_supplier, array('class' => 'form-control select2', 'id' => 'dd_id_supplier'));
                                        }
                                        ?>
                                    </div>    

                                    <?= form_label('Tanggal', "txt_tgl_po", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text' ,'readonly' => 'readonly', 'autocomplete' => 'off', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control', 'id' => 'txt_tanggal', 'placeholder' => 'Tanggal')); ?>
                                    </div>
                                    <?= form_label('Pembayaran', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_pembayaran"), DefaultEmptyDropdown(@$list_type_pembayaran, "", "Pembayaran"), @$type_pembayaran, array('class' => 'form-control', 'id' => 'dd_type_pembayaran')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Type PPN', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_dropdown(array("name" => "type_ppn"), DefaultEmptyDropdown(@$list_type_ppn, "", "PPN"), @$type_ppn, array('class' => 'form-control', 'id' => 'dd_type_ppn')); ?>
                                    </div>
                                    <?= form_label('PPN', "dd_id_gudang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-1">
                                        <?= form_input(array('type' => 'text','readonly' => 'readonly',  'value' => DefaultCurrency(@$ppn), 'class' => 'form-control', 'id' => 'txt_ppn', 'placeholder' => 'PPN', 'name' => 'ppn', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Top', "txt_top", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'name' => 'top','readonly' => 'readonly',  'value' => @$top, 'class' => 'form-control top', 'id' => 'txt_top', 'placeholder' => 'Top')); ?>
                                    </div>  
                                    <?= form_label('Jth Tempo', "dd_id_cabang", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'name' => 'jth_tempo', 'value' => DefaultDatePicker(@$jth_tempo), 'class' => 'form-control', 'id' => 'txt_jth_tempo', 'placeholder' => 'Jatuh Tempo')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Cabang', "dd_id_cabang", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "id_cabang"), DefaultEmptyDropdown(@$list_cabang, "json", "Cabang"), @$id_cabang, array('class' => 'form-control', 'id' => 'dd_id_cabang')); ?>
                                    </div>
                                   
                                </div>
                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_keterangan", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', "rows" => 4, 'readonly' => 'readonly', 'value' => @$keterangan, 'name' => 'keterangan', 'class' => 'form-control', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>

                                </div>  
                                <hr/>
                                <?php if (CheckEmpty(@$id_kpu)) { ?>
                                    <div class="form-group">
                                        <?= form_label('Type Unit', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-2">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_type_unit_search', 'class' => 'form-control select2', 'id' => 'dd_id_type_unit_search', 'placeholder' => 'Type Unit'), DefaultEmptyDropdown(@$list_type_unit, "json", "Type Unit"), @$id_type_unit); ?>
                                        </div>
                                        <?= form_label('Unit', "dd_id_unit_search", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-3">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_unit_search', 'class' => 'form-control select2', 'id' => 'dd_id_unit_search', 'placeholder' => 'Unit'), DefaultEmptyDropdown(@$list_unit, "json", "Unit"), @$id_unit); ?>
                                        </div>
                                        <?= form_label('Customer', "dd_id_customer_search", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-3">
                                            <?= form_dropdown(array('type' => 'text', 'name' => 'id_customer_search', 'class' => 'form-control select2', 'id' => 'dd_id_customer_search', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer"), @$id_customer); ?>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="form-group">
                                        <?= form_label('Type Unit', "dd_id_type_unit_search", array("class" => 'col-sm-2 control-label')); ?>
                                        <div class="col-sm-2">
                                            <?php  
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_vrf, 'name' => 'id_vrf'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$vrf_code, 'class' => 'form-control', 'id' => 'txt_vrf_code', 'placeholder' => 'VRF'));
                                            ?>
                                        </div>
                                        <?= form_label('Unit', "dd_id_unit_search", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-3">
                                            <?php  
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_vrf, 'name' => 'id_vrf'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$vrf_code, 'class' => 'form-control', 'id' => 'txt_vrf_code', 'placeholder' => 'VRF'));
                                            ?>
                                        </div>
                                        <?= form_label('Customer', "dd_id_customer_search", array("class" => 'col-sm-1 control-label')); ?>
                                        <div class="col-sm-3">
                                            <?php  
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_vrf, 'name' => 'id_vrf'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$vrf_code, 'class' => 'form-control', 'id' => 'txt_vrf_code', 'placeholder' => 'VRF'));
                                            ?>
                                        </div>
                                    </div>
                                    
                                <?php } ?>
                                <div class="form-group">

                                    <?= form_label('VRF', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?php
                                        if (!CheckEmpty(@$id_kpu)) {
                                            echo form_input(array('type' => 'hidden', 'value' => @$id_vrf, 'name' => 'id_vrf'));
                                            echo form_input(array('type' => 'text', 'readonly' => 'readonly', 'autocomplete' => 'off', 'value' => @$vrf_code, 'class' => 'form-control', 'id' => 'txt_vrf_code', 'placeholder' => 'VRF'));
                                        } else {
                                            echo form_dropdown(array('type' => 'text', 'name' => 'id_vrf', 'class' => 'form-control select2', 'id' => 'dd_id_vrf', 'placeholder' => 'VRF'), DefaultEmptyDropdown(@$list_vrf, "json", "VRF"), @$id_vrf);
                                        }
                                        ?>
                                    </div>


                                </div>

                                <div class="form-group">
                                    <?= form_label('Unit', "txt_id_model", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <div id="unit-input"><?= form_input(array('type' => 'text', 'name' => 'unit', 'disabled' => 'disabled', 'value' => @$nama_unit, 'class' => 'form-control', 'id' => 'txt_nama_unit', 'placeholder' => 'Unit')); ?></div>
                                        <div id="unit-select" style="display:none"><?= form_dropdown(array('type' => 'text', 'name' => 'id_unit', 'class' => 'form-control', 'id' => 'dd_id_unit', 'placeholder' => 'Unit'), DefaultEmptyDropdown($list_unit, "json", "Unit"), @$id_unit); ?></div>
                                    </div>
                                    <?= form_label('Tipe Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tipe_unit', 'disabled' => 'disabled', 'value' => @$nama_type_unit, 'class' => 'form-control', 'id' => 'txt_nama_type_unit', 'placeholder' => 'Tipe Unit')); ?>
                                        <?= form_input(array('type' => 'hidden', 'class' => 'form-control', 'id' => 'id_type_unit')); ?>
                                    </div>
                                    <?= form_label('Kategori Unit', "txt_id_model", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'kategori_unit', 'disabled' => 'disabled', 'value' => @$nama_kategori, 'class' => 'form-control', 'id' => 'txt_nama_kategori', 'placeholder' => 'Kategori Unit')); ?>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <?= form_label('Vin', "txt_vin", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text','readonly' => 'readonly',  'name' => 'vin', 'onkeypress' => 'return isNumberKey(event);', 'value' => @$vin, 'class' => 'form-control', 'id' => 'txt_vin', 'placeholder' => 'Vin')); ?>
                                    </div>

                                    <?= form_label('Warna', "txt_id_colour", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?php
                                        echo form_dropdown(array('type' => 'text', 'name' => 'id_warna', 'class' => 'form-control', 'id' => 'dd_id_colour', 'placeholder' => 'Colour'), DefaultEmptyDropdown($list_colour, "json", "Warna"), @$id_warna);
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Qty Total', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_total', 'value' => DefaultCurrency(@$qty_total), 'class' => 'form-control', 'id' => 'txt_qty_total', 'placeholder' => 'Qty Total', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Qty Sisa', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'qty_sisa', 'value' => DefaultCurrency(@$qty_sisa), 'class' => 'form-control', 'id' => 'txt_qty_sisa', 'placeholder' => 'Qty Sisa', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <?= form_label('Qty', "txt_qty", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-2">
                                        <?= form_input(array('type' => 'text','readonly' => 'readonly',  'name' => 'qty', 'value' => DefaultCurrency(@$qty), 'class' => 'form-control', 'id' => 'txt_qty', 'placeholder' => 'Qty', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                    <?= form_label('Dnp', "txt_dnp", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text','readonly' => 'readonly',  'name' => 'price', 'value' => DefaultCurrency(@$price), 'class' => 'form-control', 'id' => 'txt_dnp', 'placeholder' => 'Dnp', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>

                                    <?= form_label('Discount', "txt_discount", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text','readonly' => 'readonly',  'name' => 'disc1', 'value' => DefaultCurrency(@$disc1), 'class' => 'form-control', 'id' => 'txt_discount', 'placeholder' => 'Discount', 'onkeyup' => 'javascript:this.value=CommaWithoutHitungan(this.value);HitungSemua();', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>
                                </div>




                                <hr/>
                                <div class="form-group">
                                    <?= form_label('Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'total', 'value' => DefaultCurrency(@$total), 'class' => 'form-control', 'id' => 'txt_total', 'placeholder' => 'Total', "readonly" => "readonly")); ?>
                                    </div>
                                    <?= form_label('PPN', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'nominal_ppn', 'value' => DefaultCurrency(@$nominal_ppn), 'class' => 'form-control', 'id' => 'txt_nominal_ppn', 'placeholder' => 'PPN', "readonly" => "readonly")); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Type Pembulatan', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("name" => "type_pembulatan"), @$list_type_pembulatan, @$type_pembulatan, array('class' => 'form-control', 'id' => 'dd_type_pembulatan')); ?>
                                    </div>
                                    <?= form_label('Pembulatan', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text','readonly' => 'readonly',  'name' => 'disc_pembulatan', 'value' => DefaultCurrency(@$disc_pembulatan), 'class' => 'form-control', 'id' => 'txt_disc_pembulatan', 'placeholder' => 'Disc Pembulatan', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>


                                </div>
                                <div class="form-group">
                                    <?= form_label('Grand Total', "txt_kepada", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'readonly' => 'readonly', 'name' => 'grandtotal', 'value' => DefaultCurrency(@$grandtotal), 'class' => 'form-control', 'id' => 'txt_grandtotal', 'placeholder' => 'Grand Total')); ?>
                                    </div>
                                    <?= form_label('Jumlah&nbsp;Bayar', "txt_kepada", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text','readonly' => 'readonly',  'name' => 'jumlah_bayar', 'value' => DefaultCurrency(@$jumlah_bayar), 'class' => 'form-control', 'id' => 'txt_jumlah_bayar', 'placeholder' => 'Jumlah Bayar', 'onkeyup' => 'javascript:this.value=Comma(this.value);', 'onkeypress' => 'return isNumberKey(event);')); ?>
                                    </div>


                                </div>
                               
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script>


   
    $(document).ready(function () {
        $("select").select2({ disabled : true });
    })


</script>