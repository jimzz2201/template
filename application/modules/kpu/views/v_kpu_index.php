
<section class="content-header">
    <h1>
        Kpu
        <small>Data Transaksi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Transaksi</li>
        <li class="active">KPU</li>
    </ol>


</section>

<section class="content">
    <div class="box box-default">

        <div class="box-body">
            <div id="notification" ></div>
            <form id="frm_search"  class="form-horizontal">
                <div class="row">
                    <div class="form-group">
                        <?= form_label('Pencarian', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_pencarian', 'selected' => @$jenis_pencarian, 'class' => 'form-control select2', 'id' => 'dd_jenis_pencarian', 'placeholder' => 'jenis_pencarian'), @$list_pencarian); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'start_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_awal'), DefaultDatePicker($start_date), array('required' => 'required')); ?>
                        </div>


                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'end_date', 'class' => 'form-control datepicker', 'id' => 'txt_tanggal_akhir'), DefaultDatePicker($end_date)); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_supplier', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_supplier', 'placeholder' => 'Supplier'), DefaultEmptyDropdown(@$list_supplier, "json", "Supplier")); ?>
                        </div>
                       <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'status', 'selected' => @$status, 'class' => 'form-control select2', 'id' => 'status', 'placeholder' => 'status'), DefaultEmptyDropdown(@$list_status, "json", "Status")); ?>
                        </div>
                        
                        
                        
                    </div>
                    <div class="form-group">
                        <?= form_label('&nbsp;', "txt_tanggal_awal", array("class" => 'col-sm-1 control-label')); ?>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_customer', 'value' => "", 'class' => 'form-control select2', 'id' => 'dd_id_customer', 'placeholder' => 'Customer'), DefaultEmptyDropdown(@$list_customer, "json", "Customer")); ?>
                        </div>
                       <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'id_kategori', 'selected' => @$id_kategori, 'class' => 'form-control select2', 'id' => 'dd_kategori', 'placeholder' => 'Kategori'), DefaultEmptyDropdown(@$list_kategori, "json", "Kategori")); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_dropdown(array('name' => 'jenis_keyword', 'selected' => @$jenis_keyword, 'class' => 'form-control select2', 'id' => 'dd_jenis_keyword', 'placeholder' => 'Jenis Keyword'), @$list_keyword); ?>
                        </div>
                        <div class="col-sm-2">
                            <?= form_input(array("selected" => "", "autocomplete" => "off", 'name' => 'keyword', 'class' => 'form-control', 'id' => 'txt_keyword'), ""); ?>
                        </div>
                         <div class="col-sm-1">
                        <button id="btt_Search" type="submit" class="btn btn-block btn-success pull-right">Search</button>
                    </div>
                    </div>


                </div>



            </form>
            <hr/>
            <div class=" headerbutton">

                <?php echo anchor(site_url('kpu/create_kpu'), 'Create', 'class="btn btn-success"'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet-body form">
                    <table class="table table-striped table-bordered table-hover" id="mytable">

                    </table>
                </div>
            </div>

        </div>
    </div>

</section>
<script type="text/javascript">
    var table;
       $("#dd_jenis_keyword").change(function(){
        
        LoadAreaKeyword();
    })
    function LoadAreaKeyword()
    {
       
        if(CheckEmpty($("#dd_jenis_keyword").val()))
        {
            $("#txt_keyword").val("");
            $("#txt_keyword").attr("readonly","readonly");
        }
        else
        {
            $("#txt_keyword").removeAttr("readonly");
        }
    }
    function batalkpu(id_kpu) {


        swal({
            title: "Apakah kamu ingin membatalkan kpu berikut?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, batalkan!",
            closeOnConfirm: true
        }).then((result) => {
            if (result.value)
            {
                $.ajax({
                    type: 'POST',
                    url: baseurl + 'index.php/kpu/kpu_batal',
                    dataType: 'json',
                    data: {
                        id_kpu: id_kpu
                    },
                    success: function (data) {
                        if (data.st)
                        {
                            messagesuccess(data.msg);
                            table.fnDraw(false);
                        } else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error) {
                        messageerror(xhr.responseText);
                    }
                });
            }
        });


    }
    function RefreshGrid()
    {
        table.fnDraw(false);
    }
    $("form#frm_search").submit(function () {
        RefreshGrid();
        return false;
    })
    $(document).ready(function () {
        $(".datepicker").datepicker();
        $(".select2").select2();
        $('#dd_id_customer').select2({
            placeholder: "Pilih Customer",
            allowClear: true,
            ajax: {
                url: baseurl + 'index.php/customer/search_customer',
                dataType: 'json',
                method: 'POST',
                minimumInputLength: 3,
                processResult: function (data) {
                    return {
                        results: data.results
                    }
                }
            }
        });
        var isbatal=<?php echo CekModule("K114",false)?"true":"false";?>;
        $.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        table = $("#mytable").dataTable({
            initComplete: function () {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function (e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                            }
                        });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            ordering: false,
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: {"url": "kpu/getdatakpu", "type": "POST", "data": function (d) {
                    return $.extend({}, d, {
                        "extra_search": $("form#frm_search").serialize()
                    });
                }},
            columns: [
                {
                    data: "id_kpu",
                    title: "No",
                    width: "30px",
                    orderable: false
                },
                {data: "tanggal", orderable: false, title: "Tanggal", "className": "text-center", "width": "50px",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},
                {data: "nomor_master", orderable: false, title: "KPU"},
                {data: "vrf_code", orderable: false, title: "VRF"},
                {data: "nomor_do_kpu", orderable: false, title: "Nomor DO"},
                {data: "nama_supplier", orderable: false, title: "Supplier"},
                {data: "nama_customer", orderable: false, title: "Customer"},
                {data: "vin", orderable: false, title: "Vin"},
                {data: "qty", orderable: false, title: "Qty", "width": "30px", className: "text-center"},
                {data: "nama_warna", orderable: false, title: "Colour"},
                {data: "tanggal_pelunasan", orderable: false, title: "Pelunasan", "width": "50px", className: "text-center",
                    mRender: function (data, type, row) {
                        return data == null ? "" : DefaultDateFormat(data);
                    }},

                {data: "grandtotal", orderable: false, title: "GrandTotal", "className": "text-right",
                    mRender: function (data, type, row) {
                        return Comma(data == undefined ? 0 : data);
                    }},
                {data: "status", orderable: false, title: "Status",
                    mRender: function (data, type, row) {
                        var status="";
                        if(data!=1)
                        {
                            status= data==4?"Finished":"Batal";
                        }
                        return status;
                    }},
                {
                    "data": "view",
                    "orderable": false,
                    "width": "150px",
                    "className": "text-center",
                    mRender: function (data, type, row) {
                        var returnhtml = data;

                        if (CheckEmpty(row['id_do_kpu_detail'])&&row['created_by']=="<?php echo GetUserId(); ?>"||(isbatal))
                        {
                            returnhtml = returnhtml +row['edit'];
                            if(CheckEmpty(row['id_do_kpu_detail']))
                            {
                                 returnhtml = returnhtml +row['batal'];
                            }
                           
                        }
                        
                        if (row['status'] == '2')
                        {
                            returnhtml = '';
                        }
                        return returnhtml;
                    },
                }
            ],
            order: [[0, 'desc']],
            rowCallback: function (row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>
