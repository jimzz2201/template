<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kpu extends CI_Model {

    public $table = '#_kpu';
    public $id = 'id_kpu';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct() {
        parent::__construct();
    }

    function GetDropDownKPUFromSupplier($params) {
        $where = array($this->table . '.status' => 1, $this->table . '.deleted_date' => null);
        $where['#_kpu.id_supplier'] = @$params['id_supplier'];

        if (!CheckEmpty(@$params['id_kategori'])) {
            $where['#_unit.id_kategori'] = $params['id_kategori'];
        }
        if (!CheckEmpty(@$params['id_unit'])) {
            $where['#_kpu_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit'])) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer'])) {
            $where['#_kpu.id_customer'] = $params['id_customer'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_kpu_detail", "condition" => "#_kpu_detail.id_kpu=#_kpu.id_kpu");
        $arrayjoin[] = array("table" => "#_vrf", "condition" => "#_vrf.id_vrf=#_kpu_detail.id_vrf");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_kpu_detail.id_unit");
        $select = 'DATE_FORMAT(#_kpu.tanggal, "%d %M %Y") as kpu_date_convert,nomor_master,nama_customer,nama_unit,#_kpu.id_kpu';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_vrf.id_customer");
        $listkpu = GetTableData($this->table, 'id_kpu', array('nomor_master', "nama_customer", "nama_unit", "kpu_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listkpu;
    }
    

    function GetDropDownKPUFromCustomer($params) {
        $where = array($this->table . '.status' => 1, $this->table . '.deleted_date' => null);
        $where['#_kpu.id_supplier'] = @$params['id_supplier'];
        if (!CheckEmpty(@$params['id_unit'])) {
            $where['#_kpu_detail.id_unit'] = $params['id_unit'];
        }
        if (!CheckEmpty(@$params['id_type_unit'])) {
            $where['#_unit.id_type_unit'] = $params['id_type_unit'];
        }
        if (!CheckEmpty(@$params['id_customer'])) {
            $where['#_kpu.id_customer'] = $params['id_customer'];
        }
        $arrayjoin = [];
        $arrayjoin[] = array("table" => "#_kpu_detail", "condition" => "#_kpu_detail.id_kpu=#_kpu.id_kpu");
        $arrayjoin[] = array("table" => "#_vrf", "condition" => "#_vrf.id_vrf=#_kpu_detail.id_vrf");
        $arrayjoin[] = array("table" => "#_unit", "condition" => "#_unit.id_unit=#_kpu_detail.id_unit");
        $select = 'DATE_FORMAT(#_kpu.tanggal, "%d %M %Y") as kpu_date_convert,nomor_master,nama_customer,nama_unit,#_kpu.id_kpu';
        $arrayjoin[] = array("table" => "#_customer", "condition" => "#_customer.id_customer=#_vrf.id_customer");
        $listkpu = GetTableData($this->table, 'id_kpu', array('nomor_master', "nama_customer", "nama_unit", "kpu_date_convert"), $where, "json", array(), array(), $arrayjoin, $select);
        return $listkpu;
    }

    // datatables
    function GetDatakpu($params) {


        $isedit = CekModule("K114", false);
        $this->load->library('datatables');
        $this->datatables->select('#_kpu_detail.*,group_concat(distinct nomor_do_kpu) as nomor_do_kpu,#_kpu.*,#_kpu.id_kpu as id,nama_unit,nama_warna,vrf_code,nama_supplier,#_do_kpu_detail.id_do_kpu_detail,nama_customer,#_kpu.created_by');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table . '.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_kpu_detail', '#_kpu_detail.id_kpu = #_kpu.id_kpu', 'left');
        $this->datatables->join('#_do_kpu_detail', '#_do_kpu_detail.id_kpu_detail = #_kpu_detail.id_kpu_detail and #_do_kpu_detail.status !=2', 'left');
        $this->datatables->join('#_do_kpu', '#_do_kpu.id_do_kpu = #_do_kpu_detail.id_do_kpu and #_do_kpu.status !=2', 'left');

        $this->datatables->join('#_vrf', '#_vrf.id_vrf = #_kpu_detail.id_vrf', 'left');
        $this->datatables->join('#_warna', '#_warna.id_warna = #_kpu_detail.id_warna', 'left');
        $this->datatables->join('#_unit', '#_unit.id_unit = #_kpu_detail.id_unit', 'left');
        $this->datatables->join('#_supplier', '#_supplier.id_supplier = #_kpu.id_supplier', 'left');
        $this->datatables->join('#_customer', '#_customer.id_customer = #_kpu.id_customer', 'left');
        $this->db->order_by('#_kpu.id_kpu', "desc");
        $this->datatables->where("#_kpu.type", "KPU");
        $this->datatables->group_by('#_kpu.id_kpu');
        $extra = [];
        $where = [];
        if (!CheckEmpty(@$params['keyword'])) {
            $pencariankeyword = "nomor_master";

            if (@$params['jenis_keyword'] == "nomor_master") {
                $pencariankeyword = "#_kpu.nomor_master";
            } else if (@$params['jenis_keyword'] == "no_vrf") {
                $pencariankeyword = "#_vrf.vrf_code";
            } else if (@$params['jenis_keyword'] == "nomor_do_kpu") {
                $pencariankeyword = "#_do_kpu.nomor_do_kpu";
            } else if (@$params['jenis_keyword'] == "no_prospek") {
                $pencariankeyword = "#_prospek.no_prospek";
                $this->datatables->join("#_unit_serial", "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail");
                $this->datatables->join("#_prospek_detail", "#_unit_serial.id_prospek_detail=#_prospek_detail.id_prospek_detail");
                $this->datatables->join("#_prospek", "#_prospek.id_prospek=#_prospek_detail.id_prospek");
            }





            $where[$pencariankeyword] = trim($params['keyword']);
        } else {
            $tanggal = "date(#_prospek.tanggal_prospek)";
            if ($params['jenis_pencarian'] == "created_date") {
                $tanggal = "date(#_kpu.created_date)";
            } else if ($params['jenis_pencarian'] == "updated_date") {
                $tanggal = "date(#_kpu.updated_date)";
            } else if ($params['jenis_pencarian'] == "tanggal") {
                $tanggal = "#_kpu.tanggal";
            }
            $this->db->order_by($tanggal, "desc");
            if (isset($params['start_date'], $params['end_date']) && !empty($params['start_date']) && !empty($params['end_date'])) {
                array_push($extra, $tanggal . " BETWEEN '" . DefaultTanggalDatabase($params['start_date']) . "' AND '" . DefaultTanggalDatabase($params['end_date']) . "'");
                unset($params['start_date'], $params['end_date']);
            } else {

                if (isset($params['start_date']) && empty($params['end_date'])) {
                    $where[$tanggal] = DefaultTanggalDatabase($params['start_date']);
                    unset($params['start_date']);
                } else {
                    $where[$tanggal] = DefaultTanggalDatabase($params['end_date']);
                    unset($params['end_date']);
                }
            }

            if (!CheckEmpty($params['status'])) {
                $where['#_kpu.status'] = $params['status'] == "6" ? 0 : $params['status'];
            }
            if (!CheckEmpty($params['id_kategori'])) {
                $where['#_unit.id_kategori'] = $params['id_kategori'];
            }
            if (!CheckEmpty($params['id_supplier'])) {
                $where['#_kpu.id_supplier'] = $params['id_supplier'];
            }
            if (!CheckEmpty($params['id_customer'])) {
                $where['#_kpu.id_customer'] = $params['id_customer'];
            }
        }
        if (count($where)) {
            $this->datatables->where($where);
        }
        if (count($extra)) {
            $this->datatables->where(implode(" AND ", $extra));
        }

        $isdelete = true;
        $stredit = "";
        $strbatal = '';
        $strview = '';
        $strview .= anchor(site_url('kpu/view_kpu/$1'), 'View', array('class' => 'btn btn-default btn-xs'));
        if ($isedit) {
            $stredit .= anchor(site_url('kpu/edit_kpu/$1'), 'Revisi', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $strbatal .= anchor("", 'Batal', array('class' => 'btn btn-danger btn-xs', "onclick" => "batalkpu($1);return false;"));
        }
        $this->datatables->add_column('edit', $stredit, 'id');
        $this->datatables->add_column('view', $strview, 'id');
        $this->datatables->add_column('batal', $strbatal, 'id');
        return $this->datatables->generate();
    }

    // get all
    function GetOneKpu($keyword, $type = '#_kpu.id_kpu', $isfull = true) {
        $this->db->where($type, $keyword);
        $this->db->join('#_kpu_detail', '#_kpu_detail.id_kpu = #_kpu.id_kpu', 'left');
        $this->db->join('#_unit', '#_kpu_detail.id_unit = #_unit.id_unit', 'left');
        $this->db->where($this->table . '.deleted_date', null);
        if ($isfull) {
            $this->db->select('#_kpu_detail.*,#_kpu.*,#_unit.id_kategori,nama_supplier,vrf_code,nama_unit,nama_type_unit,nama_warna,nama_kategori,#_vrf.qty as qty_total,nama_customer');
            $this->db->join('#_vrf', '#_vrf.id_vrf = #_kpu_detail.id_vrf', 'left');
            $this->db->join('#_supplier', '#_kpu.id_supplier = #_supplier.id_supplier', 'left');
            $this->db->join('#_customer', '#_kpu.id_customer = #_customer.id_customer', 'left');

            $this->db->join("#_kategori", "#_kategori.id_kategori=#_unit.id_kategori", "left");
            $this->db->join("#_type_unit", "#_unit.id_type_unit=#_type_unit.id_type_unit", "left");
            $this->db->join("#_warna", "#_kpu_detail.id_warna=#_warna.id_warna", "left");
        } else {
            $this->db->select('#_kpu_detail.*,#_kpu.*,#_unit.id_kategori');
        }
        $kpu = $this->db->get($this->table)->row();
        if ($isfull && $kpu) {
            $this->db->from("#_pembayaran_utang_detail");
            $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id", "left");
            $this->db->where(array("id_kpu" => $kpu->id_kpu, "#_pembayaran_utang_master.status !=" => 2));
            $this->db->order_by("#_pembayaran_utang_master.tanggal_transaksi", "asc");
            $listpembayaran = $this->db->get()->result();
            $jumlah = 0;
            foreach ($listpembayaran as $pembayaran) {
                $jumlah += $pembayaran->jumlah_bayar;
            }
            $this->db->from("#_do_kpu");
            $this->db->join("#_do_kpu_detail", "#_do_kpu.id_do_kpu=#_do_kpu_detail.id_do_kpu");
            $this->db->where(array("#_do_kpu_detail.id_kpu_detail" => $kpu->id_kpu_detail, "#_do_kpu.status !=" => 2, "#_do_kpu_detail.status !=" => 2));
            $this->db->select("ifnull(sum(qty_do),0) as jumlah");
            $rowjumlah = $this->db->get()->row();
            if ($rowjumlah) {
                $kpu->qty_kirim = $rowjumlah->jumlah;
            } else {
                $kpu->qty_kirim = 0;
            }

            $kpu->sisa = $kpu->grandtotal - $jumlah;
            $kpu->terbayar = $jumlah;
            $kpu->listpembayaran = $listpembayaran;
        }
        return $kpu;
    }

    function UpdateStatusPembayaranKPU($id_kpu) {
        $kpu = $this->GetOneKpu($id_kpu, "#_kpu.id_kpu", true);
        if ($kpu) {
            if (CheckEmpty($kpu->tanggal_pelunasan)) {
                if ($kpu->sisa <= 0) {
                    if (count($kpu->listpembayaran) > 0) {
                        $this->db->update("#_kpu", array("tanggal_pelunasan" => $kpu->listpembayaran[count($kpu->listpembayaran) - 1]->tanggal_transaksi), array("id_kpu" => $id_kpu));
                    }
                }
            } else if (!CheckEmpty($kpu->tanggal_pelunasan)) {
                if ($kpu->sisa > 0) {
                    $this->db->update("#_kpu", array("tanggal_pelunasan" => null), array("id_kpu" => $id_kpu));
                }
            }
        }
    }

    function ChangeStatusKPU($id_kpu) {
        $kpu = $this->GetOneKpu($id_kpu, "#_kpu.id_kpu", true);
        if ($kpu->status != 2) {
            if ($kpu->qty_kirim >= $kpu->qty) {
                $this->db->update("#_kpu", array("status" => 4), array("id_kpu" => $id_kpu));
            } else {
                $this->db->update("#_kpu", array("status" => 1), array("id_kpu" => $id_kpu));
            }
        }
    }

    function GetUnitSerialFromKPU($id_kpu) {
        $this->db->from("#_unit_serial");
        $this->db->join("#_kpu_detail", "#_kpu_detail.id_kpu_detail=#_unit_serial.id_kpu_detail");
        $this->db->where(array("#_kpu_detail.id_kpu" => $id_kpu));
        $result = $this->db->get()->result();
        return $result;
    }

    function KpuManipulate($model) {
        try {
            $jenis_pencarian = "created_date";
            $message = "";
            $nomor_before = "";
            $this->db->trans_rollback();
            $this->db->trans_begin();
            $kpudetail['id_vrf'] = ForeignKeyFromDb($model['id_vrf']);
            $kpudetail['qty'] = DefaultCurrencyDatabase($model['qty']);
            $kpudetail['id_warna'] = ForeignKeyFromDb(@$model['id_warna']);
            $kpudetail['vin'] = $model['vin'];
            $kpudetail['status'] = 1;
            $kpudetail['id_unit'] = ForeignKeyFromDb(@$model['id_unit']);

            $kpudetail['price'] = DefaultCurrencyDatabase($model['price']);
            $kpudetail['disc1'] = DefaultCurrencyDatabase($model['disc1']);
            $kpudetail['disc2'] = 0;
            $kpudetail['is_persen_1'] = 0;
            $kpudetail['is_persen_2'] = 0;
            $kpudetail['subtotal'] = $kpudetail['qty'] * ($kpudetail['price'] - $kpudetail['disc1']);
            $kpu['nomor_master'] = $model['nomor_master'];
            $kpu['keterangan'] = $model['keterangan'];
            $kpu['id_supplier'] = ForeignKeyFromDb($model['id_supplier']);
            $kpu['id_customer'] = ForeignKeyFromDb(@$model['id_customer']);
            $kpu['tanggal'] = DefaultTanggalDatabase($model['tanggal']);
            $kpu['top'] = DefaultCurrencyDatabase($model['top']);
            $kpu['type_ppn'] = $model['type_ppn'];
            $kpu['id_cabang'] = $model['id_cabang'];
            $kpu['type_pembulatan'] = $model['type_pembulatan'];
            $kpu['disc_pembulatan'] = DefaultCurrencyDatabase($model['disc_pembulatan']);
            $kpu['nominal_ppn'] = DefaultCurrencyDatabase($model['nominal_ppn']);
            $kpu['grandtotal'] = DefaultCurrencyDatabase($model['grandtotal']);
            $kpu['jumlah_bayar'] = DefaultCurrencyDatabase($model['jumlah_bayar']) > $kpu['grandtotal'] ? $kpu['grandtotal'] : DefaultCurrencyDatabase($model['jumlah_bayar']);
            $kpu['total'] = DefaultCurrencyDatabase($model['total']);
            $kpu['ppn'] = DefaultCurrencyDatabase($model['ppn']);
            $kpu['type'] = 'KPU';
            $this->load->model("vrf/m_vrf");
            $vrf = $this->m_vrf->GetOneVrf($model['id_vrf']);
            if ($vrf) {
                $kpu['id_customer'] = @$vrf->id_customer;
            }
            $userid = GetUserId();
            $this->load->model("supplier/m_supplier");
            $supplier = $this->m_supplier->GetOneSupplier($kpu['id_supplier']);
            if (!$vrf && @$kpu['id_vrf'] > 0) {
                $message .= "VRF Tidak ditemukan dalam databases";
            }
            $id_kpu = 0;
            if (CheckEmpty(@$model['id_kpu'])) {
                $kpu['created_date'] = GetDateNow();
                $kpu['created_by'] = ForeignKeyFromDb(GetUserId());
                $kpu['status'] = 1;
                $res = $this->db->insert($this->table, $kpu);
                $id_kpu = $this->db->insert_id();
            } else {
                $kpu['updated_date'] = GetDateNow();
                $kpu['updated_by'] = ForeignKeyFromDb(GetUserId());
                $jenis_pencarian = "updated_date";
                $id_kpu = $model['id_kpu'];
                $kpudb = $this->GetOneKpu($id_kpu, "#_kpu.id_kpu", false);
                $nomor_before = $kpudb->nomor_master;

                $kpu['nomor_master'] = $kpudb->nomor_master;
            }
            $this->db->from("#_kpu_detail");
            $this->db->where(array("id_kpu" => $id_kpu, "status" => 1));
            $kpudetaildb = $this->db->get()->row();
            $kpudetail['id_kpu'] = $id_kpu;
            $id_kpu_detail = 0;
            if ($kpudetaildb) {
                $this->db->update("#_kpu_detail", $kpudetail, array("id_kpu_detail" => $kpudetaildb->id_kpu_detail));
                $id_kpu_detail = $kpudetaildb->id_kpu_detail;
            } else {
                $this->db->insert("#_kpu_detail", $kpudetail);
                $id_kpu_detail = $this->db->insert_id();
            }
            $this->db->from("#_kpu_detail_unit");
            $this->db->where("id_kpu_detail", $id_kpu_detail);
            $jumlahdbunit = $this->db->get()->num_rows();

            $jumlahcreate = $kpudetail['qty'];
            if ($jumlahdbunit > $jumlahcreate) {
                $this->db->delete("#_kpu_detail_unit", array("id_kpu_detail" => $id_kpu_detail, "id_unit_serial" => null));
            }
            $this->db->from("#_kpu_detail_unit");
            $this->db->where("id_kpu_detail", $id_kpu_detail);
            $jumlahdbunit = $this->db->get()->num_rows();
            $jumlahcreate = $jumlahcreate - $jumlahdbunit;


            for ($i = 0; $i < $jumlahcreate; $i++) {
                $unitserial = [];
                $unitserial['id_kpu_detail'] = $id_kpu_detail;
                $unitserial['is_batal'] = 0;
                $unitserial['created_date'] = GetDateNow();
                $unitserial['created_by'] = GetUserId();
                $this->db->insert("#_kpu_detail_unit", $unitserial);
            }

            $this->db->from("#_kpu_detail_unit");
            $this->db->where(array("id_kpu_detail" => $id_kpu_detail));
            $this->db->select("ifnull(sum(if(id_unit_serial is null,1,0)),0) as qty_pending,ifnull(sum(if(id_unit_serial is not null,1,0)),0) qty_stok");
            $unitket = $this->db->get()->row();

            if (!CheckEmpty(@$model['id_kpu'])) {
                $kpu['status'] = $unitket->qty_pending > 0 ? 1 : 4;
                $res = $this->db->update($this->table, $kpu, array("id_kpu" => $model['id_kpu']));
                $this->db->update("#_kpu", $kpu, array("id_kpu" => $id_kpu));
            }
            if ($unitket->qty_stok > $kpudetail['qty']) {
                $message .= "Jumlah Unit teregister dalam sistem lebih banyak dari jumlah kpu<br/>";
            }


            $this->ChangeStatusKPU($id_kpu, "#_kpu.id_kpu");
            if ($this->db->trans_status() === FALSE || $message != '') {
                $this->db->trans_rollback();
                $message = "Some Error Occured<br/>" . $message;
                return array("st" => false, "msg" => $message);
            } else {
                ClearCookiePrefix("_kpu", $jenis_pencarian);
                $this->db->trans_commit();
                SetMessageSession(true, "Data KPU Sudah di" . (CheckEmpty(@$model['id_kpu']) ? "masukkan" : "update") . " ke dalam database");
                $arrayreturn["st"] = true;
                SetPrint($id_kpu, $kpu['nomor_master'], 'pembelian');
                return $arrayreturn;
            }
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }

    /*function GenerateJurnalKPU($keyword, $type = "#_kpu.id_kpu", $nomor_before) {
        $this->load->model("supplier/m_supplier");
        $kpu = $this->GetOneKpu($keyword, $type, false);
        $supplier = $this->m_supplier->GetOneSupplier($kpu->id_supplier);
        $userid = GetUserId();
        if ($kpu) {
            $this->load->model("neraca/m_neraca");
            $this->m_neraca->UpdateDokumen($kpu->nomor_master, $nomor_before);

            $totalpembelianwithoutppn = 0;
            $totalpembelianwithppn = 0;
            $totalinvoice = 0;
            $totalinvoice = ($kpu->price - $kpu->disc1) * $kpu->qty;
            $totalhitungan = $totalinvoice;
            $nominalppn = 0;
            if ($kpu->type_pembulatan == "1") {
                $totalhitungan = $totalhitungan - $kpu->disc_pembulatan;
            }

            if ($kpu->type_ppn == "1") {
                $totalpembelianwithoutppn += ($totalhitungan * 100) / (100 + $kpu->ppn);
                $totalpembelianwithppn += $totalhitungan;
            } else {
                $totalpembelianwithoutppn += $totalhitungan;
                $totalpembelianwithppn += $totalhitungan * (100 + $kpu->ppn) / 100;
            }

            $nominalppn = $totalpembelianwithppn - $totalpembelianwithoutppn;

            if ($kpu->type_pembulatan == "1") {
                $totalpembelianwithoutppn = $totalpembelianwithoutppn - $kpu->disc_pembulatan;
            } else {
                $totalpembelianwithppn = $totalpembelianwithppn - $kpu->disc_pembulatan;
            }



            $this->db->from("#_pembayaran_utang_detail");
            $this->db->join("#_pembayaran_utang_master", "#_pembayaran_utang_master.pembayaran_utang_id=#_pembayaran_utang_detail.pembayaran_utang_id");
            $this->db->where(array("keterangan" => "Pembayaran Di Muka", "id_kpu" => $kpu->id_kpu));

            $pembayaran_utang_master = $this->db->get()->row();
            if ($pembayaran_utang_master != null && $kpu->jumlah_bayar == 0) {
                $this->db->delete("#_pembayaran_utang_detail", array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->delete("#_pembayaran_utang_master", array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master != null && $pembayaran_utang_master->jumlah_bayar != $kpu->jumlah_bayar) {
                $updatedet = array();
                $updatedet['updated_date'] = GetDateNow();
                $updatedet['updated_by'] = $userid;
                $updatedet['jumlah_bayar'] = $kpu->jumlah_bayar;
                $updatemas = array();
                $updatemas['updated_date'] = GetDateNow();
                $updatemas['updated_by'] = $userid;
                $updatemas['total_bayar'] = $kpu->jumlah_bayar;
                $this->db->update("#_pembayaran_utang_detail", $updatedet, array("pembayaran_utang_detail_id" => $pembayaran_utang_master->pembayaran_utang_detail_id));
                $this->db->update("#_pembayaran_utang_master", $updatemas, array("pembayaran_utang_id" => $pembayaran_utang_master->pembayaran_utang_id));
            } else if ($pembayaran_utang_master == null && $kpu->jumlah_bayar > 0) {
                $dokumenpembayaran = AutoIncrement('#_pembayaran_utang_master', 'BB/' . date("y") . '/', 'dokumen', 5);
                $pembayaranmaster = array();
                $pembayaranmaster['dokumen'] = $dokumenpembayaran;
                $pembayaranmaster['tanggal_transaksi'] = $kpu->tanggal;
                $pembayaranmaster['id_supplier'] = $kpu->id_supplier;
                $pembayaranmaster['jenis_pembayaran'] = "Cash";
                $pembayaranmaster['total_bayar'] = $kpu->jumlah_bayar > $kpu->grandtotal ? $kpu->grandtotal : $kpu->jumlah_bayar;
                $pembayaranmaster['biaya'] = 0;
                $pembayaranmaster['potongan'] = 0;
                $pembayaranmaster['status'] = 1;
                $pembayaranmaster['keterangan'] = "Pembayaran Di Muka";
                $pembayaranmaster['created_date'] = GetDateNow();
                $pembayaranmaster['created_by'] = $userid;
                $this->db->insert("#_pembayaran_utang_master", $pembayaranmaster);
                $pembayaran_utang_id = $this->db->insert_id();
                $pembayarandetail = array();
                $pembayarandetail['id_kpu'] = $kpu->id_kpu;
                $pembayarandetail['pembayaran_utang_id'] = $pembayaran_utang_id;
                $pembayarandetail['jumlah_bayar'] = $kpu->jumlah_bayar;
                $pembayarandetail['created_date'] = GetDateNow();
                $pembayarandetail['created_by'] = $userid;
                $this->db->insert("#_pembayaran_utang_detail", $pembayarandetail);
            }
            $this->load->model("gl_config/m_gl_config");
            $akunglpembelian = $this->m_gl_config->GetIdGlConfig("pembelian", $kpu->id_cabang, array("kategori" => $kpu->id_kategori, "unit" => $kpu->id_unit));
            $arrakun = [];
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglpembelian, "dokumen" => $kpu->nomor_master));
            $akun_pembelian = $this->db->get()->row();

            if (CheckEmpty($akun_pembelian)) {
                $akunpembelianinsert = array();
                $akunpembelianinsert['id_gl_account'] = $akunglpembelian;
                $akunpembelianinsert['tanggal_buku'] = $kpu->tanggal;
                $akunpembelianinsert['keterangan'] = "Pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunpembelianinsert['dokumen'] = $kpu->nomor_master;
                $akunpembelianinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpembelianinsert['id_subject'] = $kpu->id_supplier;
                $akunpembelianinsert['debet'] = $totalpembelianwithoutppn;
                $akunpembelianinsert['kredit'] = 0;
                $akunpembelianinsert['created_date'] = GetDateNow();
                $akunpembelianinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpembelianinsert);
                $arrakun[] = $this->db->insert_id();
            } else {
                $akunpembelianupdate['tanggal_buku'] = $kpu->tanggal;
                $akunpembelianupdate['keterangan'] = "Pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunpembelianupdate['dokumen'] = $kpu->nomor_master;
                $akunpembelianupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpembelianupdate['id_subject'] = $kpu->id_supplier;
                $akunpembelianupdate['debet'] = $totalpembelianwithoutppn;
                $akunpembelianupdate['kredit'] = 0;
                $akunpembelianupdate['updated_date'] = GetDateNow();
                $akunpembelianupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpembelianupdate, array("id_buku_besar" => $akun_pembelian->id_buku_besar));
                $arrakun[] = $akun_pembelian->id_buku_besar;
            }

            $akunglppn = $this->m_gl_config->GetIdGlConfig("ppnpembelian", $kpu->id_cabang, array("kategori" => $kpu->id_kategori, "unit" => $kpu->id_unit));
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglppn, "dokumen" => $kpu->nomor_master));
            $akun_ppn = $this->db->get()->row();

            if (CheckEmpty($akun_ppn) && $kpu->nominal_ppn > 0) {
                $akunppninsert = array();
                $akunppninsert['id_gl_account'] = $akunglppn;
                $akunppninsert['tanggal_buku'] = $kpu->tanggal;
                $akunppninsert['keterangan'] = "PPN pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunppninsert['dokumen'] = $kpu->nomor_master;
                $akunppninsert['subject_name'] = @$supplier->kode_supplier;
                $akunppninsert['id_subject'] = $kpu->id_supplier;
                $akunppninsert['debet'] = $nominalppn;
                $akunppninsert['kredit'] = 0;
                $akunppninsert['created_date'] = GetDateNow();
                $akunppninsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunppninsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_ppn)) {
                $akunppnupdate['tanggal_buku'] = $kpu->tanggal;
                $akunppnupdate['keterangan'] = "PPN pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunppnupdate['dokumen'] = $kpu->nomor_master;
                $akunppnupdate['subject_name'] = @$supplier->kode_supplier;
                $akunppnupdate['id_subject'] = $kpu->id_supplier;
                $akunppnupdate['debet'] = $nominalppn;
                $akunppnupdate['kredit'] = 0;
                $akunppnupdate['updated_date'] = GetDateNow();
                $akunppnupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunppnupdate, array("id_buku_besar" => $akun_ppn->id_buku_besar));
                $arrakun[] = $akun_ppn->id_buku_besar;
            }


            $akunglutang = $this->m_gl_config->GetIdGlConfig("utang", $kpu->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunglutang, "dokumen" => $kpu->nomor_master));
            $akun_utang = $this->db->get()->row();

            if (CheckEmpty($akun_utang) && $kpu->jumlah_bayar < $kpu->grandtotal) {
                $akunutanginsert = array();
                $akunutanginsert['id_gl_account'] = $akunglutang;
                $akunutanginsert['tanggal_buku'] = $kpu->tanggal;
                $akunutanginsert['keterangan'] = "Utang pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunutanginsert['dokumen'] = $kpu->nomor_master;
                $akunutanginsert['subject_name'] = @$supplier->kode_supplier;
                $akunutanginsert['id_subject'] = $kpu->id_supplier;
                $akunutanginsert['debet'] = 0;
                $akunutanginsert['kredit'] = $kpu->grandtotal - $kpu->jumlah_bayar;
                $akunutanginsert['created_date'] = GetDateNow();
                $akunutanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunutanginsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_utang)) {
                $akunutangupdate['tanggal_buku'] = $kpu->tanggal;
                $akunutangupdate['keterangan'] = "Utang pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunutangupdate['dokumen'] = $kpu->nomor_master;
                $akunutangupdate['subject_name'] = @$supplier->kode_supplier;
                $akunutangupdate['id_subject'] = $kpu->id_supplier;
                $akunutangupdate['debet'] = 0;
                $akunutangupdate['kredit'] = $kpu->grandtotal - $kpu->jumlah_bayar;
                $akunutangupdate['updated_date'] = GetDateNow();
                $akunutangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunutangupdate, array("id_buku_besar" => $akun_utang->id_buku_besar));
                $arrakun[] = $akun_utang->id_buku_besar;
            }
            $this->m_supplier->KoreksiSaldoHutang(@$supplier->id_supplier);
            $this->UpdateStatusKPU($kpu->id_kpu);
            if ($kpu->id_vrf > 0) {
                $this->m_vrf->ChangeManualVRF($kpu->id_vrf);
            }
            $akunpotonganpembelian = $this->m_gl_config->GetIdGlConfig("potonganpembelian", $kpu->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpotonganpembelian, "dokumen" => $kpu->nomor_master));
            $akun_potongan = $this->db->get()->row();

            if (CheckEmpty($akun_potongan) && $kpu->disc_pembulatan > 0) {
                $akunpotonganinsert = array();
                $akunpotonganinsert['id_gl_account'] = $akunpotonganpembelian;
                $akunpotonganinsert['tanggal_buku'] = $kpu->tanggal;
                $akunpotonganinsert['keterangan'] = "Potongan pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunpotonganinsert['dokumen'] = $kpu->nomor_master;
                $akunpotonganinsert['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganinsert['id_subject'] = $kpu->id_supplier;
                $akunpotonganinsert['debet'] = 0;
                $akunpotonganinsert['kredit'] = $kpu->disc_pembulatan;
                $akunpotonganinsert['created_date'] = GetDateNow();
                $akunpotonganinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunpotonganinsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_potongan)) {
                $akunpotonganupdate['tanggal_buku'] = $kpu->tanggal;
                $akunpotonganupdate['keterangan'] = "Potongan pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunpotonganupdate['dokumen'] = $kpu->nomor_master;
                $akunpotonganupdate['subject_name'] = @$supplier->kode_supplier;
                $akunpotonganupdate['id_subject'] = $kpu->id_supplier;
                $akunpotonganupdate['debet'] = 0;
                $akunpotonganupdate['kredit'] = $kpu->disc_pembulatan;
                $akunpotonganupdate['updated_date'] = GetDateNow();
                $akunpotonganupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunpotonganupdate, array("id_buku_besar" => $akun_potongan->id_buku_besar));
                $arrakun[] = $akun_potongan->id_buku_besar;
            }

            $akunkas = $this->m_gl_config->GetIdGlConfig("kasbesar", $kpu->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunkas, "dokumen" => $kpu->nomor_master));
            $akun_kas = $this->db->get()->row();

            if (CheckEmpty($akun_kas) && $kpu->jumlah_bayar > 0) {
                $akunkasinsert = array();
                $akunkasinsert['id_gl_account'] = $akunkas;
                $akunkasinsert['tanggal_buku'] = $kpu->tanggal;
                $akunkasinsert['keterangan'] = "Pengeluaran kas untuk pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunkasinsert['dokumen'] = $kpu->nomor_master;
                $akunkasinsert['subject_name'] = @$supplier->kode_supplier;
                $akunkasinsert['id_subject'] = $kpu->id_supplier;
                $akunkasinsert['debet'] = 0;
                $akunkasinsert['kredit'] = $kpu->jumlah_bayar;
                $akunkasinsert['created_date'] = GetDateNow();
                $akunkasinsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akunkasinsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_kas)) {
                $akunkasupdate['tanggal_buku'] = $kpu->tanggal;
                $akunkasupdate['keterangan'] = "Pengeluaran kas untuk Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akunkasupdate['dokumen'] = $kpu->nomor_master;
                $akunkasupdate['subject_name'] = @$supplier->kode_supplier;
                $akunkasupdate['id_subject'] = $kpu->id_supplier;
                $akunkasupdate['debet'] = 0;
                $akunkasupdate['kredit'] = $kpu->jumlah_bayar;
                $akunkasupdate['updated_date'] = GetDateNow();
                $akunkasupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akunkasupdate, array("id_buku_besar" => $akun_kas->id_buku_besar));
                $arrakun[] = $akun_kas->id_buku_besar;
            }

            $this->db->where(array("dokumen" => $kpu->nomor_master));
            if (count($arrakun) > 0) {
                $this->db->where_not_in("id_buku_besar", $arrakun);
            }
            $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)));



            $akunpenyeimbang = $this->m_gl_config->GetIdGlConfig("penyeimbang", $kpu->id_cabang, array());
            $this->db->from("#_buku_besar");
            $this->db->where(array("dokumen" => $kpu->nomor_master, "id_gl_account !=" => $akunpenyeimbang));
            $this->db->select("sum(debet)-sum(kredit) as selisih");
            $row = $this->db->get()->row();
            $this->db->from("#_buku_besar");
            $this->db->where(array("id_gl_account" => $akunpenyeimbang, "dokumen" => $kpu->nomor_master));
            $akun_penyeimbang = $this->db->get()->row();

            if (CheckEmpty($akun_penyeimbang) && $row && $row->selisih > 0) {
                $akun_penyeimbanginsert = array();
                $akun_penyeimbanginsert['id_gl_account'] = $akunpenyeimbang;
                $akun_penyeimbanginsert['tanggal_buku'] = $kpu->tanggal;
                $akun_penyeimbanginsert['keterangan'] = "Penyeimbang untuk pembelian Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akun_penyeimbanginsert['dokumen'] = $kpu->nomor_master;
                $akun_penyeimbanginsert['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbanginsert['id_subject'] = $kpu->id_supplier;
                $akun_penyeimbanginsert['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbanginsert['created_date'] = GetDateNow();
                $akun_penyeimbanginsert['created_by'] = $userid;
                $this->db->insert("#_buku_besar", $akun_penyeimbanginsert);
                $arrakun[] = $this->db->insert_id();
            } else if (!CheckEmpty($akun_penyeimbang)) {
                $akun_penyeimbangupdate['tanggal_buku'] = $kpu->tanggal;
                $akun_penyeimbangupdate['id_cabang'] = $kpu->id_cabang;
                $akun_penyeimbangupdate['keterangan'] = "Penyeimbang untuk Ke " . @$supplier->nama_supplier . ' ' . $kpu->keterangan;
                $akun_penyeimbangupdate['dokumen'] = $kpu->nomor_master;
                $akun_penyeimbangupdate['subject_name'] = @$supplier->kode_supplier;
                $akun_penyeimbangupdate['id_subject'] = $kpu->id_supplier;
                $akun_penyeimbangupdate['debet'] = DefaultCurrencyDatabase($row->selisih) > 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['kredit'] = DefaultCurrencyDatabase($row->selisih) < 0 ? 0 : abs(DefaultCurrencyDatabase($row->selisih));
                $akun_penyeimbangupdate['updated_date'] = GetDateNow();
                $akun_penyeimbangupdate['updated_by'] = $userid;
                $this->db->update("#_buku_besar", $akun_penyeimbangupdate, array("id_buku_besar" => $akun_penyeimbang->id_buku_besar));
                $arrakun[] = $akun_penyeimbang->id_buku_besar;
            }
        }
    }
     */

    function GetUnitFromKPU($id_kpu) {

        $this->db->from("#_unit_serial");
        $this->db->join("#_kpu_detail", "#_kpu_detail.id_kpu_detail=#_unit_serial.id_kpu_detail");
        $this->db->where(array("id_kpu" => $id_kpu));
        $listunit = $this->db->get()->result();
        return $listunit;
    }

    function GetDropDownKPUUnitSerial($param = []) {


        if (!CheckEmpty(@$param['id_unit'])) {
            $where['#_unit.id_unit'] = $param['id_unit'];
        }
        if (!CheckEmpty(@$param['id_kategori'])) {
            $where['#_unit.id_kategori'] = $param['id_kategori'];
        }

        if (count($where) > 0) {
            $this->db->where($where);
        }


        $this->db->join("#_customer", "#_kpu.id_customer=#_customer.id_customer", "left");
        $this->db->join("#_kpu_detail", "#_kpu.id_kpu=#_kpu_detail.id_kpu", "left");
        $this->db->join("#_unit", "#_kpu_detail.id_unit=#_unit.id_unit", "left");
        $this->db->join("#_unit_serial", "#_unit_serial.id_kpu_detail=#_kpu_detail.id_kpu_detail", "left");
        $this->db->where(array("id_do_prospek_detail" => null));
        $this->db->group_by("#_kpu.id_kpu");
        $select = "#_kpu.id_kpu,nomor_master,nama_customer,nama_unit,concat(#_kpu_detail.qty,' unit') as qty";
        $listkpu = GetTableData($this->table, 'id_kpu', array("id_kpu", 'nomor_master', "nama_customer", "nama_unit", "qty"), array($this->table . '.status !=' => 2, $this->table . '.deleted_date' => null), "json", [], [], [], $select,[],2000);

        return $listkpu;
    }

    function GetDropDownKpu() {
        $listkpu = GetTableData($this->table, 'id_kpu', '', array($this->table . '.status' => 1, $this->table . '.deleted_date' => null));

        return $listkpu;
    }

    function KPUBatal($id_kpu) {
        try {
            $kpu = $this->GetOneKpu($id_kpu);
            $model['status'] = 2;
            $model = MergeUpdate($model);
            if ($kpu) {
                $this->db->update("#_buku_besar", MergeUpdate(array("debet" => 0, "kredit" => 0)), array("dokumen" => $kpu->nomor_master));
            }

            $this->db->update($this->table, $model, array('id_kpu' => $id_kpu));
            $this->db->update("#_kpu_detail", $model, array('id_kpu' => $id_kpu));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "KPU sudah dibatalkan");
    }

    function GetKpuByVrf($id_vrf) {
        $this->db->select("vrf_code, nomor_master, tanggal, #_kpu.keterangan, #_kpu.total, jth_tempo, #_kpu_detail.qty, #_kpu_detail.price, #_kpu_detail.disc1, #_kpu_detail.vin, #_kpu.top, #_kpu.tanggal_pelunasan, #_kpu.nominal_ppn, #_kpu.jumlah_bayar, #_kpu.disc_pembulatan, #_kpu.ppn, #_kpu.grandtotal, #_kpu.id_kpu");
        $this->db->where('#_vrf.id_vrf', $id_vrf);
        $this->db->where('#_vrf.deleted_date', null);
        $this->db->join("#_kpu_detail", "#_kpu_detail.id_vrf=#_vrf.id_vrf");
        $this->db->join($this->table, $this->table . '.id_kpu = #_kpu_detail.id_kpu', 'left');
        $grid_kpu = $this->db->get('#_vrf')->result_array();
        foreach ($grid_kpu as $row => $value) {
            $this->db->from("#_do_kpu");
            $this->db->from("#_do_kpu_detail", "#_do_kpu_detail.id_do_kpu=#_do_kpu.id_do_kpu");
            $this->db->where(array("#_do_kpu.id_do_kpu" => $value['id_kpu'], "#_do_kpu.status !=" => 2, "#_do_kpu_detail.status !=" => 2));
            $this->db->select("IFNULL(SUM(qty_do),0) as jumlah");
            $res = $this->db->get()->row();
            $jml = $res ? $res->jumlah : 0;
            $grid_kpu[$row]['qty_sisa'] = $value['qty'] - $jml;
        }

        return $grid_kpu;
    }

}
