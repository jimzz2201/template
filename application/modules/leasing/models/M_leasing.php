<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_leasing extends CI_Model
{

    public $table = '#_leasing';
    public $id = 'id_leasing';
    public $order = 'DESC';
    public $kodeincrement = '10';
    public $incrementkey = 5;

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function GetDataleasing() {
        $this->load->library('datatables');
        $this->datatables->select('id_leasing,kode_leasing,nama_leasing,telp_leasing,email_leasing,alamat_leasing,nama_kota,kode_pos,dgmi_leasing.status');
        $this->datatables->from($this->table);
        $this->datatables->where($this->table.'.deleted_date', null);
        //add this line for join
        $this->datatables->join('#_kota', 'dgmi_leasing.id_kota = dgmi_kota.id_kota');
        $isedit = true;
        $isdelete = true;
        $straction = '';
        if ($isedit) {
            $straction.=anchor(site_url('leasing/edit_leasing/$1'), 'Update', array('class' => 'btn btn-primary btn-xs'));
        }
        if ($isdelete) {
            $straction.=anchor("", 'Delete', array('class' => 'btn btn-danger btn-xs', "onclick" => "deleteleasing($1);return false;"));
        }
        $this->datatables->add_column('action', $straction, 'id_leasing');
        return $this->datatables->generate();
    }

    // get all
    function GetOneLeasing($keyword, $type = 'id_leasing') {
        $this->db->where($type, $keyword);
        $this->db->where($this->table.'.deleted_date', null);
        $leasing = $this->db->get($this->table)->row();
        return $leasing;
    }

    function LeasingManipulate($model) {
        try {
                $model['id_kota'] = ForeignKeyFromDb($model['id_kota']);
                $model['status'] = DefaultCurrencyDatabase($model['status']);

            if (CheckEmpty($model['id_leasing'])) {                $model['created_date'] = GetDateNow();
                $model['created_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->insert($this->table, $model);
		SetMessageSession(1, 'Leasing successfull added into database');
		return array("st" => true, "msg" => "Leasing successfull added into database");
            } else {
                $model['updated_date'] = GetDateNow();
                $model['updated_by'] = ForeignKeyFromDb(GetUserId());
                $this->db->update($this->table, $model, array("id_leasing" => $model['id_leasing']));
		SetMessageSession(1, 'Leasing has been updated');
		return array("st" => true, "msg" => "Leasing has been updated");
            }
        } catch (Exception $ex) {
            return array("st" => false, "msg" => $ex->getMessage());
        }
    }
    
    function GetDropDownLeasing() {
        $listleasing = GetTableData($this->table, 'id_leasing', 'nama_leasing', array($this->table.'.status' => 1,$this->table.'.deleted_date' => null));

        return $listleasing;
    }

    function LeasingDelete($id_leasing) {
        try {
            $model['deleted_date'] = GetDateNow();
            $model['deleted_by'] = GetUserId();
            $this->db->update($this->table, $model, array('id_leasing' => $id_leasing));
        } catch (Exception $ex) {
            return array("st" => false, "msg" => "Some Error Occured");
        }
        return array("st" => true, "msg" => "Leasing has been deleted from database");
    }


}