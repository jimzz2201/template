<section class="content-header">
    <h1>
        Leasing <?= @$button ?>
        <small>Leasing</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Leasing</li>
        <li class="active">Leasing <?= @$button ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_leasing" class="form-horizontal form-groups-bordered validate" method="post">
                                <input type="hidden" name="id_leasing" value="<?php echo @$id_leasing; ?>" /> 
                                <div class="form-group">
                                    <?= form_label('Kode Leasing', "txt_kode_leasing", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?php
                                        $mergearray = array();
                                        if (!CheckEmpty(@$id_leasing)) {
                                            $mergearray['readonly'] = "readonly";
                                            $mergearray['name'] = "kode_leasing";
                                        } else {
                                            $mergearray['name'] = "kode_leasing";
                                        }
                                        ?>
                                        <?= form_input(array_merge($mergearray, array('type' => 'text', 'value' => @$kode_pos, 'class' => 'form-control', 'id' => 'txt_kode_pos', 'placeholder' => 'Kode Leasing'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Nama Leasing', "txt_nama_leasing", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_input(array('type' => 'text', 'name' => 'nama_leasing', 'value' => @$nama_leasing, 'class' => 'form-control', 'id' => 'txt_nama_leasing', 'placeholder' => 'Nama Leasing')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Telp Leasing', "txt_telp_leasing", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_input(array('type' => 'text', 'name' => 'telp_leasing', 'value' => @$telp_leasing, 'class' => 'form-control', 'id' => 'txt_telp_leasing', 'placeholder' => 'Telp Leasing')); ?>
                                    </div>
                                    <?= form_label('Email Leasing', "txt_email_leasing", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">
                                        <?= form_input(array('type' => 'text', 'name' => 'email_leasing', 'value' => @$email_leasing, 'class' => 'form-control', 'id' => 'txt_email_leasing', 'placeholder' => 'Email Leasing')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Alamat Leasing', "txt_alamat_leasing", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-10">
                                        <?= form_textarea(array('type' => 'text', 'name' => 'alamat_leasing', 'rows' => '3', 'cols' => '10', 'value' => @$alamat_leasing, 'class' => 'form-control', 'id' => 'txt_alamat', 'placeholder' => 'Alamat')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= form_label('Kota', "txt_id_kota", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array('type' => 'text', 'name' => 'id_kota', 'class' => 'form-control', 'id' => 'dd_id_kota', 'placeholder' => 'kota'), DefaultEmptyDropdown($list_kota, "json", "Kota"), @$id_kota); ?>
                                    </div>
                                    <?= form_label('Kode Pos', "txt_kode_pos", array("class" => 'col-sm-1 control-label')); ?>
                                    <div class="col-sm-5">

                                        <?= form_input(array('type' => 'text', 'value' => @$kode_pos, 'class' => 'form-control', 'id' => 'txt_kode_pos', 'placeholder' => 'Kode Pos')); ?>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <?= form_label('Status', "txt_status", array("class" => 'col-sm-2 control-label')); ?>
                                    <div class="col-sm-4">
                                        <?= form_dropdown(array("selected" => @$status, "name" => "status"), array('1' => 'Active', '0' => 'Not Active'), @$status, array('class' => 'form-control', 'id' => 'status')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/leasing' ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><script>
    $("#frm_leasing").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/leasing/leasing_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/leasing';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    })
</script>
<style>
    .control-label {
        text-align: left !important;
    }
</style>