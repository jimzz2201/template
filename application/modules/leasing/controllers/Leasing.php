<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Leasing extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_leasing');
    }

    public function index()
    {
        $javascript = array();
        $javascript[] = "assets/plugins/datatables/jquery.dataTables.js";
        $javascript[] = "assets/plugins/datatables/dataTables.bootstrap.js";
        $module = "K052";
        $header = "K001";
        $title = "Leasing";
        $model=array("title" => $title, "form" => $header, "formsubmenu" => $module);
	LoadTemplate($model, "leasing/v_leasing_index", $javascript);
        
    } 
    public function get_one_leasing() {
        $model = $this->input->post();
        $this->form_validation->set_rules('id_leasing', 'Leasing', 'trim|required');
        $message = "";
        if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $data['obj'] = $this->m_leasing->GetOneLeasing($model["id_leasing"]);
            $data['st'] = $data['obj'] != null;
            $data['msg'] = !$data['st'] ? "Data Leasing tidak ditemukan dalam database" : "";
            echo json_encode($data);
        }
    }
    public function getdataleasing() {
        header('Content-Type: application/json');
        echo $this->m_leasing->GetDataleasing();
    }
    
    public function create_leasing() 
    {
        $row=['button'=>'Add'];
        $javascript = array();
	    $module = "K052";
        $header = "K001";
        $row['title'] = "Add Leasing";
        CekModule($module);
        $row['form'] = $header;
        $row['formsubmenu'] = $module;        
        $this->load->model("kota/m_kota");
        $row['list_kota'] = $this->m_kota->GetDropDownKota();
	LoadTemplate($row,'leasing/v_leasing_manipulate', $javascript);       
    }
    
    public function leasing_manipulate() 
    {
        $message='';
        $model=$this->input->post();
        if(CheckEmpty(@$model['id_leasing']))
        {
            $this->form_validation->set_rules('kode_leasing', 'Kode Leasing', 'trim|required');
        }
	$this->form_validation->set_rules('nama_leasing', 'Nama Leasing', 'trim|required');
	$this->form_validation->set_rules('id_kota', 'Kota', 'trim|required');
        $model=$this->input->post();
         if ($this->form_validation->run() === FALSE || $message !== '') {
            echo json_encode(array('st' => false, 'msg' => 'Error :<br/>' . validation_errors() . $message));
        } else {
            $status=$this->m_leasing->LeasingManipulate($model);
            echo json_encode($status);
        }
    }
    
    public function edit_leasing($id=0) 
    {
        $row = $this->m_leasing->GetOneLeasing($id);
        
        if ($row) {
            $row->button='Update';
            $row = json_decode(json_encode($row), true);
            $javascript = array();
	        $module = "K052";
            $header = "K001";
            $row['title'] = "Edit Leasing";
            CekModule($module);
            $row['form'] = $header;
            $row['formsubmenu'] = $module;        
            $this->load->model("kota/m_kota");
            $row['list_kota'] = $this->m_kota->GetDropDownKota();
	    LoadTemplate($row,'leasing/v_leasing_manipulate', $javascript);
        } else {
            SetMessageSession(0, "Leasing cannot be found in database");
            redirect(site_url('leasing'));
        }
    }
    
    public function leasing_delete() 
    {
        $message='';
        $this->form_validation->set_rules('id_leasing', 'Leasing', 'required');
        if ($this->form_validation->run() == FALSE||$message!='') {
            $result=array();
            $result['st']=false;
            $result['msg']='Error :<br/>' . validation_errors() . $message;
        }
        else {
            $model=$this->input->post();
            $result=$this->m_leasing->LeasingDelete($model['id_leasing']);
        }
        
        echo json_encode($result);
        
    }

}

/* End of file Leasing.php */