<section class="content-header">
    <h1>
        <?=GetJenisBO($jenis_bo);?><?= @$button ?>
        <small>Create</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url() ?>"><i class="fa fa-dashboard"></i>Home</a></li>
        <li>Data Entry</li>
        <li><?=GetJenisBO($jenis_bo);?></li>
        <li class="active">Create</li>
    </ol>
</section>
<section class="content">
    <div class="box box-default">
        <div class="box-body" >
            <table class="tablestadt" >
                <tbody>
                    <tr>
                        <td style="width: 25%">
                            <b>No.SPJ</b>
                        </td>
                        <td style="width: 25%">
                            <span id="lbl_no_spj"><?= @$no_spj ?></span>
                        </td>
                        <td>
                            <b>Tanggal</b>
                        </td>
                        <td>
                            <span id="lbl_tanggal"><?= DefaultDatePicker(@$tanggal) ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>No Body</b>
                        </td>
                        <td>
                            <span id="lbl_no_body"><?= @$no_body ?></span>
                        </td>
                        <td>
                            <b>Jam</b>
                        </td>
                        <td>
                            <span id="lbl_jam"><?= DefaultTimePicker(@jam) ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>No Polisi</b>
                        </td>
                        <td>
                            <span id="lbl_nopol"><?= @$nopol ?></span>
                        </td>
                        <td>
                            <b>Jenis</b>
                        </td>
                        <td>
                            <span id="lbl_jenis"><?= @$nama_jenis_spj ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Asal Barat</b>
                        </td>
                        <td>
                            <span id="lbl_asal_barat"><?= @$kode_asal_barat . ' / ' . @$nama_asal_barat ?></span>
                        </td>
                        <td>
                            <b>Tujuan Timur</b>
                        </td>
                        <td>
                            <span id="lbl_tujuan_timur"><?= @$kode_tujuan_timur . ' / ' . @$nama_tujuan_timur ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Asal Timur</b>
                        </td>
                        <td>
                            <span id="lbl_asal_timur"><?= @$kode_asal_timur . ' / ' . @$nama_asal_timur ?></span>
                        </td>
                        <td>
                            <b>Tujuan Barat</b>
                        </td>
                        <td>
                            <span id="lbl_tujuan_barat"><?= @$kode_tujuan_barat . ' / ' . @$nama_tujuan_barat ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Kode SPJ</b>
                        </td>
                        <td>
                            <span id="lbl_kode_spj"><?= @$no_random ?></span>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border-top-style: solid; border-top-width: 1px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Pengemudi 1</b>
                        </td>
                        <td>
                            <span id="lbl_nama_pengemudi_1"><?= @$nama_pengemudi_1 ?></span>
                        </td>
                        <td>
                            <b>NIC</b>
                        </td>
                        <td>
                            <span id="lbl_nic_pengemudi_1"><?= @$nic_pengemudi_1 ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Pengemudi 2</b>
                        </td>
                        <td>
                            <span id="lbl_nama_pengemudi_2"><?= @$nama_pengemudi_2 ?></span>
                        </td>
                        <td>
                            <b>NIC</b>
                        </td>
                        <td>
                            <span id="lbl_nic_pengemudi_2"><?= @$nic_pengemudi_2 ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Kernet</b>
                        </td>
                        <td>
                            <span id="lbl_nama_kernet"><?= @$nama_kernet ?></span>
                        </td>
                        <td>
                            <b>NIC</b>
                        </td>
                        <td>
                            <span id="lbl_nic_kernet"><?= @$nic_kernet ?></span>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div class="box-body">
            <div id="notification" ></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel" data-collapsed="0">
                        <div class="panel-body"><form id="frm_operasional" class="form-horizontal form-groups-bordered validate" method="post">
                                <div class="form-group">
                                    <?= form_input(array('type' => 'hidden', 'name' => 'id_spj', 'value' => @$id_spj, 'class' => 'form-control', 'id' => 'txt_id_spj')); ?>

                                    <?= form_label('Tanggal', "txt_tanggal", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'tanggal', 'value' => DefaultDatePicker(@$tanggal), 'class' => 'form-control datepicker', 'id' => 'txt_tgl_stadt', 'placeholder' => 'Tgl Stadt')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Keterangan', "txt_id_asal", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-6">
                                        <?= form_textarea(array('type' => 'text', 'name'=>'keterangan', 'value' => '', 'class' => 'form-control ', 'id' => 'txt_keterangan', 'placeholder' => 'Keterangan')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <?= form_label('Nominal', "txt_nominal", array("class" => 'col-sm-4 control-label')); ?>
                                    <div class="col-sm-3">
                                        <?= form_input(array('type' => 'text', 'name' => 'nominal', 'value' => '', 'onkeyup'=>'javascript:this.value=Comma(this.value);', 'onkeypress'=>'return isNumberKey(event);','class' => 'form-control', 'id' => 'txt_nominal', 'placeholder' => 'Nominal')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <a href="<?php echo base_url() . 'index.php/'. $jenis_bo;  ?>" class="btn btn-default"  >Cancel</a>
                                    <button type="submit" class="btn btn-primary" id="btt_modal_ok" >Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script>

    $(document).ready(function () {
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd M yyyy',
            weekStart: 1,
            language: "id",
            todayHighlight: true,
        });
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
    });
    function deleteRow(sender)
    {
        $(sender).parent().parent().remove();
    }

    $("#frm_operasional").submit(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + 'index.php/<?=$jenis_bo;?>/bo_manipulate',
            dataType: 'json',
            data: $(this).serialize(),
            success: function (data) {
                if (data.st)
                {
                    window.location.href = baseurl + 'index.php/<?=$jenis_bo;?>';
                } else
                {
                    messageerror(data.msg);
                }

            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;

    });

</script>