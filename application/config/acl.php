<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$acl = array(
    'keberangkatan' => array(
        'view' => array(
            1 => TRUE,
            2 => TRUE,
        ),
        'input' => array(
            1 => TRUE,
        ),
        'edit' => array(
            1 => TRUE,
        ),
        'delete' => array(
            1 => FALSE,
        )
    ),
    
    'pemesanan' => array(
        'view' => array(
            1 => TRUE,
            2 => TRUE,
        ),
        'input' => array(
            1 => TRUE,
        ),
        'edit' => array(
            1 => TRUE,
        ),
        'delete' => array(
            1 => TRUE,
        )
    ),

);

$config['acl'] = $acl;
/* End of file acl.php */
/* Location: ./application/config/acl.php */
