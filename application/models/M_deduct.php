<?php

class M_deduct extends CI_Model {

	private $_table = '#_deduct';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function deductManipulate($model){
	    $status = ['st'=>false];
        if(!isset($model['id_deduct'])){
	        $model['is_batal'] = 0;
            $model['created_date'] = GetDateNow();
            $model['created_by'] = GetUserId();

	        if($this->db->insert($this->_table, $model)){
		        $status['st'] = true;
		        $status['msg'] = "Deduct berhasil disimpan";
	        }else{
		        $status['msg'] = "Deduct gagal disimpan";
	        }
        }else{
            $model['updated_date'] = GetDateNow();
            $model['updated_by'] = GetUserId();

	        if($this->db->update($this->_table, $model, ['id_deduct'=>$model['id_deduct']])){
		        $status['st'] = true;
		        $status['msg'] = "Deduct berhasil diupdate";
	        }else{
		        $status['msg'] = "Deduct gagal diupdate";
	        }
        }

	    return $status;
    }

	function getData($id_pangkalan, $id_agen=null){
		$data = [];
		$komisiPrev = $this->getSummaryDeduct(1, $id_pangkalan, $id_agen, null, true, "prev");
		$komisiNow = $this->getSummaryDeduct(1, $id_pangkalan, $id_agen, null, true, "now");
//		echo $this->db->last_query();
		$refund = $this->getSummaryDeduct(2, $id_pangkalan, $id_agen, null, true);
//		echo $this->db->last_query();

		if($komisiPrev){
			$data[] = [
				'id_pangkalan' => $id_pangkalan,
				'kode_pangkalan' => $komisiPrev['kode_pangkalan'],
				'nominal' => (int)$komisiPrev['nominal'],
				'ids' => $komisiPrev['ids'],
				'jenis_deduct' => 1,
				'tgl' => 'Bln sblmnya',
				'jenis_deduct_text' => 'Komisi Penukaran',
				'selectable' => true
			];
		}
		if($komisiNow){
			$data[] = [
				'id_pangkalan' => $id_pangkalan,
				'kode_pangkalan' => $komisiNow['kode_pangkalan'],
				'nominal' => (int)$komisiNow['nominal'],
				'ids' => $komisiNow['ids'],
				'jenis_deduct' => 1,
				'tgl' => 'Bulan ini',
				'jenis_deduct_text' => 'Komisi Penukaran',
				'selectable' => false
			];
		}

		if($refund){
			$data[] = [
				'id_pangkalan' => $id_pangkalan,
				'kode_pangkalan' => $refund['kode_pangkalan'],
				'nominal' => (int)$refund['nominal'],
				'ids' => $refund['ids'],
				'jenis_deduct' => 2,
				'tgl' => '<= sekarang',
				'jenis_deduct_text' => 'Pengembalian Dana (Refund) Tiket',
				'selectable' => true
			];
		}

		return $data;
	}

	function getDataByManifest($id_manifest){
		$data = [
			'komisi' => null,
			'refund' => null
		];
		$komisi = $this->getSummaryDeduct(1, null, null, $id_manifest, false);
		$refund = $this->getSummaryDeduct(2, null, null, $id_manifest, false);

		if($komisi){
			$data['komisi'] = (int)$komisi['nominal'];
		}

		if($refund){
			$data['refund'] = (int)$refund['nominal'];
		}

		return $data;
	}

	function getSummaryDeduct($jenis, $id_pangkalan=null, $id_agen=null, $id_manifest=null, $non_batal_only=false, $mode='default'){
		$this->db->select("SUM(nominal) AS nominal, GROUP_CONCAT(id_deduct) as ids,kode AS kode_pangkalan");
		$this->db->from($this->_table);
		$this->db->join('#_pangkalan', '#_pangkalan.id_pangkalan = #_deduct.id_pangkalan');
		$this->db->where('jenis_deduct', $jenis, false);
		if($id_pangkalan){
			$this->db->where('#_deduct.id_pangkalan', $id_pangkalan, false);
		}
		if((int)$id_agen){
			$this->db->where('#_deduct.id_agen', $id_agen, false);
		}
		if($id_manifest){
			$this->db->where('#_deduct.id_manifest', $id_manifest, false);
		}else{
			$this->db->where('#_deduct.id_manifest IS NULL');
		}

		if($non_batal_only){
			$this->db->where('#_deduct.is_batal', 0, false);
		}

		if($mode == "now"){
			$this->db->where("MONTH(#_deduct.tanggal_transaksi)", date('m'));
			$this->db->where("YEAR(#_deduct.tanggal_transaksi)", date('Y'));
		}elseif($mode == "prev"){
			$this->db->where("#_deduct.tanggal_transaksi < '".date('Y-m')."-01'");
		}

		if((int)$id_agen){
			$this->db->group_by('#_deduct.id_pangkalan, #_deduct.id_agen');
		}else{
			$this->db->group_by('#_deduct.id_pangkalan');
		}

		return $this->db->get()->row_array();
	}

	function generateDeduct($id_pemesanan, $nominal_refund){
		$pemesanan = GetRowData('#_pemesanan', ['id_pemesanan'=>$id_pemesanan]);
		$deduct = GetRowData('#_deduct', ['id_pemesanan'=>$id_pemesanan, 'jenis_deduct'=>1, 'is_batal'=>0]);
		$this->db->trans_begin();
		$insertRefundTiket = [
			'id_pemesanan' => $id_pemesanan,
			'jenis_deduct' => 2,        // refund tiket
			'id_manifest' => null,
			'id_pangkalan' => GetIdPangkalan(),
			'id_agen' => GetUserId(),
			'nominal' => DefaultCurrencyDatabase($nominal_refund) * -1,
			'tanggal_transaksi' => GetDateNow(),
			'is_batal' => 0
		];
		$this->m_deduct->insert($insertRefundTiket);
		$insertRefundKomisi = [
			'id_pemesanan' => $id_pemesanan,
			'jenis_deduct' => 1,        // refund komisi penjualan/pemesanan
			'id_manifest' => null,
			'id_pangkalan' => $pemesanan['id_pangkalan_beli'],
			'id_agen' => $pemesanan['id_agen'],
			'nominal' => $pemesanan['komisi'],
			'tanggal_transaksi' => GetDateNow(),
			'is_batal' => 0
		];
		$this->m_deduct->insert($insertRefundKomisi);

		/*if(GetIdPangkalan() && GetIdPangkalan() != $pemesanan['id_pangkalan_beli']){
			$insertKomisi = [
				'id_pemesanan' => $id_pemesanan,
				'jenis_deduct' => 1,        // komisi penjualan/pemesanan
				'id_manifest' => null,
				'id_pangkalan' => $pemesanan['id_pangkalan_beli'],
				'nominal' => $pemesanan['komisi'],
				'tanggal_transaksi' => GetDateNow(),
				'is_batal' => 0
			];
			$this->m_deduct->insert($insertKomisi);
		}*/

		if($deduct){
			$insertKomisiPenukaran = [
				'id_pemesanan' => $id_pemesanan,
				'jenis_deduct' => 1,        // komisi penukaran
				'id_manifest' => null,
				'id_pangkalan' => $deduct['id_pangkalan'],
				'id_agen' => $deduct['id_agen'],
				'nominal' => $deduct['nominal'] * -1,
				'tanggal_transaksi' => GetDateNow(),
				'is_batal' => 0
			];
			$this->m_deduct->insert($insertKomisiPenukaran);
		}

		$status = $this->db->trans_status();
		if($status === TRUE){
			$this->db->trans_commit();
		}else{
			$this->db->trans_rollback();
		}

		return $status;
	}

	function insert($model){
		$model['created_date'] = GetDateNow();
		$model['created_by'] = GetUserId();
		return $this->db->insert($this->_table, $model);
	}

	function update($model, $cond){
		$model['updated_date'] = GetDateNow();
		$model['updated_by'] = GetUserId();
		return $this->db->update($this->_table, $model, $cond);
	}

	function select($cond){
		$this->db->from($this->_table);
		$this->db->where($cond);
		return $this->db->get()->result_array();
	}
}

?>