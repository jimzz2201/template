<?php

class M_menu extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }


    function GetMenu($id_akses=0){
        $this->db->from("#_form");
        $this->db->join("#_akses_group","#_form.id_form=#_akses_group.id_akses  and  id_group=".$id_akses,"left");
        $this->db->where(array("kode_parent"=>null,"is_url"=>1));
        $this->db->order_by("urut");
        $this->db->select("#_form.*");
        $limenu = $this->db->get()->result();
       
        foreach($limenu as $menu)
        {
            $this->db->from("#_form");
            $this->db->join("#_akses_group","#_form.id_form=#_akses_group.id_akses","left");
            $this->db->select("#_form.*");
            $this->db->where(array("kode_parent"=>$menu->id_form,"is_url"=>1,"id_group"=>$id_akses));
            $this->db->order_by("urut");
            $menu->submenu = $this->db->get()->result();
            
        }
        return $limenu;
    }
   
    function GetOneBank($id=0){
        $this->db->from("mlmtbl_bank");
        $this->db->where(array("bank_id"=>$id));

        return $this->db->get()->row();
    }
}

?>