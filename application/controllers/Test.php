<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		session_destroy();
	}

	public function afterPromo($kode_promo, $id_pangkalan, $id_berangkat)
	{
		$this->load->model('promo/m_promo');
		$promo = $this->m_promo->GetByCode($kode_promo);
		echo "ga ada promo<hr/>";
		echo $this->db->last_query()."<hr/>";
		$tglBerangkat = GetScalarData('#_berangkat', 'id_berangkat', $id_berangkat, 'tanggal');
		$hariBerangkat = date('N', strtotime($tglBerangkat));
		$berlaku = false;
		if ($promo) {
			if (GetTicketingLayout()) {
				$id_pangkalan = GetIdPangkalan();
			}
			if (CheckEmpty($promo->id_pangkalan)) {
				$berlaku = true;
				echo "pangkalan kosong<hr/>";
			} else {
				$pangkalan = explode(",", $promo->id_pangkalan);
				if (in_array($id_pangkalan, $pangkalan)) {
					$berlaku = true;
					echo "pangkalan masuk<hr/>";
				}
			}

			if ($berlaku) {
				if (CheckEmpty($promo->day)) {
					$berlaku = true;
					echo "hari kosong<hr/>";
				} else {
					$day = explode(",", $promo->day);
					if (in_array($hariBerangkat, $day)) {
						$berlaku = true;
						echo "hari masuk<hr/>";
					} else {
						$berlaku = false;
						echo "hari tidak masuk<hr/>";
					}
				}
			}
		}

		dumperror($berlaku);
	}

	public function regenerate_detail_setoran($id_manifest){
		$this->db->select('id_berangkat, tanggal_penjualan, kode as kode_order, sum(harga_tiket - komisi) as nominal');
		$this->db->from('#_pemesanan');
		$this->db->where('id_manifest', intval($id_manifest));
		$this->db->group_by('kode');
		$detail = $this->db->get()->result_array();
		$total1 = 0;
//		dumperror($detail);
		if($detail){
			$this->db->delete('#_manifest_detail', ['id_manifest' => intval($id_manifest)]);
			foreach(@$detail as $row){
				$row['id_manifest'] = intval($id_manifest);
				$total1 += $row['nominal'];
				$this->db->insert('#_manifest_detail', $row);
				$exist = CountData('#_manifest_detail', $row);
				echo $row['kode_order'].':'.$row['nominal'].':'.intval($exist).'<hr/>';
			}

			$update = [
				'total_penjualan' => $total1,
				'total' => $total1
			];
			$this->db->update('#_manifest', $update, ['id_manifest' => $id_manifest]);
		}
		echo $total1;
	}

	public function set_pangkalan($id_pangkalan){
		$this->session->set_userdata('id_pangkalan', $id_pangkalan);
	}

	public function genref($bytes=5){
		$rand = random_bytes($bytes);
		echo bin2hex($rand);
	}
	

}
