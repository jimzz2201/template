<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ACL {
	private $_ci = null;
	private $_rules;
	private $_group_id;

	public function __construct() {
		$this->_ci =& get_instance();
		$this->_ci->config->load('acl');
		$this->_rules = $this->_ci->config->item('acl');

		$this->_group_id = $this->_ci->session->userdata('group_id');
	}

	public function auth(){
		$perms = TRUE;
		$class = $this->_ci->router->fetch_class();

		if(array_key_exists($class, $this->_rules)){
			$perms = FALSE;
			if(isset($this->_rules[$class]['view'][$this->_group_id])){
				$perms = $this->_rules[$class]['view'][$this->_group_id];
			}
		}

		if($perms == FALSE){
			$resp = array(
				'message' => "Anda tidak memiliki hak untuk mengakses halaman ini!",
				'heading' => 'An Error Was Encountered',
				'error_code' => 403,
			);
//			show_error($resp['message'], 403, $resp['heading']);
			set_status_header($resp['error_code']);
			ob_start();
			LoadTemplate($resp, 'errors/html/error_page');
			$buffer = ob_get_contents();
			ob_end_clean();
			echo $buffer;
			exit(1);
		}
	}
}
