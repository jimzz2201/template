<?php

define("STR_PAD_BIG", 32);
define("STR_PAD_SMALL", 62);
define("STR_PAD_DETAIL", 20);

function GetAge($bithdayDate)
{
    $date = new DateTime($bithdayDate);
    $now = new DateTime();
    $interval = $now->diff($date);
    return $interval->y.' Tahun '.$interval->m." Bulan ";
}

function ReturnApi($arrayreturn)
{
    $CI = get_instance();
    if(CheckKey($arrayreturn, "msg"))
    {
        $arrayreturn['msg']=str_replace("<br/>", "\n", $arrayreturn['msg']);
    }
    
    $CI->response($arrayreturn, REST_Controller::HTTP_OK);
}


function SetCookieSetting($name,$value)
{
    setcookie($name, $value, time() + (86400 * 30), "/"); 
}

function ClearCookiePrefix($prefix,$jenis_pencarian)
{
    $arr=["clear","start_date","end_date","jenis_keyword","jenis_pencarian"];
    
    foreach ($arr as $key) {
            setcookie($key . $prefix, '', time() - 10000, '/');
    }
    if(!CheckEmpty($jenis_pencarian))
    {
        SetCookieSetting("jenis_pencarian".$prefix,$jenis_pencarian);
    }
   
}


function UpdateOrJoin($dbparam,$jointable,$condition,$type,$id_report)
{
    $dbparam->join($jointable,$condition,$type);
}


function GetCookieSetting($name,$default="")
{
    $returndata=get_cookie($name);
    if(CheckEmpty($returndata)&&!CheckEmpty($default))
    {
        $returndata=$default;
    }
    return $returndata;
}
function InfoExcel($tipe = "phpexcel", $merge = 1, $laporan = "") {
    $info = [];
    $infoheader = [];
    $CI = get_instance();
    $str = $CI->input->post("search");
    parse_str($str, $search);
    if (CheckEmpty($search)) {
        $search = $CI->input->get();
    }
    if (@$search['tipe_tanggal'] == 1) {
        $colTanggal = "Tanggal";
    } else {
        $colTanggal = "Tanggal";
    }

    if (isset($search['end_date']) && $search['end_date'] != $search['start_date']) {
        $infoheader[] = $search['start_date'] . " - " . $search['end_date'];
    } else {
        $infoheader[] = @$search['start_date'];
    }



   
    if (isset($search['id_supplier']) && !empty($search['id_supplier'])) {
        $info[] = "Supplier : " . GetScalarData("#_supplier", "id_supplier", $search['id_supplier'], 'nama_supplier');
    }
    if (isset($search['id_customer']) && !empty($search['id_customer'])) {
        $info[] = "Customer : " . GetScalarData("#_customer", "id_customer", $search['id_customer'], 'nama_customer');
    }
   
    if (isset($search['id_cabang']) && !empty($search['id_cabang'])) {
        $info[] = "Cabang : " . GetScalarData("#_cabang", "id_cabang", $search['id_cabang'], 'nama_cabang');
    }

    if (isset($search['id_user']) && !empty($search['id_user'])) {
        $info[] = "Operator: " . GetScalarData("#_user", "id_user", $search['id_user'], 'nama_user');
    }
    if (isset($search['status']) && !empty($search['status'])) {
        $info[] = "Status: " . GetStatus($search['status']);
    }
    

    if ($tipe == "phpexcel") {
        return [[], ['text' => implode(" | ", $info), "subheader" => [[], $colTanggal, implode(" | ", $infoheader)], 'merge' => $merge - 1]];
    } else {
        return ['', 'Keyword', implode(" | ", $info), [[], 'Pencarian ' . $colTanggal, implode(" | ", $infoheader)]];
    }
}
function GetPoolCustomer()
{
    return 3;
}
function GetPoolSupplier()
{
    return 2;
}
function GetDefaultPool()
{
    return 1;
}
function callAPI($method, $url, $data){
   $curl = curl_init();

   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }

   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'APIKEY: 111111111111111111111',
      'Content-Type: application/json',
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}

function MaxSize($keyword,$number)
{
    if(strlen($keyword)>$number)
    {
        return substr($keyword, 0,$number)."...";
    }
    else
    {
        return $keyword;
    }
}

function SetPrint($id,$dokumen,$jenis)
{
    $CI = get_instance();
    $CI->session->set_userdata(array( 'id_'.$jenis=>$id,'dokumen_'.$jenis=>$dokumen));
}
function InsertApiHistory($json,$message,$error_text,$module)
{
    $CI = get_instance();
    $jsonhistory=array();
    $jsonhistory['json_text']=$json;
    $jsonhistory['message']=$message;
    $jsonhistory['error_text']=$error_text;
    $jsonhistory['module']=$module;
    $jsonhistory['created_date']= GetDateNow();
    $jsonhistory['created_by']= GetUserId();
    $CI->db->insert("#_api_log",$jsonhistory);
}

function MergeUpdate($update, $userid = null) {
    $updatetambahan = [];
    $updatetambahan['updated_date'] = GetDateNow();
    if (CheckEmpty($userid)) {
        $userid = GetUserId();
    }
    $updatetambahan['updated_by'] = $userid;
    return array_merge($update, $updatetambahan);
}

function MergeCreated($created, $userid = null) {
    $createdtambahan = [];
    $createdtambahan['created_date'] = GetDateNow();
    if (CheckEmpty($userid)) {
        $userid = GetUserId();
    }
    $createdtambahan['created_by'] = $userid;
    return array_merge($created, $createdtambahan);
}
function DefaultEmptyDropdown($data,$type="json",$text="")
{
  
    
    if(CheckEmpty($data))
    {
        $data=array();
    }
    if($type=="obj"||$type=="")
    {
        $data= array("0"=> CheckEmpty($text)? "":"Pilih ".$text)+$data;
    }
    else
    {   
        $inserted=array(array("id"=> "0","text"=>CheckEmpty($text)? "":"Pilih ".$text));
        $data=array_merge($inserted,$data);
    }
    
   
    return $data;
}
function round_out($value, $places = 0) {
    if ($places < 0) {
        $places = 0;
    }
    $mult = pow(10, $places);
    return ($value >= 0 ? ceil($value * $mult) : floor($value * $mult)) / $mult;
}

function GetMessageLogin() {

    return 'Your account is expired , you can retry for login again by click <i><b><a href="' . base_url() . '" this</b></i> form above<br/>';
}

function GetLastQueryDataTable(){
    $CI = get_instance();
    return  $CI->session->userdata('querydatatable');
}

function SetLastQueryDataTable($query) {
    $CI = get_instance();
    $CI->session->set_userdata("querydatatable", $query);
}



function dumperror($obj) {
    echo "<pre>";
    var_dump($obj);
    echo "</pre>";
}

function AutoIncrement($table, $kode, $column, $length = 6) {
    $CI = get_instance();
    $query = "select ifnull(max(right(" . $column . "," . $length . ")),0) as 'sum' from " . $table . " where " . $column . " like '%" . $kode . "%' and " . $column . " is not null";

    $lastnumber = intval($CI->db->query($query)->row()->sum);
    return $kode . substr('000000000' . ($lastnumber + 1), -$length);
}

function CheckKey($arr, $key) {
    if (array_key_exists($key, $arr)) {
        if (is_array($arr[$key])) {
            if(count($arr[$key])>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        } else {
            if (trim($arr[$key]) != '') {
                return true;
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
}

function array_msort($array, $cols) {
    $colarr = array();
    foreach ($cols as $col => $order) {
        $colarr[$col] = array();
        foreach ($array as $k => $row) {
            $colarr[$col]['_' . $k] = strtolower($row[$col]);
        }
    }
    $eval = 'array_multisort(';
    foreach ($cols as $col => $order) {
        $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
    }
    $eval = substr($eval, 0, -1) . ');';
    eval($eval);
    $ret = array();
    foreach ($colarr as $col => $arr) {
        foreach ($arr as $k => $v) {
            $k = substr($k, 1);
            if (!isset($ret[$k]))
                $ret[$k] = $array[$k];
            $ret[$k][$col] = $array[$k][$col];
        }
    }
    return $ret;
}

function GetMessageStatus() {
    $CI = get_instance();

    $status = $CI->session->userdata('st');


    if ($status != null && $status != '') {

        return $status;
    } else {

        return 5;
    }
}

Function DefaultDatePicker($val) {
    if (strtotime($val)) {
        return date('d M Y', strtotime($val));
    } else {
        return "";
    }
}

Function DefaultTimePicker($val = "") {
    if (strtotime($val)) {
        return date('H:i', strtotime($val));
    } else {
        return date("H:i");
    }
}

function ClearMessage() {
    $CI = get_instance();
    $CI->session->unset_userdata('message');
    $CI->session->unset_userdata('messagestatus');
    $CI->session->unset_userdata('title');
}

function OpenModalJavaScript($url, $width = 480, $title = "PopUp") {
    echo "<script language='javascript'>var left = (screen.width/2)-(" . $width . "/2);" . "window.open('" . $url . "','" . $title . "', 'width=" . $width . ", height=600, menubar=yes, scrollbars=yes, resizable=yes,left='+left)" . "</script>";
}

function GetMessage() {
    $CI = get_instance();
    if ($CI->session->userdata('msg') != null && $CI->session->userdata('msg') != '') {
        return $CI->session->userdata('msg');
    } else {
        return '';
    }
}

function GetTitle() {
    $CI = get_instance();
    if ($CI->session->userdata('title') != null && $CI->session->userdata('title') != '') {
        return $CI->session->userdata('title');
    } else {
        return GetMessageStatus() == 1 ? "Konfirmasi" : "Error";
    }
}

function CekSessionUser() {
    $CI = get_instance();
    if ($CI->session->userdata('username') != null && $CI->session->userdata('username') != '') {
        return true;
    } else {
        redirect(base_url() . '?url=' . "http://" . $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI]);
    }
}

function CheckAdmin($akses = '') {
    $CI = get_instance();
    $CI->load->model("user/m_user");
    $user = $CI->m_user->GetOneUser(GetUsername());
    if ($user != null) {
        if ($user->jenis_user == 'S') {
            return true;
        } else {
            redirect(base_url() . 'index.php/user/login');
            return false;
        }
    } else {
        CheckSession();
        return false;
    }
}

function CekModule($kodemenu, $isredirect = true,$userid=null) {
    if($kodemenu=="")
    {
        return true;
    }
    else
    {
        if(CheckEmpty($userid))
        {
            $userid = GetUserId();
        }
        
        $CI = get_instance();
        $CI->load->model("akses/m_akses");
        if (!$CI->m_akses->CekAkses($userid, $kodemenu)) {
            if ($isredirect) {
                SetMessageSession(2, "Anda tidak memiliki akses untuk menuju link tersebut");
                redirect(base_url() . 'index.php/main/message');
            }
            return false;
        } else {
            return true;
        }
    }
}

function LoadTemplate($model, $template, $javascript = array(), $css = array(), $link = array()) {
    date_default_timezone_set('Asia/Jakarta');
    IF (CekSessionUser()) {
        $CI = get_instance();
        $userid = GetUserId();
        $CI->load->model("user/m_user");
        $CI->load->model("m_menu");
        $user = $CI->m_user->GetOneUser($userid, 'id_user');

        $model['listmenu'] = $CI->m_menu->GetMenu($user->id_group);
        $model['nama_user_login'] = $user->nama_user;
        $CI->load->model("akses/m_akses");
        $group = $CI->m_akses->GetOneAkses($user->id_group);

        $fromcss = array(
            'assets/css/AdminLTE.min.css',
            'assets/css/jquery-ui.css',
            'assets/css/remodal.css',
            'assets/css/skins/_all-skins.min.css',
            'assets/plugins/iCheck/flat/blue.css',
            'assets/plugins/morris/morris.css',
            'assets/plugins/datepicker/datepicker3.css',
            'assets/plugins/datatables/dataTables.bootstrap.css',
            'assets/plugins/daterangepicker/daterangepicker.css',
            'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
            'assets/css/sweetalertnew.css');

        $fromjavascript = array(
//            'assets/js/jquery-ui.min.js',
//            'assets/bootstrap/js/bootstrap.min.js',
            'assets/bootstrap/js/bootstrap-alert-box.js',
            'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
            'assets/js/app.min.js',
            'assets/js/remodal.js',
            'assets/js/moment.min.js',
            'assets/js/alasql.js',
            'assets/plugins/datepicker/bootstrap-datepicker.js',
            'assets/js/sweetalertnew.js');
         $fromcss[] = "assets/plugins/select2/select2.css";
        
        // DEFAULT TEMPLATE
        $view = 'template/template_atas';

        $fromjavascript = array(
                               'assets/js/jquery-ui.min.js',
            //                    'assets/bootstrap/js/bootstrap.min.js',
        'assets/bootstrap/js/bootstrap-alert-box.js',
        'assets/js/app.min.js',
        'assets/js/remodal.js',
        'assets/js/alasql.js',
          
        'assets/js/moment.min.js',
        'assets/js/sweetalertnew.js');
        $fromjavascript[] = "assets/plugins/select2/select2.js";
    
        $css = array_merge($fromcss, $css);
        $model['css'] = $css;

        $model['link'] = $link;

        $CI->load->view($view, $model);
        $CI->load->view($template, $model);
        $bottom = array();

        $javascript = array_merge($fromjavascript, $javascript);

        $bottom['javascript'] = $javascript;
        $bottom['messagestatus'] = GetMessageStatus();
        $bottom['openmenu'] = @$model['openmenu'];
        $bottom['message'] = GetMessage();

        $CI->load->view('template/template_bawah', $bottom);
        SetMessageSession(5, '');
    }
}


function LoadTemplateDashboardTicketing($model, $template) {
    date_default_timezone_set('Asia/Jakarta');
    IF (CekSessionUser()) {
        $CI = get_instance();
        $userid = GetUserId();
        $CI->load->model("user/m_user");
        $CI->load->model("m_menu");
        $user = $CI->m_user->GetOneUser($userid, 'id_user');

        $model['listmenu'] = $CI->m_menu->GetMenu($user->id_group);
        $model['nama_user_login'] = $user->nama_user;
        $CI->load->model("akses/m_akses");
        $group = $CI->m_akses->GetOneAkses($user->id_group);

        $view = 'template/template_atas_ticketing';
        $css = array(
            'assets/css/AdminLTE.min.css',
            'assets/css/jquery-ui.css',
            'assets/css/skins/_all-skins.min.css',
        );
        $js = array(
//                    'assets/js/jquery-ui.min.js',
//                    'assets/bootstrap/js/bootstrap.min.js',
            'assets/bootstrap/js/bootstrap-alert-box.js',
            'assets/js/app.min.js',
        );

        $model['css'] = $css;

        $CI->load->view($view, $model);
        $CI->load->view($template, $model);
        $bottom = array();

        $bottom['javascript'] = $js;
        $bottom['messagestatus'] = GetMessageStatus();
        $bottom['openmenu'] = @$model['openmenu'];
        $bottom['message'] = GetMessage();

        $CI->load->view('template/template_bawah', $bottom);
        SetMessageSession(5, '');
    }
}

function LoadVendingTemplate($model, $template, $javascript = array(), $css = array()) {

    IF (CekSessionUser()) {
        $CI = get_instance();
        $userid = GetUserId();
        $CI->load->model("user/m_user");
        $CI->load->model("m_menu");
        $user = $CI->m_user->GetOneUser($userid, 'id_user');

        $fromcss = array(
            'assets/css/AdminLTE.min.css',
            'assets/css/jquery-ui.css',
            'assets/css/remodal.css',
            'assets/css/skins/_all-skins.min.css',
            'assets/plugins/iCheck/flat/blue.css',
            'assets/plugins/morris/morris.css',
            'assets/plugins/datepicker/datepicker3.css',
            'assets/plugins/datatables/dataTables.bootstrap.css',
            'assets/plugins/daterangepicker/daterangepicker.css',
            'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
            'assets/css/sweetalertnew.css');
        $css = array_merge($fromcss, $css);
        $model['css'] = $css;
        $view = 'template/template_atas_vending';
        $CI->load->view($view, $model);
        $CI->load->view($template, $model);
        $bottom = array();
        $fromjavascript = array(
            'assets/js/jquery-ui.min.js',
//            'assets/bootstrap/js/bootstrap.min.js',
            'assets/bootstrap/js/bootstrap-alert-box.js',
            'assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
            'assets/js/app.min.js',
            'assets/js/remodal.js',
            'assets/js/moment.min.js',
            'assets/js/sweetalertnew.js');
        $javascript = array_merge($fromjavascript, $javascript);

        $bottom['javascript'] = $javascript;
        $bottom['messagestatus'] = GetMessageStatus();
        $bottom['openmenu'] = @$model['openmenu'];
        $bottom['message'] = GetMessage();

        $CI->load->view('template/template_bawah_vending', $bottom);
        SetMessageSession(5, '');
    }
}

function CheckEmpty($val) {
    if ($val == '' || $val == '0' || $val == false || $val == "false" || $val == null || $val=="null"||$val=="0000-00-00"||$val=="undefined") {
        return true;
    }
    return false;
}

function GetUserId() {
    $CI = get_instance();
    $id = $CI->session->userdata('member_id');
    if ($id != null)
        return $id;
    else
        return 0;
}


function SetCabangAkses($list_cabang) {
    $CI = get_instance();
    $CI->session->set_userdata("listcabang", $list_cabang);
}

function GetCabangAkses() {
    $CI = get_instance();
    $list_cabang = $CI->session->userdata('listcabang');
    if ($list_cabang != null)
        return (array)$list_cabang;
    else
        return [];
}

function GetGroupId() {
    $CI = get_instance();
    $id = $CI->session->userdata('group_id');
    if ($id != null)
        return $id;
    else
        return 0;
}

function GetMasterUserId() {
    $CI = get_instance();
    $id = $CI->session->userdata('master_id');
    if ($id != null)
        return $id;
    else
        return 0;
}
function GetIdCabang() {
    $CI = get_instance();
    $id = $CI->session->userdata('id_cabang');
    if ($id != null)
        return $id;
    else
        return 0;
}

function SetCabangId($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("id_cabang", $Id);
}


function SetUserId($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("member_id", $Id);
}


function SetGroupId($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("group_id", $Id);
}

function SetShift($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("shift_id", $Id);
}

function GetShift() {
    $CI = get_instance();
    $id = $CI->session->userdata('shift_id');
    if ($id != null)
        return $id;
    else
        return 0;
}

function SetShiftName($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("shift_name", $Id);
}

function GetShiftName() {
    $CI = get_instance();
    $name = $CI->session->userdata('shift_name');
    if ($name != null)
        return $name;
    else
        return '';
}

function SetMasterUserId($Id) {
    $CI = get_instance();
    $CI->session->set_userdata("master_id", $Id);
}

function SetUsername($username) {
    $CI = get_instance();
    $CI->session->set_userdata("username", $username);
}

function SetPangkalan($id_pangkalan) {
    $CI = get_instance();
    $CI->session->set_userdata("id_pangkalan", $id_pangkalan);
}

function SetTicketingLayout($is_ticketing) {
    $CI = get_instance();
    $CI->session->set_userdata("is_ticketing", $is_ticketing);
}

function GetUsername() {
    $CI = get_instance();
    $username = $CI->session->userdata('username');
    if ($username != null)
        return $username;
    else
        return '';
}

function SetIdSpj($id_spj) {
    $CI = get_instance();
    $CI->session->set_userdata("id_spj", $id_spj);
}

function UnsetIdSpj() {
    $CI = get_instance();
    $CI->session->unset_userdata("id_spj");
}

function GetIdSpj() {
    $CI = get_instance();
    $id_spj = $CI->session->userdata('id_spj');
    if ($id_spj != null)
        return $id_spj;
    else
        return '0';
}

function SetVendingSession() {
    $CI = get_instance();
    $CI->session->set_userdata("vending", 1);
}

function UnsetVendingSession() {
    $CI = get_instance();
    $CI->session->set_userdata("vending", 0);
}

function GetVendingSession() {
    $CI = get_instance();
    return $CI->session->userdata('vending');
}

function GetIdPangkalan() {
    $CI = get_instance();
    $id_pangkalan = $CI->session->userdata('id_pangkalan');
    if ($id_pangkalan != null)
        return $id_pangkalan;
    else
        return NULL;
}

function GetTicketingLayout() {
    $CI = get_instance();
    return $CI->session->userdata('is_ticketing');
}

function RandomPassword($numlength = 8) {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $numlength; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function SetMessageSession($st, $message) {
    $CI = get_instance();

    $CI->session->set_userdata(array("st" => $st, 'msg' => $message));
}

function ClearSessionData() {
    $CI = get_instance();
    $CI->session->unset_userdata("st");
    $CI->session->unset_userdata("msg");
}

function GetDateNow() {
    return date("Y-m-d H:i:s");
}

function AddDays($date,$increment) {
    $date = strtotime($date);
    $date = strtotime($increment, $date);
    return date('Y-m-d H:i:s', $date);
}

function DefaultCurrency($number) {
    if (CheckEmpty($number)) {
        $number = '0';
    }
    return number_format(DefaultCurrencyDatabase($number), 0, '', ',');
}

function DefaultCurrencyAkuntansi($number) {
    if (CheckEmpty($number)) {
        return '-';
    }
    if ($number < 0)
        return "(" . DefaultCurrency(abs($number)) . ")";
    else
        return number_format(DefaultCurrencyDatabase($number), 0, '', ',');
}

function GetValueRadioButton($rad, $arrayname) {
    $yangdibandingin = $rad;
    if ($arrayname !== "") {
        $yangdibandingin = isset($rad[$arrayname]) ? $rad[$arrayname] : null;
    }
    if (isset($yangdibandingin)) {
        if ($yangdibandingin === "on" || $yangdibandingin===true || $yangdibandingin==="true" ||$yangdibandingin==="1"||$yangdibandingin===1) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function GetValueCheckbox($arr, $el) {
    $result = false;
    if(array_key_exists($el, $arr)){
        if($arr[$el] === true || $arr[$el] == "true" || $arr[$el] == 1){
            $result = true;
        }
    }
    return $result;
}

function ForeignKeyFromDb($val, $default = null) {
    if ($val != null && $val != '' && $val != '0'&& $val !="null") {
        return $val;
    } else {
        return $default;
    }
}

function CheckIfIsset($arr, $el, $default=null) {
    if(array_key_exists($el, $arr)){
        if($arr[$el] != null && $arr[$el] != "" && $arr[$el] != '0' && $arr[$el] != "null"){
            $default = $arr[$el];
        }
    }
    return $default;
}

function CheckArray($arr, $key) {
    if (array_key_exists($key, $arr)) {
        if (isset($arr[$key])) {
            if ($arr[$key] != null && $arr[$key] != false && $arr[$key] != '' && is_array($arr[$key])) {
                return $arr[$key];
            } else
                return array();
        }
        else {
            return array();
        }
    } else {
        return array();
    }
}

function DefaultCurrencyDatabase($number = NULL) {
    if ($number === NULL||$number=="null"||$number=="")
        return 0;

    return str_replace(',', '', $number);
}

function Multi_array_search($array, $field, $value) {
    $results = array();

    if (is_array($array)) {
        foreach ($array as $arraysatuan) {
            if ($arraysatuan[$field] == $value) {  //chek the filed against teh value, you can do addional check s(i.e. if field has been set)
                $results[] = $arraysatuan;
            }
        }
    }

    return $results;
}

function SearchArray($array, $search) {

    // Create the result array
    $result = array();

    // Iterate over each array element
    foreach ($array as $key => $value) {

        // Iterate over each search condition
        foreach ($search as $k => $v) {

            // If the array element does not meet the search condition then continue to the next element
            if (!isset($value[$k]) || $value[$k] != $v) {
                continue 2;
            }
        }

        // Add the array element's key to the result array
        $result[] = $key;
    }

    // Return the result array
    return $result;
}

function Multi_array_searchindex($array, $field, $value) {
    $index = -1;

    if (is_array($array)) {
        foreach ($array as $key => $arraysatuan) {
            if ($arraysatuan[$field] == $value) {  //chek the filed against teh value, you can do addional check s(i.e. if field has been set)
                $index = $key;
                return $index;
            }
        }
    }

    return $index;
}

function Datatable($arraytemp) {

    $row = 0;
    foreach ($arraytemp as $arraysatuan) {
        $arraysatuan->DT_RowId = "row" . $row;
        $row++;
    }
    return $arraytemp;
}

function GetErrorDb($tempstring = "") {
    $CI = get_instance();
    $message = "";
    if ($CI->db->error()['message'] != "") {
        if ($CI->db->db_debug == true) {
            $message = preg_replace('/(<\/?p>)+/', ' ', $CI->db->error()['message']);
        } else {
            if ($tempstring != "") {
                $message = $tempstring;
            } else {
                $message = "Terjadi Sesuatu yang salah dengan system";
            }
        }
    }
    if ($message != "") {
        throw new Exception("Database error occured with message : {$message}");
    }
}

function SetValue($val, $return = "0") {
    if ($val == null) {
        return $return;
    } else {
        return $val;
    }
}

function DefaultTanggal($val) {
    if (!$val) {
        return '';
    }
    return date('d M Y', strtotime($val));
}

function GetDateNowWithTime() {
    return date("d M Y h:i:s");
}

function DefaultTanggalDempet($val) {
    return date('d/m/y', strtotime($val));
}

function DefaultTanggalSemestinya($val) {
    return date('m/d/Y', strtotime($val));
}

function DefaultTanggalDatabase($val) {
    $date= date('Y-m-d', strtotime($val));
    if($date=="1970-01-01")
    {
        return null;
    }
    else {
        return $date;
    }
}

function DefaultTanggalWithday($val) {
    return date('l , d-M-Y', strtotime($val));
}

function GetConfig() {
    $ci = get_instance();
    return $ci->config;
}

function round_up($value, $places = 0) {
    if ($places < 0) {
        $places = 0;
    }
    $mult = pow(10, $places);
    return ceil($value * $mult) / $mult;
}

function CheckSession() {
    if (GetUserId() == 0) {
        redirect(base_url() . 'index.php/user/login');
    }
}

// tablefrom , id , col ,where ,type
// order , where_in , join ,select , or_where
// limit
function GetTableData($table, $id, $col, $where = array(), $type = 'json', $order = array(), $where_in = array(), $join = array(),$select="",$or_where=[],$limit=1000) {
    $CI = get_instance();
    $list = array();
    if(!CheckEmpty($select))
    {
         $CI->db->select($select);
    }
    if(count($or_where)>0)
    {
        $CI->db->group_start();
    }
    if (count($where)) {
        $CI->db->where($where);
    }
    if (count($where_in) && !CheckEmpty($where_in[1])) {
        $CI->db->where_in($where_in[0], $where_in[1]);
    }
    if(count($or_where)>0)
    {
        $CI->db->group_end();
    }
    if(count($or_where)>0)
    {
        $CI->db->or_group_start();
        $CI->db->where($or_where);
        $CI->db->group_end();
    }
    
    if ($order) {
        $CI->db->order_by($order[0], $order[1]);
    }
    if ($join) {
        foreach ($join as $row) {
            $CI->db->join($row["table"], $row["condition"],"left");
        }
    }

    $data = $CI->db->get($table, $limit)->result_array();
    if ($data) {
        foreach ($data as $row) {
            if (is_array($col)) {
                $text = array();
                foreach ($col as $item) {
                    array_push($text, $row[$item]);
                }
                $text = implode(" - ", $text);
            } else {
                $text = $row[$col];
            }
            if ($type == "json") {
                $list[] = array('id' => (int) $row[$id],
                    'text' => $text,
                );
            } elseif ($type == "obj") {
                $list[$row[$id]] = $text;
            }
        }
    }

    return $list;
}

function GetRowData($table, $where = array(), $where_in = array(), $group_by = array(), $order = array(), $time = false) {
    $CI = get_instance();

    if ($where) {
        $CI->db->where($where);
    }
    if (count($where_in)) {
        $CI->db->where_in($where_in[0], $where_in[1]);
    }
    $CI->db->select($table . ".*");
    if ($group_by) {
        $CI->db->group_by($group_by);
    }
    if ($order) {
        $CI->db->order_by($order[0], $order[1]);
    }

    $data = $CI->db->get($table, 1)->row_array();
    if ($time == false) {
        unset($data['updated_by'], $data['updated_date'], $data['created_by'], $data['created_date']);
    }
    return $data;
}

function GetRowsData($table, $where = array(), $where_in = array(), $group_by = array(), $order = null, $type="array") {
    $CI = get_instance();

    if ((is_array($where) && count($where)) || !CheckEmpty($where)) {
        $CI->db->where($where);
    }
    if (count($where_in)) {
        $CI->db->where_in($where_in[0], $where_in[1]);
    }
    $CI->db->select($table . ".*");
    if ($group_by) {
        $CI->db->group_by($group_by);
    }
    if ($order) {
        if(is_array($order)){
            $CI->db->order_by($order[0], $order[1]);
        }else{
            $CI->db->order_by($order);
        }
    }

	if($type == "array"){
		$data = $CI->db->get($table)->result_array();
	}else{
		$data = $CI->db->get($table)->result();
	}

    return $data;
}

function GetColsData($table, $cols, $where = array(), $where_in = array(), $group_by = array(), $order = array(), $type="array") {
    $CI = get_instance();

    if (count($where)) {
        $CI->db->where($where);
    }
    if (count($where_in)) {
        $CI->db->where_in($where_in[0], $where_in[1]);
    }
    $CI->db->select($table . "." . $cols);
    if ($group_by) {
        $CI->db->group_by($group_by);
    }
    if ($order) {
        $CI->db->order_by($order[0], $order[1]);
    }

	$result = $CI->db->get($table)->result_array();
	$data = [];
	foreach($result as $row){
		$data[] = $row[$cols];
	}
    if($type == "string"){
	    $data = implode(",", $data);
    }

    return $data;
}

function GetScalarData($table, $key, $match, $col, $alias = null) {
    $result = '';
    $CI = get_instance();
    if ($alias) {
        $CI->db->select($col . ' AS ' . $alias);
    } else {
        $CI->db->select($col);
        $alias = $col;
    }
    $CI->db->where($key, $match);
    $data = $CI->db->get($table)->row_array();
    if ($data) {
        $result = $data[$alias];
    }

    return $result;
}

function GetScalarDataByCond($table, $where = array(), $col, $alias = null) {
    $result = '';
    $CI = get_instance();
    if ($alias) {
        $CI->db->select($col . ' AS ' . $alias);
    } else {
        $CI->db->select($col);
        $alias = $col;
    }
    if ((is_array($where) && count($where)) || !empty($where)) {
        $CI->db->where($where);
    } else {
        $CI->db->where('1 <> 1');
    }

    $data = $CI->db->get($table)->row_array();
    if ($data) {
        $result = $data[$alias];
    }

    return $result;
}

function CountData($table, $where = null, $join = array(), $group_by = null, $where_in_col = null, $where_in = array()) {
    $result = 0;
    $CI = get_instance();
    $CI->db->select("COUNT(*) AS jml");
    $CI->db->from($table);
    if (count($join)) {
        foreach ($join as $item) {
            $CI->db->join($item[0], $item[1]);
        }
    }

    if ($where)
        $CI->db->where($where);

    if ($group_by)
        $CI->db->group_by($group_by);

    if ($where_in && $where_in_col)
        $CI->db->where_in($where_in_col, $where_in);

    $data = $CI->db->get()->row_array();
    if ($data) {
        $result = (int) $data['jml'];
    }

    return $result;
}

function ColumnExistCheck($table, $col_name){
    $CI = get_instance();
    $fieldIsBatal = $CI->db->query("SHOW COLUMNS FROM ".$table." LIKE '".$col_name."'")->row_array();

    return (bool)$fieldIsBatal;
}

function GetJenisTiket($id = null) {
    $jenis = array(
        1 => "PNP",
        2 => "BDB",
        3 => "TNI"
    );

    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetJenisPembayaran($id = null) {
    $jenis = array(
        'TRANSFER' => 'Transfer',
        'CASH' => 'Cash',
    );

    if ($id && array_key_exists($id, $jenis)) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetJenisPembayaran2($id = null) {
    $jenis = array(
        'TRANSFER' => 'Transfer',
        'CASH' => 'Cash',
//        'CASH & TRANSFER' => 'Cash & Transfer',
    );

    if ($id && array_key_exists($id, $jenis)) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetStatusKeberangkatan($id = null) {
    $status = array(
        0 => "Planned",
        1 => "Done",
        2 => "Canceled"
    );

    if ($id) {
        $status = $status[$id];
    }

    return $status;
}

function GetMonth($id = null) {
    $month = array();
    $month[1] = "Januari";
    $month[2] = "Februari";
    $month[3] = "Maret";
    $month[4] = "April";
    $month[5] = "Mei";
    $month[6] = "Juni";
    $month[7] = "Juli";
    $month[8] = "Agustus";
    $month[9] = "September";
    $month[10] = "Oktober";
    $month[11] = "November";
    $month[12] = "Desember";

    if ($id) {
        $month = $month[$id];
    }

    return $month;
}

function GetMonthShort($id = null) {
    $month = array();
    $month[1] = "Jan";
    $month[2] = "Feb";
    $month[3] = "Mar";
    $month[4] = "Apr";
    $month[5] = "Mei";
    $month[6] = "Jun";
    $month[7] = "Jul";
    $month[8] = "Agu";
    $month[9] = "Sept";
    $month[10] = "Okt";
    $month[11] = "Nov";
    $month[12] = "Des";

    if ($id) {
        $month = $month[$id];
    }

    return $month;
}

function GetDay($id = null) {
    $day = array();
    $day[0] = "Minggu";
    $day[1] = "Senin";
    $day[2] = "Selasa";
    $day[3] = "Rabu";
    $day[4] = "Kamis";
    $day[5] = "Jumat";
    $day[6] = "Sabtu";

    if ($id != null) {
        $day = $day[$id];
    }

    return $day;
}

function GetJenisBO($id = null) {
    $jenis = array(
        'bopb' => "BOPB",
        'bopt' => "BOPT",
        'titipan' => "Titipan"
    );

    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetJenisBiaya($id = null,$isfull=false) {
//    $jenis = array(
//        '1' => "Barat",
//        '2' => "Timur",
//        '3' => "Tambahan Barat",
//        '4' => "Tambahan Timur",
//        '5' => "Lain-lain 1",
//        '6' => "Lain-lain 2",
//        '7' => "BOPB",
//        '8' => "BOPT",
//        '9' => "Titipan",
//    );
    if(!$isfull)
    $jenis = GetTableData('#_jenis_biaya', 'id_jenis_biaya', 'nama_jenis_biaya', array('status' => 1, 'id_jenis_biaya !=' => 9), 'obj',array("nama_jenis_biaya","asc"));
	else
	$jenis = GetTableData('#_jenis_biaya', 'id_jenis_biaya', 'nama_jenis_biaya', array( 'id_jenis_biaya !=' => 9), 'obj',array("nama_jenis_biaya","asc"));
		
    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetYaTidak($id = null) {
    $opsi = array(
        1 => 'Ya',
        0 => 'Tidak',
    );

    if ($id !== null) {
        $opsi = $opsi[$id];
    }

    return $opsi;
}


function GetLevel($id = null) {
    $opsi = array(
        1 => '1',
        2 => '2',
        3 => '3',
    );

    if ($id !== null) {
        $opsi = $opsi[$id];
    }

    return $opsi;
}

function GetTipeKomisi($id = null) {
    $opsi = array(
        0 => 'Nominal',
        1 => 'Persen',
    );

    if ($id) {
        $opsi = $opsi[$id];
    }

    return $opsi;
}

function GetJenisMitra($id = null) {
    $opsi = array(
        1 => 'Agen',
        2 => 'Agen Online',
        3 => 'Pihak Ketiga',
    );

    if ($id) {
        $opsi = $opsi[$id];
    }

    return $opsi;
}

function GetPerms($class, $action) {
    $CI = & get_instance();
    if (null === $CI->config->item('acl')) {
        $CI->config->load('acl');
    }
    $rules = $CI->config->item('acl');
    $group_id = $CI->session->userdata('group_id');
    $perms = TRUE;

    if (array_key_exists($class, $rules)) {
        $perms = FALSE;
        if (isset($rules[$class][$action][$group_id])) {
            $perms = $rules[$class][$action][$group_id];
        }
    }

    return $perms;
}

function checkPerms($class, $action, $redirect = null) {
    return TRUE;
    if (Getperms($class, $action) == FALSE) {
        $resp = array(
            'message' => "Anda tidak memiliki hak untuk mengakses halaman ini!",
            'heading' => 'An Error Was Encountered',
            'error_code' => 403,
        );
        if (!empty($redirect)) {
            redirect($redirect);
        }
        set_status_header($resp['error_code']);
        ob_start();
        LoadTemplate($resp, 'errors/html/error_page');
        $buffer = ob_get_contents();
        ob_end_clean();
        echo $buffer;
        exit(1);
    } else {
        return TRUE;
    }
}

function getAlphas($index, $row = null) {
    $index += -1;
    $alphas = range("A", "Z");
    if (($index + 1) > 26) {
        $rep = ($index / 26) - 1;
        $mod = $index % 26;
        $column = $alphas[$rep] . $alphas[$mod];
    } else {
        $column = $alphas[$index];
    }

    if ($row) {
        $column .= $row;
    }

    return $column;
}

function getJenisAliranKas($id = null) {
    $jenis = [
        1 => 'Pemasukan',
        2 => 'Pengeluaran',
    ];
    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function getJenisBiayaKas($id = null) {
    $jenis = [
        1 => 'Setoran',
        2 => 'Biaya Terminal',
        3 => 'Administrasi Kantor',
        4 => 'Biaya lain-lain',
        5 => 'Setoran Resume',
    ];
    if ($id) {
        $jenis = $jenis[$id];
    }

    return $jenis;
}

function GetJenisBiayaPengemudi($idx = null, $forForm=false) {
    $jenis = [
        'bonus_kerajinan' => "Bonus Kerajinan",
        'cuci' => "Cuci",
        'denda' => "Denda",
        'full_seat_barat' => "Fullseat Barat",
        'full_seat_timur' => "Fullseat Timur",
        'hutang' => "Hutang",
        'honor_pengemudi' => "Honor Pengemudi",
        'jaminan_pengemudi' => "Jaminan Pengemudi",
        'kasbon' => "Kasbon",
        'kp_selamat_barat' => "KPS Barat",
        'kp_selamat_timur' => "KPS Timur",
        'kps_ekstra' => "KPS Ekstra",
        'kps_pas' => "KPS Pas",
        'kp_prs' => "KP PRS",
        'kurang_setor' => "Kurang Setor",
        'laka' => "Laka",
        'lebih_setor' => "Lebih Setor",
        'lain_lain' => "Lain-lain",
        'parkir' => "Parkir",
        'overblass_barat' => "Overblass Barat",
        'overblass_timur' => "Overblass Timur",
        'pendapatan' => "Pendapatan Cash Pengemudi",
        'piutang' => "Pendapatan Piutang Pengemudi",
        'retur_bop' => "Retur BOP",
        'subsidi_toilet' => "Subsidi Toilet",
        'tabungan' => "Tabungan",
        'tambahan_interval' => "Tambahan Interval",
        'tilangan' => "Tilangan",
        'transitan' => "Transitan",
        'uang_makan' => "Uang Makan",
        'uang_maintenance' => "Uang Maintenance",
        'uht' => "UHT",
    ];
    if ($idx) {
        $jenis = $jenis[$idx];
    }elseif($forForm){
        unset($jenis['pendapatan']);
        unset($jenis['piutang']);
        unset($jenis['tabungan']);
        unset($jenis['pendapatan']);
        unset($jenis['honor_pengemudi']);
        unset($jenis['bonus_kerajinan']);
    }

    return $jenis;
}

function bolehLihatReservasi() {
    $isAllowed = true;
    if (GetTicketingLayout()) {
        $isAllowed = false;
        $tipe_bus_user = explode(",", GetScalarData('#_user', 'id_user', GetUserId(), 'id_tipe_bus'));
        if (count($tipe_bus_user) && in_array(7, $tipe_bus_user)) {
            $isAllowed = true;
        }
    }

    return $isAllowed;
}

function availableTipeBus($type = 'obj') {
    $tipe_bus_user = GetScalarData('#_user', 'id_user', GetUserId(), 'id_tipe_bus');

    if (strlen($tipe_bus_user) && $tipe_bus_user != "0") {
        $tipe_bus_user = explode(",", $tipe_bus_user);
        if($type != "obj" && $type != "json"){
            $ids = [];
            $CI = get_instance();
            $CI->db->select("id_tipe_bus");
            $CI->db->from("#_tipe_bus");
            $CI->db->where("status", 1, false);
            $CI->db->where_in("id_tipe_bus", $tipe_bus_user);
            $list_tipe_bus = $CI->db->get()->result_array();
            if($list_tipe_bus){
                foreach($list_tipe_bus as $row){
                    $ids[] = $row['id_tipe_bus'];
                }
            }
            if($type == 'string'){
                return implode(",", $ids);
            }
            if($type == 'id'){
                return $ids;
            }
        }else{
            $list_tipe_bus = GetTableData('#_tipe_bus', 'id_tipe_bus', array('nama_tipe'), array('status' => 1), $type, array('id_tipe_bus', 'ASC'), array("id_tipe_bus", $tipe_bus_user));
        }
    } else {
        $list_tipe_bus = GetTableData('#_tipe_bus', 'id_tipe_bus', array('nama_tipe'), array('status' => 1), $type);
    }

    return $list_tipe_bus;
}

function DefaultTanggalIndo($val, $day = false) {
    $tanggal = date('j', strtotime($val)) . ' ' . GetMonth(date('n', strtotime($val))) . ' ' . date('Y', strtotime($val));
    if ($day) {
        $tanggal = GetDay(date('w', strtotime($val))) . ', ' . $tanggal;
    }
    return $tanggal;
}

function DefaultTanggalIndoShort($val, $day = false) {
    $tanggal = date('j', strtotime($val)) . ' ' . GetMonthShort(date('n', strtotime($val))) . ' ' . date('y', strtotime($val));
    if ($day) {
        $tanggal = GetDay(date('w', strtotime($val))) . ', ' . $tanggal;
    }
    return $tanggal;
}

function DefaultCurrencyIndo($number) {
    if (CheckEmpty($number)) {
        $number = '0';
    }
    return number_format(DefaultCurrencyDatabase($number), 0, ',', '.');
}

function GetKey() {
    $setting = GetRowData('#_setting', ['id_setting' => 1]);
    $mode = $setting['online_payment_mode'];
    $col = ($mode == "sandbox") ? "_sandbox" : "";
    $client_key = $setting['midtrans' . $col . '_client_key'];
    $server_key = $setting['midtrans' . $col . '_server_key'];
    return [
        'client_key' => $client_key,
        'server_key' => $server_key,
        'is_production' => $mode == "production" ? true : false
    ];
}

function GetPersenRefund($t1, $t2, $persen_only = true){
    $t1 = strtotime($t1);
    $t2 = strtotime($t2);
    $diff = (int)(($t2 - $t1) / 3600);

    $persen = $persen_only ? 0 : ['persen'=>0, 'text'=>''];

    $min = 2;
    $max = 48;

    if($diff <= $min){
        $persen = $persen_only ? 0 : ['persen'=>0, 'text'=>'&le; '.$min];
    }elseif($diff > $max){
        $persen = $persen_only ? 100 : ['persen'=>100, 'text'=>'&ge; '.$max];
    }else{
        $range = [
            ['min' => $min, 'max' => 8, 'persen' => 15],
            ['min' => 8, 'max' => 12, 'persen' => 25],
            ['min' => 12, 'max' => 24, 'persen' => 50],
            ['min' => 24, 'max' => $max, 'persen' => 100],
        ];

        foreach($range as $val){
            if($diff > $val['min'] && $diff <= $val['max']){
                $persen = $persen_only ? $val['persen'] : ['persen'=>$val['persen'], 'text' => $min.' &lt; x &le; '.$max];
            }
        }
    }

    return $persen;
}

function KirimEmailTerserah($id_kelengkapan=null, $kelengkapan=[],$debug=false){
    if($id_kelengkapan){
        $kelengkapan = GetRowData('#_history_kelengkapan', ['id_kelengkapan' => $id_kelengkapan], [], [], [], true);
    }
	$email = GetScalarData('#_setting', 'id_setting', 1, 'email_kelengkapan');
	
	if($email && $kelengkapan){
		if(($kelengkapan['penilaian_interior']>0&&$kelengkapan['penilaian_interior']<=7)||($kelengkapan['penilaian_eksterior']>0&&$kelengkapan['penilaian_eksterior']<=7))
		{
				try{
					$sisipan = GetRowsData('#_history_kelengkapan_receipt', ['id_kelengkapan' => $id_kelengkapan]);
					$data = [
						'kelengkapan' => $kelengkapan,
						'body' => GetRowData('#_body', ['id_body' => $kelengkapan['id_body']]),
						'pembuat' => GetScalarData('#_user', 'id_user', $kelengkapan['created_by'], 'nama_user'),
						'pengedit' => $kelengkapan['updated_by'] ? GetScalarData('#_user', 'id_user', $kelengkapan['updated_by'], 'nama_user') : "",
						'sisipan' => $sisipan
					];
					
					$CI = get_instance();

		//			echo $CI->load->view('api/email_kelengkapan_template', $data);
					$CI->load->library('email', [
						'mailtype' => 'html',
						'charset' => 'iso-8859-1'
					]);
					$config['protocol']    = 'smtp';
					$config['smtp_host']    = 'ssl://smtp.gmail.com';
					$config['smtp_port']    = '465';
					$config['smtp_timeout'] = '7';
					$config['smtp_user']    = 'kondisibus@gmail.com';
					$config['smtp_pass']    = 'admin1234/.?';
					$config['charset']    ='iso-8859-1';
					$config['newline']    = "\r\n";
					$config['mailtype'] = 'html'; // or html
					$config['validation'] = TRUE; // bool whether to validate email or not      

					$CI->email->initialize($config);
					
					$CI->email->from('kondisibus@gmail.com', 'Sistem Sinar Jaya');
					$CI->email->to(explode(';',$email));
					//$CI->email->to("sentosa.jimmy@yahoo.com");
					//$CI->email->reply_to('noreply@eticket.dgti.co.id');
					
					$CI->email->subject('Laporan Kelengkapan');
					$message = $CI->load->view('api/email_kelengkapan_template', $data, true);
					$CI->email->message($message);
					/*if(count($sisipan)){
						foreach($sisipan as $item){
							$file_path = FCPATH . 'uploads\\kelengkapan\\'.$item['receiptfile'];
							if(file_exists($file_path)){
								$CI->email->attach(base_url().'uploads/kelengkapan/'.$item['receiptfile']);
							}
						}
					}*/
				
				$returnmess=$CI->email->send();
				//dumperror($CI->email->print_debugger());
				if($debug)
				{
					
					
				
				}
				return $returnmess;

			}catch (Exception $ex){
				return $ex->getMessage();
			}
		}
	}
	return '';
}

function GetPT($id = null) {
    $pt = array(
        1 => "Sinar Jaya",
        2 => "Kommuter",
    );
    
    if ($id) {
        $pt = $pt[$id];
    }
    
    return $pt;
}


function GetArrayValueByKey($array, $key=null){
    if($key && array_key_exists($key, $array)){
        return $array[$key];
    }
    return null;
}

function isThirdParty($id_user=null){
    if(!$id_user){
        $id_user = GetUserId();
    }
    $jenis_mitra = GetScalarData('#_user', 'id_user', $id_user, 'jenis_mitra');

    if($jenis_mitra == 3){
        return true;
    }
    return false;
}

function GetLevelJabatan()
{
    $CI = get_instance();
    $jbtn = $CI->db->select('#_jabatan.level')->from("#_pegawai")->where('id_user', GetUserId())
            ->join("#_jabatan", '#_pegawai.id_jabatan = #_jabatan.id_jabatan')->get()->row();

    return ($jbtn) ? $jbtn->level : 0;
}