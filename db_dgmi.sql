alter table dgmi_prospek_detail add id_warna_karoseri int;
alter table dgmi_prospek_detail add foreign key(id_warna_karoseri) references dgmi_warna(id_warna);
alter table dgmi_prospek_detail add foreign key(id_warna) references dgmi_warna(id_warna);
alter table dgmi_prospek_detail add foreign key(id_unit) references dgmi_unit(id_unit);
alter table dgmi_prospek_detail add foreign key(id_jenis_plat) references dgmi_jenis_plat(id_jenis_plat);
update dgmi_prospek_detail set id_type_body=null where id_type_body=0;
alter table dgmi_prospek_detail add foreign key(id_type_body) references dgmi_type_body(id_type_body);
ALTER TABLE `dgmi_prospek_history_detail` ADD COLUMN `id_warna_karoseri` int(255) NULL AFTER `id_warna`;
ALTER TABLE `db_dgmi`.`dgmi_prospek_history` ADD COLUMN `close_status` tinyint(1) NULL AFTER `status`;
ALTER TABLE `db_dgmi`.`dgmi_prospek_history` 
ADD COLUMN `nama_stnk` varchar(45) NULL AFTER `id_cabang`,
ADD COLUMN `alamat_stnk` varchar(200) NULL AFTER `nama_stnk`,
ADD COLUMN `tdp_prospek` varchar(100) NULL AFTER `alamat_stnk`,
ADD COLUMN `no_ktp_prospek` varchar(100) NULL AFTER `tdp_prospek`;